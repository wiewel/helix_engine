Helix Engine
============

This repository contains the source code of the Helix Engine and its accompanying tools.
Due to licensing restrictions some of the third party source code was removed from this repository.
In order to build the engine, the missing source code needs to be obtained otherwise.

The game engine was developed for several practical course projects during our studies at the Technical University of Munich.
It was developed from 2014 until 2017.


Impressions
-----------

Photogrammetry of Train Station:
![Train Station](Images/TrainStation_Real_0.jpg)
![Train Station](Images/TrainStation_Real_1.jpg)
![Photogrammetry](Images/Photogrammetry_1.jpg)
![Photogrammetry](Images/Photogrammetry_0.jpg)

Nucleo Leveleditor:
![Nucleo Leveleditor](Images/Nucleo_Leveleditor.jpg)

Game Screenshots:
![RedLine](Images/RedLine_0.jpg)
![RedLine](Images/RedLine_1.jpg)
![RedLine](Images/RedLine_2.jpg)
![RedLine](Images/RedLine_3.jpg)
