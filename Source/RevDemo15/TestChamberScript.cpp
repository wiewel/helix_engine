//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher, David Labode
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "TestChamberScript.h"
#include "Input\InputMapper.h"
#include "Async\Delegate.h"
#include "Scene\Camera.h"
#include "Scene\PhysicsScene.h"
#include "Math\XMMath.h"
#include "Physics\InertiaTensor.h"
#include "Scene\LightContext.h"
#include "Util\Functional.h"
#include "Resources\FileSystem.h"

using namespace EddyFlux;
using namespace Helix;
using namespace Resources;

//---------------------------------------------------------------------------------------------------
TestChamberScript::TestChamberScript(const std::string& _sLevelName) :
	Async::IScriptObject(),
	m_Level(_sLevelName)
{
	m_LevelList = Helix::map(FileSystem::GetFileNameWithoutExtension, FileSystem::GetFilesFromDirectory(FileSystem::Instance()->GetKnownDirectoryPath(Resources::HelixDirectories_Levels), ".lvl"));
	m_uCurrentLevel = (std::find(m_LevelList.begin(), m_LevelList.end(), _sLevelName) - m_LevelList.begin()) % m_LevelList.size();
}
//---------------------------------------------------------------------------------------------------
TestChamberScript::~TestChamberScript()
{
}
//---------------------------------------------------------------------------------------------------
void TestChamberScript::SetCamera(std::shared_ptr<Helix::Scene::Camera> _pCamera)
{
	m_pCamera = _pCamera;
}
//---------------------------------------------------------------------------------------------------
void TestChamberScript::SetView(Display::RevDemoView* _pRevDemoView)
{
	m_pRevDemoView = _pRevDemoView;
}
//---------------------------------------------------------------------------------------------------
void TestChamberScript::SetCollisionSystem(Collision::CollisionSystem * _pCollisionSystem)
{
	m_pCollisionSystem = _pCollisionSystem;
}
//---------------------------------------------------------------------------------------------------
void TestChamberScript::SetParticleSystem(Helix::Scene::ParticleSystem * _pParticleSystem)
{
	m_pParticleSystem = _pParticleSystem;
}
//---------------------------------------------------------------------------------------------------
void TestChamberScript::SetGravityField(Helix::Physics::GravityField * _pGravityField)
{
	m_pGravityField = _pGravityField;
}
//---------------------------------------------------------------------------------------------------
void TestChamberScript::OnSceneBegin()
{
	if (m_pFluxCompensator)
	{
		m_pFluxCompensator->Destroy();
		m_pFluxCompensator = nullptr;
	}

	m_bEditMode = true;

	m_Level.Initialize(m_LevelList[m_uCurrentLevel]);

	m_FluxCompMesh = Display::MeshClusterDX11("FluxCompensator");
	m_FluxCompMat = Display::MaterialDX11("FluxCompensatorMaterial");

	m_FluxMagnetMesh = Display::MeshClusterDX11("FluxMagnets");
	m_FluxMagnetMat = Display::MaterialDX11("FluxMagnetsMaterial");

	const Resources::SpawnPoint* pSpawnpoint = m_Level.GetSpawnPoint(0);

	if (pSpawnpoint != nullptr && m_bResetCameraOnLevelLoad)
	{
		AMP::float3 vPosition(pSpawnpoint->vPosition.x, pSpawnpoint->vPosition.y, pSpawnpoint->vPosition.z);
		AMP::float3 vTarget(vPosition.x + pSpawnpoint->vDirection.x, vPosition.y + pSpawnpoint->vDirection.y, vPosition.z + pSpawnpoint->vDirection.z);

		m_pCamera->LookAtFrom(vPosition, vTarget);

		m_bResetCameraOnLevelLoad = false;
	}	
	
	m_InputReceiver.AddContext("EddyFlux", "EddyFlux_InputMapping");

	m_bFluxCompensatorLaunched = false;
	m_pFluxLight = m_Level.GetGameObjectByName("FluxLight");

	Datastructures::GameObject* pGoal = m_Level.GetGameObjectByName("PortalRed");

	if (pGoal != nullptr)
	{
		m_GoalOBB = pGoal->OrientedBoundingBox;
		m_GoalOBB.m_vHalfExtents *= 2.f;
	}

	if (m_pParticleSystem != nullptr)
	{
		m_pParticleSystem->SetPaused(true);
	}

	//m_pRevDemoView->SetFullscreenTexture("WinMaterial", false);
	m_vCameraSmoothedGoal = { 0,0 };
	m_vCameraSmoothedVelocity = { 0,0 };

	ResetAllGravityParticles();

	SpawnGravityPlacementHelper(m_fCurrentStrength);

	// print current values to console
	IncrementGravityRadius(0);
	IncrementGravityStrength(0);

	//m_pRevDemoView->GetFluxPass()->SetEnabled(false);
}
//---------------------------------------------------------------------------------------------------
void TestChamberScript::OnSceneEnd()
{
}
//---------------------------------------------------------------------------------------------------
void TestChamberScript::OnUpdate(double _fDeltaTime, double _fTotalTime)
{
	float fDeltaTime = _fDeltaTime;

	if (m_pFluxCompensator != nullptr)
	{
		if (m_pFluxLight != nullptr)
		{
			m_pFluxLight->Position = m_pFluxCompensator->Position;
		}
	}
	else if (m_pFluxLight != nullptr)
	{
		// 'disable' light
		m_pFluxLight->Position = { -1000.f,-1000.f ,-1000.f };
	}

	// keyboard
	const float fGravityRadius = 0.5f;
	if (m_InputReceiver.Action("Gravity_Radius_Down"))
	{
		IncrementGravityRadius(-fGravityRadius);
	}
	if (m_InputReceiver.Action("Gravity_Radius_Up"))
	{
		IncrementGravityRadius(fGravityRadius);
	}
	//if(m_InputReceiver.Action("Gravity_Radius_Toggle"))
	//{
	//	m_pRevDemoView->GetFluxPass()->SetEnabled(!m_pRevDemoView->GetFluxPass()->IsEnabled());
	//}

	const float fGravityStrength = 0.01f;
	if (m_InputReceiver.Action("Gravity_Strength_Down"))
	{
		IncrementGravityStrength(-fGravityStrength);
	}
	if (m_InputReceiver.Action("Gravity_Strength_Up"))
	{
		IncrementGravityStrength(fGravityStrength);
	}
	
	// XBONE
	if (m_InputReceiver.Action("Gravity_Strength_Radius_Toggle"))
	{
		m_bGravityStrength = !m_bGravityStrength;
	}

	if(m_bGravityStrength)
	{
		if (m_InputReceiver.Action("Bumper_Left"))
		{
			IncrementGravityStrength(-fGravityStrength);
		}
		if (m_InputReceiver.Action("Bumper_Right"))
		{
			IncrementGravityStrength(fGravityStrength);
		}
	}
	else // radius mode
	{
		if (m_InputReceiver.Action("Bumper_Left"))
		{
			IncrementGravityRadius(-fGravityRadius);
		}
		if (m_InputReceiver.Action("Bumper_Right"))
		{
			IncrementGravityRadius(fGravityRadius);
		}
	}

	//if (m_InputReceiver.Action("SSAO_Toggle"))
	//{
	//	m_pRevDemoView->GetSSAOPass()->EnableSSAO(!m_pRevDemoView->GetSSAOPass()->IsSSAOEnabled());
	//	HLOG("SSAO enabled: %u", m_pRevDemoView->GetSSAOPass()->IsSSAOEnabled());
	//}

	//const static float fRadiusDelta = 0.1f;
	//if (m_InputReceiver.Action("SSAO_Radius_Down"))
	//{
	//	m_pRevDemoView->GetSSAOPass()->SetSSAORadius(m_pRevDemoView->GetSSAOPass()->GetSSAORadius() - fRadiusDelta);
	//	HLOG("SSAORadius: %f", m_pRevDemoView->GetSSAOPass()->GetSSAORadius());
	//}
	//if (m_InputReceiver.Action("SSAO_Radius_Up"))
	//{
	//	m_pRevDemoView->GetSSAOPass()->SetSSAORadius(m_pRevDemoView->GetSSAOPass()->GetSSAORadius() + fRadiusDelta);
	//	HLOG("SSAORadius: %f", m_pRevDemoView->GetSSAOPass()->GetSSAORadius());
	//}

	//const static float fPowerDelta = 0.5f;
	//if (m_InputReceiver.Action("SSAO_Power_Down"))
	//{
	//	m_pRevDemoView->GetSSAOPass()->SetSSAOPower(m_pRevDemoView->GetSSAOPass()->GetSSAOPower() - fPowerDelta);
	//	HLOG("SSAOPower: %f", m_pRevDemoView->GetSSAOPass()->GetSSAOPower());
	//}
	//if (m_InputReceiver.Action("SSAO_Power_Up"))
	//{
	//	m_pRevDemoView->GetSSAOPass()->SetSSAOPower(m_pRevDemoView->GetSSAOPass()->GetSSAOPower() + fPowerDelta);
	//	HLOG("SSAOPower: %f", m_pRevDemoView->GetSSAOPass()->GetSSAOPower());
	//}

	// Toggle Edit Mode
	if (m_InputReceiver.Action("Toggle_Edit_Mode"))
	{
		m_bEditMode = !m_bEditMode;
	}
	if (m_InputReceiver.Action("Toggle_Grid_Mode"))
	{
		m_bGridMode = !m_bGridMode;
	}
	if(m_pFluxCompensator != nullptr)
	{
		m_bEditMode = false;
	}

	if (m_InputReceiver.Action("Draw_Particles") && m_pParticleSystem != nullptr)
	{
		m_pParticleSystem->SetPaused(!m_pParticleSystem->GetPaused());
	}

	if (m_InputReceiver.Action("Restart_Particles") && m_pParticleSystem != nullptr)
	{
		StartParticleSystem();
	}

	bool bSpawnGravityParticle = m_InputReceiver.Action("Spawn_Gravity_Particle");
	if(m_bEditMode)
	{
		// Move Demo Particle
		AMP::float3 vPosition = m_pCamera->GetPosition() + m_pCamera->GetLook();

		if (m_bGridMode)
		{
			vPosition.x = std::roundf(vPosition.x);
			vPosition.y = std::roundf(vPosition.y);
			vPosition.z = std::roundf(vPosition.z);
		}

		if(m_pGravityDemoParticle == nullptr)
		{
			SpawnGravityPlacementHelper(m_fCurrentStrength);
		}
		else
		{
			m_pGravityDemoParticle->SetScale(CalculateSize(m_fCurrentStrength));
			m_pGravityDemoParticle->SetPosition(vPosition);
		}

		SelectMagnet();

		bool bGravityDemoParticleIsColliding = m_pCollisionSystem->DidCollide(*m_pGravityDemoParticle);

		AMP::float3 vColor;

		// Make Demo Particle invisible if it hits the ground
		if (bGravityDemoParticleIsColliding)
		{
			vColor = AMP::float3(0.7f, 0.0f, 0.0f);
		}
		else
		{
			vColor = AMP::float3(0.0f, 0.7f, 0.0f);
		}

		// Set emissive color
		if (m_pGravityDemoParticle->GetMaterial() != nullptr)
		{
			m_pGravityDemoParticle->GetMaterial().GetProperties().vEmissive = vColor;
		}

		if (bSpawnGravityParticle && bGravityDemoParticleIsColliding == false)
		{
			if (m_bGridMode)
			{
				vPosition.x = std::roundf(vPosition.x);
				vPosition.y = std::roundf(vPosition.y);
				vPosition.z = std::roundf(vPosition.z);
			}

			SpawnMagnet(vPosition, m_fCurrentStrength);
		}
	}
	else
	{
		if(m_pGravityDemoParticle)
		{
			m_pGravityDemoParticle->Destroy();
			m_pGravityDemoParticle = nullptr;
		}
	}

	if (m_InputReceiver.Action("Spawn_Flux_Kompensator"))
	{
		if (m_pFluxCompensator != nullptr)
		{
			m_pFluxCompensator->Destroy();
			m_pFluxCompensator = nullptr;
			m_bEditMode = true;
		}
		else 
		{
			m_pFluxCompensator = CreateGameObject().get();

			if (m_pFluxCompensator != nullptr)
			{
				m_pFluxCompensator->Initialize(
					(m_Level.GetSpawnPoint(1) != nullptr) ? m_Level.GetSpawnPoint(1)->vPosition : AMP::float3(0, 0, 0),
					AMP::float3(1, 1, 1),
					HMFLOAT3_ZERO,
					Datastructures::GameObjectFlag::None,
					1.0f,
					0.2f,
					0.5f,
					m_Level.GetSpawnPoint(1)->vDirection,
					AMP::float3(5.0, 1.0, 2.0),
					Physics::InertiaTensor::ComputeCuboidInertiaTensor(AMP::float3(0.3, 0.3, 0.3), 1.0f),
					Collision::OBB(HMFLOAT3_ZERO, AMP::float3(0.75f, 0.75f, 0.75f), HMQUATERNION_IDENTITY)
					);


				m_pFluxCompensator->SetMesh(m_FluxCompMesh);
				m_pFluxCompensator->SetMaterial(m_FluxCompMat);
			}
		}			
	}

	if (m_pFluxCompensator == nullptr)
	{
		if (m_InputReceiver.Action("Undo_Gravity_Particle"))
		{
			RemoveSelectedMagnet();
		}
	}

	if(m_pFluxCompensator != nullptr && m_pCollisionSystem != nullptr)
	{
		//	m_pRevDemoView->SetFullscreenTexture("WinMaterial", true);

		Collision::CollisionInformation Info;
		if (m_pFluxCompensator->OrientedBoundingBox.Intersects(m_GoalOBB, Info))
		{
			m_bResetCameraOnLevelLoad = true;
			++m_uCurrentLevel;
			m_uCurrentLevel %= m_LevelList.size();
			OnSceneBegin();
			return;
		}

		if (m_pCollisionSystem->DidCollide(*m_pFluxCompensator))
		{
			HLOG("Destroyed %u", m_pFluxCompensator->Index);
			m_pFluxCompensator->Destroy();
			m_pFluxCompensator = nullptr;

			m_bEditMode = true;
		}
	}

	//Movement

	// Keyboard movement
	if (m_InputReceiver.State("Move_Forward"))
	{
		m_pCamera->Walk(m_fMoveSpeed * fDeltaTime);
	}
	if (m_InputReceiver.State("Move_Right"))
	{
		m_pCamera->Strafe(m_fMoveSpeed * fDeltaTime);
	}
	if (m_InputReceiver.State("Move_Back"))
	{
		m_pCamera->Walk(-m_fMoveSpeed * fDeltaTime);
	}
	if (m_InputReceiver.State("Move_Left"))
	{
		m_pCamera->Strafe(-m_fMoveSpeed * fDeltaTime);
	}

	// Gamepad movement
	int32_t iMoveX = 0;
	if (m_InputReceiver.Range("Move_X", iMoveX) && abs(iMoveX) > m_iGamepadDeadzone)
	{
		iMoveX -= (iMoveX > 0) ? m_iGamepadDeadzone : -m_iGamepadDeadzone;
		float fMove = iMoveX / (32768.0f - m_iGamepadDeadzone);
		m_pCamera->Strafe(fMove * m_fMoveSpeed * fDeltaTime);
	}
	int32_t iMoveY = 0;
	if (m_InputReceiver.Range("Move_Y", iMoveY) && abs(iMoveY) > m_iGamepadDeadzone)
	{
		iMoveY -= (iMoveY > 0) ? m_iGamepadDeadzone : -m_iGamepadDeadzone;
		float fMove = -iMoveY / (32768.0f - m_iGamepadDeadzone);
		m_pCamera->Walk(fMove * m_fMoveSpeed * fDeltaTime);
	}
	
	if (m_InputReceiver.Action("Reload_Level"))
	{
		OnSceneBegin();
		return;
	}
	
	if (m_InputReceiver.Action("Next_Level"))
	{
		m_bResetCameraOnLevelLoad = true;
		++m_uCurrentLevel;
		m_uCurrentLevel %= m_LevelList.size();
		OnSceneBegin();
		return;
	}

	// Mouse looking
	int32_t iLookX = 0;
	int32_t iLookY = 0;

	if (m_InputReceiver.Mouse(iLookX, iLookY))
	{
		if (m_InputReceiver.State("Click"))
		{
			float fLookX = iLookX;
			float fLookY = iLookY;
			m_vCameraSmoothedGoal.x += fLookX;
			m_vCameraSmoothedGoal.y += fLookY;
			m_vCameraSmoothedVelocity.x = m_vCameraSmoothedGoal.x / (1.0f + m_uMouseSmoothness);
			m_vCameraSmoothedVelocity.y = m_vCameraSmoothedGoal.y / (1.0f + m_uMouseSmoothness);
		}
	}
	if (fabsf(m_vCameraSmoothedGoal.x) > 0.1f)
	{
		m_vCameraSmoothedGoal.x -= m_vCameraSmoothedVelocity.x;
		m_pCamera->RotateY(m_vCameraSmoothedVelocity.x  * m_fLookSpeed * fDeltaTime);
	}
	else
	{
		m_vCameraSmoothedGoal.x = 0;
		m_vCameraSmoothedVelocity.x = 0;
	}
	if (fabsf(m_vCameraSmoothedGoal.y) > 0.1f)
	{
		m_vCameraSmoothedGoal.y -= m_vCameraSmoothedVelocity.y;
		m_pCamera->Pitch(m_vCameraSmoothedVelocity.y  * m_fLookSpeed * fDeltaTime);
	}
	else
	{
		m_vCameraSmoothedGoal.y = 0;
		m_vCameraSmoothedVelocity.y = 0;
	}

	// Gamepad looking
	iLookX = 0;
	if (m_InputReceiver.Range("Look_X", iLookX) && abs(iLookX) > m_iGamepadDeadzone)
	{
		iLookX -= (iLookX > 0) ? m_iGamepadDeadzone : -m_iGamepadDeadzone;
		float fMove = iLookX / (32768.0f - m_iGamepadDeadzone);
		m_pCamera->RotateY(fMove * m_fLookSpeed * fDeltaTime);
	}
	iLookY = 0;
	if (m_InputReceiver.Range("Look_Y", iLookY) && abs(iLookY) > m_iGamepadDeadzone)
	{
		iLookY -= (iLookY > 0) ? m_iGamepadDeadzone : -m_iGamepadDeadzone;
		float fMove = iLookY / (32768.0f - m_iGamepadDeadzone);
		m_pCamera->Pitch(fMove * m_fLookSpeed * fDeltaTime);
	}
	m_InputReceiver.Sync();
}
//---------------------------------------------------------------------------------------------------
void TestChamberScript::SpawnGravityPlacementHelper(const float _fStrength)
{
	if (m_pGravityDemoParticle)
	{
		m_pGravityDemoParticle->Destroy();
		m_pGravityDemoParticle = nullptr;
	}

	// Spawn gravity placement helper
	AMP::float3 vPosition = m_pCamera->GetPosition() + m_pCamera->GetLook();

	m_pGravityDemoParticle = CreateGameObject().get();

	if (m_pGravityDemoParticle != nullptr)
	{
		m_pGravityDemoParticle->Initialize(
			vPosition,
			CalculateSize(_fStrength),
			HMFLOAT3_ZERO,
			Datastructures::GameObjectFlag::NoPhysics,
			1.0f,
			0.2f,
			0.5f,
			HMFLOAT3_ZERO,
			HMFLOAT3_ZERO,
			Physics::InertiaTensor::ComputeCuboidInertiaTensor(CalculateSize(_fStrength), 1.0f),
			Collision::OBB(HMFLOAT3_ZERO, AMP::float3(1.0f, 1.0f, 1.0f), HMQUATERNION_IDENTITY)
		);
		m_pGravityDemoParticle->SetMesh(m_FluxMagnetMesh);
		m_pGravityDemoParticle->SetMaterial(m_FluxMagnetMat);//Display::MaterialDX11("PlacementHelperMat")		
		m_pGravityDemoParticle->GetMaterial().GetProperties().vEmissive = AMP::float3(0.0f, 0.7f, 0.0f);
	}
}
//---------------------------------------------------------------------------------------------------
void TestChamberScript::SpawnMagnet(const AMP::float3& _vPosition, const float _fStrength)
{
	if (m_pGravityField->PushGravityPoint(_vPosition, _fStrength))
	{
		Datastructures::GameObject* pMagnet = CreateGameObject().get();

		if (pMagnet != nullptr)
		{
			HLOG("Spawning GravityParticle %f %f %f", _vPosition.x, _vPosition.y, _vPosition.z);
			pMagnet->Initialize(
				_vPosition,
				CalculateSize(_fStrength),
				HMFLOAT3_ZERO,
				Datastructures::GameObjectFlag::Static | Datastructures::GameObjectFlag::NoCollision,
				1.0f,
				0.2f,
				0.5f,
				HMFLOAT3_ZERO,
				HMFLOAT3_ZERO,
				Physics::InertiaTensor::ComputeCuboidInertiaTensor(CalculateSize(_fStrength), 1.0f),
				Collision::OBB(HMFLOAT3_ZERO, AMP::float3(1.0f, 1.0f, 1.0f), HMQUATERNION_IDENTITY)
			);

			pMagnet->SetName(std::string("GravityMagnet ") + std::to_string(m_GravityMagnets.size()));

			pMagnet->SetMesh(m_FluxMagnetMesh);
			pMagnet->SetMaterial(m_FluxMagnetMat);

			m_GravityMagnets.push_back(pMagnet);
			//m_pRevDemoView->GetFluxPass()->PushMagnet(pMagnet->Position);
		}
	}
}
//---------------------------------------------------------------------------------------------------
void TestChamberScript::RemoveSelectedMagnet()
{
	if (m_pSelectedMagnet == nullptr || m_GravityMagnets.empty())
	{
		return;
	}

	for (auto it = m_GravityMagnets.begin(); it != m_GravityMagnets.end(); ++it)
	{
		if (*it == m_pSelectedMagnet)
		{
			HLOG("Destroyed %s", m_pSelectedMagnet->GetName().c_str());

			
			//m_pRevDemoView->GetFluxPass()->RemoveMagnet(m_pSelectedMagnet->Position);
			m_pSelectedMagnet->Destroy();
			m_pSelectedMagnet = nullptr;

			m_GravityMagnets.erase(it);

			return;
		}
	}
}
//---------------------------------------------------------------------------------------------------
void TestChamberScript::ResetAllGravityParticles()
{
	for (Datastructures::GameObject* pMagnet : m_GravityMagnets)
	{
		//m_pRevDemoView->GetFluxPass()->RemoveMagnet(pMagnet->Position);
		pMagnet->Destroy();
	}

	m_pSelectedMagnet = nullptr;
	m_GravityMagnets.resize(0);
}
//---------------------------------------------------------------------------------------------------
void TestChamberScript::IncrementGravityStrength(float _fIncrement)
{
	m_fCurrentStrength = Math::Clamp(m_fCurrentStrength + _fIncrement, m_fMinStrength, m_fMaxStrength);
	HLOG("Gravity Strength: %f", m_fCurrentStrength);
}
//---------------------------------------------------------------------------------------------------
void TestChamberScript::IncrementGravityRadius(float _fIncrement)
{
	float fMaxDist = m_pGravityField->GetMaxDistance() + _fIncrement;
	m_pGravityField->SetMaxDistance(fMaxDist);
	//m_pRevDemoView->GetFluxPass()->SetMagnetRadius(fMaxDist);
	HLOG("Gravity Distance: %f", fMaxDist);
}
//---------------------------------------------------------------------------------------------------
void EddyFlux::TestChamberScript::StartParticleSystem()
{
	if (m_pParticleSystem != nullptr)
	{

		const GeneralSettings& Settings = m_Level.GetLevelFile().GetGeneralSettings();

		AMP::float4 vSpawnSphere = { 0.f,0.f,0.f, Settings.fParticleSpawnRadius};
		AMP::float3 vSpawnVelocity = HMFLOAT3_ZERO;

		if (m_Level.GetSpawnPoint(1) != nullptr)
		{
			vSpawnSphere.xyz = m_Level.GetSpawnPoint(1)->vPosition;
			vSpawnVelocity = m_Level.GetSpawnPoint(1)->vDirection;
		}

		float fLifeTime = Settings.fParticleLifeTime <= 0.f ? 50.f : Settings.fParticleLifeTime;
		float fRate = Settings.fParticleSpawnFrequency <= 0.f ? 1000.f : Settings.fParticleSpawnFrequency;

		m_pParticleSystem->ConfigureSystem(vSpawnSphere, vSpawnVelocity, fLifeTime, fRate );
		m_pParticleSystem->SetPaused(false);

		HLOG("Starting ParticleSystem [%f,%f,%f] LifeTime %f Rate %f", vSpawnSphere.x, vSpawnSphere.y, vSpawnSphere.z, fLifeTime, fRate);
	}
}
//---------------------------------------------------------------------------------------------------
void EddyFlux::TestChamberScript::SelectMagnet()
{
	if (m_pGravityDemoParticle == nullptr)
	{
		return;
	}

	const float fMaxMinDist = 9.f; // squared dist
	float fMinDist = fMaxMinDist; // 3 meters
	Datastructures::GameObject* pNewSelection = m_pSelectedMagnet;

	for (Datastructures::GameObject* pMagnet : m_GravityMagnets)
	{
		float fDist = pMagnet->Position.DistanceSquared(m_pGravityDemoParticle->Position);
		if (fDist < fMinDist)
		{
			fMinDist = fDist;
			pNewSelection = pMagnet;
		}
	}

	if (fMinDist < fMaxMinDist)
	{
		if (pNewSelection != m_pSelectedMagnet)
		{
			// change old material
			if (m_pSelectedMagnet != nullptr)
			{
				if (m_pSelectedMagnet->GetMaterial() != nullptr)
				{
					m_pSelectedMagnet->GetMaterial().GetProperties() = m_pSelectedMagnet->GetMaterial().GetReferenceConst()->Properties;
				}
			}

			m_pSelectedMagnet = pNewSelection;

			// aplly new material		
			Display::MaterialProperties& Mat = m_pSelectedMagnet->GetMaterial().GetProperties();
			Mat.vEmissive = AMP::float3(0.0f, 0.0f, 1.f); // blue
		}		
	}
	else if(m_pSelectedMagnet != nullptr) // non selected
	{
		// reset old material
		if (m_pSelectedMagnet->GetMaterial() != nullptr)
		{
			m_pSelectedMagnet->GetMaterial().GetProperties() = m_pSelectedMagnet->GetMaterial().GetReferenceConst()->Properties;
		}

		m_pSelectedMagnet = nullptr;
	}
}
//---------------------------------------------------------------------------------------------------
Helix::AMP::float3 TestChamberScript::CalculateSize(float _fStrength) const
{
	float fInterpolant = Math::Clamp(_fStrength, m_fMinStrength, m_fMaxStrength) - m_fMinStrength;
	fInterpolant /= (m_fMaxStrength - m_fMinStrength);
	return (m_vMaxSize - m_vMinSize) * fInterpolant + m_vMinSize;
}
//---------------------------------------------------------------------------------------------------
