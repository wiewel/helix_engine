//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "SimpleLevelLoaderScript.h"
#include "Scene\RenderingScene.h"

using namespace Helix;

//---------------------------------------------------------------------------------------------------

SimpleLevelLoaderScript::SimpleLevelLoaderScript()
{
}
//---------------------------------------------------------------------------------------------------

SimpleLevelLoaderScript::~SimpleLevelLoaderScript()
{
}
//---------------------------------------------------------------------------------------------------

void SimpleLevelLoaderScript::OnSceneBegin()
{	
	m_Level.Load(STR(m_sLevelName), true);
}
//---------------------------------------------------------------------------------------------------

void SimpleLevelLoaderScript::OnSceneEnd()
{
	m_Level.Unload();
}
//---------------------------------------------------------------------------------------------------

void SimpleLevelLoaderScript::OnUpdate(double _fDeltaTime, double _fTotalTime)
{
	float fFPS = 1.f / static_cast<float>(Scene::RenderingScene::Instance()->GetExecutionTime());
	hlx::string s = STR("Monochromacy FPS " + std::to_string(fFPS));

	SetWindowText(m_hWND, s.c_str());
	//m_InputReciever.Sync();
}
//---------------------------------------------------------------------------------------------------