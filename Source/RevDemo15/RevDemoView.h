//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef REVDEMOVIEW_H
#define REVDEMOVIEW_H
 
#include "Display\DX11\RenderViewDX11.h"
#include "Scene\ScriptObject.h"
#include "SimpleLevelLoaderScript.h"

namespace Helix
{
	namespace Display
	{
		class RevDemoView : public RenderViewDX11
		{
		public:
			HDEBUGNAME("RevDemoView");
			RevDemoView();
			~RevDemoView();

			bool OnInitialize(HWND _hWnd);
			void OnShutdown();

			void SetLevel(const std::string& _sLevel);
		private:
			void InitializeAssets();

		private:
			SimpleLevelLoaderScript m_Script;
		};

		inline void RevDemoView::SetLevel(const std::string& _sLevel) { m_Script.SetLevel(_sLevel); }

	} // Display
} // Helix
 
#endif //REVDEMOVIEW_H