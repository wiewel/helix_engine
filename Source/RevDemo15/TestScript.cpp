//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "TestScript.h"
#include "Math\MathTypes.h"
#include "Physics\InertiaTensor.h"
#include "Collision\OBB.h"
#include "DataStructures\GameObject.h"
#include "Display\Geometry.h"
#include "Scene\Camera.h"
#include "Scene\LightContext.h"

using namespace Helix;
using namespace Helix::Math;
using namespace Helix::Scene;
using namespace Helix::Datastructures;

using namespace SSAO;
//---------------------------------------------------------------------------------------------------
TestScript::TestScript() :
	IScriptObject(),
	m_Level("SSAO_Level")
{
}
//---------------------------------------------------------------------------------------------------
TestScript::~TestScript()
{
}
//---------------------------------------------------------------------------------------------------
void TestScript::SetCamera(std::shared_ptr<Helix::Scene::Camera>  _pCamera)
{
	m_pCamera = _pCamera;
}
//---------------------------------------------------------------------------------------------------
void TestScript::SetView(Helix::Display::RevDemoView* _pRevDemoView)
{
	m_pRevDemoView = _pRevDemoView;
}
//---------------------------------------------------------------------------------------------------
void TestScript::OnSceneBegin()
{
	std::srand(std::time(0));
	
	m_Level.Initialize();

	const Resources::SpawnPoint* pSpawnpoint = m_Level.GetSpawnPoint(0);

	if (pSpawnpoint != nullptr && m_bResetCameraOnLevelLoad)
	{
		Helix::Math::XMFLOAT3 vPosition(pSpawnpoint->vPosition.x, pSpawnpoint->vPosition.y, pSpawnpoint->vPosition.z);
		Helix::Math::XMFLOAT3 vTarget(vPosition.x + pSpawnpoint->vDirection.x, vPosition.y + pSpawnpoint->vDirection.y, vPosition.z + pSpawnpoint->vDirection.z);

		m_pCamera->LookAtFrom(vPosition, vTarget);

		m_bResetCameraOnLevelLoad = false;
	}
	m_InputReceiver.AddContext("SSAO", "SSAO_InputMapping");
	//m_pRevDemoView->SetFullscreenTexture("WinMaterial", true);

	//m_pRevDemoView->SetFullscreenTexture("", false);

	//m_pRevDemoView->GetSSAOPass()->EnableSSAO(true);

	//m_pRevDemoView->GetSSAOPass()->SetSSAORadius(0.2f);
	//m_pRevDemoView->GetSSAOPass()->SetSSAOPower(3.0f);

	GameObject* pLight1 = m_Level.GetGameObjectByName("AngelL1");
	GameObject* pLight2 = m_Level.GetGameObjectByName("AngelL2");
	GameObject* pLight3 = m_Level.GetGameObjectByName("AngelL3");

	const std::vector<PointLight>& PointLights = LightContext::Instance()->GetPointLights();

	m_AngelLights.clear();
	HFOREACH(it, end, PointLights)
		const GameObject* pParent = it->GetParent();
		if(pParent == pLight1 || pParent == pLight2 || pParent == pLight3)
		{
			m_AngelLights.push_back(*it);
		}
	HFOREACH_END

	HLOG("Found %u angel lights!", m_AngelLights.size());

	// Spawn lights
	SpawnLights(60, {0.0f,-0.8f,0.0f});
}
//---------------------------------------------------------------------------------------------------
void TestScript::OnSceneEnd()
{
}
//---------------------------------------------------------------------------------------------------
void TestScript::OnUpdate(double _fDeltaTime, double _fTotalTime)
{
	//float fRandom = 0;

	uint32_t uAngelLightCount = 0;
	HFOREACH(it, end, m_AngelLights)
		float3 vColor = (LightContext::Instance()->GetLight((*it).GetIndex()))->GetColor();
		// sinf clamped to [-0.05,0.05]
		vColor = vColor +(std::sinf((_fTotalTime + uAngelLightCount*0.81354f) * 4.0f) / 5.0f  *_fDeltaTime);
		LightContext::Instance()->SetColor((*it).GetIndex(), vColor);
		++uAngelLightCount;
	HFOREACH_END

	uint32_t uLightCount = 0;
	HFOREACH(it,end, m_Lights)
	//fRandom = static_cast<float>(std::rand()) * m_fInvRandMax;
	float3 vPos = (*it).pObject->Position;
	vPos += (*it).vMoveAxis * std::sinf(_fTotalTime+ uLightCount*0.81354f)*_fDeltaTime;
	(*it).pObject->Position = vPos;
	++uLightCount;
	HFOREACH_END

	//Movement
	float fMoveSpeed = 2.5f;
	int32_t iGamepadDeadzone = 8192 << 1;

	// Keyboard movement
	if (m_InputReceiver.State("Move_Forward"))
	{
		m_pCamera->Walk(fMoveSpeed * _fDeltaTime);
	}
	if (m_InputReceiver.State("Move_Right"))
	{
		m_pCamera->Strafe(fMoveSpeed * _fDeltaTime);
	}
	if (m_InputReceiver.State("Move_Back"))
	{
		m_pCamera->Walk(-fMoveSpeed * _fDeltaTime);
	}
	if (m_InputReceiver.State("Move_Left"))
	{
		m_pCamera->Strafe(-fMoveSpeed * _fDeltaTime);
	}

	// Gamepad movement
	int32_t iMoveX = 0;
	if (m_InputReceiver.Range("Move_X", iMoveX) && abs(iMoveX) > iGamepadDeadzone)
	{
		float fMove = iMoveX / 32768.0f;
		m_pCamera->Strafe(fMove * fMoveSpeed * _fDeltaTime);
	}
	int32_t iMoveY = 0;
	if (m_InputReceiver.Range("Move_Y", iMoveY) && abs(iMoveY) > iGamepadDeadzone)
	{
		float fMove = -iMoveY / 32768.0f;
		m_pCamera->Walk(fMove * fMoveSpeed * _fDeltaTime);
	}

	if (m_InputReceiver.Action("Reload_Level"))
	{
		HLOG("Reloading Level...");
		OnSceneBegin();
		return;
	}

	//if (m_InputReceiver.Action("RTV_WorldNormal"))
	//{
	//	m_pRevDemoView->SetRenderViewTexture(Display::RenderViewTexture_WorldNormal);
	//}
	//if (m_InputReceiver.Action("RTV_SSAO"))
	//{
	//	m_pRevDemoView->SetRenderViewTexture(Display::RenderViewTexture_SSAO);
	//}
	//if (m_InputReceiver.Action("RTV_None"))
	//{
	//	m_pRevDemoView->SetRenderViewTexture(Display::RenderViewTexture_None);
	//}

	//if (m_InputReceiver.Action("SSAO_Toggle"))
	//{
	//	m_pRevDemoView->GetSSAOPass()->EnableSSAO(!m_pRevDemoView->GetSSAOPass()->IsSSAOEnabled());
	//	HLOG("SSAO enabled: %u", m_pRevDemoView->GetSSAOPass()->IsSSAOEnabled());
	//}

	//const static float fRadiusDelta = 0.1f;
	//if (m_InputReceiver.Action("SSAO_Radius_Down"))
	//{
	//	m_pRevDemoView->GetSSAOPass()->SetSSAORadius(m_pRevDemoView->GetSSAOPass()->GetSSAORadius() - fRadiusDelta);
	//	HLOG("SSAORadius: %f", m_pRevDemoView->GetSSAOPass()->GetSSAORadius());
	//}
	//if (m_InputReceiver.Action("SSAO_Radius_Up"))
	//{
	//	m_pRevDemoView->GetSSAOPass()->SetSSAORadius(m_pRevDemoView->GetSSAOPass()->GetSSAORadius() + fRadiusDelta);
	//	HLOG("SSAORadius: %f", m_pRevDemoView->GetSSAOPass()->GetSSAORadius());
	//}

	//const static float fPowerDelta = 0.5f;
	//if (m_InputReceiver.Action("SSAO_Power_Down"))
	//{
	//	m_pRevDemoView->GetSSAOPass()->SetSSAOPower(m_pRevDemoView->GetSSAOPass()->GetSSAOPower() - fPowerDelta);
	//	HLOG("SSAOPower: %f", m_pRevDemoView->GetSSAOPass()->GetSSAOPower());
	//}
	//if (m_InputReceiver.Action("SSAO_Power_Up"))
	//{
	//	m_pRevDemoView->GetSSAOPass()->SetSSAOPower(m_pRevDemoView->GetSSAOPass()->GetSSAOPower() + fPowerDelta);
	//	HLOG("SSAOPower: %f", m_pRevDemoView->GetSSAOPass()->GetSSAOPower());
	//}

	// Orientation
	int32_t iLookX = 0;
	int32_t iLookY = 0;
	float fLookSpeed = 2.0f;

	if (m_InputReceiver.Mouse(iLookX, iLookY))
	{
		if (m_InputReceiver.State("Click"))
		{
			float fLookX = iLookX / 2.0f;
			float fLookY = iLookY / 2.0f;

			m_pCamera->RotateY(fLookX  * fLookSpeed * _fDeltaTime);
			m_pCamera->Pitch(fLookY * fLookSpeed * _fDeltaTime);
		}
	}
	// Gamepad looking
	iLookX = 0;
	if (m_InputReceiver.Range("Look_X", iLookX) && abs(iLookX) > iGamepadDeadzone)
	{
		float fMove = iLookX / 32768.0f;
		m_pCamera->RotateY(fMove * fLookSpeed * _fDeltaTime);
	}
	iLookY = 0;
	if (m_InputReceiver.Range("Look_Y", iLookY) && abs(iLookY) > iGamepadDeadzone)
	{
		float fMove = iLookY / 32768.0f;
		m_pCamera->Pitch(fMove * fLookSpeed * _fDeltaTime);
	}

	m_InputReceiver.Sync();
}
//---------------------------------------------------------------------------------------------------
bool TestScript::SpawnLights(const uint32_t& _uCount, const float3& _vPosition)
{
	m_Lights.clear();
	LightObject LightObj;

	const Math::float3 vRandomRangePos = { 6.0f, 1.0f, 6.0f };
	const Math::float3 vRandomRangeMoveAxis = { 4.0f, 1.0f, 4.0f };
	const Math::float3 vRandomRangeCol = { 0.2f, 0.2f, 0.2f };
		
	LightAttenuationDesc Attenuation;
	Attenuation.fConstantAttenuation = 0.0f;
	Attenuation.fLinearAttenuation = 0.3f;
	Attenuation.fQuadraticAttenuation = 0.4f;

	Math::float3 vPos = { 0,0,0 };
	Math::float3 vColor = { 0,0,0 };

	for (uint32_t i = 0; i < _uCount; ++i)
	{
		// Position
		float fRandom = static_cast<float>(std::rand()) * m_fInvRandMax;
		vPos.x = fRandom * vRandomRangePos.x - vRandomRangePos.x * 0.5f;
		fRandom = static_cast<float>(std::rand()) * m_fInvRandMax;
		vPos.y = fRandom * vRandomRangePos.y - vRandomRangePos.y * 0.5f;
		fRandom = static_cast<float>(std::rand()) * m_fInvRandMax;
		vPos.z = fRandom * vRandomRangePos.z - vRandomRangePos.z * 0.5f;

		vPos += _vPosition;

		// Move Axis
		fRandom = static_cast<float>(std::rand()) * m_fInvRandMax;
		LightObj.vMoveAxis.x = fRandom * vRandomRangeMoveAxis.x - vRandomRangeMoveAxis.x * 0.5f;
		fRandom = static_cast<float>(std::rand()) * m_fInvRandMax;
		LightObj.vMoveAxis.y = fRandom * vRandomRangeMoveAxis.y - vRandomRangeMoveAxis.y * 0.5f;
		fRandom = static_cast<float>(std::rand()) * m_fInvRandMax;
		LightObj.vMoveAxis.z = fRandom * vRandomRangeMoveAxis.z - vRandomRangeMoveAxis.z * 0.5f;

		std::string sName = "PointLight" + std::to_string(LightContext::Instance()->GetPointLights().size());
		GameObject* pGO = m_Level.SpawnGameObject(sName, vPos, Math::float3(1, 1, 1), HMFLOAT3_ZERO, "", "", false, Datastructures::GameObjectFlag::NoCollision);
		if (pGO == nullptr)
		{
			return false;
		}
		LightObj.pObject = pGO;
		m_Lights.push_back(LightObj);

		// Color
		fRandom = static_cast<float>(std::rand()) * m_fInvRandMax;
		vColor.x = fRandom * vRandomRangeCol.x;
		fRandom = static_cast<float>(std::rand()) * m_fInvRandMax;
		vColor.y = fRandom * vRandomRangeCol.y;
		fRandom = static_cast<float>(std::rand()) * m_fInvRandMax;
		vColor.z = fRandom * vRandomRangeCol.z;

		LightContext::Instance()->AddLight(std::move(PointLight(pGO, vColor, 0.0f, Attenuation)));
	}

	HLOG("Spawned %u lights!", _uCount);

	return true;
}
//---------------------------------------------------------------------------------------------------
