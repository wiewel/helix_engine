//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef TESTSCRIPT_H
#define TESTSCRIPT_H

#include "Async\ScriptObject.h"
#include "Scene\Level.h"
#include "Input\InputReciever.h"
#include "RevDemoView.h"
#include "Collision\CollisionSystem.h"

namespace SSAO
{
	class TestScript : public Helix::Async::IScriptObject
	{
		struct LightObject
		{
			Helix::Datastructures::GameObject* pObject;
			Helix::Math::float3 vMoveAxis;
		};

	public:
		TestScript();
		~TestScript();
		void SetCamera(std::shared_ptr<Helix::Scene::Camera> _pCamera);
		void SetView(Helix::Display::RevDemoView* _pRevDemoView);
	private:
		void OnSceneBegin();
		void OnSceneEnd();
		void OnUpdate(double _fDeltaTime, double _fTotalTime);
		bool SpawnLights(const uint32_t& _uCount, const Helix::Math::float3& _vPosition);
	private:
		bool m_bResetCameraOnLevelLoad = true;
		Helix::Input::InputReciever m_InputReceiver;
		Helix::Scene::Level m_Level;
		std::shared_ptr<Helix::Scene::Camera> m_pCamera = nullptr;
		Helix::Display::RevDemoView* m_pRevDemoView = nullptr;

		std::vector<LightObject> m_Lights;
		std::vector<Helix::Scene::PointLight> m_AngelLights;

		const float m_fInvRandMax = 1.0f / static_cast<float>(RAND_MAX);
	};
}

#endif // TESTSCRIPT_H