//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "RevDemoView.h"
#include "SimpleLevelLoaderScript.h"
#include "resource.h"

using namespace Helix::Display;
using namespace Helix::Datastructures;

RevDemoView::RevDemoView()
{
}
//---------------------------------------------------------------------------------------------------
RevDemoView::~RevDemoView()
{
}
//---------------------------------------------------------------------------------------------------
bool RevDemoView::OnInitialize(HWND _hWnd)
{
	m_Script.SetHWND(_hWnd);
	return true;
}
//---------------------------------------------------------------------------------------------------
void RevDemoView::OnShutdown()
{
	
}
//---------------------------------------------------------------------------------------------------
void RevDemoView::InitializeAssets()
{

}
//---------------------------------------------------------------------------------------------------
