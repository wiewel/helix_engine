//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "main.h"
#include "hlx\src\StandardDefines.h"

#include "Engine\EngineInitializer.h"
#include "Display\DX11\DeferredRendererDX11.h"

using namespace Helix;
using namespace Helix::Display;

int WINAPI WinMain(HINSTANCE _hInstance, HINSTANCE _hPrevInstance, LPSTR _lpCmdLine, int _iCmdShow)
{
	HInitDebug();

	RevDemoView RevDemo;
	RevDemo.SetLevel(_lpCmdLine);

	View view (&RevDemo);
	const char* sGameTitle = "Monochromacy";
	HWND hwnd = view.Initialize(_hInstance, _iCmdShow, sGameTitle);
	if(hwnd != 0)
	{
		
		Display::DeferredRendererDX11 m_DeferredRenderer;
		Engine::EngineInitializer m_Initializer(RevDemo);

		if (m_Initializer.Initialize(S("RevDemo15"), hwnd))
		{
			//m_DeferredRenderer.SetDebugMode(true);
			bool bNoRendering = m_DeferredRenderer.Initialize() == false;
			// if renderer init fails, renderscene wont be invoked
			if (m_Initializer.Start(false, bNoRendering))
			{
				view.Run();	
			}
		}
	}

	view.Shutdown();

    return S_OK;
}
