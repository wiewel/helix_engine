//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef SIMPLELEVELLOADERSCRIPT_H
#define SIMPLELEVELLOADERSCRIPT_H

#include "Scene\ScriptObject.h"
#include "Scene\Level.h"

namespace Helix
{
	class SimpleLevelLoaderScript : public Helix::Scene::IScriptObject
	{
	public:
		SimpleLevelLoaderScript();
		~SimpleLevelLoaderScript();
		void SetLevel(const std::string& _sLevel);

		void SetHWND(HWND _hHWND);
	private:
		void OnSceneBegin() final;
		void OnSceneEnd() final;
		void OnUpdate(double _fDeltaTime, double _fTotalTime) final;

	private:
		std::string m_sLevelName;
		Helix::Scene::Level m_Level;
		HWND m_hWND = 0;
	};

	inline void SimpleLevelLoaderScript::SetLevel(const std::string& _sLevel) { m_sLevelName = _sLevel; }
	inline void SimpleLevelLoaderScript::SetHWND(HWND _hHWND)
	{
		m_hWND = _hHWND;
	}
}

#endif // SIMPLELEVELLOADERSCRIPT_H