//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher, David Labode
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef TESTCHAMBERSCRIPT_H
#define TESTCHAMBERSCRIPT_H

#include "Async\ScriptObject.h"
#include "Scene\Level.h"
#include "Input\InputReciever.h"
#include "RevDemoView.h"

#include <stack>

namespace EddyFlux 
{
	class TestChamberScript : public Helix::Async::IScriptObject
	{
	public:
		TestChamberScript(const std::string& _sLevelName);
		~TestChamberScript();
		void SetCamera(std::shared_ptr<Helix::Scene::Camera> _pCamera);
		void SetView(Helix::Display::RevDemoView* _pRevDemoView);
	private:
		void OnSceneBegin();
		void OnSceneEnd();
		void OnUpdate(double _fDeltaTime, double _fTotalTime);
		void SpawnGravityPlacementHelper(const float _fStrength);
		void SpawnMagnet(const Helix::AMP::float3& _vPosition, const float _fStrength);
		void RemoveSelectedMagnet();
		void ResetAllGravityParticles();
		void IncrementGravityStrength(float _fIncrement);
		void IncrementGravityRadius(float _fIncrement);
		void StartParticleSystem();
		void SelectMagnet();

		Helix::AMP::float3 CalculateSize(float _fStrength) const;
	private:
		std::vector<std::string> m_LevelList;
		uint32_t m_uCurrentLevel = 0;

		Helix::Collision::OBB m_GoalOBB = {};
		Helix::Datastructures::GameObject* m_pFluxLight = nullptr;
		// HACK
		bool m_bFluxCompensatorLaunched = false;

		Helix::Datastructures::GameObject* m_pFluxCompensator = nullptr;
		Helix::Display::RevDemoView* m_pRevDemoView = nullptr;
		// ENDHACK
		Helix::Datastructures::GameObject* m_pGravityDemoParticle = nullptr;

		//cache materials
		Helix::Display::MaterialDX11 m_FluxCompMat = nullptr;
		Helix::Display::MeshClusterDX11 m_FluxCompMesh = nullptr;

		Helix::Display::MaterialDX11 m_FluxMagnetMat = nullptr;
		Helix::Display::MeshClusterDX11 m_FluxMagnetMesh = nullptr;

		bool m_bGravityStrength = true;
		bool m_bGridMode = true;
		bool m_bEditMode = true;
		Helix::AMP::float2 m_vCameraSmoothedGoal;
		Helix::AMP::float2 m_vCameraSmoothedVelocity;
		uint32_t m_uMouseSmoothness = 2; // num frames over which mouse input is smoothed
		float m_fLookSpeed = 0.8f;
		float m_fMoveSpeed = 2.3f;
		int32_t m_iGamepadDeadzone = 24000;

		bool m_bResetCameraOnLevelLoad = true;
		Helix::Input::InputReciever m_InputReceiver;
		Helix::Scene::Level m_Level;
		std::shared_ptr<Helix::Scene::Camera> m_pCamera = nullptr;

		Helix::AMP::float3 m_vMinSize = Helix::AMP::float3(0.5f, 0.5f, 0.5f);
		Helix::AMP::float3 m_vMaxSize = Helix::AMP::float3(1.0f, 1.0f, 1.0f);
		float m_fMinStrength = 0.01f;
		float m_fMaxStrength = 0.1f;
		float m_fCurrentStrength = m_fMinStrength;

		std::vector<Helix::Datastructures::GameObject*> m_GravityMagnets;
		Helix::Datastructures::GameObject* m_pSelectedMagnet = nullptr;
	};
}
#endif // TESTCHAMBERSCRIPT_H