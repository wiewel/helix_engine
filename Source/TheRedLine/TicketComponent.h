//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef TICKETCOMPONENT_H
#define TICKETCOMPONENT_H

#include "DataStructures\GameObjectComponent.h"
#include "DataStructures\ShapeComponent.h"
#include "DataStructures\TransformHierarchyComponent.h"
#include "Scene\RotationComponent.h"
#include "Sound\Speaker.h"
#include "PlayerScript.h"

namespace RedLine
{
	struct TicketComponentDesc
	{
		Helix::Math::transform vInitialPos;
		Helix::Math::transform vCarryTransform;
		Helix::Math::float3 vScale = { 0.35f,0.35f,0.35f };
		std::string sAquireSound = "dramatic_violin";
	};

	class TicketComponent : public Helix::Datastructures::IGameObjectComponent
	{
	public:
		TicketComponent(Helix::Datastructures::GameObject* _pParent, const TicketComponentDesc& _Desc);

		COMPTYPENAME(TicketComponent, Helix::Datastructures::GameObject, 'Tick');
		COMPDESCONSTR(TicketComponent, Helix::Datastructures::GameObject, IGameObjectComponent);

		//void OnLevelLoaded() final;
		//void OnUpdate(double _fDeltaTime, double _fElapsedTime) final;

		void UseTicket();
		void CarryTicket();

		void Reset();
		
	private:
		TicketComponentDesc m_Desc;
		Helix::Sound::Speaker* m_pSpeaker = nullptr;
		Helix::Datastructures::ShapeComponent* m_pTriggerShape = nullptr;
		Helix::Datastructures::TransformHierarchyComponent* m_pTHC = nullptr;

		bool m_bUsed = false;
		bool m_bCarrying = false;
	private:
		static void TicketTriggered(Helix::Physics::TriggerInfo& _Info);
	};

	inline TicketComponent::TicketComponent(Helix::Datastructures::GameObject* _pParent, const TicketComponentDesc& _Desc) :
		Helix::Datastructures::IGameObjectComponent(_pParent), m_Desc(_Desc)
	{
		//remove other shapes
		for (auto* pShape : GetParent()->GetComponentsByType<Helix::Datastructures::ShapeComponent>())
		{
			if (pShape->GetUsage() != Helix::Physics::ShapeUsageType_Query)
			{
				GetParent()->RemoveComponent(pShape);
			}
		}

		Helix::Physics::GeometryDesc Desc;
		Desc.kType = Helix::Physics::GeometryType_Box;
		Desc.Box.vHalfExtents = { 1.5f, 1.5f, 1.5f};
		Desc.kUsage = Helix::Physics::ShapeUsageType_Trigger;
		Desc.bScaleWithParent = false;
		m_pTriggerShape = _pParent->AddComponent<Helix::Datastructures::ShapeComponent>(Desc);

		m_pTriggerShape->RegisterCallback(&TicketComponent::TicketTriggered);

		m_pSpeaker = _pParent->AddComponent<Helix::Sound::Speaker>();
		m_pSpeaker->AddVoice(STR(_Desc.sAquireSound));

		m_pTHC = _pParent->AddComponent<Helix::Datastructures::TransformHierarchyComponent>();

		GetParent()->SetName("ticket");
	}

	inline TicketComponent::~TicketComponent()
	{
	}

	//inline void TicketComponent::OnLevelLoaded()
	//{
	//}

	//inline void TicketComponent::OnUpdate(double _fDeltaTime, double _fElapsedTime)
	//{
	//}

	inline void TicketComponent::UseTicket()
	{
		if (m_bUsed || m_bCarrying == false)
			return;

		HLOG("Used ticket");
		m_bUsed = true;
		if (m_pTHC != nullptr)
		{
			m_pTHC->SetParentNode(nullptr);
		}

		GetParent()->SetFlags(Helix::Datastructures::GameObjectFlag_Invisible);
		GetParent()->Transform = m_Desc.vInitialPos;

		GameScript::Instance().GateOpened();
	}

	inline void TicketComponent::CarryTicket()
	{
		if (m_bUsed || m_bCarrying)
			return;

		HLOG("Aquired ticked");

		if (m_pSpeaker != nullptr && m_Desc.sAquireSound.empty() == false)
		{
			m_pSpeaker->Start(CSTR(m_Desc.sAquireSound));
		}

		m_bCarrying = true;
		GetParent()->ClearFlags(Helix::Datastructures::GameObjectFlag_Invisible);

		Helix::Scene::CameraComponent* pCameraComponent = Helix::Scene::CameraManager::Instance()->GetCameraByName("MainCamera");
		HASSERT(pCameraComponent != nullptr, "There is no main camera in the scene");
		
		// carry ticket
		Helix::Datastructures::TransformHierarchyComponent* pCameraTH = pCameraComponent->GetParent()->GetComponent<Helix::Datastructures::TransformHierarchyComponent>();
		if (pCameraTH != nullptr && m_pTHC != nullptr)
		{
			m_pTHC->SetLocalTransform(m_Desc.vCarryTransform);
			m_pTHC->SetParentNode(pCameraTH);
		}

		GetParent()->RenderingScale = m_Desc.vScale;

		// disable rotation components if any
		for (auto* pRotComp : GetParent()->GetComponentsByType<Helix::Scene::RotationComponent>())
		{
			pRotComp->SetPaused(true);
		}
	}

	inline void TicketComponent::Reset()
	{
		m_bUsed = false;
		m_bCarrying = false;

		PlayerScript::Instance().SetStampInRange(false);
		PlayerScript::Instance().SetTicketInRange(nullptr);

		if (m_pTHC != nullptr)
		{
			m_pTHC->SetParentNode(nullptr);
		}

		Helix::Datastructures::GameObject* pParent = GetParent();
		pParent->Transform = m_Desc.vInitialPos;
		pParent->ClearFlags(Helix::Datastructures::GameObjectFlag_Invisible);
		pParent->RenderingScale = HFLOAT3_ONE;

		for (auto* pRotComp : GetParent()->GetComponentsByType<Helix::Scene::RotationComponent>())
		{
			pRotComp->SetPaused(false);
		}
	}

	inline void TicketComponent::TicketTriggered(Helix::Physics::TriggerInfo& _Info)
	{
		if (_Info.pActivator->GetName() == "MainCamera")
		{
			if (_Info.State == Helix::Physics::SimulationEventState_Enter)
			{
				HLOG("Ticket in range");
				PlayerScript::Instance().SetTicketInRange(_Info.pTrigger);
			}
			else if (_Info.State == Helix::Physics::SimulationEventState_Leave)
			{
				PlayerScript::Instance().SetTicketInRange(nullptr);
			}
		}
		else if (_Info.pActivator->GetName() == "stamp_machine")
		{
			if (_Info.State == Helix::Physics::SimulationEventState_Enter)
			{
				HLOG("Stamp in range");
				PlayerScript::Instance().SetStampInRange(true);
			}
			else if (_Info.State == Helix::Physics::SimulationEventState_Leave)
			{
				PlayerScript::Instance().SetStampInRange(false);
			}
		}
	}
}

#endif // !TICKETCOMPONENT_H
