//   Copyright 2020 Steffen Wiewel, Fabian Wahlster, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "GameScript.h"
#include "PlayerScript.h"
#include "StatueComponent.h"
#include "TicketComponent.h"
#include "LeverComponent.h"
#include "SolidEffectRenderPass.h"

using namespace RedLine;
using namespace Helix;
STATICSCRIPT_CPP(GameScript)

//---------------------------------------------------------------------------------------------------
RedLine::GameScript::GameScript()
{
}
//---------------------------------------------------------------------------------------------------
RedLine::GameScript::~GameScript()
{
}
//---------------------------------------------------------------------------------------------------
void RedLine::GameScript::OnLevelLoaded()
{
	// get the primary gate and open it
	Datastructures::GameObject* pGate = Datastructures::GameObjectPool::Instance()->GetObjectByName(m_GameObjectNames.sRollerDoor);
	if (pGate != nullptr)
	{
		GateComponentDesc Desc;
		Desc.vClosePosition = pGate->GetPosition();
		Desc.vOpenPosition = Desc.vClosePosition;
		Desc.vOpenPosition.y += 3.0f;
		Desc.fSpeed = 1.0f;
		m_pPrimaryGate = pGate->AddComponent<GateComponent>(Desc);
	}

	// get the secondary gate and open it
	pGate = Datastructures::GameObjectPool::Instance()->GetObjectByName(m_GameObjectNames.sRollerDoorHall);
	if (pGate != nullptr)
	{
		GateComponentDesc Desc;
		Desc.vClosePosition = pGate->GetPosition();
		Desc.vOpenPosition = Desc.vClosePosition;
		Desc.vOpenPosition.y += 3.0f;
		Desc.fSpeed = 1.0f;
		m_pHallGate = pGate->AddComponent<GateComponent>(Desc);
	}

	// get the secondary gate and open it
	pGate = Datastructures::GameObjectPool::Instance()->GetObjectByName(m_GameObjectNames.sRollerDoorTicket);
	if (pGate != nullptr)
	{
		GateComponentDesc Desc;
		Desc.vClosePosition = pGate->GetPosition();
		Desc.vOpenPosition = Desc.vClosePosition;
		Desc.vOpenPosition.y += 3.0f;
		Desc.fSpeed = 1.0f;
		m_pTicketGate = pGate->AddComponent<GateComponent>(Desc);
	}

	// create the trigger which closes the gate
	Datastructures::GameObject* pGateTrigger = Datastructures::GameObjectPool::Instance()->GetObjectByName(m_GameObjectNames.sRollerDoorTrigger);
	if (pGateTrigger != nullptr)
	{
		Datastructures::ShapeComponent* pGateTriggerShape = pGateTrigger->GetComponent<Datastructures::ShapeComponent>();
		pGateTriggerShape->RegisterCallback(&GameScript::OnTrigger);
	}

	Datastructures::GameObjectPool::Instance()->GetGameObjectsByTag("light_normal", m_NormalLights);
	Datastructures::GameObjectPool::Instance()->GetGameObjectsByTag("light_emergency", m_EmergencyLights);
	Datastructures::GameObjectPool::Instance()->GetGameObjectsByTag("light_normal_lamp", m_LightModels);
	SetAllEmissiveEnabled(m_LightModels, false);

	m_pEmergencyLever = Datastructures::GameObjectPool::Instance()->GetObjectByName(m_GameObjectNames.sEmergencyLever);

	//GateComponent* pEmergencyLeverGate = m_pEmergencyLever->AddComponent()

	Datastructures::GameObject* pPlayer = PlayerScript::Instance().GetPlayer();
	if (pPlayer != nullptr)
	{
		m_pPlayerSpeaker = pPlayer->GetComponent<Sound::Speaker>();
	}

	Datastructures::GameObject* pTicket = Datastructures::GameObjectPool::Instance()->GetObjectByName(m_GameObjectNames.sTicket);
	if (pTicket != nullptr)
	{
		TicketComponentDesc Desc;
		Desc.vInitialPos = pTicket->Transform;
		Desc.vCarryTransform.p = { 0.25f, 0.0f, 1.f };
		Desc.vCarryTransform.q = Math::QuaternionFromEulerDegreesXYZ(-40.f, 75.f, 0.f);
		pTicket->AddComponent<TicketComponent>(Desc);
	}

	m_pEmergencyLever = Datastructures::GameObjectPool::Instance()->GetObjectByName(m_GameObjectNames.sEmergencyLever);

	Datastructures::GameObjectPool::Instance()->GetGameObjectsByTag("statue", m_Statues);

	for (Datastructures::GameObject* pLight : m_EmergencyLights)
	{
		Scene::LightComponent* pLightComp = pLight->GetComponent<Scene::LightComponent>();
		if (pLightComp != nullptr)
		{
			m_LightIntensities[pLightComp] = pLightComp->GetIntensity();
		}
	}
	for (Datastructures::GameObject* pLight : m_NormalLights)
	{
		Scene::LightComponent* pLightComp = pLight->GetComponent<Scene::LightComponent>();
		if (pLightComp != nullptr)
		{
			m_LightIntensities[pLightComp] = pLightComp->GetIntensity();
		}
	}
	m_pGameOverTrigger = Datastructures::GameObjectPool::Instance()->GetObjectByName("gameover_trigger");
	if (m_pGameOverTrigger != nullptr)
	{
		Datastructures::ShapeComponent* pShape = m_pGameOverTrigger->GetComponent<Datastructures::ShapeComponent>();
		pShape->RegisterCallback(OnTrigger);
	}
}
//---------------------------------------------------------------------------------------------------
void RedLine::GameScript::OnSceneBegin()
{
}
//---------------------------------------------------------------------------------------------------
void RedLine::GameScript::OnSceneEnd()
{
}
//---------------------------------------------------------------------------------------------------
void RedLine::GameScript::OnUpdate(double _fDeltaTime, double _fElapsedTime)
{
	if (m_State == GameState_None)
	{
		Datastructures::GameObject* pPlayer = PlayerScript::Instance().GetPlayer();
		if (pPlayer != nullptr)
		{
			m_pPlayerSpeaker = pPlayer->GetComponent<Sound::Speaker>();
			if (m_pPlayerSpeaker != nullptr)
			{
				m_pPlayerSpeaker->AddVoice(m_GameSounds.sMusicStart, true);
				m_pPlayerSpeaker->AddVoice(m_GameSounds.sMusicMain, true);
				m_pPlayerSpeaker->AddVoice(m_GameSounds.sGameEvent);
				m_pPlayerSpeaker->AddVoice(m_GameSounds.sEndstation);
				m_pPlayerSpeaker->SetVolume(m_GameSounds.sEndstation, 2.0f);
				m_pPlayerSpeaker->AddVoice(m_GameSounds.sDeath);
			}
		}

		Started();
	}

	if (m_State == GameState_Dead)
	{
		m_fDeadTime += _fDeltaTime;
		if (m_fDeadTime >= m_GameSettings.fDeadTime)
		{
			m_fDeadTime = 0.0f;
			Started();
		}
	}

	if (m_State == GameState_Success)
	{
		m_fSuccessTime += _fDeltaTime;
		if (m_fSuccessTime >= m_GameSettings.fSuccessTime)
		{
			m_fDeadTime = 0.0f;
			Started();
		}

		if (m_bTriggeredSuccess)
		{
			for (Datastructures::GameObject* pStatue : m_Statues)
			{
				StatueComponent* pStatueComponent = pStatue->GetComponent<StatueComponent>();
				if (pStatueComponent != nullptr)
				{
					pStatueComponent->SetActive(false);
				}
			}
			m_bTriggeredSuccess = false;
		}
	}

	// fade out lights
	if (m_bFadeOutLight)
	{
		m_fLightIntensity -= _fDeltaTime * m_fFadeSpeed;
		if (m_fLightIntensity < 0.0f)
		{
			m_fLightIntensity = 0.0f;
			m_bFadeOutLight = false;
			m_bLightsOn = false;
		}
		SetAllLightsIntensity(m_NormalLights, m_fLightIntensity);
		SetAllLightsIntensity(m_EmergencyLights, m_fLightIntensity);
	}
	else if (m_bFadeInLight)
	{
		m_fLightIntensity += _fDeltaTime * m_fFadeSpeed;
		if (m_fLightIntensity > 1.0f)
		{
			m_fLightIntensity = 1.0f;
			m_bFadeInLight = false;
			m_bLightsOn = true;
		}
		SetAllLightsIntensity(m_NormalLights, m_fLightIntensity);
		SetAllLightsIntensity(m_EmergencyLights, m_fLightIntensity);
	}
}
//---------------------------------------------------------------------------------------------------
void RedLine::GameScript::OnTrigger(Physics::TriggerInfo& _Info)
{
	if (_Info.pActivator->GetName() == "MainCamera" && _Info.State == Physics::SimulationEventState_Enter)
	{
		if (_Info.pTrigger->GetName() == "rollerdoor_01_trigger" && GameScript::Instance().GetState() == GameState_Started)
		{
			Datastructures::GameObject* pEmergencyLever = Datastructures::GameObjectPool::Instance()->GetObjectByName("Emergency_Lever");
			// deactivate emergency lever
			if (pEmergencyLever != nullptr)
			{
				LeverComponent* pLever = pEmergencyLever->GetComponent<LeverComponent>();
				if (pLever != nullptr)
				{
					pLever->Activate();
					pLever->SwitchOn();
				}
			}
		}
		if (_Info.pTrigger->GetName() == "gameover_trigger" && GameScript::Instance().GetState() == GameState_GateOpened)
		{
			GameScript::Instance().Success();
		}
	}

}
//---------------------------------------------------------------------------------------------------
void RedLine::GameScript::Started()
{
	m_State = GameState_Started;

	if (m_pPrimaryGate != nullptr)
	{
		m_pPrimaryGate->OpenGate();
	}

	if (m_pHallGate != nullptr)
	{
		m_pHallGate->CloseGate();
	}

	if (m_pTicketGate != nullptr)
	{
		m_pTicketGate->CloseGate();
	}

	Display::SolidEffectRenderPass::bDestroyEverything = false;

	// play game music
	if (m_pPlayerSpeaker != nullptr)
	{
		m_pPlayerSpeaker->Stop(m_GameSounds.sMusicMain);
		m_pPlayerSpeaker->Start(m_GameSounds.sMusicStart);
	}

	PlayerScript::Instance().ResetButtons();

	PlayerScript::Instance().SpawnPlayer();

	// deactivate emergency lighting
	SetAllLightsEnabled(m_EmergencyLights, false);

	// activate normal lighting
	SetAllLightsEnabled(m_NormalLights, true);

	// activate lamps
	SetAllEmissiveEnabled(m_LightModels, true);

	// activate fade in for lights
	m_fLightIntensity = 0.0f;
	SetAllLightsIntensity(m_NormalLights, m_fLightIntensity);
	SetAllLightsIntensity(m_EmergencyLights, m_fLightIntensity);
	m_bFadeInLight = true;
	m_bFadeOutLight = false;

	// reset ticket
	Datastructures::GameObject* pTicket = Datastructures::GameObjectPool::Instance()->GetObjectByName(m_GameObjectNames.sTicket);
	if (pTicket != nullptr)
	{
		TicketComponent* pTicketComponent = pTicket->GetComponent<TicketComponent>();
		if (pTicketComponent != nullptr)
		{
			pTicketComponent->Reset();
		}
	}

	// deactivate emergency lever
	if (m_pEmergencyLever != nullptr)
	{
		LeverComponent* pLever = m_pEmergencyLever->GetComponent<LeverComponent>();
		if (pLever != nullptr)
		{
			pLever->Deactivate();
		}
	}

	// deactivate statues and reset
	for (Datastructures::GameObject* pStatue : m_Statues)
	{
		StatueComponent* pStatueComponent = pStatue->GetComponent<StatueComponent>();
		if (pStatueComponent != nullptr)
		{
			pStatueComponent->SetActive(false);
			pStatueComponent->Reset();
		}
	}
}
//---------------------------------------------------------------------------------------------------
void RedLine::GameScript::GateClosed()
{
	m_State = GameState_GateClosed;

	if(m_pPrimaryGate != nullptr)
	{
		m_pPrimaryGate->CloseGate();
	}

	if (m_pHallGate != nullptr)
	{
		m_pHallGate->CloseGate();
	}

	// Deactivate normal lighting
	SetAllLightsEnabled(m_NormalLights, false);

	// Activate emergency lighting
	SetAllLightsEnabled(m_EmergencyLights, true);

	// Deactivate all lamps
	SetAllEmissiveEnabled(m_LightModels, false);

	if (m_pPlayerSpeaker != nullptr)
	{
		m_pPlayerSpeaker->Start(S("game_event"));
		m_pPlayerSpeaker->Stop(m_GameSounds.sMusicStart);
		m_pPlayerSpeaker->Start(m_GameSounds.sMusicMain);
	}
}
//---------------------------------------------------------------------------------------------------
void RedLine::GameScript::EmergencyStopped()
{
	m_State = GameState_EmergencyStopped;

	if (m_pHallGate != nullptr)
	{
		m_pHallGate->OpenGate();
	}

	// play sound for major game event
	if (m_pPlayerSpeaker != nullptr)
	{
		m_pPlayerSpeaker->Start(S("game_event"));
	}

	// deactivate emergency lighting
	SetAllLightsEnabled(m_EmergencyLights, false);

	// activate normal lighting
	SetAllLightsEnabled(m_NormalLights, true);

	// activate all lamps
	SetAllEmissiveEnabled(m_LightModels, true);

	// activate statues
	for (Datastructures::GameObject* pStatue : m_Statues)
	{
		StatueComponent* pStatueComponent = pStatue->GetComponent<StatueComponent>();
		if (pStatueComponent != nullptr)
		{
			pStatueComponent->SetActive(true);
		}
	}

	// deactivate emergency lever
	if (m_pEmergencyLever != nullptr)
	{
		LeverComponent* pLever = m_pEmergencyLever->GetComponent<LeverComponent>();
		if (pLever != nullptr)
		{
			pLever->Deactivate();
		}
	}
}
//---------------------------------------------------------------------------------------------------
void RedLine::GameScript::PassageOpened()
{
	m_State = GameState_PassageOpened;

	// play sound for major game event
	if (m_pPlayerSpeaker != nullptr)
	{
		m_pPlayerSpeaker->Start(S("game_event"));
	}

	// open passage gate
	if (m_pTicketGate != nullptr)
	{
		m_pTicketGate->OpenGate();
	}
}
//---------------------------------------------------------------------------------------------------
void RedLine::GameScript::GateOpened()
{
	m_State = GameState_GateOpened;

	// play sound for major game event
	if (m_pPlayerSpeaker != nullptr)
	{
		m_pPlayerSpeaker->Start(S("game_event"));
	}

	// TODO: activate train

	// open the gate to the trains
	if (m_pPrimaryGate != nullptr)
	{
		m_pPrimaryGate->OpenGate();
	}
}
//---------------------------------------------------------------------------------------------------
void RedLine::GameScript::Success()
{
	m_State = GameState_Success;
	
	// play sound for major game event
	if (m_pPlayerSpeaker != nullptr)
	{
		m_pPlayerSpeaker->Start(m_GameSounds.sEndstation);
		m_pPlayerSpeaker->Stop(m_GameSounds.sMusicStart);
	}

	m_bFadeInLight = false;
	m_bFadeOutLight = true;

	Display::SolidEffectRenderPass::bDestroyEverything = true;

	// deactivate statues
	m_bTriggeredSuccess = true;
	// TODO: show success sign
}
//---------------------------------------------------------------------------------------------------
void RedLine::GameScript::Dead()
{
	m_State = GameState_Dead;
	// reset game

	m_bFadeInLight = false;
	m_bFadeOutLight = true;

	// play sound for major game event
	if (m_pPlayerSpeaker != nullptr)
	{
		m_pPlayerSpeaker->Start(m_GameSounds.sDeath);
	}

	// TODO: deactivate train
}
//---------------------------------------------------------------------------------------------------
void RedLine::GameScript::SetAllLightsEnabled(GameObjectVector& _Vector, bool _bEnabled)
{
	for (auto pGO : _Vector)
	{
		Scene::LightComponent* pLight = pGO->GetComponent<Scene::LightComponent>();
		if (pLight != nullptr)
		{
			pLight->SetEnabled(_bEnabled);
		}
	}
}
//---------------------------------------------------------------------------------------------------
void RedLine::GameScript::SetAllLightsIntensity(GameObjectVector & _Vector, float _fIntensity)
{
	for (auto pGO : _Vector)
	{
		Scene::LightComponent* pLight = pGO->GetComponent<Scene::LightComponent>();
		if (pLight != nullptr)
		{
			float fIntensity = m_LightIntensities[pLight] * _fIntensity;
			pLight->SetIntensity(fIntensity);
		}
	}
}
//---------------------------------------------------------------------------------------------------
void RedLine::GameScript::SetAllEmissiveEnabled(GameObjectVector & _Vector, bool _bActive)
{
	for (auto pGO : _Vector)
	{
		std::vector<Display::MaterialDX11>& Materials = pGO->GetMaterials();
		if (_bActive == false)
		{
			//m_LightModelsEmissiveColor[pGO] = Math::float3(0,0,0);
			for (auto& Mat : Materials)
			{
				Math::float3 vColor = Mat.GetProperties().vEmissiveColor;
				if (vColor.isZero() == false)
				{
					m_LightModelsEmissiveColor[pGO] = vColor;
					Mat.GetProperties().vEmissiveColor = Math::float3(0.1f, 0, 0);
				}
			}
		}
		else
		{
			for (auto& Mat : Materials)
			{
				Math::float3 vColor = Mat.GetProperties().vEmissiveColor;
				if (vColor.isZero() == false)
				{
					Mat.GetProperties().vEmissiveColor = m_LightModelsEmissiveColor[pGO];
				}
			}
		}
	}
}
