//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef GAMESCRIPT_H
#define GAMESCRIPT_H

#include "Scene\ScriptObject.h"
#include "DataStructures\GameObject.h"
#include "GateComponent.h"
#include "DataStructures\ShapeComponent.h"
#include "Scene\LightComponent.h"

namespace RedLine
{
	struct GameSettings
	{
		float fDeadTime = 3.0f;
		float fSuccessTime = 5.0f;
	};
	struct GameObjectNames
	{
		std::string sRollerDoor = "rollerdoor_01";
		std::string sRollerDoorTrigger = "rollerdoor_01_trigger";
		std::string sEmergencyLever = "Emergency_Lever";
		std::string sTicket = "ticket";
		std::string sTicketMachine = "ticket_machine";
		std::string sStampMachine = "stamp_machine";
		std::string sRollerDoorHall = "rollerdoor_small_tohall";
		std::string sRollerDoorTicket = "rollerdoor_small_toticket";
	};
	struct GameSounds
	{
		hlx::string sGameEvent = S("game_event");
		hlx::string sMusicStart = S("music_start");
		hlx::string sMusicMain = S("gnossienne_no_3");
		hlx::string sEndstation = S("endstation");
		hlx::string sDeath = S("die");
	};

	class GameScript : public Helix::Scene::IScriptObject
	{
	public:
		using GameObjectVector = std::vector<Helix::Datastructures::GameObject*>;
		enum GameState : uint32_t
		{
			GameState_None,
			GameState_Started,
			GameState_GateClosed,
			GameState_EmergencyStopped,
			GameState_PassageOpened,
			GameState_GateOpened,
			GameState_Success,
			GameState_Dead,
		};

		GameScript();
		~GameScript();

		STATICSCRIPT(GameScript)

		void OnLevelLoaded() final;
		void OnSceneBegin() final;
		void OnSceneEnd() final;
		void OnUpdate(double _fDeltaTime, double _fElapsedTime) final;

		static void OnTrigger(Helix::Physics::TriggerInfo& _Info);
		GameState GetState() const;

		// functions to change game state
		void Started();
		void GateClosed();
		void EmergencyStopped();
		void PassageOpened();
		void GateOpened();
		void Success();
		void Dead();

		const GameSounds& GetGameSounds() const;

	private:
		void SetAllLightsEnabled(GameObjectVector& _Vector, bool _bActive);
		void SetAllLightsIntensity(GameObjectVector& _Vector, float _fIntensity);
		void SetAllEmissiveEnabled(GameObjectVector& _Vector, bool _bActive);

	private:
		GameState m_State = GameState_None;

		GameSettings m_GameSettings;
		GameObjectNames m_GameObjectNames;
		GameSounds m_GameSounds;

		GateComponent* m_pPrimaryGate = nullptr;
		GateComponent* m_pHallGate = nullptr;
		GateComponent* m_pTicketGate = nullptr;

		GameObjectVector m_Statues;
		Helix::Datastructures::GameObject* m_pEmergencyLever = nullptr;
		Helix::Sound::Speaker* m_pPlayerSpeaker = nullptr;
		float m_fDeadTime = 0.0f;
		float m_fSuccessTime = 0.0f;
		bool m_bTriggeredSuccess = false;
		Helix::Datastructures::GameObject* m_pStampMachine = nullptr;
		Helix::Datastructures::GameObject* m_pTicket = nullptr;
		Helix::Datastructures::GameObject* m_pTicketMachine = nullptr;
		Helix::Datastructures::GameObject* m_pGameOverTrigger = nullptr;
		bool m_bFadeOutLight = false;
		bool m_bFadeInLight = false;
		bool m_bLightsOn = true;
		float m_fFadeSpeed = 0.2f;
		float m_fLightIntensity = 1.0f;
		std::unordered_map<Helix::Scene::LightComponent*, float> m_LightIntensities;
		std::unordered_map<Helix::Datastructures::GameObject*, Helix::Math::float3> m_LightModelsEmissiveColor;

		GameObjectVector m_LightModels;
		GameObjectVector m_NormalLights;
		GameObjectVector m_EmergencyLights;
	};
	inline GameScript::GameState GameScript::GetState() const { return m_State; }
	inline const GameSounds& GameScript::GetGameSounds() const { return m_GameSounds; }
}

#endif // !GAMESCRIPT_H
