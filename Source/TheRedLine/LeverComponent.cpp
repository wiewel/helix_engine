//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "LeverComponent.h"

using namespace RedLine;

//---------------------------------------------------------------------------------------------------
LeverComponent::LeverComponent(Helix::Datastructures::GameObject * _pParent, LeverComponentDesc& _Desc) :
	Helix::Datastructures::IGameObjectComponent(_pParent, Helix::Datastructures::ComponentCallback_OnUpdate),
	m_Settings(_Desc)
{
	m_pSpeakerComponent = GetParent()->AddComponent<Helix::Sound::Speaker>();
	m_pSpeakerComponent->AddVoice(m_Settings.sLeverSound);
	m_pSpeakerComponent->AddVoice(m_Settings.sLightsOn);
	m_pSpeakerComponent->AddVoice(m_Settings.sError);
	m_pSpeakerComponent->AddVoice(m_Settings.sAlarm, true);

	Helix::Physics::GeometryDesc ShapeDesc;
	ShapeDesc.Sphere.fRadius = 2.0f;
	ShapeDesc.kType = Helix::Physics::GeometryType_Sphere;
	Helix::Datastructures::ShapeComponent* pShape = GetParent()->AddComponent<Helix::Datastructures::ShapeComponent>(ShapeDesc);
	pShape->SetUsage(Helix::Physics::ShapeUsageType_Trigger);
	pShape->RegisterCallback(&OnTrigger);

	if (m_bIsOn)
	{
		GetParent()->SetPosition(m_Settings.vOnPosition);
	}
	else
	{
		GetParent()->SetPosition(m_Settings.vOffPosition);
	}
}
//---------------------------------------------------------------------------------------------------
LeverComponent::~LeverComponent()
{
}
//---------------------------------------------------------------------------------------------------
void LeverComponent::OnUpdate(double _fDeltaTime, double _fElapsedTime)
{
	if (m_bIsActive)
	{
		if (m_bIsSwitchingOff)
		{
			if (Move(m_Settings.vOffPosition, _fDeltaTime))
			{
				// the lever reached off position
				m_bIsSwitchingOff = false;
				m_bIsOn = false;
				m_pSpeakerComponent->Stop(m_Settings.sLeverSound);
				m_pSpeakerComponent->Stop(m_Settings.sAlarm);
				m_pSpeakerComponent->Start(m_Settings.sLightsOn);

				// activate next game state
				GameScript::Instance().EmergencyStopped();
			}
		}

		if (m_bIsSwitchingOn)
		{
			if (Move(m_Settings.vOnPosition, _fDeltaTime))
			{
				// the lever reached on position
				m_bIsSwitchingOn = false;
				m_bIsOn = true;
				m_pSpeakerComponent->Stop(m_Settings.sLeverSound);
				m_pSpeakerComponent->Start(m_Settings.sAlarm);

				// activate game state
				GameScript::Instance().GateClosed();
			}
		}
	}
}
//---------------------------------------------------------------------------------------------------
void LeverComponent::TryInteract()
{
	if (m_bPlayerInRange)
	{
		if (m_bIsOn)
		{
			SwitchOff();
		}
		else
		{
			SwitchOn();
		}
	}
}
//---------------------------------------------------------------------------------------------------
void RedLine::LeverComponent::SwitchOn()
{
	if (m_bIsOn == false)
	{
		m_bIsSwitchingOn = true;
		m_bIsSwitchingOff = false;
		if (m_bIsActive)
		{
			m_pSpeakerComponent->Start(m_Settings.sLeverSound);
		}
		else
		{
			m_pSpeakerComponent->Start(m_Settings.sError);
		}
	}
}
//---------------------------------------------------------------------------------------------------
void LeverComponent::SwitchOff()
{
	if (m_bIsOn)
	{
		m_bIsSwitchingOff = true;
		m_bIsSwitchingOn = false;
		if (m_bIsActive)
		{
			m_pSpeakerComponent->Start(m_Settings.sLeverSound);
		}
		else
		{
			m_pSpeakerComponent->Start(m_Settings.sError);
		}
	}
}
//---------------------------------------------------------------------------------------------------
void RedLine::LeverComponent::SetOn()
{
	m_bIsSwitchingOff = false;
	m_bIsSwitchingOn = false;
	m_bIsOn = true;
	GetParent()->SetPosition(m_Settings.vOnPosition);
}
//---------------------------------------------------------------------------------------------------
void RedLine::LeverComponent::SetOff()
{
	m_bIsSwitchingOff = false;
	m_bIsSwitchingOn = false;
	m_bIsOn = false;
	GetParent()->SetPosition(m_Settings.vOffPosition);
}
//---------------------------------------------------------------------------------------------------
bool LeverComponent::Move(Helix::Math::float3 _vTargetPos, double _fDeltaTime)
{
	Helix::Math::float3 vPos = GetParent()->GetPosition();
	Helix::Math::float3 vDir = _vTargetPos - vPos;
	float fMoveDistance = _fDeltaTime * m_Settings.fSpeed;

	// would the gate reach the target this update?
	if (vDir.magnitude() < fMoveDistance)
	{
		GetParent()->SetPosition(_vTargetPos);
		return true;
	}

	// move the gate towards the target
	vPos += vDir.getNormalized() * fMoveDistance;
	GetParent()->SetPosition(vPos);
	return false;
}
//---------------------------------------------------------------------------------------------------
void LeverComponent::OnTrigger(Helix::Physics::TriggerInfo & _Info)
{
	if (_Info.pActivator->GetName() == "MainCamera")
	{
		LeverComponent* pThis = _Info.pTrigger->GetComponent<LeverComponent>();
		if (_Info.State == Helix::Physics::SimulationEventState_Enter)
		{
			pThis->m_bPlayerInRange = true;
		}
		else if (_Info.State == Helix::Physics::SimulationEventState_Leave)
		{
			pThis->m_bPlayerInRange = false;
		}
	}
}