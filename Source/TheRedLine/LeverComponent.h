//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef LEVERCOMPONENT_H
#define LEVERCOMPONENT_H

#include "DataStructures\GameObjectComponent.h"
#include "Sound\Speaker.h"
#include "GameScript.h"
#include "DataStructures\ShapeComponent.h"

namespace RedLine
{
	struct LeverComponentDesc
	{
		float fSpeed = 0.5f;
		Helix::Math::float3 vOffPosition = Helix::Math::float3(0, 0, 0);
		Helix::Math::float3 vOnPosition = Helix::Math::float3(0, -0.5, 0);
		hlx::string sLeverSound = S("lever");
		hlx::string sLightsOn = S("lights");
		hlx::string sError = S("error");
		hlx::string sAlarm = S("alarm");
	};

	class LeverComponent : public Helix::Datastructures::IGameObjectComponent
	{
	private:
		LeverComponent(Helix::Datastructures::GameObject* _pParent, LeverComponentDesc& _Desc);
		COMPTYPENAME(LeverComponent, Helix::Datastructures::GameObject, 'Levr');
		COMPDESCONSTR(LeverComponent, Helix::Datastructures::GameObject, IGameObjectComponent);
		//COMPDEFCONSTR(LeverComponent, Helix::Datastructures::GameObject, IGameObjectComponent);
		
		void OnUpdate(double _fDeltaTime, double _fElapsedTime) final;

	public:
		void TryInteract();
		void SwitchOn();
		void SwitchOff();
		void SetOn();
		void SetOff();
		void Activate();
		void Deactivate();
		bool GetIsActive() const;
		bool GetIsOn() const;

		static void OnTrigger(Helix::Physics::TriggerInfo& _Info);
	private:
		bool Move(Helix::Math::float3 _vTargetPos, double _fDeltaTime);

		bool m_bIsActive = false;
		bool m_bPlayerInRange = false;
		bool m_bIsOn = false;
		bool m_bIsSwitchingOn = false;
		bool m_bIsSwitchingOff = false;

		Helix::Sound::Speaker* m_pSpeakerComponent = nullptr;
		Helix::Datastructures::ShapeComponent* m_pShapeComponent = nullptr;
		LeverComponentDesc m_Settings;
	};
	//---------------------------------------------------------------------------------------------------
	inline void LeverComponent::Activate()
	{
		m_bIsActive = true;
	}
	//---------------------------------------------------------------------------------------------------
	inline void LeverComponent::Deactivate()
	{
		m_bIsActive = false;
	}
	//---------------------------------------------------------------------------------------------------
	inline bool LeverComponent::GetIsActive() const
	{
		return m_bIsActive;
	}
	//---------------------------------------------------------------------------------------------------
	inline bool LeverComponent::GetIsOn() const
	{
		return m_bIsOn;
	}
}

#endif