//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "PlayerScript.h"

#include "Scene\CameraComponent.h"
#include "Physics\ControllerComponent.h"
#include "Scene\CameraManager.h"
#include "MainScript.h"
#include "DataStructures\TransformHierarchyComponent.h"
#include "Scene\LightComponent.h"
#include "GameScript.h"
#include "TicketComponent.h"
#include "LeverComponent.h"

using namespace RedLine;
using namespace Helix;

STATICSCRIPT_CPP(PlayerScript)

/// TODO: make a pair of these variables
bool PlayerScript::PushButtonMask[kButtonCount];
std::vector<Datastructures::GameObject*> PlayerScript::PushButtons;
std::vector<Datastructures::GameObject*> PlayerScript::PushButtonLights;

Datastructures::GameObject* PlayerScript::pCamera = nullptr;
Datastructures::GameObject* PlayerScript::pDoor = nullptr;

//---------------------------------------------------------------------------------------------------
PlayerScript::PlayerScript(const hlx::string& _sLevelName)
{
}
//---------------------------------------------------------------------------------------------------
PlayerScript::~PlayerScript()
{
}
//---------------------------------------------------------------------------------------------------
void PlayerScript::OnTrigger(Physics::TriggerInfo& _Info)
{
	Datastructures::GameObject* pActivator = _Info.pActivator;
	if (pActivator->GetName() == "MainCamera" && _Info.State == Physics::SimulationEventState_Enter)
	{
		HLOG("PushButton triggered");
		ToggleState(*_Info.pTrigger);
		
		/// TODO: trigger button down sound
	}
}
//---------------------------------------------------------------------------------------------------
void PlayerScript::ToggleState(Datastructures::GameObject& _PushButton)
{
	std::string sName = _PushButton.GetName();
	std::vector<std::string> SplitName = hlx::split(sName, '-');
	if (SplitName.size() != 2u || SplitName.front() != "PushButton")
	{
		return;
	}

	uint32_t uID = stoul(SplitName.back());
	if (uID >= kButtonCount || uID >= PushButtons.size())
	{
		return;
	}

	// only up buttons should activate
	if (PushButtonMask[uID] == false)
	{
		return;
	}

	//HLOG("Found PushButton with ID: %u", uID);

	//std::vector<uint32_t> ButtonsToAnimateDown;
	//std::vector<uint32_t> ButtonsToAnimateUp;
	std::vector<uint32_t> ButtonsToToggle;

	//	0: 0 + 1 + 2
	//	1: 1 + 3
	//	2 : 0 + 2 + 3
	//	3 : 3
	switch (uID)
	{
	case 0:
		/// e.g.:
		ButtonsToToggle.push_back(0u);
		break;
	case 1:
		ButtonsToToggle.push_back(1u);
		break;
	case 2:
		ButtonsToToggle.push_back(2u);
		break;
	case 3:
		ButtonsToToggle.push_back(3u);
	default:
		break;
	}

	for (const uint32_t uButton : ButtonsToToggle)
	{
		if (PushButtonMask[uButton])
		{
			/// When the button is up ( mask == false ) -> animate down
			PushButtons[uButton]->SetPosition(PushButtons[uButton]->Position - Math::float3(0, 0.14, 0));
			PushButtonMask[uButton] = false;
			Scene::LightComponent* pLight = PushButtonLights[uButton]->GetComponent<Scene::LightComponent>();
			if (pLight != nullptr)
			{
				pLight->SetEnabled(true);
			}
		}
		else
		{
			/// When the button is up ( mask == true ) -> animate up
			PushButtons[uButton]->SetPosition(PushButtons[uButton]->Position + Math::float3(0, 0.14, 0));
			PushButtonMask[uButton] = true;
			Scene::LightComponent* pLight = PushButtonLights[uButton]->GetComponent<Scene::LightComponent>();
			if (pLight != nullptr)
			{
				pLight->SetEnabled(false);
			}
		}
	}

	// play sound to notify player about activation of mechanic
	if (pCamera)
	{
		Sound::Speaker* pCameraSpeaker = pCamera->GetComponent<Sound::Speaker>();
		if (pCameraSpeaker)
		{
			pCameraSpeaker->Start(S("button"));
		}
	}

	// check if all buttons are pressed
	bool bAllDown = true;
	for (uint32_t i = 0; i < kButtonCount; ++i)
	{
		bAllDown = bAllDown && (PushButtonMask[i] == false);
	}

	if (bAllDown)
	{
		// puzzle solved -> unlock next room
		GameScript::Instance().PassageOpened();
	}
	//for(const uint32_t uButton : ButtonsToAnimateDown)
	//{
	//	if(PushButtonMask[uButton])
	//	{
	//		/// When the button is up ( mask == true ) -> animate down
	//		PushButtons[uButton]->SetPosition(PushButtons[uButton]->Position - Math::float3(0, 0.07, 0));
	//		PushButtonMask[uButton] = false;
	//	}
	//}
	//for (const uint32_t uButton : ButtonsToAnimateUp)
	//{
	//	if (PushButtonMask[uButton] == false)
	//	{
	//		/// When the button is up ( mask == true ) -> animate up
	//		PushButtons[uButton]->SetPosition(PushButtons[uButton]->Position + Math::float3(0, 0.07, 0));
	//		PushButtonMask[uButton] = true;
	//	}
	//}
}
//---------------------------------------------------------------------------------------------------
void PlayerScript::SetTicketInRange(Helix::Datastructures::GameObject* _pTicket)
{
	if (m_bHasTicket == false)
	{
		m_pTicket = _pTicket;
	}
}
//---------------------------------------------------------------------------------------------------
void PlayerScript::Catch(const Math::float3& _vCatchPos)
{
	if (m_pCameraController != nullptr)
	{
		m_pCameraController->LookAt(_vCatchPos);
	}
	if (m_bIsDead == false)
	{
		m_bIsDead = true;
		HLOG("Gotcha!");
		GameScript::Instance().Dead();
		//m_DeathTimer.InvokeAfter(5s);
	}
}
//---------------------------------------------------------------------------------------------------
void PlayerScript::SpawnPlayer()
{
	Datastructures::GameObject* pSpawn = Datastructures::GameObjectPool::Instance()->GetObjectByName("SpawnPoint");
	if (pSpawn != nullptr && m_pCameraController != nullptr)
	{
		Math::float3 vSpawnPosition = pSpawn->GetPosition();
		//pSpawn->Destroy();
		m_pCameraController->Teleport(vSpawnPosition);
		Datastructures::GameObject* pGate = Datastructures::GameObjectPool::Instance()->GetObjectByName("rollerdoor_01");
		if (pGate != nullptr)
		{
			Math::float3 vLookAtPos = pGate->GetPosition();
			m_pCameraController->LookAt(vLookAtPos);
		}
		HLOG("Spawned Player at (%f, %f, %f)", vSpawnPosition.x, vSpawnPosition.y, vSpawnPosition.z);
		m_bIsDead = false;
	}

	m_bHasTicket = false;
	m_pTicket = nullptr;
	m_FlashLight.fBatteryPower = m_FlashLight.kDefaultBatteryPower;
}
//---------------------------------------------------------------------------------------------------
void PlayerScript::ResetButtons()
{
	for (uint32_t uButton = 0u; uButton < std::min(kButtonCount, static_cast<uint32_t>(PushButtons.size())); ++uButton)
	{
		if (PushButtonMask[uButton] == false)
		{
			PushButtons[uButton]->SetPosition(PushButtons[uButton]->Position + Math::float3(0, 0.14, 0));
			PushButtonMask[uButton] = true;
			if(PushButtonLights[uButton] != nullptr)
			{
				Scene::LightComponent* pLight = PushButtonLights[uButton]->GetComponent<Scene::LightComponent>();
				if (pLight != nullptr)
				{
					pLight->SetEnabled(false);
				}
			}
		}
	}
}
//---------------------------------------------------------------------------------------------------
void PlayerScript::OnSceneBegin()
{
	for (bool& State : PushButtonMask)
	{
		/// true stands for "the button is up"
		State = false;
	}

	m_InputReciever.Initialize();
	m_InputReciever.AddContext("Game", S("Game_InputMapping"));

	Scene::CameraComponent* pCameraComponent = pCamera->GetComponent<Scene::CameraComponent>();
	if (pCameraComponent != nullptr)
	{
		pCameraComponent->SetFOV(50.0f);
		float fHeadOffset = m_PlayerSettings.fHeight / 2.0f;
		pCameraComponent->SetOffset(Math::float3(0, fHeadOffset, 0));
	}

	// create a speaker that plays sound when the player walks into the trigger
	GameSounds GameSounds = GameScript::Instance().GetGameSounds();

	m_pSpeakerComponent = pCamera->AddComponent<Sound::Speaker>();
	m_pSpeakerComponent->SetIs3D(false);
	m_pSpeakerComponent->AddVoice(m_Sounds.sButton);
	m_pSpeakerComponent->SetVolume(m_Sounds.sButton, 0.5f);

	m_pSprintSpeaker = pCamera->AddComponent<Sound::Speaker>();
	m_pSprintSpeaker->SetIs3D(false);
	m_pSprintSpeaker->AddVoice(m_Sounds.sHeartBeat, true);
	m_pSprintSpeaker->AddVoice(m_Sounds.sBreathe, true);
	m_pSprintSpeaker->SetVolume(m_Sounds.sHeartBeat, 0.0f);
	m_pSprintSpeaker->SetVolume(m_Sounds.sBreathe, 0.0f);
	m_pSprintSpeaker->Start(m_Sounds.sHeartBeat);
	m_pSprintSpeaker->Start(m_Sounds.sBreathe);

	Sound::Listener* pListenerComponent = pCamera->AddComponent<Sound::Listener>();

	// create a simulation shape to the camera to allow activation of triggers
	//Physics::GeometryDesc ShapeDesc;
	//ShapeDesc.Box.vHalfExtents = Math::float3(0.25f, 1.0f, 0.25f);
	//ShapeDesc.kUsage = Physics::ShapeUsageType_Simulation;
	//Datastructures::ShapeComponent* pShape = pCamera->AddComponent<Datastructures::ShapeComponent>(ShapeDesc);
	
	// for testing create a transform hierarchy and attach some object to the camera
	Datastructures::TransformHierarchyComponent* pCameraTH = pCamera->AddComponent<Datastructures::TransformHierarchyComponent>();

	//Math::transform Transform(pCamera->GetTransform());
	//Transform.q = HQUATERNION_IDENTITY;

	Datastructures::GameObject* pFlashlight = Datastructures::GameObjectPool::Instance()->Create().get();
	pFlashlight->SetKinematic(true);
	Datastructures::TransformHierarchyComponent* pChildTH = pFlashlight->AddComponent<Datastructures::TransformHierarchyComponent>();
	pChildTH->SetParentNode(pCameraTH);
	pChildTH->SetLocalTransform({ -0.3f, 0.2f, -0.5f});
	m_pFlashlightComponent = pFlashlight->AddComponent<Scene::LightComponent>(Scene::LightType_Spot);
	m_pFlashlightComponent->ShadowCaster = true;
	m_pFlashlightComponent->DecayStart = 3.0f;
	m_pFlashlightComponent->Angle = 25.f;
	m_pFlashlightComponent->Range = 25.f;
	m_pFlashlightComponent->Intensity = m_FlashLight.kFlashlightIntensity;
	m_pFlashlightComponent->SetEnabled(false);
	m_pFlashlightComponent->Color = m_FlashLight.vColor; //Math::float3(1.0f, 0.8f, 0.7f);
	pFlashlight->SetFlags(Datastructures::GameObjectFlag_Invisible);
	
	// add gameobject for left foot speaker
	Datastructures::GameObject* pFoot = Datastructures::GameObjectPool::Instance()->Create().get();
	pFoot->SetKinematic(true);
	Datastructures::TransformHierarchyComponent* pFootTH = pFoot->AddComponent<Datastructures::TransformHierarchyComponent>();
	pFootTH->SetParentNode(pCameraTH);
	pFootTH->SetLocalTransform({ -0.1f, -0.5f, 0.3f });
	m_pLeftFootSpeaker = pFoot->AddComponent<Sound::Speaker>();
	m_pLeftFootSpeaker->AddVoice(m_Sounds.sStepLeft);
	m_pLeftFootSpeaker->SetVolume(0.5f);

	// add gameobject for right foot speaker
	pFoot = Datastructures::GameObjectPool::Instance()->Create().get();
	pFoot->SetKinematic(true);
	pFootTH = pFoot->AddComponent<Datastructures::TransformHierarchyComponent>();
	pFootTH->SetParentNode(pCameraTH);
	pFootTH->SetLocalTransform({ 0.1f, -0.5f, 0.3f });
	m_pRightFootSpeaker = pFoot->AddComponent<Sound::Speaker>();
	m_pRightFootSpeaker->AddVoice(m_Sounds.sStepRight);
	m_pRightFootSpeaker->SetVolume(0.5f);

	m_bIsDead = false;
}
//---------------------------------------------------------------------------------------------------

void PlayerScript::OnSceneEnd()
{
	m_InputReciever.Uninitialize();
}
//---------------------------------------------------------------------------------------------------
void PlayerScript::OnLevelLoaded()
{
	
	// get the main camera
	Scene::CameraComponent* pCameraComponent = Scene::CameraManager::Instance()->GetCameraByName("MainCamera");
	HASSERT(pCameraComponent, "There is no main camera in the scene");
	pCamera = static_cast<Datastructures::GameObject*>(pCameraComponent->GetParent());

	// create a controller for the camera, an abstraction of movement
	pCamera->SetPosition(Math::float3(-2, 2, 5));
	Physics::CapsuleControllerDesc Desc;
	Desc.fHeight = m_PlayerSettings.fHeight;
	Desc.fRadius = m_PlayerSettings.fRadius;
	Desc.fStepOffset = 0.1f;
	Desc.vPosition = pCamera->GetPosition();
	m_pCameraController = pCamera->AddComponent<Physics::ControllerComponent>(Desc);

	pDoor = Datastructures::GameObjectPool::Instance()->GetObjectByName("Door");

	// register triggers for buttons
	for (uint32_t i = 0u; i < kButtonCount; ++i)
	{
		std::vector<Datastructures::GameObject*> Objects;
		Datastructures::GameObjectPool::Instance()->GetObjectsByName("PushButton-" + std::to_string(i), Objects);
		if (Objects.size() > 0u)
		{
			Datastructures::GameObject* pTrigger = Objects[0];
			for (auto pShape : pTrigger->GetComponentsByType<Datastructures::ShapeComponent>())
			{
				if (pShape->GetUsage() == Physics::ShapeUsageType_Trigger)
				{
					pShape->RegisterCallback(Physics::TriggerEvent::Callback(&OnTrigger));
					PushButtons.push_back(pTrigger);
					PushButtonMask[i] = false;
					m_bTriggerRegistered = true;
				}
			}
			Datastructures::GameObject* pButtonLight = Datastructures::GameObjectPool::Instance()->GetObjectByName("PushButton-" + std::to_string(i) + "_spotlight");
			PushButtonLights.push_back(pButtonLight);
		}
	}

	// get the lever
	Datastructures::GameObject* pLever = Datastructures::GameObjectPool::Instance()->GetObjectByName("Emergency_Lever");
	if (pLever != nullptr)
	{
		LeverComponentDesc Desc;
		Desc.vOffPosition = pLever->GetPosition();
		Desc.vOnPosition = Desc.vOffPosition;
		Desc.vOnPosition.y -= 0.1f;
		Desc.fSpeed = 0.16f;
		m_pLeverComponent = pLever->AddComponent<LeverComponent>(Desc);
	}
}
//---------------------------------------------------------------------------------------------------

void PlayerScript::OnUpdate(double _fDeltaTime, double _fTotalTime)
{
	FlashLightStateUpdate(_fDeltaTime, _fTotalTime);
	//SprintStateUpdate(_fDeltaTime, _fTotalTime);

	if (m_pCameraController != nullptr && m_bIsDead == false)
	{
		if (m_InputReciever.Action("Flashlight"))
		{
			if (m_pFlashlightComponent->GetEnabled())
			{
				if(m_FlashLight.State == FlashLightSettings::State_On)
				{
					m_FlashLight.State = FlashLightSettings::State_Off;
				}
			}
			else
			{
				if (m_FlashLight.State == FlashLightSettings::State_Off)
				{
					m_FlashLight.State = FlashLightSettings::State_On;
				}
			}
		}

		if (m_InputReciever.Action("Reset"))
		{
			//m_pCameraController->SetPosition(m_PlayerSettings.fJumpVelocity);
		}

		if (m_InputReciever.Action("Interact"))
		{
			if (m_pTicket != nullptr)
			{
				TicketComponent* pTicketComponent = m_pTicket->GetComponent<TicketComponent>();
				if (pTicketComponent != nullptr)
				{
					if (m_bStamIsInRange && m_bHasTicket)
					{
						pTicketComponent->UseTicket();
					}
					else
					{
						pTicketComponent->CarryTicket();
						m_bHasTicket = true;
					}
				}
			}
			if (m_pLeverComponent != nullptr)
			{
				m_pLeverComponent->TryInteract();
			}
		}

		// Movement
		float fMovementSpeed = m_PlayerSettings.fWalkSpeed;
		if (m_InputReciever.State("Run"))
		{
			m_SprintSettings.fSprintDuration += _fDeltaTime;
			fMovementSpeed = m_PlayerSettings.fRunSpeed;
		}
		else
		{
			m_SprintSettings.fSprintDuration -= _fDeltaTime * 2.0f;
			m_SprintSettings.fSprintDuration = max(m_SprintSettings.fSprintDuration, 0.0f);
		}
		//if (m_InputReciever.Action("Jump"))
		//{
		//	m_pCameraController->Jump(m_PlayerSettings.fJumpVelocity);
		//}
		if ((m_InputReciever.State("Move_Forward") || m_InputReciever.State("Move_Back")) &&
			m_InputReciever.State("Move_Left") || m_InputReciever.State("Move_Right"))
		{
			fMovementSpeed = fMovementSpeed * 0.70711f;
		}
		if (m_InputReciever.State("Move_Forward"))
		{
			m_pCameraController->Walk(fMovementSpeed * _fDeltaTime);
		}
		if (m_InputReciever.State("Move_Right"))
		{
			m_pCameraController->Strafe(fMovementSpeed * _fDeltaTime);
		}
		if (m_InputReciever.State("Move_Back"))
		{
			m_pCameraController->Walk(-fMovementSpeed * _fDeltaTime);
		}
		if (m_InputReciever.State("Move_Left"))
		{
			m_pCameraController->Strafe(-fMovementSpeed * _fDeltaTime);
		}
		int32_t iLookX = 0;
		int32_t iLookY = 0;
		if (m_InputReciever.Mouse(iLookX, iLookY))
		{
			m_pCameraController->Turn(static_cast<float>(iLookX)  * m_PlayerSettings.fLookSpeed * _fDeltaTime);
			m_pCameraController->Nod(static_cast<float>(iLookY) * m_PlayerSettings.fLookSpeed * _fDeltaTime);
		}

		// Gamepad
		int iWalk = 0;
		if (m_InputReciever.Range("Move_Y", iWalk))
		{
			float fWalk = iWalk * -3.0f / 65535.0f;
			m_pCameraController->Walk(fWalk * fMovementSpeed * _fDeltaTime);
		}
		int iStrafe = 0;
		if (m_InputReciever.Range("Move_X", iStrafe))
		{
			float fStrafe = iStrafe * 3.0f / 65535.0f;
			m_pCameraController->Strafe(fStrafe * fMovementSpeed * _fDeltaTime);
		}
		int iTurn = 0;
		if (m_InputReciever.Range("Look_X", iTurn))
		{
			float fTurn = iTurn * 3.0f / 65535.0f;
			m_pCameraController->Turn(fTurn * m_PlayerSettings.fLookSpeed * _fDeltaTime);
		}
		int iNod = 0;
		if (m_InputReciever.Range("Look_Y", iNod))
		{
			HLOG("Y val: %d", iNod);
			float fNod = iNod * 3.0f / 65535.0f;
			m_pCameraController->Nod(fNod * m_PlayerSettings.fLookSpeed * _fDeltaTime);
		}
	}

	m_InputReciever.Sync();

	m_fMoveDistance += m_pCameraController->GetMoveDistance();
	if (m_fMoveDistance > m_PlayerSettings.fStepLength)
	{
		m_fMoveDistance -= m_PlayerSettings.fStepLength;
		m_uStepNum = (m_uStepNum + 1) % 2;
		switch (m_uStepNum)
		{
		case 0u:
			m_pLeftFootSpeaker->Start(m_Sounds.sStepLeft);
			break;
		case 1u:
			m_pRightFootSpeaker->Start(m_Sounds.sStepRight);
			break;
		default:
			break;
		}
	}

	/// Exhaustion sounds
	if(m_SprintSettings.fSprintDuration > m_SprintSettings.fHeartStart)
	{
		float fVolumeHeart = min(max(m_SprintSettings.fSprintDuration - m_SprintSettings.fHeartStart, 0.0f) / (m_SprintSettings.fHeartMax - m_SprintSettings.fHeartStart), 1.5f);
		float fVolumeBreath = min(max(m_SprintSettings.fSprintDuration - m_SprintSettings.fBreathStart, 0.0f) / (m_SprintSettings.fBreathMax - m_SprintSettings.fBreathStart), 0.8f);

		//HLOG("Volumes: %f %f", fVolumeHeart, fVolumeBreath);
		m_pSprintSpeaker->SetVolume(m_Sounds.sHeartBeat, fVolumeHeart);
		m_pSprintSpeaker->SetVolume(m_Sounds.sBreathe, fVolumeBreath);
	}
}
//---------------------------------------------------------------------------------------------------
void PlayerScript::OnDestroyObjects()
{
}
//---------------------------------------------------------------------------------------------------
void PlayerScript::FlashLightStateUpdate(float _fDeltaTime, float _fTotalTime)
{
	if (GameScript::Instance().GetState() == GameScript::GameState_Success)
	{
		m_FlashLight.State = FlashLightSettings::State_Off;
	}
	switch (m_FlashLight.State)
	{
	case FlashLightSettings::State_On:
	{
		m_FlashLight.fBatteryPower -= _fDeltaTime;
		if (m_FlashLight.fBatteryPower < 0.0f)
		{
			m_FlashLight.State = FlashLightSettings::State_CoolDown;
			m_FlashLight.fCoolDownStartTime = _fTotalTime;
			m_pFlashlightComponent->Color = m_FlashLight.vColor;
		}
		else
		{
			float fBatteryStrength = Math::Saturate(log10f(m_FlashLight.fBatteryPower + 1.0f) * 0.8f);//Math::Saturate((log10f(m_FlashLight.fBatteryPower) + 10.0f) / (log10f(m_FlashLight.kDefaultBatteryPower) + 10.0f));
			m_pFlashlightComponent->Intensity = Math::Saturate(fBatteryStrength + 0.1f) * m_FlashLight.kFlashlightIntensity;
			m_pFlashlightComponent->SetEnabled(true);
			m_pFlashlightComponent->Color = Math::Lerp(m_FlashLight.vColor * Math::float3(1.0f, 0.82f, 0.6f), m_FlashLight.vColor, fBatteryStrength);
		}
		break;
	}
	case FlashLightSettings::State_Off:
	{
		if (m_FlashLight.fBatteryPower < m_FlashLight.kDefaultBatteryPower)
		{
			m_FlashLight.fBatteryPower += _fDeltaTime * 1.5f;
		}
		else
		{
			m_FlashLight.fBatteryPower = m_FlashLight.kDefaultBatteryPower;
		}
		m_pFlashlightComponent->SetEnabled(false);
		break;
	}
	case FlashLightSettings::State_CoolDown:
	{
		float x = _fTotalTime - m_FlashLight.fCoolDownStartTime;
		m_pFlashlightComponent->Intensity = x < 2.0f ? (Math::Saturate(sinf(sqrt(x) * 22.0f) / (2.0f * x)) * 0.245f) : 0.0f;
		m_pFlashlightComponent->Intensity *= m_FlashLight.kFlashlightIntensity;
		if (m_FlashLight.fCoolDownStartTime + m_FlashLight.kCoolDown < _fTotalTime)
		{
			m_FlashLight.State = FlashLightSettings::State_Off;
		}
		break;
	}
	default:
		break;
	}
}
//---------------------------------------------------------------------------------------------------
//void PlayerScript::SprintStateUpdate(float _fDeltaTime, float _fTotalTime)
//{
//	switch (m_SprintSettings.State)
//	{
//	case SprintSettings::State_Sprint:
//	{
//		m_SprintSettings.fSprintDuration -= _fDeltaTime;
//		if (m_SprintSettings.fSprintDuration < 0.0f)
//		{
//			m_SprintSettings.State = SprintSettings::State_CoolDown;
//			m_SprintSettings.fCoolDownStartTime = _fTotalTime;
//		}
//		else
//		{
//
//		}
//		break;
//	}
//	case SprintSettings::State_Walk:
//	{
//		if (m_SprintSettings.fSprintDuration < m_SprintSettings.kDefaultSprintDuration)
//		{
//			m_SprintSettings.fSprintDuration += _fDeltaTime * 1.5f;
//		}
//		else
//		{
//			m_SprintSettings.fSprintDuration = m_SprintSettings.kDefaultSprintDuration;
//		}
//		break;
//	}
//	case SprintSettings::State_CoolDown:
//	{
//		if (m_SprintSettings.fCoolDownStartTime + m_SprintSettings.kCoolDown < _fTotalTime)
//		{
//			m_SprintSettings.State = SprintSettings::State_Walk;
//		}
//		break;
//	}
//	default:
//		break;
//	}
//}
//---------------------------------------------------------------------------------------------------
