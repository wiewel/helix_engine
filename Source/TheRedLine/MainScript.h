//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef MAINSCRIPT_H
#define MAINSCRIPT_H

#include "Scene\ScriptObject.h"
#include "Scene\Level.h"

namespace RedLine
{
	class MainScript : public Helix::Scene::IScriptObject
	{
	public:
		MainScript(const hlx::string& _sLevelName = {});
		~MainScript();

		void SetLevelName(const hlx::string& _sLevelName);

		STATICSCRIPT(MainScript)

		static void ReloadLevel();
		Helix::Scene::Level& GetLevel();

	private:
		void OnSceneBegin() final;
		void OnSceneEnd() final;
		void OnUpdate(double _fDeltaTime, double _fTotalTime) final;
		void OnLevelLoaded() final;
		// the use can safely destroy gameobjects in this function, without crashing the rendering / physics
		void OnDestroyObjects() final;

	private:
		hlx::string m_sLevelName;
		Helix::Scene::Level m_Level;

	};
	inline void MainScript::SetLevelName(const hlx::string & _sLevelName){m_sLevelName = _sLevelName;}
	inline Helix::Scene::Level & RedLine::MainScript::GetLevel() { return m_Level; }
} // RedLine

#endif // !MAINSCRIPT_H
