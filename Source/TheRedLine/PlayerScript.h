//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef PLAYERSCRIPT_H
#define PLAYERSCRIPT_H

#include "Scene\ScriptObject.h"
#include "Scene\Level.h"
#include "Input\InputReciever.h"
#include "DataStructures\ShapeComponent.h"
#include "Async\Event.h"
#include "Sound\Speaker.h"

namespace RedLine
{
	class LeverComponent;
	using namespace std::chrono_literals;
	struct PlayerSettings
	{
		float fWalkSpeed = 2.0f;
		float fRunSpeed = 4.0f;
		float fLookSpeed = 10.0f;
		float fJumpVelocity = 5.0f;
		float fStepLength = 1.5f;
		float fHeight = 1.0f;
		float fRadius = 0.3f;
		std::chrono::duration<uint64_t> ReloadTime = 5s;
	};
	struct FlashLightSettings
	{
		float fCoolDownStartTime = 0.0f;
		float fBatteryPower = 45.0f;

		const float kCoolDown = 3.0f;
		const float kDefaultBatteryPower = 45.0f;
		const float kFlashlightIntensity = 0.1f;
		const Helix::Math::float3 vColor = Helix::Math::float3(1.0f, 0.8f, 0.65f);

		enum State
		{
			State_On = 0,
			State_Off,
			State_CoolDown,
		} State = State_Off;
	};
	struct SprintSettings
	{
		float fSprintDuration = 0.0f;
		float fHeartStart = 5.0f;
		float fHeartMax = 10.0f;

		float fBreathStart = 7.0f;
		float fBreathMax = 10.0f;
		//float fCoolDownStartTime = 0.0f;
		//const float kCoolDown = 3.0f;
		//const float kDefaultSprintDuration = 20.0f;

		//enum State
		//{
		//	State_Sprint = 0,
		//	State_Walk,
		//	State_CoolDown,
		//} State;
	};
	struct Sounds
	{
		hlx::string sStepLeft = S("step_1");
		hlx::string sStepRight = S("step_2");
		hlx::string sButton = S("button");
		hlx::string sBreathe = S("breathe");
		hlx::string sHeartBeat = S("heartbeat");
	};

	class StatueComponent;

	class PlayerScript : public Helix::Scene::IScriptObject
	{
	public:
		PlayerScript(const hlx::string& _sLevelName = {});
		~PlayerScript();

		STATICSCRIPT(PlayerScript)

		static void OnTrigger(Helix::Physics::TriggerInfo& _Info);
		static void ToggleState(Helix::Datastructures::GameObject& _PushButton);
		void SetTicketInRange(Helix::Datastructures::GameObject* _pTicket);
		void SetStampInRange(bool _bInRange);

		void Catch(const Helix::Math::float3& _vCatchPosition);
		void SpawnPlayer();
		void ResetButtons();
		Helix::Datastructures::GameObject* GetPlayer();

	private:
		void OnSceneBegin() final;
		void OnSceneEnd() final;
		void OnLevelLoaded() final;
		void OnUpdate(double _fDeltaTime, double _fTotalTime) final;

		// the use can safely destroy gameobjects in this function, without crashing the rendering / physics
		void OnDestroyObjects() final;
	
		void FlashLightStateUpdate(float _fDeltaTime, float _fTotalTime);
		void SprintStateUpdate(float _fDeltaTime, float _fTotalTime);

	private:
		PlayerSettings m_PlayerSettings;
		Sounds m_Sounds;
		Helix::Input::InputReciever m_InputReciever;
		bool m_bTriggerRegistered = false;
		bool m_bIsDead = false;
		Helix::Async::Event<void> m_DeathTimer;

		static const uint32_t kButtonCount = 4u;
		static bool PushButtonMask[kButtonCount];
		static std::vector<Helix::Datastructures::GameObject*> PushButtons;
		static std::vector<Helix::Datastructures::GameObject*> PushButtonLights;
		static Helix::Datastructures::GameObject* pCamera;
		static Helix::Datastructures::GameObject* pDoor;

		bool m_bStamIsInRange = false;

		Helix::Datastructures::GameObject* m_pTicket = nullptr;

		Helix::Physics::ControllerComponent* m_pCameraController = nullptr;
		Helix::Sound::Speaker* m_pSpeakerComponent = nullptr;
		Helix::Sound::Speaker* m_pLeftFootSpeaker = nullptr;
		Helix::Sound::Speaker* m_pRightFootSpeaker = nullptr;
		Helix::Scene::LightComponent* m_pFlashlightComponent = nullptr;
		LeverComponent* m_pLeverComponent = nullptr;

		float m_fMoveDistance = 0.0f;
		uint32_t m_uStepNum = 0u;
		bool m_bHasTicket = false;

		/// flickering flash light
		FlashLightSettings m_FlashLight;

		/// exhaustion
		Helix::Sound::Speaker* m_pSprintSpeaker = nullptr;
		SprintSettings m_SprintSettings;
	};
	inline Helix::Datastructures::GameObject* PlayerScript::GetPlayer()
	{
		return pCamera;
	}
	inline void PlayerScript::SetStampInRange(bool _bInRange)
	{
		m_bStamIsInRange = _bInRange;
	}
} // RedLine

#endif // !PLAYERSCRIPT_H
