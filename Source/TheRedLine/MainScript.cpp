//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "MainScript.h"
#include "Scene\PhysicsScene.h"
#include "StatueComponent.h"
#include "DataStructures\GameObject.h"
#include "Scene\ScriptScene.h"
#include "Scene\RenderingScene.h"
#include "Scene\ComponentScene.h"
#include "Scene\LightComponent.h"
#include "DataStructures\TransformHierarchyComponent.h"
//#include "PlayerScript.h"

using namespace RedLine;
using namespace Helix;

STATICSCRIPT_CPP(MainScript)

//---------------------------------------------------------------------------------------------------
MainScript::MainScript(const hlx::string& _sLevelName) :
	m_sLevelName(_sLevelName)
{
}
//---------------------------------------------------------------------------------------------------

MainScript::~MainScript()
{
}

//---------------------------------------------------------------------------------------------------
void MainScript::ReloadLevel()
{
	// TODO: Allow level reload
	Scene::PhysicsScene::Instance()->Pause(true);
	Scene::ScriptScene::Instance()->Pause(true);
	Scene::ScriptScene::Instance()->End();
	Scene::RenderingScene::Instance()->Pause(true);
	Scene::RenderingScene::Instance()->Lock(); // rendering might be qthosted!
	Scene::ComponentScene::Instance()->Pause(true);

	//Instance().OnSceneEnd(); // clean up
	//Instance().OnSceneBegin();  // load
	//PlayerScript::Instance().OnSceneBegin();

	Scene::ComponentScene::Instance()->Continue();
	Scene::ScriptScene::Instance()->Begin();
	Scene::RenderingScene::Instance()->Continue();
	//Scene::ScriptScene::Instance()->Continue();
	Scene::RenderingScene::Instance()->Unlock();
	Scene::PhysicsScene::Instance()->Continue();
}
//---------------------------------------------------------------------------------------------------
void MainScript::OnSceneBegin()
{
	if (m_sLevelName.empty() == false)
	{
		m_Level.Load(m_sLevelName, true);
	}

	//Scene::PhysicsScene::Instance()->Continue();
}
//---------------------------------------------------------------------------------------------------

void MainScript::OnSceneEnd()
{
	m_Level.Unload();
}
//---------------------------------------------------------------------------------------------------

void MainScript::OnUpdate(double _fDeltaTime, double _fTotalTime)
{
}
//---------------------------------------------------------------------------------------------------
void MainScript::OnLevelLoaded()
{
	std::vector<Datastructures::GameObject*> Statues;
	Datastructures::GameObjectPool::Instance()->GetGameObjectsByTag("statue", Statues);
	for (Datastructures::GameObject* pStatue : Statues)
	{
		if (pStatue)
		{
			StatueSettings StatueDesc;
			pStatue->AddComponent<StatueComponent>(StatueDesc);
		}
	}
}
//---------------------------------------------------------------------------------------------------

void MainScript::OnDestroyObjects()
{
}
//---------------------------------------------------------------------------------------------------
