//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef THEREDLINE_H
#define THEREDLINE_H

#include <QtWidgets/QMainWindow>
#include "ui_TheRedLine.h"
#include <qtimer.h>
#include "SolidEffectRenderPass.h"

//forward decls

namespace Helix
{
	namespace Engine
	{
		class D3DWidget;
	} // Engine
}// Helix


namespace RedLine
{
	class TheRedLine : public QMainWindow
	{
		Q_OBJECT

	public:
		TheRedLine();
		~TheRedLine();

		void HideToolBars(bool _bHide);

	private slots:
		void UpdateTitle();

	private:
		Ui::TheRedLineClass ui;
		Helix::Engine::D3DWidget* m_pD3DWidget = nullptr;
		QTimer* m_pTimer = nullptr;

		//MainScript m_MainScript;

	private:
		std::shared_ptr<Helix::Display::SolidEffectRenderPass> m_pSolidPass;
	};
}// RedLine


#endif // THEREDLINE_H
