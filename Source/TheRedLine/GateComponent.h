//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef GATECOMPONENT_H
#define GATECOMPONENT_H

#include "DataStructures\GameObjectComponent.h"
#include "Math\MathTypes.h"
#include "Sound\Speaker.h"

namespace RedLine
{
	struct GateSounds
	{
		hlx::string sGateMove = S("garage_move");
		hlx::string sGateStop = S("garage_stop");
	};
	struct GateComponentDesc
	{
		bool bStartOpen = false;
		float fSpeed = 0.5f;
		Helix::Math::float3 vClosePosition = Helix::Math::float3(0, 0, 0);
		Helix::Math::float3 vOpenPosition = Helix::Math::float3(0, 3, 0);
	};
	class GateComponent : public Helix::Datastructures::IGameObjectComponent
	{
	public:
		GateComponent(Helix::Datastructures::GameObject* _pParent, GateComponentDesc& _Desc);
		COMPTYPENAME(GateComponent, Helix::Datastructures::GameObject, 'Gate');
		COMPDESCONSTR(GateComponent, Helix::Datastructures::GameObject, IGameObjectComponent);

		void OnLevelLoaded() final;
		void OnUpdate(double _fDeltaTime, double _fElapsedTime) final;

		void OpenGate();
		void CloseGate();

		bool IsOpen() const;
		bool IsCloded() const;

	private:
		bool Move(Helix::Math::float3 _vTarget, float _fDeltaTime);

		bool m_bIsOpen = false;
		bool m_bIsOpening = false;
		bool m_bIsClosing = false;
		const GateComponentDesc m_Desc;

		GateSounds m_GateSounds;
		Helix::Sound::Speaker* m_pSpeakerComponent = nullptr;
	};
	//---------------------------------------------------------------------------------------------------
	inline GateComponent::GateComponent(Helix::Datastructures::GameObject * _pParent, GateComponentDesc& _Desc)
		: Helix::Datastructures::IGameObjectComponent(_pParent, Helix::Datastructures::ComponentCallback_OnUpdate | Helix::Datastructures::ComponentCallback_OnLevelLoaded)
		, m_Desc(_Desc)
	{
		m_pSpeakerComponent = GetParent()->AddComponent<Helix::Sound::Speaker>();
		m_pSpeakerComponent->AddVoice(m_GateSounds.sGateMove, true);
		m_pSpeakerComponent->AddVoice(m_GateSounds.sGateStop);
		m_pSpeakerComponent->SetDistanceFalloff(10.0f);
	}
	//---------------------------------------------------------------------------------------------------
	inline GateComponent::~GateComponent()
	{

	}
	//---------------------------------------------------------------------------------------------------
	inline void GateComponent::OnLevelLoaded()
	{
	}
	//---------------------------------------------------------------------------------------------------
	inline void GateComponent::OnUpdate(double _fDeltaTime, double _fElapsedTime)
	{
		if (m_bIsClosing)
		{
			if (Move(m_Desc.vClosePosition, _fDeltaTime))
			{
				// the gate reached close position
				m_bIsClosing = false;
				m_bIsOpen = false;
				m_pSpeakerComponent->Stop(m_GateSounds.sGateMove);
				m_pSpeakerComponent->Start(m_GateSounds.sGateStop);
			}
		}

		if (m_bIsOpening)
		{
			if (Move(m_Desc.vOpenPosition, _fDeltaTime))
			{
				// the gate reached open position
				m_bIsOpening = false;
				m_bIsOpen = true;
				m_pSpeakerComponent->Stop(m_GateSounds.sGateMove);
				m_pSpeakerComponent->Start(m_GateSounds.sGateStop);
			}
		}
	}
	//---------------------------------------------------------------------------------------------------
	inline void GateComponent::OpenGate()
	{
		if (m_bIsOpen == false || m_bIsClosing)
		{
			m_bIsClosing = false;
			m_bIsOpening = true;
			m_pSpeakerComponent->Start(m_GateSounds.sGateMove);
		}
	}
	//---------------------------------------------------------------------------------------------------
	inline void GateComponent::CloseGate()
	{
		if (m_bIsOpen || m_bIsOpening)
		{
			m_bIsClosing = true;
			m_bIsOpening = false;
			m_pSpeakerComponent->Start(m_GateSounds.sGateMove);
		}
	}
	//---------------------------------------------------------------------------------------------------
	inline bool GateComponent::IsOpen() const
	{
		return m_bIsOpen;
	}
	//---------------------------------------------------------------------------------------------------
	inline bool GateComponent::IsCloded() const
	{
		return m_bIsOpen == false;
	}
	//---------------------------------------------------------------------------------------------------
	// Returns true if the gate arrived at target
	inline bool GateComponent::Move(Helix::Math::float3 _vTarget, float _fDeltaTime)
	{
		Helix::Math::float3 vPos = GetParent()->GetPosition();
		Helix::Math::float3 vDir = _vTarget - vPos;
		float fMoveDistance = _fDeltaTime * m_Desc.fSpeed;
		
		// would the gate reach the target this update?
		if (vDir.magnitude() < fMoveDistance)
		{
			GetParent()->SetPosition(_vTarget);
			return true;
		}

		// move the gate towards the target
		vPos += vDir.getNormalized() * fMoveDistance;
		GetParent()->SetPosition(vPos);
		return false;
	}
	//---------------------------------------------------------------------------------------------------
}

#endif // !GATECOMPONENT_H
