//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "TheRedLine.h"
#include "Engine\D3DWidget.hpp"
#include "hlx\src\StandardDefines.h"
#include "MainScript.h"
#include "Scene\RenderingScene.h"

using namespace Helix;
using namespace RedLine;
//---------------------------------------------------------------------------------------------------

TheRedLine::TheRedLine(): QMainWindow(nullptr)
{
	ui.setupUi(this);

	QStringList Args = QApplication::arguments();
	if (Args.size() > 1u)
	{
		MainScript::Instance().SetLevelName(STR(Args[1].toStdWString()));
	}
	
	m_pD3DWidget = new Engine::D3DWidget(true, this);

	/// Register Custom Render Passes
	m_pSolidPass = std::make_shared<Display::SolidEffectRenderPass>();
	m_pD3DWidget->GetRenderer().SetSolidRenderPass(m_pSolidPass);

	/// Initialize D3D View
	m_pD3DWidget->Initialize(S("TheRedLine"));


	m_pD3DWidget->setFocus();
	move(0, 0);

	if (m_pD3DWidget->IsRendererFullScreen())
	{
		setWindowState(Qt::WindowFullScreen);
		setWindowFlags(Qt::CustomizeWindowHint | Qt::FramelessWindowHint);

		HideToolBars(true);
	}

	setCentralWidget(m_pD3DWidget);

	m_pTimer = new QTimer(this);

	connect(m_pTimer, SIGNAL(timeout()), this, SLOT(UpdateTitle()));
	m_pTimer->setInterval(1000);
	m_pTimer->start();
}
//---------------------------------------------------------------------------------------------------

TheRedLine::~TheRedLine()
{
	if (m_pTimer != nullptr)
	{
		m_pTimer->stop();
	}
	HSAFE_DELETE(m_pTimer);
	HSAFE_DELETE(m_pD3DWidget);
}
//---------------------------------------------------------------------------------------------------
void TheRedLine::HideToolBars(bool _bHide)
{
	QList<QToolBar *> toolbars = findChildren<QToolBar *>();

	if (_bHide)
	{
		for (QToolBar* pToolBar : toolbars)
		{
			pToolBar->hide();
		}

		statusBar()->hide();
	}
	else
	{
		for (QToolBar* pToolBar : toolbars)
		{
			pToolBar->show();
		}

		statusBar()->show();
	}
}
//---------------------------------------------------------------------------------------------------
void TheRedLine::UpdateTitle()
{
	float fFPS = 1.f / static_cast<float>(Scene::RenderingScene::Instance()->GetExecutionTime());

	setWindowTitle(QString::asprintf("TheRedLine - %f", fFPS));
}
//---------------------------------------------------------------------------------------------------
