//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef STATUECOMPONENT_H
#define STATUECOMPONENT_H

#include "DataStructures\GameObjectComponent.h"
#include "DataStructures\ShapeComponent.h"
#include "Physics\ControllerComponent.h"
#include "Async\SpinLock.h"
#include "Physics\QueryFilterCallback.h"
#include "Scene\CameraComponent.h"
#include "PlayerScript.h"
#include "DataStructures\TransformHierarchyComponent.h"

#include <random>
#include <vector>
#include "GameEntityDefines.h"

namespace RedLine
{
	class StatueUserData : public Helix::Datastructures::UserData
	{
	public:
		struct InternalStatueUserData
		{
			float  fImplosionProgress = 1.0f; // should decrease from x -> 0 over time
			float  fTailLength = 1.0f; // scales the tail length of a moving object
			BOOL   bImplode = true;
			BOOL   bTail = true;
			Helix::Math::float3 vVelocity = Helix::Math::float3(0.0f,0.0f,0.5f);
		};
	public:
		StatueUserData() :
			UserData(RedLineUserData_Statue, sizeof(StatueUserData)) {}
	public:
		InternalStatueUserData m_Data;
	};

	struct StatueSettings
	{
		float fTriggerRadius = 15.0f;
		float fControllerRadius = 0.5f;
		float fControllerHeight = 0.5f;
		float fMaxMoveSpeed = 2.0f; // m/s
		float fCatchDistance = 1.1f;
		float fWanderTargetRadius = 0.8f;
		float fMaxWanderTargetDistance = 1.5f;
		float fAcceleration = 4.0f; // m/s^2
		uint32_t uTargetSelectionTries = 25u;
		float fAltitudeBound = 0.39269908f; // pi/8
		float fStatueFreezeThreshold = 0.39269908f; // pi/32
		float fScareSoundTime = 10.0f;
		float fScareMaxDistance = 5.0f;
		float fPlayerBias = 4.0f;
		float fEyesFlickerDuration = 1.5f;
		//float fCatchRadius = 0.6f;
		float fImplosionDuration = 2.0f;
	};

	class StatueComponent : public Helix::Datastructures::IGameObjectComponent
	{
		StatueComponent(Helix::Datastructures::GameObject* _pParent, StatueSettings _Settings);
		COMPTYPENAME(StatueComponent, Helix::Datastructures::GameObject, 'Stat');
		COMPDESCONSTR(StatueComponent, Helix::Datastructures::GameObject, IGameObjectComponent);
		COMPDEFCONSTR(StatueComponent, Helix::Datastructures::GameObject, IGameObjectComponent);

	public:
		void OnUpdate(double _fDeltaTime, double _fTotalTime) final;
		void OnLevelLoaded() final;

		Helix::Datastructures::SyncComponent* GetSyncComponent() final;

		float GetTriggerRadius() const;
		void SetTriggerRadius(const float _fTriggerRadius);

		void SetActive(const bool _bActive);
		bool GetActive() const;
		void Reset();

	private:
		void SetTarget(Helix::Datastructures::GameObject* _pTarget);
		void Idle();

		bool Move(float _fDeltaTime);
		void Wander(float _fDeltaTime);

	public:
		static void OnTrigger(Helix::Physics::TriggerInfo& _Info);
		static void OnContact(Helix::Physics::ContactInfo& _Info);

	private:
		StatueUserData m_RenderUserData;
		StatueSettings m_Settings;
		float m_fCurrentSpeed = 0.0f;
		bool m_bActive = false;
		bool m_bSeeking = false;
		bool m_bFreezed = true;
		bool m_bSeenByPlayer = false;
		float m_fTimeSinceLastScare = 0.0f;
		float m_fTimeSinceLastFreeze = 0.0f;
		Helix::Math::float3 m_vOrigin = Helix::Math::float3(0, 0, 0);
		Helix::Math::float3 m_vTargetPos = Helix::Math::float3(0, 0, 0);
		std::string m_sStatueName;

		float m_fImplosionTime = 0.0f;

		float m_fEyesLightIntensity = 0.0f;
		float m_fEyesFlickerTime = 0.0f;
		bool m_bEyesEnabled = false;
		Helix::Scene::LightComponent* m_pLeftEye = nullptr;
		Helix::Scene::LightComponent* m_pRightEye = nullptr;
		
		std::mt19937 m_RandomEngine;
		std::uniform_real_distribution<float> m_PhiDistribution;
		std::uniform_real_distribution<float> m_ThetaDistribution;

		Helix::Datastructures::ShapeComponent* m_pShapeComponent = nullptr;
		Helix::Physics::ControllerComponent* m_pControllerComponent = nullptr;
		Helix::Datastructures::GameObject* m_pMainCamera = nullptr;
		Helix::Datastructures::GameObject* m_pTicket = nullptr;
		Helix::Datastructures::GameObject* m_pTarget = nullptr;

		Helix::Physics::GeoCapsule m_SweepGeometry;
		Helix::Physics::QueryFilterIgnore m_QueryFilter;
		
		Helix::Async::SpinLock m_StatueLock;

		Helix::Sound::Speaker* m_pSpeakerComponent = nullptr;


#ifdef HNUCLEO
		SYNOBJ(TriggerRadius, StatueComponent, float, GetTriggerRadius, SetTriggerRadius) m_SyncRadius = { *this, "Trigger Radius" };
		Helix::Datastructures::SyncComponent m_ShapeGroup = { this, "Statue", {} };
#endif
	};

	//---------------------------------------------------------------------------------------------------
	inline StatueComponent::StatueComponent(Helix::Datastructures::GameObject* _pParent, StatueSettings _Settings)
		: IGameObjectComponent(
			_pParent,
			Helix::Datastructures::ComponentCallback(
				Helix::Datastructures::ComponentCallback_OnUpdate
				| Helix::Datastructures::ComponentCallback_OnLevelLoaded)),
		m_Settings(_Settings),
		m_PhiDistribution(m_Settings.fAltitudeBound, 6.28318530f - m_Settings.fAltitudeBound),
		m_ThetaDistribution(0.0f, 3.14159265f),
		m_sStatueName(_pParent->GetName())
	{
		using namespace Helix;
		Datastructures::GameObject* pParent = GetParent();
		m_vOrigin = pParent->GetPosition();
		m_Settings = _Settings;

		// the main camera will be the target of the statue
		m_pMainCamera = Datastructures::GameObjectPool::Instance()->GetObjectByName("MainCamera");
		m_pTarget = m_pMainCamera;
		m_pTicket = Datastructures::GameObjectPool::Instance()->GetObjectByName("ticket");

		// the statue is activated if the player walks into the trigger shape
		Physics::GeometryDesc TriggerDesc;
		TriggerDesc.kUsage = Physics::ShapeUsageType_Trigger;
		TriggerDesc.kType = Physics::GeometryType_Sphere;
		TriggerDesc.Sphere.fRadius = m_Settings.fTriggerRadius;
		m_pShapeComponent = pParent->AddComponent<Datastructures::ShapeComponent>(TriggerDesc);
		m_pShapeComponent->RegisterCallback(Physics::TriggerEvent::Callback(&OnTrigger));

		// the abstraction on movement for the statue
		Physics::CapsuleControllerDesc ControllerDesc;
		ControllerDesc.fHeight = m_Settings.fControllerHeight;
		ControllerDesc.fRadius = m_Settings.fControllerRadius;
		ControllerDesc.vPosition = GetParent()->GetPosition();
		ControllerDesc.fStepOffset = 0.2f;
		m_pControllerComponent = GetParent()->AddComponent<Physics::ControllerComponent>(ControllerDesc);

		m_QueryFilter.AddIgnore(GetParent());
		m_QueryFilter.AddIgnore(m_pMainCamera);
		m_SweepGeometry.halfHeight = 0.0f;//m_Settings.fControllerHeight / 8.0f;
		m_SweepGeometry.radius = m_Settings.fControllerRadius / 8.0f;

		m_pSpeakerComponent = pParent->AddComponent<Sound::Speaker>();
		m_pSpeakerComponent->SetVolume(1.0f);
		m_pSpeakerComponent->AddVoice(S("statue_move"), true);
		m_pSpeakerComponent->SetVolume(S("statue_move"), 1.0f);
		m_pSpeakerComponent->AddVoice(S("statue_scare"));
		m_pSpeakerComponent->AddVoice(S("statue_stop"));
		m_pSpeakerComponent->SetDistanceFalloff(0.2f);

		Datastructures::TransformHierarchyComponent* pStatueTHC = GetParent()->AddComponent<Datastructures::TransformHierarchyComponent>();

		// left eye
		Datastructures::GameObject* pEye = Datastructures::GameObjectPool::Instance()->Create().get();
		m_pLeftEye = pEye->AddComponent<Scene::LightComponent>(Scene::LightType_Point);
		m_pLeftEye->Color = Math::float3(1.0f, 0.0f, 0.0f);
		m_pLeftEye->Intensity = 0.0f;
		Datastructures::TransformHierarchyComponent* pEyeTHC = pEye->AddComponent<Datastructures::TransformHierarchyComponent>();
		pEyeTHC->SetLocalTransform({ 0.0f, 0.606861f, 0.136657f });
		pEyeTHC->SetParentNode(pStatueTHC);

		// right eye
		pEye = Datastructures::GameObjectPool::Instance()->Create().get();
		m_pRightEye = pEye->AddComponent<Scene::LightComponent>(Scene::LightType_Point);
		m_pRightEye->Color = Math::float3(1.0f, 0.0f, 0.0f);
		m_pRightEye->Intensity = 0.0f;
		pEyeTHC = pEye->AddComponent<Datastructures::TransformHierarchyComponent>();
		pEyeTHC->SetLocalTransform({ 0.0564645f, 0.606861f, 0.145176f });
		pEyeTHC->SetParentNode(pStatueTHC);

		pParent->SetUserData(&m_RenderUserData);

		std::vector<Helix::Datastructures::ShapeComponent*> Shapes;
		GetParent()->GetComponentsByType <Helix::Datastructures::ShapeComponent>(Shapes);
		for (auto Shape : Shapes)
		{
			GetParent()->RemoveComponent(Shape);
		}
	}
	//---------------------------------------------------------------------------------------------------
	inline StatueComponent::~StatueComponent()
	{
	}
	//---------------------------------------------------------------------------------------------------
	inline void StatueComponent::OnLevelLoaded()
	{

	}
	//---------------------------------------------------------------------------------------------------
	inline Helix::Datastructures::SyncComponent* StatueComponent::GetSyncComponent()
	{
#ifdef HNUCLEO
		return &m_ShapeGroup;
#else
		return nullptr;
#endif
	}
	//---------------------------------------------------------------------------------------------------
	inline float StatueComponent::GetTriggerRadius() const
	{
		return m_Settings.fTriggerRadius;
	}
	//---------------------------------------------------------------------------------------------------
	inline void StatueComponent::SetTriggerRadius(const float _fTriggerRadius)
	{
		m_Settings.fTriggerRadius = _fTriggerRadius;
	}
	//---------------------------------------------------------------------------------------------------
	inline void StatueComponent::SetActive(const bool _bActive)
	{
		m_bActive = _bActive;
		if (m_bActive)
		{
			// the abstraction on movement for the statue
			Helix::Physics::CapsuleControllerDesc ControllerDesc;
			ControllerDesc.fHeight = m_Settings.fControllerHeight;
			ControllerDesc.fRadius = m_Settings.fControllerRadius;
			ControllerDesc.vPosition = GetParent()->GetPosition();
			ControllerDesc.fStepOffset = 0.2f;
			m_pControllerComponent->CreateBody(ControllerDesc);

			//std::vector<Helix::Datastructures::ShapeComponent*> Shapes;
			//GetParent()->GetComponentsByType <Helix::Datastructures::ShapeComponent>(Shapes);
			//for (auto Shape : Shapes)
			//{
			//	Shape->ActivateUsage(true);
			//}
		}
		else
		{
			if (m_pSpeakerComponent != false)
			{
				m_pSpeakerComponent->Stop(S("statue_move"));
			}
			m_bSeenByPlayer = false;
			m_bEyesEnabled = false;
			m_fEyesLightIntensity = 0.0f;
			m_fEyesFlickerTime = 0.0f;
			m_fImplosionTime = 0.0f;
			m_RenderUserData.m_Data.bImplode = false;
			m_RenderUserData.m_Data.fImplosionProgress = 0.0f;

			m_pControllerComponent->DestroyBody();
		}
	}
	//---------------------------------------------------------------------------------------------------
	inline bool StatueComponent::GetActive() const
	{
		return m_bActive;
	}
	//---------------------------------------------------------------------------------------------------
	inline void StatueComponent::SetTarget(Helix::Datastructures::GameObject * _pTarget)
	{
		using namespace Helix;
		if (_pTarget != m_pTarget && m_bActive)
		{
			Async::ScopedSpinLock Lock(m_StatueLock);
			m_pTarget = _pTarget;
		}
	}
	//---------------------------------------------------------------------------------------------------
	inline void StatueComponent::Idle()
	{
		using namespace Helix;
		Async::ScopedSpinLock Lock(m_StatueLock);
		m_pTarget = nullptr;
	}
	//---------------------------------------------------------------------------------------------------
	inline void StatueComponent::Reset()
	{
		using namespace Helix;
		if (m_pControllerComponent != nullptr)
		{
			m_pControllerComponent->Teleport(m_vOrigin);
		}
		else
		{
			GetParent()->SetPosition(m_vOrigin);
		}
		m_bSeenByPlayer = false;
	}
	//---------------------------------------------------------------------------------------------------
	inline void StatueComponent::OnUpdate(double _fDeltaTime, double _fTotalTime)
	{
		using namespace Helix;

		if(m_RenderUserData.m_Data.bImplode)
		{
			m_fImplosionTime += _fDeltaTime;
			float fX = m_fImplosionTime / m_Settings.fImplosionDuration;

			m_RenderUserData.m_Data.bImplode = true;
			m_RenderUserData.m_Data.fImplosionProgress = 1.0f - fX;

			if(m_RenderUserData.m_Data.fImplosionProgress <= 0.0f)
			{
				m_RenderUserData.m_Data.fImplosionProgress = 0.0f;
				m_RenderUserData.m_Data.bImplode = false;
			}
		}
		else
		{
			m_RenderUserData.m_Data.fImplosionProgress = 0.f;
		}

		if (m_bEyesEnabled)
		{
			if (m_fEyesLightIntensity < 1.0f)
			{
				m_fEyesFlickerTime += _fDeltaTime;
				float fX = m_fEyesFlickerTime / m_Settings.fEyesFlickerDuration;
				m_fEyesLightIntensity = Math::Saturate(sinf(powf(fX, 4.0f) * 50.0f) * fX);
			}
			else
			{
				m_fEyesFlickerTime = 0.0f;
				m_fEyesLightIntensity = 1.0f;
			}
		}
		else
		{
			m_fEyesLightIntensity = 0.0f;
		}
		if (m_pLeftEye != nullptr)
		{
			m_pLeftEye->Intensity = m_fEyesLightIntensity;
		}
		if (m_pRightEye != nullptr)
		{
			m_pRightEye->Intensity = m_fEyesLightIntensity;
		}
		
		m_fTimeSinceLastScare += _fDeltaTime;
		m_fTimeSinceLastFreeze += _fDeltaTime;
		// the actual seeking 'algorithm'
		if (m_bActive)
		{
			if (Move(static_cast<float>(_fDeltaTime)))
			{
				// the statue is moving
				if (m_bFreezed)
				{
					m_pSpeakerComponent->Start(S("statue_move"));
					m_bFreezed = false;
				}
			}
			else
			{
				// player looks at statue -> freeze
				if (m_bFreezed == false)
				{
					m_pSpeakerComponent->Stop(S("statue_move"));
					if(m_fTimeSinceLastFreeze > 2.0f)
					{
						m_pSpeakerComponent->Start(S("statue_stop"));
					}
					m_fTimeSinceLastFreeze = 0.0f;
					if (m_fTimeSinceLastScare > m_Settings.fScareSoundTime)
					{
						Math::float3 vCamPos = m_pMainCamera->GetPosition();
						Math::float3 vStatuePos = GetParent()->GetPosition();
						float fDistance = (vCamPos - vStatuePos).magnitude();
						if (fDistance <= m_Settings.fScareMaxDistance)
						{
							m_pSpeakerComponent->Start(S("statue_scare"));
						}
					}
					m_fTimeSinceLastScare = 0.0f;
					m_bFreezed = true;
				}
			}
		}
	}
	//---------------------------------------------------------------------------------------------------
	inline bool StatueComponent::Move(float _fDeltaTime)
	{
		using namespace Helix;
		
		Async::ScopedSpinLock Lock(m_StatueLock);

		Math::float3 vStatuePos = GetParent()->GetPosition();
		Math::float3 vCameraPos = m_pMainCamera->GetPosition();
		
		// only move the statue if it can see the player, so that the statue does not walk into walls
		Scene::CameraComponent* pCamera = m_pMainCamera->GetComponent<Scene::CameraComponent>();
		Math::float3 vCameraLook = pCamera->GetLook();
		Math::float3 vDirection = vCameraPos - vStatuePos;

		// compute the angle under which the statue is visible relative to the look direction of the camera
		Math::float3 v2DDirection = vStatuePos - vCameraPos;
		v2DDirection.y = 0.0f;
		v2DDirection.normalize();
		Math::float3 v2DLook = vCameraLook;
		v2DLook.y = 0.0f;
		v2DLook.normalize();
		float fStatueAngle = acosf(v2DDirection.dot(v2DLook));
		float fViewAngle = pCamera->GetFovX() / 2.0f;

		if ((fViewAngle - fStatueAngle) >= - m_Settings.fStatueFreezeThreshold)
		{
			Math::float3 vDir = vDirection.getNormalized();
			Math::float3 vRayOriginOffset = Math::float3(0.0f, 1.0f, 0.0f).cross(vDir) * m_Settings.fControllerRadius * 1.3f;
			// the statue is inside -> don't move
			Datastructures::GameObject* pHit = nullptr;
			Math::float3 vRayOrigin = vStatuePos + vRayOriginOffset;
			Scene::PhysicsScene::Instance()->Raycast(vRayOrigin, (vCameraPos - vRayOrigin).getNormalized(), 1000.0f, pHit, GetParent());
			if (pHit != nullptr && (pHit  == m_pMainCamera || pHit == m_pTicket))
			{
				m_bSeenByPlayer = true;
				m_bEyesEnabled = true;
				m_RenderUserData.m_Data.bImplode = true;
				return false;
			}

			vRayOriginOffset *= -1.0f;
			vRayOrigin = vStatuePos + vRayOriginOffset;
			Scene::PhysicsScene::Instance()->Raycast(vRayOrigin, (vCameraPos - vRayOrigin).getNormalized(), 1000.0f, pHit, GetParent());

			if (pHit != nullptr && (pHit == m_pMainCamera || pHit == m_pTicket))
			{
				m_bSeenByPlayer = true;
				m_bEyesEnabled = true;
				m_RenderUserData.m_Data.bImplode = true;
				return false;
			}
		}

		if (m_bSeenByPlayer == false)
		{
			return false;
		}

		if (vDirection.magnitude() > m_Settings.fCatchDistance)
		{
			Datastructures::GameObject* pHit = nullptr;
			Datastructures::GameObject* pParent = GetParent();
			bool bHit = Scene::PhysicsScene::Instance()->Raycast(vStatuePos, vDirection.getNormalized(), m_Settings.fTriggerRadius, pHit, GetParent());
			if (pHit && (pHit == m_pMainCamera || pHit == m_pTicket))
			{
				if (m_fCurrentSpeed < m_Settings.fMaxMoveSpeed)
				{
					// head with full acceleration towards the target 
					m_fCurrentSpeed = m_fCurrentSpeed + _fDeltaTime * m_Settings.fAcceleration;
					// clamp speed to maximum
					m_fCurrentSpeed = min(m_fCurrentSpeed, m_Settings.fMaxMoveSpeed);
				}

				m_pControllerComponent->LookAt(m_pMainCamera->GetPosition());
				m_pControllerComponent->Walk(m_fCurrentSpeed * _fDeltaTime);
			}
			else
			{
				Wander(_fDeltaTime);
			}
		}
		else
		{

#ifndef HNUCLEO // HACK: we really need to fix separation of game and engine

			// Statue is near enough to catch the player
			PlayerScript::Instance().Catch(vStatuePos);
#endif // !HNUCLEO
		}
		return true;
	}
	//---------------------------------------------------------------------------------------------------
	inline void StatueComponent::Wander(float _fDeltaTime)
	{
		using namespace Helix;

		Math::transform Pose = GetParent()->GetTransform();

		// if the statue is moving, the center of distribution lies ahead and biases movement to forward direction
		float fDistance = m_Settings.fMaxWanderTargetDistance * m_fCurrentSpeed / m_Settings.fMaxMoveSpeed;
		Math::float3 vTargetSelectionCenter = m_pControllerComponent->GetLook() * fDistance + Pose.p;

		Math::float3 vCameraDir = m_pMainCamera->GetPosition() - vTargetSelectionCenter;
		Math::float3 vCameraXZ = vCameraDir;
		vCameraXZ.y = 0.0f;
		vCameraXZ.normalize();
		float fPhiPlayer = acosf(vCameraDir.getNormalized().dot(vCameraXZ));
		float fThetaPlayer = atan2f(vCameraDir.x, vCameraDir.z) - atan2f(0, 1);
		float fStdDev = 3.14159f / m_Settings.fPlayerBias;
		std::normal_distribution<float> PhiDistribution(fPhiPlayer, fStdDev);
		std::normal_distribution<float> ThetaDistribution(fThetaPlayer, fStdDev);

		// try to find a target on a sphere around the center of distribution
		uint32_t uTryNum;
		for (uTryNum = 0u; uTryNum < m_Settings.uTargetSelectionTries; ++uTryNum)
		{
			// random spherical coordinates
			float fPhi = PhiDistribution(m_RandomEngine); // Rad from zenith
			float fTheta = ThetaDistribution(m_RandomEngine); // Azimuth
			
			//float fSpeedScaler = (1.2f * m_Settings.fMaxMoveSpeed - m_fCurrentSpeed) / (1.2f * m_Settings.fMaxMoveSpeed);

			// point on target sphere
			Math::float3 vProposedTarget;
			vProposedTarget.x = sinf(fTheta) * cosf(fPhi);
			vProposedTarget.y = sinf(fPhi);
			vProposedTarget.z = cosf(fTheta) * cosf(fPhi);
			vProposedTarget *= m_Settings.fWanderTargetRadius;
			vProposedTarget += vTargetSelectionCenter;

			// sweep params
			Math::float3 vDirection = vProposedTarget - Pose.p;
			float fDistance = vDirection.magnitude();
			vDirection.normalize();
			Datastructures::GameObject* pHit = nullptr;
			
			// validate proposed target with sweep (no block means the statue can move to the destination)
			bool bHit = Scene::PhysicsScene::Instance()->Sweep(m_SweepGeometry, Pose, vDirection, fDistance, m_QueryFilter, pHit);
			
			// set proposed point as new target if destination is valid
			if (bHit == false)
			{
				m_vTargetPos = vProposedTarget;
				break;
			}
		}

		// if no target is found, slow down and arrive at some location ahead of the obstacle
		if (uTryNum == m_Settings.uTargetSelectionTries)
		{
			// arrive update for velocity
			// v_n+1 = v_n - 1/d * v_n
			m_fCurrentSpeed -= m_fCurrentSpeed * _fDeltaTime / m_Settings.fMaxWanderTargetDistance;
			m_fCurrentSpeed = max(m_fCurrentSpeed, 0.0f);
		}
		// if a target is found, move with full acceleration towards it
		else
		{
			m_fCurrentSpeed += m_Settings.fAcceleration * _fDeltaTime / m_Settings.fMaxWanderTargetDistance;
			m_fCurrentSpeed = min(m_fCurrentSpeed, m_Settings.fMaxMoveSpeed);
		}

		// move towards the target
		m_pControllerComponent->LookAt(m_vTargetPos);
		m_pControllerComponent->Walk(m_fCurrentSpeed * _fDeltaTime);
	}
	//---------------------------------------------------------------------------------------------------
	inline void StatueComponent::OnTrigger(Helix::Physics::TriggerInfo & _Info)
	{
		using namespace Helix;
		if (_Info.pActivator->GetName() == "MainCamera")
		{
			StatueComponent* Statue = _Info.pTrigger->GetComponent<StatueComponent>();
			if (_Info.State == Physics::SimulationEventState_Enter)
			{
				Statue->SetTarget(_Info.pActivator);
			}
			else if (_Info.State == Physics::SimulationEventState_Leave)
			{
				//Statue->Reset();
			}
		}
	}
	//---------------------------------------------------------------------------------------------------
	inline void StatueComponent::OnContact(Helix::Physics::ContactInfo & _Info)
	{
	}
}

#endif