//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef SOLIDEFFECTRENDERPASS_H
#define SOLIDEFFECTRENDERPASS_H

#include "Display/DX11/HelixSolidRenderPassDX11.h"
#include "GameEntityDefines.h"
#include "StatueComponent.h"

namespace Helix
{
	namespace Display
	{
		class SolidEffectRenderPass : public HelixSolidRenderPassDX11
		{
		public:
			HDEBUGNAME("SolidEffectRenderPass");

			bool OnPerObject(const RenderObjectDX11& _RenderObject, const MaterialDX11& _Material) final;
			/// TODO: clean up after demo day
			static bool bDestroyEverything;
		};
		//---------------------------------------------------------------------------------------------------
		inline bool SolidEffectRenderPass::OnPerObject(const RenderObjectDX11& _RenderObject, const MaterialDX11& _Material)
		{
			bool bResult = HelixSolidRenderPassDX11::OnPerObject(_RenderObject, _Material);

			bool bStatue = false;
			Datastructures::UserData* pUserData = _RenderObject.GetUserData();
			if(pUserData != nullptr && bDestroyEverything == false)
			{
				if(pUserData->kType == RedLineUserData_Statue)
				{
					RedLine::StatueUserData* pStatueUserData = pUserData->Get<RedLine::StatueUserData>(RedLineUserData_Statue);
					if(pStatueUserData != nullptr)
					{
						m_CBSolid->fEffectStrength = 5.0f;
						m_CBSolid->fEffectRadius = 1.5f;
						m_CBSolid->fEffectProgress = pStatueUserData->m_Data.fImplosionProgress;
						m_CBSolid->vVelocity = Math::float4(pStatueUserData->m_Data.vVelocity, 0.0f);
						m_CBSolid->bImplode = pStatueUserData->m_Data.bImplode;
						bStatue = pStatueUserData->m_Data.bImplode;
					}
				}
			}

			if(bDestroyEverything)
			{
				/// TODO: clean up after demo day
				float fGlobalTime = hlx::Timer::GetGlobalTimeF();
				bStatue = bDestroyEverything;
				m_CBSolid->fEffectStrength = -15.0f;
				m_CBSolid->fEffectRadius = 20.5f;
				m_CBSolid->fEffectProgress = (sinf(fGlobalTime*0.1f) + 1.0f) * 0.5f;
				m_CBSolid->bImplode = bDestroyEverything;
			}

			GetShader()->EnableStage(ShaderType_GeometryShader, bStatue || bDestroyEverything);

			return bResult;
		}
	} // Display
} // Helix

#endif // SOLIDEFFECTRENDERPASS_H