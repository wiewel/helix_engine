//   Copyright 2020 Fabian Wahlster
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef RESOURCETOOL_H
#define RESOURCETOOL_H

#include <QtWidgets/QMainWindow>
#include "ui_resourcetool.h"
#include "TreeModel.h"
#include "WorkerThread.h"

#include "Resources\ResourceFileFormat.h"

class ResourceTool : public QMainWindow
{
	Q_OBJECT

public:
	ResourceTool(QWidget *parent = 0);
	~ResourceTool();

public slots:
	void openProject();
	void saveProject();

	void selectionChanged();
	void changeButtonPressed();

	void newStatusMessage(const QString &msg);

	void setNewModel();
private:
    int setPreloadRecursive(TreeItem *item,bool preload, Helix::Resources::ResourceEntryFlag entryType, Helix::Resources::ResourceType resourceType);

	QString curProjectDir;
	QString curRifFile;
	TreeItem *selectedEntry;
	TreeModel *model;
	
	WorkerThread *worker;

	Ui::ResourceToolClass ui;
};

#endif // RESOURCETOOL_H
