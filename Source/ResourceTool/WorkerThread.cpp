//   Copyright 2020 Fabian Wahlster
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "WorkerThread.h"

#include <QDirIterator>
#include <QFileInfo>
#include "Compression.h"

WorkerThread::WorkerThread(){
	resFile = nullptr;
	useResouceFile = false;
	save = false;
	compressionLevel = -1;
}

void WorkerThread::initialize(TreeItem *rootItem, QString dirPath){
	root = rootItem;
	rootPath = dirPath;
	curID = 0;
	useResouceFile = false;
	save = false;
}

void WorkerThread::initialize(TreeItem *rootItem, QString dirPath, QString resourceFilePath){
	root = rootItem;
	rootPath = dirPath;
	resFilePath = resourceFilePath;
	curID = 0;
	save = false;
	useResouceFile = true;
}

void WorkerThread::initialize(TreeItem *rootItem, QString _outputPath,const ResourceIndexFileHeader &header, uint32_t _packageSize,int _compressionLevel){
	root = rootItem;
	outputPath = _outputPath;
	curID = 0;
	save = true;
	useResouceFile = false;
	resHeader = header;
	packageSize = _packageSize;
	compressionLevel = _compressionLevel;
}

WorkerThread::~WorkerThread(){
	if(resFile != nullptr) {
		delete resFile;
		resFile = nullptr;
	}
}

void WorkerThread::run(){
	curID = 0;
	root->resourceEntry.fileID = 0;
	curPackageSize = 0;

	if(save){
		QList<TreeItem*> entries; entries.append(root);
		harvestTree(root,entries);
		build(entries);
		entries.clear();
	}else{
		emit sendMessage("Constructing " + rootPath + "...");
		
		if(useResouceFile) {
			if(resFile != nullptr) delete resFile;
			resFile = new ResourceFile(resFilePath.toStdString(),rootPath.toStdString());
			if(resFile->Read() == false){
				emit sendMessage("Failed to load " + resFilePath + " check log file");
				sleep(2);
				delete resFile; resFile = nullptr;
				useResouceFile = false;
			}
		}

		construct(root,rootPath);

		if(resFile != nullptr) {
			delete resFile;
			resFile = nullptr;
		}
	
		emit sendMessage("Constructing " + rootPath + "...Done! [" + QString::number(curID) + " Entries]");
	}
}

//This function linearizes  the tree structure to a list for easier processing
//note that the entries are not in a correct order, but that doesnt matter since it will be corrected when read
void WorkerThread::harvestTree(TreeItem *parent,QList<TreeItem*> &out){
	if(parent->childCount() == 0) return;
	int offset = out.size();
	out.append(parent->getChildren());
	int end =  out.size();
	for(;offset < end;++offset){
		if(out.at(offset)->childCount() > 0) harvestTree(out.at(offset),out);
	}
}

void WorkerThread::build(QList<TreeItem*> &in){
	//construct output rif filepath
	QString packagePath = QDir::toNativeSeparators(outputPath) + "\\" + QString::fromStdString(resHeader.packageIdentifier);

	stdrzr::fbytestream rif(packagePath.toStdString() + ".rif", std::ios::out | std::ios::binary);
	if (rif.is_open() == false){
		emit sendMessage("Unable to open " + resFilePath + ".rif");
		return;
	}

	uint32_t dataoffset = resHeader.Size();

	packagePath += "_";
	//skip the header, write it later
	rif.seekp(dataoffset, std::ios::beg);

	uint32_t curPackageSize = 0;
	uint32_t curPackageIndex = 0;
	TreeItem *entry = nullptr;

	stdrzr::fbytestream curPackage;
	stdrzr::bytes buffer;
	for (int i = 0; i < in.size(); ++i){
		float percentage = ((float)i / (float)in.size())*100.f;
		emit progressUpdated((int)percentage);
		entry = in.at(i);
		//open new package
		if (i == 0 || curPackageSize >= packageSize){
			//close the previous package
			if (curPackage.is_open()) {
				curPackage.flush();
				curPackage.close();
				++curPackageIndex;
			}

			curPackage.open((packagePath + QString::number(curPackageIndex)).toStdString() + ".rpf", std::ios::out | std::ios::binary);

			if (curPackage.is_open() == false){
				emit sendMessage("Unable to open " + packagePath + QString::number(curPackageIndex) + ".rpf");
				rif.close();
				return;
			}

			ResourcePackageFileHeader rpfHeader;
			rpfHeader.fileVersion = resHeader.fileVersion;
			rpfHeader.packageIndex = curPackageIndex;
			rpfHeader.packageIdentifier = resHeader.packageIdentifier;

			rpfHeader.Write(curPackage);

			curPackageSize = rpfHeader.Size(); 
		}

		entry->resourceEntry.packageIndex = curPackageIndex;

		//if the entry is a dir we dont need to write anything to the package file
		if(entry->resourceEntry.entryType == HRESOURCE_ENTRY_TYPE_E::directory){
			entry->resourceEntry.packageIndex = HR_INVALID;
			entry->resourceEntry.Write(rif);
			continue;
		}

		stdrzr::fbytestream file(QDir::toNativeSeparators(entry->path).toStdString(), std::ios::in | std::ios::binary);
		if(file.is_open() == false){
			emit sendMessage("Unable to open " + entry->path);
			curPackage.close();
			rif.close();
			return;
		}

		emit sendMessage("Processing " + entry->path);

		//read the file
		buffer.clear();
		buffer = file.get<stdrzr::bytes>(file.size());
		file.close();

		//compute checksum before compressing to be able to verify if the uncompressed data is valid
		entry->resourceEntry.checksum = zlib::crc32(buffer);
		
		//compress the file if needed
		if(entry->resourceEntry.entryType == HRESOURCE_ENTRY_TYPE_E::compressed_file ||
			entry->resourceEntry.entryType == HRESOURCE_ENTRY_TYPE_E::compressed_file_local_has_priority){
				emit sendMessage("Processing " + entry->path + " (Compressing)");
			buffer = zlib::compress(buffer,compressionLevel);
		}

		//finally set the offset and size parameters and write the file to the package/preload
		if(entry->preload){
			entry->resourceEntry.preloadOffset = (uint32_t)rif.tellp();
			entry->resourceEntry.preloadSize = (uint32_t)buffer.size();
			//preload size does not count into curPackageSize
			entry->resourceEntry.Write(rif,buffer);
		}else{
			entry->resourceEntry.preloadSize = 0;
			//write to package
			entry->resourceEntry.offset = curPackageSize;
			entry->resourceEntry.size = buffer.size();
			curPackageSize += entry->resourceEntry.size;
			entry->resourceEntry.Write(rif);
			curPackage.put<stdrzr::bytes>(buffer);
		}		
	}

	//set the new info and write the header to the file
	resHeader.entryCount = in.size();
	resHeader.packageCount = curPackageIndex+1;

	rif.seekp(0,std::ios::beg);

	resHeader.Write(rif);

	//close all files;
	rif.flush();
	rif.close();
	curPackage.flush();
	curPackage.close();

	emit progressUpdated(100);
	emit sendMessage("Building " + QString::fromStdString(resHeader.packageIdentifier) + " done!");
}

//recursive function
void WorkerThread::construct(TreeItem *parent, QString path){
	QDirIterator dir(path,QDir::NoDotAndDotDot | QDir::NoSymLinks | QDir::Files | QDir::Dirs | QDir::Readable,QDirIterator::NoIteratorFlags);
	QFileInfo file; QString curPath;TreeItem *child;
	const ResourceEntry *resEntry;ResourceEntry entry;
	TreeItem *previous = nullptr;std::string searchPath;
	while(dir.hasNext()){		
		resEntry = nullptr;

		curPath = dir.next();
		file = dir.fileInfo();//needed for size etc

		entry.name = dir.fileName().toStdString();

		entry.parent = parent->resourceEntry.fileID;
		entry.fileID = ++curID;//increment global file id

		if(useResouceFile && resFile != nullptr){
			//check if the entry with the same id in the resource file has the same name
			resEntry = resFile->getEntry(curID);
			if(!(resEntry != nullptr && entry.name.compare(resEntry->name) == 0)) {
				resEntry = nullptr;
				searchPath = curPath.toStdString().substr(rootPath.size()+1);
				if(searchPath.empty() == false)resEntry = resFile->getEntry(searchPath,'/');
				if(resEntry == nullptr)	HLOG("Could not find %s in the ResourceIndex",searchPath.c_str());
			}
		}		

		//if the parents child has not been set, its the current entry
		if(parent->resourceEntry.child == HR_INVALID) parent->resourceEntry.child = curID;
		
		if(previous != nullptr) previous->resourceEntry.next = curID;	
		
		if(file.isDir()){
			entry.entryType = HRESOURCE_ENTRY_TYPE_E::directory;
		}else{
			if(resEntry == nullptr){
				entry.entryType = HRESOURCE_ENTRY_TYPE_E::file;
			}else{
				entry.entryType = resEntry->entryType;
			}
			
			entry.size = file.size();
		}

		//Insert a new child
		if(parent->insertChildren(parent->childCount(), 1, root->columnCount()) == false){
			emit sendMessage("Failed to insert child " + dir.fileName());
			return;
		}

		//set the display and entry data of the child
		child = parent->child(parent->childCount() - 1);
		child->setData(0, QVariant(dir.fileName()));
		child->path = curPath;//path will be used when building the file

		//If we have more information from the resource file use it:
		if(resEntry != nullptr){
			child->preload = resEntry->preloadSize > 0;
			entry.resourceType = resEntry->resourceType;
			entry.offset = resEntry->offset;
			entry.preloadSize = resEntry->preloadSize;
			entry.preloadOffset = resEntry->preloadOffset;
			entry.checksum = resEntry->checksum;
			entry.packageIndex = resEntry->packageIndex;
		}

		child->resourceEntry = entry;

		//traverse into subdir if it is not empty
		if(file.isDir() && QDir(curPath).count() > 0){
			construct(child,curPath);
		} 

		entry.Reset();
		previous = child;
	}
}