//   Copyright 2020 Fabian Wahlster
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "resourcetool.h"
#include "TreeItem.h"
#include <QFileDialog>

ResourceTool::ResourceTool(QWidget *parent)
	: QMainWindow(parent)
{
	HInitDebug();
	Logger::Instance()->WriteToFile();
	selectedEntry = nullptr;
	model = nullptr;

	worker = new WorkerThread();

	ui.setupUi(this);

	QStringList entries; entries << "Directory" << "File" << "Compressed File" << "File (Local has priority)" << "Compressed File (Local has priority)";
	ui.entryType_comboBox->addItems(entries);
	entries.clear(); entries << "Config" << "Script" << "Model" << "Texture" << "Misc";
	ui.resourceType_comboBox->addItems(entries);

	connect(worker,SIGNAL(sendMessage(const QString &)),this,SLOT(newStatusMessage(const QString &)));
	connect(worker,SIGNAL(finished()),this,SLOT(setNewModel()));
	connect(worker,SIGNAL(progressUpdated(int)),ui.progressBar,SLOT(setValue(int)));

	connect(ui.actionNew,SIGNAL(triggered()),this,SLOT(openProject()));
	connect(ui.actionSave,SIGNAL(triggered()),this,SLOT(saveProject()));

	connect(ui.changeEntry_pushButton,SIGNAL(clicked()),this,SLOT(changeButtonPressed()));
}

ResourceTool::~ResourceTool(){
	if(worker != nullptr) {
		worker->wait();
		delete worker;	
	}
	if(model != nullptr)delete model;
	//Logger::Instance()->Destroy();
}

void ResourceTool::newStatusMessage(const QString &msg){
	ui.statusBar->showMessage(msg);
}

void ResourceTool::selectionChanged(){
	QModelIndex index = ui.treeView->selectionModel()->currentIndex();
	TreeItem *item = model->getItem(index);
	selectedEntry = item;
	
	//display the item
	ui.resourceEntry_groupBox->setTitle(QString::fromStdString(item->resourceEntry.name));

	ui.fileID_Label->setText(QString::number(item->resourceEntry.fileID));
	ui.parent_Label->setText(QString::number(item->resourceEntry.parent));
	ui.child_Label->setText((item->resourceEntry.child != HR_INVALID) ? QString::number(item->resourceEntry.child) : "N/A");
	ui.next_Label->setText((item->resourceEntry.next != HR_INVALID) ? QString::number(item->resourceEntry.next) : "N/A");
	
	ui.size_Label->setText(QString::number(item->resourceEntry.size / 1024.f,'f',1) + " kB");
	ui.offset_Label->setText("0x"+QString::number(item->resourceEntry.offset,16));
	ui.preloadSize_Label->setText(QString::number(item->resourceEntry.preloadSize));	
	ui.preloadOffset_Label->setText("0x"+QString::number(item->resourceEntry.preloadOffset,16));
	ui.crc32_Label->setText("0x"+QString::number(item->resourceEntry.checksum,16));

	ui.resourceType_comboBox->setCurrentIndex((int)item->resourceEntry.resourceType);
	ui.entryType_comboBox->setCurrentIndex((int)item->resourceEntry.entryType);

	ui.preload_checkBox->setChecked(item->preload);
}

void ResourceTool::changeButtonPressed(){
	QList<QModelIndex> selectedItems = ui.treeView->selectionModel()->selectedIndexes();
	int num = 0; 

	bool preload = ui.preload_checkBox->isChecked();
	HRESOURCE_ENTRY_TYPE_E entry_type = (HRESOURCE_ENTRY_TYPE_E)ui.entryType_comboBox->currentIndex();
	HRESOURCE_TYPE_E type = (HRESOURCE_TYPE_E)ui.resourceType_comboBox->currentIndex();

	for(int i = 0; i < selectedItems.size();++i){
		num += setPreloadRecursive( model->getItem(selectedItems.at(i)), preload, entry_type,type);
	}

	if(selectedEntry != nullptr){
		newStatusMessage("[FileID " + QString::number(selectedEntry->resourceEntry.fileID)  +  "] " 
			+ QString::fromStdString(selectedEntry->resourceEntry.name)
			+ " and " + QString::number(num) + " sub entries updated");
	}
}


int ResourceTool::setPreloadRecursive(TreeItem *item,bool preload,HRESOURCE_ENTRY_TYPE_E entryType,HRESOURCE_TYPE_E resourceType){
	if(item == nullptr) return 0;
	item->preload = preload;
	int count = 1;

	item->resourceEntry.resourceType = resourceType;
	if(item->resourceEntry.entryType != HRESOURCE_ENTRY_TYPE_E::directory && entryType != HRESOURCE_ENTRY_TYPE_E::directory){
        item->resourceEntry.entryType = entryType;
		//++count;
	}

	TreeItem *child = nullptr;
	for(int i = 0; i<item->childCount();++i){
		child = item->child(i);
		++count;
		child->preload = preload;
		if(child->resourceEntry.entryType != HRESOURCE_ENTRY_TYPE_E::directory && entryType != HRESOURCE_ENTRY_TYPE_E::directory){
			child->resourceEntry.entryType = entryType;
		}
		child->resourceEntry.resourceType = resourceType;
		if(child->childCount() > 0) count += setPreloadRecursive(child,preload,entryType,resourceType);
	}

	return count;
}

void ResourceTool::setNewModel(){
	if(model != nullptr && worker != nullptr && worker->getSaved() == false) {
		ui.treeView->setModel(model);
		connect(ui.treeView->selectionModel(),SIGNAL(selectionChanged(const QItemSelection &,const QItemSelection &)),
				this, SLOT(selectionChanged()));

		ResourceIndexFileHeader header = worker->getHeader();	
		ui.fileVersion_spinBox->setValue(header.fileVersion);
		ui.headerVersion_Label->setText(QString::number(header.headerVersion));
		ui.packageCount_Label->setText(QString::number(header.packageCount));
		if(header.packageIdentifier.empty() == false)ui.packageIdentifier_lineEdit->setText(QString::fromStdString(header.packageIdentifier));
	}
	ui.actionNew->setEnabled(true);
	ui.actionSave->setEnabled(true);
	ui.changeEntry_pushButton->setEnabled(true);
}

void ResourceTool::openProject(){
	if(worker->isRunning()) return;
	QString dir = QFileDialog::getExistingDirectory(this, tr("Open Project-Directory"),"",  QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
	if(dir.isEmpty() == false){
		curProjectDir = dir;

		ui.actionNew->setEnabled(false);
		ui.actionSave->setEnabled(false);
		ui.changeEntry_pushButton->setEnabled(false);

		ui.treeView->reset();
		delete model; model = new TreeModel();

		QString file = QFileDialog::getOpenFileName(this, tr("Open ResourceFile - OPTIONAL"),"",tr("ResourceIndexFiles (*.rif)"));
		if(file.isEmpty()){
			worker->initialize(model->getRootItem(),dir);
		}else{
			curRifFile = file;
			worker->initialize(model->getRootItem(),dir,file);
		}
		
		worker->start();
	}
}

void ResourceTool::saveProject(){
	if(worker->isRunning()) return;

	QString output = QFileDialog::getExistingDirectory(this, tr("Output Directory"),"",  QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
	if(output.isEmpty()) return;

	ResourceIndexFileHeader header;

	header.packageIdentifier = ui.packageIdentifier_lineEdit->text().toStdString();
	header.fileVersion = (uint32_t)ui.fileVersion_spinBox->value();
	uint32_t packageSize = (uint32_t)ui.packageSize_spinBox->value()*1024*1024;
	int comprLevel = ui.compressionLevel_spinBox->value();
	worker->initialize(model->getRootItem(),output,header,packageSize,comprLevel);

	ui.actionNew->setEnabled(false);
	ui.actionSave->setEnabled(false);
	ui.changeEntry_pushButton->setEnabled(false);
	worker->start();
}
