//   Copyright 2020 Fabian Wahlster
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef WORKERTHREAD_H
#define WORKERTHREAD_H

#include <QThread>
#include <QMutex>
#include <QWaitCondition>
#include "TreeItem.h"
#include "Resources\ResourceFile.h"

//using namespace helix;
class WorkerThread : public QThread{
	Q_OBJECT
public:
	WorkerThread();
	~WorkerThread();

	void initialize(TreeItem *rootItem, QString dirPath);
	void initialize(TreeItem *rootItem, QString dirPath, QString resourceFilePath);
    void initialize(TreeItem *rootItem, QString outputPath,const Helix::Resources::ResourceIndexFileHeader &header, uint32_t packageSize, int compressionLevel);

	bool getSaved(){return save;}
    Helix::Resources::ResourceIndexFileHeader getHeader(){return resHeader;}
    HDEBUGNAME("WorkerThread");
protected:
    void run();
signals:
	void sendMessage(const QString &msg);
	void progressUpdated(int value);
private:
	void construct(TreeItem *parent, QString path);
	void harvestTree(TreeItem *parent,QList<TreeItem*> &out);
	void build(QList<TreeItem*> &in);
	//Thread
	QMutex sync;
    QWaitCondition pauseCond;
    bool pauseval,closeval;

	//WorkerData
	bool useResouceFile,save;
	QString rootPath;
	TreeItem *root;	
	int compressionLevel;
	uint32_t curID;
	uint32_t packageSize;
	uint32_t curPackageSize;
	QString resFilePath;
	QString outputPath;
    Helix::Resources::ResourceFile *resFile;
    Helix::Resources::ResourceIndexFileHeader resHeader;
};

#endif //WORKERTHREAD_H
