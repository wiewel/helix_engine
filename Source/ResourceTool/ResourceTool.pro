#-------------------------------------------------
#
# Project created by QtCreator 2015-04-18T14:25:53
#
#-------------------------------------------------

QT       += core gui

CONFIG += c++14

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ResourceTool
TEMPLATE = app

INCLUDEPATH += "$$PWD/../../ThirdParty/Source/"
INCLUDEPATH += "$$PWD/../Helix/"

SOURCES += main.cpp\
    resourcetool.cpp \
    TreeItem.cpp \
    TreeModel.cpp \
    WorkerThread.cpp

HEADERS  += resourcetool.h \
    TreeItem.h \
    TreeModel.h \
    WorkerThread.h

FORMS    += resourcetool.ui
