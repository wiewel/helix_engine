//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef HLXSLCOMPILER_H
#define HLXSLCOMPILER_H

#include <QtWidgets/QMainWindow>
#include "ui_hlxslcompiler.h"
#include "stdrzr\source\TextToken.h"
#include "Util\Logger.h"
#include "Compiler\PermutationCompiler.h"
#include <QMessageBox>
#include <map>

namespace Helix
{
	typedef std::map<std::string, Compiler::ShaderDocument> TOpenDocuments;

	class HLXSLCompiler : public QMainWindow
	{
		Q_OBJECT

	public:
		HDEBUGNAME("HLXSLCompiler");

		HLXSLCompiler(QWidget *parent = 0);
		~HLXSLCompiler();

	private:

		void ProgressUpdated(uint32_t _uCurrent, uint32_t _uTotal);
		void Log(const TCHAR* _pLogMsg, HERROR_E _kErrorLevel);
		void TaskComplete();

		void SaveOpenDocuments();
		void UpdateIncludes();
		
		private slots:

		void Parse();
		void Compile();

		void OpenFile();
		void SelectedShaderChanged(int _iIndex);
		void TextChanged(); // shader text changed

		void AddIncludeDirectory();
		void RemoveSelectedIncludeDirectory();

	private:

		Ui::HLXSLCompilerClass ui;
		QMessageBox SaveOpenFileDialog;

		Compiler::PermutationCompiler* m_pPermuationCompiler = nullptr;

		QString m_sOpenShaderDialogDirectory;
		stdrzr::TextToken* m_pSettingsFile = nullptr;

		TOpenDocuments m_OpenDocuments;
		TOpenDocuments::iterator m_CurrentOpenDocumentItr;
	};
} // Helix



#endif // HLXSLCOMPILER_H
