//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "hlxslcompiler.h"
#include <QFileDialog>
#include "Resources\HLXSLFileFormat.h"
#include "Async\Delegate.h"

using namespace Helix;
using namespace Helix::Compiler;

HLXSLCompiler::HLXSLCompiler(QWidget *parent)
	: QMainWindow(parent)
{
	HInitDebug();
	ui.setupUi(this);

	connect(ui.OpenShader_Button, SIGNAL(released()), this, SLOT(OpenFile()));
	connect(ui.SelectedShader_comboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(SelectedShaderChanged(int)));
	connect(ui.AddIncludeDirectory_pushButton, SIGNAL(released()), this, SLOT(AddIncludeDirectory()));
	connect(ui.RemoveIncludeDirectory_pushButton, SIGNAL(released()), this, SLOT(RemoveSelectedIncludeDirectory()));
	connect(ui.ShaderCode_textEdit, SIGNAL(textChanged()), this, SLOT(TextChanged()));
	connect(ui.Compile_pushButton, SIGNAL(released()), this, SLOT(Compile()));
	connect(ui.ParseShader_Button, SIGNAL(released()), this, SLOT(Parse()));

	//write messages to the compileoutput log
	Logger::Instance()->SetLogCallback(Async::make_delegate(&HLXSLCompiler::Log, this));
	Logger::Instance()->SetShowMessageBoxOnFatal(true);

	m_pPermuationCompiler = new PermutationCompiler();
	m_pPermuationCompiler->AddIncludeDirectory("Hitler");
	//start suspended
	//m_pPermuationCompiler->StartInNewThread(true);

	m_pPermuationCompiler->SetCompletionCallback(Async::make_delegate(&HLXSLCompiler::TaskComplete, this));
	m_pPermuationCompiler->SetProgressCallback(Async::make_delegate(&HLXSLCompiler::ProgressUpdated, this));

	m_CurrentOpenDocumentItr = m_OpenDocuments.end();

	m_pSettingsFile = new stdrzr::TextToken("Settings.cfg", "HLXSLCompileSettings", false, std::ios::in);

	// in case the file could not be loaded, set the root key
	if (m_pSettingsFile->getKey().empty())
	{
		m_pSettingsFile->setKey("HLXSLCompileSettings");
	}

	m_sOpenShaderDialogDirectory = QString(m_pSettingsFile->getValue("OpenShaderDialogDirectory").c_str());

	std::vector<std::string> IncludeDirs = m_pSettingsFile->getAllValues("IncludePath");

	for (std::string& sDir : IncludeDirs)
	{
		ui.IncludeDirectories_listWidget->addItem(QString(sDir.c_str()));
	}

	SaveOpenFileDialog.setWindowTitle("HLXSLCompiler");
	SaveOpenFileDialog.setText("The document has been modified.");
	SaveOpenFileDialog.setInformativeText("Do you want to save your changes?");
	SaveOpenFileDialog.setStandardButtons(QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
	SaveOpenFileDialog.setDefaultButton(QMessageBox::Save);
}
//---------------------------------------------------------------------------------------------------
HLXSLCompiler::~HLXSLCompiler()
{
	int iCount = ui.IncludeDirectories_listWidget->count();

	//remove all previous include dirs
	m_pSettingsFile->remove("IncludePath", true);

	//save include dirs
	for (int i = 0; i < iCount;++i)
	{
		QListWidgetItem* pItem = ui.IncludeDirectories_listWidget->item(i);
		m_pSettingsFile->add<std::string>("IncludePath", pItem->text().toStdString(), false);
	}

	//save open file dialog dir
	if (m_sOpenShaderDialogDirectory.isEmpty() == false)
	{
		m_pSettingsFile->set<std::string>("OpenShaderDialogDirectory", m_sOpenShaderDialogDirectory.toStdString(), false);
	}

	//save settings file
	if (m_pSettingsFile->serialze("Settings.cfg") == false)
	{
		//Do something
		HERROR("Failed to write Settings.cfg");
	}

	HSAFE_DELETE(m_pPermuationCompiler);

	SaveOpenDocuments();

	HSAFE_DELETE(m_pSettingsFile);
}
//---------------------------------------------------------------------------------------------------
void HLXSLCompiler::OpenFile()
{
	QString sFile = QFileDialog::getOpenFileName(this, "Open .hlsl", m_sOpenShaderDialogDirectory, "HighLevelShaderLanguage (*.hlsl)");

	QFileInfo FileInfo(sFile);

	m_sOpenShaderDialogDirectory = FileInfo.absolutePath();

	QString sFileName = FileInfo.fileName();

	std::string sFileNameStd = sFileName.toStdString();

	TOpenDocuments::iterator it = m_OpenDocuments.find(sFileNameStd);

	if (it == m_OpenDocuments.end())
	{
		ShaderDocument ShaderDoc;

		ShaderDoc.m_sFileName = sFileNameStd;
		ShaderDoc.m_sFilePath = sFile.toStdString();

		stdrzr::fbytestream FStream(ShaderDoc.m_sFilePath, std::ios::in);

		if (FStream.is_open())
		{
			ShaderDoc.m_sCode = FStream.get<std::string>(FStream.size());

			FStream.close();

			m_OpenDocuments[sFileNameStd] = std::move(ShaderDoc);

			ui.SelectedShader_comboBox->addItem(sFileName);

			ui.SelectedShader_comboBox->setCurrentIndex(ui.SelectedShader_comboBox->count() - 1);
		}
		else
		{
			HERROR("Failed to open %s", ShaderDoc.m_sFilePath.c_str());
		}

		//TODO: load meta file with permutation and compiler settings
	}
	else
	{
		// File already opened
		for (int i = 0; i < ui.SelectedShader_comboBox->count(); ++i)
		{
			if (ui.SelectedShader_comboBox->itemText(i) == sFileName)
			{
				ui.SelectedShader_comboBox->setCurrentIndex(i);
				break;
			}
		}

	}
}
//---------------------------------------------------------------------------------------------------
void HLXSLCompiler::SelectedShaderChanged(int _iIndex)
{
	QString sSelectedFile = ui.SelectedShader_comboBox->itemText(_iIndex);

	if (sSelectedFile.isEmpty())
		return;

	TOpenDocuments::iterator it = m_CurrentOpenDocumentItr;

	//save open document
	if (it != m_OpenDocuments.end())
	{
		it->second.m_sCode = ui.ShaderCode_textEdit->toPlainText().toStdString();
	}
		
	// open next document
	it = m_OpenDocuments.find(sSelectedFile.toStdString());

	if (it != m_OpenDocuments.end())
	{
		HLOG("Open shader %s", sSelectedFile.toStdWString().c_str());
		ui.ShaderCode_textEdit->setText(QString(it->second.m_sCode.c_str()));
		m_CurrentOpenDocumentItr = it;
		//Update / display settings
	}
	else
	{
		HERROR("Document not found");
	}		
}
//---------------------------------------------------------------------------------------------------
void HLXSLCompiler::TextChanged()
{
	if (m_CurrentOpenDocumentItr != m_OpenDocuments.end())
	{
		ShaderDocument& ShaderDoc = m_CurrentOpenDocumentItr->second;

		ShaderDoc.m_bChanged = true;
	}
}
//---------------------------------------------------------------------------------------------------

void HLXSLCompiler::AddIncludeDirectory()
{
	QString sDir = QFileDialog::getExistingDirectory(this, "Open Include-Directory", "", QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
	if (sDir.isEmpty() == false)
	{
		ui.IncludeDirectories_listWidget->addItem(sDir);
		m_pPermuationCompiler->AddIncludeDirectory(sDir.toStdString());
	}
}
//---------------------------------------------------------------------------------------------------
void HLXSLCompiler::RemoveSelectedIncludeDirectory()
{
	QList<QListWidgetItem*> SelectedItems = ui.IncludeDirectories_listWidget->selectedItems();
	for (QListWidgetItem* pItem : SelectedItems)
	{
		delete pItem;
	}
}
//---------------------------------------------------------------------------------------------------
void HLXSLCompiler::Compile()
{
	if (m_CurrentOpenDocumentItr != m_OpenDocuments.end())
	{
		ui.centralWidget->setEnabled(false);

		// Todo: make this optional
		SaveOpenDocuments();

		UpdateIncludes();

		//get the newest shader code
		ShaderDocument& ShaderDoc = m_CurrentOpenDocumentItr->second;
		ShaderDoc.m_sCode = ui.ShaderCode_textEdit->toPlainText().toStdString();

		if (m_pPermuationCompiler->Compile(ShaderDoc) == false)
		{
			HWARNING("Unable to start compilation, compiler is busy!");
		}
	}
}
//---------------------------------------------------------------------------------------------------
void HLXSLCompiler::Parse()
{
	if (m_CurrentOpenDocumentItr != m_OpenDocuments.end())
	{
		ui.centralWidget->setEnabled(false);

		UpdateIncludes();

		//get the newest shader code
		ShaderDocument& ShaderDoc = m_CurrentOpenDocumentItr->second;
		ShaderDoc.m_sCode = ui.ShaderCode_textEdit->toPlainText().toStdString();

		m_pPermuationCompiler->Parse(ShaderDoc);		
	}
}
//---------------------------------------------------------------------------------------------------
void HLXSLCompiler::TaskComplete()
{
	ui.centralWidget->setEnabled(true);
}
//---------------------------------------------------------------------------------------------------
void HLXSLCompiler::ProgressUpdated(uint32_t _uCurrent, uint32_t _uTotal)
{
	float fPercent = static_cast<float>(_uCurrent) / static_cast<float>(_uTotal) * 100.f;
}
//---------------------------------------------------------------------------------------------------
void HLXSLCompiler::Log(const TCHAR * _pLogMsg, HERROR_E _kErrorLevel)
{
	if (ui.CompileOutput_textBrowser != nullptr)
	{
		QString sMessage = QString::fromUtf16(reinterpret_cast<const ushort*>(_pLogMsg));
		ui.CompileOutput_textBrowser->append(sMessage);
	}	
}
//---------------------------------------------------------------------------------------------------
void HLXSLCompiler::SaveOpenDocuments()
{
	TOpenDocuments::iterator it = m_CurrentOpenDocumentItr;
	TOpenDocuments::iterator end = m_OpenDocuments.end();

	//save open document text
	if (it != end)
	{
		it->second.m_sCode = ui.ShaderCode_textEdit->toPlainText().toStdString();
	}

	bool bSaveChangedFiles = false;
	bool bDialogDone = false;

	//save the rest
	for (it = m_OpenDocuments.begin(); it != end; ++it)
	{
		ShaderDocument& ShaderDoc = it->second;

		//skip unchanged files
		if (ShaderDoc.m_bChanged == false)
		{
			continue;
		}

		if (bDialogDone == false)
		{
			switch (SaveOpenFileDialog.exec())
			{
			case QMessageBox::Save:
				bSaveChangedFiles = true;
				break;
			case QMessageBox::Discard:
				bSaveChangedFiles = false;
				break;
			case QMessageBox::Cancel:
				bDialogDone = false;
				continue; // next file
			default:
				// should never be reached
				break;
			}

			bDialogDone = true;
		}

		//don't save any file
		if (bSaveChangedFiles == false)
		{
			break;
		}

		stdrzr::fbytestream FStream(ShaderDoc.m_sFilePath, std::ios::out | std::ios::trunc);

		if (FStream.is_open())
		{
			HLOG("Saving %s", ShaderDoc.m_sFileName.c_str());

			FStream.put<std::string>(ShaderDoc.m_sCode);
			FStream.close();
		}
		else
		{
			HERROR("Failed to save %s", ShaderDoc.m_sFilePath.c_str());
		}
	}
}
//---------------------------------------------------------------------------------------------------
void HLXSLCompiler::UpdateIncludes()
{
	//clear include dirs and opened files
	m_pPermuationCompiler->ResetIncludeInterface();

	int iCount = ui.IncludeDirectories_listWidget->count();

	//save include dirs
	for (int i = 0; i < iCount; ++i)
	{
		QListWidgetItem* pItem = ui.IncludeDirectories_listWidget->item(i);
		m_pPermuationCompiler->AddIncludeDirectory(pItem->text().toStdString());
	}

	// add opened shader documents to the include db to avoid file io
	HFOREACH_CONST(it, end, m_OpenDocuments)
		if (it != m_CurrentOpenDocumentItr)
		{
			m_pPermuationCompiler->UpdateIncludeFile(it->first, it->second.m_sCode);
		}
	HFOREACH_CONST_END
}
//---------------------------------------------------------------------------------------------------