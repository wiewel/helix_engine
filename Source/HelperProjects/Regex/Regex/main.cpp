//   Copyright 2020 Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include <iostream>
#include <regex>
#include <string>
#include <cctype>

#include "Bytes.h"
#include "StringHelpers.h"

using namespace std;

enum ContainerType
{
	ContainerType_Struct,
	ContainerType_InputStruct,
	ContainerType_CBuffer,
};

struct ShaderContainerEntry
{
	std::string sType;
	std::string sName;
	std::string sSemanticName;
	uint32_t uSemanticIndex;
};

struct ShaderContainer
{
	std::string sName;
	std::vector<ShaderContainerEntry> Entries;
	ContainerType kType = ContainerType_Struct;
};

uint32_t AnalyzeSemanticName(_In_ const std::string& _sSemanticName)
{
	// VB0_TEXCOORD123
	// .*?(\\d+)$ // -> returns [1] -> 123

	uint32_t uResult = 0;

	try
	{
		std::regex SemanticIndexRE(".*?(\\d+)$");
		std::smatch ReMatch;

		if(std::regex_match(_sSemanticName.begin(), _sSemanticName.end(), ReMatch, SemanticIndexRE))
		{
			std::string sResult = ReMatch[1];
			uResult = static_cast<uint32_t>(atoi(&(sResult.back())));
		}
	}
	catch (std::regex_error& e)
	{
		cerr << e.what() << endl;
	}

	return uResult;
}

void AnalyzeContainerContent(_In_ const std::string& _sContainerContent, _In_ const ContainerType& _kType, _Out_ std::vector<ShaderContainerEntry>& _Entries)
{
	ShaderContainerEntry TempEntry;

	try
	{
		std::regex ContainerEntryRE;

		switch (_kType)
		{
		case ContainerType_InputStruct:
			ContainerEntryRE = std::regex("(\\w+)\\s+(\\w+)\\s+:\\s+(\\w+)\\s+;");
			break;
		case ContainerType_Struct:
		case ContainerType_CBuffer:
			ContainerEntryRE = std::regex("(\\w+)\\s+(\\w+)\\s+;");
			break;
		default:
			break;
		}
		std::sregex_iterator ReIT(_sContainerContent.cbegin(), _sContainerContent.cend(), ContainerEntryRE);
		std::sregex_iterator ReEnd;

		while (ReIT != ReEnd)
		{
			std::smatch ReMatch = *ReIT;

			TempEntry.sType = ReMatch[1];
			TempEntry.sName = ReMatch[2];

			if (_kType == ContainerType_InputStruct)
			{
				TempEntry.sSemanticName = ReMatch[3];
				TempEntry.uSemanticIndex = AnalyzeSemanticName(TempEntry.sSemanticName);
			}

			_Entries.push_back(TempEntry);

			++ReIT;
		}
	}
	catch (std::regex_error& e)
	{
		cerr << e.what() << endl;
	}
}

void AnalyzeShaderCode(_In_ const std::string& _sShaderCode, _Out_ std::vector<ShaderContainer>& _InputStructs, _Out_ std::vector<ShaderContainer>& _Structs, _Out_ std::vector<ShaderContainer>& _CBuffer)
{
//	struct PS_IN
//	{
//		float4 vPosition : SV_POSITION;
//		float3 vPosWS : POSITION;
//		float3 vNormal : NORMAL;
//		float3 vTangent : TANGENT;
//
//#line 256
//		float2 vTexCoord : TEXCOORD0;
//
//#line 259
//	};

	static const std::vector<std::string> InputStructNames =
	{
		"VS_IN",
		"PS_IN",
		"HS_IN",
		"GS_IN",
		"DS_IN",
		"PS_OUT"
	};

	ShaderContainer TempStruct;

	try
	{
		std::regex StructRE("(\\w+)\\s+([\\w|_]+)\\s*\\{([^\\}]*)\\}\\s*;"); // [0] -> Complete Container, [1] -> Container type, [2] -> Container Name, [3] -> Content inside curly brackets
		std::sregex_iterator ReIT(_sShaderCode.cbegin(), _sShaderCode.cend(), StructRE);
		std::sregex_iterator ReEnd;

		while (ReIT != ReEnd)
		{
			TempStruct = {};

			std::smatch ReMatch = *ReIT;
			std::string&& sContainerType = ReMatch[1];
			std::string&& sContainerName = ReMatch[2];
			std::string&& sContainerContent = ReMatch[3];

			if (stdrzr::to_lower(sContainerType.c_str()) == "cbuffer")
			{
				TempStruct.kType = ContainerType_CBuffer;
			}
			else if (std::find(InputStructNames.begin(), InputStructNames.end(), sContainerName) != InputStructNames.end())
			{
				TempStruct.kType = ContainerType_InputStruct;
			}
			else if (stdrzr::to_lower(sContainerType.c_str()) == "struct")
			{
				TempStruct.kType = ContainerType_Struct;
			}
			else
			{
				cerr << "Unknown container type... Check this!" << endl;
			}

			TempStruct.sName = sContainerName;
			AnalyzeContainerContent(sContainerContent, TempStruct.kType, TempStruct.Entries);

			switch (TempStruct.kType)
			{
			case ContainerType_InputStruct:
				_InputStructs.push_back(TempStruct);
				break;
			case ContainerType_Struct:
				_Structs.push_back(TempStruct);
				break;
			case ContainerType_CBuffer:
				_CBuffer.push_back(TempStruct);
				break;
			default:
				break;
			}

			++ReIT;
		}
	}
	catch (std::regex_error& e)
	{
		cerr << e.what() << endl;
	}
}

int main()
{
	// Metacharacters:	\\w -> a-z A-Z 0-9 or _
	//					\\s -> space, tab, carriage return, new line, vertical tab, form feed

	//   float3 vPosition : VB0_POSITION ;
	//(\\w+)\\s+(\\w+)\\s+:\\s+(\\w+)\\s+;


	stdrzr::fbytestream BS("preproc.txt", ios_base::in);
	std::string sPreProc = BS.get<std::string>();

	
	std::vector<ShaderContainer> ShaderInputStructs;
	std::vector<ShaderContainer> ShaderStructs;
	std::vector<ShaderContainer> ShaderCBuffer;

	AnalyzeShaderCode(sPreProc, ShaderInputStructs, ShaderStructs, ShaderCBuffer);
	
	// Debug output
	cout << "Input Structs:" << endl;

	for(auto InStruct : ShaderInputStructs)
	{
		cout << InStruct.sName << endl;

		for (auto Ele : InStruct.Entries)
		{
			cout << "\t" << Ele.sType << "  " << Ele.sName << "  " << Ele.sSemanticName << "  " << Ele.uSemanticIndex << endl;
		}
	}

	cout << endl << endl << "Shader Structs:" << endl;

	for (auto Struct : ShaderStructs)
	{
		cout << Struct.sName << endl;

		for (auto Ele : Struct.Entries)
		{
			cout << "\t" << Ele.sType << "  " << Ele.sName << endl;
		}
	}	
	
	cout << endl << endl << "Shader CBuffer:" << endl;

	for (auto Struct : ShaderCBuffer)
	{
		cout << Struct.sName << endl;

		for (auto Ele : Struct.Entries)
		{
			cout << "\t" << Ele.sType << "  " << Ele.sName << endl;
		}
	}

	system("pause");

	return 0;
}