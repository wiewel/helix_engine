//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "hlx\src\StandardDefines.h"
#include "hlx\src\Logger.h"
#include "Resources\FileSystem.h"
#include "hlx\src\StringHelpers.h"

#include "H3D\AssetImporter.h"
#include <unordered_set>

using namespace Helix;
using namespace Helix::Display;
using namespace Helix::H3D;

inline void Update(const hlx::string&, int32_t, int32_t, int32_t)
{

}

int main(int argc, char *argv[])
{
	HInitDebug();
	//HBreakOnAlloc(6457006);
	//HBreakOnAlloc(6456990);
	//HBreakOnAlloc(2583000);
	//HBreakOnAlloc(2383798);

	SetConsoleTitle("H3DTool v1");

	Resources::FileSystem* pFS = Resources::FileSystem::Instance();

	hlx::Logger::Instance()->SetSuppressLoggerStartedStopped(true);
	hlx::Logger::Instance()->WriteToStream(&std::wcout);

	if (argc < 2)
	{
		HERROR("Invalid number of arguments");
		HLOG("Options: [input] -param [arg]");
		HLOG("-h3dout output directory for H3D files");
		HLOG("-hmatout output directory for HMat files");
		HLOG("-texout output directory for image files (.jpg|.png extracted from FBX)");
		HLOG("-uv Copy TexCoords from input file");
		HLOG("-n Copy/Generate normals from input file");
		HLOG("-t Copy/Generate tangents from input file");
		HLOG("-bt Copy/Generate bitangents from input file");
		HLOG("-r reverse winding");
		HLOG("-phxsx compute convex hull mesh");
		HLOG("-flipv flip V TexCoord");
		HLOG("-scale mesh scale factor");
		HLOG("-opengl right handed coordinate system OpenGL");
		HLOG("-directx left handed coordinate system DirectX");
		HLOG("-3dsmax coordinate system 3DS Max");
		HLOG("-mayazup coordinate system MayaZUp");
		HLOG("-mayayup coordinate system MayaYUp");
		HLOG("-motionbuilder coordinate system MotionBuilder");
		HLOG("-lightwave coordinate system LightWave");
		HLOG("-notex no texture export");
		HLOG("-nomat no material export");
		HLOG("-mip mip level");
		//HLOG("-v2 target h3d version);
		return E_FAIL;
	}

	std::string sInputFile;
	std::string sH3DOutDir;
	std::string sHMATOutDir;
	std::string sTexOutDir;

	bool bReverseWinding = false;
	bool bFlipV = false;
	bool bToLeftHanded = false;
	bool bExportMaterials = true;
	bool bExportTextures = true;

	uint32_t uMipLevels = 0u;
	float fScaleFactor = 1.f;

	Resources::TH3DFlags kComponentFlag = Resources::H3DFlag_None;
	TVertexDeclaration kTargetDeclaration = VertexLayout_Pos_XYZ; // minimal output
	Resources::H3DVersion kTargetVersion = Resources::H3DVersion_v1;
	H3D::CoordinateSystem CoordSys = CoordinateSystem_Unknown;

	for (int i = 2; i < argc; ++i)
	{
		std::string sParam(argv[i]);
		std::string sNextParam = (i + 1) < argc ? std::string(argv[i + 1]) : std::string();

		if (sParam == "-uv")
		{
			kTargetDeclaration |= VertexLayout_UV;
		}
		else if (sParam == "-n")
		{
			kTargetDeclaration |= VertexLayout_Normal;
		}
		else if (sParam == "-t")
		{
			kTargetDeclaration |= VertexLayout_Tangent;
		}
		else if (sParam == "-bt")
		{
			kTargetDeclaration |= VertexLayout_Bitangent;
		}
		else if (sParam == "-r")
		{
			bReverseWinding = true;
		}
		else if (sParam == "-flipv")
		{
			bFlipV = true;
		}
		else if (sParam == "-physx")
		{
			kComponentFlag |= Resources::H3DFlag_PhysxMesh;
		}
		else if (sParam == "-scale")
		{
			fScaleFactor = std::stof(sNextParam);
		}
		else if (sParam == "-lhs")
		{
			bToLeftHanded = true;
		}
		else if (sParam == "-notex")
		{
			bExportTextures = false;
		}
		else if (sParam == "-nomat")
		{
			bExportMaterials = false;
		}
		else if (sParam == "-mip")
		{
			uMipLevels = std::stoul(sNextParam);
		}
		else if (sParam == "-opengl")
		{
			CoordSys = CoordinateSystem_OpenGL;
		}
		else if (sParam == "-directx")
		{
			CoordSys = CoordinateSystem_DirectX;
		}
		else if (sParam == "-3dsmax")
		{
			CoordSys = CoordinateSystem_Max;
		}
		else if (sParam == "-mayazup")
		{
			CoordSys = CoordinateSystem_MayaZUp;
		}
		else if (sParam == "-mayayup")
		{
			CoordSys = CoordinateSystem_MayaYUp;
		}
		else if (sParam == "-motionbuilder")
		{
			CoordSys = CoordinateSystem_MotionBuilder;
		}
		else if (sParam == "-lightwave")
		{
			CoordSys = CoordinateSystem_Lightwave;
		}
		else if (sParam == "-h3dout")
		{
			sH3DOutDir = sNextParam;
		}
		else if (sParam == "-hmatout")
		{
			sHMATOutDir = sNextParam;
		}
		else if (sParam == "-texout")
		{
			sTexOutDir = sNextParam;
		}
	}

	sInputFile = std::string(argv[1]);

	if (sInputFile.empty())
	{
		HERROR("No input file provided");
		return E_FAIL;
	}

	if (hlx::ends_with(sTexOutDir, "/") == false &&
		hlx::ends_with(sTexOutDir, "\\") == false &&
		sTexOutDir.empty() == false)
	{
		sTexOutDir += "\\";
	}

	std::string sExt = hlx::to_lower(hlx::get_right_of(sInputFile, "."));

	bool bOBJ = sExt == "obj";
	bool bFBX = sExt == "fbx";

	std::string sInputDir = Resources::FileSystem::GetDirectoryFromPath(sInputFile);

	if (sH3DOutDir.empty())
	{
		sH3DOutDir = sInputDir;
	}

	if (sHMATOutDir.empty())
	{
		sHMATOutDir = sInputDir;
	}

	if (sTexOutDir.empty())
	{
		sTexOutDir = sInputDir;
	}

	if (hlx::ends_with(sH3DOutDir, "\\") == false && hlx::ends_with(sH3DOutDir, "/") == false)
	{
		sH3DOutDir += "\\";
	}

	if (hlx::ends_with(sHMATOutDir, "\\") == false && hlx::ends_with(sHMATOutDir, "/") == false)
	{
		sHMATOutDir += "\\";
	}

	if (hlx::ends_with(sTexOutDir, "\\") == false && hlx::ends_with(sTexOutDir, "/") == false)
	{
		sTexOutDir += "\\";
	}

	HLOG("Target vertex format:");
	for (uint32_t i = 0; i < VertexLayout_NumOf; ++i)
	{
		if (kTargetDeclaration & static_cast<VertexLayout>(1 << i))
		{
			HLOG("\t%s", VertexComponent[i].sName);
		}
	}

	HLOG("CoordSystem: %s", CoordSys == CoordinateSystem_DirectX ? S("DirectX") : S("OpenGL"));
	HLOG("Scale: %f", fScaleFactor);
	HLOG("FlipV: %s", bFlipV ? S("true") : S("false"));
	HLOG("ReverseWinding: %s", bReverseWinding ? S("true") : S("false"));
	HLOG("Generate PhysXMesh: %s", kComponentFlag & Resources::H3DFlag_PhysxMesh ? S("true") : S("false"));

	HLOG("H3DOutput: %s", CSTR(sH3DOutDir));
	HLOG("HMatOutput: %s", CSTR(sHMATOutDir));
	HLOG("TexOutput: %s", CSTR(sTexOutDir));
	
	if (bFBX) // ######################### FBX #########################
	{
		AssetImporter Importer;
		Importer.SetFilesToImport({ sInputFile });
		Importer.SetCoordinateSystem(CoordSys);
		Importer.SetCreateConvexHull(kComponentFlag & Resources::H3DFlag_PhysxMesh);
		Importer.SetH3DOutDir(sH3DOutDir);
		Importer.SetHMatOutputDir(sHMATOutDir);
		Importer.SetTexOutDir(sTexOutDir);
		Importer.SetImportMaterials(bExportMaterials);
		Importer.SetImportTextures(bExportTextures);
		Importer.SetFlipV(bFlipV);
		Importer.SetMipLevel(uMipLevels);
		Importer.SetReverseWinding(bReverseWinding);
		Importer.SetScale(fScaleFactor);
		Importer.SetToLefthanded(bToLeftHanded);
		Importer.SetVertexLayout(kTargetDeclaration);
		Importer.SetProgressFunction(Async::make_delegate(&Update));

		Importer.Start(false);
	}
	else
	{
		HERROR("Unknown Extension %s", sExt.c_str());
		return E_FAIL;
	}

	return S_OK;
}