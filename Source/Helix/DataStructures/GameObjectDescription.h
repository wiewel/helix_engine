//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef GAMEOBJECTDESCRIPTION_H
#define GAMEOBJECTDESCRIPTION_H

#include "Math\MathTypes.h"
#include "Physics\GeometryDescriptions.h"
#include "Util\Flag.h"
#include "hlx\src\Logger.h"
#include <vector>

namespace Helix
{
	namespace Datastructures
	{
		enum GameObjectFlag : uint32_t
		{
			GameObjectFlag_None = 0,
			GameObjectFlag_Destroyed = 1 << 0, // just for debugging
			GameObjectFlag_Kinematic = 1 << 1,
			GameObjectFlag_NoCollision = 1 << 2, // not implemented
			GameObjectFlag_Invisible = 1 << 3
		};

		using TGameObjectFlags = Flag<GameObjectFlag>;

		enum GameObjectType : uint32_t
		{
			GameObjectType_RenderObject = 0,
			GameObjectType_RigidStatic = 1,
			GameObjectType_RigidDynamic = 2,
			GameObjectType_Unknown
		};	

		struct GameObjectDesc
		{
			GameObjectType kType = GameObjectType_RigidDynamic;
			Math::transform Transform = HTRANSFORM_IDENTITY;

			Math::float3 vRenderingScale = { 1.f,1.f,1.f };
			TGameObjectFlags kFlags = GameObjectFlag_None;

			std::string sMeshFileName; // name of h3d file
			std::vector<std::string> sMeshClusterSubNames; // name of clusters within file
			std::vector<std::string> sMaterialNames;
			std::string sObjectName;
			std::vector<std::string> sTags;

			bool bContinuousCollisionDetection = false;
			float fMass = 1.f; // 1 kg
		};
	} // Datastructures
} // Helix

#endif // !GAMEOBJECTDESCRIPTION_H
