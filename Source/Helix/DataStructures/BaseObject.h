//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef BASEOBJECT_H
#define BASEOBJECT_H

#include "Component.h"
#include "Math\MathTypes.h"
#include "hlx\src\StandardDefines.h"
#include <vector>

namespace Helix
{
	namespace Datastructures
	{
		class IBaseObject
		{
		public:
			IBaseObject(const ObjectType _kType, IBaseObject* _pParent = nullptr);
			virtual ~IBaseObject();

			IBaseObject* GetParent();
			const IBaseObject* GetParent() const;
			const ObjectType GetType() const;

			// removes components, mabye deletes child objects
			virtual void Uninitialize();

			// interface functions
			virtual const std::string& GetName() const = 0;
			virtual void SetName(const std::string& _sName) = 0;

			virtual Math::transform GetTransform() const = 0;
			virtual void SetTransform(const Math::transform& _Transform) = 0;

			// functions using the interface functions
			Math::float3 GetPosition() const;
			void SetPosition(const Math::float3& _vPos);

			Math::quaternion GetOrientation() const;
			void SetOrientation(const Math::quaternion& _vOrientation);

			/// Do a local rotation, otherwise in world space
			void Rotate(const Math::quaternion& _vRot, const bool _bLocal = true);

			__declspec(property(get = GetPosition, put = SetPosition)) Math::float3 Position;
			__declspec(property(get = GetOrientation, put = SetOrientation)) Math::quaternion Orientation;
			__declspec(property(get = GetTransform, put = SetTransform)) Math::transform Transform;

			// component functions

			template<class Component, class ...Args>
			Component* AddComponent(Args&&... _args);

			bool RemoveComponent(IComponent* _pComponent);

			const std::vector<IComponent*>& GetComponents() const;

			template <class Component>
			void GetComponentsByType(_Out_ std::vector<Component*>& _Components) const;
			template <class Component>
			std::vector<Component*> GetComponentsByType() const;

			// returns the first component of this type
			template <class Component>
			Component* GetComponent() const;

			bool DeserializeComponent(const hlx::TextToken& _Token);

			void SetUserData(UserData* _pUserData);
			UserData* GetUserData() const;

		private:
			IBaseObject* const m_pParent;
			const ObjectType m_kType;
			std::vector<IComponent*> m_Components;

			UserData* m_pUserData = nullptr;
		};

		inline IBaseObject* IBaseObject::GetParent(){return m_pParent;}
		inline const IBaseObject* IBaseObject::GetParent() const { return m_pParent; }
		inline const ObjectType IBaseObject::GetType() const{return m_kType;}
		inline void IBaseObject::SetUserData(UserData* _pUserData) { m_pUserData = _pUserData; }
		inline UserData* IBaseObject::GetUserData() const{ return m_pUserData; }

		//---------------------------------------------------------------------------------------------------
		// Transform functions
		//---------------------------------------------------------------------------------------------------

		inline Math::float3 IBaseObject::GetPosition() const{return GetTransform().p;}
		inline void IBaseObject::SetPosition(const Math::float3& _vPos)
		{
			SetTransform(Math::transform(_vPos, GetTransform().q));
		}

		inline Math::quaternion IBaseObject::GetOrientation() const	{return GetTransform().q;}
		inline void IBaseObject::SetOrientation(const Math::quaternion& _vOrientation)
		{
			SetTransform(Math::transform(GetTransform().p, _vOrientation));
		}

		inline void IBaseObject::Rotate(const Math::quaternion& _vRot, const bool _bLocal)
		{
			Math::transform Transform = GetTransform();

			if(_bLocal)
			{
				Transform.q *= _vRot;
			}
			else
			{
				Transform.q = static_cast<physx::PxQuat>(_vRot * Transform.q);
			}

			Transform.q.normalize();
			SetTransform(Transform);
		}
		//---------------------------------------------------------------------------------------------------
		// Component functions
		//---------------------------------------------------------------------------------------------------
		inline const std::vector<IComponent*>& IBaseObject::GetComponents() const { return m_Components; }
		//---------------------------------------------------------------------------------------------------
		template<class Component>
		void IBaseObject::GetComponentsByType(std::vector<Component*>& _Components) const
		{
			for (IComponent* pComponent : m_Components)
			{
				if (pComponent->GetTypeID() == Component::ID)
				{
					_Components.push_back(static_cast<Component*>(pComponent));
				}
			}
		}
		//---------------------------------------------------------------------------------------------------
		template<class Component>
		std::vector<Component*> IBaseObject::GetComponentsByType() const
		{
			std::vector<Component*> Components;

			// ID requires COMPTYPENAME macro define
			for (IComponent* pComponent : m_Components)
			{
				if (pComponent->GetTypeID() == Component::ID)
				{
					Components.push_back(static_cast<Component*>(pComponent));
				}
			}

			return Components;
		}
		//---------------------------------------------------------------------------------------------------
		template<class Component>
		Component* IBaseObject::GetComponent() const
		{
			for (IComponent* pComponent : m_Components)
			{
				if (pComponent->GetTypeID() == Component::ID)
				{
					return static_cast<Component*>(pComponent);
				}
			}
			return nullptr;
		}
		//---------------------------------------------------------------------------------------------------

		template<class Component, class ...Args>
		inline Component* IBaseObject::AddComponent(Args&& ...args)
		{
			Component* pComponent = new Component(static_cast<Component::BaseType*>(this), std::forward<Args>(args)...);

			if (*pComponent) // evaluate bool operator to check if the type was object supported
			{
				pComponent->Initialize();
				m_Components.push_back(pComponent);
				return pComponent;
			}

			HSAFE_DELETE(pComponent);
			return nullptr;
		}
		//---------------------------------------------------------------------------------------------------

		inline bool IBaseObject::RemoveComponent(IComponent* _pComponent)
		{
			std::vector<IComponent*>::iterator it = std::remove(m_Components.begin(), m_Components.end(), _pComponent);
			if (it != m_Components.end())
			{
				m_Components.erase(it);
				HSAFE_DELETE(_pComponent);
				return true;
			}
			return false;
		}
		//---------------------------------------------------------------------------------------------------


	} // Datastructures
} // Helix

#endif // !BASEOBJECT_H
