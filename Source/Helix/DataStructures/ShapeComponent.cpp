//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "ShapeComponent.h"
#include "Physics\GeometryShapeFactory.h"
#include "Resources\CommonTypeTextSerializer.h"

using namespace Helix;
using namespace Helix::Physics;
using namespace Helix::Resources;
using namespace Helix::Datastructures;
//---------------------------------------------------------------------------------------------------------------------

ShapeComponent::ShapeComponent(GameObject* _pParent, const Physics::GeometryDesc& _Shape) :
	IGameObjectComponent(_pParent, ComponentCallback_OnSerialize),
	m_Desc(_Shape)
{
	Create(_Shape);
}
//---------------------------------------------------------------------------------------------------------------------

ShapeComponent::~ShapeComponent()
{
	Destroy();
}

//---------------------------------------------------------------------------------------------------------------------

bool ShapeComponent::Create(const Physics::GeometryDesc& _Shape)
{
	HNCL(m_UsageEnum.uSelectedIndex = _Shape.kUsage);
	
	// destroy previous shape
	Destroy();

	physx::PxRigidActor* pActor = GetParent()->GetActor();

	if (pActor != nullptr)
	{
		Physics::GeometryDesc Desc = _Shape;
		
		if (Desc.bScaleWithParent)
		{
			Desc.vScale = Desc.vScale * GetParent()->GetRenderingScale();
		}

		m_pShape = Physics::GeometryShapeFactory::Instance()->CreateShape(Desc);
		if (m_pShape != nullptr)
		{
			if (pActor->getScene() != nullptr)
			{
				Physics::SceneWriteLock lock;
				pActor->attachShape(*m_pShape);
			}
			else // actor has not beed added to the scene yet
			{
				pActor->attachShape(*m_pShape);
			}

			// the userdata of all physx objects should point to the gameobject that they are attached to
			m_pShape->userData = GetParent();
			m_Desc = _Shape;
			return true;
		}
	}
	else
	{
		HERROR("Parent gameobject actor is invalid, cant create shape!");
	}

	return false;
}
//---------------------------------------------------------------------------------------------------------------------

void ShapeComponent::Destroy()
{
	Physics::SimulationEventCallback::Instance()->GetOnTrigger().RemoveShape(m_pShape);
	Physics::SimulationEventCallback::Instance()->GetOnContact().RemoveShape(m_pShape);

	// if this is a template component the parent might be a nullptr!
	physx::PxRigidActor* pActor = GetParent() == nullptr ? nullptr : GetParent()->GetActor();

	if (pActor != nullptr && m_pShape != nullptr)
	{
		physx::PxScene* pScene = pActor->getScene();
		if (pScene != nullptr)
		{
			Physics::SceneWriteLock lock;
			pActor->detachShape(*m_pShape);
		}
		else // actor has not been added to the scene yet
		{
			pActor->detachShape(*m_pShape);
		}
		m_pShape = nullptr;
	}
	else if (m_pShape != nullptr)
	{
		HERROR("Parent gameobject actor is invalid!");
		m_pShape->release();
		m_pShape = nullptr;
	}
}
//---------------------------------------------------------------------------------------------------------------------

void ShapeComponent::FromMesh()
{
	GameObject* pObject = GetParent();

	if (pObject == nullptr)
		return;

	const Display::MeshClusterDX11& Mesh = pObject->GetMesh();

	if (Mesh)
	{
		const std::vector<uint32_t>& CRCs = Mesh.GetReferenceConst()->ConvexMeshChecksums;

		if (CRCs.empty() == false)
		{
			const std::vector<Display::SubMesh>& SubMeshes = Mesh.GetSubMeshes();

			if (SubMeshes.empty() == false)
			{
				std::string sName = SubMeshes.front().m_sSubName;
				uint32_t i = 0u;
				
				for (const Display::SubMesh& SubMesh : Mesh.GetReferenceConst()->m_SubMeshes)
				{
					if (SubMesh.m_sSubName == sName)
					{
						// found the mesh
						break;
					}
					++i;
				}

				if (i < CRCs.size())
				{
					m_Desc.ConvexMesh.uCRC32 = CRCs[i];
					m_Desc.kType = GeometryType_ConvexMesh;
					Create(m_Desc);
				}
			}			
		}
	}
}
//---------------------------------------------------------------------------------------------------------------------
void ShapeComponent::RegisterCallback(Physics::TriggerEvent::Callback _Callback)
{
	m_OnTrigger.AddListener(_Callback);
	if (m_Desc.kUsage != Physics::ShapeUsageType_Trigger)
	{
		HWARNING("Registered trigger callback to a shape that is not of trigger usage type. Change the usage type of the shape to receive events.");
	}
	if (m_OnTrigger.GetListenerCount() == 1u)
	{
		Physics::SimulationEventCallback::Instance()->GetOnTrigger().RegisterShape(m_pShape, &m_OnTrigger);
	}
}
//---------------------------------------------------------------------------------------------------------------------
void ShapeComponent::RemoveCallback(Physics::TriggerEvent::Callback& _Callback)
{
	m_OnTrigger.RemoveListener(_Callback);
	if (m_OnTrigger.GetListenerCount() == 0u)
	{
		Physics::SimulationEventCallback::Instance()->GetOnTrigger().RemoveShape(m_pShape);
	}
}
//---------------------------------------------------------------------------------------------------------------------
void ShapeComponent::RegisterCallback(Physics::ContactEvent::Callback _Callback)
{
	m_OnContact.AddListener(_Callback);
	if (m_Desc.kUsage != Physics::ShapeUsageType_Simulation)
	{
		HWARNING("Registered contact callback to a shape that is not of simulation usage type. Change the usage type of the shape to receive events.");
	}
	if (m_OnContact.GetListenerCount() == 1u)
	{
		Physics::SimulationEventCallback::Instance()->GetOnContact().RegisterShape(m_pShape, &m_OnContact);
	}
}
//---------------------------------------------------------------------------------------------------------------------
void ShapeComponent::RemoveCallback(Physics::ContactEvent::Callback& _Callback)
{
	m_OnContact.RemoveListener(_Callback);
	if (m_OnContact.GetListenerCount() == 0u)
	{
		Physics::SimulationEventCallback::Instance()->GetOnContact().RemoveShape(m_pShape);
	}
}
//---------------------------------------------------------------------------------------------------------------------
void ShapeComponent::ActivateUsage(bool _bActivate)
{
	if (m_pShape == nullptr)
		return;

	Physics::SceneWriteLock lock;

	switch (m_Desc.kUsage)
	{
	case ShapeUsageType_Query:
		m_pShape->setFlag(physx::PxShapeFlag::eSCENE_QUERY_SHAPE, _bActivate);
		break;
	case ShapeUsageType_Simulation:
		m_pShape->setFlag(physx::PxShapeFlag::eSIMULATION_SHAPE, _bActivate);
		break;
	case ShapeUsageType_Trigger:
		m_pShape->setFlag(physx::PxShapeFlag::eTRIGGER_SHAPE, _bActivate);
		break;
	default:
		break;
	}
}
//---------------------------------------------------------------------------------------------------------------------

void ShapeComponent::OnDeserialize(const hlx::TextToken& _Token)
{
	hlx::TextToken InToken;

	if (_Token.getKey() == "ShapeComponent")
	{
		InToken = _Token;
	}
	else if (_Token.getTokenConst("ShapeComponent", InToken) == false)
	{
		return;
	}

	m_Desc.kType = InToken.get<GeometryType>("Type");
	m_Desc.kUsage = InToken.get<ShapeUsageType>("Usage");

	m_Desc.Filter.word0 = InToken.get<uint32_t>("Filter0", CollisionFilterFlag_None);
	m_Desc.Filter.word1 = InToken.get<uint32_t>("Filter1", CollisionFilterFlag_None);
	m_Desc.Filter.word2 = InToken.get<uint32_t>("Filter2", CollisionFilterFlag_None);
	m_Desc.Filter.word3 = InToken.get<uint32_t>("Filter3", CollisionFilterFlag_None);

	m_Desc.vScale = to(InToken.getVector<float, 3>("Scale", HFLOAT3_ONE));
	m_Desc.bScaleWithParent = InToken.get<bool>("ScaleWithParent", true);

	m_Desc.LocalTransform = GetTransform("LocalTransfrom", InToken);

	hlx::TextToken Material;
	if (InToken.getTokenConst("SurfaceMaterial", Material))
	{
		m_Desc.Material.fStaticFriction = Material.get<float>("StaticFriction", 0.2f);
		m_Desc.Material.fDynamicFriction = Material.get<float>("DynamicFriction", 0.2f);
		m_Desc.Material.fRestitution = Material.get<float>("Restitution", 0.0125f);
		m_Desc.Material.kRestitutionCombineMode = Material.get<CombineMode>("RestitutionCombineMode", CombineMode_Average);
		m_Desc.Material.kFrictionCombineMode = Material.get<CombineMode>("FrictionCombineMode", CombineMode_Average);
		m_Desc.Material.kFrictionFlags = Material.get<FrictionFlag>("FrictionFlags", FrictionFlag_None);
	}

	hlx::TextToken Shape;
	if (InToken.getTokenConst("Shape", Shape))
	{
		switch (m_Desc.kType)
		{
		case GeometryType_Box:
			m_Desc.Box.vHalfExtents = to(Shape.getVector<float, 3>("HalfExtents", { 0.5f, 0.5f, 0.5f }));
			break;
		case GeometryType_Sphere:
			m_Desc.Sphere.fRadius = Shape.get<float>("Radius", 0.5f);
			break;
		case GeometryType_Capsule:
			m_Desc.Capsule.fRadius = Shape.get<float>("Radius", 0.25f);
			m_Desc.Capsule.fHalfHeight = Shape.get<float>("HalfHeight", 0.25f);
			break;
		case GeometryType_ConvexMesh:
			m_Desc.ConvexMesh.uCRC32 = Shape.get<uint32_t>("CRC32");
			break;
		}
	}

	Create(m_Desc);
}
//---------------------------------------------------------------------------------------------------------------------

bool ShapeComponent::OnSerialize(hlx::TextToken& _Token) const
{
	_Token.set("Type", m_Desc.kType);
	_Token.set("Usage", m_Desc.kUsage);

	_Token.set("Filter0", m_Desc.Filter.word0);
	_Token.set("Filter1", m_Desc.Filter.word1);
	_Token.set("Filter2", m_Desc.Filter.word2);
	_Token.set("Filter3", m_Desc.Filter.word3);

	_Token.setVector("Scale", to(m_Desc.vScale));
	_Token.set("ScaleWithParent", m_Desc.bScaleWithParent);
	SetTranfsorm("LocalTransfrom", _Token, m_Desc.LocalTransform);

	hlx::TextToken Material("SurfaceMaterial");
	Material.set("StaticFriction", m_Desc.Material.fStaticFriction);
	Material.set("DynamicFriction", m_Desc.Material.fDynamicFriction);
	Material.set("Restitution", m_Desc.Material.fRestitution);
	Material.set("RestitutionCombineMode", m_Desc.Material.kRestitutionCombineMode);
	Material.set("FrictionCombineMode", m_Desc.Material.kFrictionCombineMode);
	Material.set("FrictionFlags", m_Desc.Material.kFrictionFlags);

	_Token.setToken("SurfaceMaterial", Material);

	if (m_Desc.kType == GeometryType_Unknown)
		return false;

	hlx::TextToken Shape("Shape");
	switch (m_Desc.kType)
	{
	case GeometryType_Box:
		Shape.setVector("HalfExtents", to(m_Desc.Box.vHalfExtents));
		break;
	case GeometryType_Sphere:
		Shape.set("Radius", m_Desc.Sphere.fRadius);
		break;
	case GeometryType_Capsule:
		Shape.set("Radius", m_Desc.Capsule.fRadius);
		Shape.set("HalfHeight", m_Desc.Capsule.fHalfHeight);
		break;
	case GeometryType_ConvexMesh:
		Shape.set("CRC32", m_Desc.ConvexMesh.uCRC32);
		break;
	default:
		return false;
	}

	_Token.setToken("Shape", Shape);

	return true;
}
//---------------------------------------------------------------------------------------------------------------------
