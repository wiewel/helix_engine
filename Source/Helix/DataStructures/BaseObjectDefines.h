//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef BASEOBJECTDEFINES_H
#define BASEOBJECTDEFINES_H

#include <stdint.h>
#include <type_traits>

namespace Helix
{
	namespace Datastructures
	{
		enum ObjectType : uint32_t
		{
			ObjectType_BaseObject = (1 << 0),
			ObjectType_RenderObject = (1 << 1) | ObjectType_BaseObject,
			ObjectType_GameObject = (1 << 2) | ObjectType_RenderObject,
		};

		class UserData
		{
		protected:
			UserData(const uint64_t _kTypeID, const size_t _uSize) :
				kType(_kTypeID), uSize(_uSize){}

		public:
			const uint64_t kType;
			const size_t uSize;	

			template <class T, typename std::enable_if<std::is_base_of<UserData, T>::value>::type* = 0>
			inline T* Get(const uint64_t _kTypeID)
			{
				if (sizeof(T) == uSize && _kTypeID == kType)
				{
					return reinterpret_cast<T*>(this);
				}

				return nullptr;
			}
		};

		//template <uint64_t TypeID>
		//class UserDataT : public UserData
		//{
		//	static constexpr uint64_t ID = TypeID;
		//	UserDataT() : UserData(TypeID, sizeof()){}
		//};

	}
} // Helix

#endif // BASEOBJECTDEFINES_H
