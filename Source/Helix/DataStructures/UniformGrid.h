//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef UNIFORMGRID_H
#define UNIFORMGRID_H

#include "Debug.h"
#include "GameObject.h"
#include <vector>
#include <list>

namespace helix
{
	namespace datastructures
	{
		using namespace DirectX;

		typedef std::list<GameObject*> TCell;
		typedef std::vector<TCell> TCells;

		class UniformGrid
		{
		public:
			UniformGrid::UniformGrid(uint32_t _uCellCount, float _fGridCellSize);

			void Update(GameObject* _pEntity);

			void GatherCollisions(_Out_ std::vector<collision::CollisionInfo>& _Collisions);

			uint32_t ComputeHash(const XMFLOAT3& _vPosition);
			uint32_t ComputeHash(int32_t _iX, int32_t _iY, int32_t _iZ);

		private:
			TCells m_Cells;

			uint32_t m_uCellCount;
			float m_fGridCellSize;
		};
	}
}

#endif // UNIFORMGRID_H