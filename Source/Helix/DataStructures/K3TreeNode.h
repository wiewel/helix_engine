//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef K3TREENODE_H
#define K3TREENODE_H

#include "Debug.h"
#include "GameObject.h"
#include "CollisionContact.h"
//#include "AsyncObject.h"
#include "StdAfxH.h"
#include <mutex>
#include <vector>

#include <algorithm>

namespace helix
{
	namespace datastructures
	{
		using namespace DirectX;

		enum SeparatingAxis
		{
			SeparatingAxis_X = 0,
			SeparatingAxis_Y = 1,
			SeparatingAxis_Z = 2
		};

		typedef std::vector<GameObject*> TEntityVector;

		class K3TreeNode /*: public IAsyncObject*/
		{
		public:
			HDEBUGNAME("K3Node");

			K3TreeNode(TEntityVector::iterator _Begin, TEntityVector::iterator _End, uint32_t _uDepth);
			~K3TreeNode();

			void GatherCollisions(_In_ GameObject* _pEntity, _Out_ std::vector<collision::CollisionInfo>& _Collisions);

		private:
			SeparatingAxis m_Axis;

			GameObject* m_pEntity = nullptr;

			K3TreeNode* m_pLeft = nullptr;
			K3TreeNode* m_pRight = nullptr;
		};
	}
}


#endif // K3TREENODE_H