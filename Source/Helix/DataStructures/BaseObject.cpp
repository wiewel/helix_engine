//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "BaseObject.h"
#include "hlx\src\Logger.h"
#include "Scene\ComponentScene.h"

using namespace Helix::Datastructures;
//---------------------------------------------------------------------------------------------------

IBaseObject::IBaseObject(const ObjectType _kType, IBaseObject* const _pParent) :
	m_kType(_kType), m_pParent(_pParent) 
{
}
//---------------------------------------------------------------------------------------------------

IBaseObject::~IBaseObject()
{
	Uninitialize();
}
//---------------------------------------------------------------------------------------------------
void IBaseObject::Uninitialize()
{
	while (m_Components.empty() == false)
	{
		IComponent* pComponent = m_Components.back();
		m_Components.pop_back();

		if (pComponent->m_kCallbacks & ComponentCallback_OnDestroy)
		{
			pComponent->OnDestroy();
		}

		HSAFE_DELETE(pComponent);
	}
}
//---------------------------------------------------------------------------------------------------
bool IBaseObject::DeserializeComponent(const hlx::TextToken& _Token)
{
	IComponent* pTemplate = nullptr;

	if (_Token.getKey() != "Component" || _Token.getChildCount() != 1u)
	{
		HERROR("Token has no Compoment data!");
		return false;
	}

	// identify component
	int32_t iComponentTypeID = _Token.get<int32_t>("TypeID");
	std::string sTokenTypeName = _Token.getValue("TypeName");
	ObjectType kRequiredType = _Token.get<ObjectType>("RequiredObjectType");
	TComponentCallbacks kCallbacks = _Token.get<ComponentCallback>("Callbacks");

	// try to find matching template for the typeid
	pTemplate = Helix::Scene::ComponentScene::Instance()->GetTemplate(iComponentTypeID);

	if (pTemplate != nullptr)
	{
		// create a base component base on the derived CreateComponent function of the actual type
		IComponent* pComponent = pTemplate->CreateComponent(this, kRequiredType, kCallbacks);
		if (pComponent != nullptr && pComponent->operator bool())
		{
			// get the token and use it for deserialization (setting member parameters of the new component)
			hlx::TextToken ComponentToken;
			if (_Token.getTokenConst(sTokenTypeName, ComponentToken))
			{
				pComponent->OnDeserialize(ComponentToken);
				m_Components.push_back(pComponent); // gameobject has to manage components
				return true;
			}
			else
			{
				HERROR("No token with typename %s found", CSTR(sTokenTypeName));
			}			
		}

		// object type does not match
		delete pComponent;
		return false;
	}

	HERROR("No matching component template found for ID %d", iComponentTypeID);

	return false;
}
//---------------------------------------------------------------------------------------------------