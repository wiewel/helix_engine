//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef COMPONENT_H
#define COMPONENT_H

#include "hlx\src\StandardDefines.h"
#include "Util\Flag.h"
#include "hlx\src\TextToken.h"
#include "BaseObjectDefines.h"
#include "Util\UniqueID.h"
#include "hlx\src\Singleton.h"

namespace Helix
{
	// forward decls
	namespace Scene
	{
		class ComponentScene;
	}

	namespace Datastructures
	{
		class ComponentID : public UniqueID<uint64_t>, public hlx::Singleton<ComponentID> {};

		enum ComponentCallback : uint32_t
		{
			ComponentCallback_None = 0,
			ComponentCallback_OnUpdate = 1 << 0,
			ComponentCallback_OnLevelLoaded = 1 << 1,
			ComponentCallback_OnDestroy = 1 << 2,
			ComponentCallback_OnSerialize = 1 << 3,
			ComponentCallback_Unknown
		};

		// forward decls
		class IBaseObject;
		struct SyncComponent;

		using TComponentCallbacks = Flag<ComponentCallback>;

		// every derived component must define this macro to ensure the interface compatibility
#ifndef COMPTYPENAME_EX
#define COMPTYPENAME_EX(_comp_type, _obj_type, _int32_id, _required_callbacks) public: \
	using BaseType = _obj_type; \
	friend class Helix::Scene::ComponentScene;\
	friend class Helix::Datastructures::IBaseObject;\
	_comp_type(const _comp_type&) = delete; \
	_comp_type& operator=(const _comp_type& _Other);\
	static constexpr int32_t ID = _int32_id; \
	inline virtual int32_t GetTypeID() const override { return _int32_id; } \
	inline virtual const std::string GetTypeName() const override {return #_comp_type;} \
	static constexpr Helix::Datastructures::TComponentCallbacks RequiredCallbacks = _required_callbacks; \
	inline virtual Helix::Datastructures::TComponentCallbacks GetRequiredCallbacks() const override { return _required_callbacks; } \
	protected: virtual ~_comp_type(); private:\
	inline virtual Helix::Datastructures::IComponent* CreateComponent(Helix::Datastructures::IBaseObject* _pParent, const Helix::Datastructures::ObjectType _kRequiredObjectType, Helix::Datastructures::TComponentCallbacks _kCallback) const override \
	{return new _comp_type(static_cast<_obj_type*>(_pParent), _kRequiredObjectType, _kCallback | _required_callbacks);} public:
#endif
		// HNEW does not work because the preprocessor does not reevaluate the generated expression

#ifndef COMPTYPENAME
#define COMPTYPENAME(_comp_type, _obj_type, _int32_id) COMPTYPENAME_EX(_comp_type, _obj_type, _int32_id, Helix::Datastructures::ComponentCallback_None)
#endif

		// template register constructor, user can also provide own implementation initializing other (const) members
#ifndef COMPDEFCONSTR
#define COMPDEFCONSTR(_comp_type, _obj_type, _parent_type) private: \
	inline _comp_type(DefaultInitializerType) : _parent_type(DefaultInit) {} public:
#endif

		// default deserialization constructor, user can also provie own implementation initializing other (const) members
#ifndef COMPDESCONSTR
#define COMPDESCONSTR(_comp_type, _obj_type, _parent_type) private: \
	inline _comp_type(_obj_type* _pParent, const Helix::Datastructures::ObjectType _kRequiredObjectType, Helix::Datastructures::TComponentCallbacks _kCallbacks) : \
			_parent_type(_pParent, _kRequiredObjectType, _kCallbacks) {} public:
#endif

		class IComponent
		{
			friend class IBaseObject;
			friend class Scene::ComponentScene;
		protected:
			IComponent(
				IBaseObject* _pParent,
				const ObjectType _kRequiredObjectType,
				TComponentCallbacks _kCallbacks = ComponentCallback_None);

			// for template registration
			IComponent(DefaultInitializerType);

			virtual ~IComponent();

			// restrict copying, parent has to keep the reference and delete it
			IComponent(const IComponent&) = delete;
			// assigning is okay as long as the type matches, parent cant be overwriteen
			IComponent& operator=(const IComponent& _Other);

			// for deserialization construction
			virtual IComponent* CreateComponent(IBaseObject* _pParent, const ObjectType _kRequiredObjectType, TComponentCallbacks _kCallback) const = 0;

			void Initialize();

		public:
			// removes component from parent objects and calls the destructor
			void Destroy();

			virtual void OnUpdate(double _fDeltaTime, double _fTotalTime) {}
			virtual void OnLevelLoaded() {}
			virtual void OnDestroy() {}

			virtual void OnDeserialize(const hlx::TextToken& _Token) {};
			// user needs to return true if data was serialized into the token
			virtual bool OnSerialize(_Out_ hlx::TextToken& _Token) const { return false; };

			// allows the user to bind components to the object SyncDock
			virtual SyncComponent* GetSyncComponent() { return nullptr; };

			const ObjectType GetRequiredObjectType() const;
			const TComponentCallbacks& GetActiveCallbacks() const;

			virtual int32_t GetTypeID() const = 0;
			virtual const std::string GetTypeName() const = 0;
			virtual TComponentCallbacks GetRequiredCallbacks() const = 0;

			// return true if base type and required object type match
			explicit operator bool() const;

			const uint64_t GetID() const;

			virtual IBaseObject* GetParent() const;

		protected:
			void AddCallback(const ComponentCallback& _kCallbacks);
			void RemoveCallback(const ComponentCallback& _kCallbacks);

			const uint64_t m_uID;

		private:
			IBaseObject* const m_pParent;
			TComponentCallbacks m_kCallbacks;
			const ObjectType m_kRequiredObjectType;
		};

		inline IBaseObject* IComponent::GetParent() const{ return m_pParent;}
		inline const ObjectType IComponent::GetRequiredObjectType() const{return m_kRequiredObjectType;	}
		inline const TComponentCallbacks& IComponent::GetActiveCallbacks() const{return m_kCallbacks;}
		inline const uint64_t IComponent::GetID() const
		{
			return m_uID;
		}

	} // Datastructures
} // Helix

#endif // !GAMEOBJECTCOMPONENT_H
