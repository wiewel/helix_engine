//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "TransformHierarchyComponent.h"
#include "Scene\TransformHierarchy.h"

using namespace Helix::Datastructures;

//---------------------------------------------------------------------------------------------------
TransformHierarchyComponent::TransformHierarchyComponent(GameObject * _pParent)
	: IGameObjectComponent(_pParent, ComponentCallback_OnUpdate)
{
	Scene::TransformHierarchy::Instance()->RegisterComponent(this);
}
//---------------------------------------------------------------------------------------------------
TransformHierarchyComponent::~TransformHierarchyComponent()
{
	Scene::TransformHierarchy::Instance()->RemoveComponent(this);
}
//---------------------------------------------------------------------------------------------------
void TransformHierarchyComponent::OnUpdate(double _fDeltaTime, double _fTotalTime)
{
	if (m_pParent == nullptr)
	{
		m_LocalTransform = GetParent()->GetTransform();
	}
	else if (m_pNode)
	{
		GetParent()->SetTransform( m_pNode->GetTransform());
	}
}
