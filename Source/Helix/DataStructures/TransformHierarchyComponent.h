//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef TRANSFORMHIERARCHYCOMPONENT_H
#define TRANSFORMHIERARCHYCOMPONENT_H

#include "GameObjectComponent.h"
#include "Math\MathTypes.h"
#include "Display\DX11\RenderObjectDX11.h"

namespace Helix
{
	namespace Scene
	{
		class TransformHierarchy;
		struct TransformNode;
	}
	namespace Datastructures
	{
		class TransformHierarchyComponent : public IGameObjectComponent
		{
			friend class Scene::TransformHierarchy;

		public:
			TransformHierarchyComponent(GameObject* _pParent);
			COMPTYPENAME(TransformHierarchyComponent, GameObject, 'Hrch');
			COMPDESCONSTR(TransformHierarchyComponent, GameObject, IGameObjectComponent);

			void OnUpdate(double _fDeltaTime, double _fTotalTime) final;
			
			void SetLocalTransform(const Math::transform& _LocalTransform);
			const Math::transform& GetLocalTransform() const;
			void SetParentNode(TransformHierarchyComponent* _pParent);
			TransformHierarchyComponent* GetParentNode() const;

		private:
			const Scene::TransformNode* m_pNode = nullptr;
			Math::transform m_LocalTransform;
			TransformHierarchyComponent* m_pParent = nullptr;
		};
		//---------------------------------------------------------------------------------------------------
		inline void TransformHierarchyComponent::SetLocalTransform(const Math::transform & _LocalTransform)
		{
			m_LocalTransform = _LocalTransform;
		}
		//---------------------------------------------------------------------------------------------------
		inline const Math::transform & TransformHierarchyComponent::GetLocalTransform() const
		{
			return m_LocalTransform;
		}
		//---------------------------------------------------------------------------------------------------
		inline void TransformHierarchyComponent::SetParentNode(TransformHierarchyComponent * _pParent)
		{
			m_pParent = _pParent;
		}
		//---------------------------------------------------------------------------------------------------
		inline TransformHierarchyComponent * TransformHierarchyComponent::GetParentNode() const
		{
			return m_pParent;
		}
	}
}
#endif // !TRANSFORMHIERARCHYCOMPONENT_H
