//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "GameObjectPool.h"
#include "GameObject.h"
#include "hlx\src\Profiler.h"

using namespace Helix::Datastructures;

GameObjectPool::GameObjectPool()
{
}
//---------------------------------------------------------------------------------------------------
bool GameObjectPool::Initialize(uint32_t _uCount)
{
	m_bIsLocked.store(false);

	HASSERT(m_Usage.empty(), "GameObjectPool has already been initialized!");

	m_Usage.resize(_uCount);

	//Initialization of references
	for (uint32_t i = 0; i < _uCount; ++i)
	{
		m_Usage.at(i) = new GameObject(*this, i);
		m_Usage.at(i)->m_uUsageIndex = i;
	}

	HLOG("Initialized GameObjectPool with %u entities", _uCount);
	return true;
}
//---------------------------------------------------------------------------------------------------
GameObjectPool::~GameObjectPool()
{
	Destroy();
}
//---------------------------------------------------------------------------------------------------
void GameObjectPool::Destroy()
{
	m_bIsLocked.store(true);
	std::lock_guard<std::mutex> Lock(m_Mutex);

	HSAFE_VECTOR_DELETE(m_Usage);
	m_Usage.resize(0);

	HASSERTD(m_NamedObjects.empty(), "Some named objects have not been destoyed properly");
	HASSERTD(m_TaggedObjects.empty(), "Some tagged objects have not been destoyed properly");

	m_NamedObjects.clear();
	m_TaggedObjects.clear();

	m_bIsLocked.store(false);
}
//---------------------------------------------------------------------------------------------------
std::future<GameObject*> GameObjectPool::Create()
{
	return std::async(std::launch::async, [&]()
	{ 
		GameObject* pObject = nullptr;

		{
			m_bIsLocked.store(true);

			std::lock_guard<std::mutex> Lock(m_Mutex);

			pObject = Alloc();
		}
		
		m_bIsLocked.store(false);

		return pObject;	
	});
}
//---------------------------------------------------------------------------------------------------
std::future<std::vector<GameObject*>> GameObjectPool::Create(uint32_t _uCount)
{
	return std::async(std::launch::async, [&]()
	{
		std::vector<GameObject*> Objects;

		{
			m_bIsLocked.store(true);

			std::lock_guard<std::mutex> Lock(m_Mutex);

			GameObject* pObject = nullptr;
			for (uint32_t i = 0; i < _uCount; ++i)
			{
				if ((pObject = Alloc()) == nullptr)
				{
					// Allocation failed, pool is occupied
					break;
				}

				Objects.push_back(pObject);
			}
		}
		
		m_bIsLocked.store(false);

		return Objects;
	});
}
//---------------------------------------------------------------------------------------------------
GameObject* GameObjectPool::Alloc()
{
	const size_t uUsageSize = m_Usage.size();
	//no free slots
	if (m_uFirstFree == uUsageSize)
	{
		const size_t uCount = (m_Usage.size() / 3u) + 1u;
		const size_t uNewSize = uUsageSize + uCount;

		m_Usage.resize(uNewSize);

		for (size_t i = uUsageSize; i < uNewSize; ++i)
		{
			m_Usage.at(i) = new GameObject(*this, static_cast<uint32_t>(i));
			m_Usage.at(i)->m_uUsageIndex = static_cast<uint32_t>(i);
		}
	}

	// return first free slot and increment
	GameObject* pGO = m_Usage.at(m_uFirstFree++);
	
	pGO->m_bAccessible = true;
	pGO->m_Flags.ResetFlag();

	HPROFILE_COUNT(GameObjects, m_uFirstFree);

	return pGO;
}
//---------------------------------------------------------------------------------------------------
void GameObjectPool::RemoveNameEntry(GameObject * _pMem)
{
	Async::ScopedSpinLock Lock(m_NameMapLock);

	auto pair = m_NamedObjects.equal_range(_pMem->GetName());
	for (TNameMap::iterator it = pair.first; it != pair.second; ++it)
	{
		if (it->second == _pMem)
		{
			m_NamedObjects.erase(it);
			break;
		}
	}
}
//---------------------------------------------------------------------------------------------------
void GameObjectPool::Free(GameObject* _pMem)
{
	m_bIsLocked.store(true);

	{
		std::lock_guard<std::mutex> Lock(m_Mutex);

		HASSERT(_pMem->m_bAccessible && m_Usage.at(_pMem->m_uUsageIndex) == _pMem, "Usage control inconsistency");

		// avoid use after free
		_pMem->m_bAccessible = false;
		
		// swap last used with now freed object
		m_Usage.at(_pMem->m_uUsageIndex) = m_Usage.at(--m_uFirstFree);
		m_Usage.at(_pMem->m_uUsageIndex)->m_uUsageIndex = _pMem->m_uUsageIndex;

		// move freed object to first free index, assign usage index
		m_Usage.at(m_uFirstFree) = _pMem;
		_pMem->m_uUsageIndex = m_uFirstFree;

		HPROFILE_COUNT(GameObjects, m_uFirstFree);
	}

	m_bIsLocked.store(false);
}
//---------------------------------------------------------------------------------------------------
void GameObjectPool::SetName(const std::string& _sOldName, const std::string& _sNewName, GameObject* _pObject)
{
	Async::ScopedSpinLock Lock(m_NameMapLock);

	auto pair = m_NamedObjects.equal_range(_sOldName);
	for (TNameMap::iterator it = pair.first; it != pair.second; ++it)
	{
		if (it->second == _pObject)
		{
			m_NamedObjects.erase(it);
			break;
		}
	}

	m_NamedObjects.insert({_sNewName, _pObject});
}
//---------------------------------------------------------------------------------------------------
void GameObjectPool::AddTag(const uint64_t _uTag, GameObject* _pObject)
{
	Async::ScopedSpinLock Lock(m_TagMapLock);

#ifdef _DEBUG
	auto pair = m_TaggedObjects.equal_range(_uTag);
	for (TTagMap::iterator it = pair.first; it != pair.second; ++it)
	{
		if (it->second == _pObject)
		{
			m_TaggedObjects.erase(it);
			HERROR("GameObject %s allready associated to tag %llu", CSTR(_pObject->m_sName), _uTag);
			//break;
		}
	}
#endif

	m_TaggedObjects.insert({ _uTag, _pObject });
}
//---------------------------------------------------------------------------------------------------
void GameObjectPool::RemoveTags(const Helix::Flag<uint64_t>& _kTags, GameObject* _pObject)
{
	Async::ScopedSpinLock Lock(m_TagMapLock);

	for (uint8_t i = 0; i < 64u && m_TaggedObjects.empty() == false; ++i)
	{
		uint64_t uTag = 1ull << i;
		if (_kTags.CheckFlag(uTag))
		{
			auto pair = m_TaggedObjects.equal_range(uTag);
			for (TTagMap::iterator it = pair.first; it != pair.second; ++it)
			{
				if (it->second == _pObject)
				{
					m_TaggedObjects.erase(it);
					break;
				}
			}
		}
	}
}
//---------------------------------------------------------------------------------------------------
void GameObjectPool::Synchronize()
{
	m_bIsLocked.store(true);
	std::lock_guard<std::mutex> Lock(m_Mutex);
	
	for (auto it = cbegin(), end = cend(); it != end; ++it)
	{
		GameObject* pObject = *it;

		physx::PxRigidActor* pActor = pObject->GetActor();
		if (pActor == nullptr || pActor->getScene() == nullptr)
		{
			continue;
		}

		pObject->m_LockObj.Lock();
		pObject->m_bLocking = false;

		physx::PxRigidDynamic* pDynamic = pActor->is<physx::PxRigidDynamic>();
		const TGOSyncFlags& kComponents = pObject->m_kUpdatedComponents;
		bool bKinematic = pObject->m_bKinematic;
		Math::transform Transform = pObject->m_Transform;
		bool bDisableSim = pObject->m_bDisableSimuation;

		// SIMULATION
		if (kComponents & GOSyncFlag_DisableSimulation)
		{
			pActor->setActorFlag(physx::PxActorFlag::eDISABLE_SIMULATION, bDisableSim);
		}

		if (bDisableSim == false) // simulation is enabled for this object
		{
			// DYNAMIC ACTOR
			if (pObject->GetDynamic())
			{
				// KINEMATIC			
				if (kComponents & GOSyncFlag_Kinematic)
				{
					pDynamic->setRigidBodyFlag(physx::PxRigidBodyFlag::eKINEMATIC, bKinematic);
				}

				// SLEEPING
				if (kComponents & GOSyncFlag_WakeUp && bKinematic == false)
				{
					pDynamic->wakeUp();
				}

				bool bSleeping = pDynamic->isSleeping();

				// TRANSFORM
				if (kComponents & GOSyncFlag_Transform)
				{
					if (bKinematic)
					{
						pDynamic->setKinematicTarget(Transform);
					}
					else
					{
						pDynamic->setGlobalPose(Transform);
					}
				}
				else if (bSleeping == false)
				{
					if (pDynamic->getKinematicTarget(pObject->m_Transform) == false)
					{
						pObject->m_Transform = pDynamic->getGlobalPose();
					}
				}

				// LINEAR VELOCITY
				if (kComponents & GOSyncFlag_LinearVelocity)
				{
					pDynamic->setLinearVelocity(pObject->m_vLinearVelocity);
				}
				else if (bSleeping == false)
				{
					pObject->m_vLinearVelocity = pDynamic->getLinearVelocity();
				}

				// MASS
				if (kComponents & GOSyncFlag_Mass)
				{
					physx::PxRigidBodyExt::setMassAndUpdateInertia(*pDynamic, pObject->m_fMass);
				}
			}
			else // static actor
			{
				if (kComponents & GOSyncFlag_Transform)
				{
					pActor->setGlobalPose(Transform);
				}
			}
		}		

		pObject->m_kUpdatedComponents.ResetFlag();
		pObject->m_LockObj.Unlock();
		pObject->m_bLocking = true;
	}

	m_bIsLocked.store(false);
}
//---------------------------------------------------------------------------------------------------
void GameObjectPool::GetObjectsByName(const std::string& _sName, std::vector<GameObject*>& _Objects)
{
	Async::ScopedSpinLock Lock(m_NameMapLock);

	auto pair = m_NamedObjects.equal_range(_sName);
	for (TNameMap::iterator it = pair.first; it != pair.second; ++it)
	{
		_Objects.push_back(it->second);
	}
}
//---------------------------------------------------------------------------------------------------
GameObject* GameObjectPool::GetObjectByName(const std::string & _sName)
{
	Async::ScopedSpinLock Lock(m_NameMapLock);

	TNameMap::iterator it = m_NamedObjects.find(_sName);
	if (it != m_NamedObjects.end())
	{
		return it->second;
	}
	return nullptr;
}
//---------------------------------------------------------------------------------------------------
void GameObjectPool::GetGameObjectsByTag(const std::string& _sTag, std::vector<GameObject*>& _Objects)
{
	GetGameObjectsByTag(GameObject::GetTagID(_sTag), _Objects);
}
//---------------------------------------------------------------------------------------------------

void GameObjectPool::GetGameObjectsByTag(const uint64_t _uTag, std::vector<GameObject*>& _Objects)
{
	Async::ScopedSpinLock Lock(m_TagMapLock);

	auto pair = m_TaggedObjects.equal_range(_uTag);
	for (TTagMap::iterator it = pair.first; it != pair.second; ++it)
	{
		_Objects.push_back(it->second);
	}
}
//---------------------------------------------------------------------------------------------------

void GameObjectPool::GetGameObjectsByTags(const Helix::Flag<uint64_t> _kTags, std::vector<GameObject*>& _Objects)
{
	for (uint8_t i = 0; i < 64u; ++i)
	{
		uint64_t uTag = 1ull << i;
		if (_kTags.CheckFlag(uTag))
		{
			GetGameObjectsByTag(uTag, _Objects);
		}
	}
}
//---------------------------------------------------------------------------------------------------
