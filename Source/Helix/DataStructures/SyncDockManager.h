//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef SYNCDOCKMANAGER_H
#define SYNCDOCKMANAGER_H

#include "hlx\src\Singleton.h"
#include "Async\SpinLock.h"
#include "Async\Delegate.h"
#include <functional>
#include <set>

namespace Helix
{
	namespace Nucleo
	{
		class Nucleo;
	}

	namespace Datastructures
	{
		// forward decls;
		struct SyncVariableBase;
		struct SyncDock;

		using TBindFunc = std::function<void(const SyncDock*)>;
		using TUnbindFunc = std::function<void(const SyncDock*)>;

		class SyncDockManager : hlx::Singleton<SyncDockManager>
		{
			friend class Nucleo::Nucleo;
			friend struct SyncDock;
		public:
			SyncDockManager();
			~SyncDockManager();
			
			void SetBindDelegates(const TBindFunc& _Bind, const TUnbindFunc& _Unbind);

		private:
			void Bind(const SyncDock* _pSyncDock);
			void Unbind(const SyncDock* _pSyncDock);
			void Register(const SyncDock* _pSyncDock);

		private:
			Async::SpinLock m_Lock;
			std::set<std::string> m_ExistingDocks;

			TBindFunc m_BindFunc;
			TUnbindFunc m_UnbindFunc;
		};

	}
} // Helix

#endif // !SYNCDOCKMANAGER_H
