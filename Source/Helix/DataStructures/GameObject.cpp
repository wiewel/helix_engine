//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "GameObject.h"
#include "Scene\PhysicsScene.h"
#include "DataStructures\ShapeComponent.h"

using namespace Helix::Display;
using namespace Helix::Datastructures;
using namespace Helix::Math;
using namespace Helix::Physics;

Helix::UniqueAssociationID<std::string, uint64_t> GameObject::m_TagManager;
//---------------------------------------------------------------------------------------------------
//constructor
GameObject::GameObject(GameObjectPool& _GameObjectPool, uint32_t _uID) :
	RenderObjectDX11(RenderObjectDX11::GOIDType()), // pass token
	m_GameObjectPool(_GameObjectPool),
	ID(_uID)
{
#ifdef HNUCLEO
	m_ObjectGroup.AddVariable(&m_SyncPosition);
	m_ObjectGroup.AddVariable(&m_SyncOrientation);
	m_ObjectGroup.AddVariable(&m_SyncKinematic);
	m_ObjectGroup.AddVariable(&m_SyncKinematicAsStatic);
	m_ObjectGroup.AddVariable(&m_SyncVisible);
	m_ObjectGroup.AddVariable(&m_SyncMass);
	m_ObjectGroup.AddVariable(&m_SyncTag);
#endif
}
//---------------------------------------------------------------------------------------------------
GameObject::~GameObject()
{
	if (m_pActor != nullptr)
	{
		HWARNING("Physics components of %s have not been released properly (Level has not been unloaded or dynamic objects have not been destroyed)", CSTR(m_sName));
	}

	UninitializeActor();
}

//---------------------------------------------------------------------------------------------------
void GameObject::Initialize(const GameObjectDesc& _Desc)
{
	HASSERT(_Desc.kType != GameObjectType_RigidDynamic || _Desc.kType != GameObjectType_RigidStatic, "Invalid GameObjectType");

	m_Flags = _Desc.kFlags;
	m_bKinematic = m_Flags.CheckFlag(GameObjectFlag_Kinematic);
	SetTagsFromString(_Desc.sTags);

#ifdef HNUCLEO
	size_t uTags = _Desc.sTags.size();
	for (size_t i = 0; i < uTags; i++)
	{
		m_sTagString += _Desc.sTags[i];
		if (i < uTags-1)
		{
			m_sTagString += " ";
		}
	}
#endif

	switch (_Desc.kType)
	{
	case GameObjectType_RigidStatic:
		CreatePhysicsActorStatic(_Desc.Transform);
		break;
	case GameObjectType_RigidDynamic:
		CreatePhysicsActorDynamic(
			_Desc.Transform,
			_Desc.fMass,
			m_bKinematic,
			_Desc.bContinuousCollisionDetection);
		break;
	default:
		break;
	}

	// must be called after the actor is created to be able do assign a debug name to it
	RenderObjectDX11::Initialize(_Desc);

	if (m_MeshCluster.GetSubMeshes().empty() && m_Materials.empty() && m_Flags.CheckFlag(GameObjectFlag_Invisible) == false)
	{
		HWARNING("Object %s has no SubMeshes and Materials assigned, it will be flagged as invisible", CSTR(_Desc.sObjectName));
		m_Flags.SetFlag(GameObjectFlag_Invisible);
	}
}

//---------------------------------------------------------------------------------------------------

GameObjectDesc GameObject::CreateDescription() const
{
	GameObjectDesc Desc;

	if (m_pActor == nullptr)
	{
		HFATAL("Gameobject %s has no valid actor!", CSTR(m_sName));
		return Desc;
	}

	Desc = RenderObjectDX11::CreateDescription();

#ifdef HNUCLEO
	bool bDynamic = m_bDynamicActor && (Flags & GameObjectFlag_Kinematic) && m_bExportKinematicAsStatic;
	Desc.kType = bDynamic ? GameObjectType_RigidDynamic : GameObjectType_RigidStatic;

	if (m_sTagString.empty() == false)
	{
		Desc.sTags = hlx::split(m_sTagString, " ");
	}
#else
	Desc.kType = m_bDynamicActor ? GameObjectType_RigidDynamic : GameObjectType_RigidStatic;
#endif

	Desc.kFlags = m_Flags;
	Desc.fMass = GetMass();

	// TODO: CCD & CollisionFilter
	
	return Desc;
}

//---------------------------------------------------------------------------------------------------

void GameObject::CreatePhysicsActorStatic(const transform& _Transform)
{
	HASSERT(m_pActor == nullptr, "PhysX::Actor still alive!");

	m_Transform = _Transform;

	if (m_pActor == nullptr)
	{
		physx::PxPhysics* pPhysics = Scene::PhysicsScene::Instance()->GetPhysics();

		HASSERT(pPhysics != nullptr, "Physx has not been initialized!");

		if (pPhysics != nullptr)
		{
			m_pActor = pPhysics->createRigidStatic(m_Transform);
			m_pActor->userData = this;
		}	
	}

	m_bDynamicActor = false;
}
//---------------------------------------------------------------------------------------------------

void GameObject::CreatePhysicsActorDynamic(const transform& _Transform, float _fMass, bool _bKinematic, bool _bCCD)
{
	HASSERT(m_pActor == nullptr, "PhysX::Actor still alive!");

	m_Transform = _Transform;
	
	if (m_pActor == nullptr)
	{
		physx::PxPhysics* pPhysics = Scene::PhysicsScene::Instance()->GetPhysics();

		HASSERT(pPhysics != nullptr, "Physx has not been initialized!");

		if (pPhysics != nullptr)
		{
			physx::PxRigidDynamic* pDynamic = nullptr;

			m_pActor = pDynamic = pPhysics->createRigidDynamic(m_Transform);
			m_pActor->userData = this;

			if (_bKinematic)
			{
				pDynamic->setRigidBodyFlag(physx::PxRigidBodyFlag::eKINEMATIC, true);
				pDynamic->setRigidBodyFlag(physx::PxRigidBodyFlag::eUSE_KINEMATIC_TARGET_FOR_SCENE_QUERIES, true);
			}
			if (_bCCD)
			{
				pDynamic->setRigidBodyFlag(physx::PxRigidBodyFlag::eENABLE_CCD, true);
			}
			if (physx::PxRigidBodyExt::setMassAndUpdateInertia(*pDynamic, _fMass) == false)
			{
				HERROR("Failed to Inertia for GameObject %s", CSTR(m_sName));
			}
		}
	}

	m_bDynamicActor = true;
}
//---------------------------------------------------------------------------------------------------
bool GameObject::AddShape(const GeometryDesc& _Shape)
{
	HASSERT(m_pActor != nullptr, "Gameobject %s [u] has no valid actor!", CSTR(m_sName), ID);

	ShapeComponent* pComponent = AddComponent<ShapeComponent>(_Shape);

	if (pComponent != nullptr)
	{
		return true;
	}

	HERROR("Failed to create Shape for %s", CSTR(m_sName));
	return false;
}
//---------------------------------------------------------------------------------------------------
void GameObject::AddToScene()
{
	HASSERTD(m_pActor != nullptr, "Invalid Actor, unable to instantiate GameObject");
	if (m_pActor != nullptr)
	{
		physx::PxScene* pScene = Scene::PhysicsScene::Instance()->GetScene();
		SceneWriteLock lock;
		// prints error if already in scene
		pScene->addActor(*m_pActor);
	}
}

//---------------------------------------------------------------------------------------------------
void GameObject::Destroy()
{
	if (m_kTags.GetFlag() != 0u)
	{
		m_GameObjectPool.RemoveTags(m_kTags, this);
	}

	m_GameObjectPool.RemoveNameEntry(this);

	Uninitialize();
	m_GameObjectPool.Free(this);
}
//---------------------------------------------------------------------------------------------------
void GameObject::Uninitialize()
{
	HASSERT(m_pActor != nullptr, "Invalid PhysX::Actor!");

	Flags = GameObjectFlag_Destroyed;
	m_kTags.ResetFlag();
	m_bDynamicActor = false;
	m_bKinematic = false;

	HNCL(m_sTagString.clear());

	// destroy & remove physics actor
	UninitializeActor();

	// unbinds materials & mesh
	RenderObjectDX11::Uninitialize();
}
//---------------------------------------------------------------------------------------------------

void GameObject::UninitializeActor()
{
	if (m_pActor == nullptr)
		return;

	// destroy all shape componets, they need a valid actor
	std::vector<ShapeComponent*> Shapes = GetComponentsByType<ShapeComponent>();
	for (ShapeComponent* pShape : Shapes)
	{
		RemoveComponent(pShape);
	}

	physx::PxScene* pScene = m_pActor->getScene();
	if(pScene != nullptr)
	{
		Physics::SceneWriteLock lock;
		pScene->removeActor(*m_pActor);
	}

	m_pActor->release();
	m_pActor = nullptr;
}

//---------------------------------------------------------------------------------------------------
void GameObject::SetName(const std::string& _sName)
{
	if (m_sName != _sName)
	{
		m_GameObjectPool.SetName(m_sName, _sName, this);
		m_sName = _sName;
		if (m_pActor != nullptr)
		{
			m_pActor->setName(m_sName.c_str());
		}
	}
}
//---------------------------------------------------------------------------------------------------
void GameObject::ActivatePhysicsQuery(bool _bEnable)
{
	std::vector<Datastructures::ShapeComponent*> Shapes = GetComponentsByType<Datastructures::ShapeComponent>();
	for(Datastructures::ShapeComponent* pShape : Shapes)
	{
		if(pShape->Usage == Physics::ShapeUsageType_Query)
		{
			pShape->ActivateUsage(_bEnable);
		}
	}
}
//---------------------------------------------------------------------------------------------------

void GameObject::UpdateShapes()
{
	for (ShapeComponent* pShape : GetComponentsByType<ShapeComponent>())
	{
		pShape->Create(pShape->GetDescription());
	}
}
//---------------------------------------------------------------------------------------------------
void GameObject::SetRenderingScale(const float3& _vScale)
{
	m_vRenderingScale = _vScale;
#ifdef HNUCLEO
	UpdateShapes();
#endif
}
//---------------------------------------------------------------------------------------------------
void GameObject::SetTagsFromString(const std::vector<std::string>& _Tags)
{
	Flag<uint64_t> NewTags;

	for (const std::string& sTag : _Tags)
	{
		uint64_t uTag = 1ull << m_TagManager.GetAssociatedID(hlx::to_lower(sTag));

		NewTags.SetFlag(uTag);
		if (m_kTags.CheckFlag(uTag) == false)
		{
			m_kTags.SetFlag(uTag); // OR

			m_GameObjectPool.AddTag(uTag, this);
		}
	}

	// 1001101 prev tags
	// 0100101 new tags from _sTags
	// ======= OR
	// 1101101 prev and new tags
	// 0100101 new tags from _sTags
	// ======= XOR
	// 1001000 tags to be removed

	Flag<uint64_t> OldTags(m_kTags.GetFlag() ^ NewTags.GetFlag()); // XOR
	if (OldTags.GetFlag() != 0u)
	{
		m_GameObjectPool.RemoveTags(OldTags, this);
	}
}
//---------------------------------------------------------------------------------------------------
uint64_t GameObject::GetTagID(const std::string& _sTagString)
{
	return 1ull << m_TagManager.GetAssociatedID(hlx::to_lower(_sTagString));
}
//---------------------------------------------------------------------------------------------------
