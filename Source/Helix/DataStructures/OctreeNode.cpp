//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "OctreeNode.h"

#include <algorithm>

#include "Util\HelixDefines.h"

#include "Collision\CollisionResolution.h"

using namespace Helix;
using namespace Helix::Datastructures;
using namespace Helix::Collision;

//---------------------------------------------------------------------------------------------------
//constructor
OctreeNode::OctreeNode(OctreeNode* _pParentNode, OctreeNode* _pRootNode,  TCollisionMap& _CollisionMap, const XMFLOAT3& _vCenter, float _fRadius, int32_t _iRecursionDepth, BoxIndices _kBoxIndex, bool _bParallel) :
	m_kBoxIndex(_kBoxIndex),
	m_pParentNode(_pParentNode),
	m_pRootNode(_pRootNode == nullptr ? this : _pRootNode),
	m_CollisionMap(_CollisionMap),
	m_fRadius(_fRadius),
	m_iDepth(_iRecursionDepth),
	m_uCurEntityCount(0),
	m_uCurTotalEntityCount(0)
{
#ifdef _DEBUG
	if (m_pParentNode != nullptr)
	{
		m_sBoxPath = m_pParentNode->m_sBoxPath;
	}

	m_sBoxPath += std::string(BoxIndiceMapping[_kBoxIndex]) + " ";
#endif

	if (_bParallel && (m_bRecursiveTaskSpawn || m_pParentNode == nullptr))
	{
		m_pTaskGroup = new concurrency::task_group();
	}

	// create new bounding box
	m_AABB = BoundingBox(_vCenter, XMFLOAT3(m_fRadius, m_fRadius, m_fRadius));

	//if we prealloc the nodes and theres still some recursion depth left:
	if (--_iRecursionDepth > -1)
	{
		//split box
		_fRadius *= 0.5f;
		
		//create new child nodes
		for (uint32_t i = 0; i < 8; ++i)
		{
			m_pChildren[i] = new OctreeNode(this, m_pRootNode, m_CollisionMap, GetCenterOfNode(static_cast<BoxIndices> (i)), _fRadius, _iRecursionDepth, static_cast<BoxIndices>(i), _bParallel);
		}
	}
	else
	{
		m_pEntities = new TEntities();
		m_pEntityMutex = new std::mutex;

		// null all children if we don't preallocate
		for (uint32_t i = 0; i < 8; ++i)
		{
			m_pChildren[i] = nullptr;
		}
	}
}
//---------------------------------------------------------------------------------------------------
//destructor
OctreeNode::~OctreeNode()
{
	if (m_pTaskGroup != nullptr)
	{
		m_pTaskGroup->wait();
	}

	HSAFE_DELETE(m_pTaskGroup);
	HSAFE_DELETE(m_pEntities);
	HSAFE_DELETE(m_pEntityMutex);

	// free all da children =)
	for (uint32_t i = 0; i < 8; ++i)
	{
		HSAFE_DELETE(m_pChildren[i]);
	}
}
//---------------------------------------------------------------------------------------------------
//recrusivly insert a entity into the tree
bool OctreeNode::Insert(TplGameObject* _pEntity, bool _bIntersectsStep/*, bool _bAllowBottomUp*/)
{
	//update case
	//if (_bAllowBottomUp && m_pParentNode != nullptr)
	//{
	//	if (_pEntity->ContainedBy(m_AABB) == false)
	//	{
	//		//try to insert into parent node
	//		return m_pParentNode->Insert(_pEntity, false, true);
	//	}
	//}

	//root node case
	if (m_pParentNode == nullptr)
	{
		// entity is outside the octree
		if (_pEntity->Intersects(m_AABB) == false)
		{
			_pEntity->SetFlag(TplGameObjectFlag_Destroyed);
			HLOGD("GameObject %d destroyed (outside octree)", _pEntity->m_uID);
			return false;
		}
	}

	//we reached the lowest level of the octree
	if (m_iDepth == 0)
	{
		HASSERTD(m_pEntities != nullptr, "Invalid pointer");

		{
			std::lock_guard<std::mutex> Lock(*m_pEntityMutex);
			m_pEntities->push_back(_pEntity);
		}
		
		_pEntity->AddNode(this);
		
		//HLOGD("GameObject %d: added node %d \"%s\" of depth %d.", _pEntity->m_uID, _pEntity->m_Nodes.size(), m_sBoxPath.c_str(), m_iDepth);
		
		return true;
	}

	ObjectType Type = _pEntity->GetType();
	// Reduction
	if (_bIntersectsStep == false) //containment test
	{
		//get the index of the nearest child (highest change the entitiy is enclosed by the child AABB)
		uint32_t uNearestChildIndex = GetNearestChildIndex(_pEntity->m_vPosition);

		//Check if the entity is fully inside a sub node:
		//for (uint32_t i = 0; i < 8; ++i, ++uNearestChildIndex %= 8) // wait, why??
		//{
			//entity is fully enclosed by the child bounding box
			if (_pEntity->ContainedBy(m_pChildren[uNearestChildIndex]->m_AABB))
			{
				return m_pChildren[uNearestChildIndex]->Insert(_pEntity);
			}
		//}

		//the object is not fully enclosed by any node => object intersects with a face of the root node
		//run intersection test
		return Insert(_pEntity, true);
	}
	else // intersection test
	{
		//The entity did not fit into any sub node, but it might still intersect with them
		bool bRet = false;
		for (uint32_t i = 0; i < 8; ++i)
		{
			if (_pEntity->Intersects(m_pChildren[i]->m_AABB)) // TODO: change to overlaps
			{
				bRet = bRet || m_pChildren[i]->Insert(_pEntity, true);
			}
		}

		return bRet;
	}

	HASSERTD(false, "Insert failed, this should never happen!");
	return false;
}
//---------------------------------------------------------------------------------------------------
//remove an entity from the tree, this does not clear the node pointer of the entity!
void OctreeNode::Remove(uint32_t _uID)
{
	HASSERTD(m_pEntities != nullptr, "Invalid pointer");
	
	std::lock_guard<std::mutex> Lock(*m_pEntityMutex);

	TEntities::iterator RemoveIt = std::remove_if(m_pEntities->begin(), m_pEntities->end(), [&](const TplGameObject* pEntitiy){ return pEntitiy->GetID() == _uID; });
	if (RemoveIt != m_pEntities->end())
	{
		m_pEntities->erase(RemoveIt);
	}
}
//---------------------------------------------------------------------------------------------------
//returns the coordinates of the child node center
XMFLOAT3 OctreeNode::GetCenterOfNode(Helix::BoxIndices _ChildIndex) const
{
	float fRadius = m_fRadius * 0.5f;

	switch (_ChildIndex)
	{
	case BoxIndices_BottomFrontLeft:
		return XMFLOAT3(m_AABB.Center.x - fRadius, m_AABB.Center.y - fRadius, m_AABB.Center.z - fRadius);
	case BoxIndices_BottomFrontRight:
		return XMFLOAT3(m_AABB.Center.x + fRadius, m_AABB.Center.y - fRadius, m_AABB.Center.z - fRadius);
	case BoxIndices_TopFrontRight:
		return XMFLOAT3(m_AABB.Center.x + fRadius, m_AABB.Center.y + fRadius, m_AABB.Center.z - fRadius);
	case BoxIndices_TopFrontLeft:
		return XMFLOAT3(m_AABB.Center.x - fRadius, m_AABB.Center.y + fRadius, m_AABB.Center.z - fRadius);
	case BoxIndices_BottomBackLeft:
		return XMFLOAT3(m_AABB.Center.x - fRadius, m_AABB.Center.y - fRadius, m_AABB.Center.z + fRadius);
	case BoxIndices_BottomBackRight:
		return  XMFLOAT3(m_AABB.Center.x + fRadius, m_AABB.Center.y - fRadius, m_AABB.Center.z + fRadius);
	case BoxIndices_TopBackRight:
		return XMFLOAT3(m_AABB.Center.x + fRadius, m_AABB.Center.y + fRadius, m_AABB.Center.z + fRadius);
	case BoxIndices_TopBackLeft:
		return XMFLOAT3(m_AABB.Center.x - fRadius, m_AABB.Center.y + fRadius, m_AABB.Center.z + fRadius);
	default:
		return m_AABB.Center;
	}
}
//---------------------------------------------------------------------------------------------------
//this function should only be called on the RootNode of the Octree!
void OctreeNode::GatherCollisions()
{
	HASSERTD(m_pParentNode == nullptr, "this function should only be called on the RootNode of the Octree!");

	//clear previous collisions
	m_CollisionMap.clear();

	GatherCollisionsTask();
}
//---------------------------------------------------------------------------------------------------
uint32_t OctreeNode::GetNearestChildIndex(const XMFLOAT3 & _vPosition)
{
	XMVECTOR vEntityPos = XMLoadFloat3(&_vPosition);
	XMVECTOR vChildPos;

	float fMinDist = FLT_MAX;
	uint32_t iMinID = 0;
	float fDist;

	for (uint32_t i = 0; m_iDepth > 0 && i < 8; ++i)
	{
		if (m_pChildren[i] == nullptr)
		{
			//child node does not exist, calculate position
			vChildPos = XMLoadFloat3(&GetCenterOfNode(static_cast<BoxIndices>(i)));
		}
		else
		{
			//load position from child position
			vChildPos = XMLoadFloat3(&m_pChildren[i]->m_AABB.Center);
		}

		//find nearest child id
		fDist = Math::XMVector3DistanceF(vEntityPos, vChildPos);

		if (fDist < fMinDist)
		{
			fMinDist = fDist;
			iMinID = i;
		}
	}

	return iMinID;
}
//---------------------------------------------------------------------------------------------------
void OctreeNode::GatherCollisionsTask()
{	
	//Gather collision pairs in this (lowest) node
	if (m_pEntities != nullptr)
	{
		std::lock_guard<std::mutex> Lock(*m_pEntityMutex);

		for (TEntities::iterator it = m_pEntities->begin(), end = m_pEntities->end(); it != end; ++it)
		{
			TplGameObject* pGO1 = *it;

			if (pGO1->CheckFlag(TplGameObjectFlag_NoCollision))
			{
				continue;
			}

			//check for collisions with the other nodes in the list
			for (TEntities::iterator it2 = it + 1; it2 != end; ++it2)
			{
				TplGameObject* pGO2 = *it2;

				if (pGO2->CheckFlag(TplGameObjectFlag_NoCollision))
				{
					continue;
				}

				//both are static objects, no Collision to evaluate
				if (pGO1->CheckFlag(TplGameObjectFlag_Static) && pGO2->CheckFlag(TplGameObjectFlag_Static))
				{
					continue;
				}

				//check if collision already exists
				CollisionPair Pair;
				Pair.m_uID1 = pGO1->m_uID;
				Pair.m_uID2 = pGO2->m_uID;

				if (m_CollisionMap.count(Pair) == 0)
				{
					CollisionInfo Info;
					if (pGO1->Intersects(*pGO2, Info))
					{
						Info.m_pGO1 = pGO1;
						Info.m_pGO2 = pGO2;
						m_CollisionMap.insert(std::make_pair(Pair, Info));
					}
				}
			}
		}
	}
	
	//add subnodes to thetask list
	for (uint32_t i =  0; m_iDepth > 0 && i < 8; ++i)
	{
		OctreeNode* pNode = m_pChildren[i];
		uint32_t uNumEntities = pNode->GetTotalNumOfEntities();
		if (pNode->GetTotalNumOfEntities() > 1)
		{
			if (m_pTaskGroup != nullptr /*&& (m_bRecursiveTaskSpawn || m_pParentNode == nullptr)*/)
			{
				m_pTaskGroup->run([pNode]() {pNode->GatherCollisionsTask();});
				//m_pTaskGroup->run(std::bind(&OctreeNode::GatherCollisionsTask, pNode));
			}
			else
			{
				pNode->GatherCollisionsTask();
			}
		}
	}

	if (m_pTaskGroup != nullptr)
	{
		m_pTaskGroup->wait();
	}
}
//---------------------------------------------------------------------------------------------------
// This function should only be called once after all entities have been updated and before starting any Task!
uint32_t OctreeNode::GatherTotalNumOfEntities()
{
	uint32_t uEntities = 0;
	
	if (m_pEntities != nullptr)
	{
		std::lock_guard<std::mutex> Lock(*m_pEntityMutex);
		uEntities = static_cast<uint32_t>(m_pEntities->size());
	}
	
	m_uCurEntityCount = uEntities;

	for (uint32_t i = 0; m_iDepth > 0 && i < 8; ++i)
	{
		uEntities += m_pChildren[i]->GatherTotalNumOfEntities();
	}

	m_uCurTotalEntityCount = uEntities;

	return uEntities;
}
//---------------------------------------------------------------------------------------------------
// computes the recursion depth for the minimum object radius to be contained by a cell
int32_t OctreeNode::CalculateRecursionDepth(float _fCellRadius, float _fMinRadius)
{
	int32_t iDepth = 0;

	while (_fCellRadius > _fMinRadius)
	{
		++iDepth;
		_fCellRadius *= 0.5f;
	}

	return iDepth;
}
//---------------------------------------------------------------------------------------------------
// computes the number of octree nodes for the given depth
uint32_t OctreeNode::CalculateNumOfNodes(int32_t iRecursionDepth)
{
	uint32_t iNodes = 0;

	// the number of nodes in a octree with depth D is calculated by accumulating all levels:
	// D = 3 => 8^3 + 8^2 +8^1 + 8^0

	while (iRecursionDepth > 0)
	{
		// 8^x = 2 << ((x-1)*3) since 2^3 = 8
		// 8^2 = 8*8 = 64 = 2^6 = 1 << 6
		iNodes += (1 << (iRecursionDepth * 3));
		--iRecursionDepth;
	}

	// add 1 for 8^0 = 1 => root node
	return iNodes + 1;
}