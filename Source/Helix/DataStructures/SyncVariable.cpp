//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "SyncVariable.h"
#include "SyncDockManager.h"

using namespace Helix::Datastructures;

//---------------------------------------------------------------------------------------------------

SyncDock::SyncDock(
	const std::string& _sName,
	const std::vector<SyncGroup*>& _Groups,
	DockAnchor _kAnchor,
	bool _bOnTopTab,
	bool _bFloating,
	Helix::Flag<DockAnchor> _kAllowedAreas,
	WidgetLayout _kLayout) :
	sName(_sName),
	bOnTopTab(_bOnTopTab),
	bFloating(_bFloating),
	kAnchor(_kAnchor),
	kAllowedAreas(_kAllowedAreas),
	kLayout(_kLayout),
	Groups(_Groups)
{
	for (const SyncGroup* pGroup : Groups)
	{
		pGroup->pParent = this;
	}

	SyncDockManager::Instance()->Register(this);
}

SyncDock::~SyncDock()
{
}
//---------------------------------------------------------------------------------------------------

void SyncDock::Bind() const
{
	SyncDockManager::Instance()->Bind(this);
}
//---------------------------------------------------------------------------------------------------

void SyncDock::Unbind() const
{
	SyncDockManager::Instance()->Unbind(this);
}
//---------------------------------------------------------------------------------------------------
