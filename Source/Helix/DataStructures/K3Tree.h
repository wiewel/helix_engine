//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef K3TREE_H
#define K3TREE_H

#include "K3TreeNode.h"

namespace helix
{
	namespace datastructures
	{
		class K3Tree
		{
		public:
			K3Tree();
			~K3Tree();

			void Insert(TEntityVector::iterator _Begin, TEntityVector::iterator _End);

			void GatherCollisions(_In_ const TEntityVector& _Entities,_Out_ std::vector<collision::CollisionInfo>& _Collisions);
		private:
			K3TreeNode* m_pRootNode = nullptr;

		};
	}
}

#endif