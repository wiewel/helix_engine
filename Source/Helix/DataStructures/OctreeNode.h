//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef OCTREENODE_H
#define OCTREENODE_H

#include "Util\Flag.h"
#include "Collision\XMCollision.h"

#include <list>
#include <mutex>

#include "Util\HelixDefines.h"

#include "TplGameObject.h"

#include <concurrent_unordered_map.h>

namespace Helix
{
	namespace Datastructures
	{

		struct CollisionPair
		{
			uint32_t m_uID1;
			uint32_t m_uID2;
		};

		struct PairHash
		{
			std::size_t operator()(const CollisionPair& _Pair) const
			{
				std::size_t uHash = 14695981039346656037ull;;//+(_Pair.m_uID1 * 2166136261) ^ (_Pair.m_uID2 * 16777619);
				uHash ^= _Pair.m_uID1;
				uHash ^= static_cast<uint64_t>(_Pair.m_uID2) << 32;
				//ensure atleast 1 bist is set
				//size_t uHash = static_cast<uint64_t>((_Pair.m_uID2+1) * 16777619) << 32; // the hash should start with bits set
				//uHash |= (_Pair.m_uID1+1) * 16777619; // 8000000B is the next prime after 0x80000000 (half uint space -> "1st" bit set)
				return uHash;
			}
		};			

		struct PairEqual
		{
			bool operator()(const CollisionPair& _Pair1, const CollisionPair& _Pair2) const
			{
				return (_Pair1.m_uID1 == _Pair2.m_uID1 && _Pair1.m_uID2 == _Pair2.m_uID2) ||
					(_Pair1.m_uID1 == _Pair2.m_uID2 && _Pair1.m_uID2 == _Pair2.m_uID1);
			}
		};			
		
		typedef concurrency::concurrent_unordered_map<CollisionPair, Collision::CollisionInfo, PairHash, PairEqual> TCollisionMap;

		using namespace DirectX;

		// OctreeNode -------------------------------------------------------------------------------------
		class OctreeNode
		{
			//TODO: make this a sorted set (sorted by uID of the gameobjects) for faster delete and insert
			typedef std::vector<TplGameObject*> TEntities;
		public:
			HDEBUGNAME("OctreeNode");

			OctreeNode(OctreeNode* _pParentNode, OctreeNode* _pRootNode, TCollisionMap& _pCollisionMap, const XMFLOAT3& _vCenter, float _fRadius, int32_t _iRecursionDepth, BoxIndices _kBoxIndex, bool _bParallel);
			~OctreeNode();

			//insert a entity, if _bAllowBottomUp is set, the entity can be passed to the parent node if its not inside the current node (needed for update)
			//if _bIntersectsStep == true will skip the containment test and test for intersections instead (the function will set it to true
			//if the smallest containing node has been found and entity needs to be inserted in all intersecting sub nodes
			bool Insert(TplGameObject* _pEntity, bool _bIntersectsStep = false/*, bool _bAllowBottomUp = false*/);
			
			void Remove(uint32_t _uID);
			
			void GatherCollisions();

			template <typename BoundingVolume>
			void GatherInVolume(const BoundingVolume& _BV, TGameObjectMap& _Objects);

			// returns the index [0..7] of the nearest child of this node to the given position, or HUNDEFINED if there are no children (depth = 0)
			uint32_t GetNearestChildIndex(const XMFLOAT3& _vPosition);

			//This function should only be called once after all entities have been updated and before starting any Task!
			//gets number of entities in this node and all child nodes
			uint32_t GatherTotalNumOfEntities();

			//get number of entities stored in this node
			uint32_t GetNumOfEntities() const;

			//get number of entities stored in this node and all subnodes
			uint32_t GetTotalNumOfEntities() const;

			static int32_t CalculateRecursionDepth(float _fCellRadius, float _fMinRadius);
			static uint32_t CalculateNumOfNodes(int32_t iRecursionDepth);

			const BoundingBox& GetBoundingBox() const;
			XMFLOAT3 GetCenterOfNode(BoxIndices _ChildIndex) const;
			int32_t GetDepth() const;
			OctreeNode* const GetRootNode() const;

		private:
			//Collision gathering functions:
			//==============================

			void GatherCollisionsTask();


			//Gather in volumes functions:
			//============================

			template <typename BoundingVolume>
			void GatherInVolumeTask(const BoundingVolume& _BV, TGameObjectMap& _Objects);

		private:
			concurrency::task_group* m_pTaskGroup = nullptr;
			bool m_bRecursiveTaskSpawn = false;

			OctreeNode* const m_pParentNode;
			OctreeNode* const m_pRootNode;

			TCollisionMap& m_CollisionMap;

			const uint32_t m_kBoxIndex;

#ifdef _DEBUG
			std::string m_sBoxPath;
#endif

			OctreeNode* m_pChildren[BoxIndices_NumOfEdges];

			BoundingBox m_AABB;

			//Only defined in lowest octree level:
			std::mutex* m_pEntityMutex = nullptr;
			TEntities* m_pEntities = nullptr;

			const float m_fRadius;
			const int32_t m_iDepth;

			//number of entities stored in this node and all subnodes
			uint32_t m_uCurTotalEntityCount;
			//number of entities stored in this node
			uint32_t m_uCurEntityCount;

			std::mutex m_Mutex;
		};

		inline const BoundingBox& OctreeNode::GetBoundingBox() const { return m_AABB; }
		inline uint32_t OctreeNode::GetNumOfEntities() const { /*std::lock_guard<std::recursive_mutex> Lock(m_Mutex);*/ return m_uCurEntityCount; }
		inline uint32_t OctreeNode::GetTotalNumOfEntities() const { /*std::lock_guard<std::recursive_mutex> Lock(m_Mutex);*/ return m_uCurTotalEntityCount; }
		inline int32_t OctreeNode::GetDepth() const { return m_iDepth; }
		inline OctreeNode* const OctreeNode::GetRootNode() const { return m_pRootNode; }
	};
};

//include template functions here:
#include "OctreeNodeTemplate.h"

#endif //EOCTREENODE_H