//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "Component.h"
#include "Scene\ComponentScene.h"
#include "Display\DX11\RenderObjectDX11.h"
#include "BaseObject.h"
#include "hlx\src\Logger.h"

using namespace Helix::Datastructures;

//---------------------------------------------------------------------------------------------------

IComponent::IComponent(
	IBaseObject* _pParent,
	const ObjectType _kRequiredObjectType,
	TComponentCallbacks _kCallbacks) :
	m_pParent(_pParent),
	m_kRequiredObjectType(_kRequiredObjectType),
	m_kCallbacks(_kCallbacks),
	m_uID(ComponentID::Instance()->NextID())
{
	if (m_pParent != nullptr && (m_kRequiredObjectType & m_pParent->GetType()) == m_kRequiredObjectType)
	{
		Helix::Scene::ComponentScene::Instance()->RegisterComponent(this);
	}
	else
	{
		HFATAL("Object type %d does not match required type %u",
			m_pParent != nullptr ? m_pParent->GetType() : 0, m_kRequiredObjectType);
	}
}
//---------------------------------------------------------------------------------------------------
IComponent::IComponent(DefaultInitializerType) :
	m_pParent(nullptr),
	m_kRequiredObjectType(ObjectType_BaseObject),
	m_kCallbacks(ComponentCallback_None),
	m_uID(ComponentID::Instance()->NextID())
{
}
//---------------------------------------------------------------------------------------------------
IComponent::~IComponent()
{
	if (m_pParent != nullptr)
	{
		Helix::Scene::ComponentScene::Instance()->UnregisterComponent(this);
	}
}
//---------------------------------------------------------------------------------------------------
IComponent& IComponent::operator=(const IComponent& _Other)
{
	if (this == &_Other)
		return *this;

	HASSERT(GetTypeID() == _Other.GetTypeID(), "Component type mismatch!");
	m_kCallbacks = _Other.m_kCallbacks;

	return *this;
}
//---------------------------------------------------------------------------------------------------

void IComponent::Initialize()
{
	m_kCallbacks |= GetRequiredCallbacks();
}
//---------------------------------------------------------------------------------------------------

void IComponent::Destroy()
{
	IBaseObject* pParent = GetParent();
	if (pParent != nullptr)
	{
		bool bResult = pParent->RemoveComponent(this);
		HASSERT(bResult, "Removing Component failed!");
	}
}
//---------------------------------------------------------------------------------------------------
IComponent::operator bool() const
{
	return m_pParent != nullptr && (m_pParent->GetType() & m_kRequiredObjectType);
}
//---------------------------------------------------------------------------------------------------

void IComponent::AddCallback(const ComponentCallback& _kCallbacks)
{
	m_kCallbacks.SetFlag(_kCallbacks);
}
//---------------------------------------------------------------------------------------------------

void IComponent::RemoveCallback(const ComponentCallback& _kCallbacks)
{
	m_kCallbacks.ClearFlag(_kCallbacks);
	// ensure required callbacks
	m_kCallbacks |= GetRequiredCallbacks();
}

//---------------------------------------------------------------------------------------------------
