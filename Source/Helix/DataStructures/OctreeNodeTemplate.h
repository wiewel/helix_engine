//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "OctreeNode.h"

namespace Helix
{
	namespace Datastructures
	{

		//---------------------------------------------------------------------------------------------------
		template <typename BoundingVolume>
		inline void OctreeNode::GatherInVolume(const BoundingVolume & _BV, TGameObjectMap& _Objects)
		{
			HASSERTD(m_pParentNode == nullptr, "this function should only be called on the RootNode of the Octree!");

			//check if the BV is actually in the octree
			//TODO: make sure that Collision::Intersects / Overlap detect any interpenetration!
			if (m_uCurTotalEntityCount > 0 && Collision::Intersects(m_AABB, _BV)) 
			{
				GatherInVolumeTask<BoundingVolume>(_BV, _Objects);
			}

			//HLOGD("Visible Objects %d", _Objects.size());
		}

		//---------------------------------------------------------------------------------------------------
		template <typename BoundingVolume>
		void OctreeNode::GatherInVolumeTask(const BoundingVolume& _BV, TGameObjectMap& _Objects)
		{
			if (m_pEntities != nullptr)
			{
				for (TEntities::iterator it = m_pEntities->begin(), end = m_pEntities->end(); it != end; ++it)
				{
					if (_Objects.count((*it)->GetID()) == 0)
					{
						//TODO: make sure Intersects works correctly
						if ((*it)->Intersects(_BV))
						{
							_Objects.insert(std::make_pair((*it)->GetID(), *it));
						}
					}
				}
			}			

			//add subnodes to the task list
			for (uint32_t i = 0; m_iDepth > 0 && i < 8; ++i)
			{
				OctreeNode* pNode = m_pChildren[i];

				if (pNode->GetTotalNumOfEntities() > 0)
				{
					//check if the OctreeNode's AABB intersects with the BV
					if (Collision::Intersects(pNode->m_AABB, _BV))
					{
						if (m_pTaskGroup != nullptr)
						{
							//m_pTaskGroup->run(std::bind(&OctreeNode::GatherInVolumeTask<BoundingVolume>, pNode, _BV, _Objects));
							m_pTaskGroup->run([&, pNode]() {pNode->GatherInVolumeTask<BoundingVolume>(_BV, _Objects);});
						}
						else
						{
							pNode->GatherInVolumeTask<BoundingVolume>(_BV, _Objects);
						}
					}
				}
			}

			if (m_pTaskGroup != nullptr)
			{
				m_pTaskGroup->wait();
			}
		}

	}; // Datastructures
}; // Helix

