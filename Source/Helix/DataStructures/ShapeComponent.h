//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef SHAPECOMPONENT_H
#define SHAPECOMPONENT_H

#include "GameObjectComponent.h"
#include "Physics\SimulationEventCallback.h"
#include "Physics\CollisionFilter.h"

namespace Helix
{
	namespace Datastructures
	{
		class ShapeComponent : public IGameObjectComponent
		{
		private:
			ShapeComponent(GameObject* _pParent, const Physics::GeometryDesc& _Shape);

			COMPTYPENAME(ShapeComponent, GameObject, 'Shpe');
			COMPDESCONSTR(ShapeComponent, GameObject, IGameObjectComponent);
			COMPDEFCONSTR(ShapeComponent, GameObject, IGameObjectComponent);

		public:
			void OnDeserialize(const hlx::TextToken& _Token) final;
			// user needs to return true if data was serialized into the token
			bool OnSerialize(_Out_ hlx::TextToken& _Token) const final;

			// this will destroy the old shape
			bool Create(const Physics::GeometryDesc& _Shape);

			Datastructures::SyncComponent* GetSyncComponent() final;

			Physics::ShapeUsageType GetUsage() const;
			void SetUsage(Physics::ShapeUsageType _kUsage);

			Physics::GeometryType GetType() const;
			void SetType(Physics::GeometryType _kType);

			const Math::float3& GetScale() const;
			void SetScale(const Math::float3& _vScale);

			const Math::transform& GetLocalTransform() const;
			void SetLocalTransform(const Math::transform& _Transform);

			bool GetScaleWithParent() const;
			void SetScaleWithParent(const bool _bScaleWithParent);

			const Math::float3& GetHalfExtents() const;
			void SetHalfExtents(const Math::float3& _vHalfExtends);

			const float& GetRadius() const;
			void SetRadius(const float& _fRadius);

			const float& GetHalfHeight() const;
			void SetHalfHeight(const float& _fHalfHeight);

			const Physics::CollisionFilter& GetFilterData() const;
			void SetFilterData(const Physics::CollisionFilter& _FilterData);

			void RegisterCallback(Physics::TriggerEvent::Callback _Callback);
			void RemoveCallback(Physics::TriggerEvent::Callback& _Callback);
			void RegisterCallback(Physics::ContactEvent::Callback _Callback);
			void RemoveCallback(Physics::ContactEvent::Callback& _Callback);

			__declspec(property(get = GetUsage, put = SetUsage)) Physics::ShapeUsageType Usage;
			__declspec(property(get = GetType, put = SetType)) Physics::GeometryType Type;
			__declspec(property(get = GetScale, put = SetScale)) Math::float3 Scale;
			__declspec(property(get = GetScaleWithParent, put = SetScaleWithParent)) bool ScaleWithParent;
			__declspec(property(get = GetHalfExtents, put = SetHalfExtents)) Math::float3 HalfExtents;
			__declspec(property(get = GetRadius, put = SetRadius)) float Radius;
			__declspec(property(get = GetHalfHeight, put = SetHalfHeight)) float HalfHeight;
			__declspec(property(get = GetFilterData, put = SetFilterData)) Physics::CollisionFilter FilterData;

			const Physics::GeometryDesc& GetDescription() const;

			void ActivateUsage(bool _bActivate);

		private:
			void Destroy();
			void FromMesh();

		private:
			physx::PxShape* m_pShape = nullptr;
			Physics::GeometryDesc m_Desc;

			Physics::TriggerEvent m_OnTrigger;
			Physics::ContactEvent m_OnContact;

#ifdef HNUCLEO
			inline Math::uint4 GetFilterDataInternal() const { return{ m_Desc.Filter.word0, m_Desc.Filter.word1 ,m_Desc.Filter.word2 ,m_Desc.Filter.word3 }; }
			inline void SetFilterDataInternal(const Math::uint4& _FilterData) { m_Desc.Filter = { _FilterData.x, _FilterData.y, _FilterData.z, _FilterData.w }; Create(m_Desc); }

			inline const physx::PxVec3& GetLocalPosition() const { return m_Desc.LocalTransform.p; }
			inline void SetLocalPosition(const physx::PxVec3& _vPos) { m_Desc.LocalTransform.p = _vPos; Create(m_Desc); }

			inline const physx::PxQuat& GetLocalOrientation() const { return m_Desc.LocalTransform.q; }
			inline void SetLocalOrientation(const physx::PxQuat& _vRot) { m_Desc.LocalTransform.q = _vRot; Create(m_Desc); }

				Datastructures::SyncEnum m_TypeEnum = { {
				{ Physics::GeometryType_Box , "Box" },
				{ Physics::GeometryType_Sphere , "Sphere" },
				{ Physics::GeometryType_Capsule , "Capsule" },
				}, 0 };

			inline const Datastructures::SyncEnum& GetShapeType() const { return m_TypeEnum; }
			inline void SetShapeType(const Datastructures::SyncEnum& _Enum)
			{
				m_TypeEnum = _Enum;
				m_Desc.kType = static_cast<Physics::GeometryType>(_Enum.Get());
				Create(m_Desc);
			}
			//---------------------------------------------------------------------------------------------------------------------

			Datastructures::SyncEnum m_UsageEnum = { {
				{ Physics::ShapeUsageType_Query , "Query" },
				{ Physics::ShapeUsageType_Simulation , "Simulation" },
				{ Physics::ShapeUsageType_Trigger , "Trigger" },
				}, 0 };

			inline const Datastructures::SyncEnum& GetUsageType() const { return m_UsageEnum; }
			inline void SetUsageType(const Datastructures::SyncEnum& _Enum)
			{
				m_UsageEnum = _Enum;
				m_Desc.kUsage = static_cast<Physics::ShapeUsageType>(_Enum.Get());
				Create(m_Desc);
			}

			SYNOBJ(TypeEnum, ShapeComponent, Datastructures::SyncEnum, GetShapeType, SetShapeType) m_SyncShapeType = { *this, "Type" };
			SYNOBJ(UsageEnum, ShapeComponent, Datastructures::SyncEnum, GetUsageType, SetUsageType) m_SyncUsageType = { *this, "Usage" };
			SYNOBJ(LocalPos, ShapeComponent, physx::PxVec3, GetLocalPosition, SetLocalPosition) m_SyncLocalPos = { *this, "Position" };
			SYNOBJ(LocalRot, ShapeComponent, physx::PxQuat, GetLocalOrientation, SetLocalOrientation) m_SyncLocalRot = { *this, "Orientation" };
			SYNOBJ(HalfExtSyn, ShapeComponent, Math::float3, GetHalfExtents, SetHalfExtents) m_SyncHalfExtents = { *this, "Half Extents" };
			SYNOBJ(ScaleSyn, ShapeComponent, Math::float3, GetScale, SetScale) m_SyncScale= { *this, "Scale" };
			SYNOBJ(ScaleWithParentSyn, ShapeComponent, bool, GetScaleWithParent, SetScaleWithParent) m_SyncScaleWithParent = { *this, "Scale with Parent" };
			SYNOBJ(FilterData, ShapeComponent, Math::uint4, GetFilterDataInternal, SetFilterDataInternal) m_SyncFilterData = { *this, "Filter" };

			SYNOBJ(RaiusSyn, ShapeComponent, float, GetRadius, SetRadius) m_SyncRadius = { *this, "Sphere/Capsule Radius" };
			SYNOBJ(HalfHeightSyn, ShapeComponent, float, GetHalfHeight, SetHalfHeight) m_SyncHalfHeight = { *this, "Capsule Half-Height" };
			
			Datastructures::SyncVariable<uint32_t> m_ConvexMeshCRC = { m_Desc.ConvexMesh.uCRC32, "ConvexHull", Datastructures::WidgetLayout_Horizontal, true };
			Datastructures::SyncButton m_FromMeshButton = { "From Mesh", [&]() {FromMesh(); } };

			Datastructures::SyncComponent m_ShapeGroup = { this, "Collider",
			{ &m_SyncShapeType, &m_SyncUsageType, &m_SyncScale, &m_SyncScaleWithParent,
				&m_SyncHalfExtents, &m_SyncRadius, &m_SyncHalfHeight,
				&m_SyncLocalPos, &m_SyncLocalRot, &m_ConvexMeshCRC, &m_FromMeshButton, &m_SyncFilterData } };
#endif

		};

		inline Datastructures::SyncComponent* ShapeComponent::GetSyncComponent()
		{
#ifdef HNUCLEO
			return &m_ShapeGroup;
#else
			return nullptr;
#endif
		}

		inline Physics::ShapeUsageType ShapeComponent::GetUsage() const { return m_Desc.kUsage; }
		inline void ShapeComponent::SetUsage(Physics::ShapeUsageType _kUsage) { m_Desc.kUsage = _kUsage; Create(m_Desc); }

		inline Physics::GeometryType ShapeComponent::GetType() const { return m_Desc.kType; }
		inline void ShapeComponent::SetType(Physics::GeometryType _kType) { m_Desc.kType = _kType; Create(m_Desc); }

		inline const Math::float3& ShapeComponent::GetScale() const{return m_Desc.vScale;}
		inline void ShapeComponent::SetScale(const Math::float3& _vScale){m_Desc.vScale = _vScale; Create(m_Desc);}

		inline const Math::transform& ShapeComponent::GetLocalTransform() const{ return m_Desc.LocalTransform;	}
		inline void ShapeComponent::SetLocalTransform(const Math::transform& _Transform){m_Desc.LocalTransform = _Transform; Create(m_Desc);}

		inline bool ShapeComponent::GetScaleWithParent() const{	return m_Desc.bScaleWithParent;	}
		inline void ShapeComponent::SetScaleWithParent(const bool _bScaleWithParent){m_Desc.bScaleWithParent = _bScaleWithParent; Create(m_Desc);}

		inline const Math::float3& ShapeComponent::GetHalfExtents() const{	return m_Desc.Box.vHalfExtents;	}
		inline void ShapeComponent::SetHalfExtents(const Math::float3& _vHalfExtends){m_Desc.Box.vHalfExtents = _vHalfExtends;  Create(m_Desc);}

		inline const float& ShapeComponent::GetRadius() const{return m_Desc.Sphere.fRadius;}
		inline void ShapeComponent::SetRadius(const float& _fRadius)
		{
			m_Desc.Capsule.fRadius = _fRadius;
			m_Desc.Sphere.fRadius = _fRadius;
			Create(m_Desc);
		}

		inline const float& ShapeComponent::GetHalfHeight() const{return m_Desc.Capsule.fHalfHeight;}
		inline void ShapeComponent::SetHalfHeight(const float& _fHalfHeight){m_Desc.Capsule.fHalfHeight = _fHalfHeight; Create(m_Desc);}

		inline const Physics::CollisionFilter& ShapeComponent::GetFilterData() const{return m_Desc.Filter;}
		inline void ShapeComponent::SetFilterData(const Physics::CollisionFilter& _FilterData) { m_Desc.Filter = _FilterData; Create(m_Desc); }

		inline const Physics::GeometryDesc& ShapeComponent::GetDescription() const{	return m_Desc;}

	} // Datastructures
} // Helix

#endif // !SHAPECOMPONENT_H
