//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef TPLMEMPOOL_H
#define TPLMEMPOOL_H

#include <tuple>
#include <vector>
#include <set>

#include "Util\Logger.h"

namespace Helix
{
	namespace Datastructures
	{
		template <typename Parent, typename ... Ts>
		class TplMemPool
		{
			//MemBlock - Storage container
			//---------------------------------------------------------------------------------------------------

			//Forward declaration:
			template<typename... Types>
			struct MemBlock;

			//Specialization for two or more types
			template <typename First, typename ...Rest>
			struct MemBlock<First, Rest...>
			{
				MemBlock() : m_Parent(m_Data)
				{
				}

				//MemBlock(const MemBlock&) = delete;

				First m_Parent;
				std::tuple<Rest...> m_Data;
			};

			//Spepialization for one type
			template <typename First>
			struct MemBlock<First>
			{
				//MemBlock(){}
				//MemBlock(const MemBlock&) = delete;

				First m_Parent;
			};

		public:

#pragma region Iterators
			// iterator class
			//---------------------------------------------------------------------------------------------------
			class iterator : public std::iterator<std::random_access_iterator_tag, Parent>
			{
			public:

#ifdef _DEBUG
			#define update_ptr() { m_uIndex < m_pUsage->size() ? m_pObject = m_pUsage->at(m_uIndex) : m_pObject = nullptr; }
#else
			#define update_ptr()
#endif

				iterator() {}
				iterator(size_t _uIndex, std::vector<Parent*> * _pUsage) :					
					m_uIndex(_uIndex),
					m_pUsage(_pUsage)
				{
					update_ptr();
				}

				iterator(const iterator& other) : 
					m_uIndex(other.m_uIndex),
					m_pUsage(other.m_pUsage)
				{
					update_ptr();
				}

				const iterator& operator=(const iterator& other)
				{
					m_uIndex = other.m_uIndex;
					m_pUsage = other.m_pUsage;
					update_ptr();
					return other;
				}

				iterator& operator++() { ++m_uIndex; update_ptr(); return *this; } // ++prefix
				iterator  operator++(int) { return iterator(m_uIndex++, m_pUsage); } // postfix++
				iterator& operator--() { --m_uIndex; update_ptr(); return *this; } // --prefix
				iterator  operator--(int) { return iterator(m_uIndex--, m_pUsage); } // postfix--

				void     operator+=(const std::size_t& n) { m_uIndex += n; update_ptr(); }
				void     operator+=(const iterator& other) { m_uIndex += other.m_uIndex; update_ptr(); }
				iterator operator+ (const std::size_t& n) const { return iterator(m_uIndex + n, m_pUsage); }
				iterator operator+ (const iterator& other) const { return iterator(m_uIndex + other.m_uIndex, m_pUsage); }

				void        operator-=(const std::size_t& n) { m_uIndex -= n; update_ptr(); }
				void        operator-=(const iterator& other) { m_uIndex -= other.m_uIndex; update_ptr(); }
				iterator    operator- (const std::size_t& n) const { return iterator(m_uIndex - n, m_pUsage);}
				std::size_t operator- (const iterator& other) const { return m_uIndex - other.m_uIndex; }

				bool operator< (const iterator& other) const { return m_uIndex < other.m_uIndex; }
				bool operator<=(const iterator& other) const { return m_uIndex <= other.m_uIndex; }
				bool operator> (const iterator& other) const { return m_uIndex > other.m_uIndex; }
				bool operator>=(const iterator& other) const { return m_uIndex >= other.m_uIndex; }
				bool operator==(const iterator& other) const { return m_uIndex == other.m_uIndex; }
				bool operator!=(const iterator& other) const { return m_uIndex != other.m_uIndex; }

				Parent& operator[](const int& n) { return *m_pUsage->at(m_uIndex + n); }
				Parent& operator*() { return *m_pUsage->at(m_uIndex); }
				Parent* operator->() { return m_pUsage->at(m_uIndex); }

				size_t index() const { return m_uIndex; }
			private:
				size_t m_uIndex = 0;
				HDBG(Parent* m_pObject = nullptr);
				std::vector<Parent*> * m_pUsage = nullptr;
			};

			// iterator class
			//---------------------------------------------------------------------------------------------------
			class const_iterator : public std::iterator<std::random_access_iterator_tag, Parent>
			{
			public:
				const_iterator() {}
				const_iterator(size_t _uIndex, const std::vector<Parent*> * _pUsage) :
					m_uIndex(_uIndex),
					m_pUsage(_pUsage)
				{
				}

				const_iterator(const const_iterator& other) :
					m_uIndex(other.m_uIndex),
					m_pUsage(other.m_pUsage)
				{
				}

				const const_iterator& operator=(const const_iterator& other)
				{
					m_uIndex = other.m_uIndex;
					m_pUsage = other.m_pUsage;
					return other;
				}

				const_iterator& operator++() { ++m_uIndex; return *this; } // ++prefix
				const_iterator  operator++(int) { return const_iterator(m_uIndex++, m_pUsage); } // postfix++
				const_iterator& operator--() { --m_uIndex; return *this; } // --prefix
				const_iterator  operator--(int) { return const_iterator(m_uIndex--, m_pUsage); } // postfix--

				void     operator+=(const std::size_t& n) { m_uIndex += n; }
				void     operator+=(const const_iterator& other) { m_uIndex += other.m_uIndex; }
				const_iterator operator+ (const std::size_t& n) const { return const_iterator(m_uIndex + n, m_pUsage); }
				const_iterator operator+ (const const_iterator& other) const { return const_iterator(m_uIndex + other.m_uIndex, m_pUsage); }

				void        operator-=(const std::size_t& n) { m_uIndex -= n; }
				void        operator-=(const const_iterator& other) { m_uIndex -= other.m_uIndex; }
				const_iterator operator- (const std::size_t& n) const { return const_iterator(m_uIndex - n, m_pUsage); }
				std::size_t operator- (const const_iterator& other) const { return m_uIndex - other.m_uIndex; }

				bool operator< (const const_iterator& other) const { return m_uIndex < other.m_uIndex; }
				bool operator<=(const const_iterator& other) const { return m_uIndex <= other.m_uIndex; }
				bool operator> (const const_iterator& other) const { return m_uIndex > other.m_uIndex; }
				bool operator>=(const const_iterator& other) const { return m_uIndex >= other.m_uIndex; }
				bool operator==(const const_iterator& other) const { return m_uIndex == other.m_uIndex; }
				bool operator!=(const const_iterator& other) const { return m_uIndex != other.m_uIndex; }

				const Parent& operator[](const int& n) const { return *m_pUsage->at(n); }
				const Parent& operator*() const { return *m_pUsage->at(m_uIndex); }
				const Parent* operator->() const { return m_pUsage->at(m_uIndex); }

				size_t index() const { return m_uIndex; }

			private:
				size_t m_uIndex;
				Parent* m_pObject = nullptr;
				const std::vector<Parent*> * m_pUsage;
			};

#pragma endregion Iterators			


#pragma region ContainerFunctions
			inline iterator begin() { return iterator(0, &m_Usage); }
			inline iterator end() { return iterator(m_uFirstFree, &m_Usage); }

			inline const_iterator cbegin() const { return const_iterator(0, &m_Usage); }
			inline const_iterator cend() const { return const_iterator(m_uFirstFree, &m_Usage); }

			inline Parent& operator[](const size_t& n) const { HASSERTD(n < m_uFirstFree, "Index out of bounds!"); return *m_Usage.at(n); }
			inline size_t size() const { return m_uFirstFree; }
#pragma endregion
			//TplMemPool - Tuple memory pool implementation using sorted set 
			//---------------------------------------------------------------------------------------------------

			TplMemPool(uint32_t _uElements = 0)
			{
				if (_uElements > 0)
				{
					Resize(_uElements);
				}
			}

			//ATTENTION!
			//This function will break all previous references to objects held by this container if _uElements > m_Blocks.capacity()!
			//Only call this function if you are totally aware of the consequences!
			//---------------------------------------------------------------------------------------------------
			void Resize(uint32_t _uElements)
			{
				m_uFirstFree = 0;
				m_Blocks.resize(_uElements);
				m_Usage.resize(_uElements);

				//Initialization of references
				for (uint32_t i = 0; i < _uElements; ++i)
				{				
					m_Usage.at(i) = &m_Blocks[i].m_Parent;
				}
			}

			//---------------------------------------------------------------------------------------------------
			Parent* Alloc()
			{
				//no free slots
				if (m_uFirstFree == m_Usage.size())
				{
					return nullptr;
				}

				// return frist free slot and increment
				return m_Usage.at(m_uFirstFree++);
			}

			//---------------------------------------------------------------------------------------------------
			void Free(Parent* pMem)
			{
				for (uint32_t i = 0; i < m_uFirstFree; ++i)
				{
					if (m_Usage.at(i) == pMem)
					{
						//write last used into free slot
						m_Usage.at(i) = m_Usage.at(--m_uFirstFree);
						//write free mem to new first free slot
						m_Usage.at(m_uFirstFree) = pMem;
						break;
					}
				}

				HASSERTD(false, "Shit");
			}
			//---------------------------------------------------------------------------------------------------

			iterator Free(const iterator& _Itr)
			{
				size_t uIndex = _Itr.index();
				//save free slot
				Parent* pParent = m_Usage.at(uIndex);
				//write last used into free slot
				m_Usage.at(uIndex) = m_Usage.at(--m_uFirstFree);
				//write free mem to new first free slot
				m_Usage.at(m_uFirstFree) = pParent;

				if (uIndex + 1 < m_uFirstFree)
				{
					return _Itr + 1;
				}
				else
				{
					return end();
				}
			}
			
		private:
			uint32_t m_uFirstFree = 0;
			std::vector< MemBlock<Parent, Ts...> > m_Blocks;
			// 0 - m_uFirstFree => used used slots
			// m_uFirstFree - end => free slots
			std::vector<Parent*> m_Usage;

		};
	};
};

#endif
