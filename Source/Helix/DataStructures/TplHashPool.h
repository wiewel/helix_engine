//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef TPLHASHPOOL_H
#define TPLHASHPOOL_H

#include "hlx\src\Logger.h"
#include <concurrent_unordered_map.h>
#include <vector>
#include "Util\Functional.h"
#include "Display\DX11\ViewDefinesDX11.h"
#include "Async\SpinLock.h"

namespace Helix
{
	namespace Datastructures
	{
		template <typename Hash, typename Desc, typename Ref>
		class TplRefHashPool
		{
		public:
			typedef typename Ref RefType;
			typedef typename Hash HashType;
			typedef typename Desc DescType;

			TplRefHashPool() {}

			struct RefCounter
			{
				RefCounter(Ref* _pPtr)
				{
					m_pPtr = _pPtr;;
					m_iRefCount = 1;
				}

				~RefCounter()
				{
					HASSERTD(m_pPtr == nullptr, "Reference has not been deleted properly!");
					m_pPtr = nullptr; // Reference must be deleted in Free/Release function
					m_iRefCount = 0;
				}

				Async::SpinLock m_Lock;
				Ref* m_pPtr = nullptr;
				int32_t m_iRefCount = 0;
			};

			//The deriving class must free all data in the hashmap!
			virtual ~TplRefHashPool()
			{
				Clear();
			}

			inline void Clear(bool _bAssert = true)
			{
				for (auto it = m_HashMap.begin(); it != m_HashMap.end(); ++it)
				{
					if (_bAssert && it->second != nullptr && it->second->m_iRefCount > 0)
					{
						AssertRef <Has_Release<Ref>::value>(it->second->m_pPtr, it->second->m_iRefCount);
					}

					Free<Has_Release<Ref>::value>(it->second->m_pPtr);

					HSAFE_DELETE(it->second);
				}

				m_HashMap.clear();
			}

			virtual Hash HashFunction(const Desc& _Desc) = 0;

			// specialized template for calling correct free function
			template <bool Release>
			uint32_t Free(Ref*& _pRef);

			template<>
			inline uint32_t Free<true>(Ref*& _pRef)
			{
				if (_pRef != nullptr) 
				{
					uint32_t uRefCount = _pRef->Release();
					_pRef = nullptr;
					return uRefCount;
				}

				return 0;
			}

			template<>
			inline uint32_t Free<false>(Ref*& _pRef)
			{
				HSAFE_DELETE(_pRef); 
				return 0;
			}

			template<bool GetPrivateData>
			inline void AssertRef(Ref* _pRef, uint32_t uInternalRefCount);

			template<>
			inline void AssertRef<false>(Ref*_pRef, uint32_t uInternalRefCount)
			{
				HERRORD("Reference has not been released! [%u references alive]", uInternalRefCount);
			}

			template<>
			inline void AssertRef<true>(Ref* _pRef, uint32_t uInternalRefCount)
			{
				const char* pName = HDXGETDEBUGNAME(_pRef);
				HERRORD("%s has not been released! [%u references alive]", pName != nullptr ? CSTR(pName) : S("Undefined reference") , uInternalRefCount);
			}

			// Decrement RefCount and delete Reference if zero
			inline void Destroy(const Hash& _Key)
			{
				auto Itr = m_HashMap.find(_Key);
				if (Itr != m_HashMap.end())
				{
					RefCounter* pRC = Itr->second;

					bool bDelete = false;
					// internal refcount for dx11 interfaces
					uint32_t uRefCount = 0;

					Ref* pRef = pRC->m_pPtr;

					//ThreadSafe delete
					{
						Async::ScopedSpinLock lock(pRC->m_Lock);
						pRC->m_iRefCount--;
						if (bDelete = (pRC->m_iRefCount == 0))
						{
							uRefCount = Free<Has_Release<Ref>::value>(pRC->m_pPtr);
							m_HashMap.unsafe_erase(Itr);
						}
					}

					// exit scoped lock, RefCount
					if (bDelete)
					{

#ifdef _DEBUG
						if (uRefCount > 0)
						{
							AssertRef <Has_Release<Ref>::value>(pRef, uRefCount);
						}
#endif
						
						HSAFE_DELETE(pRC);
					}
				}
			}

			// Aquire new reference, increments RefCounter
			inline Ref* Get(const Hash& _Key)
			{
				auto Itr = m_HashMap.find(_Key);
				if (Itr != m_HashMap.end())
				{
					RefCounter* pRC = Itr->second;
					Async::ScopedSpinLock lock(pRC->m_Lock);
					pRC->m_iRefCount++;
					return pRC->m_pPtr;
				}

				return nullptr;
			}

			// Create one new Reference based on Desc
			inline Ref* Get(_In_ const Desc& _Desc, _Out_ Hash& _Hash, bool _bGetDefaultIfFailed = true)
			{
				Hash Key = HashFunction(_Desc);

				_Hash = Key;

				Ref* pRet = Get(Key);

				if (pRet != nullptr)
				{
					return pRet;
				}
				else
				{
					pRet = CreateFromDesc(_Desc, Key);

					if (pRet != nullptr)
					{
						m_HashMap.insert(std::make_pair(Key, new RefCounter(pRet)));
					}
					else if(_bGetDefaultIfFailed)
					{
						_Hash = GetDefaultKey();
						return Get(_Hash);
					}

					return pRet;
				}

				return nullptr;
			}

			// Create new References based on Desc & VariantKeys => New reference is saved under several keys, returned in _Hashes
			inline Ref* Get(_In_ const Desc& _Desc, _Out_ std::vector<Hash>& _VariantKeys)
			{
				Hash Key = HashFunction(_Desc);

				_Hash = Key;

				Ref* pRet = Get(Key);

				if (pRet != nullptr)
				{
					return pRet;
				}
				else
				{
					pRet = CreateFromDesc(_Desc, Key, _VariantKeys);

					if (pRet != nullptr)
					{
						RefCounter* pRC = new RefCounter(pRet);

						HASSERTD(_VariantKeys.empty() == false, "You need to specify VariantKeys!");

						for (const Hash& VarKey : _VariantKeys)
						{
							m_HashMap.insert(std::make_pair(Key ^ VarKey, pRC));
						}						
					}

					return pRet;
				}

				return nullptr;
			}

			// Adds a new Variant stored under Key XOR Variant => _VariantKey, returns false if _Key not found.
			inline bool AddVariant(_In_ const Hash& _Key, _In_ const Hash& _Variant, _Out_ Hash& _VariantKey)
			{
				Ref* pRet = Get(Key);

				if (pRet == nullptr)
				{
					return false;
				}

				_VariantKey = Key ^ _Variant;

				m_HashMap.insert(std::make_pair(_VariantKey, pRC));

				return true;
			}

			// return pointer to a default reference when CreateFromDesc failed
			virtual Hash GetDefaultKey() { return Hash(); }
		protected:
			//must be overwritten
			virtual Ref* CreateFromDesc(const Desc& _Desc, const Hash& _Key) = 0;

			// can be overwritten
			virtual Ref* CreateFromDesc(const Desc& _Desc, const Hash& _Key, _Out_ std::vector<Hash>& _VariantKeys)
			{
				_VariantKeys.push_back(0); // Key XOR 0 = Key
				return CreateFromDesc(_Desc, _Key);
			}

		protected:
			concurrency::concurrent_unordered_map<Hash, RefCounter*> m_HashMap;
		};

		//---------------------------------------------------------------------------------------------------
		template <typename HashPool>
		class TplHashPoolReference
		{
		public:
			typedef typename HashPool::RefType RefType;
			typedef typename HashPool::HashType HashType;
			typedef typename HashPool::DescType DescType;

			// default constructor
			inline constexpr TplHashPoolReference() noexcept {}
			inline constexpr TplHashPoolReference(std::nullptr_t) noexcept {}
			inline constexpr TplHashPoolReference(const DefaultInitializerType&)
			{
				m_Hash = HashPool::Instance()->GetDefaultKey();
				m_pReference = HashPool::Instance()->Get(m_Hash);
			}

			inline static TplHashPoolReference DefaultReference()
			{
				return TplHashPoolReference(DefaultInit);
			}
			
			// Hash constructor
			inline TplHashPoolReference(const HashType& _Hash) : m_Hash(_Hash)
			{
				m_pReference = HashPool::Instance()->Get(m_Hash);
			}

			// Desc constructor
			inline TplHashPoolReference(const DescType& _Desc)
			{
				m_pReference = HashPool::Instance()->Get(_Desc, m_Hash);
			}

			// Copy constructor
			// Increments the ref count, does not copy any data!
			inline TplHashPoolReference(const TplHashPoolReference& _Other)
			{
				// copy hash
				m_Hash = _Other.m_Hash;

				// increment refcount for copied ref
				m_pReference = HashPool::Instance()->Get(m_Hash);
			}

			// Move constructor
			// Moves reference to newly constructed object, leaving the original reference in a empty default state
			inline TplHashPoolReference(TplHashPoolReference&& _Other)
			{
				// copy hash
				m_Hash = _Other.m_Hash;

				// copy reference
				m_pReference = _Other.m_pReference;

				// reset original object to empty default state
				_Other.m_Hash = HashType(0);
				_Other.m_pReference = nullptr;
			}

			// destructor
			virtual ~TplHashPoolReference()
			{
				if (m_Hash != HashType(0))
				{
					HashPool::Instance()->Destroy(m_Hash);
				}

				m_pReference = nullptr;
				m_Hash = HashType(0);
			}

			// Assignment operator
			// Increments the ref count, does not copy any data!
			inline TplHashPoolReference& operator=(const TplHashPoolReference& _Other)
			{
				if (this != &_Other && m_Hash != _Other.m_Hash)
				{
					// decrement refcount for previous ref
					if (m_Hash != HashType(0))
					{
						HashPool::Instance()->Destroy(m_Hash);
					}

					// copy hash
					m_Hash = _Other.m_Hash;

					// increment refcount for copied ref
					m_pReference = HashPool::Instance()->Get(m_Hash);
				}

				return *this;
			}

			// Move assignment operator
			// Moves reference to this object (releasing old reference), leaving the original reference in a empty default state
			inline TplHashPoolReference& operator=(TplHashPoolReference&& _Other)
			{
				// don't move self
				if(this != &_Other && m_Hash != _Other.m_Hash)
				{
					// decrement refcount for previous ref
					if (m_Hash != HashType(0))
					{
						HashPool::Instance()->Destroy(m_Hash);
					}

					// copy hash
					m_Hash = _Other.m_Hash;

					// copy reference
					m_pReference = _Other.m_pReference;

					// reset original object to empty default state
					_Other.m_Hash = HashType(0);
					_Other.m_pReference = nullptr;
				}			

				return *this;
			}

			// nullptr assign
			inline TplHashPoolReference& operator=(std::nullptr_t)
			{
				if (m_Hash != HashType(0))
				{
					HashPool::Instance()->Destroy(m_Hash);
				}

				m_pReference = nullptr;
				m_Hash = HashType(0);

				return *this;
			}

			// comparison operators
			inline bool operator==(std::nullptr_t) const { return m_pReference == nullptr; }
			inline bool operator!=(std::nullptr_t) const { return m_pReference != nullptr; }
			inline explicit operator bool() const { return m_pReference != nullptr; }
			inline bool operator==(const TplHashPoolReference& _Ref) const { return m_pReference == _Ref.m_pReference; }
			inline bool operator!=(const TplHashPoolReference& _Ref) const { return m_pReference != _Ref.m_pReference; }

			inline const RefType* GetReferenceConst() const { return m_pReference; }
			inline RefType* GetReference() const { return m_pReference; }
			inline const HashType& GetKey() const { return m_Hash; }

			inline bool IsDefaultReference() const
			{
				return m_Hash == HashPool::Instance()->GetDefaultKey();
			}

		protected:
			RefType* m_pReference = nullptr;
			HashType m_Hash = 0;
		};

	} // Datastructres
} // Helix

#endif