//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "K3Tree.h"
#include "StdAfxH.h"

using namespace helix::datastructures;
using namespace helix::collision;

K3Tree::K3Tree()
{

}

//---------------------------------------------------------------------------------------------------

K3Tree::~K3Tree()
{
	HSAFE_DELETE(m_pRootNode);
}

//---------------------------------------------------------------------------------------------------

void K3Tree::Insert(TEntityVector::iterator _Begin, TEntityVector::iterator _End)
{
	HSAFE_DELETE(m_pRootNode);

	m_pRootNode = new K3TreeNode(_Begin, _End, 0);
}

//---------------------------------------------------------------------------------------------------

void K3Tree::GatherCollisions(_In_ const TEntityVector& _Entities, _Out_ std::vector<CollisionInfo>& _Collisions)
{
	if (m_pRootNode == nullptr)
		return;

	HFOREACH_CONST(it, end, _Entities)	
		m_pRootNode->GatherCollisions(*it, _Collisions);
	HFOREACH_CONST_END
}