//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H

#include "hlx\src\StandardDefines.h"
#include "Util\Flag.h"
#include "hlx\src\Logger.h"
#include "GameObjectPool.h"
#include "Math\MathTypes.h"
#include "Display\DX11\RenderObjectDx11.h"
#include "GameObjectDescription.h"
#include "Physics\GeometryTypes.h"
#include "PxRigidActor.h"
#include "PxRigidDynamic.h"
#include "extensions\PxRigidBodyExt.h"
#include "Math\MathTypes.h"
#include "Math\MathFunctions.h"
#include "Physics\SceneLock.h"
#include "DataStructures\SyncVariable.h"
#include "Physics\QueryFilter.h"
#include "Util\UniqueAssociationID.h"

namespace Helix
{
	namespace Datastructures
	{
		// specifies which GO data members have been modified by setters 
		Flag32E(GOSyncFlag)
		{
			GOSyncFlag_None = 0,
			GOSyncFlag_Transform = 1 << 0,
			GOSyncFlag_LinearVelocity = 1 << 1,
			GOSyncFlag_Mass = 1 << 2,
			GOSyncFlag_Kinematic = 1 << 3,
			GOSyncFlag_WakeUp = 1 << 4,
			GOSyncFlag_DisableSimulation = 1 << 5
		};

		using TGOSyncFlags = Flag<GOSyncFlag>;

		class GameObject : public Display::RenderObjectDX11
		{
			friend class GameObjectPool;

		public:
			void Initialize(const GameObjectDesc& _Desc) final;
			Datastructures::GameObjectDesc CreateDescription() const final;

			void CreatePhysicsActorStatic(const Math::transform& _Transform);
			void CreatePhysicsActorDynamic(const Math::transform& _Transform, float _fMass, bool _bKinematic = false, bool _bCCD = false);

			bool AddShape(const Physics::GeometryDesc& _Shape);

			// add to physics scene
			void AddToScene();

			void Destroy();

			void SetName(const std::string& _sName) final;

			void ActivatePhysicsQuery(bool _bEnable);

			void UpdateShapes();

			void SetRenderingScale(const Math::float3& _vScale) final;

			void SetTagsFromString(const std::vector<std::string>& _Tags);

			static uint64_t GetTagID(const std::string& _sTagString);

		private:
			GameObject(GameObjectPool& _GameObjectPool, uint32_t _uID);
			virtual ~GameObject();

			void Uninitialize() final;
			void UninitializeActor();

		private:
			GameObjectPool& m_GameObjectPool;

			// don't ever change this bool manually, the pool will handle use after free protection!
			bool m_bAccessible = true;
			uint32_t m_uUsageIndex = 0u;

			bool m_bDynamicActor = false;

			physx::PxRigidActor* m_pActor = nullptr;

			mutable Async::SpinLock m_LockObj;
			bool m_bLocking = true;
			TGOSyncFlags m_kUpdatedComponents;

			// dont change these manually!
			mutable Flag<uint64_t> m_kGatheredCameras;
			mutable Flag<uint64_t> m_kGatheredPasses;

			// Components, Transform derived from RenderObject
			TGameObjectFlags m_Flags;
			Flag<uint64_t> m_kTags;
			Math::float3 m_vLinearVelocity;
			float m_fMass = 1.f;
			bool m_bSleeping = false;
			bool m_bKinematic = false;
			bool m_bDisableSimuation = false;
		public:
			const uint32_t ID;

			// ALL GETTERS/SETTERS NEED TO CHECK m_bAccessible:  HASSERTD(m_bAccessible, "GameObject is inaccessible"); 
			//---------------------------------------------------------------------------------------------------
			// PROPERTYS
			//---------------------------------------------------------------------------------------------------

#define HACCESSIBLE() HASSERTD(m_bAccessible, "GameObject %s [%u] is inaccessible",CSTR(m_sName), ID)
//#define HLOCKED() HASSERTD(m_bAccessible && m_GameObjectPool.IsLocked() == false, "GameObject %s [%u] is inaccessible", CSTR(m_sName), ID)
#define HDYNAMIC() HASSERTD(m_bDynamicActor, "GameObject %s [%u] is not a dynamic actor!", CSTR(m_sName), ID)

			__declspec(property(get = GetFlags, put = SetFlags)) GameObjectFlag Flags;
			__declspec(property(get = GetMass, put = SetMass)) float Mass;
			__declspec(property(get = GetLinearVelocity, put = SetLinearVelocity)) Math::float3 LinearVelocity;
			__declspec(property(get = GetKinematic, put = SetKinematic)) bool IsKinematic;
			__declspec(property(get = GetDisableSimulation, put = SetDisableSimulation)) bool DisableSimulation;
			__declspec(property(get = GetDynamic)) bool IsDynamic;

			inline const TGOSyncFlags& GetComponentFlags() { return m_kUpdatedComponents; }

			inline physx::PxRigidActor* GetActor() const
			{
				HACCESSIBLE();
				return m_pActor;
			}
			inline bool GetDynamic() const { HACCESSIBLE(); return m_bDynamicActor; }

			// ############### ACTOR LINEAR VELOCITY ###############
			inline Math::float3 GetLinearVelocity() const
			{
				HACCESSIBLE(); 
				Async::ScopedSpinLock lock(m_LockObj, m_bLocking);
				return m_vLinearVelocity;
			}
			inline void SetLinearVelocity(const Math::float3& _vVelocity)
			{
				HACCESSIBLE();
				Async::ScopedSpinLock lock(m_LockObj, m_bLocking);
				m_vLinearVelocity = _vVelocity;
				m_kUpdatedComponents |= GOSyncFlag_LinearVelocity;
			}

			// ############### ACTOR WAKEUP / SLEEPING ###############
			inline void WakeUp()
			{
				HACCESSIBLE();
				Async::ScopedSpinLock lock(m_LockObj, m_bLocking); // wake up GO in next sim step
				m_kUpdatedComponents |= GOSyncFlag_WakeUp;
				m_bSleeping = false;
			}
			inline bool IsSleeping()
			{
				HACCESSIBLE();
				Async::ScopedSpinLock lock(m_LockObj, m_bLocking);
				return m_bSleeping;
			}

			// ############### GO FLAGS ###############
			inline const GameObjectFlag& GetFlags() const { HACCESSIBLE(); Async::ScopedSpinLock lock(m_LockObj, m_bLocking); return m_Flags.GetFlag(); }
			inline void SetFlags(const GameObjectFlag& _uFlag) { HACCESSIBLE(); Async::ScopedSpinLock lock(m_LockObj, m_bLocking); m_Flags.SetFlag(_uFlag); }
			inline void ClearFlags(const GameObjectFlag& _uFlag) { HACCESSIBLE(); Async::ScopedSpinLock lock(m_LockObj, m_bLocking); m_Flags.ClearFlag(_uFlag); }
			inline bool CheckFlags(const GameObjectFlag& _uFlag) const { HACCESSIBLE(); Async::ScopedSpinLock lock(m_LockObj, m_bLocking); return m_Flags.CheckFlag(_uFlag); }

			// ############### ACTOR IS KINEMATIC ###############
			inline bool GetKinematic() const
			{
				HACCESSIBLE();
				Async::ScopedSpinLock lock(m_LockObj, m_bLocking);
				return m_bKinematic;
			}
			inline void SetKinematic(bool _bIsKinematic)
			{
				HACCESSIBLE();
				Async::ScopedSpinLock lock(m_LockObj, m_bLocking);
				m_kUpdatedComponents |= GOSyncFlag_Kinematic;
				if (_bIsKinematic)
					m_Flags.SetFlag(GameObjectFlag_Kinematic);
				else
					m_Flags.ClearFlag(GameObjectFlag_Kinematic);

				m_bKinematic = _bIsKinematic;		
			}

			// ############### ACTOR IS SIMULATED ###############
			inline bool GetDisableSimulation() const
			{
				HACCESSIBLE();
				Async::ScopedSpinLock lock(m_LockObj, m_bLocking);
				return m_bDisableSimuation;
			}
			inline void SetDisableSimulation(bool _bDisableSim)
			{
				HACCESSIBLE();
				Async::ScopedSpinLock lock(m_LockObj, m_bLocking);
				m_kUpdatedComponents |= GOSyncFlag_DisableSimulation;
				m_bDisableSimuation = _bDisableSim;
			}

			// ############### ACTOR TRANSFORM ###############
			inline Math::transform GetTransform() const final
			{
				HACCESSIBLE();
				Async::ScopedSpinLock lock(m_LockObj, m_bLocking);
				HASSERTD(m_Transform.isSane(), "The transform returned by GetTransform is invalid!");
				return m_Transform;
			}

			inline void SetTransform(const Math::transform& _Transform) final
			{ 
				HACCESSIBLE();
				Async::ScopedSpinLock lock(m_LockObj, m_bLocking);
				m_kUpdatedComponents |= GOSyncFlag_Transform;
				m_Transform = _Transform;
			}

			// ############### ACTOR MASS ###############
			inline float GetMass() const
			{
				HACCESSIBLE();
				Async::ScopedSpinLock lock(m_LockObj, m_bLocking);
				return m_fMass;
			}
			inline void SetMass(const float& _fMass)
			{
				HACCESSIBLE();
				Async::ScopedSpinLock lock(m_LockObj, m_bLocking);
				m_kUpdatedComponents |= GOSyncFlag_Mass;
				m_fMass = _fMass;
			}
			//---------------------------------------------------------------------------------------------------
			inline void GetObjectToWorldMatrix(_Out_ Math::float4x4& _mObjectToWorld) const final
			{
				Math::GetTransformationMatrix(GetTransform(), m_vRenderingScale, _mObjectToWorld);
			}
			//---------------------------------------------------------------------------------------------------
			inline void GetObjectToWorldMatrix(_Out_ Math::float4x4& _mObjectToWorld, _Out_ Math::float4x4& _mNormal) const final
			{
				Math::GetTransformationAndNormalMatrix(GetTransform(), m_vRenderingScale, _mObjectToWorld, _mNormal);
			}
			//---------------------------------------------------------------------------------------------------

			inline bool IsAccessible() const { return m_bAccessible; }
			inline bool IsLocked() const { return m_bAccessible == false || m_GameObjectPool.IsLocked();}

			// checks whether this objects has been gathered by camera _uCameraID for pass _uPassID, sets both IDs in any case
			inline bool CheckAndSetGatherFlags(uint64_t _uCameraID, uint64_t _uPassID) const
			{
				HACCESSIBLE();
				Async::ScopedSpinLock lock(m_LockObj, m_bLocking);

				bool bGathered = m_kGatheredCameras.CheckFlag(_uCameraID) && m_kGatheredPasses.CheckFlag(_uPassID);
				m_kGatheredCameras.SetFlag(_uCameraID);
				m_kGatheredPasses.SetFlag(_uPassID);
				return bGathered;
			}

			// resets gather flags before gathering for the next frame
			inline void ResetGatherFlags() const
			{
				HACCESSIBLE();
				Async::ScopedSpinLock lock(m_LockObj, m_bLocking); 
				m_kGatheredCameras.ResetFlag(); m_kGatheredPasses.ResetFlag();
			}

			private:
				static UniqueAssociationID<std::string, uint64_t> m_TagManager;
				// NUCLEO SECTION
#ifdef HNUCLEO
				std::string m_sTagString;

				inline const std::string& GetTagString() const { return m_sTagString; };
				inline void SetTagString(const std::string& _sTagString) { if (m_sTagString != _sTagString) { SetTagsFromString(hlx::split(_sTagString, " ")); }	m_sTagString = _sTagString; }

				inline bool GetVisible() const { return m_Flags.CheckFlag(GameObjectFlag_Invisible) == false; }
				inline void SetVisible(const bool _bVisible){ if (_bVisible) m_Flags.ClearFlag(GameObjectFlag_Invisible); else m_Flags.SetFlag(GameObjectFlag_Invisible);	}

				bool m_bExportKinematicAsStatic = true;
				SyncVariable<bool> m_SyncKinematicAsStatic = { m_bExportKinematicAsStatic, "Export Kinematic As Static" };
				SYNOBJ(Mass, GameObject, float, GetMass, SetMass) m_SyncMass = { *this, "Mass" };
				SYNOBJ(Tag, GameObject, std::string, GetTagString, SetTagString) m_SyncTag = { *this, "Tags" };
				SYNOBJ(Kinematic, GameObject, bool, GetKinematic, SetKinematic) m_SyncKinematic = { *this, "Kinematic" };
				SYNOBJ(Visible, GameObject, bool, GetVisible, SetVisible) m_SyncVisible = { *this, "Visible" };
				SYNOBJ(Position, GameObject, Math::float3, GetPosition, SetPosition) m_SyncPosition = { *this, "Position" };
				SYNOBJ(Orientation, GameObject, Math::quaternion, GetOrientation, SetOrientation) m_SyncOrientation = { *this, "Orientation" };
#endif
		};		
}; // namespace datastructures
}; // namespace helix

#endif // GAMEOBJECT_H