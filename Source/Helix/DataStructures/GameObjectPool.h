//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef GAMEOBJECTPOOL_H
#define GAMEOBJECTPOOL_H

#include "Math\XMMath.h"
#include <mutex>
#include <future>
#include "Async\SpinLock.h"
#include "Util\Flag.h"
#include <atomic>
#include "Math\MathTypes.h"
#include <concurrent_vector.h>
#include <unordered_map>

#include "hlx\src\Logger.h"
#include "hlx\src\Singleton.h"

namespace Helix
{
	namespace Datastructures
	{
		class GameObject;
		using TNameMap = std::unordered_multimap<std::string, GameObject*>;
		using TTagMap = std::unordered_multimap<uint64_t, GameObject*>;

		class GameObjectPool : public hlx::Singleton<GameObjectPool>
		{
			friend class GameObject;
			HDEBUGNAME("GameObjectPool");
		public:
			GameObjectPool();
			~GameObjectPool();

			// TODO: make private & friend class accessors
			bool Initialize(uint32_t _uGameObjectCount);

			// only call this when the engine is shutdown
			void Destroy();

			// Allocates a gameobject when the pool is not busy
			std::future<GameObject*> Create();

			// Allocates a bunch of gameobjects when the pool is not busy
			std::future<std::vector<GameObject*>> Create(uint32_t _uCount);

			bool IsLocked();

			uint32_t ActiveGameObjectCount() const;

			// TODO: visible to scene
			std::vector<GameObject*>::const_iterator cbegin() const;
			std::vector<GameObject*>::const_iterator cend() const;

			// synchronize state with simulation
			void Synchronize();

			void GetObjectsByName(const std::string& _sName, _Out_ std::vector<GameObject*>& _Objects);
			GameObject* GetObjectByName(const std::string& _sName);

			// get objects associated with sTag
			void GetGameObjectsByTag(const std::string& _sTag, _Out_ std::vector<GameObject*>& _Objects);

			// get objects associated with uTag
			void GetGameObjectsByTag(const uint64_t _uTag, _Out_ std::vector<GameObject*>& _Objects);

			// get objects associated to any Tag in kTags
			void GetGameObjectsByTags(const Flag<uint64_t> _kTags, _Out_ std::vector<GameObject*>& _Objects);

		private:
			GameObject* Alloc();
			void RemoveNameEntry(GameObject* _pMem);
			void Free(GameObject* _pMem);

			void SetName(const std::string& _sOldName, const std::string& _sNewName, GameObject* _pObject);
			
			// associats the gameobject to ONE tag (only one bit set!)
			void AddTag(const uint64_t _uTag, GameObject* _pObject);

			// removes the gameobject from several associated Tags
			void RemoveTags(const Flag<uint64_t>& _kTags, GameObject* _pObject);

		private:
			uint32_t m_uFirstFree = 0;
			// [0 - m_uFirstFree[ => used slots
			// [m_uFirstFree - end[ => free slots
			std::vector<GameObject*> m_Usage;

			std::mutex m_Mutex;
			std::atomic_bool m_bIsLocked;

			Async::SpinLock m_NameMapLock;
			Async::SpinLock m_TagMapLock;

			TNameMap m_NamedObjects;
			TTagMap m_TaggedObjects;
		};

		inline std::vector<GameObject*>::const_iterator GameObjectPool::cbegin() const { return m_Usage.cbegin(); }
		inline std::vector<GameObject*>::const_iterator GameObjectPool::cend() const { return  m_Usage.cbegin() + m_uFirstFree;}

		inline bool GameObjectPool::IsLocked() { return m_bIsLocked.load(); }
		inline uint32_t GameObjectPool::ActiveGameObjectCount() const { return m_uFirstFree; }
	} // Datastructures
} // Helix


#endif // GAMEOBJECTPOOL_H