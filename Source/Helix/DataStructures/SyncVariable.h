//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef SYNCVARIABLE_H
#define SYNCVARIABLE_H

#include <string>
#include <vector>
#include "hlx\src\Logger.h"
#include "Math\MathTypes.h"
#include "Util\Flag.h"
#include "Util\Functional.h"
#include "Component.h"
#include "Async\SpinLock.h"

namespace Helix
{
	namespace Datastructures
	{
		enum DockAnchor : uint32_t
		{
			DockAnchor_Left = 0x1,
			DockAnchor_Right = 0x2,
			DockAnchor_Top = 0x4,
			DockAnchor_Bottom = 0x8,
			DockAnchor_Any = 0xf
		};

		enum WidgetLayout : uint32_t
		{
			WidgetLayout_Horizontal = 0u,
			WidgetLayout_Vertical,
			WidgetLayout_Form,
			WidgetLayout_Grid
		};

		enum VarType : uint32_t
		{
			VarType_Int32 = 0u,
			VarType_UInt32,
			VarType_Int64,
			VarType_UInt64,
			VarType_Float,
			VarType_Double,
			VarType_Float2,
			VarType_Float3,
			VarType_Float4,
			VarType_Int2,
			VarType_Int3,
			VarType_Int4,
			VarType_UInt2,
			VarType_UInt3,
			VarType_UInt4,
			VarType_Quaternion,
			VarType_String,
			VarType_Bool,
			VarType_Enum,
			VarType_Button,
			VarType_List,
			VarType_Chart,
			VarType_Unknown
		};
	
		struct SyncEnumValue
		{
			uint64_t uValue;
			const char* sName;
		};

		struct SyncEnum
		{
			SyncEnum(const std::vector<SyncEnumValue>& _Values = {}, uint32_t _uIndex = 0u):
				Values(_Values), uSelectedIndex(_uIndex){}

			std::vector<SyncEnumValue> Values;
			uint32_t uSelectedIndex = 0u;

			inline uint64_t Get() const { return Values[uSelectedIndex].uValue; }
		};

		struct SyncVariableBase
		{
			SyncVariableBase(
				const VarType _kType,
				const std::string& _sName,
				const WidgetLayout _kLayout = WidgetLayout_Horizontal,
				const bool _bReadOnly = false) :
				kType(_kType), sName(_sName), kLayout(_kLayout), bReadOnly(_bReadOnly) {}

			virtual ~SyncVariableBase() {}

			const VarType kType;
			const std::string sName;
			const WidgetLayout kLayout;
			const bool bReadOnly;
			
			template <class T>
			inline const T* GetAs() { return reinterpret_cast<const T*>(Get()); };
			virtual const void* Get() = 0;
			virtual void Set(const void* _pData) = 0;
		};

		struct SyncGroup
		{
			friend struct SyncDock; // forward decl
			SyncGroup(
				const std::string& _sName,
				const std::vector<SyncVariableBase*>& _Variables = {},
				const WidgetLayout _kLayout = WidgetLayout_Form) :
				sName(_sName), kLayout(_kLayout), Variables(_Variables){}

			virtual ~SyncGroup() {};

			inline void AddVariable(SyncVariableBase* _pVariable) { std::lock_guard<std::recursive_mutex> lock(m_Lock); bUpdated = true; Variables.push_back(_pVariable); }
			inline void RemoveVariable(SyncVariableBase* _pVariable)
			{
				std::lock_guard<std::recursive_mutex> lock(m_Lock);
				std::vector<SyncVariableBase*>::iterator it = std::remove(Variables.begin(), Variables.end(), _pVariable);
				if (it != Variables.end())
				{
					Variables.erase(it);
					bUpdated = true;
				}
			}

			// Lock() needs to be acquired before calling this function!
			inline const std::vector<SyncVariableBase*>& GetVariables() const { return Variables; }
			//inline std::vector<SyncVariableBase*>& GetVariables() { return Variables; }

			inline void ClearVariables() { std::lock_guard<std::recursive_mutex> lock(m_Lock);  Variables.resize(0); bUpdated = true; }
						
			inline bool GetUpdated() const { std::lock_guard<std::recursive_mutex> lock(m_Lock); return bUpdated; }
			inline void ResetUpdated() const { std::lock_guard<std::recursive_mutex> lock(m_Lock); bUpdated = false; }

			inline std::recursive_mutex& GetLockObject() const { return m_Lock; }

		public:
			const std::string sName;
			const WidgetLayout kLayout;
		protected:
			mutable SyncDock* pParent = nullptr;
		private:
			std::vector<SyncVariableBase*> Variables;
			mutable bool bUpdated = false;
			mutable std::recursive_mutex m_Lock;
		};

		struct SyncDock
		{
			friend class SyncDockManager;

			SyncDock(
				const std::string& _sName,
				const std::vector<SyncGroup*>& _Groups = {},
				DockAnchor _kAnchor = DockAnchor_Right,
				bool _bOnTopTab = false,
				bool _bFloating = false,
				Flag<DockAnchor> _kAllowedAreas = DockAnchor_Any,
				WidgetLayout _kLayout = WidgetLayout_Vertical);

			virtual ~SyncDock();

			inline void AddGroup(SyncGroup* _pGroup) { std::lock_guard<std::recursive_mutex> lock(m_Lock); if (_pGroup != nullptr) { _pGroup->pParent = this; Groups.push_back(_pGroup); } bUpdated = true; }
			
			// Lock() needs to be acquired before calling this function
			inline const std::vector<SyncGroup*>&  GetGroups() const { return Groups; }
			//inline std::vector<SyncGroup*>& GetGroups() { return Groups; }

			inline void RemoveGroup(SyncGroup* _pGroup)
			{
				std::lock_guard<std::recursive_mutex> lock(m_Lock);
				std::vector<SyncGroup*>::iterator it = std::remove(Groups.begin(), Groups.end(), _pGroup);
				if (it != Groups.end())
				{
					Groups.erase(it);
					bUpdated = true;
				}
			}

			inline void ClearGroups() { std::lock_guard<std::recursive_mutex> lock(m_Lock); Groups.resize(0); bUpdated = true;}

			void Bind() const;
			void Unbind() const;

			inline bool IsRegisterTemplate() const { std::lock_guard<std::recursive_mutex> lock(m_Lock); return bRegisterTemplate; }

			inline bool GetUpdated() const
			{
				std::lock_guard<std::recursive_mutex> lock(m_Lock); 
				return bUpdated || std::any_of(Groups.cbegin(), Groups.cend(), [](const SyncGroup* pGroup) {return pGroup->GetUpdated(); });
			}

			inline void ResetUpdated() const { std::lock_guard<std::recursive_mutex> lock(m_Lock); bUpdated = false; }

			//inline void Lock() const { m_Lock.Lock(); }
			//inline void Unlock() const { m_Lock.Unlock(); }
			inline std::recursive_mutex& GetLockObject() const { return m_Lock; }

		private:
			inline void SetIsRegisterTemplate(bool _bRegisterTemplate) const { std::lock_guard<std::recursive_mutex> lock(m_Lock); bRegisterTemplate = _bRegisterTemplate; }

		public:
			const bool bOnTopTab;
			const bool bFloating;
			const std::string sName;
			const DockAnchor kAnchor;
			const Flag<DockAnchor> kAllowedAreas;
			const WidgetLayout kLayout;

		private:
			std::vector<SyncGroup*> Groups;
			mutable bool bRegisterTemplate = false;
			mutable bool bUpdated = false;
			mutable std::recursive_mutex m_Lock;
		};

		// in nucleo:
		// Gather Groups on init, create Widget if it doesnt exist yet, in draw event get new variable values

		template <class T>
		inline VarType GetVarType() { return VarType_Unknown; }
		template <>
		inline VarType GetVarType<bool>() { return VarType_Bool; }
		template <>
		inline VarType GetVarType<int32_t>() { return VarType_Int32; }
		template <>
		inline VarType GetVarType<uint32_t>() { return VarType_UInt32; }
		template <>
		inline VarType GetVarType<int64_t>() { return VarType_Int64; }
		template <>
		inline VarType GetVarType<uint64_t>() { return VarType_UInt64; }
		template <>
		inline VarType GetVarType<float>() { return VarType_Float; }
		template <>
		inline VarType GetVarType<double>() { return VarType_Double; }

		template <>
		inline VarType GetVarType<Math::float2>() { return VarType_Float2; }
		//template <>
		//inline VarType GetVarType<physx::PxVec2>() { return VarType_Float2; }
		template <>
		inline VarType GetVarType<Math::float3>() { return VarType_Float3; }
		template <>
		inline VarType GetVarType<physx::PxVec3>() { return VarType_Float3; }
		template <>
		inline VarType GetVarType<Math::float4>() { return VarType_Float4; }
		template <>
		inline VarType GetVarType<physx::PxVec4>() { return VarType_Float4; }

		template <>
		inline VarType GetVarType<Math::int2>() { return VarType_Int2; }
		template <>
		inline VarType GetVarType<Math::int3>() { return VarType_Int3; }
		template <>
		inline VarType GetVarType<Math::int4>() { return VarType_Int4; }

		template <>
		inline VarType GetVarType<Math::uint2>() { return VarType_UInt2; }
		template <>
		inline VarType GetVarType<Math::uint3>() { return VarType_UInt3; }
		template <>
		inline VarType GetVarType<Math::uint4>() { return VarType_UInt4; }

		template <>
		inline VarType GetVarType<physx::PxQuat>() { return VarType_Quaternion; }
		template <>
		inline VarType GetVarType<Math::quaternion>() { return VarType_Quaternion; }
		template <>
		inline VarType GetVarType<std::string>() { return VarType_String; }
		template <>
		inline VarType GetVarType<SyncEnum>() { return VarType_Enum; }

		template <class T>
		struct SyncVariable : SyncVariableBase
		{
		public:
			inline SyncVariable(
				T& _Var,
				const std::string& _sName,
				const WidgetLayout _kLayout = WidgetLayout_Horizontal,
				const bool _bReadOnly = false) :
				SyncVariableBase(GetVarType<T>(), _sName, _kLayout, _bReadOnly),
				m_Var(_Var)
			{
				static_assert(!std::is_same<T, Math::transform>::value, "Use SyncTransformGroup for Math::transform!");
			}
			virtual ~SyncVariable()	{}

			inline const void* Get() final { return &m_Var; };
			inline void Set(const void* _pData) final { if(_pData != nullptr && bReadOnly == false) m_Var = *static_cast<const T*>(_pData); };
		private:
			T& m_Var;
		};

		template <class T>
		struct SyncVariableConst : SyncVariableBase
		{
		public:
			inline SyncVariableConst(
				const T& _Var,
				const std::string& _sName,
				const WidgetLayout _kLayout = WidgetLayout_Horizontal) :
				SyncVariableBase(GetVarType<T>(), _sName, _kLayout, true),
				m_Var(_Var)
			{
				static_assert(!std::is_same<T, Math::transform>::value, "Use SyncTransformGroup for Math::transform!");
			}
			virtual ~SyncVariableConst() {}

			inline const void* Get() final { return &m_Var; };
			inline void Set(const void* _pData) final {};
		private:
			const T& m_Var;
		};

		template <class T>
		struct SyncVariableInstance : public SyncVariableBase
		{
			inline SyncVariableInstance(
				const std::string& _sName,
				const T& _Value = {},
				const WidgetLayout _kLayout = WidgetLayout_Horizontal,
				const bool _bReadOnly = false) :
				SyncVariableBase(GetVarType<T>(), _sName, _kLayout, _bReadOnly),
				Var(_Value)
			{
			}

			virtual ~SyncVariableInstance()	{}

			inline const void* Get() final { return &Var; };
			inline void Set(const void* _pData) final { if (_pData != nullptr && bReadOnly == false) Var = *static_cast<const T*>(_pData); };

			inline SyncVariableInstance& operator=(const T& _Val) { m_Var = _Val; return *this; }
			inline operator T() { return m_Var; }

			T Var = {};
		};

		struct SyncChart : public SyncVariableBase
		{
			inline SyncChart(
				float& _fVar,
				const std::string& _sName) :
				SyncVariableBase(VarType_Chart, _sName, WidgetLayout_Horizontal, true),
				m_fVar(_fVar) {}

			virtual ~SyncChart() {};

			inline const void* Get() final { return &m_fVar; };
			inline void Set(const void* _pData) final { };

		private:
			float& m_fVar;
		};

		struct SyncButton : public SyncVariableBase
		{
			inline SyncButton(
				const std::string& _sName,
				const std::function<void()>& _Func,
				const WidgetLayout _kLayout = WidgetLayout_Horizontal) :
				SyncVariableBase(VarType_Button, _sName, _kLayout),
				m_Func(_Func) {}

			inline const void* Get() final { return nullptr; };
			inline void Set(const void* _pData) final { m_Func(); };
		private:
			std::function<void()> m_Func;
		};

		struct SyncComponent : public SyncGroup
		{
			SyncComponent(
				IComponent* _pComponent,
				const std::string& _sName,
				const std::vector<SyncVariableBase*>& _Variables = {}) :
				SyncGroup(_sName, _Variables, WidgetLayout_Form),
				pComponent(_pComponent)
			{
				AddVariable(&RemoveButton);
			}

			virtual ~SyncComponent() {};

		private:
			IComponent* const pComponent;
			SyncButton RemoveButton = { "Remove Component", [&]() {pParent->RemoveGroup(this); pComponent->Destroy(); } };
		};

		struct SyncListBase : public SyncVariableBase
		{
			SyncListBase(const std::string& _sName) : SyncVariableBase(VarType_List, _sName, WidgetLayout_Vertical){}
			virtual ~SyncListBase() {};

			inline const void* Get() final { return nullptr; };
			inline void Set(const void* _pData) final {};

			virtual std::string GetText(int _iIndex) = 0;
			virtual void SelectItem(int _iIndex) = 0;
			virtual int GetSize() const = 0;
		};

		template <class T>
		struct SyncList : public SyncListBase
		{
			using GetTextFunc = std::function<std::string(int)>;
			using ItemSelectedFunc = std::function<void(int)>;

			SyncList(
				const std::string& _sName,
				const std::vector<T>* _pList,
				const GetTextFunc& _GetText,
				const ItemSelectedFunc& _Select = nullptr) :
				SyncListBase(_sName),
				m_pList(_pList), m_GetTextFunc(_GetText), m_SelectedFunc(_Select){}
			virtual ~SyncList() {};

			inline std::string GetText(int _iIndex) final
			{
				if (m_GetTextFunc != nullptr)
				{
					return m_GetTextFunc(_iIndex);
				}
				else
				{
					return{};
				}
			}

			inline void SelectItem(int _iIndex) { if (m_SelectedFunc != nullptr)m_SelectedFunc(_iIndex); }

			int GetSize() const final { return static_cast<int>(m_pList->size()); }

			void SetList(const std::vector<T>* _pList) { m_pList = _pList; }
		private:
			const std::vector<T>* m_pList;

			std::function<std::string(int)> m_GetTextFunc = nullptr;
			std::function<void(int)> m_SelectedFunc = nullptr;
		};

		// todo: check if set function with args exists => exec eif exists
#ifndef SYNOBJ
#define SYNOBJ(_Name, _ObjType, _VarType, _GetFunc, _SetFunc) \
		struct SyncVariableObject##_Name : Helix::Datastructures::SyncVariableBase \
		{ \
			EXEC_IF_EXISTS(_SetFunc)\
			inline SyncVariableObject##_Name(_ObjType& _Obj, const std::string& _sName, \
			const Helix::Datastructures::WidgetLayout _kLayout = Helix::Datastructures::WidgetLayout_Horizontal, const bool _bReadOnly = false) : \
			Helix::Datastructures::SyncVariableBase(Helix::Datastructures::GetVarType<_VarType>(), _sName, _kLayout, _bReadOnly), m_Obj(_Obj) {}\
			inline const void* Get() final { m_Var = m_Obj._GetFunc(); return &m_Var; };\
			inline void Set(const void* _pData) final { m_Var = *static_cast<const _VarType*>(_pData); if(bReadOnly == false) m_Obj._SetFunc(m_Var); };\
		private:\
			_VarType m_Var = {};\
			_ObjType& m_Obj;\
		}
#endif

	} // Datastructures
} // Helix

#endif // !SYNCVARIABLE_H
