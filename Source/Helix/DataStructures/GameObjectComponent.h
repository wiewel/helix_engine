//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef GAMEOBJECTCOMPONENT_H
#define GAMEOBJECTCOMPONENT_H

#include "Component.h"
#include "GameObject.h"

namespace Helix
{
	namespace Datastructures
	{
		class IGameObjectComponent : public IComponent
		{
			COMPTYPENAME(IGameObjectComponent, GameObject, 'Game')

		protected:
			IGameObjectComponent(GameObject* _pParent, TComponentCallbacks _kCallbacks = ComponentCallback_None);

		public:
			// template register constructor
			//IGameObjectComponent(DefaultInitializerType);
			inline	IGameObjectComponent(DefaultInitializerType) :IComponent(DefaultInit),m_pParent(nullptr)	{}

			// deserialization constructor
			IGameObjectComponent(GameObject* _pParent, const ObjectType _kObjectType, TComponentCallbacks _kCallbacks);

			GameObject* GetParent() const final;

		private:
			GameObject* const m_pParent;
		};

		inline GameObject* IGameObjectComponent::GetParent() const	{return m_pParent;}
	}// Datastructures
} // Helix

#endif // !GAMEOBJECTCOMPONENT_H
