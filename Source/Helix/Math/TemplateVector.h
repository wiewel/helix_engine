//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef TEMPLATEVECTOR_H
#define TEMPLATEVECTOR_H

#include <array>
#include <type_traits>

// Helper construct
#ifndef PROPERTY
#define PROPERTY(member, index) __declspec(property(get = Get##member, put = Set##member)) T member; \
	inline const T& Get##member() const {return Data[index]; } \
	inline void Set##member(const T& _Other) { Data[index] = _Other; }
#endif

namespace Helix
{
	namespace Math
	{
		template <typename T, int _Dim = 2>
		struct VecT
		{
			T Data[_Dim] = {};
			static constexpr int Dim = _Dim;

			constexpr VecT() /*: Data({})*/ {}

			template <typename... Tail>
			constexpr VecT(typename std::enable_if<sizeof...(Tail)+1 == _Dim, T>::type head, Tail... tail)
				: Data{ head, T(tail)... } {}

			inline VecT(const T(&_Other)[_Dim])
			{
				std::copy(std::cbegin(_Other), std::cend(_Other), Data);
			}

			inline VecT(const std::array<T, _Dim>& _Other)
			{
				std::copy(std::cbegin(_Other), std::cend(_Other), Data);
			}

			inline VecT& operator=(const VecT& _Other)
			{
				std::copy(std::cbegin(_Other.Data), std::cend(_Other.Data), Data);
				return *this;
			}

			inline VecT& operator=(const T(&_Other)[_Dim])
			{
				std::copy(std::cbegin(_Other), std::cend(_Other), Data);
				return *this;
			}

			inline VecT& operator=(const std::array<T, _Dim>& _Other)
			{
				std::copy(std::cbegin(_Other), std::cend(_Other), Data);
				return *this;
			}

			inline T& operator[](uint32_t n)
			{
				return Data[n];
			}

			inline const T& operator[](uint32_t n) const
			{
				return Data[n];
			}

			inline VecT operator+ (const VecT& _Other)
			{
				VecT vSum = *this;

				for (int i = 0; i < _Dim; ++i)
				{
					vSum.Data[i] += _Other.Data[i];
				}

				return vSum;
			}

			inline VecT& operator+= (const VecT& _Other)
			{
				for (int i = 0; i < _Dim; ++i)
				{
					Data[i] += _Other.Data[i];
				}

				return *this;
			}

			inline VecT operator- (const VecT& _Other)
			{
				VecT vSum = *this;

				for (int i = 0; i < _Dim; ++i)
				{
					vSum.Data[i] -= _Other.Data[i];
				}

				return vSum;
			}

			inline VecT& operator-= (const VecT& _Other)
			{
				for (int i = 0; i < _Dim; ++i)
				{
					Data[i] -= _Other.Data[i];
				}

				return *this;
			}

			// scale
			inline VecT operator* (const T& _Scale)
			{
				VecT vScaled = *this;

				for (int i = 0; i < _Dim; ++i)
				{
					vScaled.Data[i] *= _Scale;
				}

				return vScaled;
			}

			inline VecT& operator*= (const T& _Scale)
			{
				for (int i = 0; i < _Dim; ++i)
				{
					Data[i] *= _Scale;
				}

				return *this;
			}

			inline VecT operator* (const VecT& _Other)
			{
				VecT vProduct = *this;

				for (int i = 0; i < _Dim; ++i)
				{
					vProduct.Data[i] *= _Other.Data[i];
				}

				return vProduct;
			}

			inline VecT& operator*= (const VecT& _Other)
			{
				for (int i = 0; i < _Dim; ++i)
				{
					Data[i] *= _Other.Data[i];
				}

				return *this;
			}

		};

		template <typename T>
		struct Vec2 : public VecT<T, 2>
		{
			using VecT<T, 2>::VecT;

			PROPERTY(x, 0);
			PROPERTY(y, 1);
		};

		template <typename T>
		struct Vec3 : public VecT<T, 3>
		{
			using VecT<T, 3>::VecT;

			PROPERTY(x, 0);
			PROPERTY(y, 1);
			PROPERTY(z, 2);
		};

		template <typename T>
		struct Vec4 : public VecT<T, 4>
		{
			using VecT<T, 4>::VecT;

			PROPERTY(x, 0);
			PROPERTY(y, 1);
			PROPERTY(z, 2);
			PROPERTY(w, 3);
		};
	} // Math
} // Helix

#endif // !TEMPLATEVECTOR_H
