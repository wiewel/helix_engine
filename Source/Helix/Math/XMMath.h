//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!!!!Never include DirectXMath.h, always include this header instead!!!!
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


//http://msdn.microsoft.com/en-us/library/windows/desktop/ee418728%28v=vs.85%29.aspx

//The FXMVECTOR, GXMVECTOR, HXMVECTOR, and CXMVECTOR aliases support these conventions :
//
//Use the FXMVECTOR alias to pass up to the first three instances of XMVECTOR used as arguments to a function.
//Use the GXMVECTOR alias to pass the 4th instance of an XMVECTOR used as an argument to a function.
//Use the HXMVECTOR alias to pass the 5th and 6th instances of an XMVECTOR used as an argument to a function.For info about additional considerations, see the __vectorcall documentation.
//Use the CXMVECTOR alias to pass any further instances of XMVECTOR used as arguments.
//
//Note  For output parameters, always use XMVECTOR* or XMVECTOR& and ignore them with respect to the preceding rules for input parameters.
//
//Because of limitations with __vectorcall, we recommend that you not use GXMVECTOR or HXMVECTOR for C++ constructors.Just use FXMVECTOR for the first three XMVECTOR values, then use CXMVECTOR for the rest.
//
//The FXMMATRIX and CXMMATRIX aliases help support taking advantage of the HVA argument passing with __vectorcall.
//
//Use the FXMMATRIX alias to pass the first XMMATRIX as an argument to the function.This assumes you don't have more than two FXMVECTOR arguments or more than two float, double, or FXMVECTOR arguments to the �right� of the matrix. For info about additional considerations, see the __vectorcall documentation.
//Use the CXMMATRIX alias otherwise.
//
//Because of limitations with __vectorcall, we recommend that you never use FXMMATRIX for C++ constructors.Just use CXMMATRIX.
//
//In addition to the type aliases, you must also use the XM_CALLCONV annotation to make sure the function uses the appropriate calling convention(__fastcall versus __vectorcall) based on your compiler and architecture.Because of limitations with __vectorcall, we recommend that you not use XM_CALLCONV for C++ constructors.

#ifndef XMMATH_H
#define XMMATH_H

//SSE 4.1 adds native DotProduct cpu functions
//http://blogs.msdn.com/b/chuckw/archive/2012/09/11/directxmath-sse4-1-and-sse-4-2.aspx

#ifndef _SSE4
#pragma warning (disable: 4838)
#include <DirectXMath.h>
#pragma warning (default: 4838)

#else

#pragma warning (disable: 4838)
#include "Math\DirectXMathSSE4.h"
#pragma warning (default: 4838)

//remap to new SSE4 functions
namespace DirectX
{
	//-------------------------------------------------------------------------------------
	// Vector
	//-------------------------------------------------------------------------------------

#define XMVectorGetYPtr SSE4::XMVectorGetYPtr
#define XMVectorGetZPtr SSE4::XMVectorGetZPtr
#define XMVectorGetWPtr SSE4::XMVectorGetWPtr
#define XMVectorGetIntY SSE4::XMVectorGetIntY
#define XMVectorGetIntZ SSE4::XMVectorGetIntZ
#define XMVectorGetIntW SSE4::XMVectorGetIntW
#define XMVectorGetIntYPtr SSE4::XMVectorGetIntYPtr
#define XMVectorGetIntZPtr SSE4::XMVectorGetIntZPtr
#define XMVectorGetIntWPtr SSE4::XMVectorGetIntWPtr
#define XMVectorSetY SSE4::XMVectorSetY
#define XMVectorSetZ SSE4::XMVectorSetZ
#define XMVectorSetW SSE4::XMVectorSetW
#define XMVectorSetIntY SSE4::XMVectorSetIntY
#define XMVectorSetIntZ SSE4::XMVectorSetIntZ
#define XMVectorSetIntW SSE4::XMVectorSetIntW
#define XMVectorRound SSE4::XMVectorRound
#define XMVectorTruncate SSE4::XMVectorTruncate
#define XMVectorFloor SSE4::XMVectorFloor
#define XMVectorCeiling SSE4::XMVectorCeiling

	//-------------------------------------------------------------------------------------
	// Vector2
	//-------------------------------------------------------------------------------------

#define XMVector2Dot SSE4::XMVector2Dot
#define XMVector2LengthSq SSE4::XMVector2LengthSq
#define XMVector2ReciprocalLengthEst SSE4::XMVector2ReciprocalLengthEst
#define XMVector2ReciprocalLength SSE4::XMVector2ReciprocalLength
#define XMVector2LengthEst SSE4::XMVector2LengthEst
#define XMVector2Length SSE4::XMVector2Length
#define XMVector2NormalizeEst SSE4::XMVector2NormalizeEst
#define XMVector2Normalize SSE4::XMVector2Normalize


	//-------------------------------------------------------------------------------------
	// Vector3
	//-------------------------------------------------------------------------------------

#define XMVector3Dot SSE4::XMVector3Dot
#define XMVector3LengthSq SSE4::XMVector3LengthSq
#define XMVector3ReciprocalLengthEst SSE4::XMVector3ReciprocalLengthEst
#define XMVector3ReciprocalLength SSE4::XMVector3ReciprocalLength
#define XMVector3LengthEst SSE4::XMVector3LengthEst
#define XMVector3Length SSE4::XMVector3Length
#define XMVector3NormalizeEst SSE4::XMVector3NormalizeEst
#define XMVector3Normalize SSE4::XMVector3Normalize

	//-------------------------------------------------------------------------------------
	// Vector4
	//-------------------------------------------------------------------------------------

#define XMVector4Dot SSE4::XMVector4Dot
#define XMVector4LengthSq SSE4::XMVector4LengthSq
#define XMVector4ReciprocalLengthEst SSE4::XMVector4ReciprocalLengthEst
#define XMVector4ReciprocalLength SSE4::XMVector4ReciprocalLength
#define XMVector4LengthEst SSE4::XMVector4LengthEst
#define XMVector4Length SSE4::XMVector4Length
#define XMVector4NormalizeEst SSE4::XMVector4NormalizeEst
#define XMVector4Normalize SSE4::XMVector4Normalize

	//-------------------------------------------------------------------------------------
	// Plane
	//-------------------------------------------------------------------------------------

#define XMPlaneNormalizeEst SSE4::XMPlaneNormalizeEst
#define XMPlaneNormalize SSE4::XMPlaneNormalize

}; // namespace DirectX;

#endif // _SSE4

namespace Helix
{
	namespace Math
	{
		using namespace DirectX;

		//-------------------------------------------------------------------------------------
		// Defines
		//-------------------------------------------------------------------------------------

#define XMFLOAT2_ZERO XMFLOAT2(0,0)
#define XMFLOAT3_ZERO XMFLOAT3(0,0,0)
#define XMFLOAT4_ZERO XMFLOAT4(0,0,0,0)
#define XMFLOAT2A_ZERO XMFLOAT2A(0,0)
#define XMFLOAT3A_ZERO XMFLOAT3A(0,0,0)
#define XMFLOAT4A_ZERO XMFLOAT4A(0,0,0,0)
#define XMFLOAT4_QUATERNION_IDENTITY XMFLOAT4A(0,0,0,1)
#define XMFLOAT3X3_ZERO XMFLOAT3X3(0,0,0,0,0,0,0,0,0)
#define XMFLOAT4X4_ZERO XMFLOAT4X4(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)
#define XMMATRIX_ZERO XMMATRIX(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)
#define XMVECTOR_ZERO XMVectorZero()

		//-------------------------------------------------------------------------------------
		// Functions
		//-------------------------------------------------------------------------------------

		//Scalar
		inline uint32_t Log2(const uint32_t& x)
		{
			unsigned long y;
			_BitScanReverse(&y, x);
			return y;
		}

		inline uint32_t Log2(const uint64_t& x)
		{
			unsigned long y;
			_BitScanReverse64(&y, x);
			return y;
		}

		// Vector 2
		inline XMFLOAT2 Scale(const float _fScale, const XMFLOAT2& _v0)
		{
			return XMFLOAT2(_fScale * _v0.x, _fScale * _v0.y);
		}

		inline XMFLOAT2 Add(const XMFLOAT2& _lhs, const XMFLOAT2& _rhs)
		{
			return XMFLOAT2(_lhs.x + _rhs.x, _lhs.y + _rhs.y);
		}

		inline XMFLOAT2 Sub(const XMFLOAT2& _lhs, const XMFLOAT2& _rhs)
		{
			return XMFLOAT2(_lhs.x - _rhs.x, _lhs.y - _rhs.y);
		}

		inline XMFLOAT2 Mul(const XMFLOAT2& _lhs, const XMFLOAT2& _rhs)
		{
			return XMFLOAT2(_lhs.x * _rhs.x, _lhs.y * _rhs.y);
		}

		inline XMFLOAT2 Div(const XMFLOAT2& _lhs, const XMFLOAT2& _rhs)
		{
			return XMFLOAT2(_lhs.x / _rhs.x, _lhs.y / _rhs.y);
		}

		// returns new normalized vector
		inline XMFLOAT2 Normalize(const XMFLOAT2& _v0)
		{
			float fLength = 1.0f / sqrtf((_v0.x * _v0.x) + (_v0.y * _v0.y));
			return Scale(fLength, _v0);
		}

		// Vector 3
		inline XMFLOAT3 Scale(const float _fScale, const XMFLOAT3& _v0)
		{
			return XMFLOAT3(_fScale * _v0.x, _fScale * _v0.y, _fScale * _v0.z);
		}

		inline XMFLOAT3 Add(const XMFLOAT3& _lhs, const XMFLOAT3& _rhs)
		{
			return XMFLOAT3(_lhs.x + _rhs.x, _lhs.y + _rhs.y, _lhs.z + _rhs.z);
		}

		inline XMFLOAT3 Sub(const XMFLOAT3& _lhs, const XMFLOAT3& _rhs)
		{
			return XMFLOAT3(_lhs.x - _rhs.x, _lhs.y - _rhs.y, _lhs.z - _rhs.z);
		}

		inline XMFLOAT3 Mul(const XMFLOAT3& _lhs, const XMFLOAT3& _rhs)
		{
			return XMFLOAT3(_lhs.x * _rhs.x, _lhs.y * _rhs.y, _lhs.z * _rhs.z);
		}

		inline XMFLOAT3 Div(const XMFLOAT3& _lhs, const XMFLOAT3& _rhs)
		{
			return XMFLOAT3(_lhs.x / _rhs.x, _lhs.y / _rhs.y, _lhs.z / _rhs.z);
		}

		// returns new normalized vector
		inline XMFLOAT3 Normalize(const XMFLOAT3& _v0)
		{
			float fLength = 1.0f / sqrtf((_v0.x * _v0.x) + (_v0.y * _v0.y) + (_v0.z * _v0.z));
			return Scale(fLength, _v0);
		}

		inline XMFLOAT3 XMFloat3Get(_In_ FXMVECTOR _vVector)
		{
			XMFLOAT3 vRet;
			XMStoreFloat3(&vRet, _vVector);
			return vRet;
		}

		// Vector 4
		inline XMFLOAT4 Scale(const float _fScale, const XMFLOAT4& _v0)
		{
			return XMFLOAT4(_fScale * _v0.x, _fScale * _v0.y, _fScale * _v0.z, _fScale * _v0.w);
		}

		inline XMFLOAT4 Add(const XMFLOAT4& _lhs, const XMFLOAT4& _rhs)
		{
			return XMFLOAT4(_lhs.x + _rhs.x, _lhs.y + _rhs.y, _lhs.z + _rhs.z, _lhs.w + _rhs.w);
		}

		inline XMFLOAT4 Sub(const XMFLOAT4& _lhs, const XMFLOAT4& _rhs)
		{
			return XMFLOAT4(_lhs.x - _rhs.x, _lhs.y - _rhs.y, _lhs.z - _rhs.z, _lhs.w - _rhs.w);
		}

		inline XMFLOAT4 Mul(const XMFLOAT4& _lhs, const XMFLOAT4& _rhs)
		{
			return XMFLOAT4(_lhs.x * _rhs.x, _lhs.y * _rhs.y, _lhs.z * _rhs.z, _lhs.w * _rhs.w);
		}

		inline XMFLOAT4 Div(const XMFLOAT4& _lhs, const XMFLOAT4& _rhs)
		{
			return XMFLOAT4(_lhs.x / _rhs.x, _lhs.y / _rhs.y, _lhs.z / _rhs.z, _lhs.w / _rhs.w);
		}

		// returns new normalized vector
		inline XMFLOAT4 Normalize(const XMFLOAT4& _v0)
		{
			float fLength = 1.0f / sqrtf((_v0.x * _v0.x) + (_v0.y * _v0.y) + (_v0.z * _v0.z) + (_v0.w * _v0.w));
			return Scale(fLength, _v0);
		}

		// returns new normalized color vector
		inline XMFLOAT4 NormalizeColor(const XMFLOAT4& _v0)
		{
			float fLength = 1.0f / sqrtf((_v0.x * _v0.x) + (_v0.y * _v0.y) + (_v0.z * _v0.z));
			return XMFLOAT4( _v0.x * fLength, _v0.y * fLength, _v0.z * fLength, _v0.w);
		}

		inline XMFLOAT4 XMFloat4Get(_In_ FXMVECTOR _vVector)
		{
			XMFLOAT4 vRet;
			XMStoreFloat4(&vRet, _vVector);
			return vRet;
		}

		inline XMFLOAT3X3 XMFloat3x3Get(_In_ FXMMATRIX _mMatrix)
		{
			XMFLOAT3X3 mRet;
			XMStoreFloat3x3(&mRet, _mMatrix);
			return mRet;
		}

		inline XMFLOAT4X4 XMFloat4x4Get(_In_ FXMMATRIX _mMatrix)
		{
			XMFLOAT4X4 mRet;
			XMStoreFloat4x4(&mRet, _mMatrix);
			return mRet;
		}
		
		inline XMFLOAT3A XMFloat3AGet(_In_ FXMVECTOR _vVector)
		{
			XMFLOAT3A vRet;
			XMStoreFloat3A(&vRet, _vVector);
			return vRet;
		}

		inline XMFLOAT4A XMFloat4AGet(_In_ FXMVECTOR _vVector)
		{
			XMFLOAT4A vRet;
			XMStoreFloat4A(&vRet, _vVector);
			return vRet;
		}

		//compares all components of a XMVECTOR
		inline bool XM_CALLCONV XMVectorCompare(FXMVECTOR V1, FXMVECTOR V2)
		{
			uint32_t uRes;
			XMVectorEqualR(&uRes, V1, V2);
			return XMComparisonAllTrue(uRes);
		}

		//Computes Quaternion Q2*Q1
		inline XMFLOAT4 MulQuat(const XMFLOAT4& _Q1, const XMFLOAT4& _Q2)
		{
			return XMFloat4Get(XMQuaternionMultiply(XMLoadFloat4(&_Q1), XMLoadFloat4(&_Q2)));
		}

		inline XMVECTOR XM_CALLCONV XMVector3Rotate(FXMVECTOR _vPoint, FXMVECTOR _vQuaternion)
		{
			XMMATRIX R = XMMatrixRotationQuaternion(_vQuaternion);
			return XMVector3Transform(_vPoint, R);
		}

		inline XMVECTOR XM_CALLCONV XMVector3RotateNormal(FXMVECTOR _vNormal, FXMVECTOR _vQuaternion)
		{
			XMMATRIX R = XMMatrixRotationQuaternion(_vQuaternion);
			return XMVector3TransformNormal(_vNormal, R);
		}

		inline XMVECTOR XM_CALLCONV XMVector3Distance(FXMVECTOR V1, FXMVECTOR V2)
		{
			XMVECTOR ret = XMVectorSubtract(V1, V2);
			ret = XMVector3Length(ret);
			return ret;
		}

		inline XMVECTOR XM_CALLCONV XMVector3Distance(const XMFLOAT3& V1, const XMFLOAT3& V2)
		{
			return XMVector3Distance(XMLoadFloat3(&V1), XMLoadFloat3(&V2));
		}

		inline float XM_CALLCONV XMVector3DistanceF(FXMVECTOR V1, FXMVECTOR V2)
		{
			XMVECTOR ret = XMVector3Distance(V1, V2);
			return XMVectorGetX(ret);
		}

		inline float XMVector3DistanceF(const XMFLOAT3& V1, const XMFLOAT3& V2)
		{
			return XMVector3DistanceF(XMLoadFloat3(&V1), XMLoadFloat3(&V2));
		}

		inline float XM_CALLCONV XMVector3LengthF(const XMVECTOR& V1)
		{
			return XMVectorGetX(XMVector3Length(V1));
		}
		
		inline float XM_CALLCONV XMVector4LengthF(const XMVECTOR& V1)
		{
			return XMVectorGetX(XMVector4Length(V1));
		}

		inline float XMVector3LengthF(const XMFLOAT3& V1)
		{
			return XMVectorGetX(XMVector3Length(XMLoadFloat3(&V1)));
		}

		inline float XMVector4LengthF(const XMFLOAT4& V1)
		{
			return XMVectorGetX(XMVector4Length(XMLoadFloat4(&V1)));
		}

		inline float XM_CALLCONV XMVector3LengthEstF(const XMVECTOR& V1)
		{
			return XMVectorGetX(XMVector3LengthEst(V1));
		}

		inline float XM_CALLCONV XMVector4LengthEstF(const XMVECTOR& V1)
		{
			return XMVectorGetX(XMVector4LengthEst(V1));
		}

		inline float XMVector3LengthEstF(const XMFLOAT3& V1)
		{
			return XMVectorGetX(XMVector3LengthEst(XMLoadFloat3(&V1)));
		}

		inline float XMVector4LengthEstF(const XMFLOAT4& V1)
		{
			return XMVectorGetX(XMVector4LengthEst(XMLoadFloat4(&V1)));
		}
		//---------------------------------------------------------------------------------------------------
		inline const XMMATRIX XM_CALLCONV XMMatrixTensor(const XMFLOAT3& vec)
		{
			// vec * vec^T
			XMVECTOR Diagonal = XMLoadFloat3(&vec);
			XMVECTOR Triangle = XMVectorSwizzle(Diagonal, 1, 2, 0, 3);

			Triangle = XMVectorMultiply(Diagonal, Triangle);
			Diagonal = XMVectorMultiply(Diagonal, Diagonal);

			return XMMatrixSet(XMVectorGetX(Diagonal), XMVectorGetX(Triangle), XMVectorGetZ(Triangle), 0.f,
				XMVectorGetX(Triangle), XMVectorGetY(Diagonal), XMVectorGetY(Triangle), 0.f,
				XMVectorGetZ(Triangle), XMVectorGetY(Triangle), XMVectorGetZ(Diagonal), 0.f,
				0.f, 0.f, 0.f, 0.f);
		}	
		//---------------------------------------------------------------------------------------------------
		inline void XM_CALLCONV GetObjectToWorldMatrix(_In_ const XMFLOAT3& _vPosition, _In_ const XMFLOAT3& _vScale, _In_ const XMFLOAT3X3& _mRotation, _Out_ XMFLOAT4X4& _mObjectToWorld)
		{
			//scale * rot * trans
			XMMATRIX mScale = XMMatrixScaling(_vScale.x, _vScale.y, _vScale.z);

			XMMATRIX mRot = XMLoadFloat3x3(&_mRotation);

			XMMATRIX mTrans = Math::XMMatrixTranslation(_vPosition.x, _vPosition.y, _vPosition.z);

			//(scale * (rotX * rotY * rotZ) ) * trans
			XMMATRIX mWorld = Math::XMMatrixMultiply(Math::XMMatrixMultiply(mScale, mRot), mTrans);

			//todo save for objects that dont update every frame
			XMStoreFloat4x4(&_mObjectToWorld, mWorld);
		}
		//---------------------------------------------------------------------------------------------------
		inline void XM_CALLCONV GetObjectToWorldMatrix(_In_ const XMFLOAT3& _vPosition, _In_ const XMFLOAT3& _vScale, _In_ const XMFLOAT3X3& _mRotation, _Out_ XMFLOAT4X4& _mObjectToWorld, _Out_ XMFLOAT4X4& _mNormal)
		{
			//scale * rot * trans
			XMMATRIX mScale = XMMatrixScaling(_vScale.x, _vScale.y, _vScale.z);

			XMMATRIX mRot = XMLoadFloat3x3(&_mRotation);

			XMMATRIX mTrans = Math::XMMatrixTranslation(_vPosition.x, _vPosition.y, _vPosition.z);

			//(scale * (rotX * rotY * rotZ) ) * trans
			XMMATRIX mWorld = Math::XMMatrixMultiply(Math::XMMatrixMultiply(mScale, mRot), mTrans);

			XMStoreFloat4x4(&_mObjectToWorld, mWorld);

			// Build the current normal matrix
			mWorld = Math::XMMatrixInverse(nullptr, mWorld);
			mWorld = Math::XMMatrixTranspose(mWorld);

			XMStoreFloat4x4(&_mNormal, mWorld);
		}
		//---------------------------------------------------------------------------------------------------
		inline void XM_CALLCONV ComputeTangentBasis(_In_ const XMFLOAT3& _vP0, _In_ const XMFLOAT3& _vP1, _In_ const XMFLOAT3& _vP2,
			_In_ const XMFLOAT2& _vUV0, _In_ const XMFLOAT2& _vUV1, _In_ const XMFLOAT2& _vUV2,
			_Out_ XMFLOAT3* _pNormal, _Out_ XMFLOAT3* _pTangent, _Out_ XMFLOAT3* _pBitangent)
		{
			XMVECTOR vP0 = XMLoadFloat3(&_vP0);
			XMVECTOR vP1 = XMLoadFloat3(&_vP1);
			XMVECTOR vP2 = XMLoadFloat3(&_vP2);
			XMVECTOR UV0 = XMLoadFloat2(&_vUV0);
			XMVECTOR UV1 = XMLoadFloat2(&_vUV1);
			XMVECTOR UV2 = XMLoadFloat2(&_vUV2);

			XMVECTOR e1 = vP1 - vP0;
			XMVECTOR e0 = vP2 - vP0;

			XMVECTOR vNormal = XMVector3Cross(e0, e1);

			//using Eric Lengyel's approach with a few modifications
			//from Mathematics for 3D Game Programmming and Computer Graphics
			// want to be able to transform a vector in Object Space to Tangent Space
			// such that the x-axis cooresponds to the 's' direction and the
			// y-axis corresponds to the 't' direction, and the z-axis corresponds
			// to <0,0,1>, straight up out of the texture map

			//let P = v1 - v0
			XMVECTOR P = vP1 - vP0;
			//let Q = v2 - v0
			XMVECTOR Q = vP2 - vP0;

			float s1 = XMVectorGetX(UV1) - XMVectorGetX(UV0); //UV1.x - UV0.x;
			float t1 = XMVectorGetY(UV1) - XMVectorGetY(UV0); //UV1.y - UV0.y;
			float s2 = XMVectorGetX(UV2) - XMVectorGetX(UV0); //UV2.x - UV0.x;
			float t2 = XMVectorGetY(UV2) - XMVectorGetY(UV0); //UV2.y - UV0.y;


			//we need to solve the equation
			// P = s1*T + t1*B
			// Q = s2*T + t2*B
			// for T and B

			//this is a linear system with six unknowns and six equations, for TxTyTz BxByBz
			//[px,py,pz] = [s1,t1] * [Tx,Ty,Tz]
			// qx,qy,qz     s2,t2     Bx,By,Bz

			//multiplying both sides by the inverse of the s,t matrix gives
			//[Tx,Ty,Tz] = 1/(s1t2-s2t1) *  [t2,-t1] * [px,py,pz]
			// Bx,By,Bz                      -s2,s1	    qx,qy,qz  

			//solve this for the unormalized T and B to get from tangent to object space

			float tmp = 0.0f;
			if (fabsf(s1*t2 - s2*t1) <= 0.0001f)
			{
				tmp = 1.0f;
			}
			else
			{
				tmp = 1.0f / (s1*t2 - s2*t1);
			}

			XMFLOAT3 vP;
			XMStoreFloat3(&vP, P);
			XMFLOAT3 vQ;
			XMStoreFloat3(&vQ, Q);

			if (_pTangent != nullptr) 
			{
				_pTangent->x = (t2*vP.x - t1*vQ.x) *tmp;
				_pTangent->y = (t2*vP.y - t1*vQ.y) *tmp;
				_pTangent->z = (t2*vP.z - t1*vQ.z) *tmp;

				XMStoreFloat3(_pTangent, XMVector3Normalize(XMLoadFloat3(_pTangent)));
			}

			if (_pBitangent != nullptr)
			{
				_pBitangent->x = (s1*vQ.x - s2*vP.x) *tmp;
				_pBitangent->y = (s1*vQ.y - s2*vP.y) *tmp;
				_pBitangent->z = (s1*vQ.z - s2*vP.z) *tmp;

				XMStoreFloat3(_pBitangent, XMVector3Normalize(XMLoadFloat3(_pBitangent)));
			}

			if (_pNormal != nullptr) 
			{
				XMStoreFloat3(_pNormal, XMVector3Normalize(vNormal));
			}
		}
		//---------------------------------------------------------------------------------------------------
	}; // namespace Math
}; // namespace Helix

#endif