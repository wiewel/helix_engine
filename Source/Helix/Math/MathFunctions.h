//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef MATHFUNCTIONS_H
#define MATHFUNCTIONS_H

//#include "XMMath.h"
#include "hlx\src\StandardDefines.h"
#include "MathTypes.h"
#include <math.h>
#include <vector>

namespace Helix
{
	namespace Math
	{
		//---------------------------------------------------------------------------------------------------
		template <typename T>
		inline T Saturate(const T& _Value)
		{
			return _Value < 0.0f ? 0.0f : _Value > 1.0f ? 1.0f : _Value;
		}
		//---------------------------------------------------------------------------------------------------
		template <typename T>
		inline T Clamp(const T& _Value, const T& _Lower, const T& _Upper)
		{
			return _Value < _Lower ? _Lower : _Value > _Upper ? _Upper : _Value;
			//return std::max(_Lower, std::min(_Value, _Upper));
		}
		//---------------------------------------------------------------------------------------------------
		inline float GaussianFunction(float _fX, float _fSigma)
		{
			return exp(-((_fX*_fX) / (2.0f * _fSigma*_fSigma)));
		}
		//---------------------------------------------------------------------------------------------------
		// calculates normalized gaussian values based on _uStepWidth, if _uStepWidth = 5 the function returns values for -2, -1, 0, 1, 2
		inline void GaussianCurve(uint32_t _uStepCount, float _fSigma, _Out_ std::vector<float>& _Result)
		{
			_Result.clear();

			float fSum = 0.0f;
			float* pTempResults = new float[_uStepCount];

			float fStartIndex = static_cast<float>( (static_cast<int32_t>(_uStepCount) / 2) + 1 - static_cast<int32_t>(_uStepCount));

			for (uint32_t i = 0; i < _uStepCount; ++i)
			{
				float fCurIndex = fStartIndex + static_cast<float>(i);
				pTempResults[i] = GaussianFunction(fCurIndex, _fSigma);
				fSum += pTempResults[i];
			}
			for (uint32_t i = 0; i < _uStepCount; ++i)
			{
				_Result.push_back(pTempResults[i] / fSum);
			}

			HSAFE_DELETE(pTempResults);
		}
		//---------------------------------------------------------------------------------------------------
		inline float DegToRad(const float& _fDegrees)
		{
			return _fDegrees * (3.141592654f / 180.0f);
		}
		//---------------------------------------------------------------------------------------------------
		inline float Deg2Rad(const float& _fDegrees)
		{
			return DegToRad(_fDegrees);
		}
		//---------------------------------------------------------------------------------------------------
		inline float RadToDeg(const float& _fRadians)
		{
			return _fRadians * (180.0f / 3.141592654f);
		}
		//---------------------------------------------------------------------------------------------------------------------
		inline float Rad2Deg(const float& _fRadians)
		{
			return RadToDeg(_fRadians);
		}
		//---------------------------------------------------------------------------------------------------------------------
		template <class VecType>
		inline float Distance(const VecType& _v1, const VecType& _v2)
		{
			return (_v1 - _v2).magnitude();
		}

		//---------------------------------------------------------------------------------------------------------------------
		template <class VecType>
		inline float DistanceSquared(const VecType& _v1, const VecType& _v2)
		{
			return (_v1 - _v2).magnitudeSquared();
		}
		//---------------------------------------------------------------------------------------------------------------------
		template <class T>
		inline T Random(const T& _Min, const T& _Max)
		{
			return _Min + static_cast<T> (rand()) / (static_cast <T> (RAND_MAX / (_Max - _Min)));
		}
		//---------------------------------------------------------------------------------------------------------------------
		inline float3 Min(const float3& _vA, const float3& _vB)
		{
			float3 Result;
			Result.x = (_vA.x < _vB.x) ? _vA.x : _vB.x;
			Result.y = (_vA.y < _vB.y) ? _vA.y : _vB.y;
			Result.z = (_vA.z < _vB.z) ? _vA.z : _vB.z;
			return Result;
		}
		//---------------------------------------------------------------------------------------------------------------------
		inline float4 Min(const float4& _vA, const float4& _vB)
		{
			float4 Result;
			Result.x = (_vA.x < _vB.x) ? _vA.x : _vB.x;
			Result.y = (_vA.y < _vB.y) ? _vA.y : _vB.y;
			Result.z = (_vA.z < _vB.z) ? _vA.z : _vB.z;
			Result.w = (_vA.w < _vB.w) ? _vA.w : _vB.w;
			return Result;
		}
		//---------------------------------------------------------------------------------------------------------------------
		inline float3 Max(const float3& _vA, const float3& _vB)
		{
			float3 Result;
			Result.x = (_vA.x > _vB.x) ? _vA.x : _vB.x;
			Result.y = (_vA.y > _vB.y) ? _vA.y : _vB.y;
			Result.z = (_vA.z > _vB.z) ? _vA.z : _vB.z;
			return Result;
		}
		//---------------------------------------------------------------------------------------------------------------------
		inline float4 Max(const float4& _vA, const float4& _vB)
		{
			float4 Result;
			Result.x = (_vA.x > _vB.x) ? _vA.x : _vB.x;
			Result.y = (_vA.y > _vB.y) ? _vA.y : _vB.y;
			Result.z = (_vA.z > _vB.z) ? _vA.z : _vB.z;
			Result.w = (_vA.w > _vB.w) ? _vA.w : _vB.w;
			return Result;
		}
		//---------------------------------------------------------------------------------------------------------------------
		inline float3 Abs(const float3& _vX)
		{
			return Max(_vX, _vX * -1.f);
		}
		//---------------------------------------------------------------------------------------------------------------------
		inline float4 Abs(const float4& _vX)
		{
			return Max(_vX, _vX * -1.f);
		}
		//---------------------------------------------------------------------------------------------------------------------

		inline quaternion Slerp(const quaternion& _Q1, const quaternion& _Q2, const float& _t)
		{
			XMVECTOR Q1 = XMLoadQuaternion(&_Q1);
			XMVECTOR Q2 = XMLoadQuaternion(&_Q2);

			quaternion q;
			XMStoreQuaternion(&q, XMQuaternionSlerp(Q1, Q2, _t));
			return q;
		}
		//---------------------------------------------------------------------------------------------------------------------

		// from radians Z-X-Y order
		inline quaternion QuaternionFromEulerRadiansZXY(
			float _fX, float _fY, float _fZ,
			const float3& _vXAxis = { 1.f, 0.f, 0.f },
			const float3& _vYAxis = { 0.f, 1.f, 0.f },
			const float3& _vZAxis = { 0.f, 0.f, 1.f })
		{
			quaternion vRot = quaternion(_fZ, _vZAxis) * quaternion(_fX, _vXAxis) * quaternion(_fY, _vYAxis);
			return vRot.getNormalized();
		}
		//---------------------------------------------------------------------------------------------------------------------

		// from degrees Z-X-Y order
		inline quaternion QuaternionFromEulerDegreesZXY(
			float _fX, float _fY, float _fZ,
			const float3& _vXAxis = { 1.f, 0.f, 0.f },
			const float3& _vYAxis = { 0.f, 1.f, 0.f },
			const float3& _vZAxis = { 0.f, 0.f, 1.f })
		{
			return QuaternionFromEulerRadiansZXY(Deg2Rad(_fX), Deg2Rad(_fY), Deg2Rad(_fZ), _vXAxis, _vYAxis, _vZAxis);
		}
		//---------------------------------------------------------------------------------------------------------------------

		// from radians X-Y-Z order
		inline quaternion QuaternionFromEulerRadiansXYZ(
			float _fX, float _fY, float _fZ,
			const float3& _vXAxis = { 1.f, 0.f, 0.f },
			const float3& _vYAxis = { 0.f, 1.f, 0.f },
			const float3& _vZAxis = { 0.f, 0.f, 1.f })
		{
			quaternion vRot = quaternion(_fX, _vXAxis) * quaternion(_fY, _vYAxis) * quaternion(_fZ, _vZAxis);
			return vRot.getNormalized();
		}
		//---------------------------------------------------------------------------------------------------------------------

		// from degrees X-Y-Z order
		inline quaternion QuaternionFromEulerDegreesXYZ(
			float _fX, float _fY, float _fZ,
			const float3& _vXAxis = { 1.f, 0.f, 0.f },
			const float3& _vYAxis = { 0.f, 1.f, 0.f },
			const float3& _vZAxis = { 0.f, 0.f, 1.f })
		{
			return QuaternionFromEulerRadiansXYZ(Deg2Rad(_fX), Deg2Rad(_fY), Deg2Rad(_fZ), _vXAxis, _vYAxis, _vZAxis);
		}

		//---------------------------------------------------------------------------------------------------------------------

		template <class VecType>
		inline VecType Lerp(const VecType& _V1, const VecType& _V2, const float& _t)
		{
			return _V1 + _t * (_V2 - _V1);
		}
		//---------------------------------------------------------------------------------------------------------------------

		inline float3 RotateAround(const float3& _vPoint, const float3& _vPivot, const quaternion& _vOrientation)
		{
			float3 vDir = _vPoint - _vPivot;
			vDir = _vOrientation * vDir;
			return _vPivot + vDir;
		}
		//---------------------------------------------------------------------------------------------------------------------

		inline float3 MinMaxToHalfExtents(const float3& _vMin, const float3& _vMax)
		{
			float3 vHalfExtents = (_vMax - _vMin) * 0.5f;

//#ifdef _DEBUG
//			float3 vCenter = (_vMax + _vMin) * 0.5f;
//			float3 vMinExt = Abs(_vMin - vCenter);
//			float3 vMaxExt = Abs(_vMax - vCenter);
//			float3 vExt = Max(vMinExt, vMaxExt);
//			HASSERT(vExt == vHalfExtents, "Invalid extents"); // i'm not sure why there are variations but i'm going with the simple solution now
//#endif
			return vHalfExtents;
		}
		//---------------------------------------------------------------------------------------------------------------------


		// Build a transformation (object to world) matrix from Position, Scale and Rotation
		inline void GetTransformationMatrix(const transform& _Transform, const float3& _vScale, _Out_ float4x4& _mTransformation)
		{
			_mTransformation = float4x4(_Transform) * float4x4(float4(_vScale, 1.f));
			// todo: truncate to float4x3
		}

		//---------------------------------------------------------------------------------------------------------------------
		// Build a transformation (object to world) and normal matrix from Position, Scale and Rotation
		inline void GetTransformationAndNormalMatrix(const transform& _Transform, const float3& _vScale, _Out_ float4x4& _mTransformation, _Out_ float4x4& _mNormal)
		{
			GetTransformationMatrix(_Transform, _vScale, _mTransformation);

			_mNormal = _mTransformation.getInverse();
			_mNormal = _mNormal.getTranspose();

			//_mNormal = _mTransformation.inverseRT();
		}
		//---------------------------------------------------------------------------------------------------------------------
		inline void ComputeTangentBasis(_In_ const float3& _vP0, _In_ const float3& _vP1, _In_ const float3& _vP2,
			_In_ const float2& _vUV0, _In_ const float2& _vUV1, _In_ const float2& _vUV2,
			_Out_ float3& _vNormal, _Out_ float3& _vTangent)
		{
			float3 vP = _vP1 - _vP0;
			float3 vQ = _vP2 - _vP0;

			_vNormal = vP.cross(vQ).getNormalized();

			float s1 = _vUV1.x - _vUV0.x;
			float t1 = _vUV1.y - _vUV0.y;
			float s2 = _vUV2.x - _vUV0.x;
			float t2 = _vUV2.y - _vUV0.y;

			float tmp = 0.0f;
			if (fabs(s1*t2 - s2*t1) <= 0.0001f)
			{
				tmp = 1.0f;
			}
			else
			{
				tmp = 1.0f / (s1*t2 - s2*t1);
			}

			_vTangent.x = (t2*vP.x - t1*vQ.x) * tmp;
			_vTangent.y = (t2*vP.y - t1*vQ.y) * tmp;
			_vTangent.z = (t2*vP.z - t1*vQ.z) * tmp;

			_vTangent.normalize();
		}

		//---------------------------------------------------------------------------------------------------------------------
		inline void ComputeTangentBasis(_In_ const float3& _vP0, _In_ const float3& _vP1, _In_ const float3& _vP2,
			_In_ const float2& _vUV0, _In_ const float2& _vUV1, _In_ const float2& _vUV2,
			_Out_ float3& _vNormal, _Out_ float3& _vTangent, _Out_ float3& _vBitangent)
		{
			float3 vP = _vP1 - _vP0;
			float3 vQ = _vP2 - _vP0;

			_vNormal = vP.cross(vQ).getNormalized();

			float s1 = _vUV1.x - _vUV0.x;
			float t1 = _vUV1.y - _vUV0.y;
			float s2 = _vUV2.x - _vUV0.x;
			float t2 = _vUV2.y - _vUV0.y;

			float tmp = 0.0f;
			if (abs(s1*t2 - s2*t1) <= 0.0001f)
			{
				tmp = 1.0f;
			}
			else
			{
				tmp = 1.0f / (s1*t2 - s2*t1);
			}

			_vTangent.x = (t2*vP.x - t1*vQ.x) * tmp;
			_vTangent.y = (t2*vP.y - t1*vQ.y) * tmp;
			_vTangent.z = (t2*vP.z - t1*vQ.z) * tmp;

			_vTangent.normalize();

			_vBitangent.x = (s1*vQ.x - s2*vP.x) *tmp;
			_vBitangent.y = (s1*vQ.y - s2*vP.y) *tmp;
			_vBitangent.z = (s1*vQ.z - s2*vP.z) *tmp;

			_vBitangent.normalize();
		}

		//------------------------------------------------------------------------------
		inline void MatrixLookToLH(const float3& _vEyePosition, const float3& _vEyeDirection, const float3& _vUpDirection, _Out_ matrix& _mViewMatrix)
		{
			HASSERTD(_vEyeDirection.isZero() == false, "_vEyeDirection must not be zero");
			HASSERTD(_vEyeDirection.isFinite(), "_vEyeDirection must be finite");
			HASSERTD(_vUpDirection.isZero() == false, "_vUpDirection must not be zero.");
			HASSERTD(_vUpDirection.isFinite(), "_UpDirection must be finite");

			float3 R2 = _vEyeDirection.getNormalized();

			float3 R0 = _vUpDirection.cross(R2);
			R0.normalize();

			float3 R1 = R2.cross(R0);

			float3 NegEyePosition = _vEyePosition * (-1.0f);

			_mViewMatrix.column0[0] = R0.x;
			_mViewMatrix.column0[1] = R0.y;
			_mViewMatrix.column0[2] = R0.z;
			_mViewMatrix.column0[3] = R0.dot(NegEyePosition);

			_mViewMatrix.column1[0] = R1.x;
			_mViewMatrix.column1[1] = R1.y;
			_mViewMatrix.column1[2] = R1.z;
			_mViewMatrix.column1[3] = R1.dot(NegEyePosition);

			_mViewMatrix.column2[0] = R2.x;
			_mViewMatrix.column2[1] = R2.y;
			_mViewMatrix.column2[2] = R2.z;
			_mViewMatrix.column2[3] = R2.dot(NegEyePosition);

			_mViewMatrix.column3[0] = 0.0f;
			_mViewMatrix.column3[1] = 0.0f;
			_mViewMatrix.column3[2] = 0.0f;
			_mViewMatrix.column3[3] = 1.0f;
			//_mViewMatrix = _mViewMatrix.getTranspose();
#ifdef _DEBUG
			XMVECTOR vEyePosXM = Math::XMLoadFloat3(&_vEyePosition);
			XMVECTOR vEyeDirXM = Math::XMLoadFloat3(&_vEyeDirection);
			XMVECTOR vUpXM = Math::XMLoadFloat3(&_vUpDirection);

			XMMATRIX View = XMMatrixLookToLH(vEyePosXM, vEyeDirXM, vUpXM);

			for(int i = 0; i < 4; ++i)
			{
				float4 vTemp;
				XMStoreFloat4(&vTemp, (View.r[i]));

				//HASSERT(_mViewMatrix[i] != vTemp, "HALT STOP!");
			}
#endif
		}
		//------------------------------------------------------------------------------
		inline matrix MatrixLookToLH(const float3& _vEyePosition, const float3& _vEyeDirection, const float3& _vUpDirection)
		{
			float4x4 m;
			MatrixLookToLH(_vEyePosition, _vEyeDirection, _vUpDirection, m);
			return m;
		}
		//------------------------------------------------------------------------------
		inline void MatrixLookAtLH(const float3& _vEyePosition, const float3& _vFocusPosition, const float3& _vUpDirection, _Out_ float4x4& _mViewMatrix)
		{
			float3 vEyeDirection = _vFocusPosition - _vEyePosition;
			MatrixLookToLH(_vEyePosition, vEyeDirection, _vUpDirection, _mViewMatrix);
		}
		//------------------------------------------------------------------------------
		inline void MatrixOrthographicOffCenterLH(
			float _fViewLeft,
			float _fViewRight,
			float _fViewBottom,
			float _fViewTop,
			float _fNearZ,
			float _fFarZ,
			_Out_ float4x4& _mProjection
		)
		{
			float fReciprocalWidth = 1.0f / (_fViewRight - _fViewLeft);
			float fReciprocalHeight = 1.0f / (_fViewTop - _fViewBottom);
			float fRange = 1.0f / (_fFarZ - _fNearZ);

			// Todo: may need to transpose the result
			_mProjection[0][0] = fReciprocalWidth + fReciprocalWidth;
			_mProjection[0][1] = 0.0f;
			_mProjection[0][2] = 0.0f;
			_mProjection[0][3] = 0.0f;

			_mProjection[1][0] = 0.0f;
			_mProjection[1][1] = fReciprocalHeight + fReciprocalHeight;
			_mProjection[1][2] = 0.0f;
			_mProjection[1][3] = 0.0f;

			_mProjection[2][0] = 0.0f;
			_mProjection[2][1] = 0.0f;
			_mProjection[2][2] = fRange;
			_mProjection[2][3] = 0.0f;

			_mProjection[3][0] = -(_fViewLeft + _fViewRight) * fReciprocalWidth;
			_mProjection[3][1] = -(_fViewTop + _fViewBottom) * fReciprocalHeight;
			_mProjection[3][2] = -fRange * _fNearZ;
			_mProjection[3][3] = 1.0f;

//#ifdef _DEBUG
//			XMMATRIX Proj = XMMatrixOrthographicOffCenterLH(_fViewLeft, _fViewRight, _fViewBottom, _fViewTop, _fNearZ, _fFarZ);
//
//			for (int i = 0; i < 4; ++i)
//			{
//				float4 vTemp;
//				XMStoreFloat4(&vTemp, (Proj.r[i]));
//
//				HASSERT(_mProjection[i] != vTemp, "HALT STOP!");
//			}
//#endif
		}
		//------------------------------------------------------------------------------
		// construct the camera projection matrix, metaphorically the 'lens'
		inline matrix MatrixPerspectiveFovLH(
			float _fFovYRad,
			float _fAspectHByW,
			float _fNearZ,
			float _fFarZ
		)
		{
			HASSERTD(_fFovYRad > 0.0f && _fFovYRad <= 180.0f, "The field of view angle %f� is not in the range (0, 2*PI]", _fFovYRad);
			HASSERTD(_fAspectHByW > 0.0f, "The apect ratio must be positive.");
			HASSERTD(_fNearZ > 0.0f, "The near clipping distance must be positive.");
			HASSERTD(_fFarZ > 0.0f, "The far clipping distance must be positive.");

			float fHeight = cosf(_fFovYRad / 2.0f) / sinf(_fFovYRad / 2.0f); /// 1.0f / tan(FOV/2)
			float fWidth = fHeight / _fAspectHByW;
			float fRange = _fFarZ / (_fFarZ - _fNearZ);

			// fill the matrix in column-major
			matrix m;
			// column 1
			m[0][0] = fWidth;
			m[0][1] = 0.0f;
			m[0][2] = 0.0f;
			m[0][3] = 0.0f;
			// column 2
			m[1][0] = 0.0f;
			m[1][1] = fHeight;
			m[1][2] = 0.0f;
			m[1][3] = 0.0f;
			// column 3
			m[2][0] = 0.0f;
			m[2][1] = 0.0f;
			m[2][2] = fRange;
			m[2][3] = 1.0f;
			// column 4
			m[3][0] = 0.0f;
			m[3][1] = 0.0f;
			m[3][2] = -fRange * _fNearZ;
			m[3][3] = 0.0f;

			return m;
		}
		//------------------------------------------------------------------------------
		// construct the camera infinite projection matrix
		// maps directions to points on infinitely distant far plane
		// direction => 4D vector with w = 0
		inline matrix MatrixInfinitePerspectiveFovLH(
			float _fFovYRad,
			float _fAspectHByW,
			float _fNearZ
		)
		{
			HASSERTD(_fFovYRad > 0.0f && _fFovYRad <= 180.0f, "The field of view angle %f� is not in the range (0, 2*PI]", _fFovYRad);
			HASSERTD(_fAspectHByW > 0.0f, "The apect ratio must be positive.");
			HASSERTD(_fNearZ > 0.0f, "The near clipping distance must be positive.");

			float fHeight = cosf(_fFovYRad / 2.0f) / sinf(_fFovYRad / 2.0f); /// 1.0f / tan(FOV/2)
			float fWidth = fHeight / _fAspectHByW;

			const float fEpsilon = FLT_EPSILON;

			// fill the matrix in column-major
			matrix m;
			// column 1
			m[0][0] = fWidth;
			m[0][1] = 0.0f;
			m[0][2] = 0.0f;
			m[0][3] = 0.0f;
			// column 2
			m[1][0] = 0.0f;
			m[1][1] = fHeight;
			m[1][2] = 0.0f;
			m[1][3] = 0.0f;
			// column 3
			m[2][0] = 0.0f;
			m[2][1] = 0.0f;
			m[2][2] = 1.0f - fEpsilon;
			m[2][3] = 1.0f;
			// column 4
			m[3][0] = 0.0f;
			m[3][1] = 0.0f;
			m[3][2] = (fEpsilon - 1.0f) * _fNearZ;
			m[3][3] = 0.0f;

			return m;
		}
		//------------------------------------------------------------------------------
		// construct rotation matrix from rotation around axis
		// rotation axis must be normalized
		inline matrix MatrixRotationNormal(
			const float3& _vNormalAxis,
			const float _fAngleRad
		)
		{
			HASSERTD(_vNormalAxis.isNormalized(), "Matrix rotation axis must be normalized");
			quaternion r(_fAngleRad, _vNormalAxis);
			return matrix(r);
		}
		//------------------------------------------------------------------------------
		// construct rotation matrix from rotation around axis
		inline matrix MatrixRotationAxis(
			float3 _vRotationAxis,
			const float _fAngleRad
		)
		{
			_vRotationAxis.normalize();
			MatrixRotationNormal(_vRotationAxis, _fAngleRad);
		}
		//------------------------------------------------------------------------------
		// construct quaternion from a possibly scaled rotation matrix
		inline quaternion QuaternionFromScaledRotationMatrix(const float3x3& _mScaledRotation)
		{
			quaternion vRot;

			vRot.w = sqrtf(std::max(0.0f, 1.0f + _mScaledRotation[0][0] + _mScaledRotation[1][1] + _mScaledRotation[2][2])) / 2.0f;
			vRot.x = sqrtf(std::max(0.0f, 1.0f + _mScaledRotation[0][0] - _mScaledRotation[1][1] - _mScaledRotation[2][2])) / 2.0f;
			vRot.y = sqrtf(std::max(0.0f, 1.0f - _mScaledRotation[0][0] + _mScaledRotation[1][1] - _mScaledRotation[2][2])) / 2.0f;
			vRot.z = sqrtf(std::max(0.0f, 1.0f - _mScaledRotation[0][0] - _mScaledRotation[1][1] + _mScaledRotation[2][2])) / 2.0f;

			vRot.x = std::copysign(vRot.x, _mScaledRotation[2][1] - _mScaledRotation[1][2]);
			vRot.y = std::copysign(vRot.y, _mScaledRotation[0][2] - _mScaledRotation[2][0]);
			vRot.z = std::copysign(vRot.z, _mScaledRotation[1][0] - _mScaledRotation[0][1]);

			return vRot.getNormalized();
		}
	}; // namespace Math
}; // namespace Helix

#endif // MATHFUNCTIONS_H