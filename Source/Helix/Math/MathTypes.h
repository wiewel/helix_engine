//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef MATHTYPES_H
#define MATHTYPES_H

#define PXTRANSFORM_DEFAULT_CONSTRUCT_IDENTITY

#include "foundation\PxVec2.h"
#include "foundation\PxVec3.h"
#include "foundation\PxVec4.h"
#include "foundation\PxTransform.h"
#include "foundation\PxQuat.h"
#include "foundation\PxMat33.h"
#include "foundation\PxMat44.h"
#include "characterkinematic\PxExtended.h"

#include "PhysXSDK\Source\Common\src\CmMatrix34.h"

#include "XMMath.h"
#include "TemplateVector.h"

namespace Helix
{
	namespace Math
	{
		//=================================
		// Forward declarations
		//=================================
		//using float2 = physx::PxVec2;

		using float3x3 = physx::PxMat33;
		using float3x4 = physx::Cm::Matrix34;

		using transform = physx::PxTransform;

		using int2 = Vec2<int>;
		using int3 = Vec3<int>;
		using int4 = Vec4<int>;

		using uint2 = Vec2<uint32_t>;
		using uint3 = Vec3<uint32_t>;
		using uint4 = Vec4<uint32_t>;

		class float2;
		class float3;
		class float4;
		class quaternion;
		class float4x4;
		using matrix = float4x4;

		//=================================
		// Conversion to DirectXMath
		//=================================
#pragma region Conversion
		//-----------------------------------------------------------------------------------------------
		XMFLOAT2 Convert(const float2& _f);
		//-----------------------------------------------------------------------------------------------
		XMFLOAT3 Convert(const float3& _f);
		//-----------------------------------------------------------------------------------------------
		XMFLOAT4 Convert(const float4& _f);
		//-----------------------------------------------------------------------------------------------
		XMFLOAT4 Convert(const quaternion& _f);
		//-----------------------------------------------------------------------------------------------
		XMFLOAT3X3 Convert(const float3x3& _f);
		//-----------------------------------------------------------------------------------------------
		XMFLOAT4X4 Convert(const float4x4& _f);
		//-----------------------------------------------------------------------------------------------
		XMFLOAT4X3 Convert(const float3x4& _f);
#pragma endregion

		//=================================
		// SSE4 Storing and loading
		//=================================
#pragma region float2
		//-----------------------------------------------------------------------------------------------
		_Use_decl_annotations_
			void XM_CALLCONV XMStoreFloat2(float2* pDestination, FXMVECTOR  V);
		//-----------------------------------------------------------------------------------------------
		_Use_decl_annotations_
			XMVECTOR XM_CALLCONV XMLoadFloat2(const float2* pSource);
#pragma endregion

#pragma region float3
		//-----------------------------------------------------------------------------------------------
		_Use_decl_annotations_
			void XM_CALLCONV XMStoreFloat3(float3* pDestination, FXMVECTOR  V);
		//-----------------------------------------------------------------------------------------------
		_Use_decl_annotations_
			XMVECTOR XM_CALLCONV XMLoadFloat3(const float3* pSource);
#pragma endregion

#pragma region float4
		//-----------------------------------------------------------------------------------------------
		_Use_decl_annotations_
			void XM_CALLCONV XMStoreFloat4(float4* pDestination, FXMVECTOR  V);
		//-----------------------------------------------------------------------------------------------
		_Use_decl_annotations_
			XMVECTOR XM_CALLCONV XMLoadFloat4(const float4* pSource);
#pragma endregion

#pragma region float3x3
		//-----------------------------------------------------------------------------------------------
		_Use_decl_annotations_
			XMMATRIX XM_CALLCONV XMLoadFloat3x3(const float3x3* pSource);
		//-----------------------------------------------------------------------------------------------
		_Use_decl_annotations_
			void XM_CALLCONV XMStoreFloat3x3(float3x3* pDestination, FXMMATRIX M);
#pragma endregion

#pragma region float4x3
		//-----------------------------------------------------------------------------------------------
		_Use_decl_annotations_
			XMMATRIX XM_CALLCONV XMLoadFloat4x3(const float3x4* pSource);
		//-----------------------------------------------------------------------------------------------
		_Use_decl_annotations_
			void XM_CALLCONV XMStoreFloat4x3(float3x4* pDestination, FXMMATRIX M);
#pragma endregion

#pragma region float4x4
		//-----------------------------------------------------------------------------------------------
		_Use_decl_annotations_
			XMMATRIX XM_CALLCONV XMLoadFloat4x4(const float4x4* pSource);
		//-----------------------------------------------------------------------------------------------
		_Use_decl_annotations_
			void XM_CALLCONV XMStoreFloat4x4(float4x4* pDestination, Helix::Math::FXMMATRIX M);
#pragma endregion
		
		//---------------------------------------------------------------------------------------------------------------------
		// 2 component vector
		//---------------------------------------------------------------------------------------------------------------------

		class float2 : public physx::PxVec2
		{
		public:
			static constexpr int Dim = 3;

			PX_CUDA_CALLABLE PX_FORCE_INLINE float2() {}
			PX_CUDA_CALLABLE PX_FORCE_INLINE float2(physx::PxZERO r) : physx::PxVec2(r) {}
			explicit PX_CUDA_CALLABLE PX_FORCE_INLINE float2(physx::PxReal a) : physx::PxVec2(a) {}
			PX_CUDA_CALLABLE PX_FORCE_INLINE float2(physx::PxReal nx, physx::PxReal ny) : physx::PxVec2(nx, ny) {}
			PX_CUDA_CALLABLE PX_FORCE_INLINE float2(const physx::PxVec2& v) : physx::PxVec2(v) {}

			PX_CUDA_CALLABLE PX_FORCE_INLINE float2(const float2& v) : physx::PxVec2(v) {}

			PX_CUDA_CALLABLE PX_FORCE_INLINE float2(const Helix::Math::XMFLOAT2& _v) : float2(_v.x, _v.y) { }

			PX_CUDA_CALLABLE PX_FORCE_INLINE float2& operator=(const float2& _v)
			{
				x = _v.x;
				y = _v.y;
				return *this;
			}

			PX_CUDA_CALLABLE PX_FORCE_INLINE float2 operator+(const float2& _v) const
			{
				return physx::PxVec2::operator+(_v);
			}

			PX_CUDA_CALLABLE PX_FORCE_INLINE float2 operator-(const float2& _v) const
			{
				return physx::PxVec2::operator-(_v);
			}

			// +
			PX_CUDA_CALLABLE PX_FORCE_INLINE float2 operator+(const float& _fOffset) const
			{
				return float2(x + _fOffset, y + _fOffset);
			}
			// 
			PX_CUDA_CALLABLE PX_FORCE_INLINE float2 operator-(const float& _fOffset) const
			{
				return float2(x - _fOffset, y - _fOffset);
			}

			// *
			PX_CUDA_CALLABLE PX_FORCE_INLINE float2 operator*(const float2& a) const
			{
				return multiply(a);
			}

			// scale
			PX_CUDA_CALLABLE PX_FORCE_INLINE float2 operator*(const float& _fScale) const
			{
				return float2(x * _fScale, y * _fScale);
			}

			PX_CUDA_CALLABLE PX_FORCE_INLINE float distance(const float2& _Other) const
			{
				return (*this - _Other).magnitude();
			}

			PX_CUDA_CALLABLE PX_FORCE_INLINE float distanceSquared(const float2& _Other) const
			{
				return (*this - _Other).magnitudeSquared();
			}

			PX_CUDA_CALLABLE PX_FORCE_INLINE float2 getSaturated() const
			{
				float2 tmp;
				tmp.x = x < 0.0f ? 0.0f : x > 1.0f ? 1.0f : x;
				tmp.y = y < 0.0f ? 0.0f : y > 1.0f ? 1.0f : y;
				return tmp;
			}

			PX_CUDA_CALLABLE PX_FORCE_INLINE void saturated()
			{
				x = x < 0.0f ? 0.0f : x > 1.0f ? 1.0f : x;
				y = y < 0.0f ? 0.0f : y > 1.0f ? 1.0f : y;
			}

#pragma region SwizzleVec2
		public:
			// ################################################################ Swizzle
			inline const float& get_x() const { return x; }
			inline const float& get_y() const { return y; }

			inline void set_x(const float& _x) { x = _x; }
			inline void set_y(const float& _y) { y = _y; }

			__declspec(property(get = get_x, put = set_x)) float r;
			__declspec(property(get = get_y, put = set_y)) float g;

			__declspec(property(get = get_xy, put = set_xy)) float2 xy;
			__declspec(property(get = get_xy, put = set_xy)) float2 rg;

			inline float2 get_xy() const
			{
				return float2(x, y);
			}

			inline void set_xy(const float2& _Value)
			{
				x = _Value.x;
				y = _Value.y;
			}

			__declspec(property(get = get_yx, put = set_yx)) float2 yx;
			__declspec(property(get = get_yx, put = set_yx)) float2 gr;

			inline float2 get_yx() const
			{
				return float2(y, x);
			}

			inline void set_yx(const float2& _Value)
			{
				y = _Value.x;
				x = _Value.y;
			}
#pragma endregion
		};

		PX_CUDA_CALLABLE static PX_FORCE_INLINE float2 operator *(float f, float2 _V)
		{
			return physx::operator*(f, _V);
		}

		//---------------------------------------------------------------------------------------------------------------------
		// 3 component vector
		//---------------------------------------------------------------------------------------------------------------------

		class float3 : public physx::PxVec3
		{
		public:
			static constexpr int Dim = 3;

			PX_CUDA_CALLABLE PX_FORCE_INLINE float3() {}
			PX_CUDA_CALLABLE PX_FORCE_INLINE float3(physx::PxZERO r) : physx::PxVec3(r) {}
			explicit PX_CUDA_CALLABLE PX_FORCE_INLINE float3(physx::PxReal a) : physx::PxVec3(a) {}
			PX_CUDA_CALLABLE PX_FORCE_INLINE float3(physx::PxReal nx, physx::PxReal ny, physx::PxReal nz) : physx::PxVec3(nx, ny, nz) {}
			PX_CUDA_CALLABLE PX_FORCE_INLINE float3(const physx::PxVec3& v) : physx::PxVec3(v) {}
			PX_CUDA_CALLABLE PX_FORCE_INLINE float3(const physx::PxExtendedVec3& v) : physx::PxVec3(physx::toVec3(v)) {}

			PX_CUDA_CALLABLE PX_FORCE_INLINE float3(const float3& v) : physx::PxVec3(v) {}

			PX_CUDA_CALLABLE PX_FORCE_INLINE float3(const Helix::Math::XMFLOAT3& _v) : float3(_v.x, _v.y, _v.z) { }

			PX_CUDA_CALLABLE PX_FORCE_INLINE float3& operator=(const float3& _v)
			{
				x = _v.x;
				y = _v.y;
				z = _v.z;
				return *this;
			}

			PX_CUDA_CALLABLE PX_FORCE_INLINE float3 operator+(const float3& _v) const
			{
				return physx::PxVec3::operator+(_v);
			}

			PX_CUDA_CALLABLE PX_FORCE_INLINE float3 operator-(const float3& _v) const
			{
				return physx::PxVec3::operator-(_v);
			}

			// +
			PX_CUDA_CALLABLE PX_FORCE_INLINE float3 operator+(const float& _fOffset) const
			{
				return float3(x + _fOffset, y + _fOffset, z + _fOffset);
			}
			// 
			PX_CUDA_CALLABLE PX_FORCE_INLINE float3 operator-(const float& _fOffset) const
			{
				return float3(x - _fOffset, y - _fOffset, z - _fOffset);
			}

			// *
			PX_CUDA_CALLABLE PX_FORCE_INLINE float3 operator*(const float3& a) const
			{
				return multiply(a);
			}

			// scale
			PX_CUDA_CALLABLE PX_FORCE_INLINE float3 operator*(const float& _fScale) const
			{
				return float3(x * _fScale, y * _fScale, z * _fScale);
			}

			PX_CUDA_CALLABLE PX_FORCE_INLINE float distance(const float3& _Other) const
			{
				return (*this - _Other).magnitude();
			}

			PX_CUDA_CALLABLE PX_FORCE_INLINE float distanceSquared(const float3& _Other) const
			{
				return (*this - _Other).magnitudeSquared();
			}

			PX_CUDA_CALLABLE PX_FORCE_INLINE float3 getSaturated() const
			{
				float3 tmp;
				tmp.x = x < 0.0f ? 0.0f : x > 1.0f ? 1.0f : x;
				tmp.y = y < 0.0f ? 0.0f : y > 1.0f ? 1.0f : y;
				tmp.z = z < 0.0f ? 0.0f : z > 1.0f ? 1.0f : z;
				return tmp;
			}

			PX_CUDA_CALLABLE PX_FORCE_INLINE void saturated()
			{
				x = x < 0.0f ? 0.0f : x > 1.0f ? 1.0f : x;
				y = y < 0.0f ? 0.0f : y > 1.0f ? 1.0f : y;
				z = z < 0.0f ? 0.0f : z > 1.0f ? 1.0f : z;
			}
			
			/// _vOther is the axis projected onto
			PX_CUDA_CALLABLE PX_FORCE_INLINE float3 projectOnto(const float3& _vOther) const
			{
				return (this->dot(_vOther) / (_vOther.dot(_vOther))) * _vOther;
			}

#pragma region SwizzleVec3
		public:
			// ################################################################ Swizzle
			inline const float& get_x() const { return x; }
			inline const float& get_y() const { return y; }
			inline const float& get_z() const { return z; }

			inline void set_x(const float& _x) { x = _x; }
			inline void set_y(const float& _y) { y = _y; }
			inline void set_z(const float& _z) { z = _z; }

			__declspec(property(get = get_x, put = set_x)) float r;
			__declspec(property(get = get_y, put = set_y)) float g;
			__declspec(property(get = get_z, put = set_z)) float b;

			__declspec(property(get = get_xy, put = set_xy)) float2 xy;
			__declspec(property(get = get_xy, put = set_xy)) float2 rg;

			inline float2 get_xy() const
			{
				return float2(x, y);
			}

			inline void set_xy(const float2& _Value)
			{
				x  = _Value.x;
				y  = _Value.y;
			}

			__declspec(property(get = get_xz, put = set_xz)) float2 xz;
			__declspec(property(get = get_xz, put = set_xz)) float2 rb;

			inline float2 get_xz() const
			{
				return float2(x, z);
			}

			inline void set_xz(const float2& _Value)
			{
				x  = _Value.x;
				z  = _Value.y;
			}

			__declspec(property(get = get_yx, put = set_yx)) float2 yx;
			__declspec(property(get = get_yx, put = set_yx)) float2 gr;

			inline float2 get_yx() const
			{
				return float2(y, x);
			}

			inline void set_yx(const float2& _Value)
			{
				y  = _Value.x;
				x  = _Value.y;
			}

			__declspec(property(get = get_yz, put = set_yz)) float2 yz;
			__declspec(property(get = get_yz, put = set_yz)) float2 gb;

			inline float2 get_yz() const
			{
				return float2(y, z);
			}

			inline void set_yz(const float2& _Value)
			{
				y  = _Value.x;
				z  = _Value.y;
			}

			__declspec(property(get = get_zx, put = set_zx)) float2 zx;
			__declspec(property(get = get_zx, put = set_zx)) float2 br;

			inline float2 get_zx() const
			{
				return float2(z, x);
			}

			inline void set_zx(const float2& _Value)
			{
				z  = _Value.x;
				x  = _Value.y;
			}

			__declspec(property(get = get_zy, put = set_zy)) float2 zy;
			__declspec(property(get = get_zy, put = set_zy)) float2 bg;

			inline float2 get_zy() const
			{
				return float2(z, y);
			}

			inline void set_zy(const float2& _Value)
			{
				z  = _Value.x;
				y  = _Value.y;
			}

			__declspec(property(get = get_xyz, put = set_xyz)) float3 xyz;
			__declspec(property(get = get_xyz, put = set_xyz)) float3 rgb;

			inline float3 get_xyz() const
			{
				return float3(x, y, z);
			}

			inline void set_xyz(const float3& _Value)
			{
				x  = _Value.x;
				y  = _Value.y;
				z  = _Value.z;
			}

			__declspec(property(get = get_xzy, put = set_xzy)) float3 xzy;
			__declspec(property(get = get_xzy, put = set_xzy)) float3 rbg;

			inline float3 get_xzy() const
			{
				return float3(x, z, y);
			}

			inline void set_xzy(const float3& _Value)
			{
				x  = _Value.x;
				z  = _Value.y;
				y  = _Value.z;
			}

			__declspec(property(get = get_yxz, put = set_yxz)) float3 yxz;
			__declspec(property(get = get_yxz, put = set_yxz)) float3 grb;

			inline float3 get_yxz() const
			{
				return float3(y, x, z);
			}
			inline void set_yxz(const float3& _Value)
			{
				y  = _Value.x;
				x  = _Value.y;
				z  = _Value.z;
			}

			__declspec(property(get = get_yzx, put = set_yzx)) float3 yzx;
			__declspec(property(get = get_yzx, put = set_yzx)) float3 gbr;

			inline float3 get_yzx() const
			{
				return float3(y, z, x);
			}
			inline void set_yzx(const float3& _Value)
			{
				y  = _Value.x;
				z  = _Value.y;
				x  = _Value.z;
			}

			__declspec(property(get = get_zxy, put = set_zxy)) float3 zxy;
			__declspec(property(get = get_zxy, put = set_zxy)) float3 brg;

			inline float3 get_zxy() const
			{
				return float3(z, x, y);
			}

			inline void set_zxy(const float3& _Value)
			{
				z  = _Value.x;
				x  = _Value.y;
				y  = _Value.z;
			}

			__declspec(property(get = get_zyx, put = set_zyx)) float3 zyx;
			__declspec(property(get = get_zyx, put = set_zyx)) float3 bgr;

			inline float3 get_zyx() const
			{
				return float3(z, y, x);
			}

			inline void set_zyx(const float3& _Value)
			{
				z  = _Value.x;
				y  = _Value.y;
				x  = _Value.z;
			}
#pragma endregion
		};

		PX_CUDA_CALLABLE static PX_FORCE_INLINE float3 operator *(float f, float3 _V)
		{
			return physx::operator*(f, _V);
		}

		//---------------------------------------------------------------------------------------------------------------------
		// 4 component vector
		//---------------------------------------------------------------------------------------------------------------------

		class float4 : public physx::PxVec4
		{
		public:
			static constexpr int Dim = 4;

			PX_CUDA_CALLABLE PX_FORCE_INLINE float4() {}
			PX_CUDA_CALLABLE PX_FORCE_INLINE float4(physx::PxZERO r) : physx::PxVec4(r)	{}
			explicit PX_CUDA_CALLABLE PX_FORCE_INLINE float4(physx::PxReal a) : physx::PxVec4(a) {}
			PX_CUDA_CALLABLE PX_FORCE_INLINE float4(physx::PxReal nx, physx::PxReal ny, physx::PxReal nz, physx::PxReal nw) : physx::PxVec4(nx, ny, nz, nw) {}
			PX_CUDA_CALLABLE PX_FORCE_INLINE float4(const physx::PxVec4& v) : physx::PxVec4(v) {}
			PX_CUDA_CALLABLE PX_FORCE_INLINE float4(const physx::PxVec3& v, const physx::PxReal& w) : physx::PxVec4(v.x, v.y, v.z, w) {}
			PX_CUDA_CALLABLE PX_FORCE_INLINE float4(const Helix::Math::XMFLOAT4& _v) : float4(_v.x, _v.y, _v.z, _v.w) { }
	
			PX_CUDA_CALLABLE PX_FORCE_INLINE float4& operator=(const float4& _v)
			{
				x = _v.x;
				y = _v.y;
				z = _v.z;
				w = _v.w;
				return *this;
			}

			PX_CUDA_CALLABLE PX_FORCE_INLINE float4 operator+(const float4& _v) const
			{
				return physx::PxVec4::operator+(_v);
			}

			PX_CUDA_CALLABLE PX_FORCE_INLINE float4 operator-(const float4& _v) const
			{
				return physx::PxVec4::operator-(_v);
			}

			// +
			PX_CUDA_CALLABLE PX_FORCE_INLINE float4 operator+(const float& _fOffset) const
			{
				return float4(x + _fOffset, y + _fOffset, z + _fOffset, w + _fOffset);
			}
			// -
			PX_CUDA_CALLABLE PX_FORCE_INLINE float4 operator-(const float& _fOffset) const
			{
				return float4(x - _fOffset, y - _fOffset, z - _fOffset, w - _fOffset);
			}

			// *
			PX_CUDA_CALLABLE PX_FORCE_INLINE float4 operator*(const float4& a) const
			{
				return multiply(a);
			}
			// scale
			PX_CUDA_CALLABLE PX_FORCE_INLINE float4 operator*(const float& _fScale) const
			{
				return float4(x * _fScale, y * _fScale, z * _fScale, w * _fScale);
			}

			PX_CUDA_CALLABLE PX_FORCE_INLINE float distance(const float4& _Other) const
			{
				return (*this - _Other).magnitude();
			}

			PX_CUDA_CALLABLE PX_FORCE_INLINE float distanceSquared(const float4& _Other) const
			{
				return (*this - _Other).magnitudeSquared();
			}

			PX_CUDA_CALLABLE PX_FORCE_INLINE float4 getSaturated() const 
			{
				float4 tmp;
				tmp.x = x < 0.0f ? 0.0f : x > 1.0f ? 1.0f : x;
				tmp.y = y < 0.0f ? 0.0f : y > 1.0f ? 1.0f : y;
				tmp.z = z < 0.0f ? 0.0f : z > 1.0f ? 1.0f : z;
				tmp.w = w < 0.0f ? 0.0f : w > 1.0f ? 1.0f : w;
				return tmp;
			}

			PX_CUDA_CALLABLE PX_FORCE_INLINE void saturated()
			{
				x = x < 0.0f ? 0.0f : x > 1.0f ? 1.0f : x;
				y = y < 0.0f ? 0.0f : y > 1.0f ? 1.0f : y;
				z = z < 0.0f ? 0.0f : z > 1.0f ? 1.0f : z;
				w = w < 0.0f ? 0.0f : w > 1.0f ? 1.0f : w;
			}

#pragma region SwizzleFloat4
			inline const float& get_x() const { return x; }
			inline const float& get_y() const { return y; }
			inline const float& get_z() const { return z; }
			inline const float& get_w() const { return w; }

			inline void set_x(const float& _x)  { x = _x; }
			inline void set_y(const float& _y) { y = _y; }
			inline void set_z(const float& _z) { z = _z; }
			inline void set_w(const float& _w) { w = _w; }
			
			__declspec(property(get = get_x, put = set_x)) float r;
			__declspec(property(get = get_y, put = set_y)) float g;
			__declspec(property(get = get_z, put = set_z)) float b;
			__declspec(property(get = get_w, put = set_w)) float a;

			__declspec(property(get = get_xy, put = set_xy)) float2 xy;
			__declspec(property(get = get_xy, put = set_xy)) float2 rg;

			inline float2 get_xy() const  {
				return float2(x, y);
			}

			inline void set_xy(const float2& _Value) 
			{				
				x  = _Value.x;
				y  = _Value.y;
			}

			__declspec(property(get = get_xz, put = set_xz)) float2 xz;
			__declspec(property(get = get_xz, put = set_xz)) float2 rb;

			inline float2 get_xz() const  {
				return float2(x, z);
			}

			inline void set_xz(const float2& _Value) 
			{				
				x  = _Value.x;
				z  = _Value.y;
			}

			__declspec(property(get = get_xw, put = set_xw)) float2 xw;
			__declspec(property(get = get_xw, put = set_xw)) float2 ra;

			inline float2 get_xw() const  {
				return float2(x, w);
			}

			inline void set_xw(const float2& _Value) 
			{				
				x  = _Value.x;
				w  = _Value.y;
			}

			__declspec(property(get = get_yx, put = set_yx)) float2 yx;
			__declspec(property(get = get_yx, put = set_yx)) float2 gr;

			inline float2 get_yx() const  {
				return float2(y, x);
			}

			inline void set_yx(const float2& _Value) 
			{
				
				y  = _Value.x;
				x  = _Value.y;
			}

			__declspec(property(get = get_yz, put = set_yz)) float2 yz;
			__declspec(property(get = get_yz, put = set_yz)) float2 gb;

			inline float2 get_yz() const  {
				return float2(y, z);
			}

			inline void set_yz(const float2& _Value) 
			{
				
				y  = _Value.x;
				z  = _Value.y;
			}

			__declspec(property(get = get_yw, put = set_yw)) float2 yw;
			__declspec(property(get = get_yw, put = set_yw)) float2 ga;

			inline float2 get_yw() const  {
				return float2(y, w);
			}

			inline void set_yw(const float2& _Value) 
			{				
				y  = _Value.x;
				w  = _Value.y;
			}

			__declspec(property(get = get_zx, put = set_zx)) float2 zx;
			__declspec(property(get = get_zx, put = set_zx)) float2 br;

			inline float2 get_zx() const  {
				return float2(z, x);
			}

			inline void set_zx(const float2& _Value) 
			{				
				z  = _Value.x;
				x  = _Value.y;
			}

			__declspec(property(get = get_zy, put = set_zy)) float2 zy;
			__declspec(property(get = get_zy, put = set_zy)) float2 bg;

			inline float2 get_zy() const  {
				return float2(z, y);
			}

			inline void set_zy(const float2& _Value) 
			{				
				z  = _Value.x;
				y  = _Value.y;
			}

			__declspec(property(get = get_zw, put = set_zw)) float2 zw;
			__declspec(property(get = get_zw, put = set_zw)) float2 ba;

			inline float2 get_zw() const  {
				return float2(z, w);
			}

			inline void set_zw(const float2& _Value) 
			{				
				z  = _Value.x;
				w  = _Value.y;
			}

			__declspec(property(get = get_wx, put = set_wx)) float2 wx;
			__declspec(property(get = get_wx, put = set_wx)) float2 ar;

			inline float2 get_wx() const  {
				return float2(w, x);
			}

			inline void set_wx(const float2& _Value) 
			{				
				w  = _Value.x;
				x  = _Value.y;
			}

			__declspec(property(get = get_wy, put = set_wy)) float2 wy;
			__declspec(property(get = get_wy, put = set_wy)) float2 ag;

			inline float2 get_wy() const  {
				return float2(w, y);
			}

			inline void set_wy(const float2& _Value) 
			{				
				w  = _Value.x;
				y  = _Value.y;
			}

			__declspec(property(get = get_wz, put = set_wz)) float2 wz;
			__declspec(property(get = get_wz, put = set_wz)) float2 ab;

			inline float2 get_wz() const  {
				return float2(w, z);
			}

			inline void set_wz(const float2& _Value) 
			{				
				w  = _Value.x;
				z  = _Value.y;
			}

			__declspec(property(get = get_xyz, put = set_xyz)) float3 xyz;
			__declspec(property(get = get_xyz, put = set_xyz)) float3 rgb;

			inline float3 get_xyz() const  {
				return float3(x, y, z);
			}

			inline void set_xyz(const float3& _Value) 
			{				
				x  = _Value.x;
				y  = _Value.y;
				z  = _Value.z;
			}

			__declspec(property(get = get_xyw, put = set_xyw)) float3 xyw;
			__declspec(property(get = get_xyw, put = set_xyw)) float3 rga;

			inline float3 get_xyw() const  {
				return float3(x, y, w);
			}

			inline void set_xyw(const float3& _Value) 
			{				
				x  = _Value.x;
				y  = _Value.y;
				w  = _Value.z;
			}

			__declspec(property(get = get_xzy, put = set_xzy)) float3 xzy;
			__declspec(property(get = get_xzy, put = set_xzy)) float3 rbg;

			inline float3 get_xzy() const  {
				return float3(x, z, y);
			}

			inline void set_xzy(const float3& _Value) 
			{
				
				x  = _Value.x;
				z  = _Value.y;
				y  = _Value.z;
			}

			__declspec(property(get = get_xzw, put = set_xzw)) float3 xzw;
			__declspec(property(get = get_xzw, put = set_xzw)) float3 rba;

			inline float3 get_xzw() const  {
				return float3(x, z, w);
			}

			inline void set_xzw(const float3& _Value) 
			{				
				x  = _Value.x;
				z  = _Value.y;
				w  = _Value.z;
			}

			__declspec(property(get = get_xwy, put = set_xwy)) float3 xwy;
			__declspec(property(get = get_xwy, put = set_xwy)) float3 rag;

			inline float3 get_xwy() const  {
				return float3(x, w, y);
			}

			inline void set_xwy(const float3& _Value) 
			{				
				x  = _Value.x;
				w  = _Value.y;
				y  = _Value.z;
			}

			__declspec(property(get = get_xwz, put = set_xwz)) float3 xwz;
			__declspec(property(get = get_xwz, put = set_xwz)) float3 rab;

			inline float3 get_xwz() const  {
				return float3(x, w, z);
			}

			inline void set_xwz(const float3& _Value) 
			{				
				x  = _Value.x;
				w  = _Value.y;
				z  = _Value.z;
			}

			__declspec(property(get = get_yxz, put = set_yxz)) float3 yxz;
			__declspec(property(get = get_yxz, put = set_yxz)) float3 grb;

			inline float3 get_yxz() const  {
				return float3(y, x, z);
			}

			inline void set_yxz(const float3& _Value) 
			{				
				y  = _Value.x;
				x  = _Value.y;
				z  = _Value.z;
			}

			__declspec(property(get = get_yxw, put = set_yxw)) float3 yxw;
			__declspec(property(get = get_yxw, put = set_yxw)) float3 gra;

			inline float3 get_yxw() const  {
				return float3(y, x, w);
			}

			inline void set_yxw(const float3& _Value) 
			{				
				y  = _Value.x;
				x  = _Value.y;
				w  = _Value.z;
			}

			__declspec(property(get = get_yzx, put = set_yzx)) float3 yzx;
			__declspec(property(get = get_yzx, put = set_yzx)) float3 gbr;

			inline float3 get_yzx() const  {
				return float3(y, z, x);
			}

			inline void set_yzx(const float3& _Value) 
			{				
				y  = _Value.x;
				z  = _Value.y;
				x  = _Value.z;
			}

			__declspec(property(get = get_yzw, put = set_yzw)) float3 yzw;
			__declspec(property(get = get_yzw, put = set_yzw)) float3 gba;

			inline float3 get_yzw() const  {
				return float3(y, z, w);
			}

			inline void set_yzw(const float3& _Value) 
			{
				y  = _Value.x;
				z  = _Value.y;
				w  = _Value.z;
			}

			__declspec(property(get = get_ywx, put = set_ywx)) float3 ywx;
			__declspec(property(get = get_ywx, put = set_ywx)) float3 gar;

			inline float3 get_ywx() const  {
				return float3(y, w, x);
			}

			inline void set_ywx(const float3& _Value) 
			{
				
				y  = _Value.x;
				w  = _Value.y;
				x  = _Value.z;
			}

			__declspec(property(get = get_ywz, put = set_ywz)) float3 ywz;
			__declspec(property(get = get_ywz, put = set_ywz)) float3 gab;

			inline float3 get_ywz() const  {
				return float3(y, w, z);
			}

			inline void set_ywz(const float3& _Value) 
			{				
				y  = _Value.x;
				w  = _Value.y;
				z  = _Value.z;
			}

			__declspec(property(get = get_zxy, put = set_zxy)) float3 zxy;
			__declspec(property(get = get_zxy, put = set_zxy)) float3 brg;

			inline float3 get_zxy() const  {
				return float3(z, x, y);
			}

			inline void set_zxy(const float3& _Value) 
			{				
				z  = _Value.x;
				x  = _Value.y;
				y  = _Value.z;
			}

			__declspec(property(get = get_zxw, put = set_zxw)) float3 zxw;
			__declspec(property(get = get_zxw, put = set_zxw)) float3 bra;

			inline float3 get_zxw() const  {
				return float3(z, x, w);
			}

			inline void set_zxw(const float3& _Value) 
			{				
				z  = _Value.x;
				x  = _Value.y;
				w  = _Value.z;
			}

			__declspec(property(get = get_zyx, put = set_zyx)) float3 zyx;
			__declspec(property(get = get_zyx, put = set_zyx)) float3 bgr;

			inline float3 get_zyx() const  {
				return float3(z, y, x);
			}

			inline void set_zyx(const float3& _Value) 
			{				
				z  = _Value.x;
				y  = _Value.y;
				x  = _Value.z;
			}

			__declspec(property(get = get_zyw, put = set_zyw)) float3 zyw;
			__declspec(property(get = get_zyw, put = set_zyw)) float3 bga;

			inline float3 get_zyw() const  {
				return float3(z, y, w);
			}

			inline void set_zyw(const float3& _Value) 
			{
				z  = _Value.x;
				y  = _Value.y;
				w  = _Value.z;
			}

			__declspec(property(get = get_zwx, put = set_zwx)) float3 zwx;
			__declspec(property(get = get_zwx, put = set_zwx)) float3 bar;

			inline float3 get_zwx() const  {
				return float3(z, w, x);
			}

			inline void set_zwx(const float3& _Value) 
			{				
				z  = _Value.x;
				w  = _Value.y;
				x  = _Value.z;
			}

			__declspec(property(get = get_zwy, put = set_zwy)) float3 zwy;
			__declspec(property(get = get_zwy, put = set_zwy)) float3 bag;

			inline float3 get_zwy() const  {
				return float3(z, w, y);
			}

			inline void set_zwy(const float3& _Value) 
			{				
				z  = _Value.x;
				w  = _Value.y;
				y  = _Value.z;
			}

			__declspec(property(get = get_wxy, put = set_wxy)) float3 wxy;
			__declspec(property(get = get_wxy, put = set_wxy)) float3 arg;

			inline float3 get_wxy() const  {
				return float3(w, x, y);
			}

			inline void set_wxy(const float3& _Value) 
			{				
				w  = _Value.x;
				x  = _Value.y;
				y  = _Value.z;
			}

			__declspec(property(get = get_wxz, put = set_wxz)) float3 wxz;
			__declspec(property(get = get_wxz, put = set_wxz)) float3 arb;

			inline float3 get_wxz() const  {
				return float3(w, x, z);
			}

			inline void set_wxz(const float3& _Value) 
			{				
				w  = _Value.x;
				x  = _Value.y;
				z  = _Value.z;
			}

			__declspec(property(get = get_wyx, put = set_wyx)) float3 wyx;
			__declspec(property(get = get_wyx, put = set_wyx)) float3 agr;

			inline float3 get_wyx() const  {
				return float3(w, y, x);
			}

			inline void set_wyx(const float3& _Value) 
			{				
				w  = _Value.x;
				y  = _Value.y;
				x  = _Value.z;
			}

			__declspec(property(get = get_wyz, put = set_wyz)) float3 wyz;
			__declspec(property(get = get_wyz, put = set_wyz)) float3 agb;

			inline float3 get_wyz() const  {
				return float3(w, y, z);
			}

			inline void set_wyz(const float3& _Value) 
			{				
				w  = _Value.x;
				y  = _Value.y;
				z  = _Value.z;
			}

			__declspec(property(get = get_wzx, put = set_wzx)) float3 wzx;
			__declspec(property(get = get_wzx, put = set_wzx)) float3 abr;

			inline float3 get_wzx() const  {
				return float3(w, z, x);
			}

			inline void set_wzx(const float3& _Value) 
			{				
				w  = _Value.x;
				z  = _Value.y;
				x  = _Value.z;
			}

			__declspec(property(get = get_wzy, put = set_wzy)) float3 wzy;
			__declspec(property(get = get_wzy, put = set_wzy)) float3 abg;

			inline float3 get_wzy() const  {
				return float3(w, z, y);
			}

			inline void set_wzy(const float3& _Value) 
			{				
				w  = _Value.x;
				z  = _Value.y;
				y  = _Value.z;
			}

			__declspec(property(get = get_xyzw, put = set_xyzw)) float4 xyzw;
			__declspec(property(get = get_xyzw, put = set_xyzw)) float4 rgba;

			inline float4 get_xyzw() const  {
				return float4(x, y, z, w);
			}

			inline void set_xyzw(const float4& _Value) 
			{				
				x  = _Value.x;
				y  = _Value.y;
				z  = _Value.z;
				w  = _Value.w;
			}

			__declspec(property(get = get_xywz, put = set_xywz)) float4 xywz;
			__declspec(property(get = get_xywz, put = set_xywz)) float4 rgab;

			inline float4 get_xywz() const  {
				return float4(x, y, w, z);
			}

			inline void set_xywz(const float4& _Value) 
			{				
				x  = _Value.x;
				y  = _Value.y;
				w  = _Value.z;
				z  = _Value.w;
			}

			__declspec(property(get = get_xzyw, put = set_xzyw)) float4 xzyw;
			__declspec(property(get = get_xzyw, put = set_xzyw)) float4 rbga;

			inline float4 get_xzyw() const  {
				return float4(x, z, y, w);
			}

			inline void set_xzyw(const float4& _Value) 
			{				
				x  = _Value.x;
				z  = _Value.y;
				y  = _Value.z;
				w  = _Value.w;
			}

			__declspec(property(get = get_xzwy, put = set_xzwy)) float4 xzwy;
			__declspec(property(get = get_xzwy, put = set_xzwy)) float4 rbag;

			inline float4 get_xzwy() const  {
				return float4(x, z, w, y);
			}

			inline void set_xzwy(const float4& _Value) 
			{				
				x  = _Value.x;
				z  = _Value.y;
				w  = _Value.z;
				y  = _Value.w;
			}

			__declspec(property(get = get_xwyz, put = set_xwyz)) float4 xwyz;
			__declspec(property(get = get_xwyz, put = set_xwyz)) float4 ragb;

			inline float4 get_xwyz() const  {
				return float4(x, w, y, z);
			}

			inline void set_xwyz(const float4& _Value) 
			{				
				x  = _Value.x;
				w  = _Value.y;
				y  = _Value.z;
				z  = _Value.w;
			}

			__declspec(property(get = get_xwzy, put = set_xwzy)) float4 xwzy;
			__declspec(property(get = get_xwzy, put = set_xwzy)) float4 rabg;

			inline float4 get_xwzy() const  {
				return float4(x, w, z, y);
			}

			inline void set_xwzy(const float4& _Value) 
			{				
				x  = _Value.x;
				w  = _Value.y;
				z  = _Value.z;
				y  = _Value.w;
			}

			__declspec(property(get = get_yxzw, put = set_yxzw)) float4 yxzw;
			__declspec(property(get = get_yxzw, put = set_yxzw)) float4 grba;

			inline float4 get_yxzw() const  {
				return float4(y, x, z, w);
			}

			inline void set_yxzw(const float4& _Value) 
			{
				y  = _Value.x;
				x  = _Value.y;
				z  = _Value.z;
				w  = _Value.w;
			}

			__declspec(property(get = get_yxwz, put = set_yxwz)) float4 yxwz;
			__declspec(property(get = get_yxwz, put = set_yxwz)) float4 grab;

			inline float4 get_yxwz() const  {
				return float4(y, x, w, z);
			}

			inline void set_yxwz(const float4& _Value) 
			{				
				y  = _Value.x;
				x  = _Value.y;
				w  = _Value.z;
				z  = _Value.w;
			}

			__declspec(property(get = get_yzxw, put = set_yzxw)) float4 yzxw;
			__declspec(property(get = get_yzxw, put = set_yzxw)) float4 gbra;

			inline float4 get_yzxw() const  {
				return float4(y, z, x, w);
			}

			inline void set_yzxw(const float4& _Value) 
			{				
				y  = _Value.x;
				z  = _Value.y;
				x  = _Value.z;
				w  = _Value.w;
			}

			__declspec(property(get = get_yzwx, put = set_yzwx)) float4 yzwx;
			__declspec(property(get = get_yzwx, put = set_yzwx)) float4 gbar;

			inline float4 get_yzwx() const  {
				return float4(y, z, w, x);
			}

			inline void set_yzwx(const float4& _Value) 
			{				
				y  = _Value.x;
				z  = _Value.y;
				w  = _Value.z;
				x  = _Value.w;
			}

			__declspec(property(get = get_ywxz, put = set_ywxz)) float4 ywxz;
			__declspec(property(get = get_ywxz, put = set_ywxz)) float4 garb;

			inline float4 get_ywxz() const  {
				return float4(y, w, x, z);
			}

			inline void set_ywxz(const float4& _Value) 
			{				
				y  = _Value.x;
				w  = _Value.y;
				x  = _Value.z;
				z  = _Value.w;
			}

			__declspec(property(get = get_ywzx, put = set_ywzx)) float4 ywzx;
			__declspec(property(get = get_ywzx, put = set_ywzx)) float4 gabr;
			
			inline float4 get_ywzx() const  {
				return float4(y, w, z, x);
			}

			inline void set_ywzx(const float4& _Value) 
			{				
				y  = _Value.x;
				w  = _Value.y;
				z  = _Value.z;
				x  = _Value.w;
			}
			
			__declspec(property(get = get_zxyw, put = set_zxyw)) float4 zxyw;
			__declspec(property(get = get_zxyw, put = set_zxyw)) float4 brga;

			inline float4 get_zxyw() const  {
				return float4(z, x, y, w);
			}

			inline void set_zxyw(const float4& _Value) 
			{				
				z  = _Value.x;
				x  = _Value.y;
				y  = _Value.z;
				w  = _Value.w;
			}
			
			__declspec(property(get = get_zxwy, put = set_zxwy)) float4 zxwy;
			__declspec(property(get = get_zxwy, put = set_zxwy)) float4 brag;

			inline float4 get_zxwy() const  {
				return float4(z, x, w, y);
			}

			inline void set_zxwy(const float4& _Value) 
			{				
				z  = _Value.x;
				x  = _Value.y;
				w  = _Value.z;
				y  = _Value.w;
			}

			__declspec(property(get = get_zyxw, put = set_zyxw)) float4 zyxw;
			__declspec(property(get = get_zyxw, put = set_zyxw)) float4 bgra;

			inline float4 get_zyxw() const  {
				return float4(z, y, x, w);
			}

			inline void set_zyxw(const float4& _Value) 
			{				
				z  = _Value.x;
				y  = _Value.y;
				x  = _Value.z;
				w  = _Value.w;
			}

			__declspec(property(get = get_zywx, put = set_zywx)) float4 zywx;
			__declspec(property(get = get_zywx, put = set_zywx)) float4 bgar;

			inline float4 get_zywx() const  {
				return float4(z, y, w, x);
			}

			inline void set_zywx(const float4& _Value) 
			{				
				z  = _Value.x;
				y  = _Value.y;
				w  = _Value.z;
				x  = _Value.w;
			}

			__declspec(property(get = get_zwxy, put = set_zwxy)) float4 zwxy;
			__declspec(property(get = get_zwxy, put = set_zwxy)) float4 barg;

			inline float4 get_zwxy() const  {
				return float4(z, w, x, y);
			}

			inline void set_zwxy(const float4& _Value) 
			{				
				z  = _Value.x;
				w  = _Value.y;
				x  = _Value.z;
				y  = _Value.w;
			}

			__declspec(property(get = get_zwyx, put = set_zwyx)) float4 zwyx;
			__declspec(property(get = get_zwyx, put = set_zwyx)) float4 bagr;

			inline float4 get_zwyx() const  {
				return float4(z, w, y, x);
			}
			
			inline void set_zwyx(const float4& _Value) 
			{				
				z  = _Value.x;
				w  = _Value.y;
				y  = _Value.z;
				x  = _Value.w;
			}

			__declspec(property(get = get_wxyz, put = set_wxyz)) float4 wxyz;
			__declspec(property(get = get_wxyz, put = set_wxyz)) float4 argb;

			inline float4 get_wxyz() const  {
				return float4(w, x, y, z);
			}
			inline void set_wxyz(const float4& _Value) 
			{				
				w  = _Value.x;
				x  = _Value.y;
				y  = _Value.z;
				z  = _Value.w;
			}

			__declspec(property(get = get_wxzy, put = set_wxzy)) float4 wxzy;
			__declspec(property(get = get_wxzy, put = set_wxzy)) float4 arbg;

			inline float4 get_wxzy() const  {
				return float4(w, x, z, y);
			}

			inline void set_wxzy(const float4& _Value) 
			{				
				w  = _Value.x;
				x  = _Value.y;
				z  = _Value.z;
				y  = _Value.w;
			}

			__declspec(property(get = get_wyxz, put = set_wyxz)) float4 wyxz;
			__declspec(property(get = get_wyxz, put = set_wyxz)) float4 agrb;

			inline float4 get_wyxz() const  {
				return float4(w, y, x, z);
			}

			inline void set_wyxz(const float4& _Value) 
			{				
				w  = _Value.x;
				y  = _Value.y;
				x  = _Value.z;
				z  = _Value.w;
			}

			__declspec(property(get = get_wyzx, put = set_wyzx)) float4 wyzx;
			__declspec(property(get = get_wyzx, put = set_wyzx)) float4 agbr;

			inline float4 get_wyzx() const  {
				return float4(w, y, z, x);
			}

			inline void set_wyzx(const float4& _Value) 
			{				
				w  = _Value.x;
				y  = _Value.y;
				z  = _Value.z;
				x  = _Value.w;
			}

			__declspec(property(get = get_wzxy, put = set_wzxy)) float4 wzxy;
			__declspec(property(get = get_wzxy, put = set_wzxy)) float4 abrg;

			inline float4 get_wzxy() const  {
				return float4(w, z, x, y);
			}

			inline void set_wzxy(const float4& _Value) 
			{				
				w  = _Value.x;
				z  = _Value.y;
				x  = _Value.z;
				y  = _Value.w;
			}

			__declspec(property(get = get_wzyx, put = set_wzyx)) float4 wzyx;
			__declspec(property(get = get_wzyx, put = set_wzyx)) float4 abgr;

			inline float4 get_wzyx() const  {
				return float4(w, z, y, x);
			}

			inline void set_wzyx(const float4& _Value) 
			{				
				w  = _Value.x;
				z  = _Value.y;
				y  = _Value.z;
				x  = _Value.w;
			}
#pragma endregion
		};

		//---------------------------------------------------------------------------------------------------------------------
		// 4 quaternion
		//---------------------------------------------------------------------------------------------------------------------

		class quaternion : public physx::PxQuat
		{
		public:
			static constexpr int Dim = 4;

			PX_CUDA_CALLABLE PX_FORCE_INLINE quaternion() : physx::PxQuat(physx::PxIDENTITY::PxIdentity) {	}
			PX_CUDA_CALLABLE PX_INLINE quaternion(physx::PxIDENTITY r) : physx::PxQuat(r){}
			explicit PX_CUDA_CALLABLE PX_FORCE_INLINE quaternion(physx::PxReal r) : physx::PxQuat(r) {}

			PX_CUDA_CALLABLE PX_FORCE_INLINE quaternion(physx::PxReal nx, physx::PxReal ny, physx::PxReal nz, physx::PxReal nw) : physx::PxQuat(nx, ny, nz, nw) {}
			PX_CUDA_CALLABLE PX_INLINE quaternion(physx::PxReal angleRadians, const physx::PxVec3& unitAxis) : physx::PxQuat(angleRadians, unitAxis) {}
			PX_CUDA_CALLABLE PX_FORCE_INLINE quaternion(const physx::PxQuat& v) : physx::PxQuat(v){}
			PX_CUDA_CALLABLE PX_FORCE_INLINE quaternion(const Math::XMFLOAT4& v) : physx::PxQuat(v.x, v.y, v.z, v.w) {}

			PX_CUDA_CALLABLE PX_INLINE explicit quaternion(const physx::PxMat33& m) : physx::PxQuat(m) {}

			PX_CUDA_CALLABLE PX_INLINE quaternion operator*(const quaternion& q) const
			{
				return physx::PxQuat::operator*(q);
			}
			PX_CUDA_CALLABLE PX_INLINE float3 operator*(const float3& _Other) const
			{
				return rotate(_Other);
			}

			// X Y Z order counter-clockwise
			inline float3 toEulerRadians() const
			{
				float ysqr = y * y;
				float t0 = -2.0f * (ysqr + z * z) + 1.0f;
				float t1 = +2.0f * (x * y - w * z);
				float t2 = -2.0f * (x * z + w * y);
				float t3 = +2.0f * (y * z - w * x);
				float t4 = -2.0f * (x * x + ysqr) + 1.0f;

				t2 = t2 > 1.0f ? 1.0f : t2;
				t2 = t2 < -1.0f ? -1.0f : t2;
				
				float pitch = -std::asinf(t2); // Y
				float roll = -std::atan2f(t3, t4); // X
				float yaw = -std::atan2f(t1, t0); // Z
				return float3(roll, pitch, yaw);
			}
		};

		//---------------------------------------------------------------------------------------------------------------------
		// 4x4 Matrix
		//---------------------------------------------------------------------------------------------------------------------

		class float4x4 : public physx::PxMat44
		{
		public:
			PX_CUDA_CALLABLE PX_INLINE float4x4(){}
			PX_CUDA_CALLABLE PX_INLINE float4x4(const physx::PxMat44& m) : physx::PxMat44(m) {}
			PX_CUDA_CALLABLE PX_INLINE float4x4(physx::PxIDENTITY r) : physx::PxMat44(r) {}
			PX_CUDA_CALLABLE PX_INLINE float4x4(physx::PxZERO r) : physx::PxMat44(r) {}

			//! Construct from four 4-vectors
			PX_CUDA_CALLABLE float4x4(const physx::PxVec4& col0, const physx::PxVec4& col1, const physx::PxVec4& col2, const physx::PxVec4 &col3)
				: physx::PxMat44(col0, col1, col2, col3) {}

			//! constructor that generates a multiple of the identity matrix
			explicit PX_CUDA_CALLABLE PX_INLINE float4x4(physx::PxReal r) : physx::PxMat44(r) {}

			//! Construct from three base vectors and a translation
			PX_CUDA_CALLABLE float4x4(const physx::PxVec3& col0, const physx::PxVec3& col1, const physx::PxVec3& col2, const physx::PxVec3& col3)
				: physx::PxMat44(col0, col1, col2, col3) {}

			//! Construct from float[16]
			explicit PX_CUDA_CALLABLE PX_INLINE float4x4(physx::PxReal values[]) : physx::PxMat44(values){}

			//! Construct from a quaternion
			explicit PX_CUDA_CALLABLE PX_INLINE float4x4(const physx::PxQuat& q) : physx::PxMat44(q) {}

			//! Construct from a diagonal vector
			explicit PX_CUDA_CALLABLE PX_INLINE float4x4(const physx::PxVec4& diagonal) : physx::PxMat44(diagonal) {}

			PX_CUDA_CALLABLE float4x4(const physx::PxMat33& orientation, const physx::PxVec3& position) : physx::PxMat44(orientation, position) {}

			PX_CUDA_CALLABLE float4x4(const physx::PxTransform& t) : physx::PxMat44(t) {};

			PX_CUDA_CALLABLE PX_INLINE float4x4 getInverse() const
			{
				XMMATRIX m1 = XMLoadFloat4x4(this); // transpose and load to m1
				m1 = XMMatrixInverse(nullptr, m1);

				float4x4 Result;
				XMStoreFloat4x4(&Result, m1);
				return Result;
			}
		};

		//---------------------------------------------------------------------------------------------------------------------
		// 4x4 Matrix
		//---------------------------------------------------------------------------------------------------------------------
		struct B8G8R8A8_UNorm
		{
			B8G8R8A8_UNorm(const uint32_t& _bgra = 0) : bgra(_bgra){ }
			// ORDER: R G B A 
			B8G8R8A8_UNorm(const float& _r, const float& _g,const float& _b, const float& _a) 
				:	b(static_cast<uint8_t>(255.f * _b)),
					g(static_cast<uint8_t>(255.f * _g)),
					r(static_cast<uint8_t>(255.f * _r)),
					a(static_cast<uint8_t>(255.f * _a)) {}

			union
			{
				struct
				{
					uint8_t b;
					uint8_t g;
					uint8_t r;
					uint8_t a;
				};
				uint32_t bgra;
			};
		};

#define HFLOAT2_ZERO {0.f,0.f}
#define HFLOAT3_ZERO {0.f,0.f,0.f}
#define HFLOAT4_ZERO {0.f,0.f,0.f,0.f}
#define HFLOAT2_ONE {1.f,1.f}
#define HFLOAT3_ONE {1.f,1.f,1.f}
#define HFLOAT4_ONE {1.f,1.f,1.f,1.f}
#define HQUATERNION_IDENTITY  {0.f,0.f,0.f,1.f}
#define HTRANSFORM_IDENTITY  Math::transform(physx::PxIdentity)
#define HFLOAT3x3_IDENTITY { {1.f,0.f,0.f}, {0.f,1.f,0.f}, {0.f,0.f,1.f}}
#define HFLOAT4x3_IDENTITY { {1.f,0.f,0.f,0.f}, {0.f,1.f,0.f,0.f}, {0.f,0.f,1.f,0.f}}
#define HFLOAT4x4_IDENTITY { {1.f,0.f,0.f,0.f}, {0.f,1.f,0.f,0.f}, {0.f,0.f,1.f,0.f}, {0.f,0.f,0.f,1.f}}

		//=================================
		// Conversion to DirectXMath
		//=================================
#pragma region Conversion
		//-----------------------------------------------------------------------------------------------
		inline XMFLOAT2 Convert(const float2& _f) { return XMFLOAT2(_f.x, _f.y); }
		//-----------------------------------------------------------------------------------------------
		inline XMFLOAT3 Convert(const float3& _f) { return XMFLOAT3(_f.x, _f.y, _f.z); }
		//-----------------------------------------------------------------------------------------------
		inline XMFLOAT4 Convert(const float4& _f) { return XMFLOAT4(_f.x, _f.y, _f.z, _f.w); }
		//-----------------------------------------------------------------------------------------------
		inline XMFLOAT4 Convert(const quaternion& _f) { return XMFLOAT4(_f.x, _f.y, _f.z, _f.w); }
		//-----------------------------------------------------------------------------------------------
		inline XMFLOAT3X3 Convert(const float3x3& _f)
		{	// column to row major
			return XMFLOAT3X3(
				_f.column0.x, _f.column0.y, _f.column0.z,
				_f.column1.x, _f.column1.y, _f.column1.z,
				_f.column2.x, _f.column2.y, _f.column2.z);
		}
		//-----------------------------------------------------------------------------------------------
		inline XMFLOAT4X4 Convert(const float4x4& _f)
		{	// column to row major
			return XMFLOAT4X4(
				_f.column0.x, _f.column0.y, _f.column0.z, _f.column0.w,
				_f.column1.x, _f.column1.y, _f.column1.z, _f.column1.w,
				_f.column2.x, _f.column2.y, _f.column2.z, _f.column2.w,
				_f.column3.x, _f.column3.y, _f.column3.z, _f.column3.w);
		}
		//-----------------------------------------------------------------------------------------------
		inline XMFLOAT4X3 Convert(const float3x4& _f)
		{	// column to row major
			return XMFLOAT4X3(
				_f.base0.x, _f.base0.y, _f.base0.z,
				_f.base1.x, _f.base1.y, _f.base1.z,
				_f.base2.x, _f.base2.y, _f.base2.z,
				_f.base3.x, _f.base3.y, _f.base3.z);
		}
#pragma endregion

		//=================================
		// SSE4 Storing and loading
		//=================================
#pragma region float2
		//-----------------------------------------------------------------------------------------------
		_Use_decl_annotations_
			inline void XM_CALLCONV XMStoreFloat2(float2* pDestination, FXMVECTOR  V)
		{
			XMVECTOR T = XM_PERMUTE_PS(V, _MM_SHUFFLE(1, 1, 1, 1));
			_mm_store_ss(&pDestination->x, V);
			_mm_store_ss(&pDestination->y, T);
		}
		//-----------------------------------------------------------------------------------------------
		_Use_decl_annotations_
			inline XMVECTOR XM_CALLCONV XMLoadFloat2(const float2* pSource)
		{
			__m128 x = _mm_load_ss(&pSource->x);
			__m128 y = _mm_load_ss(&pSource->y);
			return _mm_unpacklo_ps(x, y);
		}
#pragma endregion

#pragma region float3
		//-----------------------------------------------------------------------------------------------
		_Use_decl_annotations_
			inline void XM_CALLCONV XMStoreFloat3(float3* pDestination, FXMVECTOR  V)
		{
			XMVECTOR T1 = XM_PERMUTE_PS(V, _MM_SHUFFLE(1, 1, 1, 1));
			XMVECTOR T2 = XM_PERMUTE_PS(V, _MM_SHUFFLE(2, 2, 2, 2));
			_mm_store_ss(&pDestination->x, V);
			_mm_store_ss(&pDestination->y, T1);
			_mm_store_ss(&pDestination->z, T2);
		}
		//-----------------------------------------------------------------------------------------------
		_Use_decl_annotations_
			inline XMVECTOR XM_CALLCONV XMLoadFloat3(const float3* pSource)
		{
			__m128 x = _mm_load_ss(&pSource->x);
			__m128 y = _mm_load_ss(&pSource->y);
			__m128 z = _mm_load_ss(&pSource->z);
			__m128 xy = _mm_unpacklo_ps(x, y);
			return _mm_movelh_ps(xy, z);
		}
#pragma endregion

#pragma region float4
		//-----------------------------------------------------------------------------------------------
		_Use_decl_annotations_
			inline void XM_CALLCONV XMStoreFloat4(float4* pDestination, FXMVECTOR  V)
		{
			_mm_storeu_ps(&pDestination->x, V);
		}
		//-----------------------------------------------------------------------------------------------
		_Use_decl_annotations_
			inline XMVECTOR XM_CALLCONV XMLoadFloat4(const float4* pSource)
		{
			return _mm_set_ps(pSource->w, pSource->z, pSource->y, pSource->x);
		}
#pragma endregion

#pragma region quaternion
		//-----------------------------------------------------------------------------------------------
		_Use_decl_annotations_
			inline void XM_CALLCONV XMStoreQuaternion(quaternion* pDestination, FXMVECTOR  V)
		{
			_mm_storeu_ps(&pDestination->x, V);
		}
		//-----------------------------------------------------------------------------------------------
		_Use_decl_annotations_
			inline XMVECTOR XM_CALLCONV XMLoadQuaternion(const quaternion* pSource)
		{
			return _mm_set_ps(pSource->w, pSource->z, pSource->y, pSource->x);
		}

		//-----------------------------------------------------------------------------------------------
		_Use_decl_annotations_
			inline void XM_CALLCONV XMStoreQuaternion(physx::PxQuat* pDestination, FXMVECTOR  V)
		{
			_mm_storeu_ps(&pDestination->x, V);
		}
		//-----------------------------------------------------------------------------------------------
		_Use_decl_annotations_
			inline XMVECTOR XM_CALLCONV XMLoadQuaternion(const physx::PxQuat* pSource)
		{
			return _mm_set_ps(pSource->w, pSource->z, pSource->y, pSource->x);
		}
#pragma endregion

#pragma region float3x3
		//-----------------------------------------------------------------------------------------------
		_Use_decl_annotations_
			inline XMMATRIX XM_CALLCONV XMLoadFloat3x3(const float3x3* pSource)
		{
			const XMFLOAT3X3&& M = Convert(*pSource);
			return DirectX::XMLoadFloat3x3(&M);
		}
		//-----------------------------------------------------------------------------------------------
		_Use_decl_annotations_
			inline void XM_CALLCONV XMStoreFloat3x3(float3x3* pDestination, FXMMATRIX M)
		{
			XMMATRIX MT = DirectX::XMMatrixTranspose(M);
			DirectX::XMStoreFloat3x3((XMFLOAT3X3*)pDestination, MT);
		}
#pragma endregion

#pragma region float4x3
		//-----------------------------------------------------------------------------------------------
		_Use_decl_annotations_
			inline XMMATRIX XM_CALLCONV XMLoadFloat4x3(const float3x4* pSource)
		{
			XMMATRIX M;
			// Use unaligned load instructions to 
			// load the 12 floats
			M.r[0] = _mm_loadu_ps(&(pSource->base0.x));
			M.r[1] = _mm_loadu_ps(&(pSource->base1.y));
			M.r[2] = _mm_loadu_ps(&(pSource->base2.z));
			return M;
		}
		//-----------------------------------------------------------------------------------------------
		_Use_decl_annotations_
			inline void XM_CALLCONV XMStoreFloat4x3(float3x4* pDestination, FXMMATRIX M)
		{
			_mm_storeu_ps(&pDestination->base0.x, M.r[0]);
			_mm_storeu_ps(&pDestination->base1.y, M.r[1]);
			_mm_storeu_ps(&pDestination->base2.z, M.r[2]);
		}
#pragma endregion

#pragma region float4x4
		//-----------------------------------------------------------------------------------------------
		_Use_decl_annotations_
			inline XMMATRIX XM_CALLCONV XMLoadFloat4x4(const float4x4* pSource)
		{
			XMMATRIX M;
			M.r[0] = _mm_loadu_ps(&pSource->column0.x);
			M.r[1] = _mm_loadu_ps(&pSource->column1.x);
			M.r[2] = _mm_loadu_ps(&pSource->column2.x);
			M.r[3] = _mm_loadu_ps(&pSource->column3.x);
			return M;
		}
		//-----------------------------------------------------------------------------------------------
		_Use_decl_annotations_
			inline void XM_CALLCONV XMStoreFloat4x4(float4x4* pDestination, Helix::Math::FXMMATRIX M)
		{
			_mm_storeu_ps(&pDestination->column0.x, M.r[0]);
			_mm_storeu_ps(&pDestination->column1.x, M.r[1]);
			_mm_storeu_ps(&pDestination->column2.x, M.r[2]);
			_mm_storeu_ps(&pDestination->column3.x, M.r[3]);
		}
#pragma endregion
	} // Math
} // Helix

#endif // MATHTYPES_H