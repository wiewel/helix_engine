//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef PERMUTATIONCOMPILER_H
#define PERMUTATIONCOMPILER_H

#include "hlx\src\StandardDefines.h"
#include "IncludeInterfaceDX11.h"
#include "ShaderDocument.h"
#include "Resources\ShaderCompilationResult.h"

#include <concurrent_unordered_set.h>

namespace Helix
{
	namespace Compiler
	{	
		using TProgressCallback = std::function<void(uint32_t, uint32_t)>;

		enum CompilationErrorType
		{
			CompilationErrorType_None = 0,
			CompilationErrorType_Warning,
			CompilationErrorType_Error
		};

		struct CompilationErrorInfo
		{
			CompilationErrorType kType = CompilationErrorType_None;
			std::string sPermutationName;
			std::string sFunction;
			std::string sMessage;
		};

		using TErrorReport = concurrency::concurrent_vector<CompilationErrorInfo>;

		struct PermutationMap
		{
			using TRangePermuations = concurrency::concurrent_unordered_set<uint64_t>;
			using TBasePermutations = concurrency::concurrent_unordered_map<uint64_t, TRangePermuations>;
			using TFunctionsPermutations = concurrency::concurrent_vector<TBasePermutations>;

			// returns true if the permutation did not exist
			inline bool insert(const size_t& _uFunctionIndex, const uint64_t& _uBasePermutation, const uint64_t& _uRangePermutation)
			{
				TBasePermutations& Permutations = m_Permutations.at(_uFunctionIndex);
				TBasePermutations::iterator range = Permutations.find(_uBasePermutation);

				if (range != Permutations.end())
				{
					return range->second.insert(_uRangePermutation).second;
				}
				else
				{
					TRangePermuations r; r.insert(_uRangePermutation);
					Permutations.insert({ _uBasePermutation , std::move(r) });
					return true;
				}
			}

			inline void resize(const size_t& _uFunctionIndex)
			{
				m_Permutations.resize(_uFunctionIndex);
			}

			inline void clear()
			{
				m_Permutations.clear();
			}

		private:
			TFunctionsPermutations m_Permutations;
		};

		class PermutationCompiler
		{
		public:
			HDEBUGNAME("PermutationCompiler");

			PermutationCompiler(const TProgressCallback& _ProgressCallback = nullptr, bool _bParallel = true);
			~PermutationCompiler();

			bool Compile(const ShaderDocument& _Shader);

			const Resources::ShaderCompilationResult& GetCompilationResult() const;
			Resources::ShaderCompilationResult& GetCompilationResult();

			
			const TErrorReport& GetErrorReport() const;

		private:
			bool CompilePermutation(const uint32_t& _uPermutation, const uint32_t& _uIOPermutation,
				const ShaderDocument& _Shader, const Display::ShaderFunction& _Function, const size_t& _uFunctionIndex,
				_Out_ std::atomic_bool& _bSkipRange, std::vector<uint8_t> _RangeIndices, const uint32_t& uCurRange);

			void GetPermutations(const ShaderDocument& _Shader, const Display::ShaderType& _kShaderType, _Out_ uint32_t& _uMaxPermutation, _Out_ uint32_t& _uShaderDefineMask) const;

			void GetDefines(const ShaderDocument& _Shader, const Display::ShaderType& _kShaderType, uint32_t _uMaxPermutation,
				_Out_ std::vector<std::string>& _Macros, _Out_ std::string& _sPermutationName) const;

			bool SkipPermutation(
				const ShaderDocument& _Shader,
				const Display::ShaderType& _kShaderType,
				const uint32_t& _uShaderPermutationMask,				
				const uint32_t& _uPermutation,
				_Out_ uint32_t& _uPermutationResult) const;

		private:
			Resources::ShaderCompilationResult m_Result;

			TErrorReport m_Errors;
			PermutationMap m_CompiledPermutations;

			bool m_bParallel = true;
			TProgressCallback m_ProgressCallback = nullptr;
		};

		inline const Resources::ShaderCompilationResult& PermutationCompiler::GetCompilationResult() const	{ return m_Result; }
		inline Resources::ShaderCompilationResult& PermutationCompiler::GetCompilationResult() { return m_Result; }
		inline const TErrorReport& PermutationCompiler::GetErrorReport() const{return m_Errors;	}
	} //Compiler
} // Helix

#endif // PERMUTATIONCOMPILER_H
