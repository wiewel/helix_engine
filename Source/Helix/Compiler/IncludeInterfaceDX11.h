//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef INCLUDEINTERFACEDX11_H
#define INCLUDEINTERFACEDX11_H

#include <d3dcommon.h>
#include <set>
#include <string>
#include <unordered_map>

namespace Helix
{
	namespace Compiler
	{
		class IncludeInterfaceDX11 : public ID3DInclude
		{
			friend class ShaderDocument;
			using TOpenFileMap = std::unordered_map<std::string, std::string>;
		public:
			IncludeInterfaceDX11();
			~IncludeInterfaceDX11();

			HRESULT __stdcall Open(D3D_INCLUDE_TYPE IncludeType, LPCSTR pFileName, LPCVOID pParentData, LPCVOID *ppData, UINT *pBytes);
			HRESULT __stdcall Close(LPCVOID pData);

			void AddIncludeDirectory(const std::string& _sDir);
			void ClearIncludeDirectories();

			// clears all the file map
			void Reset();

			void InsertShaderText(const std::string& _sFileName, const std::string& _sShaderText);
		private:
			//returns a pointer to the data or nullptr if the file was not found or could not be opened. _bNew will be set to true if the file is new in the database
			const std::string* Find(const std::string& _sFileName, bool& _bNew);

		private:
			std::set<std::string> m_IncludeDirectories;

			std::mutex  m_Mutex;

			TOpenFileMap m_OpenIncludeFiles;
		};
	} // Compiler
} // Helix





#endif