//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "ShaderDocument.h"
#include "Resources\FileSystem.h"
#include "hlx\src\Logger.h"
#include "hlx\src\StringHelpers.h"

using namespace Helix;
using namespace Helix::Compiler;

//---------------------------------------------------------------------------------------------------
ShaderDocument::ShaderDocument(const hlx::string& _sFilePath) :
	m_sFilePath(_sFilePath)
{
	m_sFileName = Resources::FileSystem::GetFileName(_sFilePath);

	hlx::fbytestream FStream;
	if (Resources::FileSystem::Instance()->OpenFileStream(_sFilePath, std::ios::in, FStream))
	{
		Resources::FileSystem::Instance()->GetFullPath(_sFilePath, m_sFilePath);
		m_sCode = FStream.get<std::string>(FStream.size());
		FStream.close();
		HLOG("Successfully loaded %s [%.2fkb]", m_sFileName.c_str(), (float)m_sCode.size() / 1024.f);
	}
}
//---------------------------------------------------------------------------------------------------
ShaderDocument::~ShaderDocument()
{
}
//---------------------------------------------------------------------------------------------------
void ShaderDocument::AddShaderFunction(const Display::ShaderFunction& _Function)
{
	m_Functions.push_back(_Function);
}
//---------------------------------------------------------------------------------------------------
void ShaderDocument::Parse()
{
	// remove comments to avoid including commented headers
	RemoveComments();

	// expand (include) headers
	ExpandHeaders();

	// remove all comments from the include files
	RemoveComments();

	// remove unused shader functions
	RemoveUnusedShaderFunctions();

	// find permutations and skip empty ones
	GatherPermutationDefines();

	// evaluate permutation enums
	GatherPermutationEnums();

	// gather ranges
	GatherRanges();

	// wrap functions into include guards
	GuardFunctions();

	// filter combinations of exclusive permutations
	BuildIgnoreMasks();

	HLOG("%s: [%.2fkb]", m_sFileName.c_str(), (float)m_sCode.size() / 1024.f);

	return;
}
//---------------------------------------------------------------------------------------------------
const  Display::ShaderType ShaderDocument::GetShaderTypeFromDefine(const std::string& _sDefine) const
{
	if (hlx::starts_with(_sDefine, "HPMIO"))
	{
		return Display::ShaderType_NumOf;
	}
	else if (hlx::starts_with(_sDefine, "HPMVS"))
	{
		return Display::ShaderType_VertexShader;
	}
	else if (hlx::starts_with(_sDefine, "HPMHS"))
	{
		return Display::ShaderType_HullShader;
	}
	else if (hlx::starts_with(_sDefine, "HPMDS"))
	{
		return Display::ShaderType_DomainShader;
	}
	else if (hlx::starts_with(_sDefine, "HPMGS"))
	{
		return Display::ShaderType_GeometryShader;
	}
	else if (hlx::starts_with(_sDefine, "HPMPS"))
	{
		return Display::ShaderType_PixelShader;
	}
	else if (hlx::starts_with(_sDefine, "HPMCS"))
	{
		return Display::ShaderType_ComputeShader;
	}

	return Display::ShaderType_None;
}
//---------------------------------------------------------------------------------------------------
void ShaderDocument::RemoveComments()
{
	size_t uStart = m_sCode.find("//", 0);
	size_t uLength = m_sCode.find("\n", uStart) - uStart;

	for(;uStart != std::string::npos && uLength != std::string::npos;
		 uStart = m_sCode.find("//", uStart), uLength = m_sCode.find("\n", uStart) - uStart)
	{
		m_sCode.erase(uStart, uLength);
	}

	uStart = 0;
	size_t uEnd = 0;

	while (hlx::get_body(m_sCode, uStart, uEnd, "/*", "*/"))
	{
		m_sCode.erase(uStart, uEnd-uStart+4);
	}

	// remove unnecessary new line feeds
	uStart = m_sCode.find("\n\n", 0);
	
	while (uStart != std::string::npos)
	{
		for (uLength = 1; uStart + uLength < m_sCode.size() && m_sCode[uStart + uLength] == '\n'; ++uLength) {}

		if (uLength > 1)
		{
			m_sCode.erase(uStart, uLength-1);
		}		

		uStart = m_sCode.find("\n\n", uStart);
	}
}
//---------------------------------------------------------------------------------------------------
void ShaderDocument::RemoveUnusedShaderFunctions()
{
	auto itr = std::remove_if(m_Functions.begin(),m_Functions.end(),[&](const Display::ShaderFunction& _LambdaFunction) { return m_sCode.find(_LambdaFunction.sFunctionName) == std::string::npos; });

	if (itr != m_Functions.end())
	{
		m_Functions.erase(itr,m_Functions.end());
	}
}
//---------------------------------------------------------------------------------------------------
void ShaderDocument::ExpandHeaders()
{
	Resources::FileSystem* pFS = Resources::FileSystem::Instance();
	// store which headers has been expanded to avoid double/ring includes
	std::unordered_map<std::string, bool> HeadersIncluded;
	size_t uIncludePos = 0;

	while (uIncludePos != std::string::npos)
	{
		uIncludePos = m_sCode.find("#include", uIncludePos);

		if (uIncludePos == std::string::npos)
		{
			break;
		}

		size_t uIncludeOpen = m_sCode.find_first_of("\"<", uIncludePos + 1);

		if (uIncludeOpen == std::string::npos)
		{
			// malformed code
			break;
		}

		size_t uIncludeClose = m_sCode.find_first_of("\">", uIncludeOpen + 1);

		if (uIncludeClose == std::string::npos)
		{
			// malformed code
			break;
		}

		std::string sIncludePath = m_sCode.substr(uIncludeOpen + 1, uIncludeClose - uIncludeOpen - 1);
		if (hlx::ends_with(sIncludePath, ".hlsli") == false)
		{
			// normal .h include, ignore
			uIncludePos = m_sCode.find("#include", uIncludePos + 1);
			continue;
		}

		if (HeadersIncluded.count(sIncludePath) != 0)
		{
			// header has already be expanded, remove it
			m_sCode.erase(uIncludePos, uIncludeClose - uIncludePos + 1);
			uIncludePos = m_sCode.find("#include", uIncludePos + 1);
			continue;
		}

		// new include		
		// add to included headers, even if it could not be opened
		HeadersIncluded[sIncludePath] = true;

		// remove #include <blabal.h>
		m_sCode.erase(uIncludePos, uIncludeClose - uIncludePos + 1);

		hlx::fbytestream IncludeStream;
		if (pFS->OpenFileStream(STR(sIncludePath), std::ios::in, IncludeStream))
		{
			std::string sIncludeInput = IncludeStream.get<std::string>(IncludeStream.size());
			IncludeStream.close();

			hlx::remove(sIncludeInput, '\0');
			m_sCode.insert(uIncludePos, sIncludeInput);
		}

		uIncludePos = m_sCode.find("#include", uIncludePos + 1);
	}
}
//---------------------------------------------------------------------------------------------------
void ShaderDocument::GatherPermutationDefines()
{
	size_t uStart = 0;
	size_t uStartPrev = 0;
	size_t uEnd = 0;

	std::string sPermuationContext;
	while (hlx::get_body(m_sCode, uStart, uEnd, "#if", "#endif"))
	{
		size_t uNewLine = m_sCode.find("\n", uStart);

		// length of define name (skip # and \n;
		size_t uLength = uNewLine - uStart - 1;
		std::string sPermDefine = m_sCode.substr(uStart + 1, uLength);
		hlx::remove_whitespaces(sPermDefine);

		if (hlx::starts_with(sPermDefine, "ifdefined("))
		{
			hlx::remove(sPermDefine, "ifdefined(");
			hlx::remove(sPermDefine, ")");
		}
		//if (hlx::starts_with(sPermDefine, "elifdefined("))
		//{
		//	hlx::remove(sPermDefine, "elifdefined(");
		//	hlx::remove(sPermDefine, ")");
		//}
		else if (hlx::starts_with(sPermDefine, "ifdef"))
		{
			hlx::remove(sPermDefine, "ifdef");
		}
		else if (hlx::starts_with(sPermDefine, "ifndef"))
		{
			hlx::remove(sPermDefine, "ifndef");
		}
		else
		{
			uStart = uNewLine + 1;
			continue;
		}

		const Display::ShaderType kShaderIndex = GetShaderTypeFromDefine(sPermDefine);

		// skip unknown permutation defines
		if (kShaderIndex > Display::ShaderType_NumOf)
		{
			uStart = uNewLine + 1;
			continue;
		}

		//length of define content (code)
		uLength = uEnd - (uNewLine + 1);

		sPermuationContext = m_sCode.substr(uNewLine + 1, uLength);
		hlx::remove_whitespaces(sPermuationContext);

		// remove HPM#S_
		std::string sPermString = sPermDefine.substr(6);

		if (sPermuationContext.size() < 2)
		{
			// empty define
			HLOG("Skipped Permutation: %s", CSTR(sPermDefine));
			m_sCode.erase(uStart, uEnd + 6 - uStart);

			// remove falsely inserted permutation
			m_AvailableDefines[kShaderIndex].erase(sPermString);

			uStart = uStartPrev;

			continue;
		}

		//HLOGD("Permutation: %s", sPermDefine.c_str());
		m_AvailableDefines[kShaderIndex].insert({ sPermString, sPermDefine });

		uStartPrev = uStart;
		uStart = uNewLine + 1;
	}
}
//---------------------------------------------------------------------------------------------------
void ShaderDocument::GatherPermutationEnums()
{
	// parse permutation enums

	size_t uEnumPos = m_sCode.find("enum");

	while (uEnumPos != std::string::npos)
	{
		size_t uOpenBracket = m_sCode.find('{', uEnumPos);

		if (uOpenBracket == std::string::npos)
		{
			// invalid c++ code
			break;
		}

		std::string sEnum = m_sCode.substr(uEnumPos + 4, uOpenBracket - uEnumPos - 4);
		hlx::remove_whitespaces(sEnum);

		Display::ShaderType kShaderIndex = Display::ShaderType_None;

		// check for valid permutation define
		if (hlx::starts_with(sEnum, "HPM") == false ||
			(kShaderIndex = GetShaderTypeFromDefine(sEnum)) == Display::ShaderType_None)
		{
			// not the correct enum
			uEnumPos = m_sCode.find("enum", uOpenBracket);
			continue;
		}

		size_t uCloseBracket = m_sCode.find('}', uOpenBracket);

		if (uCloseBracket == std::string::npos)
		{
			break;
		}

		sEnum = m_sCode.substr(uOpenBracket + 1, uCloseBracket - uOpenBracket - 1);
		hlx::remove_whitespaces(sEnum);

		// entries could be in one line: HPMShaderXYZ_Perm1 = 1 << 2, HPMShaderXYZ_Perm2 = 1 << 3 ...
		std::vector<std::string> EnumEntries = hlx::split(sEnum, ',');
		for (const std::string& sEnumEntry : EnumEntries)
		{
			std::vector<std::string> KeyValuePair = hlx::split(sEnumEntry, '=');
			if (KeyValuePair.size() != 2)
			{
				HERROR("Malformed HPM enum ('=' missing ?)");
				continue;
			}

			uint32_t uValue = 0;

			// parse 1 << 2 etc
			std::vector<std::string> Values = hlx::split(KeyValuePair[1], "<<");
			if (Values.size() == 2)
			{
				uint32_t uLeft = std::stoul(Values[0]);
				uint32_t uRight = std::stoul(Values[1]);
				uValue = uLeft << uRight;
			}
			else
			{
				uValue = std::stoull(KeyValuePair[1]);
			}

			HLOG("%s: %u", CSTR(KeyValuePair[0]), uValue);
			// check if value is power of two and therefore a valid permutation flag
			if ((uValue != 0) && !(uValue & (uValue - 1)))
			{
				m_PermutationMap[kShaderIndex][uValue] = KeyValuePair[0].substr(6);
			}
			else
			{
				HERROR("%s is not a valid permutation flag (power of 2)", CSTR(KeyValuePair[0]));
				continue;
			}
		}

		uEnumPos = m_sCode.find("enum", uCloseBracket);
	}
}
//---------------------------------------------------------------------------------------------------
void ShaderDocument::GatherRanges()
{
	size_t uRangePos = m_sCode.find("RANGE");

	while (uRangePos != std::string::npos)
	{
		// RANGE(RangeNameString, StartUInt, StepUInt, CountUInt) e.g. RANGE(PointLightRange, 0, 2, 8)
		size_t uOpen = m_sCode.find('(', uRangePos + 1);
		size_t uClose = m_sCode.find(')', uOpen);

		if (uOpen != std::string::npos &&
			uClose != std::string::npos)
		{
			std::string sRangeParameters = m_sCode.substr(uOpen+1, uClose - uOpen - 1); // dont include RANGE ()
			hlx::remove_whitespaces(sRangeParameters);

			std::vector<std::string> Parameters = hlx::split(sRangeParameters, ',');

			if (Parameters.size() == 4u)
			{
				const std::string& sRangeName = Parameters[0];
				// check if the range is actually used later in the code
				if (m_sCode.find(sRangeName, uClose) != std::string::npos)
				{
					Range NewRange = {};
					NewRange.sDefine = sRangeName;

					NewRange.uStart = std::stoul(Parameters[1]);
					NewRange.uStep = std::stoul(Parameters[2]);
					NewRange.uCount = std::stoul(Parameters[3]);

					if (NewRange.uCount == 0 || NewRange.uStep == 0 || sRangeName.empty())
					{
						HERROR("Invalid (empty) range %s", CSTR(sRangeName));
					}
					else
					{
						HLOG("Range %s [%u-%u]", CSTR(sRangeName), NewRange.uStart, NewRange.uStart + (NewRange.uCount - 1) * NewRange.uStep);

						m_Ranges.push_back(NewRange);
					}
				}
				else
				{
					HWARNING("Unused range %s", CSTR(sRangeName));
				}
			}
			else
			{
				HERROR("Invalid number of range parameters! [%s]", CSTR(sRangeParameters));
			}			
		}

		// remove RANGE(Name, 1,2,3)
		m_sCode.erase(uRangePos, uClose - uRangePos + 1);

		uRangePos = m_sCode.find("RANGE", uRangePos +1);
	}
}
//---------------------------------------------------------------------------------------------------
void ShaderDocument::GuardFunctions()
{
	for (const Display::ShaderFunction& Func : m_Functions)
	{
		size_t uFuncNamePos = m_sCode.find(Func.sFunctionName);
		size_t uFuncStartPos = m_sCode.rfind('\n', uFuncNamePos);

		// skip user function guards
		while (uFuncNamePos != std::string::npos && uFuncNamePos > 0u && m_sCode[uFuncNamePos-1] == '_')
		{
			uFuncNamePos = m_sCode.find(Func.sFunctionName, uFuncNamePos + 1);
		}

		uFuncStartPos = m_sCode.rfind('\n', uFuncNamePos);
		if (uFuncNamePos == std::string::npos || uFuncStartPos == std::string::npos)
		{
			HERROR("Unable to find entry point for %s", CSTR(Func.sFunctionName));
			continue;
		}

		size_t uFuncOpen = uFuncNamePos;
		size_t uFuncClose = uFuncNamePos;

		if (hlx::get_body(m_sCode, uFuncOpen, uFuncClose) == false)
		{
			HERROR("Unable to find exit point for %s", CSTR(Func.sFunctionName));
			continue;
		}

		// include semantics like [maxvertexcount(4)] in the function guard
		size_t uSemanticStartPos = m_sCode.rfind('\n', uFuncStartPos-1u);
		while (uSemanticStartPos != std::string::npos)
		{
			const std::string sLine = m_sCode.substr(uSemanticStartPos+1, uFuncStartPos - uSemanticStartPos-1);
			if (sLine.find('[') != std::string::npos)
			{
				uFuncStartPos = uSemanticStartPos;
				uSemanticStartPos = m_sCode.rfind('\n', uFuncStartPos - 1u);
			}
			else
			{
				break;
			}
		}

		// insert close guard first to not corrupt uFuncStartPos
		const std::string sCloseGuard = "\n#endif // _" + Func.sFunctionName + "_";
		m_sCode.insert(uFuncClose + 1, sCloseGuard);

		const std::string sOpenGuard = "\n#ifdef _" + Func.sFunctionName + "_\n";
		m_sCode.insert(uFuncStartPos + 1, sOpenGuard);
	}
}
//---------------------------------------------------------------------------------------------------
void ShaderDocument::BuildIgnoreMasks()
{
	for (uint32_t i = 0u; i < Display::ShaderType_NumOf + 1; ++i)
	{
		const TPermutationMap& PermMap = m_PermutationMap[i];
		std::vector<uint32_t> ExclusivePerms;
		for (const TPermutationMap::value_type Perm : PermMap)
		{
			// check for exclusive flag
			if (hlx::ends_with(Perm.second, "Excl", false))
			{
				ExclusivePerms.push_back(Perm.first);
			}
		}

		// exclusive permutations will only be combined with non exclusive permutations
		// if thers no other exclusive permutation, no maks is needed
		if (ExclusivePerms.size() < 2u)
		{
			continue;
		}

		for (const uint32_t& uExclPerm : ExclusivePerms)
		{
			IgnoreMask Mask;
			Mask.uConditionBits = uExclPerm;
			for (const uint32_t& uIgnore : ExclusivePerms)
			{
				if (uIgnore != uExclPerm)
				{
					Mask.uIgnoreBits |= uIgnore;
				}
			}

			m_IgnoreMasks[i].push_back(Mask);
		}
	}	
}
//---------------------------------------------------------------------------------------------------