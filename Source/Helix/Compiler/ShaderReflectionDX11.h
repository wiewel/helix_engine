//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef SHADERREFLECTIONDX11_H
#define SHADERREFLECTIONDX11_H

#include <d3d11shader.h>
#include "hlx\src\StandardDefines.h"

#include "Resources\InputLayoutSerializer.h"
#include "Resources\ShaderInterfaceSerialzer.h"

namespace Helix
{
	// forward declaration
	namespace Display
	{
		class ShaderCBufferDesc;
		class ShaderVariableDesc;
	}

	namespace Compiler
	{
		class CodeAnalyzer;
		class ShaderReflectionDX11
		{
		public:
			HDEBUGNAME("ShaderReflectionDX11");

			ShaderReflectionDX11(ID3DBlob* _pShaderBlob, CodeAnalyzer& _CodeAnalyzer);
			~ShaderReflectionDX11();

			inline explicit operator bool() const { return m_pShaderReflection != nullptr; }

			bool GetInputLayout(_Out_ Resources::InputLayoutSerializer& _Layout);
			bool GetOutputLayout(_Out_ Resources::InputLayoutSerializer& _Layout);

			bool GetInterface(_Out_ Resources::ShaderInterfaceSerializer& _InterfaceSerializer);

		private:
			bool GetBuffer(uint32_t _uSlot,_Out_ Display::ShaderBufferDesc& _CBuffer);

			bool GetVariableTypes(Display::ShaderVariableDesc& _ParentVariable, ID3D11ShaderReflectionType* pVarType);

		private:
			CodeAnalyzer& m_CodeAnalyzer;
			std::string m_sCode;
			ID3D11ShaderReflection* m_pShaderReflection = nullptr;

			D3D11_SHADER_DESC m_ShaderDesc = {};
		};

	}// Compiler
} // Helix

#endif // SHADERREFLECTIONDX11_H