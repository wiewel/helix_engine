//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef CODEANALYZER_H
#define CODEANALYZER_H

#include "hlx\src\StandardDefines.h"
#include "hlx\src\Logger.h"
#include "Display\ViewDefines.h"
#include <unordered_map>

namespace Helix
{
	namespace Compiler
	{
		using TInputMap = std::unordered_map<std::string, std::vector<std::string>>;

		class CodeAnalyzer
		{
		public:
			HDEBUGNAME("CodeAnalyzer");
			CodeAnalyzer(const std::string& _sCode, Display::ShaderType _kStage);
			~CodeAnalyzer();

			bool Parse();

			const std::string& GetCode() const;

			bool GetInputVariableNameForSemantic(const std::string& _sSemanticName, _Out_ std::string& _sVarName, uint32_t _uIndex);
			bool GetOutputVariableNameForSemantic(const std::string& _sSemanticName, _Out_ std::string& _sVarName, uint32_t _uIndex);

		private:
			const Display::ShaderType m_kStage;

			TInputMap m_InputMap;
			TInputMap m_OutputMap;

			std::string m_sCode;
		};

		inline const std::string& CodeAnalyzer::GetCode() const	{ return m_sCode; }
		
	} // Compiler
} // Helix
#endif // !CODEANALYZER_H
