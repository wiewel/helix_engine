//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "PermutationCompiler.h"
#include "ShaderReflectionDX11.h"
#include "Resources\FileSystem.h"
#include "hlx\src\StringHelpers.h"
#include <D3DCompiler.h>
#include <bitset>
#include "CodeAnalyzer.h"
#include "Util\CRC32.h"

using namespace Helix::Compiler;
using namespace Helix::Resources;
using namespace Helix::Display;

//---------------------------------------------------------------------------------------------------
PermutationCompiler::PermutationCompiler(const TProgressCallback& _ProgressCallback, bool _bParallel) :
	m_ProgressCallback(_ProgressCallback), m_bParallel(_bParallel)
{
}
//---------------------------------------------------------------------------------------------------
PermutationCompiler::~PermutationCompiler()
{
}
//---------------------------------------------------------------------------------------------------
inline uint32_t GetHighestBit(uint32_t _uIn)
{
	_uIn |= (_uIn >> 1);
	_uIn |= (_uIn >> 2);
	_uIn |= (_uIn >> 4);
	_uIn |= (_uIn >> 8);
	_uIn |= (_uIn >> 16);
	return _uIn - (_uIn >> 1);
}
//---------------------------------------------------------------------------------------------------
bool PermutationCompiler::Compile(const ShaderDocument& _Shader)
{
	m_CompiledPermutations.clear();
	m_Result.Reset();
	m_Result.SetShaderName(hlx::to_sstring(FileSystem::GetFileNameWithoutExtension(_Shader.GetFileName())));

	uint32_t uTotalPermutations = 0u;
	std::atomic_bool bFailed = false;

	// general input output signature permutation
	uint32_t uIOShaderDefineMask = 0u;
	uint32_t uIOMaxPermutation = 0u;

	GetPermutations(_Shader, Display::ShaderType_NumOf, uIOMaxPermutation, uIOShaderDefineMask);
	m_Result.SetPermutationMask(uIOShaderDefineMask, Display::ShaderType_NumOf);

	std::atomic<uint32_t> uPermutationCount = 0u;

	auto CompileProgress = [&](const uint32_t& _uPermutation, const uint32_t& _uIOPermutation,
		const ShaderDocument& _Shader, const Display::ShaderFunction& _Function, const size_t& _uFunctionIndex,
		_Out_ std::atomic_bool& _bSkipRange, std::vector<uint8_t> _RangeIndices, const uint32_t& uCurRange) -> bool
	{
		if (CompilePermutation(_uPermutation, _uIOPermutation, _Shader, _Function, _uFunctionIndex, _bSkipRange, _RangeIndices, uCurRange) == false)
		{
			bFailed.store(true);
			return false;
		}

		if (m_ProgressCallback != nullptr)
		{
			m_ProgressCallback(++uPermutationCount, uTotalPermutations);
		}

		return true;
	};

	const std::vector<Range>& Ranges = _Shader.GetRanges();
	const uint32_t uNumOfRanges = static_cast<uint32_t>(Ranges.size());
	
	uint32_t uTotalRangeCount = 1;
	for (const Range& Range : Ranges)
	{
		uTotalRangeCount *= Range.uCount;
	}

	size_t uFunctionIndex = 0u;
	m_CompiledPermutations.resize(_Shader.GetFunctions().size());

	for (const ShaderFunction& Function : _Shader.GetFunctions())
	{
		m_Result.AddFunction(Function);
		concurrency::concurrent_vector<bool> SkippedRanges(Ranges.size(), false);

		const ShaderType kShaderType = Display::ShaderModelVersions[Function.kTargetVersion].kType;
		// built ShaderDefineMask to check afterwards if permutation needs to be computed or can be skipped because it is not part of the shader
		
		uint32_t uClassShaderDefineMask = 0u;
		uint32_t uMaxClassPermutation = 0u;

		GetPermutations(_Shader, kShaderType, uMaxClassPermutation, uClassShaderDefineMask);
		m_Result.SetPermutationMask(uClassShaderDefineMask, kShaderType);

		const uint32_t uCurrentPermutationCount = uMaxClassPermutation * uIOMaxPermutation * uTotalRangeCount;
		uTotalPermutations += uCurrentPermutationCount;

		// TODO: RANGE MASK!

		HLOG("----------------------------------------------------");
		HLOG("Compiling %u %s permutations", uCurrentPermutationCount, CSTR(Function.sFunctionName));
		HLOG("ClassDefineMask:\t%s", CSTR(std::bitset<32>(uClassShaderDefineMask).to_string()));
		HLOG("IODefineMask:\t\t%s", CSTR(std::bitset<32>(uIOShaderDefineMask).to_string()));

		// IOPermutation
		for (uint32_t uIOP = 0; uIOP < uIOMaxPermutation; ++uIOP)
		{
			uint32_t uIOPermutation = 0;
			if (SkipPermutation(_Shader, Display::ShaderType_NumOf, uIOShaderDefineMask, uIOP, uIOPermutation))
			{
				continue;
			}

			// class permutation wrapper function
			auto CompileClassPermutation = [&](const uint32_t& uCP) -> bool
			{
				uint32_t uClassPermutation = 0;
				if (SkipPermutation(_Shader, kShaderType, uClassShaderDefineMask, uCP, uClassPermutation))
				{
					return true;
				}

				auto CompileRangePermutation = [&]() -> bool
				{
					// initialize indices to 0 {0,0,0,0...}
					std::vector<uint8_t> RangeIndicies = std::vector<uint8_t>(Ranges.size(), 0u);

					auto CompileRangeRecursion = [&](const uint32_t _uRangeIndex, std::vector<uint8_t> _RangeIndices, const auto& fn) -> bool
					{
						const Range& Range = Ranges.at(_uRangeIndex);
						const uint8_t uRangeCount = std::max(1u, Range.uCount);

						std::atomic_bool bSkipRange = false;
						auto CompileRangeIndice = [&](const uint8_t r) -> bool
						{
							if (bSkipRange.load() == true)
							{
								return true;
							}
							else if (bFailed.load() == true)
							{
								return false;
							}

							// copy vector for parallel execution
							std::vector<uint8_t> RangeIndicesCopy(_RangeIndices);
							// increment current range index
							RangeIndicesCopy.at(_uRangeIndex) = r;

							if (CompileProgress(uClassPermutation, uIOPermutation, _Shader, Function, uFunctionIndex, bSkipRange, RangeIndicesCopy, _uRangeIndex) == false)
							{
								// compilation failed
								return false;
							}

							return true;
						};

						// execution policy
						if (m_bParallel)
						{
							concurrency::parallel_for(0u, static_cast<uint32_t>(uRangeCount), CompileRangeIndice);						
						}
						else
						{
							for (uint8_t r = 0; r < uRangeCount; ++r)
							{
								if (CompileRangeIndice(r) == false)
								{
									return false;
								}

								if (bSkipRange.load() == true)
								{
									break;
								}
							}
						}

						if (bSkipRange.load())
						{
							_RangeIndices.at(_uRangeIndex) = 0u;
							SkippedRanges.at(_uRangeIndex) = true;

							// next level
							if (_uRangeIndex + 1 < uNumOfRanges)
							{
								// recursive indirection
								if (fn(_uRangeIndex + 1, _RangeIndices, fn) == false)
								{
									return false;
								}
							}							
						}
						else
						{
							// recurse
							for (uint8_t r = 0; r < uRangeCount; ++r)
							{
								_RangeIndices.at(_uRangeIndex) = r;

								// next level
								if (_uRangeIndex + 1 < uNumOfRanges)
								{
									// recursive indirection
									if (fn(_uRangeIndex + 1, _RangeIndices, fn) == false)
									{
										return false;
									}
								}
							}
						}

						return true;
					};

					// initiate recursion
					return CompileRangeRecursion(0u, RangeIndicies, CompileRangeRecursion);
				};

				// RangePermutation
				if (Ranges.empty())
				{
					std::atomic_bool _bSkipRange = false;
					return CompileProgress(uClassPermutation, uIOPermutation, _Shader, Function, uFunctionIndex, _bSkipRange, {}, 0u);
				}
				else
				{
					return CompileRangePermutation();
				}
			};

			// execution policy
			if (m_bParallel)
			{
				concurrency::parallel_for(0u, uMaxClassPermutation, CompileClassPermutation);
			}
			else
			{
				//ClassPermutation
				for (uint32_t uCP = 0; uCP < uMaxClassPermutation; ++uCP)
				{
					if (CompileClassPermutation(uCP) == false)
					{
						return false;
					}
				}
			}	
		}
		uFunctionIndex++;

		for (uint32_t uRange = 0u; uRange < Ranges.size(); ++uRange)
		{
			if (SkippedRanges[uRange])
			{
				m_Result.PrunePermutationsWithRange(Function.GetHash(), uRange);
			}
		}
	}

	HLOG("----------------------------------------------------");
	m_Result.PrintStats(uTotalPermutations);

	return bFailed.load() == false;
}
//---------------------------------------------------------------------------------------------------
bool PermutationCompiler::CompilePermutation(
	const uint32_t& _uPermutation,
	const uint32_t& _uIOPermutation,
	const ShaderDocument& _Shader,
	const ShaderFunction& _Function,
	const size_t& _uFunctionIndex,
	_Out_ std::atomic_bool& _bSkipRange,
	std::vector<uint8_t> _RangeIndices,
	const uint32_t& _uCurRange)
{
	//_bSkipRange = false;

	HLXSLPermutation Permutation = {};
	Permutation.Hash = ShaderPermutationHash(_uPermutation, _uIOPermutation, _RangeIndices);

	CompilationErrorInfo ErrorInfo;
	ErrorInfo.kType = CompilationErrorType_None;
	ErrorInfo.sFunction = _Function.sFunctionName;
	
	const Display::ShaderModelDesc& ModelDesc = Display::ShaderModelVersions[_Function.kTargetVersion];
	const std::vector<Range>& Ranges = _Shader.GetRanges();
	bool bHasRanges = _RangeIndices.empty() == false;

	if (_RangeIndices.empty() == false)
	{
		HASSERT(Ranges.size() == _RangeIndices.size(), "Invalid number of RangeIndices!");
	}

	std::vector<std::string> StringMacros = { "HLSL" };

	std::string sPermutationName;

	// add input output signature permutation
	if (_uIOPermutation != 0u)
	{
		GetDefines(_Shader, Display::ShaderType_NumOf, _uIOPermutation, StringMacros, sPermutationName);
	}

	// add shader function permutation
	if (_uPermutation != 0u)
	{
		GetDefines(_Shader, ModelDesc.kType, _uPermutation, StringMacros, sPermutationName);
	}

	if (sPermutationName.empty())
	{
		sPermutationName = "NULL ";
	}

	ErrorInfo.sPermutationName = sPermutationName;

	std::vector<D3D_SHADER_MACRO> Macros;

	for (const std::string& sDefine : StringMacros)
	{
		Macros.push_back({ sDefine.c_str(), "1" });
	}

	std::vector<std::string> sRangeStrings(_RangeIndices.size());
	std::vector<std::string> sRangeValidStrings(_RangeIndices.size());

	std::string sRangeValues;

	// include guard
	const std::string sFunctionGuard = "_" + _Function.sFunctionName + "_";
	Macros.push_back({ sFunctionGuard.c_str(), "1" });

	// add terminator
	Macros.push_back({ NULL, NULL });

	ID3DBlob* pShaderBlob = nullptr;
	ID3DBlob* pErrorBlob = nullptr;

	const std::string& sCode = _Shader.GetCode();

	const TCompilerFlags& kComplationFlags = _Shader.GetCompilationFlags();
	const bool bDebugShader = kComplationFlags & CompilerFlag_Debug;

	const std::string sShaderPath = hlx::to_sstring((bDebugShader && _Shader.GetParsedFilePath().empty() == false) ? _Shader.GetParsedFilePath() : _Shader.GetFilePath());

	// preproccess permutations
	if (FAILED(D3DPreprocess(sCode.c_str(), sCode.size(),
		sShaderPath.empty() ? NULL : sShaderPath.c_str(),
		&Macros[0], D3D_COMPILE_STANDARD_FILE_INCLUDE,
		&pShaderBlob, &pErrorBlob)))
	{
		if (pErrorBlob != nullptr)
		{
			ErrorInfo.kType = CompilationErrorType_Error;
			ErrorInfo.sMessage = static_cast<char*>(pErrorBlob->GetBufferPointer());
			m_Errors.push_back(ErrorInfo);

			HERROR("%s %s:\n%s", CSTR(_Function.sFunctionName), CSTR(sPermutationName), CSTR((char*)pErrorBlob->GetBufferPointer()));
			pErrorBlob->Release();
		}

		HSAFE_RELEASE(pShaderBlob);
		return false;
	}

	const std::string sPreprocessedCode((char*)pShaderBlob->GetBufferPointer(), pShaderBlob->GetBufferSize());

	HSAFE_RELEASE(pShaderBlob);
	HSAFE_RELEASE(pErrorBlob);

	if (bHasRanges)
	{
		// remove terminator
		Macros.pop_back();
	}

	// Add Range macro definitons
	for (uint32_t i = 0u; i < _RangeIndices.size(); ++i)
	{
		const Range& Range = Ranges.at(i);
		sRangeStrings[i] = std::to_string(Range.uStart + Range.uStep * _RangeIndices.at(i));
		sRangeValidStrings[i] = Range.sDefine + "VALID";
		sRangeValues += sRangeStrings[i] + ' ';
		Macros.push_back({ Range.sDefine.c_str() , sRangeStrings.at(i).c_str() });
		Macros.push_back({ sRangeValidStrings.at(i).c_str(), nullptr });
	}

	// add terminator
	Macros.push_back({ NULL, NULL });

	bool bSkipRangeLocal = false;
	// check if the range is actually in the evaluated code
	if (bHasRanges)
	{
		sPermutationName += sRangeValues;
		ErrorInfo.sPermutationName = sPermutationName;

		const Range& Range = Ranges.at(_uCurRange);
		// range is valid but not included in permutations original code text
		bSkipRangeLocal = sPreprocessedCode.find(Range.sDefine) == std::string::npos;
	}
	else // range is invalid
	{
		bSkipRangeLocal = true;
		// compile null permutation
	}

	if (bSkipRangeLocal)
	{
		_bSkipRange.store(true);
	}

	// try to skip range if its not the null permutation
	if (_bSkipRange.load() && bHasRanges && _RangeIndices.at(_uCurRange) != 0u)
	{
		return true;
	}

	// check if this permutation has been compiled before, but after range skipping has been checked
	if (m_CompiledPermutations.insert(_uFunctionIndex, Permutation.Hash.uBasePermutation, Permutation.Hash.GetRangeHash()) == false)
	{
		return true;
	}

	// compile permutation
	if (FAILED(D3DCompile(
		bDebugShader ? sCode.c_str() : sPreprocessedCode.c_str(),
		bDebugShader ? sCode.size() : sPreprocessedCode.size(),
		sShaderPath.empty() ? NULL : sShaderPath.c_str(),
		&Macros[0], D3D_COMPILE_STANDARD_FILE_INCLUDE,
		_Function.sFunctionName.c_str(), ModelDesc.pVersionString,
		kComplationFlags.GetFlag(), 0u,
		&pShaderBlob, &pErrorBlob)))
	{
		if (pErrorBlob != nullptr)
		{
			ErrorInfo.kType = CompilationErrorType_Error;
			ErrorInfo.sMessage = static_cast<char*>(pErrorBlob->GetBufferPointer());
			m_Errors.push_back(ErrorInfo);

			HERROR("%s %s:\n%s", CSTR(_Function.sFunctionName), CSTR(sPermutationName), CSTR((char*)pErrorBlob->GetBufferPointer()));
			pErrorBlob->Release();
		}

		HSAFE_RELEASE(pShaderBlob);
		return false;
	}

	if (pErrorBlob != nullptr)
	{
		ErrorInfo.kType = CompilationErrorType_Warning;
		ErrorInfo.sMessage = static_cast<char*>(pErrorBlob->GetBufferPointer());
		m_Errors.push_back(ErrorInfo);
	}

	HSAFE_RELEASE(pErrorBlob);

	CodeAnalyzer Analyzer(sPreprocessedCode, ShaderModelVersions[_Function.kTargetVersion].kType);

	Analyzer.Parse();

	// get shader reflection	
	ShaderReflectionDX11 Reflector(pShaderBlob, Analyzer);
	
	// failed to initialize reflector
	if (!Reflector)
	{
		HSAFE_RELEASE(pShaderBlob);
		return false;
	}

	// strip unnecessary information from the shader and reduce size (only in optimized mode)
	if (bDebugShader == false)
	{
		ID3DBlob* pStrippedShaderBlob = nullptr;
		uint32_t uFlags = D3DCOMPILER_STRIP_REFLECTION_DATA | D3DCOMPILER_STRIP_TEST_BLOBS | D3DCOMPILER_STRIP_PRIVATE_DATA | D3DCOMPILER_STRIP_DEBUG_INFO;

		if (FAILED(D3DStripShader(pShaderBlob->GetBufferPointer(), pShaderBlob->GetBufferSize(), uFlags, &pStrippedShaderBlob)))
		{
			ErrorInfo.kType = CompilationErrorType_Error;
			ErrorInfo.sMessage = "Failed to strip shader " + _Function.sFunctionName + ": " + sPermutationName;
			m_Errors.push_back(ErrorInfo);

			HERROR("Failed to strip shader %s: %s", CSTR(_Function.sFunctionName), CSTR(sPermutationName));
			HSAFE_RELEASE(pShaderBlob);
			return false;
		}

		HSAFE_RELEASE(pShaderBlob);
		pShaderBlob = pStrippedShaderBlob;
	}

	uint32_t uCodeSize = 0u;
	uint32_t uCompressedCodeSize = 0u;
	// keep code only in this context
	{
		hlx::bytes Code((uint8_t*)pShaderBlob->GetBufferPointer(), pShaderBlob->GetBufferSize());
		HSAFE_RELEASE(pShaderBlob);

		uCodeSize = static_cast<uint32_t>(Code.size());

		// store code
		Permutation.uUncompressedCodeSize = uCodeSize;

		Permutation.uCodeHash = m_Result.AddCode(std::move(Code), uCompressedCodeSize);
	}
	
	// get input layout of this shader
	{
		InputLayoutSerializer Layout;
		hlx::bytes LayoutBuffer;
		hlx::bytestream LayoutStream(LayoutBuffer);

		if (Reflector.GetInputLayout(Layout))
		{
			Layout.Write(LayoutStream);
			// store input layout
			Permutation.uInputLayoutHash = m_Result.AddLayout(std::move(LayoutBuffer));
		}
		else
		{
			return false;
		}
	}

	// get output layout of this shader
	{
		InputLayoutSerializer Layout;
		hlx::bytes LayoutBuffer;
		hlx::bytestream LayoutStream(LayoutBuffer);

		if (Reflector.GetOutputLayout(Layout))
		{
			Layout.Write(LayoutStream);
			// store output layout
			Permutation.uOutputLayoutHash = m_Result.AddLayout(std::move(LayoutBuffer));
		}
		else
		{
			return false;
		}
	}

	// serialize shader interface (variables , textures etc)
	{		
		ShaderInterfaceSerializer Interface = {};
		if (Reflector.GetInterface(Interface))
		{
			// check if there are variables in the interface
			if (Interface.HasMembers())	
			{
				hlx::bytes InterfaceBuffer;
				hlx::bytestream InterfaceStream(InterfaceBuffer);

				Interface.Write(InterfaceStream);
				Permutation.uInterfaceHash = m_Result.AddInterface(std::move(InterfaceBuffer));
			}			
		}
		else
		{
			return false;
		}
	}

	m_Result.AddPermutation(_Function, Permutation);

	uint64_t uRangeHash = Permutation.Hash.GetRangeHash();

	if (bSkipRangeLocal && bHasRanges)
	{
		const Range& Range = Ranges.at(_uCurRange);
		HLOG("%s P%u IO%u %s [%u->%uByte %X] %llu SKIPPING %s",
			CSTR(_Function.sFunctionName),
			_uPermutation,
			_uIOPermutation,
			CSTR(sPermutationName),
			uCodeSize,
			uCompressedCodeSize,
			Permutation.uCodeHash,
			uRangeHash,
			CSTR(Range.sDefine));
	}
	else
	{
		HLOG("%s P%u IO%u %s [%u->%uByte %X] %llu",
			CSTR(_Function.sFunctionName),
			_uPermutation,
			_uIOPermutation,
			CSTR(sPermutationName),
			uCodeSize,
			uCompressedCodeSize,
			Permutation.uCodeHash,
			uRangeHash);
	}

	return true;
}
//---------------------------------------------------------------------------------------------------

void PermutationCompiler::GetPermutations(const ShaderDocument& _Shader, const ShaderType& _kShaderType, uint32_t & _uMaxPermutation, uint32_t& _uShaderDefineMask) const
{
	const TPermutationMap PermutationMap = _Shader.GetPermutationMap(_kShaderType);
	const TDefineMap AvailableDefines = _Shader.GetAvailableDefines(_kShaderType);

	// built ShaderDefineMask to check afterwards if permutation needs to be computed or can be skipped because it is not part of the shader
	_uMaxPermutation = 0;
	_uShaderDefineMask = 0;

	// perm id -> perm string
	for (const TPermutationMap::value_type& it : PermutationMap)
	{
		// perm string -> perm define
		if (AvailableDefines.count(it.second) > 0)
		{
			_uMaxPermutation = std::max(_uMaxPermutation, it.first);
			_uShaderDefineMask |= it.first; // permutation is available
		}
	}

	if(_uMaxPermutation == 0u)
	{
		_uMaxPermutation = 1;
	}
	else
	{
		_uMaxPermutation <<= 1;
	}
}
//---------------------------------------------------------------------------------------------------
void PermutationCompiler::GetDefines(const ShaderDocument& _Shader, const ShaderType& _kShaderType, uint32_t _uMaxPermutation, _Out_ std::vector<std::string>& _Macros, _Out_ std::string& _sPermutationName) const
{
	const TPermutationMap PermutationMap = _Shader.GetPermutationMap(_kShaderType);
	const TDefineMap AvailableDefines = _Shader.GetAvailableDefines(_kShaderType);

	const uint32_t uMSB = GetHighestBit(_uMaxPermutation) << 1;

	// construct macro list
	for (uint32_t i = 1; i < uMSB; i <<= 1)
	{
		// check for each permuation flag
		if (_uMaxPermutation & i)
		{
			const auto& pit = PermutationMap.find(i);
			if (pit != PermutationMap.cend())
			{
				_sPermutationName += pit->second + ' ';

				// get corresponding premutation definition from flag
				const auto& dit = AvailableDefines.find(pit->second);

				if (dit != AvailableDefines.cend())
				{
					_Macros.push_back(dit->second);
				}
				else
				{
					HFATALD("Mask error");
				}
			}
			else
			{
				HFATALD("Mask error");
			}
		}
	}
}
//---------------------------------------------------------------------------------------------------
bool PermutationCompiler::SkipPermutation(
	const ShaderDocument& _Shader,
	const ShaderType& _kShaderType,
	const uint32_t& _uShaderPermutationMask,
	const uint32_t& _uPermutation,
	_Out_ uint32_t& _uPermutationResult) const
{
	// Check if currently set bit part of ShaderDefineMask (valid permutation)
	_uPermutationResult = _uPermutation & _uShaderPermutationMask;
	if (_uPermutation != _uPermutationResult)
	{
		return true;
	}

	TIgnoreMasks IgnoreMasks = _Shader.GetIgnoreMasks(_kShaderType);
	for (const IgnoreMask Mask : IgnoreMasks)
	{
		// permutation shares bits with the condition
		if ((_uPermutation & Mask.uConditionBits) == Mask.uConditionBits)
		{
			// skip if the permutation also shares any bit of the permutations to be ignored
			if ((_uPermutation & Mask.uIgnoreBits) != 0u)
			{
				return true;
			}
		}
	}

	return false;
}
//---------------------------------------------------------------------------------------------------
