//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "IncludeInterfaceDX11.h"
#include "Resources\FileSystem.h"
#include "hlx\src\StringHelpers.h"

using namespace Helix::Compiler;

IncludeInterfaceDX11::IncludeInterfaceDX11()
{
}
//---------------------------------------------------------------------------------------------------
IncludeInterfaceDX11::~IncludeInterfaceDX11()
{
}
//---------------------------------------------------------------------------------------------------
HRESULT __stdcall IncludeInterfaceDX11::Open(D3D_INCLUDE_TYPE IncludeType, LPCSTR pName, LPCVOID pParentData, LPCVOID *ppData, UINT *pBytes)
{
	std::string sFile(pName);

	bool bNewFile;

	const std::string* pString = Find(sFile, bNewFile);
	
	if (pString != nullptr)
	{
		UINT uSize = pString->size();
		BYTE* pData = new BYTE[uSize];

		if (pData == nullptr)
		{
			return E_OUTOFMEMORY;
		}

		std::memcpy(pData, pString->c_str(), uSize);

		*pBytes = uSize;
		*ppData = pData;

		//*ppData = pData->c_str();		

		return S_OK;
	}
	
	return E_FAIL;
}
//---------------------------------------------------------------------------------------------------
HRESULT __stdcall IncludeInterfaceDX11::Close(LPCVOID pData)
{
	BYTE* pData2 = (BYTE*)pData;
	delete[] pData2;
	return S_OK;
}
//---------------------------------------------------------------------------------------------------
void IncludeInterfaceDX11::AddIncludeDirectory(const std::string& _sDir)
{
	std::lock_guard<std::mutex> Lock(m_Mutex);
	m_IncludeDirectories.insert(_sDir);
}
//---------------------------------------------------------------------------------------------------
void IncludeInterfaceDX11::ClearIncludeDirectories()
{
	std::lock_guard<std::mutex> Lock(m_Mutex);
	m_IncludeDirectories.clear();
}
//---------------------------------------------------------------------------------------------------
void IncludeInterfaceDX11::Reset()
{
	std::lock_guard<std::mutex> Lock(m_Mutex);
	m_OpenIncludeFiles.clear();
}
//---------------------------------------------------------------------------------------------------
void IncludeInterfaceDX11::InsertShaderText(const std::string& _sFileName, const std::string& _sShaderText)
{
	std::lock_guard<std::mutex> Lock(m_Mutex);
	m_OpenIncludeFiles[_sFileName] = _sShaderText;
}
//---------------------------------------------------------------------------------------------------
const std::string* IncludeInterfaceDX11::Find(const std::string& _sFileName, bool& _bNew)
{
	std::lock_guard<std::mutex> Lock(m_Mutex);

	{
		TOpenFileMap::iterator it = m_OpenIncludeFiles.find(_sFileName);

		if (it != m_OpenIncludeFiles.end())
		{
			_bNew = false;
			return &it->second;
		}
	}

	_bNew = true;

	HFOREACH_CONST(it,end, m_IncludeDirectories)
		stdrzr::fbytestream FileStream(*it + _sFileName, std::ios::in/* | std::ios::binary*/); // binary?

		if (FileStream.is_open() == false)
		{
			continue;
		}

		m_OpenIncludeFiles[_sFileName] = std::move(FileStream.get<std::string>(FileStream.size()));
		const std::string& Buffer = m_OpenIncludeFiles[_sFileName];

		FileStream.close();

		return &Buffer;
	HFOREACH_CONST_END

		if (Resources::FileSystem::PathExists(_sFileName))
		{
			// add parent directory to include directories
			std::string sDir = Resources::FileSystem::GetDirectoryFromPath(_sFileName);
			if (stdrzr::ends_with(sDir, "\\") || stdrzr::ends_with(sDir, "/"))
			{
				m_IncludeDirectories.insert(sDir);
			}			

			stdrzr::fbytestream FileStream(_sFileName, std::ios::in); // binary?

			if (FileStream.is_open() == false)
			{
				return nullptr;
			}

			m_OpenIncludeFiles[_sFileName] = std::move(FileStream.get<std::string>(FileStream.size()));
			const std::string& Buffer = m_OpenIncludeFiles[_sFileName];

			FileStream.close();

			return &Buffer;
		}

	return nullptr;
}
//---------------------------------------------------------------------------------------------------