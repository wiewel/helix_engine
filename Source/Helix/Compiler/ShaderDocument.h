//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef SHADERDOCUMENT_H
#define SHADERDOCUMENT_H

#include "hlx\src\StandardDefines.h"
#include "Display\ViewDefines.h"
#include <unordered_map>

namespace Helix
{
	namespace Compiler
	{
		Flag32E(CompilerFlag)
		{
			CompilerFlag_Debug = 1 << 0,
			CompilerFlag_SkipValidation = 1 << 1,
			CompilerFlag_SkipOptimization = 1 << 2,
			CompilerFlag_PackMatrix_RowMajor = 1 << 3,
			CompilerFlag_PackMatrix_ColumnMajor = 1 << 4,
			CompilerFlag_PartialPrecision = 1 << 5,
			CompilerFlag_Force_VS_NoOptimization = 1 << 6,
			CompilerFlag_Force_PS_NoOptimization = 1 << 7,
			CompilerFlag_NoPreshader = 1 << 8,
			CompilerFlag_Avoid_FlowControl = 1 << 9,
			CompilerFlag_Prefer_FlowControl = 1 << 10,
			CompilerFlag_Enable_Strictness = 1 << 11,
			CompilerFlag_Enable_BackwardsCompatibility = 1 << 12,
			CompilerFlag_IEEE_Strictness = 1 << 13,
			CompilerFlag_Optimization_Level0 = 1 << 14,
			CompilerFlag_Optimization_Level1 = 0,
			CompilerFlag_Optimization_Level2 = (1 << 14) | (1 << 15),
			CompilerFlag_Optimization_Level3 = 1 << 15,
			CompilerFlag_RESERVED16 = 1 << 16,
			CompilerFlag_RESERVED17 = 1 << 17,
			CompilerFlag_Warnings_Are_Errors = 1 << 18,
			CompilerFlag_Resources_May_Alias = 1 << 19
		};

		using TCompilerFlags = Flag<CompilerFlag>;

		// perm string -> perm define (HPM_PermString)
		using TDefineMap = std::unordered_map<std::string, std::string>;

		// permu id -> perm string
		using TPermutationMap = std::unordered_map<uint32_t, std::string>;

		struct Range
		{
			Range() {}
			std::string sDefine;
			uint32_t uStart = 0; // offset
			uint32_t uStep = 0; // increment size
			uint32_t uCount = 0; // number of increments

			inline bool IsValid() const
			{
				return uCount <= 256u && uStep > 0u && sDefine.empty() == false;
			}
		};

		struct IgnoreMask
		{
			uint32_t uConditionBits = 0u; // if Perm & uConditionBit == uConditionBit :
			uint32_t uIgnoreBits = 0u; // and Perm & uIgnoreBits != 0 => skipp permutation (perm shares any bit with ignore mask)
		};

		using TIgnoreMasks = std::vector<IgnoreMask>;
		using TShaderFunctions = std::vector<Display::ShaderFunction>;

		// all meta information of a currently opened shader document
		class ShaderDocument
		{
		public:
			HDEBUGNAME("ShaderDocument");

			ShaderDocument() {}
			ShaderDocument(const hlx::string& _sFilePath);
			~ShaderDocument();

			void AddShaderFunction(const Display::ShaderFunction& _Function);

			// calls ParseText with the member text m_sCode
			void Parse();

			// points the compiler to the parsed shader (hlsl) file to reference debug information
			void SetParsedFilePath(const hlx::string& _sParsedFilePath);
			const hlx::string& GetParsedFilePath() const;

			bool IsOpen() const;

			const TDefineMap GetAvailableDefines(const Display::ShaderType& _kShaderType) const;
			const TPermutationMap GetPermutationMap(const Display::ShaderType& _kShaderType) const;
			const TIgnoreMasks GetIgnoreMasks(const Display::ShaderType& _kShaderType) const;

			const std::vector<Range>& GetRanges() const;

			const TShaderFunctions& GetFunctions() const;

			const hlx::string& GetFilePath() const;
			const hlx::string& GetFileName() const;
			const std::string& GetCode() const;

			const TCompilerFlags& GetCompilationFlags() const;
			void SetCompilationFlags(const TCompilerFlags& _kFlags);

		private:
			const Display::ShaderType GetShaderTypeFromDefine(const std::string& _sDefine) const;

			void RemoveComments();
			void RemoveUnusedShaderFunctions();
			void ExpandHeaders();
			void GatherPermutationDefines();
			void GatherPermutationEnums();
			void GatherRanges();
			void GuardFunctions();
			void BuildIgnoreMasks();

		private: 
			hlx::string m_sFileName; // file name
			hlx::string m_sFilePath;
			hlx::string m_sParsedFilePath;

			std::string m_sCode; // shader text

			std::vector<Range> m_Ranges;
			TDefineMap m_AvailableDefines[Display::ShaderType_NumOf+1];
			TPermutationMap m_PermutationMap[Display::ShaderType_NumOf+1];
			TIgnoreMasks m_IgnoreMasks[Display::ShaderType_NumOf+1];

			// functions to be compiled
			TShaderFunctions m_Functions;

			TCompilerFlags m_kFlags;
		};

		inline void ShaderDocument::SetParsedFilePath(const hlx::string& _sParsedFilePath){	m_sParsedFilePath = _sParsedFilePath;}

		inline const hlx::string& ShaderDocument::GetParsedFilePath() const	{ return m_sParsedFilePath;	}

		inline bool ShaderDocument::IsOpen() const { return m_sCode.empty() == false; }

		inline const TShaderFunctions& ShaderDocument::GetFunctions() const { return m_Functions; }

		inline const TDefineMap ShaderDocument::GetAvailableDefines(const Display::ShaderType& _kShaderType) const
		{ 
			return m_AvailableDefines[_kShaderType];
		}
		inline const TPermutationMap ShaderDocument::GetPermutationMap(const Display::ShaderType& _kShaderType) const
		{ 
			return m_PermutationMap[_kShaderType];
		}

		inline const TIgnoreMasks ShaderDocument::GetIgnoreMasks(const Display::ShaderType& _kShaderType) const
		{
			return m_IgnoreMasks[_kShaderType];
		}

		inline const std::vector<Range>& ShaderDocument::GetRanges() const
		{
			return m_Ranges;
		}

		inline const hlx::string& ShaderDocument::GetFilePath() const { return m_sFilePath; }
		inline const hlx::string& ShaderDocument::GetFileName() const { return m_sFileName; }

		inline const std::string& ShaderDocument::GetCode() const { return m_sCode; }

		inline const TCompilerFlags& ShaderDocument::GetCompilationFlags() const { return m_kFlags; }

		inline void ShaderDocument::SetCompilationFlags(const TCompilerFlags& _kFlags) { m_kFlags = _kFlags; }

	} // Compiler
} // Helix

#endif // SHADERDOCUMENT_H