//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "CodeAnalyzer.h"
//#include <regex>
#include "hlx\src\StringHelpers.h"

using namespace Helix::Display;
using namespace Helix::Compiler;

//---------------------------------------------------------------------------------------------------

CodeAnalyzer::CodeAnalyzer(const std::string& _sCode, ShaderType _kStage) :
	m_sCode(_sCode), m_kStage(_kStage)
{
}
//---------------------------------------------------------------------------------------------------

CodeAnalyzer::~CodeAnalyzer()
{
}
//---------------------------------------------------------------------------------------------------

bool CodeAnalyzer::Parse()
{
	size_t uStructPos = m_sCode.find("struct");

	while (uStructPos < m_sCode.size() && (m_InputMap.empty() || m_OutputMap.empty()))
	{
		size_t uOpenBracket = m_sCode.find('{', uStructPos);

		if (uOpenBracket == std::string::npos)
		{
			// invalid c++ code
			break;
		}

		std::string sStructName = m_sCode.substr(uStructPos + 6, uOpenBracket - uStructPos - 6);
		hlx::remove_whitespaces(sStructName);

		std::string sStageStructName;
		switch (m_kStage)
		{
		case ShaderType_VertexShader:
			sStageStructName = m_InputMap.empty() ? "VS_IN" : "VS_OUT";
			break;
		case ShaderType_HullShader:
			sStageStructName = m_InputMap.empty() ? "HS_IN" : "HS_OUT";
			break;
		case ShaderType_DomainShader:
			sStageStructName = m_InputMap.empty() ? "DS_IN" : "DS_OUT";
			break;
		case ShaderType_GeometryShader:
			sStageStructName = m_InputMap.empty() ? "GS_IN" : "GS_OUT";
			break;
		case ShaderType_PixelShader:
			sStageStructName = m_InputMap.empty() ? "PS_IN" : "PS_OUT";
			break;
		default:
			sStageStructName = m_InputMap.empty() ? "VS_IN" : "VS_OUT";
			break;
		}

		// not the correct struct
		if (hlx::starts_with(sStructName, sStageStructName) == false)
		{
			// skip after brakets to skip nested structs
			hlx::get_body(m_sCode, uOpenBracket, uStructPos);
			uStructPos = m_sCode.find("struct", uStructPos);
			continue;
		}

		size_t uCloseBracket = std::string::npos;
		if (hlx::get_body(m_sCode, uOpenBracket, uCloseBracket)) 
		{
			std::string sStructBody = m_sCode.substr(uOpenBracket + 1, uCloseBracket - 1 - uOpenBracket);

			const std::vector<std::string>&& sLines = hlx::split(sStructBody, ";");
			
			for (const std::string& sLine : sLines)
			{
				std::vector<std::string> sLeftRight = hlx::split(hlx::trim(sLine), ":");

				if (sLeftRight.size() != 2)
				{
					//HWARNINGD("Malformed semantic name in line %s", sLine.c_str());
					continue;
				}

				std::string sTypeName = hlx::trim(sLeftRight[0]);
				std::string sSemanticName = sLeftRight[1];
				hlx::remove_whitespaces(sSemanticName);

				// clean semantic name (remove index from end)
				size_t uPos = sSemanticName.size() - 1;		
				for (; uPos != 0 && std::isdigit(sSemanticName[uPos], std::locale::classic()); --uPos) {};
				if (uPos < sSemanticName.size())
				{
					sSemanticName = sSemanticName.substr(0, uPos+1);
				}

				// clean typename
				uPos = sTypeName.size() - 1;
				for (; uPos != 0 && std::isspace(sTypeName[uPos], std::locale::classic()) == false; --uPos) {};			

				if (uPos < sTypeName.size())
				{
					std::string sName = sTypeName.substr(uPos+1);
					hlx::remove_whitespaces(sName);

					auto insert = [&](TInputMap& _Map)
					{
						TInputMap::iterator it = _Map.find(sSemanticName);
						if (it == _Map.end())
						{
							_Map.insert({ sSemanticName,{ sName } });
						}
						else
						{
							it->second.push_back(sName);
						}
					};

					if (hlx::ends_with(sStageStructName, "IN"))
					{
						insert(m_InputMap);
					}
					else
					{
						insert(m_OutputMap);
					}
				}
				else
				{
					HERROR("Malformed type name in line %s", CSTR(sLine));
					continue;
				}
			}
		}
	}

	return true;
}
//---------------------------------------------------------------------------------------------------
// helper function
bool GetVariableNameForSemantic(const TInputMap& _Map, const std::string& _sSemanticName, _Out_ std::string& _sVarName, uint32_t _uIndex)
{
	TInputMap::const_iterator it = _Map.find(_sSemanticName);
	if (it == _Map.cend())
	{
		return false;
	}

	if (_uIndex < it->second.size())
	{
		_sVarName = it->second[_uIndex];
	}
	else
	{
		return false;
	}

	return true;
}
//---------------------------------------------------------------------------------------------------
bool CodeAnalyzer::GetInputVariableNameForSemantic(const std::string& _sSemanticName, _Out_ std::string& _sVarName, uint32_t _uIndex)
{
	return GetVariableNameForSemantic(m_InputMap, _sSemanticName, _sVarName, _uIndex);
}
//---------------------------------------------------------------------------------------------------

bool CodeAnalyzer::GetOutputVariableNameForSemantic(const std::string & _sSemanticName, _Out_ std::string& _sVarName, uint32_t _uIndex)
{
	return GetVariableNameForSemantic(m_OutputMap, _sSemanticName, _sVarName, _uIndex);
}
//---------------------------------------------------------------------------------------------------
