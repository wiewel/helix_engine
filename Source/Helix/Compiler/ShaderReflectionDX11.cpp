//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "ShaderReflectionDX11.h"
#include "hlx\src\Logger.h"
#include "hlx\src\StringHelpers.h"
#include <D3DCompiler.h>

#include "CodeAnalyzer.h"
#include "Resources\InputLayoutSerializer.h"
#include "Resources\ShaderInterfaceSerialzer.h"
#include "Display\DX11\ViewDefinesDX11.h"

using namespace Helix::Compiler;
using namespace Helix::Display;
using namespace Helix::Resources;

//---------------------------------------------------------------------------------------------------
ShaderReflectionDX11::ShaderReflectionDX11(ID3DBlob* _pShaderBlob, CodeAnalyzer& _CodeAnalyzer) :
	m_CodeAnalyzer(_CodeAnalyzer)
{
	HASSERTD(_pShaderBlob != nullptr, "Invalid shader blob");
	HRESULT Result = E_FAIL;
	HR2(Result = D3DReflect(_pShaderBlob->GetBufferPointer(), _pShaderBlob->GetBufferSize(), IID_ID3D11ShaderReflection, (void**)&m_pShaderReflection));

	if (m_pShaderReflection != nullptr)
	{
		m_pShaderReflection->GetDesc(&m_ShaderDesc);
	}
}
//---------------------------------------------------------------------------------------------------
ShaderReflectionDX11::~ShaderReflectionDX11()
{
	HSAFE_RELEASE(m_pShaderReflection)
}
//---------------------------------------------------------------------------------------------------
bool ShaderReflectionDX11::GetInputLayout(_Out_ InputLayoutSerializer& _Layout)
{
	D3D11_SIGNATURE_PARAMETER_DESC SignatureDesc = {};

	for (uint32_t i = 0; i < m_ShaderDesc.InputParameters; ++i)
	{
		if (FAILED(m_pShaderReflection->GetInputParameterDesc(i, &SignatureDesc)))
		{
			return false;
		}

		Display::InputElementDesc HelixSignature = Display::ConvertToHelixDescription(SignatureDesc);
		m_CodeAnalyzer.GetInputVariableNameForSemantic(HelixSignature.sSemanticName, HelixSignature.sVariableName, HelixSignature.uSemanticIndex);

		_Layout.AddInputElement(HelixSignature);
	}

	return true;
}
//---------------------------------------------------------------------------------------------------

bool ShaderReflectionDX11::GetOutputLayout(_Out_ InputLayoutSerializer& _Layout)
{
	D3D11_SIGNATURE_PARAMETER_DESC SignatureDesc = {};

	for (uint32_t i = 0; i < m_ShaderDesc.OutputParameters; ++i)
	{
		if (FAILED(m_pShaderReflection->GetOutputParameterDesc(i, &SignatureDesc)))
		{
			return false;
		}

		Display::InputElementDesc HelixSignature = Display::ConvertToHelixDescription(SignatureDesc);
		m_CodeAnalyzer.GetOutputVariableNameForSemantic(HelixSignature.sSemanticName, HelixSignature.sVariableName, HelixSignature.uSemanticIndex);

		_Layout.AddInputElement(HelixSignature);
	}

	return true;
}
//---------------------------------------------------------------------------------------------------

bool ShaderReflectionDX11::GetInterface(_Out_ ShaderInterfaceSerializer& _InterfaceSerializer)
{	 
	HRESULT Result = E_FAIL;

	for (uint32_t br = 0; br < m_ShaderDesc.BoundResources; ++br)
	{
		D3D11_SHADER_INPUT_BIND_DESC BindDesc = {};
		if (FAILED(Result = m_pShaderReflection->GetResourceBindingDesc(br, &BindDesc)))
		{
			HERROR("Failed to get input binding description for %u", br);
			return false;
		}

		if (BindDesc.Name == nullptr)
		{
			HERROR("Invalid binding name (nullptr)");
			return false;
		}

		if (BindDesc.Type == D3D_SIT_TEXTURE || BindDesc.Type == D3D_SIT_UAV_RWTYPED)
		{
			ShaderTextureDesc Texture = {};
			Texture.BindInfo.sName = std::string(BindDesc.Name);
			Texture.BindInfo.uSlot = BindDesc.BindPoint;
			Texture.BindInfo.uCount = BindDesc.BindCount;
			Texture.BindInfo.uSize = 0;
			Texture.uNumOfSamples = BindDesc.NumSamples; // multisampled texture
			Texture.kDimension = static_cast<ResourceDimension>(BindDesc.Dimension);
			Texture.kReturnType = static_cast<ShaderResourceReturnType>(BindDesc.ReturnType);
			Texture.bRWTexture = BindDesc.Type == D3D_SIT_UAV_RWTYPED;

			_InterfaceSerializer.AddTexture(Texture);
		}
		else if (BindDesc.Type == D3D_SIT_SAMPLER)
		{
			ShaderSamplerDesc Sampler = {};

			Sampler.BindInfo.sName = std::string(BindDesc.Name);
			Sampler.BindInfo.uSlot = BindDesc.BindPoint;
			Sampler.BindInfo.uCount = BindDesc.BindCount;
			Sampler.BindInfo.uSize = 0;

			_InterfaceSerializer.AddSampler(Sampler);
		}
		else 
		{
			ShaderBufferDesc Buffer = {};

			Buffer.kType = ConvertToHelixType(BindDesc.Type);

			if (Buffer.kType == ShaderBufferType_Unknown)
			{
				HERROR("Unkown shader input resource type %X", BindDesc.Type);
				return false;
			}

			Buffer.BindInfo.sName = std::string(BindDesc.Name);
			Buffer.BindInfo.uSlot = BindDesc.BindPoint;
			Buffer.BindInfo.uCount = BindDesc.BindCount;
			Buffer.BindInfo.uSize = 0;

			if (GetBuffer(BindDesc.BindPoint, Buffer) == false)
			{
				return false;
			}

			_InterfaceSerializer.AddBuffer(Buffer);
		}
	}

	return true;
}
//---------------------------------------------------------------------------------------------------

bool ShaderReflectionDX11::GetBuffer(uint32_t _uSlot, ShaderBufferDesc& _CBuffer)
{
	D3D11_SHADER_BUFFER_DESC BufferDesc = {};
	ID3D11ShaderReflectionConstantBuffer* pConstBuffer = m_pShaderReflection->GetConstantBufferByIndex(_uSlot);

	if (pConstBuffer == nullptr)
	{
		HERROR("Failed to get constant buffer reflection interface %u", _uSlot);
		return false;
	}
	
	if (FAILED(pConstBuffer->GetDesc(&BufferDesc)))
	{
		// this case is valid if the slot has been forced by the user
		HWARNING("Failed to get constant buffer description for %s at slot %u. This might happen if the slot was manually set by the user.", CSTR(_CBuffer.BindInfo.sName), _uSlot);
		return true;
	}

	_CBuffer.BindInfo.uSize = BufferDesc.Size;

	for (uint32_t v = 0; v < BufferDesc.Variables; ++v)
	{
		ID3D11ShaderReflectionVariable* pVariable = pConstBuffer->GetVariableByIndex(v);
		if (pVariable == nullptr)
		{
			HERROR("Failed to get variable reflection interface %u", v);
			return false;
		}

		D3D11_SHADER_VARIABLE_DESC VarDesc = {};
		if (FAILED(pVariable->GetDesc(&VarDesc)))
		{
			HERROR("Failed to get variable description for %u %s", v, CSTR(BufferDesc.Name));
			return false;
		}

		// Get the variable type description and store it
		ID3D11ShaderReflectionType* pVarType = pVariable->GetType();
		if (pVarType == nullptr)
		{
			HERROR("Failed to get variable type reflection interface for %s", CSTR(VarDesc.Name));
			return false;
		}

		ShaderVariableDesc Variable = {};

		Variable.sName = std::string(VarDesc.Name);
		Variable.uSize = VarDesc.Size;
		Variable.uStartOffset = VarDesc.StartOffset;
		Variable.uFlags = VarDesc.uFlags;

		if (GetVariableTypes(Variable, pVarType) == false)
		{
			return false;
		}

		_CBuffer.Variables.push_back(Variable);
	}

	return true;
}
//---------------------------------------------------------------------------------------------------
bool ShaderReflectionDX11::GetVariableTypes(ShaderVariableDesc& _ParentVariable, ID3D11ShaderReflectionType* _pVarType)
{
	D3D11_SHADER_TYPE_DESC VarTypeDesc = {};
	if (FAILED(_pVarType->GetDesc(&VarTypeDesc)))
	{
		HERROR("Failed to get variable type description for %s", CSTR(_ParentVariable.sName));
		return false;
	}

	_ParentVariable.uMembers = VarTypeDesc.Members; // variable is a struct
	_ParentVariable.uRows = VarTypeDesc.Rows;
	_ParentVariable.uColumns = VarTypeDesc.Columns;
	_ParentVariable.uElements = VarTypeDesc.Elements;
	_ParentVariable.uStructureOffset = VarTypeDesc.Offset;

	_ParentVariable.kClass = static_cast<ShaderVariableClass>(VarTypeDesc.Class);
	_ParentVariable.kType = static_cast<ShaderVariableType>(VarTypeDesc.Type);

	for (uint32_t m = 0; m < _ParentVariable.uMembers; ++m)
	{
		ShaderVariableDesc MemberVar = {};

		ID3D11ShaderReflectionType* pMemberType = _pVarType->GetMemberTypeByIndex(m);
		if (pMemberType == nullptr)
		{
			HERROR("Failed to get variable type reflection interface for %s", CSTR(_ParentVariable.sName));
			return false;
		}

		if (GetVariableTypes(MemberVar, pMemberType) == false)
		{
			return false;
		}

		const char* pName = pMemberType->GetMemberTypeName(m);
		if (pName != nullptr)
		{
			MemberVar.sName = std::string(pMemberType->GetMemberTypeName(m));
		}
		else
		{
			// for debugging
			MemberVar.sName = MemberVar.GetVariableIdentifier(m);
		}

		// compute size
		if (MemberVar.uSize == 0)
		{
			if (MemberVar.uMembers == 0)
			{
				MemberVar.uSize = GetShaderVariableSize(MemberVar.kType) * std::max(1u, MemberVar.uColumns) * std::max(1u, MemberVar.uRows);
			}
			else
			{
				for (const ShaderVariableDesc& SubMem : MemberVar.Children)
				{
					MemberVar.uSize += SubMem.uSize;
				}
			}
		}

		_ParentVariable.Children.push_back(MemberVar);
	}

	return true;
}
//---------------------------------------------------------------------------------------------------
