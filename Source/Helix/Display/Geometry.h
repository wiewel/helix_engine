//   Copyright 2020 Fabian Wahlster
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef GEOMETRY_H
#define GEOMETRY_H

#include "DX11\MeshClusterDX11.h"

namespace Helix
{
	namespace Display
	{
		class Geometry
		{
		public:
			Geometry();
			~Geometry();

			// generate unit cube with edge length = _fSize, this function will allocate heap memory for you, but you will have to release it
			static MeshClusterDX11 GenerateCube(float _fSize = 1.f, const char* _pName = nullptr);
			static MeshClusterDX11 Generate2DPlane(float _fSize = 1.f, const char* _pName = nullptr);

		private:

		};
	} // Display
} // Helix

#endif // GEOMETRY_H