//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef VERTEXLAYOUTS_H
#define VERTEXLAYOUTS_H

#include "ViewDefines.h"

namespace Helix
{
	namespace Display
	{
		// TODO: constexpr for default constructors
		struct VertexPosXY
		{
			VertexPosXY() : m_vPosition(0, 0) {}

			VertexPosXY(Math::float2 const& _vPosition) :
				m_vPosition(_vPosition)
			{}

			VertexPosXY(float px, float py) :
				m_vPosition(px, py)
			{}

			Math::float2 m_vPosition;

			static constexpr VertexLayout Layout = VertexLayout_Pos_XY;
		};

		struct VertexPosXYZ
		{
			VertexPosXYZ() : m_vPosition(0,0,0) {}

			VertexPosXYZ(Math::float3 const& _vPosition) :
				m_vPosition(_vPosition)
			{}

			Math::float3 m_vPosition;

			static constexpr VertexLayout Layout = VertexLayout_Pos_XYZ;
		};

		struct VertexPosXYTex
		{
			VertexPosXYTex() :
				m_vPosition(0,0),
				m_vTexCoord(0,0)
			{}

			VertexPosXYTex(float px, float py, float u, float v) :
				m_vPosition(px, py),
				m_vTexCoord(u, v)
			{}

			VertexPosXYTex(Math::float2 const& _vPosition, Math::float2 const& _vTexCoord) :
				m_vPosition(_vPosition),
				m_vTexCoord(_vTexCoord)
			{}

			Math::float2 m_vPosition;
			Math::float2 m_vTexCoord;

			static constexpr VertexLayout Layout = VertexLayout_PosXYTex;
		};

		struct VertexPosXYZTex
		{
			VertexPosXYZTex() :
				m_vPosition(0,0,0),
				m_vTexCoord(0,0)
			{}

			VertexPosXYZTex(float px, float py, float pz,	float u, float v) :
				m_vPosition(px, py, pz),
				m_vTexCoord(u, v)
			{}

			VertexPosXYZTex(Math::float3 const& _vPosition, Math::float2 const& _vTexCoord) :
				m_vPosition(_vPosition),
				m_vTexCoord(_vTexCoord)
			{}

			Math::float3 m_vPosition;
			Math::float2 m_vTexCoord;

			static constexpr VertexLayout Layout = VertexLayout_PosXYZTex;
		};

		struct VertexPosXYZColor
		{
			VertexPosXYZColor() :
				m_vPosition(0, 0, 0),
				m_vColor(0, 0, 0)
			{}

			VertexPosXYZColor(float px, float py, float pz, float r, float g, float b) :
				m_vPosition(px, py, pz),
				m_vColor(r, g, b)
			{}

			VertexPosXYZColor(Math::float3 const& _vPosition, Math::float3 const& _vColor) :
				m_vPosition(_vPosition),
				m_vColor(_vColor)
			{}

			Math::float3 m_vPosition;
			Math::float3 m_vColor;

			static constexpr VertexLayout Layout = VertexLayout_PosXYZColor;
		};

		struct VertexPosXYZNormTex
		{
			VertexPosXYZNormTex() :
				m_vPosition(0,0,0),
				m_vNormal(0,0,0),
				m_vTexCoord(0,0)
			{}

			VertexPosXYZNormTex(Math::float3 const& _vPosition, Math::float3 const& _vNormal, Math::float2 const& _vTexCoord) :
				m_vPosition(_vPosition),
				m_vNormal(_vNormal),
				m_vTexCoord(_vTexCoord)
			{}

			Math::float3 m_vPosition;
			Math::float3 m_vNormal;
			Math::float2 m_vTexCoord;

			static constexpr VertexLayout Layout = VertexLayout_PosXYZNormTex;
		};

		struct VertexPosXYZNormTanTex
		{
			VertexPosXYZNormTanTex() :
				m_vPosition(0,0,0),
				m_vNormal(0, 0, 0),
				m_vTangent(0, 0, 0),
				m_vTexCoord(0, 0)
			{}

			VertexPosXYZNormTanTex(float px, float py, float pz,
				float nx, float ny, float nz,
				float tx, float ty, float tz,
				float u, float v) :
				m_vPosition(px, py, pz),
				m_vNormal(nx, ny, nz),
				m_vTangent(tx, ty, tz),
				m_vTexCoord(u, v)
			{}

			VertexPosXYZNormTanTex(Math::float3 const& _vPosition, Math::float3 const& _vNormal, Math::float3 const& _vTangent, Math::float2 const& _vTexCoord) :
				m_vPosition(_vPosition),
				m_vNormal(_vNormal),
				m_vTangent(_vTangent),
				m_vTexCoord(_vTexCoord)
			{}

			Math::float3 m_vPosition;
			Math::float3 m_vNormal;
			Math::float3 m_vTangent;
			Math::float2 m_vTexCoord;

			static constexpr VertexLayout Layout = VertexLayout_PosXYZNormTanTex;
		};

		struct VertexPosXYZNormTanBitanTex
		{
			VertexPosXYZNormTanBitanTex() :
				m_vPosition(0, 0, 0),
				m_vNormal(0, 0, 0),
				m_vTangent(0, 0, 0),
				m_vBiTangent(0, 0, 0),
				m_vTexCoord(0, 0)
			{}

			VertexPosXYZNormTanBitanTex(Math::float3 const& _vPosition, Math::float3 const& _vNormal, Math::float3 const& _vTangent, Math::float3 const& _vBiTangent, Math::float2 const& _vTexCoord) :
				m_vPosition(_vPosition),
				m_vNormal(_vNormal),
				m_vTangent(_vTangent),
				m_vBiTangent(_vBiTangent),
				m_vTexCoord(_vTexCoord)
			{}

			Math::float3 m_vPosition;
			Math::float3 m_vNormal;
			Math::float3 m_vTangent;
			Math::float3 m_vBiTangent;
			Math::float2 m_vTexCoord;

			static constexpr VertexLayout Layout = VertexLayout_PosXYZNormTanBitanTex;
		};

	} // Display
} // Helix

#endif // VERTEXLAYOUTS_H