//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "ShaderVariable.h"
#include "DX11\ShaderInterfaceDX11.h"

using namespace Helix::Display;
//---------------------------------------------------------------------------------------------------

ShaderVariable::ShaderVariable(const std::string& _sVarPath, const uint32_t& _uVarSize) :
	m_sVarPath(_sVarPath), m_uVarSize(_uVarSize)
{
}
//---------------------------------------------------------------------------------------------------

ShaderVariable::~ShaderVariable()
{
	if (m_pInterface != nullptr)
	{
		m_pInterface->UnregisterVariable(this, m_sVarPath, m_sShaderInstanceName);
	}

	m_pBufferInstance = nullptr;
}

//---------------------------------------------------------------------------------------------------

void ShaderVariable::Update(std::shared_ptr<GPUBufferInstanceDX11>& _pBufferInstance)
{
	m_pBufferInstance = _pBufferInstance;
	m_pBase = _pBufferInstance->GetData();
	m_uStructureSize = _pBufferInstance->GetDescription().GetStructureSize();

	OnUpdate();
}
//---------------------------------------------------------------------------------------------------
bool ShaderVariable::UpdateDesc(const ShaderVariableDesc& _Desc)
{
	if (_Desc.uSize == m_Description.uSize &&
		_Desc.uElements == m_Description.uElements &&
		_Desc.sName == m_Description.sName &&
		_Desc.kType == m_Description.kType)
	{
		m_Description = _Desc;
		return true;
	}
	else
	{
		HFATAL("Description for variable %s %uB does not match!", CSTR(m_sVarPath), m_uVarSize);
		return false;
	}
}
//---------------------------------------------------------------------------------------------------

void ShaderVariable::Invalidate()
{
	OnInvalidate();

	m_pBase = nullptr;
	m_pBufferInstance = nullptr;

	m_uStructureSize = 0u;
}

//---------------------------------------------------------------------------------------------------
bool ShaderVariable::Initialize(ShaderInterfaceDX11* _pInterface, const std::string& _sShaderInstanceName)
{
	if (m_pInterface == nullptr)
	{
		m_sShaderInstanceName = _sShaderInstanceName;
		m_pInterface = _pInterface;

		m_pBufferInstance = m_pInterface->RegisterVariable(this, m_sVarPath, _sShaderInstanceName, m_Description, m_uStructureSize);
		
		if (m_pBufferInstance != nullptr)
		{
			m_pBase = m_pBufferInstance->GetData();

			if (m_Description.uSize != m_uVarSize * std::max(m_Description.uElements, 1u))
			{
				m_pBase = nullptr;
				HFATAL("Size of Variable %s does not match specified type size %u", CSTR(m_sVarPath), m_uVarSize);
				return false;
			}
		}		
	}

	return m_pBase != nullptr;
}
//---------------------------------------------------------------------------------------------------
