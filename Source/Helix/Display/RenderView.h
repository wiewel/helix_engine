//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef RENDERVIEW_H
#define RENDERVIEW_H

#include <vector>
#include "ViewDefines.h"
#include "hlx\src\StandardDefines.h"
#include <mutex>

namespace Helix
{
	namespace Display
	{
		class RenderView
		{
		public:
			RenderView() {}

			~RenderView(){}
			///Disable copy constructor
			RenderView(RenderView& _RenderView) = delete;

			//INTERFACE
			virtual bool Initialize(HWND _hWnd) = 0;
			virtual void Shutdown() = 0;

			//virtual void Clear(float _fColor[4], float _fClearDepth, uint8_t _uClearStencil) = 0;
			//virtual bool RendererInitialized() = 0;

			virtual void ResizeRenderer(const uint32_t _uXResolution, const uint32_t _uYResolution) = 0;

			virtual void GetRendererResolution(_Out_ uint32_t& _uXResolution, _Out_ uint32_t& _uYResolution) const = 0;

			virtual void SetFullscreen(bool _bFullscreen = true) = 0;

			virtual void ChangeRendererSettings(uint32_t _uDisplayModeId, uint32_t _uAdapterId, uint32_t _uOutputId) = 0;

			//virtual HICON GetIcon(HINSTANCE _hInstance) = 0;

			//IMPLEMENTATION
			void ChangeViewSettings(const ViewSettings& _ViewSettings);
			const ViewSettings& GetViewSettings() const;

			void SetDebug(bool _bDebug);
			bool GetDebug() const;

		protected:
			ViewSettings m_ViewSettings;
			//HICON m_hIcon = nullptr;

		private:
			bool m_bDebug = false;
		};

		inline const ViewSettings& RenderView::GetViewSettings() const
		{
			return m_ViewSettings;
		}

		inline void RenderView::SetDebug(bool _bDebug)
		{
			m_bDebug = _bDebug;
		}

		inline bool RenderView::GetDebug() const
		{
			return m_bDebug;
		}

		inline void RenderView::ChangeViewSettings(const ViewSettings& _ViewSettings)
		{
			m_ViewSettings = _ViewSettings;
		}

	} // Display
} // Helix

#endif // RENDERVIEW_H