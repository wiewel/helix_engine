//   Copyright 2020 Fabian Wahlster
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef SHADERPERMUTATIONDEFINES_H
#define SHADERPERMUTATIONDEFINES_H

#include "ViewDefines.h"

namespace Helix
{
	namespace Display
	{
		struct StagePermutation
		{
			StagePermutation() {}
			StagePermutation(const ShaderPermutationHash& _Permutation, const ShaderType& _kStage) :
				Permutation(_Permutation), kStage(_kStage) {}

			ShaderPermutationHash Permutation;
			ShaderType kStage = ShaderType_None;
		};

		struct StageConstraints
		{
			inline bool Empty() const { return Constraints.empty(); }
			inline const std::vector<StagePermutation>& Get() const { return Constraints; }
			inline void Clear() { Constraints.resize(0); }

			//---------------------------------------------------------------------------------------------------

			inline friend bool operator <(const StageConstraints& l, const StageConstraints& r)
			{
				// TODO: find something more meaningful for constraint comparison
				// which object is less constraint than another? how chan different stages & restraints vs constraints compared in a simple way?

				uint64_t uLeftConstraint = 0u;
				uint64_t uLeftRestraint = 0u;
				uint64_t uRightConstraint = 0u;
				uint64_t uRightRestraint = 0u;

				for (const StagePermutation& lC : l.Constraints)
				{
					if (lC.Permutation.bRestraint)
					{
						uLeftRestraint |= lC.Permutation.uBasePermutation;
					}
					else
					{
						uLeftConstraint |= lC.Permutation.uBasePermutation;
					}
				}

				for (const StagePermutation& rC : r.Constraints)
				{
					if (rC.Permutation.bRestraint)
					{
						uRightRestraint |= rC.Permutation.uBasePermutation;
					}
					else
					{
						uRightConstraint |= rC.Permutation.uBasePermutation;
					}
				}

				//__popcount(uLeftConstaint);
				if (uLeftConstraint == uRightConstraint)
				{
					//if (uLeftRestraint == uRightRestraint)
					//{
					//	// TOTO: sort by range constraints => currently only post effects use ranges, so theres no need to sort 1 object
					//}

					return uLeftRestraint < uRightRestraint;
				}

				return uLeftConstraint < uRightConstraint;
			}

			//---------------------------------------------------------------------------------------------------

			inline friend bool operator==(const StageConstraints& l, const StageConstraints& r)
			{
				if (l.Constraints.size() != r.Constraints.size())
					return false;

				for (const StagePermutation& lC : l.Constraints)
				{
					if (r.GetConstraint(lC.kStage, lC.Permutation.bRestraint) != lC.Permutation)
					{
						return false;
					}
				}

				return true;
			}

			//---------------------------------------------------------------------------------------------------
			// Gets constraints & restraints
			inline ShaderPermutationHash GetConstraint(ShaderType _kShaderType, bool _bRestraint = false) const
			{
				for (const StagePermutation& Constraint : Constraints)
				{
					if (Constraint.kStage == _kShaderType
						&& Constraint.Permutation.bRestraint == _bRestraint)
					{
						return Constraint.Permutation;
					}
				}

				return ShaderPermutationHash();
			}
			//---------------------------------------------------------------------------------------------------
			inline void AddConstraint(const ShaderPermutationHash& _Constraint, ShaderType _kShaderType)
			{
				for (StagePermutation& Constraint : Constraints)
				{
					if (Constraint.kStage == _kShaderType &&
						Constraint.Permutation.bRestraint == _Constraint.bRestraint)
					{
						Constraint.Permutation.AddConstraint(_Constraint);
						return;
					}
				}

				Constraints.push_back({ _Constraint, _kShaderType });
			}
			//---------------------------------------------------------------------------------------------------
			// sets (overwrites) constraint OR restraint for this stage
			inline void SetConstraint(const ShaderPermutationHash& _Constraint, ShaderType _kShaderType)
			{
				for (StagePermutation& Constraint : Constraints)
				{
					if (Constraint.kStage == _kShaderType
						&& Constraint.Permutation.bRestraint == _Constraint.bRestraint)
					{
						Constraint.Permutation = _Constraint;
						return;
					}
				}

				Constraints.push_back({ _Constraint, _kShaderType });
			}
			//---------------------------------------------------------------------------------------------------
			inline void ResetConstraint(ShaderType _kShaderType, bool _bRestraint = false)
			{
				for (StagePermutation& Constraint : Constraints)
				{
					if (Constraint.kStage == _kShaderType
						&& Constraint.Permutation.bRestraint == _bRestraint)
					{
						Constraint.Permutation = {};
						return;
					}
				}
			}
			//---------------------------------------------------------------------------------------------------
			inline void Combine(const StageConstraints& _StageConstraints)
			{
				for (const StagePermutation& Constraint : _StageConstraints.Constraints)
				{
					AddConstraint(Constraint.Permutation, Constraint.kStage);
				}
			}

		private:
			std::vector<StagePermutation> Constraints;
		};
		//---------------------------------------------------------------------------------------------------

		struct PassConstraint
		{
			ShaderPermutationHash Constraint; // constraint for pass with identifier == kTargetPassIdentifier 
			uint64_t kTargetPassIdentifier;
			ShaderType kStage; // which stage should this constraint be applied to
		};

		//---------------------------------------------------------------------------------------------------

		struct AveragePFSpeed
		{
			inline void Add(const float _fObjectTime, const float _fObjectPF)
			{
				if (uCounter++ == 100u)
				{
					uCounter = 1u;
					fTime = _fObjectTime;
					fPF = _fObjectPF;
				}
				else
				{
					fTime += _fObjectTime;
					fPF += _fObjectPF;
				}
			}

			inline float GetPFSpeed() const { return fTime == 0.f ? 0.f : fPF / fTime; }
			inline float GetAvgPF() const { return uCounter == 0u ? fPF : fPF / static_cast<float>(uCounter); }
		private:
			uint32_t uCounter = 0u;
			float fTime = 0.f;
			float fPF = 0.f;
		};

		//---------------------------------------------------------------------------------------------------
		// Weights
		//---------------------------------------------------------------------------------------------------

		struct ShaderPermutationWeights
		{
			ShaderPermutationWeights() :
				fConstantWeight(1.f), IOWeights(32u, 1.f), ClassWeights(32u, 1.f), RangeWeights(8u, 1.f) {}

			float fConstantWeight;
			std::vector<float> IOWeights;
			std::vector<float> ClassWeights;
			std::vector<float> RangeWeights;

			// evaluate permutation factor PF
			inline float operator()(const ShaderPermutationHash& _Permutation) const
			{
				float fPF = fConstantWeight;

				for (size_t i = 0; i < IOWeights.size(); ++i)
				{
					if (_Permutation.uIOPermutation & (1 << i))
					{
						fPF += IOWeights.at(i);
					}
				}

				for (size_t i = 0; i < ClassWeights.size(); ++i)
				{
					if (_Permutation.uClassPermutation & (1 << i))
					{
						fPF += ClassWeights.at(i);
					}
				}

				size_t uRangeCount = std::min(RangeWeights.size(), _Permutation.Ranges.size());
				for (size_t i = 0; i < uRangeCount; ++i)
				{
					fPF += _Permutation.Ranges.at(i) * RangeWeights.at(i);
				}

				return fPF;
			}
		};

		//---------------------------------------------------------------------------------------------------
		// Stats & Ratings
		//---------------------------------------------------------------------------------------------------

		struct ShaderPermutationStats
		{
			ShaderPermutationHash Permutation;
			float fPermutationFactor = 0.0; // PF

			inline friend bool operator< (const ShaderPermutationStats& l, const ShaderPermutationStats& r)
			{
				return l.fPermutationFactor < r.fPermutationFactor;
			}
		};

		using TPermutationRatings = std::vector<ShaderPermutationStats>;
		//---------------------------------------------------------------------------------------------------

		struct StageRatings
		{
			StageRatings() {}

			StageRatings(
				const std::vector<ShaderPermutationHash>& _Permutations,
				const ShaderPermutationWeights& _Weights,
				const ShaderType _kStage,
				const uint64_t _uDefaultPruneMask) :
				kShaderStage(_kStage),
				Weights(_Weights)
			{
				SetPermutations(_Permutations, _uDefaultPruneMask);

				EvaluateWeights();
			}

			// binary search to find permutations with PF closest to _FindPF which has
			// Permutation.BasPermutation & _uConstraintMask == _uConstraintMask
			inline ShaderPermutationStats FindConstrained(float _fFindPF, const ShaderPermutationHash& _Constraint, const ShaderPermutationHash& _Restraint)
			{
				// find permutations that match the constraint mask
				if (Constraint != _Constraint || Restraint != _Restraint || ConstraintIndices.empty())
				{
					// check if previous constraints satisfy the new constraints and only filter ConstraintIndices
					bool bUpdate = ConstraintIndices.size() > 10u && _Constraint.SatisfiesConstraint(Constraint) && _Restraint.SatisfiesConstraint(Restraint) && ConstraintIndices.empty() == false;

					Constraint = _Constraint;
					Restraint = _Restraint;

					if (bUpdate)
					{
						// re-validate previous permutations with new constraint
						std::vector<uint32_t>::iterator end = ConstraintIndices.end();
						std::vector<uint32_t>::iterator it = std::remove_if(ConstraintIndices.begin(), end, [&](const uint32_t& i)
						{
							const ShaderPermutationStats& Stats = QualityRatings.at(i);
							// check if permutation satisfies constraint
							bool bRemove = Stats.Permutation.SatisfiesConstraint(Constraint) == false ||
								Stats.Permutation.SatisfiesConstraint(Restraint) == false;
							return bRemove;
						});

						if (it != end)
						{
							ConstraintIndices.erase(it, end);
						}
					}
					else
					{
						ConstraintIndices.resize(0);
						uint32_t uSize = static_cast<uint32_t>(QualityRatings.size());

						for (uint32_t i = 0; i < uSize; ++i)
						{
							const ShaderPermutationStats& Stats = QualityRatings.at(i);
							// check if permutation satisfies constraint
							if (Stats.Permutation.SatisfiesConstraint(Constraint) &&
								Stats.Permutation.SatisfiesConstraint(Restraint))
							{
								ConstraintIndices.push_back(i);
							}
						}
					}
				}

				if (ConstraintIndices.empty())
				{
					HERROR("No permutation found satisfying constraints & restraints");
					return ShaderPermutationStats();
				}
				else if (ConstraintIndices.size() == 1u)
				{
					return QualityRatings.at(ConstraintIndices.front());
				}

				uint32_t uBestMatch = 0u;
				float fBestDist = fMaxPF;
				float fDist = fMaxPF;

				int32_t uStart = 0;
				int32_t uEnd = static_cast<uint32_t>(ConstraintIndices.size());

				uint32_t uSearchDepth = 0u;
				// binary search for best match
				while(uStart < uEnd)
				{
					uint32_t uMid = uStart + ((uEnd - uStart) / 2);

					const float& fPF = QualityRatings.at(ConstraintIndices.at(uMid)).fPermutationFactor;

					fDist = abs(_fFindPF - fPF);

					if (fDist <= fBestDist)
					{
						uBestMatch = uMid;
						fBestDist = fDist;
					}

					if (++uSearchDepth > uMaxSearchDepth)
					{
						break;
					}

					if (_fFindPF == fPF)
					{
						uBestMatch = uMid;
						break;
					}
					else if (_fFindPF < fPF)
					{
						uEnd = uMid - 1;
					}
					else
					{
						uStart = uMid + 1;
					}
				}

				return QualityRatings.at(ConstraintIndices.at(uBestMatch));
			}
			//---------------------------------------------------------------------------------------------------

			void SetPermutations(const std::vector<ShaderPermutationHash>& _Permutations, const uint64_t _uDefaultPruneMask = 0u)
			{
				QualityRatings.resize(0);
				// prune permutations that have bits set from the DefaultPruneMask ( for example DebugPermutations should be pruned)
				for (const ShaderPermutationHash& Perm : _Permutations)
				{
					// Permutation Shares any bit with the mask => prune!
					if ((Perm.uBasePermutation & _uDefaultPruneMask) != 0u)
					{
						continue;
					}

					// keep permutation
					ShaderPermutationStats Stats;
					Stats.Permutation = Perm;
					QualityRatings.push_back(Stats);
				}
			}
			//---------------------------------------------------------------------------------------------------

			// call this function after updating Weights / Ratings
			inline void EvaluateWeights()
			{
				fAvgPF = 0.f;
				fTotalPF = 0.f;
				fMinPF = FLT_MAX;
				fMaxPF = FLT_EPSILON;

				Constraint = {};
				ConstraintIndices.resize(0);

				for (ShaderPermutationStats& Stats : QualityRatings)
				{
					Stats.fPermutationFactor = Weights(Stats.Permutation);

					fMinPF = std::min(fMinPF, Stats.fPermutationFactor);
					fMaxPF = std::max(fMaxPF, Stats.fPermutationFactor);
					fTotalPF += Stats.fPermutationFactor;
				}

				if (QualityRatings.empty() == false)
				{
					fAvgPF = fTotalPF / static_cast<float>(QualityRatings.size());
				}

				std::sort(QualityRatings.begin(), QualityRatings.end());
			}
			//---------------------------------------------------------------------------------------------------

			inline const float& GetAvgPF() const { return fAvgPF; }
			inline const float& GetMinPF() const { return fMinPF; }
			inline const float& GetMaxPF() const { return fMaxPF; }
			inline const float& GetTotalPF() const { return fTotalPF; }

			inline void SetMaxSearchDepth(const uint32_t& _uSearchDepth) { uMaxSearchDepth = _uSearchDepth; }

		public:
			TPermutationRatings QualityRatings;
			ShaderPermutationWeights Weights;

			ShaderPermutationHash Constraint;
			ShaderPermutationHash Restraint;

			std::vector<uint32_t> ConstraintIndices;
		
			ShaderType kShaderStage = ShaderType_None;
			float fAvgPF = 1.f;
			float fMinPF = FLT_MAX;
			float fMaxPF = FLT_EPSILON;
			float fTotalPF = 0.f;
			uint32_t uMaxSearchDepth = HUNDEFINED32;
		};
		//---------------------------------------------------------------------------------------------------

	} // Display
} // Helix

#endif // !SHADERPERMUTATIONDEFINES_H
