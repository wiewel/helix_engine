//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef SYNCSHADERVARIABLE_H
#define SYNCSHADERVARIABLE_H
#include "DataStructures\SyncVariable.h"
#include "ViewDefines.h"

namespace Helix
{
	namespace Display
	{
		// forward decls
		class GPUBufferInstanceDX11;

		struct SyncShaderVariable : Datastructures::SyncVariableBase
		{
		public:
			SyncShaderVariable(const ShaderVariableDesc& _Desc, GPUBufferInstanceDX11& _Buffer);

			static Datastructures::VarType GetVarType(const ShaderVariableDesc& _Desc);

		private:
			const void* Get() final;
			void Set(const void* _pData) final;

		private:
			const uint32_t m_uSize;
			GPUBufferInstanceDX11& m_Buffer;

			uint8_t* m_pVar = nullptr;

		};

		inline const void* SyncShaderVariable::Get(){return m_pVar;	}
	}
} // Helix

#endif // !SYNCSHADERVARIABLE_H
