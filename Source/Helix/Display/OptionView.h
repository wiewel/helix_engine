//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef OPTIONVIEW_H
#define OPTIONVIEW_H

#include "hlx\src\StandardDefines.h"
#include <cstdint>
#include <string>
#include <CommCtrl.h>
#include <vector>

namespace Helix
{
	namespace Display
	{
		class OptionView
		{
		public:
			OptionView(HWND _ParentHwnd, HINSTANCE _hInstanceParent)
			{
				m_hWndParent = _ParentHwnd;
				m_hInstanceParent = _hInstanceParent;
			}

			~OptionView()
			{
				UnregisterClass(TEXT("DialogClass"), m_hInstanceParent);
			}

			void Initialize(const char* _sTitle, std::vector<std::string> _DisplayModeComboboxEntries, std::vector<std::string> _AdapterComboboxEntries, std::vector<std::string> _DisplayComboboxEntries, std::vector<std::string> _AMPComboboxEntries, std::vector<std::string> _LevelComboboxEntries)//, std::vector<uint32_t> _AntialiasingComboboxEntries)
			{
				m_sTitle = _sTitle;

				RegisterDialogClass();
				CreateDialogBox();
				AddResolutionComboboxData(_DisplayModeComboboxEntries);
				AddAdapterComboboxData(_AdapterComboboxEntries);
				AddDisplayComboboxData(_DisplayComboboxEntries);
				AddAMPComboboxData(_AMPComboboxEntries);
				//AddAntialiasingComboboxData(_AntialiasingComboboxEntries);
				AddLevelComboboxData(_LevelComboboxEntries);
			}

			void SelectElements(uint32_t _uDisplayModeEntry, uint32_t _uAdapterEntry, uint32_t _uDisplayEntry, uint32_t _uAMPEntry,/*uint32_t _uAntialiasingModeEntry,*/ uint32_t _uLevelEntry, bool _bVsync, bool _bFullscreen, bool _bDebugShaders)
			{
				m_uDisplayModeCBSelectedEntry = SetComboBoxSelection(m_DisplayModeComboHwnd, _uDisplayModeEntry);
				m_uAdapterCBSelectedEntry = SetComboBoxSelection(m_AdapterComboHwnd,_uAdapterEntry);
				m_uDisplayCBSelectedEntry = SetComboBoxSelection(m_DisplayComboHwnd ,_uDisplayEntry);
				m_uAMPCBSelectedEntry = SetComboBoxSelection(m_AMPComboHwnd, _uAMPEntry);
				//m_uAntialiasingCBSelectedEntry = SetComboBoxSelection(m_AntialiasingComboHwnd,_uAntialiasingModeEntry);
				m_uLevelCBSelectedEntry = SetComboBoxSelection(m_LevelComboHwnd,_uLevelEntry);
				m_bVsync = SetCheckboxSelection(m_VSyncHwnd, _bVsync);
				m_bFullscreen = SetCheckboxSelection(m_FullscreenHwnd, _bFullscreen);
				m_bDebugShaders = SetCheckboxSelection(m_DebugShadersHwnd, _bDebugShaders);
			}

			int Run()
			{
				MSG Msg;
				while (GetMessage(&Msg, NULL, 0, 0))
				{
					switch (Msg.message)
					{
					case WM_COMMAND:
					{
						switch (HIWORD(Msg.wParam))
						{
						case CBN_SELCHANGE:
						{
							HWND TmpHwnd = (HWND)Msg.lParam;
							uint32_t uRes = SendMessage(TmpHwnd, (uint32_t)CB_GETCURSEL, (WPARAM)0, (LPARAM)0);
							if (TmpHwnd == m_DisplayModeComboHwnd)
							{
								m_uDisplayModeCBSelectedEntry = uRes;
							}
							else if (TmpHwnd == m_AdapterComboHwnd)
							{
								m_uAdapterCBSelectedEntry = uRes;
							}
							else if (TmpHwnd == m_DisplayComboHwnd)
							{
								m_uDisplayCBSelectedEntry = uRes;
							}
							else if (TmpHwnd == m_AMPComboHwnd)
							{
								m_uAMPCBSelectedEntry = uRes;
							}
							//else if (TmpHwnd == m_AntialiasingComboHwnd)
							//{
							//	m_uAntialiasingCBSelectedEntry = uRes;
							//}
							else if (TmpHwnd == m_LevelComboHwnd)
							{
								m_uLevelCBSelectedEntry = uRes;
							}
						} break;
						case BN_CLICKED:
						{
							HWND TmpHwnd = (HWND)Msg.lParam;
							if (TmpHwnd == m_ButtonHwnd)
							{
								m_bSuccessful = true;
								DestroyWindow(m_WindowHwnd);
							}
							else if (TmpHwnd == m_FullscreenHwnd)
							{
								if (SendMessage(TmpHwnd, BM_GETCHECK, 0, 0))
								{
									m_bFullscreen = true;
								}
								else
								{
									m_bFullscreen = false;
								}
							}
							else if (TmpHwnd == m_VSyncHwnd)
							{
								if (SendMessage(TmpHwnd, BM_GETCHECK, 0, 0))
								{
									m_bVsync = true;
								}
								else
								{
									m_bVsync = false;
								}
							}
							else if (TmpHwnd == m_DebugShadersHwnd)
							{
								if (SendMessage(TmpHwnd, BM_GETCHECK, 0, 0))
								{
									m_bDebugShaders = true;
								}
								else
								{
									m_bDebugShaders = false;
								}
							}
						} break;
						}
					} break;
					default:
					{
						TranslateMessage(&Msg);
						DispatchMessage(&Msg);
					}
					}
				}
				return (int)Msg.wParam;
			}

			const uint32_t GetDisplayModeEntryID() const
			{
				return m_uDisplayModeCBSelectedEntry;
			}
			const uint32_t GetAdapterEntryID() const
			{
				return m_uAdapterCBSelectedEntry;
			}
			const uint32_t GetDisplayEntryID() const
			{
				return m_uDisplayCBSelectedEntry;
			}
			const uint32_t GetAMPEntryID() const
			{
				return m_uAMPCBSelectedEntry;
			}
			//const uint32_t GetAntialiasingEntryID() const
			//{
			//	return m_uAntialiasingCBSelectedEntry;
			//}
			const uint32_t GetLevelEntryID() const
			{
				return m_uLevelCBSelectedEntry;
			}
			const bool GetVSyncState() const
			{
				return m_bVsync;
			}
			const bool GetFullscreenState() const
			{
				return m_bFullscreen;
			}
			const bool GetDebugShadersState() const
			{
				return m_bDebugShaders;
			}

			const bool IsSuccessfullyClosed() const
			{
				return m_bSuccessful;
			}

		private:
			void RegisterDialogClass()
			{
				WNDCLASSEX wc = { 0 };
				wc.cbSize = sizeof(WNDCLASSEX);
				wc.lpfnWndProc = DialogProc;
				wc.hInstance = m_hInstanceParent;
				wc.hbrBackground = GetSysColorBrush(COLOR_3DFACE);
				wc.lpszClassName = TEXT("DialogClass");
				RegisterClassEx(&wc);
			}

			void CreateDialogBox(uint32_t _uXPos = 100, uint32_t _uYPos = 100, uint32_t _uXRes = 500, uint32_t _uYRes = 600)
			{
				m_uXRes = _uXRes;
				m_uYRes = _uYRes;

				const uint32_t uMaxWidth = (m_uXRes - 2 * m_uElementMargin - 15);
				uint32_t uElementOffset = 45;

				// Create main window
				m_WindowHwnd = CreateWindowEx(WS_EX_DLGMODALFRAME | WS_EX_TOPMOST, TEXT("DialogClass"), TEXT(m_sTitle),
					WS_VISIBLE | WS_SYSMENU | WS_CAPTION | CBS_DROPDOWN | CBS_HASSTRINGS | WS_OVERLAPPED, _uXPos, _uYPos, m_uXRes, m_uYRes,
					NULL, NULL, m_hInstanceParent, NULL);


				// Create Combobox label
				// x, y, width, height
				m_DisplayModeComboLabelHwnd = CreateWindowA(WC_STATIC, TEXT(""), WS_CHILD | WS_VISIBLE | WS_TABSTOP,
					m_uElementMargin, uElementOffset, uMaxWidth, m_uElementHeight, m_WindowHwnd, NULL, NULL, NULL);
				SetWindowText(m_DisplayModeComboLabelHwnd, "Resolution:");
				uElementOffset += m_uElementHeight;
				// Create the Resolution Combobox
				// Uses the CreateWindow function to create a child window of 
				// the application window. The WC_COMBOBOX window style specifies  
				// that it is a combobox.
				m_DisplayModeComboHwnd = CreateWindowA(WC_COMBOBOX, TEXT(""), WS_CHILD | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VSCROLL | CBS_DROPDOWNLIST/*CBS_DROPDOWN | CBS_HASSTRINGS | WS_CHILD | WS_OVERLAPPED | WS_VISIBLE */,
					m_uElementMargin, uElementOffset, uMaxWidth, m_uElementHeight * m_uComboboxScrollbarHeight, m_WindowHwnd, NULL, NULL, NULL);
				uElementOffset += m_uElementHeight + m_uElementMargin;


				// Create Display-Selection Combobox label
				// x, y, width, height
				m_DisplayComboLabelHwnd = CreateWindowA(WC_STATIC, TEXT(""), WS_CHILD | WS_VISIBLE | WS_TABSTOP,
					m_uElementMargin, uElementOffset, uMaxWidth, m_uElementHeight, m_WindowHwnd, NULL, NULL, NULL);
				SetWindowText(m_DisplayComboLabelHwnd, "Display:");
				uElementOffset += m_uElementHeight;
				// Create Display-Selection Combobox
				// Uses the CreateWindow function to create a child window of 
				// the application window. The WC_COMBOBOX window style specifies  
				// that it is a combobox.
				m_DisplayComboHwnd = CreateWindowA(WC_COMBOBOX, TEXT(""), WS_CHILD | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VSCROLL | CBS_DROPDOWNLIST/*CBS_DROPDOWN | CBS_HASSTRINGS | WS_CHILD | WS_OVERLAPPED | WS_VISIBLE */,
					m_uElementMargin, uElementOffset, uMaxWidth, m_uElementHeight * m_uComboboxScrollbarHeight, m_WindowHwnd, NULL, NULL, NULL);
				uElementOffset += m_uElementHeight + m_uElementMargin;


				// Create Adapter-Selection Combobox label
				// x, y, width, height
				m_AdapterComboLabelHwnd = CreateWindowA(WC_STATIC, TEXT(""), WS_CHILD | WS_VISIBLE | WS_TABSTOP,
					m_uElementMargin, uElementOffset, uMaxWidth, m_uElementHeight, m_WindowHwnd, NULL, NULL, NULL);
				SetWindowText(m_AdapterComboLabelHwnd, "Graphics Adapter:");
				uElementOffset += m_uElementHeight;
				// Create Adapter-Selection Combobox
				// Uses the CreateWindow function to create a child window of 
				// the application window. The WC_COMBOBOX window style specifies  
				// that it is a combobox.
				m_AdapterComboHwnd = CreateWindowA(WC_COMBOBOX, TEXT(""), WS_CHILD | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VSCROLL | CBS_DROPDOWNLIST/*CBS_DROPDOWN | CBS_HASSTRINGS | WS_CHILD | WS_OVERLAPPED | WS_VISIBLE */,
					m_uElementMargin, uElementOffset, uMaxWidth, m_uElementHeight * m_uComboboxScrollbarHeight, m_WindowHwnd, NULL, NULL, NULL);
				uElementOffset += m_uElementHeight + m_uElementMargin;


				// Create AMP-Selection Combobox label
				// x, y, width, height
				m_AMPComboLabelHwnd = CreateWindowA(WC_STATIC, TEXT(""), WS_CHILD | WS_VISIBLE | WS_TABSTOP,
					m_uElementMargin, uElementOffset, uMaxWidth, m_uElementHeight, m_WindowHwnd, NULL, NULL, NULL);
				SetWindowText(m_AMPComboLabelHwnd, "AMP Accelerator:");
				uElementOffset += m_uElementHeight;
				// Create AMP-Selection Combobox
				// Uses the CreateWindow function to create a child window of 
				// the application window. The WC_COMBOBOX window style specifies  
				// that it is a combobox.
				m_AMPComboHwnd = CreateWindowA(WC_COMBOBOX, TEXT(""), WS_CHILD | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VSCROLL | CBS_DROPDOWNLIST/*CBS_DROPDOWN | CBS_HASSTRINGS | WS_CHILD | WS_OVERLAPPED | WS_VISIBLE */,
					m_uElementMargin, uElementOffset, uMaxWidth, m_uElementHeight * m_uComboboxScrollbarHeight, m_WindowHwnd, NULL, NULL, NULL);
				uElementOffset += m_uElementHeight + m_uElementMargin;


				//// Create Antialiasing-Selection Combobox label
				//// x, y, width, height
				//m_AntialiasingComboLabelHwnd = CreateWindowA(WC_STATIC, TEXT(""), WS_CHILD | WS_VISIBLE | WS_TABSTOP,
				//	m_uElementMargin, uElementOffset, uMaxWidth, m_uElementHeight, m_WindowHwnd, NULL, NULL, NULL);
				//SetWindowText(m_AntialiasingComboLabelHwnd, "Antialiasing:");
				//uElementOffset += m_uElementHeight;
				//// Create Adapter-Selection Combobox
				//// Uses the CreateWindow function to create a child window of 
				//// the application window. The WC_COMBOBOX window style specifies  
				//// that it is a combobox.
				//m_AntialiasingComboHwnd = CreateWindowA(WC_COMBOBOX, TEXT(""), WS_CHILD | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VSCROLL | CBS_DROPDOWNLIST/*CBS_DROPDOWN | CBS_HASSTRINGS | WS_CHILD | WS_OVERLAPPED | WS_VISIBLE */,
				//	m_uElementMargin, uElementOffset, uMaxWidth, m_uElementHeight * m_uComboboxScrollbarHeight, m_WindowHwnd, NULL, NULL, NULL);
				//uElementOffset += m_uElementHeight + m_uElementMargin;

				// Create Level-Selection Combobox label
				// x, y, width, height
				m_LevelComboLabelHwnd = CreateWindowA(WC_STATIC, TEXT(""), WS_CHILD | WS_VISIBLE | WS_TABSTOP,
					m_uElementMargin, uElementOffset, uMaxWidth, m_uElementHeight, m_WindowHwnd, NULL, NULL, NULL);
				SetWindowText(m_LevelComboLabelHwnd, "Level:");
				uElementOffset += m_uElementHeight;
				// Create Level-Selection Combobox
				// Uses the CreateWindow function to create a child window of 
				// the application window. The WC_COMBOBOX window style specifies  
				// that it is a combobox.
				m_LevelComboHwnd = CreateWindowA(WC_COMBOBOX, TEXT(""), WS_CHILD | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VSCROLL | CBS_DROPDOWNLIST/*CBS_DROPDOWN | CBS_HASSTRINGS | WS_CHILD | WS_OVERLAPPED | WS_VISIBLE */,
					m_uElementMargin, uElementOffset, uMaxWidth, m_uElementHeight * m_uComboboxScrollbarHeight, m_WindowHwnd, NULL, NULL, NULL);
				uElementOffset += m_uElementHeight + m_uElementMargin;


				// Create checkbox row

				// Create vsync checkbox
				m_VSyncHwnd = CreateWindowA(WC_BUTTON, TEXT("VSync"),
					WS_VISIBLE | WS_CHILD | BS_AUTOCHECKBOX,
					m_uElementMargin * 2, uElementOffset, 120, m_uElementHeight,
					m_WindowHwnd, (HMENU)1, NULL, NULL);
				//uElementOffset += m_uElementHeight + m_uElementMargin;

				// Create fullscreen checkbox
				m_FullscreenHwnd = CreateWindowA(WC_BUTTON, TEXT("Fullscreen"),
					WS_VISIBLE | WS_CHILD | BS_AUTOCHECKBOX,
					(m_uXRes / 2 - 42 - 15), uElementOffset, 120, m_uElementHeight,
					m_WindowHwnd, (HMENU)1, NULL, NULL);
				//uElementOffset += m_uElementHeight + m_uElementMargin;
				
				// Create debug shader checkbox
				m_DebugShadersHwnd = CreateWindowA(WC_BUTTON, TEXT("Debug Shaders"),
					WS_VISIBLE | WS_CHILD | BS_AUTOCHECKBOX,
					(m_uXRes / 2 + 42 + m_uElementMargin), uElementOffset, 120, m_uElementHeight,
					m_WindowHwnd, (HMENU)1, NULL, NULL);
				uElementOffset += m_uElementHeight + m_uElementMargin;

				// Create start button
				m_ButtonHwnd = CreateWindowA(WC_BUTTON, TEXT("Start"),
					WS_VISIBLE | WS_CHILD,
					(m_uXRes / 2 - 40 - 15), uElementOffset, 80, m_uElementHeight,
					m_WindowHwnd, (HMENU)1, NULL, NULL);
				uElementOffset += m_uElementHeight + m_uElementMargin;
			}

			void AddResolutionComboboxData(std::vector<std::string> _Elements)
			{
				// load the combobox with item list.  
				// Send a CB_ADDSTRING message to load each item

				SendMessage(m_DisplayModeComboHwnd, CB_RESETCONTENT, 0, 0);

				// Shouldn't be necessary anymore cause of the scrollbar
				//RECT Rect;
				//GetWindowRect(m_ComboHwnd, &Rect);		
				//SetWindowPos(m_ComboHwnd, 0, Rect.left, Rect.top, (Rect.right - Rect.left), (m_uElementHeight * (_Elements.size() + 1)), SWP_NOMOVE);


				for (uint32_t i = 0; i < _Elements.size(); ++i) 
				{
					std::string sEntry = _Elements.at(i);

					char buffer[4];
					_itoa_s(i, buffer, 10);

					sEntry += " (" + std::string(buffer) + ")";
					SendMessage(m_DisplayModeComboHwnd, CB_ADDSTRING, 0, (LPARAM)static_cast<const TCHAR*>(sEntry.c_str()));
				}

				//std::vector<std::string>::iterator it = _Elements.begin();
				//std::vector<std::string>::iterator end = _Elements.end();
				//for (; it < end; ++it)
				//{
				//	*it += " (";
				//	SendMessage(m_DisplayModeComboHwnd, CB_ADDSTRING, 0, (LPARAM)static_cast<const TCHAR*>((*it).c_str()));
				//}

				if (_Elements.size() > 0)
				{
					// Send the CB_SETCURSEL message to display an initial item 
					// in the selection field  
					uint32_t uSelection = m_uDisplayModeCBSelectedEntry < _Elements.size() ? m_uDisplayModeCBSelectedEntry : _Elements.size() - 1;
					SendMessage(m_DisplayModeComboHwnd, CB_SETCURSEL, (WPARAM)uSelection/*_Elements.size() - 1*/, (LPARAM)0);
					m_uDisplayModeCBSelectedEntry = static_cast<uint32_t>(SendMessage(m_DisplayModeComboHwnd, (uint32_t)CB_GETCURSEL, (WPARAM)0, (LPARAM)0));
				}
			}
			void AddDisplayComboboxData(std::vector<std::string> _Elements)
			{
				SendMessage(m_DisplayComboHwnd, CB_RESETCONTENT, 0, 0);

				for (uint32_t i = 0; i < _Elements.size(); ++i)
				{
					std::string sEntry = _Elements.at(i);

					char buffer[4];
					_itoa_s(i, buffer, 10);

					sEntry += " (" + std::string(buffer) + ")";
					SendMessage(m_DisplayComboHwnd, CB_ADDSTRING, 0, (LPARAM)static_cast<const TCHAR*>(sEntry.c_str()));
				}

				//std::vector<std::string>::const_iterator it = _Elements.begin();
				//std::vector<std::string>::const_iterator end = _Elements.end();
				//for (; it < end; ++it)
				//{
				//	SendMessage(m_DisplayComboHwnd, CB_ADDSTRING, 0, (LPARAM)static_cast<const TCHAR*>((*it).c_str()));
				//}

				if (_Elements.size() > 0)
				{
					uint32_t uSelection = m_uDisplayCBSelectedEntry < _Elements.size() ? m_uDisplayCBSelectedEntry : _Elements.size() - 1;
					SendMessage(m_DisplayComboHwnd, CB_SETCURSEL, (WPARAM)uSelection/*_Elements.size() - 1*/, (LPARAM)0);
					m_uDisplayCBSelectedEntry = static_cast<uint32_t>(SendMessage(m_DisplayComboHwnd, (uint32_t)CB_GETCURSEL, (WPARAM)0, (LPARAM)0));
				}
			}
			void AddAdapterComboboxData(std::vector<std::string> _Elements)
			{
				SendMessage(m_AdapterComboHwnd, CB_RESETCONTENT, 0, 0);

				for (uint32_t i = 0; i < _Elements.size(); ++i)
				{
					std::string sEntry = _Elements.at(i);

					char buffer[4];
					_itoa_s(i, buffer, 10);

					sEntry += " (" + std::string(buffer) + ")";
					SendMessage(m_AdapterComboHwnd, CB_ADDSTRING, 0, (LPARAM)static_cast<const TCHAR*>(sEntry.c_str()));
				}

				//std::vector<std::string>::const_iterator it = _Elements.begin();
				//std::vector<std::string>::const_iterator end = _Elements.end();
				//for (; it < end; ++it)
				//{
				//	SendMessage(m_AdapterComboHwnd, CB_ADDSTRING, 0, (LPARAM)static_cast<const TCHAR*>((*it).c_str()));
				//}

				if (_Elements.size() > 0)
				{
					uint32_t uSelection = m_uAdapterCBSelectedEntry < _Elements.size() ? m_uAdapterCBSelectedEntry : _Elements.size() - 1;
					SendMessage(m_AdapterComboHwnd, CB_SETCURSEL, (WPARAM)uSelection/*_Elements.size() - 1*/, (LPARAM)0);
					m_uAdapterCBSelectedEntry = static_cast<uint32_t>(SendMessage(m_AdapterComboHwnd, (uint32_t)CB_GETCURSEL, (WPARAM)0, (LPARAM)0));
				}
			}
			void AddAMPComboboxData(std::vector<std::string> _Elements)
			{
				SendMessage(m_AMPComboHwnd, CB_RESETCONTENT, 0, 0);

				for (uint32_t i = 0; i < _Elements.size(); ++i)
				{
					std::string sEntry = _Elements.at(i);

					char buffer[4];
					_itoa_s(i, buffer, 10);

					sEntry += " (" + std::string(buffer) + ")";
					SendMessage(m_AMPComboHwnd, CB_ADDSTRING, 0, (LPARAM)static_cast<const TCHAR*>(sEntry.c_str()));
				}

				if (_Elements.size() > 0)
				{
					uint32_t uSelection = m_uAMPCBSelectedEntry < _Elements.size() ? m_uAMPCBSelectedEntry : _Elements.size() - 1;
					SendMessage(m_AMPComboHwnd, CB_SETCURSEL, (WPARAM)uSelection/*_Elements.size() - 1*/, (LPARAM)0);
					m_uAMPCBSelectedEntry = static_cast<uint32_t>(SendMessage(m_AMPComboHwnd, (uint32_t)CB_GETCURSEL, (WPARAM)0, (LPARAM)0));
				}
			}
			//void AddAntialiasingComboboxData(std::vector<uint32_t> _Elements)
			//{
			//	SendMessage(m_AntialiasingComboHwnd, CB_RESETCONTENT, 0, 0);

			//	for (uint32_t i = 0; i < _Elements.size(); ++i)
			//	{
			//		char buffer[4];
			//		_itoa_s(_Elements.at(i), buffer, 10);

			//		std::string sEntry = std::string(buffer);

			//		_itoa_s(i, buffer, 10);

			//		sEntry += " (" + std::string(buffer) + ")";
			//		SendMessage(m_AntialiasingComboHwnd, CB_ADDSTRING, 0, (LPARAM)static_cast<const TCHAR*>(sEntry.c_str()));
			//	}

			//	//std::vector<uint32_t>::const_iterator it = _Elements.begin();
			//	//std::vector<uint32_t>::const_iterator end = _Elements.end();		
			//	//for (; it < end; ++it)
			//	//{
			//	//	char buffer[4];
			//	//	_itoa_s(*(it), buffer, 10);

			//	//	SendMessage(m_AntialiasingComboHwnd, CB_ADDSTRING, 0, (LPARAM)static_cast<const TCHAR*>(buffer));
			//	//}

			//	if (_Elements.size() > 0)
			//	{
			//		uint32_t uSelection = m_uAntialiasingCBSelectedEntry < _Elements.size() ? m_uAntialiasingCBSelectedEntry : _Elements.size() - 1;
			//		SendMessage(m_AntialiasingComboHwnd, CB_SETCURSEL, (WPARAM)uSelection/*_Elements.size() - 1*/, (LPARAM)0);
			//		m_uAntialiasingCBSelectedEntry = static_cast<uint32_t>(SendMessage(m_AntialiasingComboHwnd, (uint32_t)CB_GETCURSEL, (WPARAM)0, (LPARAM)0));
			//	}
			//}
			void AddLevelComboboxData(std::vector<std::string> _Elements)
			{
				SendMessage(m_LevelComboHwnd, CB_RESETCONTENT, 0, 0);

				for (uint32_t i = 0; i < _Elements.size(); ++i)
				{
					//char buffer[4];
					//_itoa_s(_Elements.at(i), buffer, 10);

					std::string sEntry = _Elements.at(i);// std::string(buffer);

					SendMessage(m_LevelComboHwnd, CB_ADDSTRING, 0, (LPARAM)static_cast<const TCHAR*>(sEntry.c_str()));
				}

				if (_Elements.size() > 0)
				{
					uint32_t uSelection = m_uLevelCBSelectedEntry < _Elements.size() ? m_uLevelCBSelectedEntry : _Elements.size() - 1;
					SendMessage(m_LevelComboHwnd, CB_SETCURSEL, (WPARAM)uSelection/*_Elements.size() - 1*/, (LPARAM)0);
					m_uLevelCBSelectedEntry = static_cast<uint32_t>(SendMessage(m_LevelComboHwnd, (uint32_t)CB_GETCURSEL, (WPARAM)0, (LPARAM)0));
				}
			}
			// Set the currently selected entry in the combobox and returns the number of the selected entry
			uint32_t SetComboBoxSelection(HWND _hComboBox, uint32_t _uSelectedEntry)
			{
				SendMessage(_hComboBox, CB_SETCURSEL, (WPARAM)_uSelectedEntry/*_Elements.size() - 1*/, (LPARAM)0);
				return static_cast<uint32_t>(SendMessage(_hComboBox, (uint32_t)CB_GETCURSEL, (WPARAM)0, (LPARAM)0));
			}
			bool SetCheckboxSelection(HWND _hCheckBox, bool _uState)
			{
				SendMessage(_hCheckBox, BM_SETCHECK, (WPARAM)_uState/*_Elements.size() - 1*/, (LPARAM)0);
				return SendMessage(_hCheckBox, BM_GETCHECK, 0, 0);
			}

		private:
			static LRESULT CALLBACK DialogProc(HWND _hWnd, uint32_t _uMessage, WPARAM _wParam, LPARAM _lParam)
			{
				switch (_uMessage)
				{
				case WM_COMMAND:
				{
					switch (HIWORD(_wParam))
					{
					case CBN_SELCHANGE:
					{
						PostMessage(_hWnd, _uMessage, _wParam, _lParam);
						return 0;
					} break;
					case BN_CLICKED:
					{
						//// Send CB_GETCURSEL message to get the index of the selected list item
						//int ItemIndex = SendMessage((HWND)_lParam, (UINT)CB_GETCURSEL, (WPARAM)0, (LPARAM)0);

						//// Send CB_GETLBTEXT message to get the item
						//wchar_t  ListItem[256];
						//SendMessage((HWND)_lParam, (UINT)CB_GETLBTEXT, (WPARAM)ItemIndex, (LPARAM)ListItem);
						//MessageBox(_hWnd, (LPCSTR)ListItem, TEXT("Item Selected"), MB_OK);
						PostMessage(_hWnd, _uMessage, _wParam, _lParam);
						//DestroyWindow(_hWnd);
						return 0;
					} break;
					}
				} break;

				//case WM_CLOSE: 
				//	DestroyWindow(_hWnd);
				//	break;

				case WM_DESTROY:
					PostQuitMessage(0);
					return 0;
				}
				return (DefWindowProc(_hWnd, _uMessage, _wParam, _lParam));
			}

		private:
			HINSTANCE m_hInstanceParent;

			HWND m_hWndParent;
			HWND m_WindowHwnd;
			HWND m_DisplayModeComboLabelHwnd;
			HWND m_DisplayModeComboHwnd;
			HWND m_DisplayComboLabelHwnd;
			HWND m_DisplayComboHwnd;
			HWND m_AdapterComboLabelHwnd;
			HWND m_AdapterComboHwnd;
			HWND m_AMPComboLabelHwnd;
			HWND m_AMPComboHwnd;
			//HWND m_AntialiasingComboLabelHwnd;
			//HWND m_AntialiasingComboHwnd;
			HWND m_LevelComboLabelHwnd;
			HWND m_LevelComboHwnd;
			HWND m_VSyncHwnd;
			HWND m_FullscreenHwnd;
			HWND m_DebugShadersHwnd;
			HWND m_ButtonHwnd;

			const char* m_sTitle;
			bool m_bSuccessful = false;

			uint32_t m_uXRes;
			uint32_t m_uYRes;

			const uint32_t m_uComboboxScrollbarHeight = 10;
			const uint32_t m_uElementHeight = 25;
			const uint32_t m_uElementMargin = 25;

			uint32_t m_uDisplayModeCBSelectedEntry = 0;
			uint32_t m_uDisplayCBSelectedEntry = 0;
			uint32_t m_uAdapterCBSelectedEntry = 0;
			uint32_t m_uAMPCBSelectedEntry = 0;
			//uint32_t m_uAntialiasingCBSelectedEntry = 0;
			uint32_t m_uLevelCBSelectedEntry = 0;
			bool m_bVsync = false;
			bool m_bFullscreen = false;
			bool m_bDebugShaders = false;
		};


	} // Display
} // Helix

#endif //OPTIONVIEW_H



