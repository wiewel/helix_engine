//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef GPUBUFFER_H
#define GPUBUFFER_H

#include "ViewDefines.h"
#include "hlx\src\Logger.h"
#include <vector>

namespace Helix
{
	namespace Display
	{
		class GPUBuffer
		{
		public:
			HDEBUGNAME("GPUBuffer");

			GPUBuffer() {}
			~GPUBuffer() {}

			GPUBuffer(const GPUBuffer& rhs) = delete;
			GPUBuffer& operator=(const GPUBuffer& rhs) = delete;

			virtual bool Initialize(const BufferDesc& _BufferDesc) = 0;

			virtual bool Apply(const uint8_t* _pData, uint32_t _uSize = 0, uint32_t _uOffset = 0) = 0;

			inline const BufferDesc& GetDescription() const { HASSERTD(m_bInitialized, "Uninitialized buffer!"); return m_Description; }

			inline bool IsInitialized() const { return m_bInitialized; }

		protected:
			BufferDesc m_Description = {};
			bool m_bInitialized = false;
		};

	} // Display
} // Helix
#endif // CONSTANTBUFFER_H
