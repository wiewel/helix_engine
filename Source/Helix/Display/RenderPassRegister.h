//   Copyright 2020 Fabian Wahlster
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef RENDERPASSREGISTER_H
#define RENDERPASSREGISTER_H

#include "Util\UniqueAssociationID.h"
#include "hlx\src\Singleton.h"
#include "hlx\src\Logger.h"
#include "hlx\src\StringHelpers.h" // to_lower

namespace Helix
{
	namespace Display
	{
		class RenderPassRegister : public hlx::Singleton<RenderPassRegister>
		{
		public:
			RenderPassRegister() {}
			~RenderPassRegister() {}

			// call this function at the program startup to ensure the same oder for all associations, independant of run time
			inline void Initialize(const std::vector<std::string>& _RenderPassTypeNames)
			{
				for (const std::string& sTypeName : _RenderPassTypeNames)
				{
					m_Associator(sTypeName);
				}
			}

			inline const uint64_t GetIdentifier(const std::string& _sTypeName)
			{
				std::lock_guard<std::mutex> Lock(m_Mutex);

				uint64_t uBase = m_Associator.GetAssociatedID(hlx::to_lower(_sTypeName));
				HASSERT(uBase < 64u, "Unique render pass type limit 64 exceeded");

				return (1ull << uBase);
			}

			inline size_t GetPassCount() const { return m_Associator.GetAssociationCount(); }

		private:
			std::mutex m_Mutex;
			UniqueAssociationID<std::string, uint32_t> m_Associator;
		};

		struct PassID
		{
			PassID(const std::string& _sTypeName) : sTypeName(_sTypeName) {}

			// lazy evaluation of pass identifier
			inline const uint64_t operator()(void) const
			{
				return Get();
			}

			inline operator uint64_t() const
			{
				return Get();
			}

			inline const uint64_t Get() const
			{
				if (uIdentifier == 0u)
				{
					uIdentifier = RenderPassRegister::Instance()->GetIdentifier(sTypeName);
				}

				return uIdentifier;
			}
		private:
			const std::string sTypeName;
			mutable std::atomic_uint64_t uIdentifier = 0u;
		};

	}// Display
}// Helix

#ifndef HPASS
#define HPASS(TypeName) Helix::Display::RenderPassRegister::Instance()->GetIdentifier(TypeName)
#endif

#endif // !RENDERPASSREGISTER_H