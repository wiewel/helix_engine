//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef RENDERDEVICE_H
#define RENDERDEVICE_H

#include <mutex>

namespace Helix
{
	namespace Display
	{
		class RenderDevice
		{
		public:
			RenderDevice() {}
			~RenderDevice() {}

			virtual void Initialize(const bool _bDebug) = 0;
			virtual void Shutdown() = 0;
			virtual bool IsInitialized() = 0;

			void Lock();
			void Unlock();

		private:
			std::mutex m_Mutex;
		};

		inline void RenderDevice::Lock()
		{
			m_Mutex.lock();
		}

		inline void RenderDevice::Unlock()
		{
			m_Mutex.unlock();
		}
	} // Display
} // Helix

#endif // RENDERDEVICE_H