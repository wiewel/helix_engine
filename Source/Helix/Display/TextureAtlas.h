﻿//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef TEXTUREATLAS_H
#define TEXTUREATLAS_H

#include "Math/MathTypes.h"

namespace Helix
{
	namespace Display
	{
		// forward decls
		struct AtlasEntry
		{
			AtlasEntry() :
				vStartTexCoord(0.f, 0.f),
				vEndTexCoord(0.f, 0.f),
				vViewPortPosition(0.f,0.f),
				vTextureSize(0.f,0.f)
			{
			}
			Math::float2 vStartTexCoord;
			Math::float2 vEndTexCoord;
			Math::float2 vViewPortPosition;
			Math::float2 vTextureSize;
		};

		class TextureAtlas
		{
		public:
			enum AtlasState
			{
				AtlasState_Valid = 0,
				AtlasState_NotEnoughSpace = 1,
				/// possible additions: AtlasState_HalfSize, AtlasState_LogarithmicDownscale, ...
			};

			TextureAtlas();
			~TextureAtlas();

			void NewFrame(Math::float2 _vAtlasSize, uint32_t _uMaxElementCount);
			AtlasState GetNextCoordinates(AtlasEntry& _vTextureCoordinates);

		private:
			Math::float2 m_vCurrentViewportPosition = Math::float2(0.f, 0.f);
			Math::float2 m_vAtlasSize = Math::float2(0.f, 0.f);
			Math::float2 m_vSingleTextureSize = Math::float2(0.f, 0.f);
		};
	} // Display
} // Helix

#endif // !TEXTUREATLAS_H
