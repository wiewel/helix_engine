//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "DXGIManager.h"
#include <stdio.h>
#include "DX11\ViewDefinesDX11.h"

using namespace Helix::Display;

//---------------------------------------------------------------------------------------------------
DXGIManager::DXGIManager()
{
	Initialize();
}
//---------------------------------------------------------------------------------------------------
DXGIManager::~DXGIManager()
{
}
//---------------------------------------------------------------------------------------------------
void DXGIManager::Initialize()
{
	InitDXGIFactory();
	InitDXGIAdapter();
	InitDXGIOutput();
	InitDisplayModes();
}
//---------------------------------------------------------------------------------------------------
void DXGIManager::Shutdown()
{
	if (m_pSwapchain)
	{
		m_pSwapchain->SetFullscreenState(false, nullptr);
	}

	HSAFE_RELEASE(m_pFactory);
	HSAFE_RELEASE(m_pSwapchain);

	HSAFE_MAP_RELEASE(m_Adapters);
	HSAFE_MAP_RELEASE(m_Outputs);
}
//---------------------------------------------------------------------------------------------------
IDXGISwapChain* DXGIManager::InitSwapChain(ID3D11Device* _pDevice, HWND _hWnd, ViewSettings& _ViewSettings/*uint32_t _uAntiAliasing, uint32_t _uMsaaQuality, bool _bFullscreen*/)
{
	HRESULT Result = S_OK;

	DXGI_SWAP_CHAIN_DESC SwapChainDesc;
	ZeroMemory(&SwapChainDesc, sizeof(DXGI_SWAP_CHAIN_DESC));

	SwapChainDesc.BufferCount = _ViewSettings.bFullscreen ? 2 : 1;
	SwapChainDesc.BufferDesc = m_DisplayModes.at(m_uMainDisplayMode);
	SwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	SwapChainDesc.OutputWindow = _hWnd;
	SwapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;

	/*
		-> https://msdn.microsoft.com/en-us/library/windows/desktop/bb173077%28v=vs.85%29.aspx

		DXGI_SWAP_EFFECT:
		To use multisampling with DXGI_SWAP_EFFECT_SEQUENTIAL or DXGI_SWAP_EFFECT_FLIP_SEQUENTIAL, 
		you must perform the multisampling in a separate render target.For example, 
		create a multisampled texture by calling ID3D11Device::CreateTexture2D with a filled D3D11_TEXTURE2D_DESC structure
		(BindFlags member set to D3D11_BIND_RENDER_TARGET and SampleDesc member with multisampling parameters).
		Next call ID3D11Device::CreateRenderTargetView to create a render - target view for the texture, 
		and render your scene into the texture.Finally call ID3D11DeviceContext::ResolveSubresource to resolve 
		the multisampled texture into your non - multisampled swap chain.
	*/

	//if (_ViewSettings.bFullscreen)
	//{ // most of the time flip model is used in fullscreen
		SwapChainDesc.SampleDesc.Count = 1; // A DXGI_SAMPLE_DESC structure that describes multi-sampling parameters. This member is valid only with bit-block transfer (bitblt) model swap chains.
		SwapChainDesc.SampleDesc.Quality = 0;
	//}
	//else
	//{
	//	SwapChainDesc.SampleDesc.Count = _ViewSettings.uAntiAliasing; // A DXGI_SAMPLE_DESC structure that describes multi-sampling parameters. This member is valid only with bit-block transfer (bitblt) model swap chains.
	//	SwapChainDesc.SampleDesc.Quality = _ViewSettings.uMsaaQuality - 1;
	//}

	SwapChainDesc.Windowed = true;		// window or full screen
	SwapChainDesc.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;

	if (FAILED(Result = m_pFactory->CreateSwapChain(_pDevice, &SwapChainDesc, &m_pSwapchain)))
	{
		HR2(Result);
		return nullptr;
	}

	HDXDEBUGNAME(m_pSwapchain, "DXGISwapChain");
	
	return m_pSwapchain;
}
//---------------------------------------------------------------------------------------------------
HRESULT DXGIManager::InitDXGIFactory()
{
	HRESULT Result;

	// Create a DXGIFactory object.
	Result = CreateDXGIFactory1(__uuidof(IDXGIFactory2), (void**)&m_pFactory);
	if (FAILED(Result))
	{
		HFATAL("Can't create DXGIFactory");
	}

	HDXDEBUGNAME(m_pFactory, "DXGIFactory");

	return Result;
}
//---------------------------------------------------------------------------------------------------
HRESULT DXGIManager::InitDXGIAdapter()
{
	HRESULT Result = E_FAIL;

	if (m_pFactory != nullptr)
	{
		// find best graphics adapter for rendering
		IDXGIAdapter1* pCurrentAdapter;

		for (uint32_t i = 0; m_pFactory->EnumAdapters1(i, &pCurrentAdapter) != DXGI_ERROR_NOT_FOUND; ++i)
		{
			m_Adapters.insert({ static_cast<uint32_t>(m_Adapters.size()), pCurrentAdapter });

#ifdef _DEBUG
			char buffer[24];
			_snprintf_s(buffer, 24, "DXGIAdapter%d", i);
			HDXDEBUGNAME(pCurrentAdapter, buffer);
#endif

			Result = S_OK;
		}

		if (m_Adapters.size() > 0)
		{
			m_uMainAdapter = 0;
		}
	}
	else
	{
		HFATAL("DXGIFactory is nullptr!");
	}

	return Result;
}
//---------------------------------------------------------------------------------------------------
HRESULT DXGIManager::InitDXGIOutput()
{
	HRESULT Result = E_FAIL;

	IDXGIAdapter1* pMainAdapter = m_Adapters.at(m_uMainAdapter);

	if (pMainAdapter != nullptr)
	{
		IDXGIOutput* pCurrentOutput;

		for (uint32_t i = 0; pMainAdapter->EnumOutputs(i, &pCurrentOutput) != DXGI_ERROR_NOT_FOUND; ++i)
		{
			m_Outputs.insert({ static_cast<uint32_t>(m_Outputs.size()), pCurrentOutput });

			HDXDEBUGNAME(pCurrentOutput, "DXGIOutput%d", i);

			Result = S_OK;
		}

		if (m_Outputs.size() > 0)
		{
			m_uMainOutput = 0;
		}
	}
	else
	{
		HFATAL("pMainAdapter is nullptr!");
	}

	return Result;
}
//---------------------------------------------------------------------------------------------------
HRESULT DXGIManager::InitDisplayModes()
{
	HRESULT Result = DXGI_ERROR_NOT_CURRENTLY_AVAILABLE;
	uint8_t uTryCount = 0;

	IDXGIOutput* pMainOutput = m_Outputs.at(m_uMainOutput);

	if (pMainOutput != nullptr)
	{
		uint32_t uFlags = 0;
		uint32_t uNumModes = 0;
		DXGI_FORMAT Format = DXGI_FORMAT_B8G8R8A8_UNORM_SRGB;

		while (FAILED(Result) && uTryCount < 3)
		{
			++uTryCount;

			// save number of available display modes in uNumModes and retry on error
			Result = pMainOutput->GetDisplayModeList(Format, uFlags, &uNumModes, nullptr);
			if (FAILED(Result))
			{
				continue;
			} 

			m_DisplayModes.resize(uNumModes);

			Result = pMainOutput->GetDisplayModeList(Format, uFlags, &uNumModes, &m_DisplayModes[0]);
		}
	}

	return Result;
}
//---------------------------------------------------------------------------------------------------