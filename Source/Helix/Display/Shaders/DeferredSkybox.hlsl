//   Copyright 2020 Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef DEFERREDSKYBOX_HLSL
#define DEFERREDSKYBOX_HLSL

#include "ConstantBuffers.hlsli"
#include "Helper.hlsli"
#include "PostEffects.hlsli"

//---------------------------------------------------------------------------------------------------
NAMESPACE(DeferredSkybox)

CBUFFER(cbDeferredSkybox)
float4 vToSunDir;
float3 vSunLightColor;
float vDeferredSkybox1PAD;
float3 vViewDir;
float vDeferredSkybox2PAD;
BUFFER_END

struct VS_IN
{
#ifdef HPMIO_Debug
	float2 vPosition SN(POSITION)
	float2 vTexCoord SN(TEXCOORD)
#else
	uint uID SN(SV_VERTEXID)
#endif
};

struct PS_IN
{
	float4 vPosition SN(SV_POSITION)
	float2 vTexCoord SN(TEXCOORD0)
	float3 vViewRay SN(TEXCOORD1)
#ifdef HPMIO_Lenseflare
	float4 vSunPosition SN(TEXCOORD2)
#endif
};

struct PS_OUT
{
	float4 vAlbedo SN(SV_TARGET0)
};

#ifndef HLSL
enum HPMIOPermutation
{
	HPMIO_Debug = 1 << 0,
	HPMIO_Lenseflare = 1 << 1,
};
#endif

NAMESPACE_END

//---------------------------------------------------------------------------------------------------
#ifdef HLSL
Texture2D<float>    gDepthTex; // linear depth buffer
TextureCube<float3> gSkybox;
Texture2D           gRandomTexture;

//---------------------------------------------------------------------------------------------------
PS_IN VShader(VS_IN _In)
{
	PS_IN Out = (PS_IN)0;

#ifdef HPMIO_Debug
	Out.vPosition = float4(_In.vPosition, 0.0f, 1.0f);
	Out.vTexCoord = _In.vTexCoord;
#else
	PosXYZWTexUV Tri = CalculateFullscreenTriangle(_In.uID);

	Out.vPosition = Tri.vPosition;
	Out.vTexCoord = Tri.vTexCoord;
#endif

	Out.vViewRay = GetViewRay(Out.vPosition.xy);

	/// only execute skybox render when depthbuffer value equals own pixel depth (far plane)
	Out.vPosition.z = Out.vPosition.w;

#ifdef HPMIO_Lenseflare
	Out.vSunPosition = mul(mViewProj, vToSunDir);
	Out.vSunPosition.xyz /= Out.vSunPosition.w;

	/// convert to uv space
	Out.vSunPosition.xy = (Out.vSunPosition.xy + 1.0f) * 0.5f;
#endif

	return Out;
}
//---------------------------------------------------------------------------------------------------
PS_OUT PShader(PS_IN _In)
{
	PS_OUT Output = (PS_OUT)0;

	float fLinDepth = gDepthTex.Sample(SamplerPointClamp, _In.vTexCoord).r;// +fNearDist; // *fFarNearDist;
	float3 vViewSpacePos = _In.vViewRay * fLinDepth * fFarDist;
	float3 vPosWS = mul(mViewInv, float4(vViewSpacePos, 1.0f)).xyz;

	float3 vToSky = normalize(vPosWS - vCameraPos.xyz);

	/// since the alpha channel of the material properties for the diffuse color is set to 0 by default
	/// (and is only changed by DiffuseLighting shader), the skybox color gets drawn unlit by the DeferredMerge shader
	Output.vAlbedo = float4(gSkybox.Sample(SamplerAnisotropicWrap, vToSky).rgb, 1.0f);

#ifdef HPMIO_Lenseflare
	float2 vSunPosUV = _In.vSunPosition.xy;
	vSunPosUV -= 0.5f;

	float2 vUV = _In.vTexCoord.xy;
	vUV.y = 1.0f - vUV.y;
	vUV -= 0.5f;

	/// fade out lensflare effect when sun is starting to face away from view dir
	float3 vLensflare = saturate(dot(vToSunDir.xyz, vViewDir) / 0.2f);
	vLensflare *= Lensflare(vUV, vSunPosUV, vSunLightColor, gRandomTexture);
	Output.vAlbedo.rgb += vLensflare;
#endif

	return Output;
}
//---------------------------------------------------------------------------------------------------
#endif // HLSL

#endif //DEFERREDSKYBOX_HLSL