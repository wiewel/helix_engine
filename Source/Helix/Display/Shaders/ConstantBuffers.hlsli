//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef CONSTANT_BUFFERS_HLSLI
#define CONSTANT_BUFFERS_HLSLI

#include "HelixShaderDefines.h"

NAMESPACE(ConstantBuffers)
//---------------------------------------------------------------------------------------------------
// Lighting Defines:
//---------------------------------------------------------------------------------------------------
#define MAXDIRECTIONALLIGHTS 1
#define MAXPOINTLIGHTS 64
#define MAXSPOTLIGHTS 64

#define MAXPOINTLIGHTSHADOWS 9
#define MAXSPOTLIGHTSHADOWS 9

#define SSAO_KERNEL_SIZE 32
#define NOISE_SIZE 16 // 4*4

struct LightingResult
{
	float3 vDiffuse;
	float3 vSpecular;
};
//---------------------------------------------------------------------------------------------------
// Material Defines
//---------------------------------------------------------------------------------------------------
// List of material properties at end of file
struct MaterialProperties
{
	float3  vAlbedo;        // 12 bytes
	float   fHeightScale;    // 4 bytes

	float2  vTextureTiling;
	float   fMetallic;
	float   fRoughness;

	float3  vEmissiveOffset;
	float   fRoughnessOffset;

	float3  vEmissiveScale;
	float   fRoughnessScale;

	float3  vEmissiveColor;
	float   fHorizonFade; /// a value of 0.f gives default behaviour without Horizon Fade
};
//---------------------------------------------------------------------------------------------------

CBUFFER(cbPerFrame)
	float3 vGlobalAmbient; // needed for final ambient contribution
	float fTime;

	float fDeltaTime;
	float3 vPerFramePAD;
BUFFER_END

CBUFFER(cbPerCamera)
	float4x4 mView; //64
	float4x4 mViewInv; //64

	float4x4 mViewProj; //64
	float4x4 mViewProjInv; //64

	float4x4 mProj; //64
	float4x4 mProjInv; //64

	float4 vCameraPos;
	float4 qCameraOrientation;

	float fNearDist;
	float fFarDist;
	float fFarNearDist;
	float fTHFOV; // tan(fov/2)

	float fAspectRatio;
	float3 vLookDir;

BUFFER_END

CBUFFER(cbPerObject)
	float4x4 mWorld; 
	float4x4 mNormal;
	MaterialProperties MaterialProps;
BUFFER_END

CBUFFERARRAY(cbDirectionalLight)
	float3 vDirection;
	float fDirLightPAD1;

	float3 vColor;
	float fDirLightPAD2;
CBUFFERARRAY_END(cbDirectionalLight, MAXDIRECTIONALLIGHTS);

CBUFFERARRAY(cbPointLight)
	float3      vColor;
	float		fRange; // dmax

	float3      vPosition;
	float       fDecayStart;

	int         iShadowIndex; /// corresponding cbPointLightShadow entry
	float3      POINTLIGHTPAD;
CBUFFERARRAY_END(cbPointLight, MAXPOINTLIGHTS);

CBUFFERARRAY(cbSpotLight)
	float3      vDirection;
	float       fSpotAngle;

	float3      vColor;
	float		fRange; // dmax

	float3      vPosition;
	float       fDecayStart;

	int         iShadowIndex; /// corresponding cbSpotLightShadow entry
	float3      SPOTLIGHTPAD;
CBUFFERARRAY_END(cbSpotLight, MAXSPOTLIGHTS);

CBUFFERARRAY(cbPointLightShadow)
	float4x4 mShadowTransformation; /// view matrix for point light
	float2	 vShadowMapStartTexCoord; /// x,y start position (left top)
	float2	 vShadowMapEndTexCoord;   /// z,w end position (bottom right)
	float	 fLightNearDist;
	float	 fLightFarDist;
	float2	 POINTLIGHTSHADOWPAD;
CBUFFERARRAY_END(cbPointLightShadow, MAXPOINTLIGHTSHADOWS);

CBUFFERARRAY(cbSpotLightShadow)
float4x4 mShadowTransformation; /// view matrix for spot light
float2	 vShadowMapStartTexCoord; /// x,y start position (left top)
float2	 vShadowMapEndTexCoord;   /// z,w end position (bottom right)
float	 fLightNearDist;
float	 fLightFarDist;
float2	 SPOTLIGHTSHADOWPAD;
CBUFFERARRAY_END(cbSpotLightShadow, MAXSPOTLIGHTSHADOWS);

NAMESPACE_END

#endif // CONSTANT_BUFFERS_H