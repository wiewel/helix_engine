//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef HELPER_HLSLI
#define HELPER_HLSLI

//---------------------------------------------------------------------------------------------------
#ifdef HLSL

//---------------------------------------------------------------------------------------------------
static const float kPi = 3.14159265358979323846f;
static const float kPiHalf = 1.57079632f;
static const float kEPSILON = 0.00001f;

//---------------------------------------------------------------------------------------------------
float2 GetUVCoordsFromNDC(float4 _vNDC)
{
	float2 vUV = _vNDC.xy * 0.5f + 0.5f; // ndc to texcoords
	vUV.y = 1.0f - vUV.y; // ndc to texcoords

	return vUV;
}
//---------------------------------------------------------------------------------------------------
struct PosXYZWTexUV
{
	float4 vPosition;
	float2 vTexCoord;
};
//---------------------------------------------------------------------------------------------------
// _uID is most of the times equal to SN(SV_VERTEXID)
PosXYZWTexUV CalculateFullscreenTriangle(uint _uID)
{
	// Alternative:
	//Out.vTexCoord = float2((_In.uID << 1) & 2, _In.uID & 2);
	//Out.vPosition = float4(Out.vTexCoord * float2(2, -2) + float2(-1, 1), 0, 1);

	PosXYZWTexUV Out = (PosXYZWTexUV)0;

	Out.vPosition.x = (float)(_uID / 2) * 4.0f - 1.0f;
	Out.vPosition.y = (float)(_uID % 2) * 4.0f - 1.0f;
	Out.vPosition.z = 0.0f;
	Out.vPosition.w = 1.0f;

	Out.vTexCoord.x = (float)(_uID / 2) * 2.0f;
	Out.vTexCoord.y = 1.0f - (float)(_uID % 2) * 2.0f;

	return Out;
}
//---------------------------------------------------------------------------------------------------
// returns texture dimensions in float
float2 GetTextureDimension(Texture2D _Tex)
{
	uint uWidth;
	uint uHeight;
	_Tex.GetDimensions(uWidth, uHeight);
	return float2(uWidth, uHeight);
}
//---------------------------------------------------------------------------------------------------
// returns texture dimensions in float
float3 GetTextureDimensionCube(TextureCube _Tex)
{
	uint uWidth;
	uint uHeight;
	uint uNumMipMaps;
	_Tex.GetDimensions(0, uWidth, uHeight, uNumMipMaps);
	return float3(uWidth, uHeight, uNumMipMaps);
}
//---------------------------------------------------------------------------------------------------
// calculates view ray based on normalized device coordinates, field of view and aspect ratio - Luna Seite 171
float3 GetViewRay(float2 _vNDC) //, float fTHFOV, float fAspectRatio)
{
	return float3(_vNDC.x * fTHFOV * fAspectRatio, _vNDC.y * fTHFOV, 1.0f);
}
//---------------------------------------------------------------------------------------------------
float LinearizeDepthTexCoord(Texture2D<float> _DepthTex, float2 _vTexCoord)
{
	uint uWidth;
	uint uHeight;
	_DepthTex.GetDimensions(uWidth, uHeight);
	float2 vTexDimension = float2(uWidth, uHeight);

	uint3 PixelCoord = uint3(clamp(_vTexCoord * vTexDimension, float2(0, 0), vTexDimension), 0);
	float fDepth = _DepthTex.Load(PixelCoord).r;
	return mProj._43 / (fDepth - mProj._33);
}
//---------------------------------------------------------------------------------------------------
float LinearizeDepthPixCoord(Texture2D<float> _DepthTex, uint3 _vPixelPos)
{
	float fDepth = _DepthTex.Load(_vPixelPos).r;
	return mProj._43 / (fDepth - mProj._33);
}
//---------------------------------------------------------------------------------------------------
// results in [-1,1]
float3 RandomVector3(Texture1D<float4> _RandomTexture, float _fOffset)
{
	return _RandomTexture.Sample(SamplerPointClamp, fTime + _fOffset).xyz;
}
//---------------------------------------------------------------------------------------------------
// results in [-1,1] on unit sphere
float3 RandomUnitVector3(Texture1D<float4> _RandomTexture, float _fOffset)
{
	return normalize(_RandomTexture.SampleLevel(SamplerPointClamp, fTime + _fOffset, 0).xyz);
}
//---------------------------------------------------------------------------------------------------
// from nvidia soft particles paper
float Contrast(float _fInput, float _fContrastPower)
{
	bool bAboveHalf = (_fInput > 0.5);
	float fOutput = 0.5 * pow(saturate(2 * (bAboveHalf ? 1 - _fInput : _fInput)), _fContrastPower);
	return (bAboveHalf ? 1 - fOutput : fOutput);
}
//---------------------------------------------------------------------------------------------------
#endif // HLSL


#endif // HELPER_HLSLI
