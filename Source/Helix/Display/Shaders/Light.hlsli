//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef LIGHT_HLSLI
#define LIGHT_HLSLI

//#include "HelixShaderDefines.h"
#include "ConstantBuffers.hlsli"
#include "Helper.hlsli"

//---------------------------------------------------------------------------------------------------
//#define PHONGLIGHTING // BLINNPHONGLIGHTING

//---------------------------------------------------------------------------------------------------
#ifdef HLSL

//---------------------------------------------------------------------------------------------------
// LIGHTING
//---------------------------------------------------------------------------------------------------

/// GGX PBS
float2 DirectSpecularGGX_FV(float _fDotLH, float _fRoughness)
{
	float fAlpha = _fRoughness*_fRoughness;

	// F
	float fF_a, fF_b;
	float fDotLH5 = pow(1.0f - _fDotLH, 5);
	fF_a = 1.0f;
	fF_b = fDotLH5;

	// V
	float fK = fAlpha / 2.0f;
	float fK2 = fK * fK;
	float fInvK2 = 1.0f - fK2;
	float fVis = rcp(_fDotLH * _fDotLH * fInvK2 + fK2);

	return float2(fF_a * fVis, fF_b * fVis);
}

float DirectSpecularGGX_D(float _fDotNH, float _fRoughness)
{
	float fAlpha = _fRoughness * _fRoughness;
	float fAlphaSqr = fAlpha * fAlpha;

	/// if fDotNH == 1.0f && fAlphaSqr = 0.f ->  _fDotNH * _fDotNH *(fAlphaSqr - 1.0) = -1.0f -> -1 + 1 = 0
	float fDenom = _fDotNH * _fDotNH *(fAlphaSqr - 1.0) + 1.0f;

	/// -> NaN division
	float fD = fDenom == 0.f ? 0.f : fAlphaSqr / (kPi * fDenom * fDenom);
	return fD;
}

float3 DirectSpecularGGX(float3 _vNormal, float3 _vToEye, float3 _vToLight, float _fRoughness, float3 _vF0)
{
	float3 fH = normalize(_vToEye + _vToLight);

	float fDotLH = saturate(dot(_vToLight, fH));
	float fDotNH = saturate(dot(_vNormal, fH));

	float fD = DirectSpecularGGX_D(fDotNH, _fRoughness);

	float2 vFVHelp = DirectSpecularGGX_FV(fDotLH, _fRoughness);

	// basically a lerp
	float3 vFV = _vF0 * vFVHelp.x + (1.0f - _vF0) * vFVHelp.y;

	float3 vSpecular = fD * vFV;
	return vSpecular;
}

///////// ####################### Future Speedup
/// Store DirectSpecularGGX_D (-> x) and DirectSpecularGGX_FV (-> yz) in one texture (GGX_Optimized_DFV.dds)
/// Use the following code to access them

//float Pow4(float _fX)
//{
//	return _fX*_fX*_fX*_fX;
//}
//float3 DirectSpecularGGX(float3 _vNormal, float3 _vToEye, float3 _vToLight, float _fRoughness, float3 _vF0)
//{
//	float3 fH = normalize(_vToEye + _vToLight);
//
//	float fDotLH = saturate(dot(_vToLight, fH));
//	float fDotNH = saturate(dot(_vNormal, fH));
//
//	float fD = gTexGGX_DFV.Sample(SamplerLinearClamp, float2(Pow4(fDotNH), _fRoughness)).x;
//	float2 vFVHelp = gTexGGX_DFV.Sample(SamplerLinearClamp, float2(fDotLH, _fRoughness)).yz;
//
//	// basically a lerp
//	float3 vFV = _vF0 * vFVHelp.x + (1.0f - _vF0) * vFVHelp.y;
//	/// optimized
//	//float3 vFV = _vF0 * vFVHelp.x + vFVHelp.y;
//
//	float3 vSpecular = fD * vFV;
//	return vSpecular;
//}


//---------------------------------------------------------------------------------------------------
/// Gloss := rough (0) -> smooth (1)
// Schlick gives an approximation to Fresnel reflectance (see pg. 233 "Real-Time Rendering 3rd Ed.").
// R0 = ( (n-1)/(n+1) )^2, where n is the index of refraction.
float3 FresnelSchlick(float3 _vR0, float3 _vNormal, float3 _vToLight)
{
	// R0 ~= 0.04 for dielectrics 
	// R0 = Specular Color for metals (R0 > 0.5)
	float cosIncidentAngle = saturate(dot(_vNormal, _vToLight));
	float f0 = 1.0f - cosIncidentAngle;
	return _vR0 + (1.0f - _vR0)*(f0*f0*f0*f0*f0); // reflect percentage
}
//---------------------------------------------------------------------------------------------------
// Fresnel with additional 'fudge factor' taking care of gloss
// Use only with filtered env maps!
float3 FresnelSchlickWithRoughness(float3 _vSpecularColor, float3 _vToEye, float3 _vNormal, float _fGloss)
{
	/// Lazarov
	return _vSpecularColor + (1.0f - _vSpecularColor) * ((pow(1 - saturate(dot(_vToEye, _vNormal)), 5)) / (4.0f - 3.0f * _fGloss));

	/// Lagarde
	//return _vSpecularColor + (max(_fGloss, _vSpecularColor) - _vSpecularColor) * pow(1 - saturate(dot(_vToEye, _vNormal)), 5);
}
//---------------------------------------------------------------------------------------------------
// _fSpecularExponentScale is the specular exponent value read from the specular texture
// result range is [2,1024] with input [0,1]
float CalculateSpecularExponent(float _fGloss)
{
	/// new formula: SpecularPower = exp2(GlossScale * Gloss + GlossBias)
	/// _fGloss == _fSpecularExponentScale
	/// GlossScale = 10
	/// GlossBias = 1

	return exp2(10.0f * _fGloss + 1.0f);
}

//---------------------------------------------------------------------------------------------------
float3 CalculateDiffuseColor(float3 _vLightColor, float _fDotNL)
{
	return _vLightColor * _fDotNL;
}
//---------------------------------------------------------------------------------------------------
// _vToLight points from vertex being shaded to light source -> LightPos - VertexPos
// _vToEye points from vertex being shaded to camera -> CameraPos - VertexPos
// _vNormal is the vertex normal
float3 CalculateSpecularColor(float3 _vLightColor, float _fDotNL, float3 _vToLight, float3 _vToEye, float3 _vNormal, float _fRoughness, float3 _vF0)
{
	/// PBR approach
	float3 vBRDF = DirectSpecularGGX(_vNormal, _vToEye, _vToLight, _fRoughness, _vF0);
	return vBRDF * _vLightColor * _fDotNL;

//#ifdef PHONGLIGHTING
//	// Phong
//	float3 vR = normalize(reflect( -_vToLight,_vNormal ) );
//	float fRdotV = saturate(dot(vR, _vToEye));
//
//	return _vLightColor * pow(fRdotV, _fSpecularPower);
//#endif
//#ifdef BLINNPHONGLIGHTING
//	// Blinn-Phong
//	float3 vH = normalize(_vToLight + _vToEye);
//	float fNdotH = saturate(dot(_vNormal, vH));
//
//	return _vLightColor * pow(fNdotH, _fSpecularPower);
//#endif
}
//---------------------------------------------------------------------------------------------------
// Helper Functions
// https://imdoingitwrong.wordpress.com/2011/02/10/improved-light-attenuation/
float CalculateAttenuation(float _fSurfacePointToLightDist, float _fRange, float _fDecayStart)
{
	_fDecayStart = max(_fDecayStart, 0.0000001f); // ensure light sphere has valid radius

	float fDmax = max(_fRange - _fDecayStart, 0.f);
	//float fD = min(max(_fSurfacePointToLightDist - _fDecayStart, 0.f), fDmax);
	float fD = max(_fSurfacePointToLightDist - _fDecayStart, 0.f);

	float fD2 = fD / fDmax;
	fD2 *= fD2;

	float fDFactor = ((fD / (1.f - fD2)) / _fDecayStart) + 1.f;
	fDFactor *= fDFactor;

	return fDFactor == 0.0f ? 0.0f : (1.0f / fDFactor) * step(_fSurfacePointToLightDist, fDmax);
}
//---------------------------------------------------------------------------------------------------
// vL = Light vector from vertex pos to light
float CalculateSpotCone(cbSpotLightTYPE _Light, float3 vL)
{
	float fMinCos = cos(_Light.fSpotAngle);
	float fMaxCos = (fMinCos + 1.0f) / 2.0f;
	float fCosAngle = dot(_Light.vDirection.xyz, -vL);
	return smoothstep(fMinCos, fMaxCos, fCosAngle);
}
//---------------------------------------------------------------------------------------------------
// _vToEye points from vertex being shaded to camera -> CameraPos - VertexPos
// _vPosWS is the world space position of the vertex 
// _vNormal is the vertex normal
LightingResult CalculateDirectionalLight(cbDirectionalLightTYPE _Light, float3 _vToEye, float3 _vNormal, float _fRoughness, float3 _vF0) //float3 _vBentNormal,
{
	LightingResult Result;

	// Light vector from vertex pos to light
	float3 vL = -_Light.vDirection.xyz;
	float fDotNL = saturate(dot(_vNormal, vL));

	Result.vDiffuse = CalculateDiffuseColor(_Light.vColor, fDotNL);
	Result.vSpecular = CalculateSpecularColor(_Light.vColor, fDotNL, vL, _vToEye, _vNormal, _fRoughness, _vF0);

//#ifdef HPMPS_BentNormal
//	float fDotBentNL = saturate(dot(_vBentNormal, vL));
//	float fContactShadowing = 1.0f - (saturate((fDotBentNL - fDotNL)));
//	Result.vDiffuse *= fContactShadowing;
//	Result.vSpecular *= fContactShadowing;
//#endif

	return Result;
}
//---------------------------------------------------------------------------------------------------
// _vToEye points from vertex being shaded to camera -> CameraPos - VertexPos
// _vPosWS is the world space position of the vertex 
// _vNormal is the vertex normal
LightingResult CalculatePointLight(cbPointLightTYPE _Light, float3 _vToEye, float3 _vPosWS, float3 _vNormal, float _fRoughness, float3 _vF0) // float3 _vBentNormal,
{
	LightingResult Result;

	// Light vector from vertex pos to light
	float3 vL = _Light.vPosition.xyz - _vPosWS;
	float fPointToLightDist = length(vL);
	vL = vL / fPointToLightDist;
	float fDotNL = saturate(dot(_vNormal, vL));

	// float _fSurfacePointToLightDist, float _fRange, float _fDecayStart
	float fAttenuation = CalculateAttenuation(fPointToLightDist, _Light.fRange, _Light.fDecayStart);

	Result.vDiffuse = CalculateDiffuseColor(_Light.vColor, fDotNL) * fAttenuation;
	Result.vSpecular = CalculateSpecularColor(_Light.vColor, fDotNL, vL, _vToEye, _vNormal, _fRoughness, _vF0) * fAttenuation;

//#ifdef HPMPS_BentNormal
//	float fDotBentNL = saturate(dot(_vBentNormal, vL));
//	float fContactShadowing = 1.0f - (saturate((fDotBentNL - fDotNL)));
//	Result.vDiffuse *= fContactShadowing;
//	Result.vSpecular *= fContactShadowing;
//#endif

	return Result;
}
//---------------------------------------------------------------------------------------------------
// _vToEye points from vertex being shaded to camera -> CameraPos - VertexPos
// _vPosWS is the world space position of the vertex 
// _vNormal is the vertex normal
LightingResult CalculateSpotLight(cbSpotLightTYPE _Light, float3 _vToEye, float3 _vPosWS, float3 _vNormal, float _fRoughness, float3 _vF0) //float3 _vBentNormal,
{
	LightingResult Result;

	// Light vector from vertex pos to light
	float3 vL = _Light.vPosition.xyz - _vPosWS;
	float fPointToLightDist = length(vL);
	vL = vL / fPointToLightDist;
	float fDotNL = saturate(dot(_vNormal, vL));

	float fAttenuation = CalculateAttenuation(fPointToLightDist, _Light.fRange, _Light.fDecayStart);
	float fSpotIntensity = CalculateSpotCone(_Light, vL);

	Result.vDiffuse = CalculateDiffuseColor(_Light.vColor, fDotNL) * fAttenuation * fSpotIntensity;
	Result.vSpecular = CalculateSpecularColor(_Light.vColor, fDotNL, vL, _vToEye, _vNormal, _fRoughness, _vF0) * fAttenuation * fSpotIntensity;

//#ifdef HPMPS_BentNormal
//	float fDotBentNL = saturate(dot(_vBentNormal, vL));
//	float fContactShadowing = 1.0f - (saturate((fDotBentNL - fDotNL)));
//	Result.vDiffuse *= fContactShadowing;
//	Result.vSpecular *= fContactShadowing;
//#endif

	return Result;
}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
// SHADOW
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
/// PCF Sample
float SamplePCF(Texture2D<float> _ShadowMap, float2 _vTexCoord, float _fSceneDepth)
{
	uint uWidth, uHeight, uNumMips;
	_ShadowMap.GetDimensions(0, uWidth, uHeight, uNumMips);

	/// Calculate tex coord delta
	float2 fDXY;
	fDXY.x = 1.0f / (float)uWidth;
	fDXY.y = 1.0f / (float)uHeight;

	float fPercentLit = 0.0f;
	const float2 Offsets[9] =
	{
		float2(-fDXY.x, -fDXY.y),	float2(0.0f, -fDXY.y),	float2(fDXY.x, -fDXY.y),
		float2(-fDXY.x, 0.0f),		float2(0.0f, 0.0f),		float2(fDXY.x, 0.0f),
		float2(-fDXY.x, fDXY.y),	float2(0.0f, fDXY.y),	float2(fDXY.x, fDXY.y)
	};

	[unroll]
	for (int i = 0; i < 9; ++i)
	{
		fPercentLit += _ShadowMap.SampleCmpLevelZero(SamplerShadowMap, _vTexCoord + Offsets[i], _fSceneDepth);
	}

	return fPercentLit / 9.0f;
}


//---------------------------------------------------------------------------------------------------
/// Projection Mapping
//---------------------------------------------------------------------------------------------------
/// Transform World Space Vertex to Projection Space
float4 TransformToProjected(float3 _vPosWS, cbSpotLightShadowTYPE _ShadowProps)
{
	float4 vPosPS = mul(float4(_vPosWS, 1.0f), _ShadowProps.mShadowTransformation);
	vPosPS.xyz /= vPosPS.w;

	/// x and y are in texture space, z is in ndc space
	return vPosPS;
}
//---------------------------------------------------------------------------------------------------
float CalculateProjectedShadowFactor(Texture2D<float> _ShadowMap, float4 _vShadowPosPS, cbSpotLightShadowTYPE _ShadowProps)
{
	float fDepth = _vShadowPosPS.z; // NDC-Space -> [0,1]

	float n = _ShadowProps.fLightNearDist;
	float f = _ShadowProps.fLightFarDist;

	/// Eye space now...
	fDepth = (n * f) / (f - fDepth * (f - n));

	float2 vTexCoord = lerp(_ShadowProps.vShadowMapStartTexCoord, _ShadowProps.vShadowMapEndTexCoord, _vShadowPosPS.xy);

	uint uWidth, uHeight, uNumMips;
	_ShadowMap.GetDimensions(0, uWidth, uHeight, uNumMips);

	/// Calculate tex coord delta
	float2 fDXY;
	fDXY.x = 1.0f / (float)uWidth;
	fDXY.y = 1.0f / (float)uHeight;

	float fPercentLit = 0.0f;
	const float2 Offsets[9] =
	{
		float2(-fDXY.x, -fDXY.y),	float2(0.0f, -fDXY.y),	float2(fDXY.x, -fDXY.y),
		float2(-fDXY.x, 0.0f),		float2(0.0f, 0.0f),		float2(fDXY.x, 0.0f),
		float2(-fDXY.x, fDXY.y),	float2(0.0f, fDXY.y),	float2(fDXY.x, fDXY.y)
	};

	[unroll]
	for (int i = 0; i < 9; ++i)
	{
		float fShadowDepth = _ShadowMap.Sample(SamplerPointClamp, vTexCoord + Offsets[i]);

		/// Eye space now...
		fShadowDepth = (n * f) / (f - fShadowDepth * (f - n));

		fPercentLit += fShadowDepth > fDepth ? 1.0f : 0.0f;
	}

	return fPercentLit / 9.0f;
}
//---------------------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------------------------
/// Paraboloid Mapping
//---------------------------------------------------------------------------------------------------
/// Sample Paraboloid Depth
/// vPosDP is in Dual-Paraboloid-Space -> World-Space vertex position transformed by light view matrix
float CalculateParaboloidShadowFactor(Texture2DArray<float> _ParaboloidMap, float3 _vTexCoordDP, float _fSceneDepthDP, cbPointLightShadowTYPE _ShadowProps)
{
	/// remap texcoords to access shadow atlas on correct coordinates
	/// current vTexCoords are in range [0,1]
	float3 vTexCoord = float3(lerp(_ShadowProps.vShadowMapStartTexCoord, _ShadowProps.vShadowMapEndTexCoord, _vTexCoordDP.xy), _vTexCoordDP.z);

	uint uWidth, uHeight, uElements, uNumMips;
	_ParaboloidMap.GetDimensions(0, uWidth, uHeight, uElements, uNumMips);

	/// Calculate tex coord delta
	float2 fDXY;
	fDXY.x = 1.0f / (float)uWidth;
	fDXY.y = 1.0f / (float)uHeight;

	float fPercentLit = 0.0f;
	const float3 Offsets[9] =
	{
		float3(-fDXY.x, -fDXY.y, 0.0f),	float3(0.0f, -fDXY.y, 0.0f),	float3(fDXY.x, -fDXY.y, 0.0f),
		float3(-fDXY.x, 0.0f, 0.0f),	float3(0.0f, 0.0f, 0.0f),		float3(fDXY.x, 0.0f, 0.0f),
		float3(-fDXY.x, fDXY.y, 0.0f),	float3(0.0f, fDXY.y, 0.0f),		float3(fDXY.x, fDXY.y, 0.0f)
	};

	[unroll]
	for (int i = 0; i < 9; ++i)
	{
		fPercentLit += _ParaboloidMap.SampleCmpLevelZero(SamplerShadowMap, vTexCoord + Offsets[i], _fSceneDepthDP);
	}

	return fPercentLit / 9.0f;
}
//---------------------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------------------------
/// Orthographic Shadow Map
//---------------------------------------------------------------------------------------------------
// _vShadowPosH is in projective shadow transformed space [mul(vVertexPosWS, mShadowTransform)]
float CalculateShadowFactor(Texture2D<float> _ShadowMap, float4 _vShadowPosH)
{
	_vShadowPosH.xyz /= _vShadowPosH.w;
	float fDepth = _vShadowPosH.z; // NDC-Space -> [0,1]

	uint uWidth, uHeight, uNumMips;
	_ShadowMap.GetDimensions(0, uWidth, uHeight, uNumMips);

	float fDX = 1.0f / (float)uWidth;

	float fPercentLit = 0.0f;
	const float2 Offsets[9] =
	{
		float2(-fDX, -fDX), float2(0.0f, -fDX), float2(fDX, -fDX),
		float2(-fDX, 0.0f), float2(0.0f, 0.0f), float2(fDX, 0.0f),
		float2(-fDX, fDX), float2(0.0f, fDX), float2(fDX, fDX)
	};

	[unroll]
	for(int i = 0; i < 9; ++i)
	{
		fPercentLit += _ShadowMap.SampleCmpLevelZero(SamplerShadowMap, _vShadowPosH.xy + Offsets[i], fDepth).r;
	}

	return fPercentLit / 9.0f;
}
//---------------------------------------------------------------------------------------------------



#endif // HLSL


#endif // LIGHT_HLSLI
