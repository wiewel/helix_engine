//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef SOLID_HLSL
#define SOLID_HLSL

#include "ConstantBuffers.hlsli"
#include "Texture.hlsli"
#include "Light.hlsli"

//---------------------------------------------------------------------------------------------------
NAMESPACE(Solid)

CBUFFER(cbSolid)
//float2 vTexFlowDir;
float  fImplosionState; // should decrease from x -> 0 over time
float  fTailLength; // scales the tail length of a moving object
BOOL   bImplode;
BOOL   bTail;

float  fEffectStrength;
float  fEffectRadius;
float  fEffectProgress;
float  SOLIDPADDING;

float4 vVelocity;
BUFFER_END

struct VS_IN
{
	float3 vPosition SN(POSITION)
	float3 vNormal SN(NORMAL)
	float3 vTangent SN(TANGENT)
	float2 vTexCoord SN(TEXCOORD)
};

struct PS_IN
{
	float4 vPosition SN(SV_POSITION)
	float3 vPosWS SN(POSITION)
	float3 vNormal SN(NORMAL)
	float3 vTangent SN(TANGENT)
	float2 vTexCoord SN(TEXCOORD0)
	float3x3 mTexSpace SN(TEXCOORD2)
	float fDepth SN(TEXCOORD9)
};

struct PS_OUT
{
	float4 vAlbedo SN(SV_TARGET0)
	float2 vNormalWS SN(SV_TARGET1)
	float2 vMetallicHorizon SN(SV_TARGET2)
	float4 vEmissiveColor SN(SV_TARGET3)
	float fRoughness SN(SV_TARGET4)
	float fLinearDepth SN(SV_TARGET5)
};

#ifndef HLSL
//enum HPMVSPermutation : uint32_t
//{
//	HPMVS_TextureFlow = 1 << 0,
//};
enum HPMPSPermutation : uint32_t
{
	HPMPS_NormalMap = 1 << 0,
	HPMPS_ParallaxMap = 1 << 1,
	HPMPS_MetallicMap = 1 << 2,
	HPMPS_RoughnessMap = 1 << 3,
	HPMPS_AlbedoMap = 1 << 4,
	HPMPS_EmissiveMap = 1 << 5,
};
#endif

NAMESPACE_END
//---------------------------------------------------------------------------------------------------
#ifdef HLSL

Texture2D<float4> gAlbedoMap;
Texture2D<float3> gNormalMap;
Texture2D<float> gMetallicMap;
Texture2D<float> gRoughnessMap;
Texture2D<float> gHeightMap;
Texture2D<float> gEmissiveMap;


//---------------------------------------------------------------------------------------------------
PS_IN VShader(VS_IN _In) //, uint uID : SV_VertexId
{
	PS_IN Output = (PS_IN) 0;

	float4 vInPos = float4(_In.vPosition, 1.0f);
	float4 vInNor = float4(_In.vNormal, 0.0f);
	float4 vInTan = float4(_In.vTangent, 0.0f);

	Output.vPosWS = mul(mWorld, vInPos).xyz;

	Output.vPosition = mul(mView, float4(Output.vPosWS, 1.0f));
	Output.fDepth = Output.vPosition.z;// Output.vPosition.w + fNearDist;// / fFarNearDist;

	Output.vPosition = mul(mViewProj, float4(Output.vPosWS, 1.0f));

	float3 vNormal = normalize(mul(mNormal, vInNor).xyz);
	Output.vNormal = vNormal;

	float3 vTangent = normalize(mul(mWorld, vInTan).xyz);
	Output.vTangent = vTangent;

#ifdef HPMVS_TextureFlow
	Output.vTexCoord = _In.vTexCoord + vTexFlowDir;// *sin(fTime);
#else
	Output.vTexCoord = _In.vTexCoord;
#endif

	//vTangent = normalize(Output.vTangent - dot(Output.vTangent, Output.vNormal) * Output.vNormal);
	float3 vBiNormal = cross(vNormal, vTangent);

	//Create the "Texture Space"
	Output.mTexSpace = float3x3(vTangent, vBiNormal, vNormal);

	return Output;
}

//---------------------------------------------------------------------------------------------------
float hash(float2 p)
{
	float h = dot(p, float2(127.1f, 311.7f));
	return frac(sin(h)*43758.5453123f);
}

float noise(in float2 p)
{
	float2 i = floor(p);
	float2 f = frac(p);
	float2 u = f*f*(3.0f - 2.0f*f);
	return -1.0f + 2.0f * lerp(
		lerp(hash(i + float2(0.0f, 0.0f)),
			hash(i + float2(1.0f, 0.0f)),
			u.x),
		lerp(hash(i + float2(0.0f, 1.0f)),
			hash(i + float2(1.0f, 1.0f)),
			u.x),
		u.y);
}

[maxvertexcount(3)]
void GShader(triangle PS_IN Input[3], inout TriangleStream<PS_IN> Output)
{
	float3 vDifferenceVector1 = Input[0].vPosWS - Input[2].vPosWS;
	float3 vDifferenceVector2 = Input[1].vPosWS - Input[2].vPosWS;
	float3 vSurfaceNormal = normalize(cross(vDifferenceVector1, vDifferenceVector2));

	PS_IN NewPoint = (PS_IN) 0;

	float3 vNegVelocity = vVelocity.xyz;

	/// Vortex
	/// Spiral Mapping
	float fAngle = fEffectStrength;// 8.0f;// *8.0f;
	float fSpiralRadius = fEffectRadius;// 2.0f;

	float3 vNoise;
	vNoise.x = noise(Input[0].vPosWS.xy);
	vNoise.y = noise(Input[1].vPosWS.xz);
	vNoise.z = noise(Input[2].vPosWS.yx);
	float3 vNoiseNormal = normalize(vSurfaceNormal + vNoise);

	float3 vCurPositionOffset = vNoiseNormal * fEffectRadius * fEffectProgress;
	float fDistance = length(vCurPositionOffset);

	float fAnimationProgress = (fSpiralRadius - fDistance) / fSpiralRadius;
	float fTheta = normalize(vNoise.x + vNoise.y) * fAnimationProgress * fAnimationProgress * fAngle;

	float fCosTheta = cos(fTheta);
	float fSinTheta = sin(fTheta);

	float3x3 S = float3x3(float3(fCosTheta, -fSinTheta, 0.0f), float3(fSinTheta, fCosTheta, 0.0f), float3(0.0f, 0.0f, 1.0f));

	for (int i = 0; i < 3; ++i)
	{
		float3 vSpiralOrigin = Input[i].vPosWS.xyz;
		float3 vNewPos = vSpiralOrigin;

		if(bImplode)
		{
			vNewPos += mul(S, vCurPositionOffset);
		}

		NewPoint.vPosWS = vNewPos;

		NewPoint.vPosition = mul(mView, float4(NewPoint.vPosWS, 1.0f));
		NewPoint.fDepth = NewPoint.vPosition.z;
		NewPoint.vPosition = mul(mViewProj, float4(NewPoint.vPosWS, 1.0f));
		NewPoint.vNormal = Input[i].vNormal;
		NewPoint.vTangent = Input[i].vTangent;
		NewPoint.vTexCoord = Input[i].vTexCoord;
		NewPoint.mTexSpace = Input[i].mTexSpace;

		Output.Append(NewPoint);
	}
}



//---------------------------------------------------------------------------------------------------
PS_OUT PShader(PS_IN _In)
{
	PS_OUT Output = (PS_OUT) 0;

	Output.fLinearDepth = _In.fDepth / fFarDist;

	_In.vNormal = normalize(_In.vNormal);

	float2 vTexCoord = _In.vTexCoord * MaterialProps.vTextureTiling.xy;

	float3 vNormal = _In.vNormal;
	float3 vTangent = normalize(_In.vTangent);

	float3 vViewDir = normalize( (vCameraPos.xyz - _In.vPosWS) );

#ifdef HPMPS_ParallaxMap
	// Parallax Mapping
	vTexCoord = ReliefMapping(vViewDir, vNormal, vTangent, vTexCoord, MaterialProps.fHeightScale, gHeightMap, SamplerAnisotropicWrap);
#endif

#ifdef HPMPS_NormalMap
	// Normalmap sampling and calculation of normal
	float3 vNormalCol = gNormalMap.Sample(SamplerAnisotropicWrap, vTexCoord).xyz;

	vNormalCol = (vNormalCol*2.0f) - 1.0f; // expand range from [0,1] to [-1,1]
			
	// Moved to VS, keep commented code here to understand whats happening...
	//vTangent = normalize(vTangent - dot(vTangent, vNormal) * vNormal);
	//float3 vBiTangent = cross(vTangent, vNormal);

	//Create the "Texture Space"
	vNormal = mul(vNormalCol, _In.mTexSpace).xyz;
	vNormal = normalize(vNormal);
#endif // HPMPS_NormalMap

	Output.vNormalWS = NormalEncode(vNormal);

#ifdef HPMPS_AlbedoMap
	float3 vTexColor = gAlbedoMap.Sample(SamplerAnisotropicWrap, vTexCoord).rgb;
	Output.vAlbedo = float4(vTexColor, 1.0f);
#else
	Output.vAlbedo = float4(MaterialProps.vAlbedo, 1.0f);
#endif

#ifdef HPMPS_MetallicMap
	Output.vMetallicHorizon.x = gMetallicMap.Sample(SamplerAnisotropicWrap, vTexCoord);
#else
	Output.vMetallicHorizon.x = MaterialProps.fMetallic;
#endif

	/// Subsurface normal light leak fix -> http://marmosetco.tumblr.com/post/81245981087
	/// Reflect view dir on "corrected" normal with normal map influence -> Reflection now may reach under surface
	float3 vReflection = reflect(-vViewDir, vNormal);
	/// Do a horizon check by building a dot product between the "incorrect" vertex normal and the reflection vector
	/// ! Remember to square the value when using it later on !
	Output.vMetallicHorizon.y = saturate(1.0f + MaterialProps.fHorizonFade * dot(vReflection, _In.vNormal));

#ifdef HPMPS_RoughnessMap
	Output.fRoughness = gRoughnessMap.Sample(SamplerAnisotropicWrap, vTexCoord) + MaterialProps.fRoughnessOffset;
	Output.fRoughness *= MaterialProps.fRoughnessScale;
#else
	Output.fRoughness = MaterialProps.fRoughness;
#endif

#ifdef HPMPS_EmissiveMap
	Output.vEmissiveColor = gEmissiveMap.Sample(SamplerAnisotropicWrap, vTexCoord) + float4(MaterialProps.vEmissiveOffset, 0.0f);
	Output.vEmissiveColor *= float4(MaterialProps.vEmissiveScale, 1.0f);
#else
	Output.vEmissiveColor = float4(MaterialProps.vEmissiveColor, 1.0f);
#endif

	return Output;
}
//---------------------------------------------------------------------------------------------------

#endif //HLSL

#endif //SOLID_HLSL
