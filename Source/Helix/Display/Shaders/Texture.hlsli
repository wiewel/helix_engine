//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef TEXTUREFUNCTIONS_HLSLI
#define TEXTUREFUNCTIONS_HLSLI

#include "HelixShaderDefines.h"
#include "Helper.hlsli"

#ifdef HLSL

//---------------------------------------------------------------------------------------------------
// Returns relief mapped UV-coordinates (similar to parallax mapping)
float2 ReliefMapping(float3 _vPosToEye, float3 _vNormal,
					 float3 _vTangent, float2 _vTexCoord,
					 float _fHeightScale, Texture2D<float> _HeightMap, SamplerState _Sampler)
{
	float3 vT = normalize(_vTangent - dot(_vTangent, _vNormal) * _vNormal);
	float3 vB = cross(_vNormal, vT);

	float a = dot(_vNormal, -_vPosToEye);
	float3 s = float3(
		dot(_vPosToEye, _vTangent),
		dot(_vPosToEye, vB),
		a);
	s *= _fHeightScale / a * 0.1f;
	float2 ds = s.xy;

	// current depth position
	float fDepth = 0.0f;

	const int linear_search_steps = 100;
	const int binary_search_steps = 25;

	// current size of search window
	float fSize = 1.0 / float(linear_search_steps);

	// search front to back for first point inside object
	for (int i = 0; i < linear_search_steps; ++i)
	{
		float fT = _HeightMap.Sample(_Sampler, _vTexCoord + ds*fDepth).r;

		if (fDepth < (1.0 - fT))
			fDepth += fSize;
	}
	// recurse around first point (fDepth) for closest match
	for (int ii = 0; ii < binary_search_steps; ++ii)
	{
		fSize *= 0.5;
		float fT = _HeightMap.Sample(_Sampler, _vTexCoord + ds*fDepth).r;
		if (fDepth < (1.0 - fT))
			fDepth += (2.0 * fSize);
		fDepth -= fSize;
	}

	return _vTexCoord + ds * fDepth;
}



//---------------------------------------------------------------------------------------------------
// Returns parallax mapped UV-coordinates to use for diffuse and normal map sampling
// float3 _vPosToEye	=> CameraPos - VertexWorldPos
// float3 _vNormal		=> VertexNormal
// float3 _vTangent		=> VertexTangent
// float2 _vTexCoord	=> VertexTexCoord
// float _fHeightScale	=> Scaling factor for max parallax offset
// Texture2D<float> _HeightMap	=> 1-channel texture with heightmap in r channel
// SamplerState _Sampler	=> Sampler to sample _HeightMap
float2 ParallaxMapping(	float3 _vPosToEye, float3 _vNormal,
						float3 _vTangent, float2 _vTexCoord,
						float _fHeightScale, Texture2D<float> _HeightMap, SamplerState _Sampler)
{
	//float3 vToEye = vCameraPos.xyz - _vPosW;
	float3 vViewDirW = -_vPosToEye;

	float3 vT = normalize(_vTangent - dot(_vTangent, _vNormal) * _vNormal);
	float3 vB = cross(_vNormal, vT); //_vBiTangent;

	float3x3 vToTangent = transpose(float3x3(vT, vB, _vNormal));

	float3 vViewDirTS = mul(vViewDirW, vToTangent);

	float2 vMaxParallaxOffset = -vViewDirTS.xy * _fHeightScale / vViewDirTS.z;

	int iMaxSampleCount = 72;
	int iMinSampleCount = 8;

	int iSampleCount = (int)lerp(iMaxSampleCount, iMinSampleCount, dot(_vPosToEye, _vNormal));

	float fZStep = 1.0f / (float)iSampleCount;
	
	float2 vTexStep = vMaxParallaxOffset * fZStep;

	// Precompute texture gradients since we cannot compute texture
	// gradients in a loop. Texture gradients are used to select the right
	// mipmap level when sampling textures. Then we use Texture2D.SampleGrad()
	// instead of Texture2D.Sample().

	float2 vDX = ddx(_vTexCoord);
	float2 vDY = ddy(_vTexCoord);
	int iSampleIndex = 0;
	float2 vCurTexOffset = 0;
	float2 vPrevTexOffset = 0;
	float2 vFinalTexOffset = 0;
	float fCurRayZ = 1.0f - fZStep;
	float fPrevRayZ = 1.0f;
	float fCurHeight = 0.0f;
	float fPrevHeight = 0.0f;

	// Ray trace the heightfield.
	while (iSampleIndex < iSampleCount + 1)
	{
		fCurHeight = _HeightMap.SampleGrad(_Sampler, _vTexCoord + vCurTexOffset, vDX, vDY).r;
		// Did we cross the height profile?
		if (fCurHeight > fCurRayZ)
		{
			// Do ray/line segment intersection and compute final tex offset.
			float fT = (fPrevHeight - fPrevRayZ) / (fPrevHeight - fCurHeight + fCurRayZ - fPrevRayZ);
			vFinalTexOffset = vPrevTexOffset + fT * vTexStep;
			// Exit loop
			iSampleIndex = iSampleCount + 1;
		}
		else
		{
			++iSampleIndex;
			vPrevTexOffset = vCurTexOffset;
			fPrevRayZ = fCurRayZ;
			fPrevHeight = fCurHeight;
			vCurTexOffset += vTexStep;
			// Negative because we are going "deeper" into the surface
			fCurRayZ -= fZStep;
		}
	}
	// Use these texture coordinates for subsequent texture
	// fetches (color map, normal map, etc.).
	return _vTexCoord + vFinalTexOffset;
}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
/// Paraboloid Mapping
//---------------------------------------------------------------------------------------------------
/// Transform World Space Vertex to Dual-Paraboloid Space
float4 TransformToParaboloid(float3 _vPosWS, cbPointLightShadowTYPE _ShadowProps)//float4x4 _mViewMatrix, float _fNearDist, float _fFarDist)
{
	float4 vPosDP = mul(_ShadowProps.mShadowTransformation, float4(_vPosWS, 1.0f));
	float L = length(vPosDP.xyz);
	vPosDP.xyz /= L;
	/// Depth in DP-Space
	vPosDP.w = (L - _ShadowProps.fLightNearDist) / (_ShadowProps.fLightFarDist - _ShadowProps.fLightNearDist);
	return vPosDP;
}
//---------------------------------------------------------------------------------------------------
/// Transform Dual-Paraboloid space position to texture coordinate
float3 ConvertParaboloidToTexCoord(float4 _vPosDP)
{
	float3 vTexCoord;

	if (_vPosDP.z >= 0.0f)
	{
		/// Front
		vTexCoord.x = (_vPosDP.x / (1.0f + _vPosDP.z)) * 0.5f + 0.5f;
		vTexCoord.y = 1.0f - ((_vPosDP.y / (1.0f + _vPosDP.z)) * 0.5f + 0.5f);
		vTexCoord.z = 0;
	}
	else
	{
		/// Back
		vTexCoord.x = (_vPosDP.x / (1.0f - _vPosDP.z)) * 0.5f + 0.5f;
		vTexCoord.y = 1.0f - ((_vPosDP.y / (1.0f - _vPosDP.z)) * 0.5f + 0.5f);
		vTexCoord.z = 1;
	}

	return vTexCoord;
}




//---------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------
/// View Space Only Normal Storage Types
///#define NORMAL_SPHEREMAP_TRANSFORM /// best: similar used in CE3
///#define NORMAL_STEREOGRAPHIC_PROJECTION
/// Arbitrary Normal Storage Types
#define NORMAL_SPHERICAL_COORDINATES
//---------------------------------------------------------------------------------------------------
#ifdef NORMAL_SPHEREMAP_TRANSFORM
//---------------------------------------------------------------------------------------------------
// Returns Lambert Azimuthal Equal-Area encoded normal
float2 NormalEncode(float3 _vNormal)
{
	float fP = sqrt(_vNormal.z * 8.0f + 8.0f);
	return float2(_vNormal.xy / fP + 0.5f);
}
//---------------------------------------------------------------------------------------------------
// Returns decoded Lambert Azimuthal Equal-Area encoded normal
float3 NormalDecode(float2 _vEncodedNormal)
{
	float2 vFenc = _vEncodedNormal * 4.0f - 2.0f;
	float fF = dot(vFenc, vFenc);
	float fG = sqrt(1.0f - fF / 4.0f);
	float3 vNormal;
	vNormal.xy = vFenc * fG;
	vNormal.z = 1.0f - fF / 2.0f;
	return vNormal;
}
//---------------------------------------------------------------------------------------------------
#elif defined(NORMAL_STEREOGRAPHIC_PROJECTION)
//---------------------------------------------------------------------------------------------------
// Returns stereographic projection encoded normal
float2 NormalEncode(float3 _vNormal)
{
	float fScale = 1.7777777f; /// depends on FoV and how much normals point away from camera
	float2 vEnc = _vNormal.xy / (_vNormal.z + 1.0f);
	vEnc /= fScale;
	vEnc = vEnc * 0.5f + 0.5f;
	return vEnc;
}
//---------------------------------------------------------------------------------------------------
// Returns decoded stereographic projection encoded normal
float3 NormalDecode(float2 _vEncodedNormal)
{
	float fScale = 1.7777777f; /// depends on FoV and how much normals point away from camera
	float3 vNN = _vEncodedNormal.xyz * float3(2.0f * fScale, 2.0f * fScale, 0.0f) + float3(-fScale, -fScale, 1.0f);
	float fG = 2.0f / dot(vNN.xyz, vNN.xyz);

	float3 vNormal;
	vNormal.xy = fG * vNN.xy;
	vNormal.z = fG - 1.0f;

	return vNormal;
}
//---------------------------------------------------------------------------------------------------
#elif defined(NORMAL_SPHERICAL_COORDINATES)
//---------------------------------------------------------------------------------------------------
// Returns spherical coordinates encoded normal
// Converts a normalized cartesian direction vector to spherical coordinates
float2 NormalEncode(float3 _vNormal)
{
	/// UNorm
	//return (float2(atan2(_vNormal.y, _vNormal.x) / kPi, _vNormal.z) + 1.0f) * 0.5f;
	/// SNorm
	return float2(atan2(_vNormal.y, _vNormal.x) / kPi, _vNormal.z);
}
//---------------------------------------------------------------------------------------------------
// Returns decoded spherical coordinates encoded normal
float3 NormalDecode(float2 _vEncodedNormal)
{
	/// UNorm
	//float2 vAngles = _vEncodedNormal * 2.0f - 1.0f;
	/// SNorm
	float2 vAngles = _vEncodedNormal;
	float2 vSinCosTheta;
	sincos(vAngles.x * kPi, vSinCosTheta.x, vSinCosTheta.y);
	float2 vSinCosPhi = float2(sqrt(1.0f - vAngles.y * vAngles.y), vAngles.y);
	return float3(vSinCosTheta.y * vSinCosPhi.x, vSinCosTheta.x * vSinCosPhi.x, vSinCosPhi.y);
}
//---------------------------------------------------------------------------------------------------
#endif

#endif // HLSL

#endif // TEXTUREFUNCTIONS_HLSLI