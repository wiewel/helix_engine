//   Copyright 2020 Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef DEFERREDMERGE_HLSL
#define DEFERREDMERGE_HLSL

#include "ConstantBuffers.hlsli"
#include "Helper.hlsli"
#include "Light.hlsli"

//---------------------------------------------------------------------------------------------------
NAMESPACE(DeferredMerge)

struct VS_IN
{
#ifdef HPMIO_Debug
	float2 vPosition SN(POSITION)
	float2 vTexCoord SN(TEXCOORD)
#else
	uint uID SN(SV_VERTEXID)
#endif
};

struct PS_IN
{
	float4 vPosition SN(SV_POSITION)
	float2 vTexCoord SN(TEXCOORD0)
	float3 vViewRay SN(TEXCOORD1)
};

struct PS_OUT
{
	float4 vMergeOutput SN(SV_TARGET0)
};

#ifndef HLSL
enum HPMIOPermutation : uint32_t
{
	HPMIO_Debug = 1 << 0,
};
enum HPMPSPermutation : uint32_t
{
	HPMPS_SSAO = 1 << 0,
	//HPMPS_EnvironmentMapping = 1 << 1,
	//HPMPS_BentNormal = 1 << 2,
};
#endif

NAMESPACE_END

//---------------------------------------------------------------------------------------------------
#ifdef HLSL

//Texture2D<float>    gDepthTex; // linear depth buffer

Texture2D<float4>   gAlbedoMap; // diffuse mat color
Texture2D<float4>   gDiffuseLightMap;
Texture2D<float4>   gSpecularLightMap;
Texture2D<float4>   gEmissiveLightMap;
Texture2D<float>    gSSAOMap;

//Texture2D<float4>   gBentNormalMap;
//TextureCube<float4> gEnvMap;

//---------------------------------------------------------------------------------------------------
PS_IN VShader(VS_IN _In)
{
	PS_IN Out = (PS_IN)0;

#ifdef HPMIO_Debug
	Out.vPosition = float4(_In.vPosition, 0.0f, 1.0f);
	Out.vTexCoord = _In.vTexCoord;
#else
	PosXYZWTexUV Tri = CalculateFullscreenTriangle(_In.uID);

	Out.vPosition = Tri.vPosition;
	Out.vTexCoord = Tri.vTexCoord;
#endif

	Out.vViewRay = GetViewRay(Out.vPosition.xy);

	return Out;
}
//---------------------------------------------------------------------------------------------------
PS_OUT PShader(PS_IN _In)
{
	PS_OUT Output;

	SamplerState Sampler = SamplerLinearClamp;

	float3 vUnlitColor = gAlbedoMap.Sample(Sampler, _In.vTexCoord).rgb;

	// MaterialParams
	float4 vDirectDiffuseLight = float4(0, 0, 0, 0); // refers to light from direct sources that is reflected diffusely
	float3 vIndirectDiffuseLight = float3(0, 0, 0); // refers to light from indirect sources that is reflected diffusely -> ambient light
	float3 vDirectSpecularLight = float3(0, 0, 0); // refers to light from direct sources that is reflected specularly (specular map)
	float3 vIndirectSpecularLight = float3(0, 0, 0); // refers to light from indirect sources that is reflected specularly [not supported yet]
	float3 vEmissiveLight = float3(0, 0, 0);

	//vIndirectDiffuseLight = gAmbientLightMap.Sample(Sampler, _In.vTexCoord).rgb * vGlobalAmbient.rgb;
	vIndirectDiffuseLight = vGlobalAmbient.rgb;

#ifdef HPMPS_SSAO
	float fVisibility = gSSAOMap.Sample(Sampler, _In.vTexCoord);
	vIndirectDiffuseLight *= fVisibility;
#endif


	vDirectDiffuseLight = gDiffuseLightMap.Sample(Sampler, _In.vTexCoord).rgba;
	vEmissiveLight = gEmissiveLightMap.Sample(Sampler, _In.vTexCoord).rgb;
	vDirectSpecularLight = gSpecularLightMap.Sample(Sampler, _In.vTexCoord).rgb;

	float3 vLitColor = (vEmissiveLight.rgb + vDirectSpecularLight.rgb + vIndirectSpecularLight.rgb) + ((vIndirectDiffuseLight.rgb + vDirectDiffuseLight.rgb) * vUnlitColor.rgb);

	/// if vDirectDiffuseLight.a == 1 => surface is drawn with lighting applied
	Output.vMergeOutput = float4(lerp(vUnlitColor.rgb, vLitColor.rgb, vDirectDiffuseLight.a).rgb, 1.0f);

	return Output;
}
//---------------------------------------------------------------------------------------------------
#endif // HLSL

#endif // DEFERREDMERGE_HLSL