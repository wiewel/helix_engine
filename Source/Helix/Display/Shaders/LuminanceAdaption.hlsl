//   Copyright 2020 Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef LUMINANCEADAPTION_HLSL
#define LUMINANCEADAPTION_HLSL

#include "ConstantBuffers.hlsli"
#include "Helper.hlsli"

//---------------------------------------------------------------------------------------------------
NAMESPACE(LuminanceAdaption)

CBUFFER(cbLuminanceAdaption)
	float fAdaptionRate;
	float3 vLuminanceAdaptionPAD;
BUFFER_END

struct VS_IN
{
#ifdef HPMIO_Debug
	float2 vPosition SN(POSITION)
	float2 vTexCoord SN(TEXCOORD)
#else
	uint uID SN(SV_VERTEXID)
#endif
};

struct PS_IN
{
	float4 vPosition SN(SV_POSITION)
	float2 vTexCoord SN(TEXCOORD)
};

struct PS_OUT
{
	float fLuminanceAdaption SN(SV_TARGET0)
};

#ifndef HLSL
enum HPMIOPermutation : uint32_t
{
	//HPMIO_Debug = 1 << 0,
};
#endif

NAMESPACE_END

//---------------------------------------------------------------------------------------------------
#ifdef HLSL

Texture2D<float> gLastLuminance;
Texture2D<float> gCurrentLuminance;

//---------------------------------------------------------------------------------------------------
PS_IN VShader(VS_IN _In)
{
	PS_IN Out = (PS_IN)0;

#ifdef HPMIO_Debug
	Out.vPosition = float4(_In.vPosition, 0.0f, 1.0f);
	Out.vTexCoord = _In.vTexCoord;
#else
	PosXYZWTexUV Tri = CalculateFullscreenTriangle(_In.uID);

	Out.vPosition = Tri.vPosition;
	Out.vTexCoord = Tri.vTexCoord;
#endif

	return Out;
}
//---------------------------------------------------------------------------------------------------
PS_OUT PShader(PS_IN _In)
{
	PS_OUT Output;

	float fLastLuminance = exp(gLastLuminance.Sample(SamplerPointClamp, _In.vTexCoord).r);
	float fCurrentLuminance = gCurrentLuminance.Sample(SamplerPointClamp, _In.vTexCoord).r;

	/// Patanaik's luminance adaption
	Output.fLuminanceAdaption = log(fLastLuminance + (fCurrentLuminance - fLastLuminance) * (1.0f - exp(-fDeltaTime * fAdaptionRate)));

	return Output;
}
//---------------------------------------------------------------------------------------------------
#endif // HLSL

#endif // LUMINANCEADAPTION_HLSL
