//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

//Include this header in every Shader you write!

#ifndef HELIXSHADERDEFINES_H
#define HELIXSHADERDEFINES_H

//force bool to be 4 byte on both cpu and gpu
#undef BOOL
#undef TRUE
#undef FALSE

//Our shader compiler defines HLSL
#ifdef HLSL

// index from 0 -> 3 (byte index) 
uint GetByte(uint uInput, uint uIndex)
{
	return (uInput & (0xff << (uIndex * 8))) >> (uIndex * 8);
}
// index from 0 -> 15 (byte index) 
uint GetByte(uint4 uInput, uint uIndex)
{
	uint u = uInput[uIndex >> 2];
	u &= 0xff << ((uIndex % 4) * 8);
	u >>= (uIndex % 4) * 8;
	return  u;
}

// index from 0 -> 1 (2byte index) 
uint GetHalf(uint uInput, uint uIndex)
{
	return (uInput & (0xffff << (uIndex * 16))) >> (uIndex * 16);
}
// index from 0 -> 8 (2byte index) 
uint GetHalf(uint4 uInput, uint uIndex)
{
	uint u = uInput[uIndex >> 1];
	u &= 0xffff << ((uIndex % 2) * 16);
	u >>= (uIndex % 2) * 16;
	return u;
}

//TODO: remove
//#pragma pack_matrix (row_major)

#define RANGE(_name, _start, _step, _count)

//SemanticName:
#define SN(_name) : _name;

#define SAMPLER(_name,_reg) SamplerState _name : register(s##_reg);
#define SAMPLERCOMP(_name,_reg) SamplerComparisonState _name : register(s##_reg);

#define CBUFFER(_name) cbuffer _name {

#define CBUFFERARRAY(_name) struct _name##TYPE {
#define CBUFFERARRAY_END(_name,_size) }; cbuffer _name { uint _name##ArraySize; float3 _name##PAD; _name##TYPE _name##Array[_size]; };

#define TBUFFER(_name) tbuffer _name{

#define BUFFER_END };

#define NAMESPACE(_name)
#define NAMESPACE_END

//Texture Types: //https ://msdn.microsoft.com/en-us/library/windows/desktop/bb509700%28v=vs.85%29.aspx

#define BOOL int
#define TRUE 1
#define FALSE 0

#elif GLSL

#else// C++ part

#define RANGE(_name, _start, _step, _count) struct _name {static constexpr uint32_t uStart = _start; static constexpr uint32_t uStep = _step; static constexpr uint32_t uCount = _count;};

//HPR_TestName(start_10, step_5, count_15)
#define GETSLOT(_slot) static inline int32_t GetSlot() {return _slot;}
#define GETDIM(_dim) static inline int32_t GetDimension() { return _dim;}
#define GETSIZE(_type) static inline size_t GetSize() {return sizeof(_type);}

#define SN(_name) ;

#define SAMPLER(_name,_reg) struct _name { GETSLOT(_reg) };
#define SAMPLERCOMP(_name,_reg) struct _name { GETSLOT(_reg) };

#define CBUFFER(_name)  struct _name {
#define TBUFFER(_name)  struct _name {
#define BUFFER_END };

#define CBUFFERARRAY(_name) struct _name##TYPE {
#define CBUFFERARRAY_END(_name, _size) }; struct _name { uint32_t m_uCurrentArraySize = 0; DirectX::XMFLOAT3 m_vPadding; _name##TYPE Array[_size]; };

#define NAMESPACE(_name) namespace Helix { namespace Shaders { namespace _name {
#define NAMESPACE_END } } }

#include "Math\MathTypes.h"
namespace Helix
{
	namespace Shaders
	{
		using namespace Math;

		//forward definitions:
		//using matrix = float4x4;
		using uint = uint32_t;
		using half = uint16_t;
		//using unorm = float;
		//using snorm = float
	} // Shaders
} // Helix


#define BOOL int32_t
#define TRUE 1
#define FALSE 0

#endif // HLSL

//Sampler defines:

SAMPLER(SamplerPointClamp, 0)
SAMPLER(SamplerPointWrap, 1)
SAMPLER(SamplerLinearClamp, 2)
SAMPLER(SamplerLinearWrap, 3)
SAMPLER(SamplerAnisotropicClamp, 4)
SAMPLER(SamplerAnisotropicWrap, 5)
SAMPLERCOMP(SamplerShadowMap, 6)


#endif // HELIXSHADERDEFINES_H

//Sample usage, where "SomeFile" should be the name of the shader
//Padding must be done explicitly by hand! packoffset should not be used! Otherwise the C++ struct will have invalid alignment!

//NAMESPACE(SomeFile)
//
//NAMESPACE(ConstantBuffers)
//
//CBUFFER(cbPerCamera, 0)
//	float4x4 mViewProj; //64
//	float4 vStuff; // 16
//	float3 vPos; // 12!
//	float pad; // 4 Manually add padding!
//BUFFER_END
//
//NAMESPACE_END

