//   Copyright 2020 Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef PARABOLOIDSHADOW_HLSL
#define PARABOLOIDSHADOW_HLSL

#include "ConstantBuffers.hlsli"

//---------------------------------------------------------------------------------------------------
NAMESPACE(ParaboloidShadow)

struct VS_IN
{
	float3 vPosition SN(POSITION)
};

struct GS_IN
{
	float4 vPosition SN(SV_POSITION)
	float fZValue SN(ZVALUE)
};

struct PS_IN
{
	float4 vPosition SN(SV_POSITION)
	uint   RTIndex SN(SV_RenderTargetArrayIndex)
};

NAMESPACE_END
//---------------------------------------------------------------------------------------------------
#ifdef HLSL
//---------------------------------------------------------------------------------------------------
GS_IN VShader(VS_IN _In)
{
	GS_IN Output = (GS_IN) 0;

	float4 vInPos = float4(_In.vPosition, 1.0f);

	/// Projection is identity
	float4x4 mWorldViewProj = mul(mView, mWorld);

	Output.vPosition = mul(mWorldViewProj, vInPos);
	Output.vPosition.xyz /= Output.vPosition.w;

	/// Distance between paraboloid origin and vertex
	float L = length(Output.vPosition.xyz);

	/// Normalize vector towards vertex
	Output.vPosition /= L;

	Output.fZValue = Output.vPosition.z;

	/// Proper depth testing
	Output.vPosition.z = (L - fNearDist) / fFarNearDist;
	Output.vPosition.w = 1.0f;

	return Output;
}
//---------------------------------------------------------------------------------------------------
[maxvertexcount(3)]
[instance(2)]
void GShader(triangle GS_IN _In[3],
			 uint _uID : SV_GSInstanceID,
			 inout TriangleStream<PS_IN> OutputStream)
{
	PS_IN Output;

	// Initialize the vertex order and the direction of the paraboloid.
	uint3 vOrder = uint3(0, 1, 2);
	float fDirection = 1.0f;

	// Check to see which copy of the primitive this is.  If it is 0, then it
	// is considered the front facing paraboloid.  If it is 1, then it is
	// considered the back facing paraboloid.  For back facing, we reverse
	// the Output vertex winding order.
	if (_uID == 1)
	{
		vOrder.xyz = vOrder.xzy;
		fDirection = -1.0f;
	}

	// Emit three vertices for one complete triangle.
	[unroll]
	for (int i = 0; i < 3; ++i)
	{
		// Create a projection factor, which determines which half space 
		// will be considered positive and also adds the viewing vector
		// which is (0,0,1) and hence can only be added to the z-component.
		float fProjFactor = _In[vOrder[i]].fZValue * fDirection + 1.0f;
		Output.vPosition.x = _In[vOrder[i]].vPosition.x / fProjFactor;
		Output.vPosition.y = _In[vOrder[i]].vPosition.y / fProjFactor;
		Output.vPosition.z = _In[vOrder[i]].vPosition.z;
		Output.vPosition.w = 1.0f;

		// Simply use the geometry shader instance as the render target
		// index for this primitive.
		Output.RTIndex = _uID;

		// Write the vertex to the Output stream.
		OutputStream.Append(Output);
	}

	OutputStream.RestartStrip();
}
//---------------------------------------------------------------------------------------------------

#endif

#endif // PARABOLOIDSHADOW_HLSL
