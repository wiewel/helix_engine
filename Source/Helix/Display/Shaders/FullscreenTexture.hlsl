//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef FULLSCREENTEXTURE_HLSL
#define FULLSCREENTEXTURE_HLSL

#include "ConstantBuffers.hlsli"
#include "Helper.hlsli"

//---------------------------------------------------------------------------------------------------
NAMESPACE(FullscreenTexture)

CBUFFER(cbFullscreenTexture)
float4 vFullscreenTextureResolution;
BUFFER_END

struct VS_IN
{
	uint uID SN(SV_VERTEXID)
};

struct PS_IN
{
	float4 vPosition SN(SV_POSITION)
	float2 vTexCoord SN(TEXCOORD)
	//float2 vTexDimension SN(TEXCOORD)
};

struct PS_OUT
{
	float4 vColor SN(SV_TARGET0)
};

#ifndef HLSL
enum HPMPSPermutation : uint32_t
{
	// standard permutation is for rgba textures
	HPMPS_R_Excl = 1 << 0,
	HPMPS_RGB_Excl = 1 << 1,
};
#endif

NAMESPACE_END

//---------------------------------------------------------------------------------------------------
#ifdef HLSL

#ifdef HPMPS_R_Excl
Texture2D<float> gTex;
#define EXCLPERM
#endif

#ifdef HPMPS_RGB_Excl
Texture2D<float3> gTex;
#define EXCLPERM
#endif

#ifndef EXCLPERM
Texture2D<float4> gTex;
#endif

//---------------------------------------------------------------------------------------------------
PS_IN VShader(VS_IN _In)
{
	PS_IN Out = (PS_IN)0;

	float2 vTexDimension = GetTextureDimension(gTex);
	float2 vSizeDifference = ((vTexDimension.xy - vFullscreenTextureResolution.xy) / vTexDimension.xy) * 0.5f;

	float2 vDelta = float2(1.0f, 1.0f) - vSizeDifference.xy;

	switch (_In.uID)
	{
	case 0:
		Out.vPosition = float4(-vDelta.x, vDelta.y, 0.0f, 1.0f);
		Out.vTexCoord = float2(0.0f, 0.0f);
		break;
	case 1:
		Out.vPosition = float4(-vDelta.x, -vDelta.y, 0.0f, 1.0f);
		Out.vTexCoord = float2(0.0f, 1.0f);
		break;
	case 2:
		Out.vPosition = float4(vDelta.x, vDelta.y, 0.0f, 1.0f);
		Out.vTexCoord = float2(1.0f, 0.0f);
		break;
	case 3:
		Out.vPosition = float4(vDelta.x, -vDelta.y, 0.0f, 1.0f);
		Out.vTexCoord = float2(1.0f, 1.0f);
		break;

	default:
		Out.vPosition = float4(0.0f, 0.0f, 0.0f, 0.0f);
		Out.vTexCoord = float2(0.0f, 0.0f);
		break;
	}

	return Out;
}
//---------------------------------------------------------------------------------------------------
PS_OUT PShader(PS_IN _In)
{
	PS_OUT Output;
	Output.vColor = float4(0,0,0,1);

#ifdef HPMPS_R_Excl
	// r textures
	Output.vColor = float4(gTex.Sample(SamplerAnisotropicClamp, _In.vTexCoord).rrr, 1.0f);
#else
	#ifdef HPMPS_RGB_Excl
		// rgb textures
	Output.vColor = float4(gTex.Sample(SamplerAnisotropicClamp, _In.vTexCoord).rgb, 1.0f);
	#else
		// rgba textures
	Output.vColor = gTex.Sample(SamplerAnisotropicClamp, _In.vTexCoord);
	#endif
#endif

	return Output;
}
//---------------------------------------------------------------------------------------------------
#endif

#endif //FULLSCREENTEXTURE_HLSL