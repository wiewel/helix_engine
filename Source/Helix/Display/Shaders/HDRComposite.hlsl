//   Copyright 2020 Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef HDRCOMPOSITE_HLSL
#define HDRCOMPOSITE_HLSL

#include "ConstantBuffers.hlsli"
#include "PostEffects.hlsli"
#include "Helper.hlsli"


//---------------------------------------------------------------------------------------------------
NAMESPACE(HDRComposite)

CBUFFER(cbHDRComposite)
	float	fBloomIntensity;
	float	fLuminanceMipLevel;
	float	fExposureKeyValue;
	float	vHDRCompositePad;
BUFFER_END

struct VS_IN
{
#ifdef HPMIO_Debug
	float2 vPosition SN(POSITION)
	float2 vTexCoord SN(TEXCOORD)
#else
	uint uID SN(SV_VERTEXID)
#endif
};

struct PS_IN
{
	float4 vPosition SN(SV_POSITION)
	float2 vTexCoord SN(TEXCOORD)
};

struct PS_OUT
{
	float4 vHDRComposite SN(SV_TARGET0)
};

#ifndef HLSL
//enum HPMIOPermutation : uint32_t
//{
//	//HPMIO_Debug = 1 << 0,
//};
enum HPMPSPermutation : uint32_t
{
	HPMPS_AutoExposure = 1 << 0,
};
#endif

NAMESPACE_END

//---------------------------------------------------------------------------------------------------
#ifdef HLSL

Texture2D gColorMap; // original image
Texture2D gLuminanceMap; // adapted luminance map
Texture2D gBloomMap; // bloom image

//---------------------------------------------------------------------------------------------------
PS_IN VShader(VS_IN _In)
{
	PS_IN Out = (PS_IN)0;

#ifdef HPMIO_Debug
	Out.vPosition = float4(_In.vPosition, 0.0f, 1.0f);
	Out.vTexCoord = _In.vTexCoord;
#else
	PosXYZWTexUV Tri = CalculateFullscreenTriangle(_In.uID);

	Out.vPosition = Tri.vPosition;
	Out.vTexCoord = Tri.vTexCoord;
#endif

	return Out;
}
//---------------------------------------------------------------------------------------------------
PS_OUT PShader(PS_IN _In)
{
	PS_OUT Output = (PS_OUT)0;

	float3 vColor = gColorMap.Sample(SamplerLinearClamp, _In.vTexCoord).rgb;

#ifdef HPMPS_AutoExposure
	/// Input image Tone Mapping
	float fAverageLuminance = GetAverageLuminance(gLuminanceMap, _In.vTexCoord, fLuminanceMipLevel);
	/// Luminance Eye Adaption
	vColor = CalculateExposedColor(vColor, fAverageLuminance, 0.0f, fExposureKeyValue);
#endif

	/// Tone Map
	vColor = ToneMap(vColor);

	/// Bloom
	float3 vBloom = gBloomMap.Sample(SamplerLinearClamp, _In.vTexCoord).rgb;
	vBloom = vBloom * fBloomIntensity;
	vColor = vColor + vBloom;

	Output.vHDRComposite = float4(vColor, 1.0f);

	return Output;
}
//---------------------------------------------------------------------------------------------------
#endif // HLSL

#endif // HDRCOMPOSITE_HLSL
