//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef POSTEFFECTS_HLSLI
#define POSTEFFECTS_HLSLI

//---------------------------------------------------------------------------------------------------
#ifdef HLSL

//---------------------------------------------------------------------------------------------------
float CalculateLuminance(float3 _vColor)
{
	return max(dot(_vColor, float3(0.299f, 0.587f, 0.114f)), 0.0001f);
}
//---------------------------------------------------------------------------------------------------
float GetAverageLuminance(Texture2D _LuminanceTex, float2 _vTexCoord, float _fMipLevel)
{
	return exp(_LuminanceTex.SampleLevel(SamplerLinearClamp, _vTexCoord, _fMipLevel).r);
}
//---------------------------------------------------------------------------------------------------
/// Different exposure handling strategies
float EvaluateExposure(float _fAverageLuminance, float _fKeyValue)
{
	float fExposure = 0.0f;

	const int iType = 0;
	float fKeyValue = _fKeyValue;

	float fAvgLum = max(_fAverageLuminance, 0.001f);

	if(iType == 1)
	{
		// TODO: check how one can avoid the overbright "effect"
		fKeyValue = 1.03f - (2.0f / (2.0f + log10(fAvgLum + 1.0f)));
	}

	float fLinearExposure = fKeyValue / fAvgLum;
	fExposure = log2(max(fLinearExposure, 0.0001f));

	return fExposure;
}
//---------------------------------------------------------------------------------------------------
float3 CalculateExposedColor(float3 _vColor, float _fAverageLuminance, float _fThreshold, float _fExposureKeyValue)
{
	float fExposure = EvaluateExposure(_fAverageLuminance, _fExposureKeyValue);
	fExposure -= _fThreshold;
	return exp2(fExposure) * _vColor;
}

//---------------------------------------------------------------------------------------------------
/// Different Tonemap implementations -> Test

//// Logarithmic mapping
//float3 ToneMapLogarithmic(float3 _vColor)
//{
//	float fLuminance = CalcLuminance(_vColor);
//	float fToneMappedLuminance = log10(1 + fLuminance) / log10(1 + WhiteLevel);
//	return fToneMappedLuminance * pow(_vColor / fLuminance, LuminanceSaturation);
//}
//// Drago's Logarithmic mapping
//float3 ToneMapDragoLogarithmic(float3 _vColor)
//{
//	float fLuminance = CalcLuminance(_vColor);
//	float fToneMappedLuminance = log10(1 + fLuminance);
//	fToneMappedLuminance /= log10(1 + WhiteLevel);
//	fToneMappedLuminance /= log10(2 + 8 * ((fLuminance / WhiteLevel) * log10(Bias) / log10(0.5f)));
//	return fToneMappedLuminance * pow(_vColor / fLuminance, LuminanceSaturation);
//}
//// Exponential mapping
//float3 ToneMapExponential(float3 _vColor)
//{
//	float fLuminance = CalcLuminance(_vColor);
//	float fToneMappedLuminance = 1 - exp(-fLuminance / WhiteLevel);
//	return fToneMappedLuminance * pow(_vColor / fLuminance, LuminanceSaturation);
//}
//// Applies Reinhard's basic tone mapping operator
//float3 ToneMapReinhard(float3 _vColor)
//{
//	float fLuminance = CalcLuminance(_vColor);
//	float fToneMappedLuminance = fLuminance / (fLuminance + 1);
//	return fToneMappedLuminance * pow(_vColor / fLuminance, LuminanceSaturation);
//}
//// Applies Reinhard's modified tone mapping operator
//float3 ToneMapReinhardModified(float3 _vColor)
//{
//	float fLuminance = CalcLuminance(_vColor);
//	float fToneMappedLuminance = fLuminance * (1.0f + fLuminance / (WhiteLevel * WhiteLevel)) / (1.0f + fLuminance);
//	return fToneMappedLuminance * pow(_vColor / fLuminance, LuminanceSaturation);
//}
// Applies the filmic curve from John Hable's presentation
float3 ToneMapFilmicALU(float3 _vColor)
{
	_vColor = max(0, _vColor - 0.004f);
	_vColor = (_vColor * (6.2f * _vColor + 0.5f)) / (_vColor * (6.2f * _vColor + 1.7f) + 0.06f);

	// result has 1/2.2 baked in
	return pow(_vColor, 2.2f);
}
//// Function used by the Uncharte2D tone mapping curve
//float3 U2Func(float3 x)
//{
//	float A = ShoulderStrength;
//	float B = LinearStrength;
//	float C = LinearAngle;
//	float D = ToeStrength;
//	float E = ToeNumerator;
//	float F = ToeDenominator;
//	return ((x*(A*x + C*B) + D*E) / (x*(A*x + B) + D*F)) - E / F;
//}
//// Applies the Uncharted 2 filmic tone mapping curve
//float3 ToneMapFilmicU2(float3 _vColor)
//{
//	float3 numerator = U2Func(_vColor);
//	float3 denominator = U2Func(LinearWhite);
//
//	return numerator / denominator;
//}

/// Custom, similar to John Hable's curve
float3 CustomToneMap(float3 _vColor, float4 _vSettings)
{
	return (_vColor * (_vSettings.x * _vColor + _vSettings.y)) * rcp(_vColor * (_vSettings.x * _vColor + _vSettings.z) + _vSettings.w);
}

//---------------------------------------------------------------------------------------------------
float3 ToneMap(float3 _vColor)
{
	float3 vColor = _vColor;

	const int iType = 2;

	if(iType == 0)
	{
		float A = 10.0f;
		float B = 0.3f;
		float C = 0.5f;
		float D = 1.5f;
		float4 vSettings = float4(A, B, C, D);
		vColor = CustomToneMap(vColor, vSettings);
	}
	else if(iType == 1)
	{
		float A = 1.8f;
		float B = 1.4f;
		float C = 0.5f;
		float D = 1.5f;
		float4 vSettings = float4(A, B, C, D);
		vColor = CustomToneMap(vColor, vSettings);
	}
	else
	{
		vColor = ToneMapFilmicALU(vColor);
	}

	return vColor;
}

//---------------------------------------------------------------------------------------------------
// Lensflare

float Noise(Texture2D _RandomTexture, float t)
{
	uint uWidth;
	uint uHeight;
	_RandomTexture.GetDimensions(uWidth, uHeight);
	float2 vTexDim = float2(uWidth, uHeight);

	return _RandomTexture.Sample(SamplerLinearWrap, float2(t, 0.0f) / vTexDim).x;
}
float Noise(Texture2D _RandomTexture, float2 t)
{
	uint uWidth;
	uint uHeight;
	_RandomTexture.GetDimensions(uWidth, uHeight);
	float2 vTexDim = float2(uWidth, uHeight);

	return _RandomTexture.Sample(SamplerLinearWrap, t / vTexDim).x;
}

float3 LensflareInternal(float2 uv, float2 pos, Texture2D _RandomTexture)
{
	float2 main = uv - pos;
	float2 uvd = uv * (length(uv));

	// HLSL atan2(x,y) == GLSL atan(y,x)
	float ang = atan2(main.y, main.x);
	float dist = length(main);
	dist = pow(dist, 0.1);
	float n = Noise(_RandomTexture, float2(ang*16.0, dist*32.0));

	float f0 = 1.0 / (length(uv - pos)*16.0 + 1.0);

	f0 = f0 + f0*(sin(Noise(_RandomTexture, (pos.x + pos.y)*2.2 + ang*4.0 + 5.954)*16.0)*.1 + dist*.1 + .8);

	float f1 = max(0.01 - pow(length(uv + 1.2*pos), 1.9), .0)*7.0;

	float f2 = max(1.0 / (1.0 + 32.0*pow(length(uvd + 0.8*pos), 2.0)), .0)*00.25;
	float f22 = max(1.0 / (1.0 + 32.0*pow(length(uvd + 0.85*pos), 2.0)), .0)*00.23;
	float f23 = max(1.0 / (1.0 + 32.0*pow(length(uvd + 0.9*pos), 2.0)), .0)*00.21;

	float2 uvx = lerp(uv, uvd, -0.5);

	float f4 = max(0.01 - pow(length(uvx + 0.4*pos), 2.4), .0)*6.0;
	float f42 = max(0.01 - pow(length(uvx + 0.45*pos), 2.4), .0)*5.0;
	float f43 = max(0.01 - pow(length(uvx + 0.5*pos), 2.4), .0)*3.0;

	uvx = lerp(uv, uvd, -.4);

	float f5 = max(0.01 - pow(length(uvx + 0.2*pos), 5.5), .0)*2.0;
	float f52 = max(0.01 - pow(length(uvx + 0.4*pos), 5.5), .0)*2.0;
	float f53 = max(0.01 - pow(length(uvx + 0.6*pos), 5.5), .0)*2.0;

	uvx = lerp(uv, uvd, -0.5);

	float f6 = max(0.01 - pow(length(uvx - 0.3*pos), 1.6), .0)*6.0;
	float f62 = max(0.01 - pow(length(uvx - 0.325*pos), 1.6), .0)*3.0;
	float f63 = max(0.01 - pow(length(uvx - 0.35*pos), 1.6), .0)*5.0;

	float3 c = float3(0.0, 0.0, 0.0);

	c.r += f2 + f4 + f5 + f6;
	c.g += f22 + f42 + f52 + f62;
	c.b += f23 + f43 + f53 + f63;

	float fLength = length(uvd)*0.05;
	c = c*1.3 - float3(fLength, fLength, fLength);
	c += float3(f0, f0, f0);

	return c;
}

float3 Lensflare(float2 _vUV, float2 _vScreenSpaceLightPos, float3 _vColor, Texture2D _RandomTexture)
{
	_vUV.x *= fAspectRatio;
	_vScreenSpaceLightPos.x *= fAspectRatio;
	float3 vColor = _vColor * LensflareInternal(_vUV, _vScreenSpaceLightPos, _RandomTexture);
	return vColor;
}

#endif // HLSL

#endif // POSTEFFECTS_HLSLI
