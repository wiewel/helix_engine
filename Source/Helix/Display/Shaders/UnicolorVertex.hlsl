//   Copyright 2020 Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef UNICOLORVERTEX_HLSL
#define UNICOLORVERTEX_HLSL

#include "ConstantBuffers.hlsli"

//---------------------------------------------------------------------------------------------------
NAMESPACE(UnicolorVertex)

struct VS_IN
{
	float3 vPosition	SN(POSITION)
	float3 vColor		SN(COLOR)
};

struct PS_IN
{
	float4 vPosition	SN(SV_POSITION)
	float3 vColor		SN(COLOR)
};

struct PS_OUT
{
	float4 vColor	SN(SV_TARGET0)
};

NAMESPACE_END
//---------------------------------------------------------------------------------------------------
#ifdef HLSL

//---------------------------------------------------------------------------------------------------
PS_IN VShader(VS_IN _In)
{
	PS_IN Output = (PS_IN)0;

	float4 vInPos = float4(_In.vPosition, 1.0f);
	Output.vPosition = mul(mViewProj, vInPos);

	Output.vColor = _In.vColor;

	return Output;
}
//---------------------------------------------------------------------------------------------------
PS_OUT PShader(PS_IN _In)
{
	PS_OUT Output = (PS_OUT)0;
	Output.vColor = float4(_In.vColor, 1.0f);
	return Output;
}
//---------------------------------------------------------------------------------------------------

#endif //HLSL

#endif //UNICOLORVERTEX_HLSL
