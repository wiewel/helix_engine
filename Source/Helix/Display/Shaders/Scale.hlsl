//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef SCALE_HLSL
#define SCALE_HLSL

#include "ConstantBuffers.hlsli"
#include "Helper.hlsli"

//---------------------------------------------------------------------------------------------------
NAMESPACE(Scale)

struct VS_IN
{
#ifdef HPMIO_Debug
	float2 vPosition SN(POSITION)
	float2 vTexCoord SN(TEXCOORD)
#else
	uint uID SN(SV_VERTEXID)
#endif
};

struct PS_IN
{
	float4 vPosition SN(SV_POSITION)
	float2 vTexCoord SN(TEXCOORD)
};

struct PS_OUT
{
	float4 vScaled SN(SV_TARGET0)
}; 

NAMESPACE_END

//---------------------------------------------------------------------------------------------------
#ifdef HLSL

Texture2D<float4> gInTex;


//---------------------------------------------------------------------------------------------------
PS_IN VShader(VS_IN _In)
{
	PS_IN Out = (PS_IN)0;

#ifdef HPMIO_Debug
	Out.vPosition = float4(_In.vPosition, 0.0f, 1.0f);
	Out.vTexCoord = _In.vTexCoord;
#else
	PosXYZWTexUV Tri = CalculateFullscreenTriangle(_In.uID);

	Out.vPosition = Tri.vPosition;
	Out.vTexCoord = Tri.vTexCoord;
#endif

	return Out;
}
//---------------------------------------------------------------------------------------------------
PS_OUT PShader(PS_IN _In)
{
	PS_OUT Output = (PS_OUT)0;
	Output.vScaled = gInTex.Sample(SamplerLinearClamp, _In.vTexCoord);
	return Output;
}
//---------------------------------------------------------------------------------------------------
#endif // HLSL

#endif // SCALE_HLSL

