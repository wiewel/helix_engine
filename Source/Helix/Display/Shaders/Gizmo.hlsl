//   Copyright 2020 Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef GIZMO_HLSL
#define GIZMO_HLSL

#include "ConstantBuffers.hlsli"
#include "Texture.hlsli"
#include "Light.hlsli"

//---------------------------------------------------------------------------------------------------
NAMESPACE(Gizmo)

CBUFFER(cbGizmoObject)
	float4x4 mGizmoWorld;
	float4 vGizmoColor;
	float4 vGizmoPosition; /// in WS
BUFFER_END

struct VS_IN
{
	float3 vPosition SN(POSITION)
};

struct PS_IN
{
	float4 vPosition SN(SV_POSITION)
};

struct PS_OUT
{
	float4 vColor SN(SV_TARGET0)
};

NAMESPACE_END
//---------------------------------------------------------------------------------------------------
#ifdef HLSL

//---------------------------------------------------------------------------------------------------
PS_IN VShader(VS_IN _In)
{
	PS_IN Output = (PS_IN) 0;

	float4 vInPos = float4(_In.vPosition, 1.0f);
	/// only apply scaling and rotation, position is set to zero
	float4 vPosWS = mul(mGizmoWorld, vInPos);
	/// translate to world space location
	vPosWS.xyz += vGizmoPosition.xyz;

	/// transform to clip space
	Output.vPosition = mul(mViewProj, vPosWS);

	return Output;
}
//---------------------------------------------------------------------------------------------------
PS_OUT PShader(PS_IN _In)
{
	PS_OUT Output = (PS_OUT)0;
	Output.vColor = vGizmoColor;
	return Output;
}
//---------------------------------------------------------------------------------------------------

#endif //HLSL

#endif //GIZMO_HLSL
