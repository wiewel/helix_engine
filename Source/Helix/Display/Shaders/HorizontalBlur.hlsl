//   Copyright 2020 Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef HORIZONTALBLUR_HLSL
#define HORIZONTALBLUR_HLSL

#include "ConstantBuffers.hlsli"
#include "Helper.hlsli"

//---------------------------------------------------------------------------------------------------
NAMESPACE(HorizontalBlur)

// supports maximum blur width of 6 
CBUFFER(cbHorizontalBlur)
	float4 GaussianWeights[4]; // 0 - 15 => center = 7

	int iBlurWidth; // blur width in one direction of center -> blur 7 pixels -> blur width = 3
	float fInputTexDim;
	float fBlurIntensity; // strided blur (skips pixels in between) default is 1

	float HorizontalBlurPAD;
BUFFER_END

struct VS_IN
{
#ifdef HPMIO_Debug
	float2 vPosition SN(POSITION)
	float2 vTexCoord SN(TEXCOORD)
#else
	uint uID SN(SV_VERTEXID)
#endif
};

struct PS_IN
{
	float4 vPosition SN(SV_POSITION)
	float2 vTexCoord SN(TEXCOORD)
};

struct PS_OUT
{
	float4 vBlurColor SN(SV_TARGET0)
};

#ifndef HLSL
enum HPMIOPermutation : uint32_t
{
	HPMIO_Debug = 1 << 0,
};
#endif

NAMESPACE_END

//---------------------------------------------------------------------------------------------------
#ifdef HLSL

Texture2D<float4> gTex;

//---------------------------------------------------------------------------------------------------
float GetGaussianWeight(uint _uIndex)
{
	return ((float[4])(GaussianWeights[_uIndex / 4]))[_uIndex % 4];
}

//---------------------------------------------------------------------------------------------------
PS_IN VShader(VS_IN _In)
{
	PS_IN Out = (PS_IN)0;

#ifdef HPMIO_Debug
	Out.vPosition = float4(_In.vPosition, 0.0f, 1.0f);
	Out.vTexCoord = _In.vTexCoord;
#else
	PosXYZWTexUV Tri = CalculateFullscreenTriangle(_In.uID);

	Out.vPosition = Tri.vPosition;
	Out.vTexCoord = Tri.vTexCoord;
#endif

	return Out;
}
//---------------------------------------------------------------------------------------------------
PS_OUT PShader(PS_IN _In)
{
	const float2 vTexScale = float2(1.0f, 0.0f);
	const int iBlurCenter = 7;

	PS_OUT Output = (PS_OUT)0;
	float4 vColor = 0;

	for (int i = -iBlurWidth; i < iBlurWidth; ++i)
	{
		float fWeight = GetGaussianWeight((uint)(iBlurCenter + i));
		float2 vTexCoord = _In.vTexCoord;
		vTexCoord += (i / fInputTexDim) * fBlurIntensity * vTexScale;
		float4 vSample = gTex.Sample(SamplerPointClamp, vTexCoord);
		vColor += vSample * fWeight;
	}

	Output.vBlurColor = vColor;
	return Output;
}
//---------------------------------------------------------------------------------------------------
#endif

#endif //HORIZONTALBLUR_HLSL