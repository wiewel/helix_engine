//   Copyright 2020 Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef GLITCH_HLSL
#define GLITCH_HLSL

#include "ConstantBuffers.hlsli"
#include "Helper.hlsli"

//---------------------------------------------------------------------------------------------------
NAMESPACE(Glitch)

struct VS_IN
{
#ifdef HPMIO_Debug
	float2 vPosition SN(POSITION)
	float2 vTexCoord SN(TEXCOORD)
#else
	uint uID SN(SV_VERTEXID)
#endif
};

struct PS_IN
{
	float4 vPosition SN(SV_POSITION)
	float2 vTexCoord SN(TEXCOORD)
};

struct PS_OUT
{
	float4 vColor SN(SV_TARGET0)
};

#ifndef HLSL
enum HPMIOPermutation : uint32_t
{
	HPMIO_Debug = 1 << 0,
};
#endif

NAMESPACE_END

//---------------------------------------------------------------------------------------------------
#ifdef HLSL

Texture2D<float4> gInTex;

//---------------------------------------------------------------------------------------------------
PS_IN VShader(VS_IN _In)
{
	PS_IN Out = (PS_IN)0;

#ifdef HPMIO_Debug
	Out.vPosition = float4(_In.vPosition, 0.0f, 1.0f);
	Out.vTexCoord = _In.vTexCoord;
#else
	PosXYZWTexUV Tri = CalculateFullscreenTriangle(_In.uID);

	Out.vPosition = Tri.vPosition;
	Out.vTexCoord = Tri.vTexCoord;
#endif

	return Out;
}
//---------------------------------------------------------------------------------------------------

float Random(float2 _vIn)
{
	return frac(sin(dot(_vIn.xy, float2(12.9898, 78.233))) * 43758.5453) * 2.0 - 1.0;
}

float Offset(float _fBlocks, float2 _vUV)
{
	return Random(float2(fTime, floor(_vUV.y * _fBlocks)));
}


PS_OUT PShader(PS_IN _In)
{
	PS_OUT Output;

	float2 vUV = _In.vTexCoord;

	Output.vColor.r = gInTex.Sample(SamplerAnisotropicWrap, vUV + float2(Offset(16.0, vUV) * 0.03, 0.0)).r;
	Output.vColor.g = gInTex.Sample(SamplerAnisotropicWrap, vUV + float2(Offset(8.0, vUV) * 0.03 * 0.16666666, 0.0)).g;
	Output.vColor.b = gInTex.Sample(SamplerAnisotropicWrap, vUV + float2(Offset(8.0, vUV) * 0.03, 0.0)).b;
	Output.vColor.a = 1.0f;

	return Output;
}
//---------------------------------------------------------------------------------------------------
#endif

#endif // GLITCH_HLSL
