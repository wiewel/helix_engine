//   Copyright 2020 Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef DEFERREDLIGHTING_HLSL
#define DEFERREDLIGHTING_HLSL

#include "ConstantBuffers.hlsli"
#include "Light.hlsli"
#include "Texture.hlsli"

//---------------------------------------------------------------------------------------------------
NAMESPACE(DeferredLighting)

CBUFFER(cbDeferredLighting)
	float4x4 mShadowTransform;
	float fSpotShadowDepthOffset;
	float fPointShadowDepthOffset;
	float2 DeferredLightingPAD;
BUFFER_END

struct VS_IN
{
#ifdef HPMIO_Debug
	float2 vPosition SN(POSITION)
	float2 vTexCoord SN(TEXCOORD)
#else
	uint uID SN(SV_VERTEXID)
#endif
};

struct PS_IN
{
	float4 vPosition SN(SV_POSITION)
	float2 vTexCoord SN(TEXCOORD0)
	float3 vViewRay SN(TEXCOORD1)
};

struct PS_OUT
{
	float4 vDiffuse SN(SV_TARGET0)
	float4 vSpecular SN(SV_TARGET1)
	//float4 vDEBUGNORMAL SN(SV_TARGET2)
};

RANGE(DirLightRange, 1, 2, 1)
RANGE(PointLightRange, 1, 16, 4)
RANGE(SpotLightRange, 1, 16, 4)

#ifndef HLSL
enum HPMIOPermutation : uint32_t
{
	HPMIO_Debug = 1 << 0,
};
enum HPMPSPermutation : uint32_t
{
	HPMPS_EnvironmentMapping = 1 << 0,
	HPMPS_ShadowMap = 1 << 1,
};
//HPMPS_BentNormal = 1 << 2,
#endif

NAMESPACE_END

//---------------------------------------------------------------------------------------------------
#ifdef HLSL
Texture2D<float> gDepthTex; // linear depth buffer
Texture2D<float> gShadowMap;
Texture2DArray<float> gParaboloidPointShadowMap; // shadow maps for point lights -> linear sample
Texture2D<float> gProjectedSpotShadowMap; // shadow maps for point lights -> linear sample

Texture2D<float2> gNormalWSMap; // normal ws
Texture2D<float2> gMetallicMap; // metallic material map / horizon normal light leakage value
Texture2D<float> gRoughnessMap; // roughness material map
Texture2D<float4> gAlbedoMap; // base color <-> specular material colors for metals
TextureCube<float4> gEnvMap; // prefiltered environment map, representing irradiance

//Texture2D<float4> gBentNormalMap; // bent normals from SSAO

//---------------------------------------------------------------------------------------------------
#ifdef _PShader_
LightingResult ComputeLighting(float3 _vViewDir, float3 _vPosWS, float3 _vNormal, float _fMetallic, float _fRoughness, float3 _vF0) // float3 _vBentNormal,
{
#ifdef HPMPS_ShadowMap
	// Calculate Shadow Factor for first DirLight
	float4 vShadowPosH = mul(float4(_vPosWS, 1.0f), mShadowTransform);
	float fShadowFactor = CalculateShadowFactor(gShadowMap, vShadowPosH);
#endif // HPMPS_ShadowMap

	LightingResult TotalResult;
	TotalResult.vDiffuse = float3(0, 0, 0);
	TotalResult.vSpecular = float3(0, 0, 0);

	[unroll]
	for (uint d = 0; d < DirLightRange; ++d)
	{
		LightingResult CurResult = CalculateDirectionalLight(cbDirectionalLightArray[d], _vViewDir, _vNormal, _fRoughness, _vF0); // _vBentNormal
#ifdef HPMPS_ShadowMap
		TotalResult.vDiffuse += (d == 0 ? fShadowFactor : 1.0f) * CurResult.vDiffuse;
		TotalResult.vSpecular += (d == 0 ? fShadowFactor : 1.0f) * CurResult.vSpecular;
#else
		TotalResult.vDiffuse += CurResult.vDiffuse;
		TotalResult.vSpecular += CurResult.vSpecular;
#endif // HPMPS_ShadowMap
	}


	[unroll]
	for (uint p = 0; p < PointLightRange; ++p)
	{
		cbPointLightTYPE PointLight = cbPointLightArray[p];

		float fShadowFactorDP = 1.0f;

		if(PointLight.iShadowIndex > -1)
		{
			cbPointLightShadowTYPE ShadowProps = cbPointLightShadowArray[PointLight.iShadowIndex];

			float4 vPosDP = TransformToParaboloid(_vPosWS, ShadowProps);
			float fSceneDepthDP = vPosDP.w;
			float3 vTexCoordDP = ConvertParaboloidToTexCoord(vPosDP);

			fShadowFactorDP = CalculateParaboloidShadowFactor(gParaboloidPointShadowMap, vTexCoordDP, fSceneDepthDP, ShadowProps);
		}

		LightingResult CurResult = CalculatePointLight(PointLight, _vViewDir, _vPosWS, _vNormal, _fRoughness, _vF0); // _vBentNormal
		TotalResult.vDiffuse += fShadowFactorDP * CurResult.vDiffuse;
		TotalResult.vSpecular += fShadowFactorDP * CurResult.vSpecular;
	}

	[unroll]
	for (uint s = 0; s < SpotLightRange; ++s)
	{
		cbSpotLightTYPE SpotLight = cbSpotLightArray[s];

		float fShadowFactorPS = 1.0f;
		
		if (SpotLight.iShadowIndex > -1)
		{
			cbSpotLightShadowTYPE ShadowProps = cbSpotLightShadowArray[SpotLight.iShadowIndex];

			float4 vPosPS = TransformToProjected(_vPosWS, ShadowProps);
			fShadowFactorPS = CalculateProjectedShadowFactor(gProjectedSpotShadowMap, vPosPS, ShadowProps);
		}

		LightingResult CurResult = CalculateSpotLight(SpotLight, _vViewDir, _vPosWS, _vNormal, _fRoughness, _vF0); // _vBentNormal
		TotalResult.vDiffuse += fShadowFactorPS * CurResult.vDiffuse;
		TotalResult.vSpecular += fShadowFactorPS * CurResult.vSpecular;
	}

	TotalResult.vDiffuse = (1.0f - _fMetallic) * TotalResult.vDiffuse;
	TotalResult.vSpecular = TotalResult.vSpecular;

	return TotalResult;
}
#endif //_PShader_
//---------------------------------------------------------------------------------------------------
PS_IN VShader(VS_IN _In)
{
	PS_IN Out = (PS_IN)0;

#ifdef HPMIO_Debug
	Out.vPosition = float4(_In.vPosition, 0.0f, 1.0f);
	Out.vTexCoord = _In.vTexCoord;
#else
	PosXYZWTexUV Tri = CalculateFullscreenTriangle(_In.uID);

	Out.vPosition = Tri.vPosition;
	Out.vTexCoord = Tri.vTexCoord;
#endif

	Out.vViewRay = GetViewRay(Out.vPosition.xy);

	/// only execute lighting when depthbuffer value is smaller than own pixel depth (far plane)
	Out.vPosition.z = Out.vPosition.w;

	return Out;
}
//---------------------------------------------------------------------------------------------------
PS_OUT PShader(PS_IN _In)
{
	PS_OUT Output = (PS_OUT)0;

	float fLinDepth = gDepthTex.Sample(SamplerPointClamp, _In.vTexCoord).r;// +fNearDist;// *fFarNearDist;

	float3 vNormal = NormalDecode(gNormalWSMap.Sample(SamplerPointWrap, _In.vTexCoord).xy);
	vNormal = normalize(vNormal);
	//Output.vDEBUGNORMAL = float4(vNormal.xyz, 1.0f);

	float3 vViewSpacePos = _In.vViewRay * fLinDepth * fFarDist;
	float3 vPosWS = mul(mViewInv, float4(vViewSpacePos, 1.0f)).xyz;

	float3 vToEye = normalize(vCameraPos.xyz - vPosWS);

//#ifdef HPMPS_BentNormal
//	float3 vBentNormal = gBentNormalMap.Sample(SamplerPointClamp, _In.vTexCoord).xyz;
//#else
//	float3 vBentNormal = float3(0, 0, 0);
//#endif // HPMPS_BentNormal

	float fRoughness = max(gRoughnessMap.Sample(SamplerLinearWrap, _In.vTexCoord).r, 0.0001f);
	float2 vMetallicHorizon = gMetallicMap.Sample(SamplerLinearWrap, _In.vTexCoord).rg;

	// Lighting
	float3 vF0 = gAlbedoMap.Sample(SamplerLinearWrap, _In.vTexCoord).rgb; // rgb->spec color map -> F0 values for metallic

	vF0 = lerp(float3(0.04, 0.04, 0.04), vF0.rgb, vMetallicHorizon.x); /// dielectric have F0 reflectance of ~0.04

	/// Calculate Direct Diffuse and Direct Specular Lighting by point light sources
	LightingResult LightResult = ComputeLighting(vToEye, vPosWS, vNormal, vMetallicHorizon.x, fRoughness, vF0); // vBentNormal,

	/// Calculate Indirect Specular Light by "Texture" lights like the skybox
	float3 vIndirectSpecularLight = float3(0, 0, 0);

	vMetallicHorizon.y *= vMetallicHorizon.y;
	LightResult.vSpecular *= vMetallicHorizon.y;

	Output.vDiffuse = float4(LightResult.vDiffuse, 1.0f);
	Output.vSpecular = float4(LightResult.vSpecular + vIndirectSpecularLight, 1.0f);

	return Output;
}
//---------------------------------------------------------------------------------------------------
#endif // HLSL

#endif // DEFERREDLIGHTING_HLSL