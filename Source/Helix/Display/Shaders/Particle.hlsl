//   Copyright 2020 Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef PARTICLE_HLSL
#define PARTICLE_HLSL

#include "ConstantBuffers.hlsli"
#include "Helper.hlsli"

//---------------------------------------------------------------------------------------------------
NAMESPACE(Particle)

CBUFFER(cbParticle)
float2 vSize;
float2 vParticlePAD;
float4 vColorScale;
BUFFER_END

struct VS_IN
{
	float3 vPosition SN(POSITION)	
	float fAge SN(AGE) // [0,1] -> 0: full opaque, 1: transparent
};

struct GS_IN
{
	float3 vPosition SN(POSITION)
};

struct PS_IN
{
	float4 vPosition SN(SV_POSITION)
	float2 vTexCoord SN(TEXCOORD0)
	float fViewDepth SN(TEXCOORD4)
};

struct PS_OUT
{
	float4 vColor SN(SV_TARGET0)
}; 


NAMESPACE_END
//---------------------------------------------------------------------------------------------------
#ifdef HLSL

Texture2D<float4> gAlbedoMap; //HTEXSLOT_DIFFUSE
//TEXTURE2D(gNormalMap, float4, HTEXSLOT_NORMAL)
Texture2D<float> gDepthTex; // HTEXSLOT_DEPTH

//---------------------------------------------------------------------------------------------------
GS_IN VShader(VS_IN _In)
{
	GS_IN Output = (GS_IN)0;

	Output.vPosition = _In.vPosition;

	// fade color with time
	//float fNewOpacity = 1.0f - smoothstep(0.0f, 1.0f, _In.fAge);
	//Output.vColor = float4(_In.vColor.rgb, fNewOpacity);

	return Output;
}
//---------------------------------------------------------------------------------------------------
[maxvertexcount(4)]
void GShader(point GS_IN _In[1], inout TriangleStream<PS_IN> _TriStream)
{
	PS_IN Output = (PS_IN)0;

	// vector from particle to camera
	float3 vLookDir = normalize(vCameraPos.xyz - _In[0].vPosition);
	float3 vRight = normalize(cross(float3(0.0f, 1.0f, 0.0f), vLookDir));
	float3 vUp = cross(vLookDir, vRight);

	float fHalfWidth = 0.5f * vSize.x;
	float fHalfHeight = 0.5f * vSize.y;

	float4 vVertices[4];
	vVertices[0] = float4(_In[0].vPosition + fHalfWidth * vRight - fHalfHeight * vUp, 1.0f);
	vVertices[1] = float4(_In[0].vPosition + fHalfWidth * vRight + fHalfHeight * vUp, 1.0f);
	vVertices[2] = float4(_In[0].vPosition - fHalfWidth * vRight - fHalfHeight * vUp, 1.0f);
	vVertices[3] = float4(_In[0].vPosition - fHalfWidth * vRight + fHalfHeight * vUp, 1.0f);

	// transform to world space, output as triangle strip

	static const float2 TexCoords[4] =
	{
		float2(0.0f, 1.0f),
		float2(1.0f, 1.0f),
		float2(0.0f, 0.0f),
		float2(1.0f, 0.0f)
	};

	[unroll]
	for (int i = 0; i < 4; ++i)
	{
		Output.vPosition = mul(mViewProj, vVertices[i]);
		Output.vTexCoord = TexCoords[i];
		Output.fViewDepth = Output.vPosition.w / fFarNearDist;
		_TriStream.Append(Output);
	}
}
//---------------------------------------------------------------------------------------------------
PS_OUT PShader(PS_IN _In)
{
	uint3 PixelCoord = uint3(_In.vPosition.xy, 0);
	float fLinDepth = gDepthTex.Load(PixelCoord).r;

	float fDepthDeltaScreenPos = (fLinDepth - _In.fViewDepth); // scene depth - particle depth

	float fDistScale = saturate(fDepthDeltaScreenPos / 0.01f); // configure when depth fading should start
	fDistScale = Contrast(fDistScale, 2.0f); // control how harsh the opacity scales

	float4 vColor = gAlbedoMap.Sample(SamplerAnisotropicWrap, _In.vTexCoord) * vColorScale;
	vColor.a *= fDistScale;

	PS_OUT Output;

	Output.vColor = vColor;

	return Output;
}
//---------------------------------------------------------------------------------------------------
#endif

#endif //PARTICLE_HLSL
