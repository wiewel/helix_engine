//   Copyright 2020 Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef SSAO_HLSL
#define SSAO_HLSL

#include "ConstantBuffers.hlsli"
#include "Texture.hlsli"
#include "Helper.hlsli"

//---------------------------------------------------------------------------------------------------
NAMESPACE(SSAO)

CBUFFER(cbSSAO)
float fSSAORadius;
float fSSAOPower;
float2 fSSAOPAD;

float4 vSSAOKernel[SSAO_KERNEL_SIZE]; // float3
float4 vSSAONoise[NOISE_SIZE]; // float2
BUFFER_END

struct VS_IN
{
#ifdef HPMIO_Debug
	float2 vPosition SN(POSITION)
	float2 vTexCoord SN(TEXCOORD)
#else
	uint uID SN(SV_VERTEXID)
#endif
};

struct PS_IN
{
	float4 vPosition SN(SV_POSITION)
	float2 vTexCoord SN(TEXCOORD0)
	float3 vViewRay SN(TEXCOORD1)
};

struct PS_OUT
{
	float vOcclusion SN(SV_TARGET0)
	//float4 vBentNormal SN(SV_TARGET1)
};

#ifndef HLSL
enum HPMIOPermutation : uint32_t
{
	HPMIO_Debug = 1 << 0,
};
//enum HPMPSPermutation : uint32_t
//{
//	HPMPS_BentNormal = 1 << 0,
//};
#endif

NAMESPACE_END

//---------------------------------------------------------------------------------------------------
#ifdef HLSL

Texture2D<float> gDepthTex; //HTEXSLOT_PPCHAIN_DYNAMIC0	// Input from previous pass
Texture2D<float2> gNormalTex; // HTEXSLOT_PPCHAIN_DYNAMIC1 // world space

//---------------------------------------------------------------------------------------------------
PS_IN VShader(VS_IN _In)
{
	PS_IN Out = (PS_IN)0;

#ifdef HPMIO_Debug
	Out.vPosition = float4(_In.vPosition, 0.0f, 1.0f);
	Out.vTexCoord = _In.vTexCoord;
#else
	PosXYZWTexUV Tri = CalculateFullscreenTriangle(_In.uID);

	Out.vPosition = Tri.vPosition;
	Out.vTexCoord = Tri.vTexCoord;
#endif

	Out.vViewRay = GetViewRay(Out.vPosition.xy);

	/// only execute SSAO when depthbuffer value is smaller than own pixel depth (far plane)
	//Out.vPosition.z = Out.vPosition.w;

	return Out;
}
//---------------------------------------------------------------------------------------------------
//float LinearizeDepth(float2 _vTexCoord)
//{
//	uint3 PixelCoord = uint3(clamp(_vTexCoord * vBackbufferRes, float2(0, 0), vBackbufferRes), 0);
//	float fDepth = gDepthTex.Load(PixelCoord).r;
//	return mProj._43 / (fDepth - mProj._33);
//}
//---------------------------------------------------------------------------------------------------
//float3 ViewSpacePositionFromDepth(float2 _vTexCoord)
//{
//	uint3 PixelCoord = uint3(clamp(_vTexCoord * vBackbufferRes, float2(0, 0), vBackbufferRes), 0);
//	float fDepth = gDepthTex.Load(PixelCoord).r;
//	
//	float fX = _vTexCoord.x * 2.0f - 1.0f;
//	float fY = (1.0f - _vTexCoord.y) * 2.0f - 1.0f;
//
//	float4 vProjPos = float4(fX, fY, fDepth, 1.0f);
//	float4 vViewSpacePos = mul(vProjPos, mProjInv);
//
//	return vViewSpacePos.xyz / vViewSpacePos.w;
//}
//---------------------------------------------------------------------------------------------------
PS_OUT PShader(PS_IN _In)
{
	uint2 vTexDimension;
	gDepthTex.GetDimensions(vTexDimension.x, vTexDimension.y);

	uint3 PixelCoord = uint3(clamp(_In.vTexCoord * vTexDimension.xy, float2(0, 0), vTexDimension.xy), 0);
	float fLinDepth = gDepthTex.Sample(SamplerPointClamp, _In.vTexCoord).r;// +fNearDist;// *fFarNearDist;

	float3 vViewSpacePos = _In.vViewRay * fLinDepth * fFarDist; //ViewSpacePositionFromDepth(_In.vTexCoord);

	float3 vNormal = NormalDecode(gNormalTex.Sample(SamplerPointClamp, _In.vTexCoord).xy);
	vNormal = normalize(vNormal);

	// try to get in view space
	vNormal = normalize(mul(mView, float4(vNormal, 0)).xyz);

	uint uIndex = (PixelCoord.x % 4) + (PixelCoord.y * 4) % NOISE_SIZE;//(PixelCoord.x % 4) + (PixelCoord.y * 4) % NOISE_SIZE;
	float3 vNoise = float3(vSSAONoise[uIndex].xy, 0.0f); // should be: [-1.0f;1.0f]

	float3 vTangent = normalize(vNoise - vNormal * dot(vNoise, vNormal));
	float3 vBiTangent = cross(vTangent, vNormal);
	float3x3 mTBN = float3x3(vTangent, vBiTangent, vNormal); // kernel basis matrix

	float fOcclusion = 0.0f;

//#ifdef HPMPS_BentNormal
//	float3 vBentNormal = float3(0, 0, 0);
//	float fBentCone = 0.0f;
//	float fUnoccludedDirections = 0.0f;
//#endif

	[unroll]
	for (int i = 0; i < SSAO_KERNEL_SIZE; ++i)
	{
		// get sample position
		float3 vSample = mul(vSSAOKernel[i].xyz, mTBN);
		vSample = (vSample * fSSAORadius) + vViewSpacePos;

		// project sample position
		float4 vOffset = float4(vSample, 1.0f);
		vOffset = mul(mProj, vOffset); // ws: mViewProj
		vOffset.xy /= vOffset.w;
		vOffset.xy = vOffset.xy * 0.5f + 0.5f; // ndc to texcoords
		vOffset.y = 1.0f - vOffset.y; // ndc to texcoords

		// get sample depth
		float fSampleDepth = gDepthTex.Sample(SamplerLinearClamp, vOffset.xy).r * fFarDist;// +fNearDist;//gDepthTex.Load(PixelCoord).r;// *fFarNearDist; // uncomment for ws

		// range check, accumulation
		float fRangeCheck = smoothstep(0.0, 1.0, fSSAORadius / abs(vViewSpacePos.z - fSampleDepth)); // _In.vViewRay.z for ws instead of vViewSpacePos
		float fSampleOcclusion = fRangeCheck * step(fSampleDepth, vSample.z);
		fOcclusion += fSampleOcclusion; // for WS: abs(vSample.z- vCameraPos.z)

//#ifdef HPMPS_BentNormal
//		vBentNormal += (1.0f - fSampleOcclusion) * normalize(vSample - vViewSpacePos);
//		fUnoccludedDirections += (1.0f - fSampleOcclusion);
//#endif
	}

	fOcclusion = 1.0f - (fOcclusion / (float)SSAO_KERNEL_SIZE);
	fOcclusion = pow(saturate(fOcclusion), fSSAOPower);

//#ifdef HPMPS_BentNormal
//	//vBentNormal = normalize(vBentNormal);
//	//vBentNormal /= fUnoccludedDirections;
//	/// left multiplication by kPiHalf to keep value in range [0,1]
//	fBentCone = (1.0f - max(0.0f, (2 * length(vBentNormal.xyz / fUnoccludedDirections) - 1.0f)));// *kPiHalf;
//	
//	vBentNormal = mul(mViewInv, float4(vBentNormal, 0)).xyz;
//	vBentNormal = normalize(vBentNormal);
//#endif

	PS_OUT Output = (PS_OUT) 0;
	Output.vOcclusion = saturate(fOcclusion);

//#ifdef HPMPS_BentNormal
//	Output.vBentNormal.xyz = (vBentNormal + 1.0f) * 0.5f; // encode normal to [0,1]
//	Output.vBentNormal.w = fBentCone;
//#endif

	return Output;
}
//---------------------------------------------------------------------------------------------------
#endif

#endif // SSAO_HLSL
