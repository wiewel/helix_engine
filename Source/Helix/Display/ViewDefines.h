//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef VIEWDEFINES_H
#define VIEWDEFINES_H

#include "Util\Flag.h"
#include "Util\HashedString.h"
#include "hlx\src\Logger.h"
#include "hlx\src\String.h"

#include <array>
#include <vector>
#include "Math\MathTypes.h"
#include <concurrent_vector.h>

namespace Helix
{
	namespace Display
	{
		enum ShaderType
		{
			ShaderType_VertexShader = 0,
			ShaderType_HullShader = 1, // 2
			ShaderType_DomainShader = 2,
			ShaderType_GeometryShader = 3, // 4
			ShaderType_PixelShader = 4, // 2	
			ShaderType_ComputeShader = 5,
			ShaderType_NumOf = 6, // Num of stages
			ShaderType_None = 7,
		};

		static const TCHAR* ShaderTypeToString[ShaderType_NumOf] = 
		{
			{ S("VertexShader")},
			{ S("HullShader") },
			{ S("DomainShader") },
			{ S("GeometryShader")},
			{ S("PixelShader") },
			{ S("ComputeShader")}
		};

		//https://msdn.microsoft.com/en-us/library/windows/desktop/jj215820%28v=vs.85%29.aspx
		enum ShaderModel
		{
			ShaderModel_VS_5_0 = 0,
			ShaderModel_HS_5_0 = 1,
			ShaderModel_DS_5_0 = 2,
			ShaderModel_GS_5_0 = 3,
			ShaderModel_PS_5_0 = 4,
			ShaderModel_CS_5_0 = 5,

			ShaderModel_VS_4_1 = 6,
			ShaderModel_GS_4_1 = 7,
			ShaderModel_PS_4_1 = 8,
			ShaderModel_CS_4_1 = 9,

			ShaderModel_VS_4_0 = 10,
			ShaderModel_GS_4_0 = 11,
			ShaderModel_PS_4_0 = 12,
			ShaderModel_CS_4_0 = 13,

			ShaderModel_NumOf
		};

		struct ShaderModelDesc
		{
			ShaderModel kVersion;
			ShaderType kType;
			const char* pVersionString;
		};

		static const ShaderModelDesc ShaderModelVersions[ShaderModel_NumOf] =
		{
			{ ShaderModel_VS_5_0, ShaderType_VertexShader, "vs_5_0"},
			{ ShaderModel_HS_5_0, ShaderType_HullShader, "hs_5_0" },
			{ ShaderModel_DS_5_0, ShaderType_DomainShader, "ds_5_0" },
			{ ShaderModel_GS_5_0, ShaderType_GeometryShader, "gs_5_0" },
			{ ShaderModel_PS_5_0, ShaderType_PixelShader, "ps_5_0" },
			{ ShaderModel_CS_5_0, ShaderType_ComputeShader, "cs_5_0" },

			{ ShaderModel_VS_4_1, ShaderType_VertexShader, "vs_4_1" },
			{ ShaderModel_GS_4_1, ShaderType_GeometryShader, "gs_4_1" },
			{ ShaderModel_PS_4_1, ShaderType_PixelShader, "ps_4_1" },
			{ ShaderModel_CS_4_1, ShaderType_ComputeShader, "cs_4_1" },

			{ ShaderModel_VS_4_0, ShaderType_VertexShader, "vs_4_0" },
			{ ShaderModel_GS_4_0, ShaderType_GeometryShader, "gs_4_0" },
			{ ShaderModel_PS_4_0, ShaderType_PixelShader, "ps_4_0" },
			{ ShaderModel_CS_4_0, ShaderType_ComputeShader, "cs_4_0" }
		};

		struct ShaderPermutationHash
		{
			struct HashFunctor
			{
				inline const size_t operator()(const ShaderPermutationHash& _Perm) const { return _Perm.GetCombinedHash(); }
			};

			ShaderPermutationHash() : uClassPermutation(0u), uIOPermutation(0u), bRestraint(false){}
			~ShaderPermutationHash() {}

			ShaderPermutationHash(
				const uint32_t& _uClassPermutation,
				const uint32_t& _uIOPermutation,
				const std::vector<uint8_t>& _Ranges = {},
				bool _bRestraint = false) :
				uClassPermutation(_uClassPermutation),
				uIOPermutation(_uIOPermutation),
				Ranges(_Ranges),
				bRestraint(_bRestraint) {}

			ShaderPermutationHash(const ShaderPermutationHash& _Other) :
				uClassPermutation(_Other.uClassPermutation),
				uIOPermutation(_Other.uIOPermutation),
				Ranges(_Other.Ranges),
				bRestraint(_Other.bRestraint){}

			// just 'converts' io & class permutation
			inline operator uint64_t() const
			{
				return uBasePermutation;
			}

			inline friend bool operator==(const ShaderPermutationHash& l, const ShaderPermutationHash& r)
			{
				return l.uBasePermutation == r.uBasePermutation && l.bRestraint == r.bRestraint && l.Ranges == r.Ranges;
			}

			inline friend bool operator!=(const ShaderPermutationHash& l, const ShaderPermutationHash& r)
			{
				return l.uBasePermutation != r.uBasePermutation || l.bRestraint != r.bRestraint || l.Ranges != r.Ranges;
			}

			inline ShaderPermutationHash& operator=(const ShaderPermutationHash& _Other)
			{
				uClassPermutation = _Other.uClassPermutation;
				uIOPermutation = _Other.uIOPermutation;
				Ranges = _Other.Ranges;
				bRestraint = _Other.bRestraint;

				return *this;
			}

			inline uint64_t GetRangeHash() const
			{
				union
				{
					uint64_t uHash = 0ull;
					uint8_t uRange[8];
				};

				//for (size_t i = 0; i < Ranges.size(); ++i)
				//{
				//	uRange[i % 8] ^= Ranges[i];
				//}
				
				// reverse order lower risk of hash collision when combining with io & class perm
				size_t size = Ranges.size();
				for (size_t i = 0u; i < size; ++i)
				{
					uRange[(size - i - 1u) % 8] ^= Ranges[i];
				}

				return uHash;
			}

			inline uint64_t GetCombinedHash() const
			{
				return uBasePermutation ^ GetRangeHash();
			}

			inline bool SatisfiesConstraint(const ShaderPermutationHash& _Constraint) const
			{
				if (_Constraint.bRestraint)
				{
					if ((uBasePermutation & _Constraint.uBasePermutation) != 0u)
					{
						return false;
					}
				}
				else
				{
					if ((uBasePermutation & _Constraint.uBasePermutation) != _Constraint.uBasePermutation)
					{
						return false;
					}
				}				

				size_t uSize = std::min(Ranges.size(), _Constraint.Ranges.size());
				for (size_t i = 0; i < uSize; i++)
				{
					uint8_t uRangeConstraint = _Constraint.Ranges.at(i);
					if (_Constraint.bRestraint) 
					{
						// only indices greater uRangeConstraint pass through
						if (Ranges.at(i) <= uRangeConstraint)
						{
							return false;
						}
					}
					else
					{
						// constrain if greater uRangeConstraint => only smaller indices pass through
						if (Ranges.at(i) > uRangeConstraint)
						{
							return false;
						}
					}
				}

				return true;
			}

			inline void AddConstraint(const ShaderPermutationHash& _Constraint)
			{
				HASSERTD(_Constraint.bRestraint == bRestraint, "Constraint is not of same type (Restraint)");

				// Add class & io constraint
				uBasePermutation |= _Constraint.uBasePermutation;

				// add range constraint
				size_t uSize = std::min(Ranges.size(), _Constraint.Ranges.size());
				for (size_t i = 0; i < uSize; i++)
				{
					Ranges.at(i) |= _Constraint.Ranges.at(i);
				}

				// copy constraints
				for (size_t i = uSize; i < _Constraint.Ranges.size(); i++)
				{
					Ranges.push_back(_Constraint.Ranges.at(i));
				}
			}

			// base permutation
			union
			{
				struct
				{
					uint32_t uClassPermutation;
					uint32_t uIOPermutation;
				};

				uint64_t uBasePermutation;
			};

			// the permutation is used as a Restraint instead of a constraint
			// Restraint: Permutation with some bits matching the restraint will be pruned
			// Constraint: Permutation which dont match all constraint bits will be pruned
			bool bRestraint;

			std::vector<uint8_t> Ranges; // ranges for this class
		};
	
#pragma region  ResourceDefines
		//https://msdn.microsoft.com/de-de/library/windows/desktop/ff476259%28v=vs.85%29.aspx
		enum ResourceUsageFlag
		{
			ResourceUsageFlag_Default = 0,
			ResourceUsageFlag_Immutable = 1,
			ResourceUsageFlag_Dynamic = 2,
			ResourceUsageFlag_Staging = 3
		};

		//https://msdn.microsoft.com/de-de/library/windows/desktop/ff476085%28v=vs.85%29.aspx
		//In general, binding flags can be combined using a logical OR(except the constant - buffer flag);
		//however, you should use a single flag to allow the device to optimize the resource usage.
		Flag32E(ResourceBindFlag)
		{
			ResourceBindFlag_None = 0,
			ResourceBindFlag_VertexBuffer = 0x1,
			ResourceBindFlag_IndexBuffer = 0x2,
			ResourceBindFlag_ConstantBuffer = 0x4,
			ResourceBindFlag_ShaderResource = 0x8,
			ResourceBindFlag_StreamOutput = 0x10,
			ResourceBindFlag_RenderTarget = 0x20,
			ResourceBindFlag_DepthStencil = 0x40,
			ResourceBindFlag_UnorderedAccess = 0x80
		};

		//https://msdn.microsoft.com/de-de/library/windows/desktop/ff476106%28v=vs.85%29.aspx
		Flag32E(CPUAccessFlag)
		{
			CPUAccessFlag_None = 0,
			CPUAccessFlag_Write = 0x10000,
			CPUAccessFlag_Read = 0x20000,
			CPUAccessFlag_ReadWrite = 0x30000
		};

		enum ResourceMapType : uint32_t
		{
			ResourceMapType_Read = 1,
			ResourceMapType_Write = 2,
			ResourceMapType_Read_Write = 3,
			ResourceMapType_Write_Discard = 4,
			ResourceMapType_Write_No_Overwrite = 5
		};

		static inline CPUAccessFlag GetCPUAccess(ResourceUsageFlag _UsageFlag)
		{
			switch (_UsageFlag)
			{
			case ResourceUsageFlag_Dynamic:
				return CPUAccessFlag_Write;
				break;
			case ResourceUsageFlag_Staging:
				return CPUAccessFlag_ReadWrite;
				break;
			case ResourceUsageFlag_Default:
			case ResourceUsageFlag_Immutable:
			default:
				return CPUAccessFlag_None;
				break;
			}
		}

		//https://msdn.microsoft.com/en-us/library/windows/desktop/ff476203%28v=vs.85%29.aspx
		Flag32E(MiscResourceFlag)
		{
			MiscResourceFlag_None = 0, 
			MiscResourceFlag_GenerateMips = 0x1L,
			MiscResourceFlag_Shared = 0x2L,
			MiscResourceFlag_TextureCube = 0x4L,
			MiscResourceFlag_DrawIndirectArgs = 0x10L,
			MiscResourceFlag_Buffer_AllowRawViews = 0x20L,
			MiscResourceFlag_Buffer_Structured = 0x40L,
			MiscResourceFlag_ResourceClamp = 0x80L,
			MiscResourceFlag_Shared_KeyedMutex = 0x100L,
			MiscResourceFlag_GDICompatible= 0x200L,
			MiscResourceFlag_Shared_NtHandle = 0x800L,
			MiscResourceFlag_RestrictedContent = 0x1000L,
			MiscResourceFlag_Restrict_Shared_Resource = 0x2000L,
			MiscResourceFlag_Restrict_Shared_Resource_Driver = 0x4000L,
			MiscResourceFlag_Guarded = 0x8000L,
			MiscResourceFlag_TilePool = 0x20000L,
			MiscResourceFlag_Tiled = 0x40000L,
			MiscResourceFlag_HWProtected = 0x80000L
		};
#pragma endregion

		struct BufferSubresourceData
		{
			const void* pData = nullptr;
			//System-memory pitch is used only for 2D and 3D texture data as it is has no meaning for the other resource types.
			uint32_t uRowPitch = 0u; // SysMemPitch
			uint32_t uDepthPitch = 0u; // SysMemSlicePitch 
		};
		
		Flag32E(BufferUAVFlag)
		{
			BufferUAVFlag_None = 0,
			BufferUAVFlag_Raw = 1 << 0,
			BufferUAVFlag_Append = 1 << 1,
			BufferUAVFlag_Counter = 1 << 2
		};

		//Helix equivalent to D3D11_BUFFER_DESC
		struct BufferDesc
		{
			ResourceUsageFlag kUsageFlag;
			Flag<ResourceBindFlag> kBindFlag;
			Flag<CPUAccessFlag> kCPUAccessFlag;
			Flag<MiscResourceFlag> kResourceFlag; //"Misc" Flags
			BufferUAVFlag kUAVFlag;
			BufferSubresourceData SubresourceData;
			uint32_t uSize; // total size
			uint32_t uStructureByteStride; // element size
		};

#pragma region VertexDeclaration
		enum PrimitiveTopology : uint32_t
		{
			PrimitiveTopology_Undefined = 0,
			PrimitiveTopology_PointList = 1,
			PrimitiveTopology_LineList = 2,
			PrimitiveTopology_LineStrip = 3,
			PrimitiveTopology_TriangleList = 4,
			PrimitiveTopology_TriangleStrip = 5,
			PrimitiveTopology_NumOf
		};

		// Move to viewdefines
		Flag32E(VertexLayout)
		{
			VertexLayout_Pos_XYZW = 1 << 0,
			VertexLayout_Pos_XYZ = 1 << 1,
			VertexLayout_Pos_XY = 1 << 2,
			VertexLayout_Normal = 1 << 3,
			VertexLayout_Tangent = 1 << 4,
			VertexLayout_Bitangent = 1 << 5,
			VertexLayout_UV = 1 << 6,
			VertexLayout_Color = 1 << 7,

			// update this value every time you add a layout
			VertexLayout_NumOf = 8,
			VertexLayout_Unknown = 0,

			// known declarations
			VertexLayout_PosXYTex = VertexLayout_Pos_XY | VertexLayout_UV,
			VertexLayout_PosXYZTex = VertexLayout_Pos_XYZ | VertexLayout_UV,
			VertexLayout_PosXYZColor = VertexLayout_Pos_XYZ | VertexLayout_Color,
			VertexLayout_PosXYZNormTex = VertexLayout_Pos_XYZ | VertexLayout_Normal | VertexLayout_UV,
			VertexLayout_PosXYZNormTanTex = VertexLayout_Pos_XYZ | VertexLayout_Normal | VertexLayout_Tangent | VertexLayout_UV,
			VertexLayout_PosXYZNormTanBitanTex = VertexLayout_Pos_XYZ | VertexLayout_Normal | VertexLayout_Tangent | VertexLayout_Bitangent | VertexLayout_UV
		};

		using TVertexDeclaration = Flag<VertexLayout>;

		struct VertexLayoutDesc
		{
			const TCHAR* sName;
			VertexLayout Layout;
			uint32_t uSize;
		};

		static const VertexLayoutDesc VertexComponent[VertexLayout_NumOf] =
		{
			{ S("Postion_XYZW"),	VertexLayout_Pos_XYZW,	16	},
			{ S("Postion_XYZ"),		VertexLayout_Pos_XYZ,	12	},
			{ S("Postion_XY"),		VertexLayout_Pos_XY,	8	},
			{ S("Normal"),			VertexLayout_Normal,	12	},
			{ S("Tangent"),			VertexLayout_Tangent,	12	},
			{ S("Bitangent"),		VertexLayout_Bitangent,	12	},
			{ S("UV"),				VertexLayout_UV,		8	},
			{ S("Color"),			VertexLayout_Color,		12	}
		};

		static inline uint32_t GetVertexDeclarationSize(const TVertexDeclaration& _VertexDecl)
		{
			uint32_t uSize = 0;

			for (int i = 0; i < VertexLayout_NumOf; ++i)
			{
				if (_VertexDecl & static_cast<VertexLayout>(1 << i))
				{
					uSize += VertexComponent[i].uSize;
				}
			}

			return uSize;
		}

#pragma endregion

#pragma region TextureDefines
		// ResourceDimension
		enum ResourceDimension : uint8_t
		{
			ResourceDimension_Unknown = 0,
			ResourceDimension_Buffer = 1,
			ResourceDimension_Texture1D = 2,
			ResourceDimension_Texture1DArray = 3,
			ResourceDimension_Texture2D = 4,
			ResourceDimension_Texture2DArray = 5,
			ResourceDimension_Texture2DMS = 6,
			ResourceDimension_Texture2DMSArray = 7,
			ResourceDimension_Texture3D = 8,
			ResourceDimension_TextureCUBE = 9,
			ResourceDimension_TextureCUBEArray = 10,
			ResourceDimension_BufferEx = 11
		};

		// TODO: create string <-> enum mapping
		enum PixelFormat : uint32_t
		{
				PixelFormat_Unknown = 0,
				PixelFormat_R32G32B32A32_Typeless = 1,
				PixelFormat_R32G32B32A32_Float = 2,
				PixelFormat_R32G32B32A32_UInt = 3,
				PixelFormat_R32G32B32A32_SInt = 4,
				PixelFormat_R32G32B32_Typeless = 5,
				PixelFormat_R32G32B32_Float = 6,
				PixelFormat_R32G32B32_UInt = 7,
				PixelFormat_R32G32B32_SInt = 8,
				PixelFormat_R16G16B16A16_Typeless = 9,
				PixelFormat_R16G16B16A16_Float = 10,
				PixelFormat_R16G16B16A16_UNorm = 11,
				PixelFormat_R16G16B16A16_UInt = 12,
				PixelFormat_R16G16B16A16_SNorm = 13,
				PixelFormat_R16G16B16A16_SInt = 14,
				PixelFormat_R32G32_Typeless = 15,
				PixelFormat_R32G32_Float = 16,
				PixelFormat_R32G32_UInt = 17,
				PixelFormat_R32G32_SInt = 18,
				PixelFormat_R32G8X24_Typeless = 19,
				PixelFormat_D32_Float_S8X24_UInt = 20,
				PixelFormat_R32_Float_X8X24_Typeless = 21,
				PixelFormat_X32_Typeless_G8X24_UInt = 22,
				PixelFormat_R10G10B10A2_Typeless = 23,
				PixelFormat_R10G10B10A2_UNorm = 24,
				PixelFormat_R10G10B10A2_UInt = 25,
				PixelFormat_R11G11B10_Float = 26,
				PixelFormat_R8G8B8A8_Typeless = 27,
				PixelFormat_R8G8B8A8_UNorm = 28,
				PixelFormat_R8G8B8A8_UNorm_SRgb = 29,
				PixelFormat_R8G8B8A8_UInt = 30,
				PixelFormat_R8G8B8A8_SNorm = 31,
				PixelFormat_R8G8B8A8_SInt = 32,
				PixelFormat_R16G16_Typeless = 33,
				PixelFormat_R16G16_Float = 34,
				PixelFormat_R16G16_UNorm = 35,
				PixelFormat_R16G16_UInt = 36,
				PixelFormat_R16G16_SNorm = 37,
				PixelFormat_R16G16_SInt = 38,
				PixelFormat_R32_Typeless = 39,
				PixelFormat_D32_Float = 40,
				PixelFormat_R32_Float = 41,
				PixelFormat_R32_UInt = 42,
				PixelFormat_R32_SInt = 43,
				PixelFormat_R24G8_Typeless = 44,
				PixelFormat_D24_UNorm_S8_UInt = 45,
				PixelFormat_R24_UNorm_X8_Typeless = 46,
				PixelFormat_X24_Typeless_G8_UInt = 47,
				PixelFormat_R8G8_Typeless = 48,
				PixelFormat_R8G8_UNorm = 49,
				PixelFormat_R8G8_UInt = 50,
				PixelFormat_R8G8_SNorm = 51,
				PixelFormat_R8G8_SInt = 52,
				PixelFormat_R16_Typeless = 53,
				PixelFormat_R16_Float = 54,
				PixelFormat_D16_UNorm = 55,
				PixelFormat_R16_UNorm = 56,
				PixelFormat_R16_UInt = 57,
				PixelFormat_R16_SNorm = 58,
				PixelFormat_R16_SInt = 59,
				PixelFormat_R8_Typeless = 60,
				PixelFormat_R8_UNorm = 61,
				PixelFormat_R8_UInt = 62,
				PixelFormat_R8_SNorm = 63,
				PixelFormat_R8_SInt = 64,
				PixelFormat_A8_UNorm = 65,
				PixelFormat_R1_UNorm = 66,
				PixelFormat_R9G9B9E5_SHAREDEXP = 67,
				PixelFormat_R8G8_B8G8_UNorm = 68,
				PixelFormat_G8R8_G8B8_UNorm = 69,
				PixelFormat_BC1_Typeless = 70,
				PixelFormat_BC1_UNorm = 71,
				PixelFormat_BC1_UNorm_SRgb = 72,
				PixelFormat_BC2_Typeless = 73,
				PixelFormat_BC2_UNorm = 74,
				PixelFormat_BC2_UNorm_SRgb = 75,
				PixelFormat_BC3_Typeless = 76,
				PixelFormat_BC3_UNorm = 77,
				PixelFormat_BC3_UNorm_SRgb = 78,
				PixelFormat_BC4_Typeless = 79,
				PixelFormat_BC4_UNorm = 80,
				PixelFormat_BC4_SNorm = 81,
				PixelFormat_BC5_Typeless = 82,
				PixelFormat_BC5_UNorm = 83,
				PixelFormat_BC5_SNorm = 84,
				PixelFormat_B5G6R5_UNorm = 85,
				PixelFormat_B5G5R5A1_UNorm = 86,
				PixelFormat_B8G8R8A8_UNorm = 87,
				PixelFormat_B8G8R8X8_UNorm = 88,
				PixelFormat_R10G10B10_XR_BIAS_A2_UNorm = 89,
				PixelFormat_B8G8R8A8_Typeless = 90,
				PixelFormat_B8G8R8A8_UNorm_SRgb = 91,
				PixelFormat_B8G8R8X8_Typeless = 92,
				PixelFormat_B8G8R8X8_UNorm_SRgb = 93,
				PixelFormat_BC6H_Typeless = 94,
				PixelFormat_BC6H_UF16 = 95,
				PixelFormat_BC6H_SF16 = 96,
				PixelFormat_BC7_Typeless = 97,
				PixelFormat_BC7_UNorm = 98,
				PixelFormat_BC7_UNorm_SRgb = 99,
				PixelFormat_AYUV = 100,
				PixelFormat_Y410 = 101,
				PixelFormat_Y416 = 102,
				PixelFormat_NV12 = 103,
				PixelFormat_P010 = 104,
				PixelFormat_P016 = 105,
				PixelFormat_420_OPAQUE = 106,
				PixelFormat_YUY2 = 107,
				PixelFormat_Y210 = 108,
				PixelFormat_Y216 = 109,
				PixelFormat_NV11 = 110,
				PixelFormat_AI44 = 111,
				PixelFormat_IA44 = 112,
				PixelFormat_P8 = 113,
				PixelFormat_A8P8 = 114,
				PixelFormat_B4G4R4A4_UNorm = 115,
				PixelFormat_P208 = 130,
				PixelFormat_V208 = 131,
				PixelFormat_V408 = 132,
				PixelFormat_ASTC_4X4_UNorm = 134,
				PixelFormat_ASTC_4X4_UNorm_SRgb = 135,
				PixelFormat_ASTC_5X4_Typeless = 137,
				PixelFormat_ASTC_5X4_UNorm = 138,
				PixelFormat_ASTC_5X4_UNorm_SRgb = 139,
				PixelFormat_ASTC_5X5_Typeless = 141,
				PixelFormat_ASTC_5X5_UNorm = 142,
				PixelFormat_ASTC_5X5_UNorm_SRgb = 143,
				PixelFormat_ASTC_6X5_Typeless = 145,
				PixelFormat_ASTC_6X5_UNorm = 146,
				PixelFormat_ASTC_6X5_UNorm_SRgb = 147,
				PixelFormat_ASTC_6X6_Typeless = 149,
				PixelFormat_ASTC_6X6_UNorm = 150,
				PixelFormat_ASTC_6X6_UNorm_SRgb = 151,
				PixelFormat_ASTC_8X5_Typeless = 153,
				PixelFormat_ASTC_8X5_UNorm = 154,
				PixelFormat_ASTC_8X5_UNorm_SRgb = 155,
				PixelFormat_ASTC_8X6_Typeless = 157,
				PixelFormat_ASTC_8X6_UNorm = 158,
				PixelFormat_ASTC_8X6_UNorm_SRgb = 159,
				PixelFormat_ASTC_8X8_Typeless = 161,
				PixelFormat_ASTC_8X8_UNorm = 162,
				PixelFormat_ASTC_8X8_UNorm_SRgb = 163,
				PixelFormat_ASTC_10X5_Typeless = 165,
				PixelFormat_ASTC_10X5_UNorm = 166,
				PixelFormat_ASTC_10X5_UNorm_SRgb = 167,
				PixelFormat_ASTC_10X6_Typeless = 169,
				PixelFormat_ASTC_10X6_UNorm = 170,
				PixelFormat_ASTC_10X6_UNorm_SRgb = 171,
				PixelFormat_ASTC_10X8_Typeless = 173,
				PixelFormat_ASTC_10X8_UNorm = 174,
				PixelFormat_ASTC_10X8_UNorm_SRgb = 175,
				PixelFormat_ASTC_10X10_Typeless = 177,
				PixelFormat_ASTC_10X10_UNorm = 178,
				PixelFormat_ASTC_10X10_UNorm_SRgb = 179,
				PixelFormat_ASTC_12X10_Typeless = 181,
				PixelFormat_ASTC_12X10_UNorm = 182,
				PixelFormat_ASTC_12X10_UNorm_SRgb = 183,
				PixelFormat_ASTC_12X12_Typeless = 185,
				PixelFormat_ASTC_12X12_UNorm = 186,
				PixelFormat_ASTC_12X12_UNorm_SRgb = 187,
		};

		static inline bool IsSingleChannel(PixelFormat _Format)
		{
			switch (_Format)
			{
			case PixelFormat_D32_Float:
			case PixelFormat_R32_Float:
			case PixelFormat_R32_UInt:
			case PixelFormat_R32_SInt:
			case PixelFormat_R16_Float:
			case PixelFormat_D16_UNorm:
			case PixelFormat_R16_UNorm:
			case PixelFormat_R16_UInt:
			case PixelFormat_R16_SNorm:
			case PixelFormat_R16_SInt:
			case PixelFormat_R8_UNorm:
			case PixelFormat_R8_UInt:
			case PixelFormat_R8_SNorm:
			case PixelFormat_R8_SInt:
			case PixelFormat_A8_UNorm:
			case PixelFormat_R1_UNorm:
			case PixelFormat_R8_Typeless:
			case PixelFormat_R32_Typeless:
			case PixelFormat_R16_Typeless:
				return true;
				break;
			default:
				return false;
				break;
			}
		}

		static inline bool IsThreeChannel(PixelFormat _Format)
		{
			switch (_Format)
			{
			case PixelFormat_R32G32B32_Typeless:
			case PixelFormat_R32G32B32_Float:
			case PixelFormat_R32G32B32_UInt:
			case PixelFormat_R32G32B32_SInt:
			case PixelFormat_R11G11B10_Float: 
			case PixelFormat_B5G6R5_UNorm:
				return true;
				break;
			default:
				return false;
				break;
			}
		}

		static inline bool IsFourChannel(PixelFormat _Format)
		{
			switch (_Format)
			{
			case PixelFormat_R32G32B32A32_Typeless:
			case PixelFormat_R32G32B32A32_Float:
			case PixelFormat_R32G32B32A32_UInt:
			case PixelFormat_R32G32B32A32_SInt:
			case PixelFormat_R16G16B16A16_Typeless:
			case PixelFormat_R16G16B16A16_Float:
			case PixelFormat_R16G16B16A16_UNorm:
			case PixelFormat_R16G16B16A16_UInt:
			case PixelFormat_R16G16B16A16_SNorm:
			case PixelFormat_R16G16B16A16_SInt:
			case PixelFormat_R10G10B10A2_Typeless:
			case PixelFormat_R10G10B10A2_UNorm:
			case PixelFormat_R10G10B10A2_UInt:
			case PixelFormat_R8G8B8A8_Typeless:
			case PixelFormat_R8G8B8A8_UNorm:
			case PixelFormat_R8G8B8A8_UNorm_SRgb:
			case PixelFormat_R8G8B8A8_UInt:
			case PixelFormat_R8G8B8A8_SNorm:
			case PixelFormat_R8G8B8A8_SInt:
			case PixelFormat_B5G5R5A1_UNorm:
			case PixelFormat_B8G8R8A8_UNorm:
			case PixelFormat_R10G10B10_XR_BIAS_A2_UNorm:
			case PixelFormat_B8G8R8A8_Typeless:
			case PixelFormat_B8G8R8A8_UNorm_SRgb:
			case PixelFormat_B8G8R8X8_Typeless:
			case PixelFormat_B8G8R8X8_UNorm_SRgb:
				return true;
				break;
			default:
				return false;
				break;
			}
		}

		static inline bool IsDepthTypeless(PixelFormat _TypelessFormat)
		{
			switch (_TypelessFormat)
			{
			case PixelFormat_R32G8X24_Typeless:
			case PixelFormat_R24G8_Typeless:
			case PixelFormat_R32_Typeless:
			case PixelFormat_R16_Typeless:
				return true;
				break;
			default:
				return false;
				break;
			}
		}

		static inline PixelFormat GetDepthSRVFormatFromTypeless(PixelFormat _TypelessFormat)
		{
			switch (_TypelessFormat)
			{
			case PixelFormat_R32G8X24_Typeless:
				return PixelFormat_R32_Float_X8X24_Typeless;
				break;
			case PixelFormat_R24G8_Typeless:
				return PixelFormat_R24_UNorm_X8_Typeless;
				break;
			case PixelFormat_R32_Typeless:
				return PixelFormat_R32_Float;
				break;
			case PixelFormat_R16_Typeless:
				return PixelFormat_R16_Float;
				break;

			default:
				return _TypelessFormat;
				break;
			}
		}

		static inline PixelFormat GetDepthFormatFromTypeless(PixelFormat _TypelessFormat)
		{
			switch (_TypelessFormat)
			{
			case PixelFormat_R32G8X24_Typeless:
				return PixelFormat_D32_Float_S8X24_UInt;
				break;
			case PixelFormat_R24G8_Typeless:
				return PixelFormat_D24_UNorm_S8_UInt;
				break;
			case PixelFormat_R32_Typeless:
				return PixelFormat_D32_Float;
				break;
			case PixelFormat_R16_Typeless:
				return PixelFormat_D16_UNorm;
				break;		

			default:
				return _TypelessFormat;
				break;
			}
		}

		static inline bool IsStencilFormat(PixelFormat _PixelFormat)
		{
			switch (_PixelFormat)
			{
			case PixelFormat_R32G8X24_Typeless:
			case PixelFormat_R24G8_Typeless:
			case PixelFormat_D24_UNorm_S8_UInt:
			case PixelFormat_R24_UNorm_X8_Typeless:
			case PixelFormat_X24_Typeless_G8_UInt:
				return true;
			default:
				return false;
			}
		}

		static inline PixelFormat GetTypelessFormat(PixelFormat _PixelFormat)
		{
			switch (_PixelFormat)
			{
			case PixelFormat_R32G32B32A32_Typeless:
			case PixelFormat_R32G32B32A32_Float:
			case PixelFormat_R32G32B32A32_UInt:
			case PixelFormat_R32G32B32A32_SInt:
				return PixelFormat_R32G32B32A32_Typeless;
				break;

			case PixelFormat_R32G32B32_Typeless:
			case PixelFormat_R32G32B32_Float:
			case PixelFormat_R32G32B32_UInt:
			case PixelFormat_R32G32B32_SInt:
				return PixelFormat_R32G32B32_Typeless;
				break;

			case PixelFormat_R16G16B16A16_Typeless:
			case PixelFormat_R16G16B16A16_Float:
			case PixelFormat_R16G16B16A16_UNorm:
			case PixelFormat_R16G16B16A16_UInt:
			case PixelFormat_R16G16B16A16_SNorm:
			case PixelFormat_R16G16B16A16_SInt:
				return PixelFormat_R16G16B16A16_Typeless;
				break;

			case PixelFormat_R32G32_Typeless:
			case PixelFormat_R32G32_Float:
			case PixelFormat_R32G32_UInt:
			case PixelFormat_R32G32_SInt:
				return PixelFormat_R32G32_Typeless;
				break;

			case PixelFormat_R10G10B10A2_Typeless:
			case PixelFormat_R10G10B10A2_UNorm:
			case PixelFormat_R10G10B10A2_UInt:
				return PixelFormat_R10G10B10A2_Typeless;
				break;

			case PixelFormat_R8G8B8A8_Typeless:
			case PixelFormat_R8G8B8A8_UNorm:
			case PixelFormat_R8G8B8A8_UNorm_SRgb:
			case PixelFormat_R8G8B8A8_UInt:
			case PixelFormat_R8G8B8A8_SNorm:
			case PixelFormat_R8G8B8A8_SInt:
				return PixelFormat_R8G8B8A8_Typeless;
				break;

			case PixelFormat_R16G16_Typeless:
			case PixelFormat_R16G16_Float:
			case PixelFormat_R16G16_UNorm:
			case PixelFormat_R16G16_UInt:
			case PixelFormat_R16G16_SNorm:
			case PixelFormat_R16G16_SInt:
				return PixelFormat_R16G16_Typeless;
				break;

			case PixelFormat_R32_Typeless:
			case PixelFormat_D32_Float:
			case PixelFormat_R32_Float:
			case PixelFormat_R32_UInt:
			case PixelFormat_R32_SInt:
				return PixelFormat_R32_Typeless;
				break;

			case PixelFormat_R24G8_Typeless:
			case PixelFormat_D24_UNorm_S8_UInt:
			case PixelFormat_R24_UNorm_X8_Typeless:
			case PixelFormat_X24_Typeless_G8_UInt:
				return PixelFormat_R24G8_Typeless;
				break;

			case PixelFormat_R8G8_Typeless:
			case PixelFormat_R8G8_UNorm:
			case PixelFormat_R8G8_UInt:
			case PixelFormat_R8G8_SNorm:
			case PixelFormat_R8G8_SInt:
				return PixelFormat_R8G8_Typeless;
				break;

			case PixelFormat_R16_Typeless:
			case PixelFormat_R16_Float:
			case PixelFormat_D16_UNorm:
			case PixelFormat_R16_UNorm:
			case PixelFormat_R16_UInt:
			case PixelFormat_R16_SNorm:
			case PixelFormat_R16_SInt:
				return PixelFormat_R16_Typeless;
				break;

			case PixelFormat_R8_Typeless:
			case PixelFormat_R8_UNorm:
			case PixelFormat_R8_UInt:
			case PixelFormat_R8_SNorm:
			case PixelFormat_R8_SInt:
			case PixelFormat_A8_UNorm:
				return PixelFormat_R8_Typeless;
				break;

			case PixelFormat_BC1_Typeless:
			case PixelFormat_BC1_UNorm:
			case PixelFormat_BC1_UNorm_SRgb:
				return PixelFormat_BC1_Typeless;
				break;

			case PixelFormat_BC2_Typeless:
			case PixelFormat_BC2_UNorm:
			case PixelFormat_BC2_UNorm_SRgb:
				return PixelFormat_BC2_Typeless;
				break;

			case PixelFormat_BC3_Typeless:
			case PixelFormat_BC3_UNorm:
			case PixelFormat_BC3_UNorm_SRgb:
				return PixelFormat_BC3_Typeless;
				break;

			case PixelFormat_BC4_Typeless:
			case PixelFormat_BC4_UNorm:
			case PixelFormat_BC4_SNorm:
				return PixelFormat_BC4_Typeless;
				break;

			case PixelFormat_BC5_Typeless:
			case PixelFormat_BC5_UNorm:
			case PixelFormat_BC5_SNorm:
				return PixelFormat_BC5_Typeless;
				break;

			case PixelFormat_B8G8R8A8_Typeless:
			case PixelFormat_B8G8R8A8_UNorm:
			case PixelFormat_B8G8R8A8_UNorm_SRgb:
				return PixelFormat_B8G8R8A8_Typeless;
				break;

			case PixelFormat_B8G8R8X8_Typeless:
			case PixelFormat_B8G8R8X8_UNorm:
			case PixelFormat_B8G8R8X8_UNorm_SRgb:
				return PixelFormat_B8G8R8X8_Typeless;
				break;

			case PixelFormat_BC6H_Typeless:
			case PixelFormat_BC6H_UF16:
			case PixelFormat_BC6H_SF16:
				return PixelFormat_BC6H_Typeless;
				break;

			case PixelFormat_BC7_Typeless:
			case PixelFormat_BC7_UNorm:
			case PixelFormat_BC7_UNorm_SRgb:
				return PixelFormat_BC7_Typeless;
				break;

			case PixelFormat_ASTC_5X4_Typeless:
			case PixelFormat_ASTC_5X4_UNorm:
			case PixelFormat_ASTC_5X4_UNorm_SRgb:
				return PixelFormat_ASTC_5X4_Typeless;
				break;

			case PixelFormat_ASTC_5X5_Typeless:
			case PixelFormat_ASTC_5X5_UNorm:
			case PixelFormat_ASTC_5X5_UNorm_SRgb:
				return PixelFormat_ASTC_5X5_Typeless;
				break;

			case PixelFormat_ASTC_6X5_Typeless:
			case PixelFormat_ASTC_6X5_UNorm:
			case PixelFormat_ASTC_6X5_UNorm_SRgb:
				return PixelFormat_ASTC_6X5_Typeless;
				break;

			case PixelFormat_ASTC_6X6_Typeless:
			case PixelFormat_ASTC_6X6_UNorm:
			case PixelFormat_ASTC_6X6_UNorm_SRgb:
				return PixelFormat_ASTC_6X6_Typeless;
				break;

			case PixelFormat_ASTC_8X5_Typeless:
			case PixelFormat_ASTC_8X5_UNorm:
			case PixelFormat_ASTC_8X5_UNorm_SRgb:
				return PixelFormat_ASTC_8X5_Typeless;
				break;

			case PixelFormat_ASTC_8X6_Typeless:
			case PixelFormat_ASTC_8X6_UNorm:
			case PixelFormat_ASTC_8X6_UNorm_SRgb:
				return PixelFormat_ASTC_8X6_Typeless;
				break;

			case PixelFormat_ASTC_8X8_Typeless:
			case PixelFormat_ASTC_8X8_UNorm:
			case PixelFormat_ASTC_8X8_UNorm_SRgb:
				return PixelFormat_ASTC_8X8_Typeless;
				break;

			case PixelFormat_ASTC_10X5_Typeless:
			case PixelFormat_ASTC_10X5_UNorm:
			case PixelFormat_ASTC_10X5_UNorm_SRgb:
				return PixelFormat_ASTC_10X5_Typeless;
				break;

			case PixelFormat_ASTC_10X6_Typeless:
			case PixelFormat_ASTC_10X6_UNorm:
			case PixelFormat_ASTC_10X6_UNorm_SRgb:
				return PixelFormat_ASTC_10X6_Typeless;
				break;

			case PixelFormat_ASTC_10X8_Typeless:
			case PixelFormat_ASTC_10X8_UNorm:
			case PixelFormat_ASTC_10X8_UNorm_SRgb:
				return PixelFormat_ASTC_10X8_Typeless;
				break;

			case PixelFormat_ASTC_10X10_Typeless:
			case PixelFormat_ASTC_10X10_UNorm:
			case PixelFormat_ASTC_10X10_UNorm_SRgb:
				return PixelFormat_ASTC_10X10_Typeless;
				break;

			case PixelFormat_ASTC_12X10_Typeless:
			case PixelFormat_ASTC_12X10_UNorm:
			case PixelFormat_ASTC_12X10_UNorm_SRgb:
				return PixelFormat_ASTC_12X10_Typeless;
				break;

			case PixelFormat_ASTC_12X12_Typeless:
			case PixelFormat_ASTC_12X12_UNorm:
			case PixelFormat_ASTC_12X12_UNorm_SRgb:
				return PixelFormat_ASTC_12X12_Typeless;
				break;

			default:
				return _PixelFormat;
				break;
			}
		}

		struct TextureResizeProperties
		{
			std::string sEventName = {};
			float fWidthScale = 1.f;
			float fHeightScale = 1.f;
			float fDepthScale = 1.f;
		};

		struct TextureDesc
		{
			uint32_t m_uWidth;
			uint32_t m_uHeight;
			uint32_t m_uDepth;

			uint32_t m_uMipLevels;
			uint32_t m_uArraySize;

			//https://msdn.microsoft.com/en-us/library/windows/desktop/bb173072%28v=vs.85%29.aspx
			//DXGI_SAMPLE_DESC
			uint32_t m_uSampleCount;
			uint32_t m_uSampleQuality;

			ResourceDimension m_kResourceDimension;

			PixelFormat m_kFormat;
			ResourceUsageFlag m_kUsageFlag;
			Flag<ResourceBindFlag> m_kBindFlag;
			Flag<CPUAccessFlag> m_kCPUAccessFlag;
			Flag<MiscResourceFlag> m_kResourceFlag; //"Misc" Flags

			bool m_bForceSRgb;
			//If maxsize parameter non-zero, then all mipmap levels larger than the maxsize are ignored before creating the Direct3D 11 resource.
			//This allows for load-time scaling. If '0', then if the attempt to create the Direct3D 11 resource fails and there are mipmaps present,
			//it will retry assuming a maxsize based on the device's current feature level.
			//uint32_t m_uMaxMipMapLevel;

			// file path relative to "Data" directory
			std::string m_sFilePathOrName;

			bool m_bLoadFromFile;

			//excluded from Hashfunction:
			bool m_bPooled; // Search in TexturePool if TRUE
			//even if the texture is not pooled it can still be copied locally using the copy constructor

			// texture reference will be created but views and resources will not be initialized (Used for Backbuffer creation)
			bool m_bNoInit = false; // this will only work if m_bLoadFromFile is FALSE

			BufferSubresourceData m_InitialData = {};
		};

#pragma endregion

#pragma region SamplerDefines
		//https://msdn.microsoft.com/de-de/library/windows/desktop/ff476132%28v=vs.85%29.aspx
		enum TextureFilter : uint32_t
		{
			TextureFilter_Min_Mag_Mip_Point = 0,
			TextureFilter_Min_Mag_Point_Mip_Linear = 0x1,
			TextureFilter_Min_Point_Mag_Linear_Mip_Point = 0x4,
			TextureFilter_Min_Point_Mag_Mip_Linear = 0x5,
			TextureFilter_Min_Linear_Mag_Mip_Point = 0x10,
			TextureFilter_Min_Linear_Mag_Point_Mip_Linear = 0x11,
			TextureFilter_Min_Mag_Linear_Mip_Point = 0x14,
			TextureFilter_Min_Mag_Mip_Linear = 0x15,
			TextureFilter_Anisotropic = 0x55,
			TextureFilter_Comparison_Min_Mag_Mip_Point = 0x80,
			TextureFilter_Comparison_Min_Mag_Point_Mip_Linear = 0x81,
			TextureFilter_Comparison_Min_Point_Mag_Linear_Mip_Point = 0x84,
			TextureFilter_Comparison_Min_Point_Mag_Mip_Linear = 0x85,
			TextureFilter_Comparison_Min_Linear_Mag_Mip_Point = 0x90,
			TextureFilter_Comparison_Min_Linear_Mag_Point_Mip_Linear = 0x91,
			TextureFilter_Comparison_Min_Mag_Linear_Mip_Point = 0x94,
			TextureFilter_Comparison_Min_Mag_Mip_Linear = 0x95,
			TextureFilter_Comparison_Anisotropic = 0xd5,
			TextureFilter_Text_1Bit = 0x80000000
		};

		//https://msdn.microsoft.com/de-de/library/windows/desktop/ff476256%28v=vs.85%29.aspx
		enum TextureAddressMode : uint8_t
		{
			TextureAddressMode_Wrap = 1,
			TextureAddressMode_Mirror = 2,
			TextureAddressMode_Clamp = 3,
			TextureAddressMode_Border = 4,
			TextureAddressMode_Mirror_Once = 5
		};

		//https://msdn.microsoft.com/de-de/library/windows/desktop/ff476101%28v=vs.85%29.aspx
		enum ComparisonFunction : uint8_t
		{
			ComparisonFunction_Never = 1,
			ComparisonFunction_Less = 2,
			ComparisonFunction_Equal = 3,
			ComparisonFunction_LessEqual = 4,
			ComparisonFunction_Greater = 5,
			ComparisonFunction_NotEqual = 6,
			ComparisonFunction_GreaterEqual = 7,
			ComparisonFunction_Always = 8
		};

		// https://msdn.microsoft.com/en-us/library/windows/desktop/jj151678%28v=vs.85%29.aspx
		struct SamplerDesc
		{
			TextureFilter m_kFilter = TextureFilter_Min_Mag_Mip_Linear; // trilinear
			TextureAddressMode m_kAddressU = TextureAddressMode_Clamp;
			TextureAddressMode m_kAddressV = TextureAddressMode_Clamp;
			TextureAddressMode m_kAddressW = TextureAddressMode_Clamp;
			float m_fMipLODBias = 0.f;
			uint32_t m_uMaxAnisotropy = 1u;
			ComparisonFunction m_kComparisonFunction = ComparisonFunction_Never;
			std::array<float,4> m_vBorderColor = { 1.0f,1.0f,1.0f,1.0f };
			float m_fMinLOD = -FLT_MAX;
			float m_fMaxLOD = FLT_MAX;
		};
		
#pragma endregion


#pragma region RasterizerDefines
		//https://msdn.microsoft.com/de-de/library/windows/desktop/ff476131%28v=vs.85%29.aspx
		enum FillMode : uint8_t
		{
			FillMode_Wireframe = 2,
			FillMode_Solid = 3
		};

		//https://msdn.microsoft.com/de-de/library/windows/desktop/ff476108%28v=vs.85%29.aspx
		enum CullMode : uint8_t
		{
			CullMode_None = 1,
			CullMode_Front = 2,
			CullMode_Back = 3
		};

		//https://msdn.microsoft.com/de-de/library/windows/desktop/ff476198%28v=vs.85%29.aspx
		struct RasterizerDesc
		{
			FillMode m_kFillMode = FillMode_Solid;	// determines fill mode to use for rendering
									// D3D11_FILL_SOLID		=> fill triangles
									// D3D11_FILL_WIREFRAME	=> draw connecting lines
			CullMode m_kCullMode = CullMode_Back;	// indicates that triangles facing the specified direction are not drawn
									// D3D11_CULL_BACK		=> do not draw triangles that are back-facing
									// D3D11_CULL_FRONT		=> do not draw triangles that are front-facing
									// D3D11_CULL_NONE		=> draw every triangle
			bool m_bFrontCounterClockwise = false;	// determines if triangle is front- or back-facing
											// if set to true: triangle will be considered front-facing if its vertices are counter-clockwise on the render target, back-facing clockwise
			int32_t m_iDepthBias = 0;			// depth value added to a given pixel
			float m_fDepthBiasClamp = 0.f;		// max depth bias of a pixel
			float m_fSlopeScaledDepthBias = 0.f;	// scalar on a given pixels slope
			bool m_bDepthClipEnable = false;		// enables clipping based on distance
			bool m_bScissorEnable = false;	// enables scissor rectangle culling
									// true: all pixels outside an active scissor rectangle are culled, rectangles can be specified using RSSetScissorRects
			bool m_bMultisampleEnable = false;	// specifies which anti-aliasing algorithm to use
										// true: use quadrilateral line anti-aliasing algorithm on multisample anti-aliasing (MSAA) render targets
										// false: use alpha line anti-aliasing algorithm
			bool m_bAntialiasedLineEnable = false;	// specifies whether to enable line antialiasing
											// applies only if doing line drawing and MultsampleEnable is false
		};
#pragma endregion

#pragma region Blending
		//https://msdn.microsoft.com/de-de/library/windows/desktop/ff476086%28v=vs.85%29.aspx
		enum BlendVariable : uint8_t
		{
			BlendVariable_Zero = 1,
			BlendVariable_One = 2,
			BlendVariable_Src_Color = 3,
			BlendVariable_Inv_Src_Color = 4,
			BlendVariable_Src_Alpha = 5,
			BlendVariable_Inv_Src_Alpha = 6,
			BlendVariable_Dest_Alpha = 7,
			BlendVariable_Inv_Dest_Alpha = 8,
			BlendVariable_Dest_Color = 9,
			BlendVariable_Inv_Dest_Color = 10,
			BlendVariable_Src_Alpha_Sat = 11,
			BlendVariable_BlendFactor = 14,
			BlendVariable_Inv_BlendFactor = 15,
			BlendVariable_Src1_Color = 16,
			BlendVariable_Inv_Src1_Color = 17,
			BlendVariable_Src1_Alpha = 18,
			BlendVariable_Inv_Src1_Alpha = 19
		};

		//https://msdn.microsoft.com/de-de/library/windows/desktop/ff476088%28v=vs.85%29.aspx
		enum BlendOperation : uint8_t
		{
			BlendOperation_Add = 1,
			BlendOperation_Sub = 2,
			BlendOperation_Rev_Sub = 3,
			BlendOperation_Min = 4,
			BlendOperation_Max = 5
		};

		enum Channel : uint8_t
		{
			Channel_Red = 1,
			Channel_Green = 2,
			Channel_Blue = 4,
			Channel_Alpha = 8,
			Channel_RGB = (Channel_Red | Channel_Green | Channel_Blue),
			Channel_All = (Channel_Red | Channel_Green | Channel_Blue | Channel_Alpha)
		};

		//https://msdn.microsoft.com/de-de/library/windows/desktop/ff476087%28v=vs.85%29.aspx
		struct RenderTargetBlendDesc
		{
			inline bool IsDefault() const
			{
				return m_bBlendEnable == false &&
					m_kSrcBlend == BlendVariable_One &&
					m_kDestBlend == BlendVariable_Zero &&
					m_kBlendOp == BlendOperation_Add &&
					m_kSrcBlendAlpha == BlendVariable_One &&
					m_kDestBlendAlpha == BlendVariable_Zero &&
					m_kBlendOpAlpha == BlendOperation_Add &&
					m_uRenderTargetWriteMask == Channel_All;
			}

			bool m_bBlendEnable = false;
			BlendVariable m_kSrcBlend = BlendVariable_One;
			BlendVariable  m_kDestBlend = BlendVariable_Zero;
			BlendOperation m_kBlendOp = BlendOperation_Add;
			BlendVariable m_kSrcBlendAlpha = BlendVariable_One;
			BlendVariable m_kDestBlendAlpha = BlendVariable_Zero;
			BlendOperation m_kBlendOpAlpha = BlendOperation_Add;
			uint8_t	m_uRenderTargetWriteMask = Channel_All;
		};

		struct BlendDesc
		{
			//constexpr BlendDesc() {}
			bool m_bAlphaToCoverageEnable = false;
			bool m_bIndependentBlendEnable = false;
			std::array<RenderTargetBlendDesc, 8> m_RenderTargets = {};

			// not in dx blend desc
			std::array<float,4> fBlendFactor = { 0.0f,0.0f,0.0f,0.0f };
			uint32_t m_uSampleMask = 0xffffffff;
		};
#pragma endregion

#pragma region DepthStencilDefines
		//https://msdn.microsoft.com/en-us/library/windows/desktop/ff476116%28v=vs.85%29.aspx
		Flag32E(DepthStencilViewFlag)
		{
			DepthStencilViewFlag_None = 0,
			DepthStencilViewFlag_ReadOnlyDepth = 0x1L,
			DepthStencilViewFlag_ReadOnlyStencil = 0x2L
		};

		//https://msdn.microsoft.com/de-de/library/windows/desktop/ff476113%28v=vs.85%29.aspx
		enum DepthWriteMask : uint8_t
		{
			DepthWriteMask_Zero = 0,	//Turn off writes to the depth-stencil buffer.
			DepthWriteMask_All = 1		//Turn on writes to the depth-stencil buffer.
		};

		//https://msdn.microsoft.com/de-de/library/windows/desktop/ff476219%28v=vs.85%29.aspx
		enum StencilOperation : uint8_t
		{
			StencilOperation_Keep = 1,
			StencilOperation_Zero = 2,
			StencilOperation_Replace = 3,
			StencilOperation_IncrSat = 4,
			StencilOperation_DecrSat = 5,
			StencilOperation_Invert = 6,
			StencilOperation_Incr = 7,
			StencilOperation_Decr = 8
		};

		//https://msdn.microsoft.com/de-de/library/windows/desktop/ff476109%28v=vs.85%29.aspx
		struct DepthStencilOperationDesc
		{
			StencilOperation	m_kStencilFailOp;
			StencilOperation	m_kStencilDepthFailOp;
			StencilOperation	m_kStencilPassOp;
			ComparisonFunction	m_kStencilFunc;
		};

		//https://msdn.microsoft.com/de-de/library/windows/desktop/ff476110%28v=vs.85%29.aspx
		// TODO: rename to DepthStencilSTATEDesc
		struct DepthStencilDesc
		{
			bool						m_bDepthEnable = true;
			DepthWriteMask				m_kDepthWriteMask = DepthWriteMask_Zero;
			ComparisonFunction			m_kDepthFunc = ComparisonFunction_Never;
			bool						m_bStencilEnable = false;
			uint8_t						m_uStencilReadMask = 0xff;
			uint8_t						m_uStencilWriteMask = 0xff;
			DepthStencilOperationDesc	m_FrontFace;
			DepthStencilOperationDesc	m_BackFace;
		};
#pragma endregion

		struct SubMesh
		{
			SubMesh() {}
			//Vertex Info
			uint32_t m_uVertexByteOffset = 0; // offset from first byte in the buffer to the first vertex to be drawn in byte
			uint32_t m_uVertexStrideSize = 0;
			uint32_t m_uVertexCount = 0; // VertexCount * StrideSize = SubMeshSize
			TVertexDeclaration m_kVertexDeclaration = VertexLayout_Pos_XYZ; // verify with shader layout (permutation)
			PrimitiveTopology m_kPrimitiveTopology = PrimitiveTopology_Undefined;

			// Index Info
			uint32_t m_uIndexByteOffset = 0;// offset from first byte in the buffer to the first index to be drawn in byte
			uint32_t m_uIndexCount = 0; // IndexCount * PixelFormatSize = Total index size
			uint32_t m_uIndexElementSize = 0; // 2 or 4 byte
			PixelFormat m_kIndexBufferFormat = PixelFormat_R32_UInt; // U16 or U32

			std::string m_sSubName = {};
		};

		using TMeshCluster = std::vector<SubMesh>;

		//Vertex + Index Buffer:
		struct MeshClusterDesc
		{
			MeshClusterDesc() {};

			// construct form mesh info & vertex data
			MeshClusterDesc(const SubMesh& _Mesh, const BufferSubresourceData& _VertexData, const Flag<ResourceBindFlag>& _kAdditionalBindFlags = ResourceBindFlag_None) :
				m_kUsageFlag(ResourceUsageFlag_Default),
				m_kCPUAccessFlag(CPUAccessFlag_None),
				m_kAdditionalBindFlags(_kAdditionalBindFlags),
				m_sFilePathOrName(_Mesh.m_sSubName),
				m_bPooled(true),
				m_bLoadFromFile(false),
				m_VertexSubresourceData(_VertexData)
			{
				HASSERTD(_Mesh.m_uVertexCount * _Mesh.m_uVertexStrideSize <= _VertexData.uRowPitch, "Invalid vertex data size");
				m_MeshCluster.push_back(_Mesh);
			}

			// construct form mesh info & vertex + index data
			MeshClusterDesc(const SubMesh& _Mesh, const BufferSubresourceData& _VertexData, const BufferSubresourceData& _IndexData, const Flag<ResourceBindFlag>& _kAdditionalBindFlags = ResourceBindFlag_None) :
				m_kUsageFlag(ResourceUsageFlag_Default),
				m_kCPUAccessFlag(CPUAccessFlag_None),
				m_kAdditionalBindFlags(_kAdditionalBindFlags),
				m_sFilePathOrName(_Mesh.m_sSubName),
				m_bPooled(true),
				m_bLoadFromFile(false),
				m_VertexSubresourceData(_VertexData),
				m_IndexSubresourceData(_IndexData)
			{
				HASSERTD(_Mesh.m_uVertexCount * _Mesh.m_uVertexStrideSize <= _VertexData.uRowPitch, "Invalid vertex data size");
				HASSERTD(_Mesh.m_uIndexCount * _Mesh.m_uIndexElementSize <= _IndexData.uRowPitch, "Invalid index data size");
				m_MeshCluster.push_back(_Mesh);
			}

			Flag<ResourceBindFlag> m_kAdditionalBindFlags = ResourceBindFlag_None; // for SRVs / UAVs
			ResourceUsageFlag m_kUsageFlag = ResourceUsageFlag_Default; // Normal -> Default, Writeable -> Dynamic 
			Flag<CPUAccessFlag> m_kCPUAccessFlag = CPUAccessFlag_None; // Writeable -> CPUWrite

			// file path or unique identifier name of a mesh
			std::string m_sFilePathOrName; // must not be empty!

			//everything below this line is excluded from the HashFunction
			//////////////////////////////////////////////////////////////

			// If FALSE, new (dynamic or empty) buffer mesh will be created
			bool m_bLoadFromFile = true;

			// Search in MeshPool if TRUE
			//even if the buffer is not pooled it can still be copied locally using the copy constructor
			bool m_bPooled = true; 
			
			//data ptrs can change but still point to the same content, ignore them in HashFunction
			BufferSubresourceData m_VertexSubresourceData = {}; // Initial Vertex Data
			BufferSubresourceData m_IndexSubresourceData = {}; // Initial Index Data

			TMeshCluster m_MeshCluster;
		};
		
		struct MaterialProperties
		{
			Flag<uint64_t> kRenderPasses = 0u;
			std::string sAssociatedSubMesh;

			Math::float3  vAlbedo = HFLOAT3_ONE;

			float   fRoughness = 0.0f;
			float   fRoughnessOffset = 0.0f;
			float   fRoughnessScale = 1.0f;

			float   fMetallic = 0.0f;
			float   fHorizonFade = 0.0f;

			float   fHeightScale = 0.0f;
			Math::float2  vTextureTiling = { 1.0f, 1.0f };

			Math::float3 vEmissiveColor = { 0.0f, 0.0f, 0.0f };
			Math::float3 vEmissiveOffset = { 0.0f, 0.0f, 0.0f };
			Math::float3 vEmissiveScale = { 1.0f, 1.0f, 1.0f };

			bool	bUseAlbedoMap = false;
			bool	bUseNormalMap = false;
			bool	bUseMetallicMap = false;
			bool	bUseRoughnessMap = false;
			bool	bUseEmissiveMap = false;
			bool	bUseHeightMap = false;

			inline uint32_t GetMask() const
			{
				uint32_t uMask = 0;
				uMask |= bUseAlbedoMap ? 1 << 0 : 0;
				uMask |= bUseNormalMap ? 1 << 1 : 0;
				uMask |= bUseMetallicMap ? 1 << 2 : 0;
				uMask |= bUseRoughnessMap ? 1 << 3 : 0;
				uMask |= bUseHeightMap ? 1 << 4 : 0;
				uMask |= bUseEmissiveMap ? 1 << 5 : 0;
				return uMask;
			}
		};

		struct MaterialDesc
		{
			MaterialDesc() {};

			MaterialDesc(const MaterialProperties& _Properties, const std::string& _sFilePathOrName, bool _bLoadFromFile = true, bool _bLoadTextures = true) :
				sFilePathOrName(_sFilePathOrName),
				Properties(_Properties),
				bLoadFromFile(_bLoadFromFile),
				bLoadTextures(_bLoadTextures)
			{
			}

			MaterialDesc(const MaterialProperties& _Properties, 
				const std::string& _sFilePathOrName, const std::string& _sAlbedoMap,
				const std::string& _sNormalMap,	const std::string& _sHeightMap,
				const std::string& _sMetallicMap, const std::string& _sRoughnessMap,
				const std::string& _sEmissiveMap,
				bool _bLoadFromFile = true, bool _bLoadTextures = true) :
				sFilePathOrName(_sFilePathOrName),
				Properties(_Properties),
				bLoadFromFile(_bLoadFromFile),
				bLoadTextures(_bLoadTextures),
				sAlbedoMap(_sAlbedoMap),
				sNormalMap(_sNormalMap),
				sHeightMap(_sHeightMap),
				sMetallicMap(_sMetallicMap),
				sRoughnessMap(_sRoughnessMap),
				sEmissiveMap(_sEmissiveMap)
			{
			}

			// file path or unique identifier name of a material
			std::string sFilePathOrName; // must not be empty!

			//everything below this line is excluded from the HashFunction
			//////////////////////////////////////////////////////////////

			// If FALSE, create material from properties below
			bool bLoadFromFile = true;
			bool bLoadTextures = true;

			// must not be empty if bLoadFromFile = false
			std::string sAlbedoMap;
			std::string sNormalMap;
			std::string sMetallicMap;
			std::string sRoughnessMap;
			std::string sEmissiveMap;
			std::string sHeightMap;

			MaterialProperties Properties;
		};

		struct Viewport
		{
			inline friend bool operator==(const Viewport& l, const Viewport& r)
			{
				return (
					l.fHeight == r.fHeight &&
					l.fWidth == r.fWidth &&
					l.fMaxDepth == r.fMaxDepth &&
					l.fMinDepth == r.fMinDepth &&
					l.fTopLeftX == r.fTopLeftX &&
					l.fTopLeftY == r.fTopLeftY);
			}

			inline friend bool operator!=(const Viewport& l, const Viewport& r)
			{
				return !(l == r);
			}

			float fTopLeftX = 0.f;
			float fTopLeftY = 0.f;
			float fWidth = 0.f;
			float fHeight = 0.f;
			float fMinDepth = 0.f;
			float fMaxDepth = 1.f;
		};

		struct ViewSettings
		{
			//uint32_t uWindowWidth	= 0;
			//uint32_t uWindowHeight = 0;
			//uint32_t uXResolution	= 640;
			//uint32_t uYResolution	= 480;
			uint32_t uAntiAliasing = 1;
			uint32_t uMsaaQuality = 0;
			bool	 bVsync = true;
			bool	 bFullscreen = false;
		};

#pragma region ShaderReflection
		enum ShaderVariableType : uint32_t
		{
			ShaderVariableType_Void = 0,
			ShaderVariableType_Bool = 1,
			ShaderVariableType_Int = 2,
			ShaderVariableType_Float = 3,
			ShaderVariableType_String = 4,
			ShaderVariableType_Texture = 5,
			ShaderVariableType_Texture1D = 6,
			ShaderVariableType_Texture2D = 7,
			ShaderVariableType_Texture3D = 8,
			ShaderVariableType_TextureCube = 9,
			ShaderVariableType_Sampler = 10,
			ShaderVariableType_Sampler1D = 11,
			ShaderVariableType_Sampler2D = 12,
			ShaderVariableType_Sampler3D = 13,
			ShaderVariableType_SamplerCube = 14,
			ShaderVariableType_PixelShader = 15,
			ShaderVariableType_VertexShader = 16,
			ShaderVariableType_PixelFragment = 17,
			ShaderVariableType_VertexFragment = 18,
			ShaderVariableType_UInt = 19,
			ShaderVariableType_UInt8 = 20,
			ShaderVariableType_GeometryShader = 21,
			ShaderVariableType_Rasterizer = 22,
			ShaderVariableType_DepthStencil = 23,
			ShaderVariableType_Blend = 24,
			ShaderVariableType_Buffer = 25,
			ShaderVariableType_CBuffer = 26,
			ShaderVariableType_TBuffer = 27,
			ShaderVariableType_Texture1DArray = 28,
			ShaderVariableType_Texture2DArray = 29,
			ShaderVariableType_RenderTargetView = 30,
			ShaderVariableType_DepthStencilView = 31,
			ShaderVariableType_Texture2DMS = 32,
			ShaderVariableType_Texture2DMSArray = 33,
			ShaderVariableType_TextureCUBEArray = 34,
			ShaderVariableType_HullShader = 35,
			ShaderVariableType_DomainShader = 36,
			ShaderVariableType_InterfacePointer = 37,
			ShaderVariableType_ComputeShader = 38,
			ShaderVariableType_Double = 39,
			ShaderVariableType_RWTexture1D = 40,
			ShaderVariableType_RWTexture1DArray = 41,
			ShaderVariableType_RWTexture2D = 42,
			ShaderVariableType_RWTexture2DArray = 43,
			ShaderVariableType_RWTexture3D = 44,
			ShaderVariableType_RWBuffer = 45,
			ShaderVariableType_ByteAddress_Buffer = 46,
			ShaderVariableType_RWByteAddress_Buffer = 47,
			ShaderVariableType_Structured_Buffer = 48,
			ShaderVariableType_RWStructured_Buffer = 49,
			ShaderVariableType_Append_Structured_Buffer = 50,
			ShaderVariableType_Consume_Structured_Buffer = 51,
			ShaderVariableType_Min8Float = 52,
			ShaderVariableType_Min10Float = 53,
			ShaderVariableType_Min16Float = 54,
			ShaderVariableType_Min12Int = 55,
			ShaderVariableType_Min16Int = 56,
			ShaderVariableType_Min16UInt = 57
		};

		static inline uint32_t GetShaderVariableSize(ShaderVariableType _kType)
		{
			switch (_kType)
			{
			case Helix::Display::ShaderVariableType_Void:
				return sizeof(void*);
			case Helix::Display::ShaderVariableType_Bool:
				return sizeof(bool);
			case Helix::Display::ShaderVariableType_Int:
			case Helix::Display::ShaderVariableType_UInt:
				return sizeof(uint32_t);
			case Helix::Display::ShaderVariableType_Float:
				return sizeof(float);		
			case Helix::Display::ShaderVariableType_UInt8:
				return sizeof(uint8_t);
			case Helix::Display::ShaderVariableType_Double:
				return sizeof(double);
			default:
				return 0;
			}
		}

		static inline const char* GetShaderVariableTypeName(ShaderVariableType _kType)
		{
			switch (_kType)
			{
			case Helix::Display::ShaderVariableType_Void:
				return "void";
			case Helix::Display::ShaderVariableType_Bool:
				return "bool";
			case Helix::Display::ShaderVariableType_Int:
				return "int";
			case Helix::Display::ShaderVariableType_UInt:
				return "uint";
			case Helix::Display::ShaderVariableType_Float:
				return "float";
			case Helix::Display::ShaderVariableType_UInt8:
				return "uint8";
			case Helix::Display::ShaderVariableType_Double:
				return "double";
			default:
				return "?";
			}
		}

		enum ShaderVariableClass : uint32_t
		{
			ShaderVariableClass_Scalar = 0,
			ShaderVariableClass_Vector = 1,
			ShaderVariableClass_MatrixRows = 2,
			ShaderVariableClass_MatrixColumns = 3,
			ShaderVariableClass_Object = 4,
			ShaderVariableClass_Struct = 5,
			ShaderVariableClass_InterfaceClass = 6,
			ShaderVariableClass_InterfacePointer = 7
		};

		enum ConstantBufferType : uint8_t
		{
			ConstantBufferType_CBuffer = 0,
			ConstantBufferType_TBuffer = 1,
			ConstantBufferType_InterfacePointers = 2,
			ConstantBufferType_ResourceBindInfo = 3
		};

		enum SystemValue : uint8_t
		{
			SystemValue_Undefined = 0,
			SystemValue_Position = 1,
			SystemValue_Clip_Distance = 2,
			SystemValue_Cull_Distance = 3,
			SystemValue_RenderTarget_ArrayIndex = 4,
			SystemValue_ViewPort_ArrayIndex = 5,
			SystemValue_VertexID = 6,
			SystemValue_PrimitveID = 7,
			SystemValue_InstanceID = 8,
			SystemValue_IsFrontFace = 9,
			SystemValue_SampleIndex = 10,
			SystemValue_Final_Quad_Edge_TessFactor = 11,
			SystemValue_Final_Quat_Inside_TessFactor = 12,
			SystemValue_Final_Tri_Edge_TessFactor = 13,
			SystemValue_Final_Tri_Inside_TessFactor = 14,
			SystemValue_Final_Line_Detail_TessFactor = 15,
			SystemValue_Final_Line_Density_TessFactor = 16,
			SystemValue_Target = 64,
			SystemValue_Depth = 65,
			SystemValue_Coverage = 66,
			SystemValue_Depth_GreaterEqual = 67,
			SystemValue_Depth_LessEqual = 68
		};

		enum ShaderInputType : uint8_t
		{
			ShaderInputType_CBuffer = 0,
			ShaderInputType_TBuffer = 1,
			ShaderInputType_Texture = 2,
			ShaderInputType_Sampler = 3,
			ShaderInputType_UAV_RWTyped = 4,
			ShaderInputType_StructuredBuffer = 5,
			ShaderInputType_UAV_RWStructuredBuffer = 6,
			ShaderInputType_ByteAddress = 7,
			ShaderInputType_UAV_RWByteAddress = 8,
			ShaderInputType_UAV_Append_StructuredBuffer = 9,
			ShaderInputType_UAV_Consume_StructuredBuffer = 10,
			ShaderInputType_UAV_RWStructuredBuffer_WithCounter = 11
		};

		enum ShaderResourceReturnType : uint8_t
		{
			ShaderResourceReturnType_UNorm = 1,
			ShaderResourceReturnType_SNorm = 2,
			ShaderResourceReturnType_SInt = 3,
			ShaderResourceReturnType_UInt = 4,
			ShaderResourceReturnType_Float = 5,
			ShaderResourceReturnType_Mixed = 6,
			ShaderResourceReturnType_Double = 7,
			ShaderResourceReturnType_Continued = 8
		};

		struct ShaderVariableDesc
		{
			std::string sName; // variable name
			uint32_t uStartOffset = 0;
			uint32_t uSize = 0;
			uint32_t uFlags = 0;

			uint32_t uRows = 1;           // Number of rows (for matrices, 1 for other numeric, 0 if not applicable)
			uint32_t uColumns = 1;        // Number of columns (for vectors & matrices, 1 for other numeric, 0 if not applicable)
			uint32_t uElements = 0;       // Number of elements (0 if not an array)
			uint32_t uMembers = 0;        // Number of members (0 if not a structure)
			uint32_t uStructureOffset = 0;// Offset from the start of structure (0 if not a structure member)

			ShaderVariableClass kClass = ShaderVariableClass_Scalar;
			ShaderVariableType kType = ShaderVariableType_Float;

			std::vector<ShaderVariableDesc> Children;

			inline const std::string GetVariableIdentifier(uint32_t _uMemberOffset = 0) const
			{
				std::string sRet;
				sRet = std::string(Display::GetShaderVariableTypeName(kType));

				if (uColumns > 1)
				{
					sRet += std::to_string(uColumns);
				}

				if (uRows > 1)
				{
					sRet += "x" + std::to_string(uRows);
				}

				if (sName.empty())
				{
					sRet += " Member" + std::to_string(_uMemberOffset);
				}
				else
				{
					sRet += " " + sName;
				}				

				if (uElements > 1)
				{
					sRet + "[" + std::to_string(uElements) + "]";
				}

				return sRet;
			}
		};

		static bool operator== (const ShaderVariableDesc& _Left, const ShaderVariableDesc& _Right)
		{
			if (_Left.sName != _Right.sName ||
				_Left.uStartOffset != _Right.uStartOffset ||
				_Left.uSize != _Right.uSize ||
				//_Left.uFlags != _Right.uFlags ||
				_Left.uRows != _Right.uRows ||
				_Left.uColumns != _Right.uColumns ||
				_Left.uElements != _Right.uElements ||
				_Left.uMembers != _Right.uMembers ||
				_Left.uStructureOffset != _Right.uStructureOffset ||
				_Left.kClass != _Right.kClass ||
				_Left.kType != _Right.kType)
			{
				return false;
			}

			if (_Left.Children.size() != _Right.Children.size())
			{
				return false;
			}

			for (auto it1 = std::cbegin(_Left.Children), it2 = std::cbegin(_Right.Children);
				it1 != std::cend(_Left.Children) && it2 != std::cend(_Right.Children);
				++it1, ++it2)
			{
				if ((*it1 == *it2) == false)
				{
					return false;
				}
			}

			return true;
		}

		static bool operator!=(const ShaderVariableDesc& _Left, const ShaderVariableDesc& _Other) { return !operator==(_Left, _Other); }

		enum ShaderBufferType : uint8_t
		{
			ShaderBufferType_ConstantBuffer = 0,
			ShaderBufferType_TextureBuffer = 1,
			ShaderBufferType_UAV_RWTyped = 2,
			ShaderBufferType_Structured = 3,
			ShaderBufferType_UAV_RWStructured = 4,
			ShaderBufferType_ByteAddress = 5,
			ShaderBufferType_UAV_RWByteAddress = 6,
			ShaderBufferType_UAV_Append_Structured = 7,
			ShaderBufferType_UAV_Consume_Structured = 8,
			ShaderBufferType_UAV_RWStructured_With_Counter = 9,
			ShaderBufferType_Unknown = 10
		};

		struct BindInfoDesc
		{
			HashedStringValue sName;
			uint32_t uSlot = 0;
			uint32_t uSize = 0;
			uint32_t uCount = 1;// Number of contiguous bind points (for arrays)
		};

		// does not concider slot as it might change without harming the definition
		static bool operator==(const BindInfoDesc& _Left, const BindInfoDesc& _Right)
		{
			return _Left.sName == _Right.sName &&
				_Left.uCount == _Right.uCount &&
				_Left.uSize == _Right.uSize;
		}

		static bool operator!=(const BindInfoDesc& _Left, const BindInfoDesc& _Right) { return !operator==(_Left,_Right); }

		struct ShaderBufferDesc
		{
			BindInfoDesc BindInfo = {};
			ShaderBufferType kType = ShaderBufferType_Unknown;
			
			std::vector<ShaderVariableDesc> Variables;

			// returns size of data structure if its a structured buffer, zero otherwise
			inline uint32_t GetStructureSize() const
			{
				uint32_t uStructureSize = 0;
				switch (kType)
				{
				case ShaderBufferType_Structured:
				case ShaderBufferType_UAV_RWStructured:
				case ShaderBufferType_UAV_Append_Structured:
				case ShaderBufferType_UAV_Consume_Structured:
				case ShaderBufferType_UAV_RWStructured_With_Counter:
					for (const ShaderVariableDesc& Var : Variables)
					{
						uStructureSize += Var.uSize;
					}
					return uStructureSize;
				default:
					return 0;
				}
			}
		};

		static bool operator==(const ShaderBufferDesc& _Left, const ShaderBufferDesc& _Right)
		{
			if (_Left.BindInfo != _Right.BindInfo ||
				_Left.kType != _Right.kType)
			{
				return false;
			}

			if (_Left.Variables.size() != _Right.Variables.size())
			{
				return false;
			}

			for (auto it1 = std::cbegin(_Left.Variables), it2 = std::cbegin(_Right.Variables);
			it1 != std::cend(_Left.Variables) && it2 != std::cend(_Right.Variables);
				++it1, ++it2)
			{
				if ((*it1 == *it2) == false)
				{
					return false;
				}
			}

			return true;
		}

		static bool operator!=(const ShaderBufferDesc& _Left, const ShaderBufferDesc& _Right) { return !operator==(_Left, _Right); }

		struct ShaderTextureDesc
		{
			BindInfoDesc BindInfo = {};
			ResourceDimension kDimension = ResourceDimension_Texture2D;
			uint32_t uNumOfSamples = 0; // Number of samples (0 if not MS texture)
			ShaderResourceReturnType kReturnType; // Texture2D<ReturnType>
			bool bRWTexture = false;
		};

		struct ShaderSamplerDesc
		{
			BindInfoDesc BindInfo = {};
		};

		struct ShaderInterfaceDesc
		{
			std::vector<ShaderBufferDesc> Buffers;
			std::vector<ShaderTextureDesc> Textures;
			std::vector<ShaderSamplerDesc> Samplers;
			std::vector<BindInfoDesc> RenderTargets;
		};

		enum ShaderComponentType : uint8_t
		{
			ShaderComponentType_Unknown = 0,
			ShaderComponentType_UInt32 = 1,
			ShaderComponentType_SInt32 = 2,
			ShaderComponentType_Float32 = 3
		};

		enum ShaderMinPrecision : uint8_t
		{
			ShaderMinPrecision_Default = 0,
			ShaderMinPrecision_Float_16 = 1,
			ShaderMinPrecision_Float_2_8 = 2,
			ShaderMinPrecision_Reserved = 3,
			ShaderMinPrecision_SInt_16 = 4,
			ShaderMinPrecision_UInt_16 = 5,
			ShaderMinPrecision_Any_16 = 0xf0,
			ShaderMinPrecision_Any_10 = 0xf1
		};

		struct ShaderFunction
		{
			ShaderFunction(){}

			ShaderFunction(const std::string& _sFunctionName, Display::ShaderModel _kTargetVersion)
			{
				sFunctionName = _sFunctionName;
				kTargetVersion = _kTargetVersion;
			}

			inline std::string GetIdentifier() const
			{
				return sFunctionName + "_" + std::string(Display::ShaderModelVersions[kTargetVersion].pVersionString);
			}

			inline size_t GetHash() const
			{
				return Helix::StrHash(GetIdentifier());
			}

			std::string sFunctionName;
			Display::ShaderModel kTargetVersion;
		};

		struct InputElementDesc
		{
			uint32_t uSemanticIndex = 0;  // Index of the semantic
			uint32_t uRegister = 0;       // Number of member variables
			SystemValue kSystemValueType = SystemValue_Undefined;// A predefined system value, or D3D_NAME_UNDEFINED if not applicable
			ShaderComponentType kShaderComponentType = ShaderComponentType_Unknown;  // Scalar type (e.g. uint, float, etc.)
			uint8_t uMask = 0;           // Mask to indicate which components of the register
									 // are used (combination of D3D10_COMPONENT_MASK values)
			uint8_t uReadWriteMask = 0;  // Mask to indicate whether a given component is 
									// never written (if this is an output signature) or
									// always read (if this is an input signature).
									// (combination of D3D_MASK_* values)
			uint32_t uStream = 0;         // Stream index
			ShaderMinPrecision kMinPrecision = ShaderMinPrecision_Default;   // Minimum desired interpolation precision

			std::string sSemanticName;   // Name of the semantic
			std::string sVariableName; // Name of the variable with above semantic
		};

#pragma endregion
	} // Display
} // Helix

#endif //VIEWDEFINES_H