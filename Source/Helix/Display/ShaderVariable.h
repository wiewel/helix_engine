//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef SHADERVARIABLE_H
#define SHADERVARIABLE_H

#include "ViewDefines.h"

namespace Helix
{
	namespace Display
	{
		class GPUBufferInstanceDX11;
		class ShaderInterfaceDX11;

		class ShaderVariable
		{
			friend class ShaderInterfaceDX11;
		public:
			// variable paths are separated by '/' e.g. "CBPerView/mProj
			ShaderVariable(const std::string& _sVarPath, const uint32_t& _uVarSize);

			virtual ~ShaderVariable();

			bool Initialize(ShaderInterfaceDX11* _pInterface, const std::string& _sShaderInstanceName);

		protected:
			// called after a reload destoryed the underlying buffer and exchanged the references
			virtual void OnUpdate() {};

			// called when before the underlying buffer is invalidated
			virtual void OnInvalidate() {};

		private:
			void Update(std::shared_ptr<GPUBufferInstanceDX11>& _pBufferInstance);
			// return if name, size, element count or type changed, description wont be applied
			bool UpdateDesc(const ShaderVariableDesc& _Desc);
			// reset base ptr & buffer instance due to shader reload
			void Invalidate();

		protected:
			const std::string m_sVarPath;
			const uint32_t m_uVarSize;
			std::string m_sShaderInstanceName;

			ShaderInterfaceDX11* m_pInterface = nullptr;

			std::shared_ptr<GPUBufferInstanceDX11> m_pBufferInstance = nullptr;
			uint32_t m_uStructureSize = 0u; // size of structure for array access (structureSize * n + VariableOffset)
			uint8_t* m_pBase = nullptr;
			ShaderVariableDesc m_Description = {};
		};
	} // Display
} // Helix

#endif