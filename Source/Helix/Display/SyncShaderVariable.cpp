//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "SyncShaderVariable.h"
#include "DX11\GPUBufferInstanceDX11.h"

using namespace Helix;
using namespace Helix::Display;
//---------------------------------------------------------------------------------------------------

SyncShaderVariable::SyncShaderVariable(const ShaderVariableDesc& _Desc, GPUBufferInstanceDX11& _Buffer) :
	SyncVariableBase(GetVarType(_Desc),_Desc.sName),
	m_uSize(_Desc.uSize),
	m_Buffer(_Buffer)
{
	m_pVar = _Buffer.GetData() + _Desc.uStartOffset;
}
//---------------------------------------------------------------------------------------------------

Datastructures::VarType SyncShaderVariable::GetVarType(const ShaderVariableDesc& _Desc)
{
	using namespace Datastructures;

	switch (_Desc.kClass)
	{
	case ShaderVariableClass_Scalar:
		switch (_Desc.kType)
		{
		case ShaderVariableType_Int:
			return VarType_Int32;
		case ShaderVariableType_UInt:
			return VarType_UInt32;
		case ShaderVariableType_Float:
			return VarType_Float;
		case ShaderVariableType_Double:
			return VarType_Double;
		default:
			break;
		}
	case ShaderVariableClass_Vector:
		switch (_Desc.kType)
		{
		case ShaderVariableType_Int:
			switch (_Desc.uColumns)
			{
			case 2:	return VarType_Int2;
			case 3:	return VarType_Int3;
			case 4:	return VarType_Int4;
			default:
				break;
			}
		case ShaderVariableType_UInt:
			switch (_Desc.uColumns)
			{
			case 2:	return VarType_UInt2;
			case 3:	return VarType_UInt3;
			case 4:	return VarType_UInt4;
			default:
				break;
			}
		case ShaderVariableType_Float:
			switch (_Desc.uColumns)
			{
			case 2:	return VarType_Float2;
			case 3:	return VarType_Float3;
			case 4:	return VarType_Float4;
			default:
				break;
			}
		//case ShaderVariableType_Double: // not supported atm
		default:
			break;
		}

	case ShaderVariableClass_MatrixRows:
	case ShaderVariableClass_MatrixColumns:
	default:
		break;
	}

	return VarType_Unknown;
}
//---------------------------------------------------------------------------------------------------

void SyncShaderVariable::Set(const void* _pData)
{
	memcpy_s(m_pVar, m_uSize, _pData, m_uSize);
	m_Buffer.SetDataChanged();
}
//---------------------------------------------------------------------------------------------------
