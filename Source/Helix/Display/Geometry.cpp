//   Copyright 2020 Fabian Wahlster
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "Geometry.h"
#include "VertexLayouts.h"

using namespace Helix::Display;

//---------------------------------------------------------------------------------------------------
Geometry::Geometry()
{
}
//---------------------------------------------------------------------------------------------------
Geometry::~Geometry()
{
}
//---------------------------------------------------------------------------------------------------
MeshClusterDX11 Geometry::GenerateCube(float _fSize, const char* _pName )
{
	SubMesh Cube = {};

	if (_pName != nullptr)
	{
		Cube.m_sSubName = std::string(_pName);
	}
	else
	{
		Cube.m_sSubName = "Cube_" + std::to_string(_fSize);
	}	

	Cube.m_kPrimitiveTopology = PrimitiveTopology_TriangleList;
	Cube.m_kVertexDeclaration = VertexLayout_PosXYZNormTanTex;

	Cube.m_uVertexCount = 24;

	Cube.m_uVertexStrideSize = GetVertexDeclarationSize(Cube.m_kVertexDeclaration);
	HASSERTD(Cube.m_uVertexStrideSize == sizeof(VertexPosXYZNormTanTex), "Invalid stride size");

	Cube.m_uVertexByteOffset = 0;

	Cube.m_kIndexBufferFormat = PixelFormat_R16_UInt;
	Cube.m_uIndexElementSize = sizeof(uint16_t);
	Cube.m_uIndexByteOffset = 0;
	Cube.m_uIndexCount = 36;

	BufferSubresourceData VertexData = {};
	BufferSubresourceData IndexData = {};

	VertexData.uRowPitch = Cube.m_uVertexStrideSize * Cube.m_uVertexCount;
	IndexData.uRowPitch = Cube.m_uIndexElementSize * Cube.m_uIndexCount;

	VertexPosXYZNormTanTex* pVertexData = new VertexPosXYZNormTanTex[Cube.m_uVertexCount];
	uint16_t* pIndexData = new uint16_t[Cube.m_uIndexCount];

	VertexData.pData = pVertexData;
	IndexData.pData = pIndexData;

	uint32_t uVertexCount = 0;

	_fSize *= 0.5f;

	// Fill in the front face vertex data.
	pVertexData[uVertexCount++] = {-_fSize, -_fSize, -_fSize, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f};
	pVertexData[uVertexCount++] = {-_fSize, +_fSize, -_fSize, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f};
	pVertexData[uVertexCount++] = {+_fSize, +_fSize, -_fSize, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f};
	pVertexData[uVertexCount++] = {+_fSize, -_fSize, -_fSize, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f};

	// Fill in the back face vertex data.
	pVertexData[uVertexCount++] = {-_fSize, -_fSize, +_fSize, 0.0f, 0.0f, 1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 1.0f};
	pVertexData[uVertexCount++] = {+_fSize, -_fSize, +_fSize, 0.0f, 0.0f, 1.0f, -1.0f, 0.0f, 0.0f, 0.0f, 1.0f};
	pVertexData[uVertexCount++] = {+_fSize, +_fSize, +_fSize, 0.0f, 0.0f, 1.0f, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f};
	pVertexData[uVertexCount++] = {-_fSize, +_fSize, +_fSize, 0.0f, 0.0f, 1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f};

	// Fill in the top face vertex data.
	pVertexData[uVertexCount++] = {-_fSize, +_fSize, -_fSize, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f};
	pVertexData[uVertexCount++] = {-_fSize, +_fSize, +_fSize, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f};
	pVertexData[uVertexCount++] = {+_fSize, +_fSize, +_fSize, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f};
	pVertexData[uVertexCount++] = {+_fSize, +_fSize, -_fSize, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f};

	// Fill in the bottom face vertex data.
	pVertexData[uVertexCount++] = {-_fSize, -_fSize, -_fSize, 0.0f, -1.0f, 0.0f, -1.0f, 0.0f, 0.0f, 1.0f, 1.0f};
	pVertexData[uVertexCount++] = {+_fSize, -_fSize, -_fSize, 0.0f, -1.0f, 0.0f, -1.0f, 0.0f, 0.0f, 0.0f, 1.0f};
	pVertexData[uVertexCount++] = {+_fSize, -_fSize, +_fSize, 0.0f, -1.0f, 0.0f, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f};
	pVertexData[uVertexCount++] = {-_fSize, -_fSize, +_fSize, 0.0f, -1.0f, 0.0f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f};

	// Fill in the left face vertex data.
	pVertexData[uVertexCount++] = {-_fSize, -_fSize, +_fSize, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f};
	pVertexData[uVertexCount++] = {-_fSize, +_fSize, +_fSize, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f};
	pVertexData[uVertexCount++] = {-_fSize, +_fSize, -_fSize, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f};
	pVertexData[uVertexCount++] = {-_fSize, -_fSize, -_fSize, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f, -1.0f, 1.0f, 1.0f};

	// Fill in the right face vertex data.
	pVertexData[uVertexCount++] = {+_fSize, -_fSize, -_fSize, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f};
	pVertexData[uVertexCount++] = {+_fSize, +_fSize, -_fSize, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f};
	pVertexData[uVertexCount++] = {+_fSize, +_fSize, +_fSize, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f};
	pVertexData[uVertexCount++] = {+_fSize, -_fSize, +_fSize, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f};

	// Fill in the front face index data
	pIndexData[0] = 0; pIndexData[1] = 1; pIndexData[2] = 2;
	pIndexData[3] = 0; pIndexData[4] = 2; pIndexData[5] = 3;

	// Fill in the back face index data
	pIndexData[6] = 4; pIndexData[7] = 5; pIndexData[8] = 6;
	pIndexData[9] = 4; pIndexData[10] = 6; pIndexData[11] = 7;

	// Fill in the top face index data
	pIndexData[12] = 8; pIndexData[13] = 9; pIndexData[14] = 10;
	pIndexData[15] = 8; pIndexData[16] = 10; pIndexData[17] = 11;

	// Fill in the bottom face index data
	pIndexData[18] = 12; pIndexData[19] = 13; pIndexData[20] = 14;
	pIndexData[21] = 12; pIndexData[22] = 14; pIndexData[23] = 15;

	// Fill in the left face index data
	pIndexData[24] = 16; pIndexData[25] = 17; pIndexData[26] = 18;
	pIndexData[27] = 16; pIndexData[28] = 18; pIndexData[29] = 19;

	// Fill in the right face index data
	pIndexData[30] = 20; pIndexData[31] = 21; pIndexData[32] = 22;
	pIndexData[33] = 20; pIndexData[34] = 22; pIndexData[35] = 23;

	MeshClusterDX11 CubeCluster(MeshClusterDesc(Cube, VertexData, IndexData));

	delete[] pVertexData;
	delete[] pIndexData;

	return CubeCluster;
}
//---------------------------------------------------------------------------------------------------
MeshClusterDX11 Geometry::Generate2DPlane(float _fSize, const char* _pName)
{
	SubMesh Plane = {};

	if (_pName != nullptr)
	{
		Plane.m_sSubName = std::string(_pName);
	}
	else
	{
		Plane.m_sSubName = "Plane_" + std::to_string(_fSize);
	}

	Plane.m_kPrimitiveTopology = PrimitiveTopology_TriangleStrip;
	Plane.m_kVertexDeclaration = VertexLayout_PosXYTex;

	Plane.m_uVertexCount = 4;

	Plane.m_uVertexStrideSize = GetVertexDeclarationSize(Plane.m_kVertexDeclaration);
	HASSERTD(Plane.m_uVertexStrideSize == sizeof(VertexPosXYTex), "Invalid stride size");

	Plane.m_uVertexByteOffset = 0;

	Plane.m_kIndexBufferFormat = PixelFormat_R16_UInt;
	Plane.m_uIndexElementSize = 0;
	Plane.m_uIndexByteOffset = 0;
	Plane.m_uIndexCount = 0;

	BufferSubresourceData VertexData = {};

	VertexData.uRowPitch = Plane.m_uVertexStrideSize * Plane.m_uVertexCount;

	VertexPosXYTex* pVertexData = new VertexPosXYTex[Plane.m_uVertexCount];

	VertexData.pData = pVertexData;

	uint32_t uVertexCount = 0;

	// Fill in the front face vertex data.
	pVertexData[uVertexCount++] = { -_fSize, -_fSize, 0.0f, 1.0f };
	pVertexData[uVertexCount++] = { -_fSize, +_fSize, 0.0f, 0.0f };
	pVertexData[uVertexCount++] = { +_fSize, -_fSize, 1.0f, 1.0f };
	pVertexData[uVertexCount++] = { +_fSize, +_fSize, 1.0f, 0.0f };

	MeshClusterDX11 CubeCluster(MeshClusterDesc(Plane, VertexData));

	delete[] pVertexData;

	return CubeCluster;
}
//---------------------------------------------------------------------------------------------------
