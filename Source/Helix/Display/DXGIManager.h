//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef DXGIMANAGER_H
#define DXGIMANAGER_H

#include <D3D11_2.h>
#include <vector>
#include <cstdint>
#include <sstream>
#include <map>

#include "ViewDefines.h"
#include "hlx\src\Singleton.h"
#include "hlx\src\Logger.h"

namespace Helix 
{
	namespace Display
	{
		class DXGIManager : public hlx::Singleton<DXGIManager>
		{
			friend class RenderViewDX11;

		public:
			HDEBUGNAME("DXGIManager");

			DXGIManager();
			~DXGIManager();

			void Initialize();
			void Shutdown(void);

			IDXGISwapChain* InitSwapChain(ID3D11Device* _pDevice, HWND _hWnd, ViewSettings& _ViewSettings);

			///<summary>Set primary display settings like resolution and refresh rate.</summary>
			///<param name='_uId'>Addresses the content of m_DisplayModes at Position _uId.</param>
			///<param name='_ViewSettings'>Changes _ViewSettings accordingly to new display mode.</param>
			void SetMainDisplayModeByIndex(_In_ const uint32_t _uId/*, _Out_ ViewSettings&	_ViewSettings*/);

			///<summary>Set primary graphics device.</summary>
			///<param name='_uId'>Addresses the content of m_vAdapters at Position _uId.</param>
			void SetMainAdapterByIndex(_In_ const uint32_t _uId);

			///<summary>Set primary display.</summary>
			///<param name='_uId'>Addresses the content of m_vOutputs at Position _uId.</param>
			void SetMainOutputByIndex(_In_ const uint32_t _uId);

			IDXGISwapChain* GetSwapChain() const;

			uint32_t GetMainDisplayModeIndex() const;
			DXGI_MODE_DESC GetMainDisplayMode() const;
			IDXGIAdapter1* GetMainAdapter() const;
			IDXGIOutput*   GetMainOutput() const;

			const std::vector<DXGI_MODE_DESC>& GetDisplayModes() const;
			//void GetDisplayModes(_Out_ std::vector<std::string>& _DisplayModes) const;
			void GetAdapters(_Out_ std::vector<std::string>& _Adapters) const;
			void GetOutputs(_Out_ std::vector<std::string>& _Outputs) const;

		private:
			HRESULT InitDXGIFactory();
			HRESULT InitDXGIAdapter();
			HRESULT InitDXGIOutput();
			HRESULT InitDisplayModes();

		private:
			IDXGIFactory2*				m_pFactory = nullptr;
			IDXGISwapChain*				m_pSwapchain = nullptr;

			std::vector<DXGI_MODE_DESC> m_DisplayModes;
			std::map<uint32_t, IDXGIAdapter1*> m_Adapters;
			std::map<uint32_t, IDXGIOutput*>   m_Outputs;

			uint32_t m_uMainDisplayMode = 0;
			uint32_t m_uMainAdapter = 0; // selected graphics card
			uint32_t m_uMainOutput = 0; // selected monitor
		};

		inline void DXGIManager::SetMainDisplayModeByIndex(const uint32_t _uId/*, ViewSettings&	_ViewSettings*/)
		{
			HASSERTD(_uId < m_DisplayModes.size(), "ID is not in display-mode range!");

			m_uMainDisplayMode = _uId;
		}

		inline const std::vector<DXGI_MODE_DESC>& DXGIManager::GetDisplayModes() const
		{
			return m_DisplayModes;
		}

		inline void DXGIManager::SetMainAdapterByIndex(const uint32_t _uId)
		{
			HASSERTD(_uId < m_Adapters.size(), "ID is not in adapter range!");
	
			m_uMainAdapter = _uId;
		}
		inline void DXGIManager::SetMainOutputByIndex(const uint32_t _uId)
		{
			HASSERTD(_uId < m_Outputs.size(), "ID is not in outputs range!");

			m_uMainOutput = _uId;
		}

		inline IDXGISwapChain* DXGIManager::GetSwapChain() const
		{
			HASSERTD(m_pSwapchain != nullptr, "Swapchain not initialized!");

			return m_pSwapchain;
		}

		inline uint32_t DXGIManager::GetMainDisplayModeIndex() const
		{
			return m_uMainDisplayMode;
		}
		inline DXGI_MODE_DESC DXGIManager::GetMainDisplayMode() const
		{
			HASSERTD(m_uMainDisplayMode < m_DisplayModes.size(), "Main-Display-Mode is not in Display-Modes range!");

			return m_DisplayModes.at(m_uMainDisplayMode);
		}
		inline IDXGIAdapter1* DXGIManager::GetMainAdapter() const
		{
			HASSERTD(m_uMainAdapter < m_Adapters.size(), "Main-Adapter is not in adapters range!");

			return m_Adapters.at(m_uMainAdapter);
		}
		inline IDXGIOutput* DXGIManager::GetMainOutput() const
		{
			HASSERTD(m_uMainOutput < m_Outputs.size(), "Main-Output is not in outputs range!");

			return m_Outputs.at(m_uMainOutput);
		}

		//inline void DXGIManager::GetDisplayModes(std::vector<std::string>& _DisplayModes) const
		//{
		//	_DisplayModes.clear();

		//	std::string sTmpBuf;
		//	std::stringstream StringStream;
		//	StringStream.precision(4);

		//	HFOREACH(it, end, m_DisplayModes)
		//		DXGI_MODE_DESC TempMode = it->second;

		//		// Clear StringStream
		//		StringStream.str(std::string());

		//		StringStream << TempMode.Width << "x" << TempMode.Height << "@" << (static_cast<float>(TempMode.RefreshRate.Numerator) / static_cast<float>(TempMode.RefreshRate.Denominator)) << "Hz";

		//		_DisplayModes.push_back(StringStream.str());
		//	HFOREACH_END
		//}

		inline void DXGIManager::GetAdapters(std::vector<std::string>& _Adapters) const
		{
			_Adapters.clear();

			HFOREACH(it, end, m_Adapters)
				DXGI_ADAPTER_DESC TempDesc;
				it->second->GetDesc(&TempDesc);

				size_t uCharsConverted = 0;

				char sTmp[128];
				wcstombs_s(&uCharsConverted, sTmp, TempDesc.Description, 128);
				_Adapters.push_back(sTmp);
			HFOREACH_END
		}
		inline void DXGIManager::GetOutputs(std::vector<std::string>& _Outputs) const
		{
			_Outputs.clear();

			HFOREACH(it, end, m_Outputs)
				DXGI_OUTPUT_DESC TempDesc;
				it->second->GetDesc(&TempDesc);

				size_t uCharsConverted = 0;

				char sTmp[32];
				wcstombs_s(&uCharsConverted, sTmp, TempDesc.DeviceName, 32);

				_Outputs.push_back(sTmp);
			HFOREACH_END
		}

	} // Display
} // Helix


#endif