//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "TextureAtlas.h"

using namespace Helix::Display;
using namespace Helix::Math;

//---------------------------------------------------------------------------------------------------
TextureAtlas::TextureAtlas()
{
}
//---------------------------------------------------------------------------------------------------
TextureAtlas::~TextureAtlas()
{
}
//---------------------------------------------------------------------------------------------------
void TextureAtlas::NewFrame(float2 _vAtlasSize, uint32_t _uMaxElementCount)
{
	m_vCurrentViewportPosition = float2(0.f, 0.f);
	m_vAtlasSize = _vAtlasSize;
	m_vSingleTextureSize = m_vAtlasSize / ceilf(sqrtf(static_cast<float>(_uMaxElementCount)));
}
//---------------------------------------------------------------------------------------------------
TextureAtlas::AtlasState TextureAtlas::GetNextCoordinates(AtlasEntry& _vTextureCoordinates)
{
	if (m_vCurrentViewportPosition.x + m_vSingleTextureSize.x > m_vAtlasSize.x)
	{
		/// current x row is filled -> jump to next row
		if (m_vCurrentViewportPosition.y + m_vSingleTextureSize.y > m_vAtlasSize.y)
		{
			return AtlasState_NotEnoughSpace;
		}
		else
		{
			m_vCurrentViewportPosition.x = 0.0f;
			m_vCurrentViewportPosition.y += m_vSingleTextureSize.y;
		}
	}

	_vTextureCoordinates.vStartTexCoord.x = (m_vCurrentViewportPosition.x / m_vAtlasSize.x);
	_vTextureCoordinates.vStartTexCoord.y = (m_vCurrentViewportPosition.y / m_vAtlasSize.y);
	_vTextureCoordinates.vEndTexCoord.x = ((m_vCurrentViewportPosition.x + m_vSingleTextureSize.x) / m_vAtlasSize.x);
	_vTextureCoordinates.vEndTexCoord.y = ((m_vCurrentViewportPosition.y + m_vSingleTextureSize.y) / m_vAtlasSize.y);

	_vTextureCoordinates.vViewPortPosition = m_vCurrentViewportPosition;

	_vTextureCoordinates.vTextureSize = m_vSingleTextureSize;

	m_vCurrentViewportPosition.x += m_vSingleTextureSize.x;

	return AtlasState_Valid;
}
//---------------------------------------------------------------------------------------------------
