//   Copyright 2020 Fabian Wahlster
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef SHADERPERMUTATIONSEARCH_H
#define SHADERPERMUTATIONSEARCH_H

#include "ShaderPermutationDefines.h"

namespace Helix
{
	namespace Display
	{
		using TBestStagePermutations = std::vector<StagePermutation>;

		class ShaderPermutationSearch
		{
		public:
			ShaderPermutationSearch();
			~ShaderPermutationSearch();

			void AddStage(
				const std::vector<ShaderPermutationHash>& _Permutations,
				const ShaderPermutationWeights& _Weights,
				const ShaderType _kStage,
				const uint64_t _uDefaultPruneMask = 0u);

			const float& GetAvgPF() const;
			const float& GetMinPF() const;
			const float& GetMaxPF() const;
			const float& GetTotalPF() const;
			const size_t& GetPermutationCount() const;

			void Clear();

			void UpdateWeights(const ShaderPermutationWeights& _Weights, const ShaderType _kStage);

			TBestStagePermutations Find(float _fObjectPF, _Out_ float& _SelectedObjectPF, const StageConstraints& _ObjectConstraint = {});

			//double GetAndResetTime();

			void SetMaxSearchDepth(const uint32_t& _uSearchDepth);

		private:
			size_t m_uPermutationCount = 0u;
			float m_fAvgPF = 0.f;
			float m_fMinPF = 0.f;
			float m_fMaxPF = 0.f;
			float m_fTotalPF = 0.f;
			uint32_t m_uSearchDepth = HUNDEFINED32;

			// Order by Pipeline Stage: [0] = Vertex [1] = Hull ... (if added)
			std::vector<StageRatings> m_StageRatings;
		};
		
		inline const float& ShaderPermutationSearch::GetAvgPF() const{	return m_fAvgPF; }
		inline const float& ShaderPermutationSearch::GetMinPF() const { return m_fMinPF; }
		inline const float& ShaderPermutationSearch::GetMaxPF() const { return m_fMaxPF; }
		inline const float& ShaderPermutationSearch::GetTotalPF() const {return m_fTotalPF; }
		inline const size_t& ShaderPermutationSearch::GetPermutationCount() const{return m_uPermutationCount;}
		inline void ShaderPermutationSearch::SetMaxSearchDepth(const uint32_t& _uSearchDepth) { m_uSearchDepth = _uSearchDepth; }

		//---------------------------------------------------------------------------------------------------
		inline TBestStagePermutations ShaderPermutationSearch::Find(float _fObjectPF, _Out_ float& _SelectedObjectPF, const StageConstraints& _ObjectConstraint)
		{
			_SelectedObjectPF = 0.0;

			if (m_StageRatings.empty())
			{
				return TBestStagePermutations();
			}

			TBestStagePermutations Results;
			StageRatings& VertexRatings = m_StageRatings.front();

			HASSERT(VertexRatings.kShaderStage == ShaderType_VertexShader, "First Rating is not a VertexStage rating!");

			StageConstraints ObjectStageConstraints(_ObjectConstraint);

			// -> compute max PF needed to draw in ObjectTime
			// -> weight AvgPF of stages 
			// -> distribute weighted PFs over stages

			float fHeuristicPF = _fObjectPF * (VertexRatings.fAvgPF / m_fAvgPF);

			VertexRatings.SetMaxSearchDepth(m_uSearchDepth);
			ShaderPermutationStats Stats = VertexRatings.FindConstrained(fHeuristicPF,
				ObjectStageConstraints.GetConstraint(ShaderType_VertexShader, false),
				ObjectStageConstraints.GetConstraint(ShaderType_VertexShader, true)); // 'unconstrained' search

			_SelectedObjectPF += Stats.fPermutationFactor;
			uint32_t uIOConstraint = Stats.Permutation.uIOPermutation;

			Results.push_back({ Stats.Permutation, ShaderType_VertexShader });

			using TRit = std::vector<StageRatings>::iterator;

			for (TRit it = m_StageRatings.begin() + 1, end = m_StageRatings.end(); it != end; ++it)
			{
				fHeuristicPF = _fObjectPF * (it->fAvgPF / m_fAvgPF);

				// vertex shader selects IO permutation#
				if (uIOConstraint != 0u)
				{
					ObjectStageConstraints.AddConstraint(ShaderPermutationHash(0u, uIOConstraint), it->kShaderStage);
				}

				it->SetMaxSearchDepth(m_uSearchDepth);

				Stats = it->FindConstrained(fHeuristicPF,
					ObjectStageConstraints.GetConstraint(it->kShaderStage, false),
					ObjectStageConstraints.GetConstraint(it->kShaderStage, true)); // IO-Constrained search

				_SelectedObjectPF += Stats.fPermutationFactor;

				Results.push_back({ Stats.Permutation, it->kShaderStage });
			}

			return Results;
		}

	} // Display
} // Helix

#endif // !SHADERPERMUTATIONSEARCH_H
