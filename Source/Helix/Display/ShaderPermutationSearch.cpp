//   Copyright 2020 Fabian Wahlster
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "ShaderPermutationSearch.h"

using namespace Helix::Display;

//---------------------------------------------------------------------------------------------------
ShaderPermutationSearch::ShaderPermutationSearch()
{
}
//---------------------------------------------------------------------------------------------------

ShaderPermutationSearch::~ShaderPermutationSearch()
{
}
//---------------------------------------------------------------------------------------------------

void ShaderPermutationSearch::AddStage(
	const std::vector<ShaderPermutationHash>& _Permutations,
	const ShaderPermutationWeights& _Weights,
	const ShaderType _kStage,
	const uint64_t _uDefaultPruneMask)
{
	m_StageRatings.push_back(StageRatings(_Permutations, _Weights, _kStage, _uDefaultPruneMask));

	m_fMinPF += m_StageRatings.back().GetMinPF();
	m_fMaxPF += m_StageRatings.back().GetMaxPF();
	m_fAvgPF += m_StageRatings.back().GetAvgPF();
	m_fTotalPF += m_StageRatings.back().GetTotalPF();
	m_uPermutationCount += m_StageRatings.back().QualityRatings.size();
}

//---------------------------------------------------------------------------------------------------

void ShaderPermutationSearch::Clear()
{
	m_fAvgPF = 0.f;
	m_fMinPF = 0.f;
	m_fAvgPF = 0.f;
	m_uPermutationCount = 0u;

	m_StageRatings.resize(0);
}
//---------------------------------------------------------------------------------------------------

void ShaderPermutationSearch::UpdateWeights(const ShaderPermutationWeights& _Weights, const ShaderType _kStage)
{
	for (StageRatings& Ratings : m_StageRatings)
	{
		if (Ratings.kShaderStage == _kStage)
		{
			Ratings.Weights = _Weights;
			Ratings.EvaluateWeights();
			break;
		}
	}
}

//---------------------------------------------------------------------------------------------------