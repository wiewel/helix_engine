//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "TextureResizeEventManagerDX11.h"

using namespace Helix::Display;

//---------------------------------------------------------------------------------------------------
TextureResizeEventManagerDX11::TextureResizeEventManagerDX11()
{
}
//---------------------------------------------------------------------------------------------------
TextureResizeEventManagerDX11::~TextureResizeEventManagerDX11()
{
	if (m_ResizeEvents.empty() == false)
	{
		HERROR("Some textures have not been unregistered from the ResizeManager before destruction");
	}

	m_ResizeEvents.clear();
}
//---------------------------------------------------------------------------------------------------
void TextureResizeEventManagerDX11::Resize(const std::string& _sEventName, uint32_t _uWidth, uint32_t _uHeight, uint32_t _uDepth)
{
	std::hash<std::string> HashFunc;
	std::lock_guard<std::mutex> Lock(m_EraseMutex);

	auto Range = m_ResizeEvents.equal_range(HashFunc(_sEventName));
	for (TEventMap::iterator it = Range.first; it != Range.second; ++it)
	{
		it->second.pTexture->ResizeEvent(_uWidth, _uHeight, _uDepth);
	}
}
//---------------------------------------------------------------------------------------------------
void TextureResizeEventManagerDX11::Add(const std::string& _sEventName, TextureDX11* _pTexture)
{
	if (_pTexture->operator bool())
	{
		std::hash<std::string> HashFunc;
		std::lock_guard<std::mutex> Lock(m_EraseMutex);

		m_ResizeEvents.insert({ HashFunc(_sEventName), {_pTexture, _pTexture->GetKey()} });
	}
}
//---------------------------------------------------------------------------------------------------
void TextureResizeEventManagerDX11::Remove(const std::string& _sEventName, const uint64_t& _uTextureHash)
{
	std::lock_guard<std::mutex> Lock(m_EraseMutex);

	std::hash<std::string> HashFunc;
	auto Range = m_ResizeEvents.equal_range(HashFunc(_sEventName));
	for (TEventMap::iterator it = Range.first; it != Range.second; ++it)
	{
		if (it->second.uKey == _uTextureHash)
		{
			m_ResizeEvents.erase(it);
			break;
		}
	}
}
//---------------------------------------------------------------------------------------------------