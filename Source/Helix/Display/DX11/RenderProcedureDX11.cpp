//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "RenderProcedureDX11.h"
#include "RenderPassCreatorDX11.h"

using namespace Helix::Display;
using namespace Helix::Resources;
using namespace Helix::Scene;

//---------------------------------------------------------------------------------------------------
RenderProcedureDX11::RenderProcedureDX11(const std::string& _sName) :
	m_sName(_sName)
{
}
//---------------------------------------------------------------------------------------------------
RenderProcedureDX11::~RenderProcedureDX11()
{
}

//---------------------------------------------------------------------------------------------------

bool RenderProcedureDX11::DefaultInitialize()
{
	for (const std::shared_ptr<RenderPassDX11>& pRenderPass : m_RenderPasses)
	{
		if (pRenderPass->DefaultInitialize() == false)
		{
			return false;
		}
	}

	return true;
}
//---------------------------------------------------------------------------------------------------
void RenderProcedureDX11::AddPass(std::shared_ptr<RenderPassDX11> _pRenderPass)
{
	m_RenderPasses.push_back(_pRenderPass);
}
//---------------------------------------------------------------------------------------------------
RenderProcedureDesc RenderProcedureDX11::CreateDescription() const
{
	RenderProcedureDesc Desc = {};

	Desc.sProcedureName = m_sName;
	Desc.bEnabled = m_bEnabled;
	
	for (const std::shared_ptr<RenderPassDX11>& pRenderPass : m_RenderPasses)
	{
		Desc.Passes.push_back(pRenderPass->CreateDescription());
	}

	return Desc;
}
//---------------------------------------------------------------------------------------------------

bool RenderProcedureDX11::Initialize(const RenderProcedureDesc& _Desc)
{
	m_sName = _Desc.sProcedureName;
	m_bEnabled = _Desc.bEnabled;

	for (const RenderPassDesc& PassDesc : _Desc.Passes)
	{
		std::shared_ptr<RenderPassDX11> pRenderPass = RenderPassCreatorDX11::Instance()->CreatePass(PassDesc.sPassType, PassDesc.sPassInstanceName);

		if (pRenderPass != nullptr)
		{
			if (pRenderPass->Initialize(PassDesc))
			{
				m_RenderPasses.push_back(pRenderPass);
			}
			else
			{
				HERROR("Failed to initialize RenderProcedure %s", CSTR(_Desc.sProcedureName));
				m_RenderPasses.clear();
				return false;
			}
		}
	}

	return true;
}
//---------------------------------------------------------------------------------------------------
