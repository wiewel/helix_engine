//   Copyright 2020 Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef HELIXGLITCHRENDERPASSDX11_H
#define HELIXGLITCHRENDERPASSDX11_H

#include "RenderPassDX11.h"
#include "Math\MathFunctions.h"

#include "Display\Shaders\Glitch.hlsl"
#include "CommonConstantBufferFunctions.h"

namespace Helix
{
	namespace Display
	{
		class HelixGlitchRenderPassDX11 : public RenderPassDX11
		{
		public:
			HDEBUGNAME("HelixGlitchRenderPassDX11");

			HelixGlitchRenderPassDX11(DefaultInitializerType);
			HelixGlitchRenderPassDX11(const std::string& _sInstanceName = {});
			~HelixGlitchRenderPassDX11();

			bool OnInitalize(const hlx::TextToken& _CustomVariables) final;
			bool OnSelectDefault() final;

			bool OnPerFrame() final;

			std::shared_ptr<RenderPassDX11> CreatePass(const std::string& _sInstanceName) const final;

		private:
			ShaderTextureDX11 m_InputTexture = { "gInTex" };
			ShaderRenderTargetDX11 m_TargetTexture = { "vColor" };
		};
		//---------------------------------------------------------------------------------------------------
		inline bool HelixGlitchRenderPassDX11::OnSelectDefault()
		{
			bool bResult = true;

			uint32_t uInOutPermutation = m_bDebug ? Shaders::Glitch::HPMIO_Debug : 0;
			GetShader()->SetIOPermutation(uInOutPermutation);

			bResult &= Select(ShaderType_PixelShader, 0);
			bResult &= Select(ShaderType_VertexShader, 0);

			return bResult;
		}
		//---------------------------------------------------------------------------------------------------
		inline bool HelixGlitchRenderPassDX11::OnPerFrame()
		{
			bool bResult = true;

			uint32_t uInOutPermutation = m_bDebug ? Shaders::Glitch::HPMIO_Debug : 0;
			GetShader()->SetIOPermutation(uInOutPermutation);

			bResult &= Select(ShaderType_PixelShader, 0);
			bResult &= Select(ShaderType_VertexShader, 0);

			//HASSERT(m_TargetTexture.Copy(true), "Failed to copy Target!");
			//SetInputTexture(m_TargetTexture.GetCopy(), "gInTex", false);

			return bResult;
		}
	} // Display
} // Helix

#endif // HELIXGLITCHRENDERPASSDX11_H