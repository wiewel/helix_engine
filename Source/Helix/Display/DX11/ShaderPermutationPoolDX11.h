//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef SHADERPERMUTATIONPOOLDX11_H
#define SHADERPERMUTATIONPOOLDX11_H

#include <concurrent_unordered_map.h>
#include "hlx\src\Singleton.h"
#include "hlx\src\Profiler.h"
#include "ShaderDX11.h"
#include "RenderDeviceShutdownCallbackDX11.h"

namespace Helix
{
	// forward delcs
	namespace Resources
	{
		class HLXSLFile;
	}

	namespace Display
	{
		using TShaderMap = concurrency::concurrent_unordered_map<hlx::string, std::shared_ptr<ShaderDX11>>;

		// data CRC -> Reduced Interface
		using TInterfaceMap = concurrency::concurrent_unordered_map<uint32_t, ShaderInterfaceDesc>;

		class ShaderPermutationPoolDX11 : public hlx::Singleton<ShaderPermutationPoolDX11>, RenderDeviceShutdownCallbackDX11
		{
			friend class ShaderDX11;
		public:
			ShaderPermutationPoolDX11();
			~ShaderPermutationPoolDX11();

			std::shared_ptr<ShaderDX11> Load(const hlx::string& _sShaderName, bool _bReload = false);
			
			static bool UncompressShaderCode(const hlx::bytes& _InputData, hlx::bytes& _OutputData, size_t _uUncompressedSize, Resources::CodeCompression _kMethod);

			void OnShutdown() final;

		private:
			// load the shader from HLXSL file
			bool LoadHLXSL(const hlx::string& _sShaderName, _Out_ Resources::HLXSLFile& _ShaderFile);

			std::vector<BindInfoDesc> GetRenderTargetsFromOutputSignature(const std::vector<InputElementDesc>& _OutputSignature);
			
			void IncrementActiveShaders();

			void Clear();

		private:
			TShaderMap m_Shaders;

			// data hash -> inputlayout
			TInputlayoutMap m_InputLayouts;

			// crc -> intefrace
			TInterfaceMap m_Interfaces;

			//TVariableMap m_Variables;

			uint32_t m_uActiveShaders = 0u; // includes all stages
		};

		inline void ShaderPermutationPoolDX11::IncrementActiveShaders()
		{
			m_uActiveShaders++;
			HPROFILE_COUNT(ActiveShaders, m_uActiveShaders);
		}
	} // Display
} // Helix

#endif //SHADERPERMUTATIONPOOLDX11_H