//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "MaterialPoolDX11.h"

#include "MaterialDX11.h"
#include "Resources\HMATFile.h"
#include "Resources\FileSystem.h"

using namespace Helix::Display;

MaterialDX11 MaterialPoolDX11::m_ErrorMaterial = nullptr;
uint64_t MaterialPoolDX11::m_kDefaultRenderPasses = 0u;

MaterialPoolDX11::MaterialPoolDX11()
{
}
//---------------------------------------------------------------------------------------------------

MaterialPoolDX11::~MaterialPoolDX11()
{
}
//---------------------------------------------------------------------------------------------------

uint64_t MaterialPoolDX11::HashFunction(const MaterialDesc& _Desc)
{
	HASSERTD(_Desc.sFilePathOrName.empty() == false, "Invalid empty file path or name");

	return StrHash(Resources::FileSystem::GetShortName(STR(_Desc.sFilePathOrName)));
}

//---------------------------------------------------------------------------------------------------

MaterialDX11ReferenceInternal* MaterialPoolDX11::CreateFromDesc(const MaterialDesc& _Desc, const uint64_t& _Hash)
{
	std::string sAlbedoMap;
	std::string sNormalMap;
	std::string sMetallicMap;
	std::string sRoughnessMap;
	std::string sHeightMap;

	// create new one from desc
	if (_Desc.bLoadFromFile == false)
	{
		sAlbedoMap =_Desc.sAlbedoMap;
		sNormalMap = _Desc.sNormalMap;
		sMetallicMap = _Desc.sMetallicMap;
		sRoughnessMap = _Desc.sRoughnessMap;
		sHeightMap = _Desc.sHeightMap;
	}
	else // load from file
	{
		Resources::HMATFile HMat;
		{
			hlx::fbytestream FileStream;
			if (Resources::FileSystem::Instance()->OpenFileStream(Resources::HelixDirectories_Materials, STR(_Desc.sFilePathOrName), std::ios_base::in, FileStream) == false)
			{
				return nullptr;
			}
			// buffer context
			{
				hlx::bytes&& InputBuffer = FileStream.get<hlx::bytes>(FileStream.size());
				FileStream.close();

				hlx::bytestream InputStream(InputBuffer);
				HMat.Read(InputStream);
			}
		}

		const Resources::HMat& Mat = HMat.GetMaterial();

		sAlbedoMap = Mat.sMapAlbedo;
		sNormalMap = Mat.sMapNormal;
		sMetallicMap = Mat.sMapMetallic;
		sRoughnessMap = Mat.sMapRoughness;
		sHeightMap = Mat.sMapHeight;
	}

	MaterialDX11ReferenceInternal* pMaterialRef = new MaterialDX11ReferenceInternal;
	
	pMaterialRef->sPathOrName = hlx::to_sstring(Resources::FileSystem::GetShortName(STR(_Desc.sFilePathOrName)));

	// load texture maps
	if (_Desc.bLoadTextures)
	{
		if (sAlbedoMap.empty() == false)
		{
			pMaterialRef->AlbedoMap = ImmutableTextureDX11(sAlbedoMap);
			if (pMaterialRef->AlbedoMap.operator bool() == false)
			{
				HERROR("Failed to load diffuse texture for material %s", CSTR(_Desc.sFilePathOrName));
				pMaterialRef->AlbedoMap = nullptr;
			}
		}

		if (sNormalMap.empty() == false)
		{
			pMaterialRef->NormalMap = ImmutableTextureDX11(sNormalMap);
			if (pMaterialRef->NormalMap.operator bool() == false)
			{
				HERROR("Failed to load normal texture for material %s", CSTR(_Desc.sFilePathOrName));
				pMaterialRef->NormalMap = nullptr;
			}
		}

		if (sMetallicMap.empty() == false)
		{
			pMaterialRef->MetallicMap = ImmutableTextureDX11(sMetallicMap);
			if (pMaterialRef->MetallicMap.operator bool() == false)
			{
				HERROR("Failed to load metallic texture for material %s", CSTR(_Desc.sFilePathOrName));
				pMaterialRef->MetallicMap = nullptr;
			}
		}

		if (sRoughnessMap.empty() == false)
		{
			pMaterialRef->RoughnessMap = ImmutableTextureDX11(sRoughnessMap);
			if (pMaterialRef->RoughnessMap.operator bool() == false)
			{
				HERROR("Failed to load roughness texture for material %s", CSTR(_Desc.sFilePathOrName));
				pMaterialRef->RoughnessMap = nullptr;
			}
		}

		if (sHeightMap.empty() == false)
		{
			pMaterialRef->HeightMap = ImmutableTextureDX11(sHeightMap);
			if (pMaterialRef->HeightMap.operator bool() == false)
			{
				HERROR("Failed to load height texture for material %s", CSTR(_Desc.sFilePathOrName));
				pMaterialRef->HeightMap = nullptr;
			}
		}
	}

	return pMaterialRef;
}
//---------------------------------------------------------------------------------------------------

uint64_t MaterialPoolDX11::GetDefaultKey()
{
	if (m_ErrorMaterial == nullptr)
	{
		MaterialDesc Desc = {};
		Desc.sFilePathOrName = "HELiXERRORMATERIAL";
		Desc.bLoadFromFile = false;
		Desc.bLoadTextures = false;

		// creates reference
		m_ErrorMaterial = MaterialDX11(Desc);

	}

	return m_ErrorMaterial.GetKey();
}
//---------------------------------------------------------------------------------------------------
