//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "DebugRenderDX11.h"

#include "hlx\src\StandardDefines.h"
#include "Scene\CameraManager.h"
#include "CommonRenderPasses.h"
#include "MeshClusterDX11.h"
#include "Scene\CameraManager.h"
#include "Scene\CameraComponent.h"

using namespace Helix::Display;
using namespace Helix::Math;
using namespace Helix::Scene;

//---------------------------------------------------------------------------------------------------
DebugRenderDX11::DebugRenderDX11() :
	m_uByteOffset(GetVertexDeclarationSize(VertexLayout_PosXYZColor) * 2u)
{
	m_Entries.reserve(m_uMaxEntries);

	//Scene::RenderingSceneGuard Guard;
	CreateMesh();

	CameraComponent* pCamera = CameraManager::Instance()->GetCameraByName("MainCamera");
	if (pCamera != nullptr)
	{
		pCamera->AddRenderObject(&m_DebugLines);
	}
}
//---------------------------------------------------------------------------------------------------
DebugRenderDX11::~DebugRenderDX11()
{
	HASSERT(m_uFirstFree == 0u, "Some lines have not been freed correctly!");
}
//---------------------------------------------------------------------------------------------------
void DebugRenderDX11::CreateMesh()
{
	MaterialDX11 DebugMat;
	//DebugMat.EnableRenderPass(g_kDebugLinesPass);
	m_DebugLines.SetMaterial(DebugMat);
	m_DebugLines.SetName("HDebugLines");

	SubMesh LineMesh = {};
	LineMesh.m_sSubName = "HLineBuffer";
	LineMesh.m_kPrimitiveTopology = PrimitiveTopology_LineList;
	LineMesh.m_kVertexDeclaration = VertexLayout_PosXYZColor;

	LineMesh.m_uVertexCount = m_uMaxEntries * 2u;
	LineMesh.m_uVertexStrideSize = GetVertexDeclarationSize(LineMesh.m_kVertexDeclaration);
	LineMesh.m_uVertexByteOffset = 0u;

	LineMesh.m_uIndexElementSize = 0u;
	LineMesh.m_uIndexByteOffset = 0u;
	LineMesh.m_uIndexCount = 0u;

	MeshClusterDesc Desc;
	Desc.m_bLoadFromFile = false;
	Desc.m_bPooled = false;
	Desc.m_kCPUAccessFlag = CPUAccessFlag_Write;
	Desc.m_kUsageFlag = ResourceUsageFlag_Dynamic;
	Desc.m_sFilePathOrName = "HDebugLines";
	Desc.m_MeshCluster.push_back(LineMesh);

	MeshClusterDX11 DebugLinesMesh = MeshClusterDX11(Desc);
	DebugLinesMesh.RemoveSubMesh("HLineBuffer");

	m_DebugLines.SetMesh(DebugLinesMesh);
}
//---------------------------------------------------------------------------------------------------
SubMesh DebugRenderDX11::GetLineDesc(const EntryType& _Line)
{
	SubMesh Line = {};
	Line.m_sSubName = "HDebugLine_" + std::to_string(_Line.first);
	Line.m_kPrimitiveTopology = PrimitiveTopology_LineList;
	Line.m_kVertexDeclaration = VertexLayout_PosXYZColor;

	Line.m_uVertexCount = 2u;
	Line.m_uVertexStrideSize = GetVertexDeclarationSize(Line.m_kVertexDeclaration);
	Line.m_uVertexByteOffset = _Line.second.uByteOffset;

	Line.m_uIndexElementSize = 0u;
	Line.m_uIndexByteOffset = 0u;
	Line.m_uIndexCount = 0u;
	return Line;
}
//---------------------------------------------------------------------------------------------------
void DebugRenderDX11::Realloc()
{
	const uint32_t uCurrentEntries = static_cast<uint32_t>(m_Entries.size());
	if (uCurrentEntries == m_uMaxEntries)
	{
		/// Resize Housekeeping
		const uint32_t uSizeIncrement = (uCurrentEntries / 3u) + 1u;
		m_uMaxEntries = uCurrentEntries + uSizeIncrement;

		m_Entries.reserve(m_uMaxEntries);

		/// Refresh Mesh
		CreateMesh();

		MeshClusterDX11& MeshCluster = m_DebugLines.GetMesh();

		/// Add all old submeshes
		for (EntryType LineEntry : m_Entries)
		{
			SubMesh Mesh = GetLineDesc(LineEntry);
			MeshCluster.AddSubMesh(Mesh);
			MeshCluster.WriteVertexData(Mesh.m_sSubName, LineEntry.second.Vertices);
		}
	}
}

//---------------------------------------------------------------------------------------------------
uint32_t DebugRenderDX11::DrawLine(const float3& _vStart, const float3& _vEnd, const float3& _vColor)
{
	return DrawLineInternal(_vStart, _vEnd, _vColor, true);
}

//---------------------------------------------------------------------------------------------------
uint32_t DebugRenderDX11::DrawLineInternal(
	const float3& _vStart,
	const float3& _vEnd,
	const float3& _vColor,
	bool _bLockRendering)
{
	//if (_bLockRendering)
	//{
	//	RenderingScene::Instance()->Lock();
	//}

	Realloc();

	EntryType NewEntry;
	NewEntry.first = m_uFirstFree;
	NewEntry.second.uByteOffset = m_uFirstFree * m_uByteOffset;
	NewEntry.second.Vertices = { VertexPosXYZColor(_vStart, _vColor), VertexPosXYZColor(_vEnd, _vColor) };

	MeshClusterDX11& MeshCluster = m_DebugLines.GetMesh();

	{
		SubMesh Mesh = GetLineDesc(NewEntry);
		MeshCluster.AddSubMesh(Mesh);
		MeshCluster.WriteVertexData(Mesh.m_sSubName, NewEntry.second.Vertices);
	}

	if (m_Entries.empty() && m_DebugLines.GetMaterials().empty() == false)
	{
		// enable rendering 
		m_DebugLines.GetMaterials().front().EnableRenderPass(g_kDebugLinesPass);
	}

	m_Entries.push_back(NewEntry);

	//if (m_Entries.empty() == false)
	//{
	//	HASSERTD(m_DebugLines.GetMesh().IsContinuous(), "Not continous");
	//}

	uint32_t uRet = m_uFirstFree++;

	//if (_bLockRendering)
	//{
	//	RenderingScene::Instance()->Unlock();
	//}

	return uRet;
}

//---------------------------------------------------------------------------------------------------
DebugRenderDX11::LineIndices DebugRenderDX11::DrawBox(std::array<float3, 8>& _Corners, const float3& _vColor)
{
	//Scene::RenderingSceneGuard Guard;

	// near plane
	uint32_t uFirst = DrawLineInternal(_Corners[kFrustumCorner_NearUpperLeft], _Corners[kFrustumCorner_NearUpperRight], _vColor, false);
	DrawLineInternal(_Corners[kFrustumCorner_NearUpperRight], _Corners[kFrustumCorner_NearLowerRight], _vColor, false);
	DrawLineInternal(_Corners[kFrustumCorner_NearLowerRight], _Corners[kFrustumCorner_NearLowerLeft], _vColor, false);
	DrawLineInternal(_Corners[kFrustumCorner_NearLowerLeft], _Corners[kFrustumCorner_NearUpperLeft], _vColor, false);

	// far plane
	DrawLineInternal(_Corners[kFrustumCorner_FarUpperLeft],  _Corners[kFrustumCorner_FarUpperRight], _vColor, false);
	DrawLineInternal(_Corners[kFrustumCorner_FarUpperRight], _Corners[kFrustumCorner_FarLowerRight], _vColor, false);
	DrawLineInternal(_Corners[kFrustumCorner_FarLowerRight], _Corners[kFrustumCorner_FarLowerLeft], _vColor, false);
	DrawLineInternal(_Corners[kFrustumCorner_FarLowerLeft],  _Corners[kFrustumCorner_FarUpperLeft], _vColor, false);

	// interconnect
	DrawLineInternal(_Corners[kFrustumCorner_NearUpperLeft], _Corners[kFrustumCorner_FarUpperLeft], _vColor, false);
	DrawLineInternal(_Corners[kFrustumCorner_NearUpperRight], _Corners[kFrustumCorner_FarUpperRight], _vColor, false);
	DrawLineInternal(_Corners[kFrustumCorner_NearLowerRight], _Corners[kFrustumCorner_FarLowerRight], _vColor, false);
	uint32_t uLast = DrawLineInternal(_Corners[kFrustumCorner_NearLowerLeft], _Corners[kFrustumCorner_FarLowerLeft], _vColor, false);

	return LineIndices(uFirst, uLast);
}
//---------------------------------------------------------------------------------------------------
void DebugRenderDX11::RemoveDebugObject(LineIndices& _Indices)
{
	if (_Indices)
	{
		for (uint32_t i = _Indices.uLast; i >= _Indices.uFirst; --i)
		{
			RemoveDebugObject(i);

			if (i == 0u)
				break;
		}

		const_cast<uint32_t&>(_Indices.uFirst) = HUNDEFINED32;
		const_cast<uint32_t&>(_Indices.uLast) = HUNDEFINED32;
	}
}
//---------------------------------------------------------------------------------------------------
void DebugRenderDX11::RemoveDebugObject(const uint32_t& _uIdentifier)
{
	//Scene::RenderingSceneGuard Guard;

	bool bFound = false;
	EntryType FoundEntry;

	std::vector<EntryType>::iterator it = m_Entries.begin();
	uint32_t uSubIndex = 0u;

	for(; it < m_Entries.end(); ++it, ++uSubIndex)
	{
		if(it->first == _uIdentifier)
		{
			FoundEntry = *it;
			bFound = true;
			break;
		}
	}
	if (bFound == false)
		return;

	/// remove from GPU buffer
	MeshClusterDX11& MeshCluster = m_DebugLines.GetMesh();
	MeshCluster.RemoveSubMesh("HDebugLine_" + std::to_string(_uIdentifier));

	if (it == m_Entries.end()-1u)
	{
		m_Entries.pop_back();
	} 
	else if (m_Entries.size() > 1u)/// rearrange sub meshes to get continous mesh
	{		
		EntryType LastEntry = m_Entries.back();

		MeshCluster.RemoveSubMesh("HDebugLine_" + std::to_string(LastEntry.first));

		LastEntry.second.uByteOffset = FoundEntry.second.uByteOffset;

		SubMesh Line = GetLineDesc(LastEntry);
		MeshCluster.InsertSubMesh(Line, uSubIndex);

		MeshCluster.WriteVertexData(Line.m_sSubName, LastEntry.second.Vertices);

		/// remove old last entry
		m_Entries.pop_back();

		/// set found entry to last entry
		*it = LastEntry;
	}

	--m_uFirstFree;

	if (m_Entries.empty() && m_DebugLines.GetMaterials().empty() == false)
	{
		// disable rendering
		m_DebugLines.GetMaterials().front().DisableRenderPass(g_kDebugLinesPass);
	}

	//if (m_Entries.empty() == false)
	//{
	//	HASSERTD(m_DebugLines.GetMesh().IsContinuous(), "Not continous");
	//}
}
//---------------------------------------------------------------------------------------------------
void DebugRenderDX11::HideAll(bool _bHide)
{
	Scene::CameraComponent* pCamera = Scene::CameraManager::Instance()->GetCameraByName("MainCamera");
	if(pCamera != nullptr)
	{
		if(_bHide)
		{
			pCamera->ClearFlag(g_kDebugLinesPass);
		}
		else
		{
			pCamera->SetFlag(g_kDebugLinesPass);
		}
	}
}
//---------------------------------------------------------------------------------------------------
