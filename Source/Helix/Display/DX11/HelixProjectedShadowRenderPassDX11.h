//   Copyright 2020 Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef HELIXPROJECTEDSHADOWRENDERPASSDX11_H
#define HELIXPROJECTEDSHADOWRENDERPASSDX11_H

#include "RenderPassDX11.h"
#include "CommonConstantBufferFunctions.h"
#include "Scene/LightDefines.h"
#include "Scene/LightComponent.h"
#include "Scene/LightContext.h"
#include "Display/TextureAtlas.h"

namespace Helix
{
	namespace Display
	{
		class HelixProjectedShadowRenderPassDX11 : public RenderPassDX11
		{
		public:
			HDEBUGNAME("HelixProjectedShadowRenderPassDX11");

			HelixProjectedShadowRenderPassDX11(DefaultInitializerType);
			HelixProjectedShadowRenderPassDX11(const std::string& _sInstanceName = {});
			~HelixProjectedShadowRenderPassDX11();

			bool OnInitalize(const hlx::TextToken& _CustomVariables) final;

			bool OnPerFrame() final;
			bool OnPerCamera(const Scene::CameraComponent& _Camera) final;
			bool OnPerObject(const RenderObjectDX11& _RenderObject, const MaterialDX11& _Material) final;
			std::shared_ptr<RenderPassDX11> CreatePass(const std::string& _sInstanceName) const final;

			void SetLightType(Scene::LightType _kLightType);

		private:
			TCBPerCamera m_CBPerCamera = { "cbPerCamera" };
			TCBPerObject m_CBPerObject = { "cbPerObject" };

			Scene::LightType m_kLightType = Scene::LightType_Spot;
			uint32_t m_uMaxLightShadows = MAXSPOTLIGHTSHADOWS;

			std::vector<const Scene::LightComponent*> m_ShadowCaster;

			TextureAtlas ShadowMapAtlas;
		};

		//---------------------------------------------------------------------------------------------------
		inline void HelixProjectedShadowRenderPassDX11::SetLightType(Scene::LightType _kLightType)
		{
			switch (_kLightType)
			{
			case Scene::LightType_Spot:
				m_uMaxLightShadows = static_cast<uint32_t>(MAXSPOTLIGHTSHADOWS);
				break;
			default:
				HFATAL("Unsupported light type!");
			}

			m_kLightType = _kLightType;
		}
		//---------------------------------------------------------------------------------------------------
		inline bool HelixProjectedShadowRenderPassDX11::OnPerFrame()
		{
			Scene::CameraComponent* pCamera = Scene::CameraManager::Instance()->GetCameraByName("MainCamera");

			m_ShadowCaster.clear();
			Scene::LightContext::Instance()->GetShadowCastingLights(m_ShadowCaster, m_kLightType);
			Scene::LightContext::Instance()->SortLights(m_ShadowCaster, pCamera->GetPosition());

			/// cut shadow casting elements if there are more in the vector than available space on the atlas
			m_ShadowCaster.resize(m_uMaxLightShadows, nullptr);

			const DepthStencilTextureDX11& ShadowTarget = GetDepthStencilTexture();
			Math::float2 vShadowAtlasSize;
			vShadowAtlasSize.x = static_cast<float>(ShadowTarget.GetDescription().m_uWidth);
			vShadowAtlasSize.y = static_cast<float>(ShadowTarget.GetDescription().m_uHeight);

			ShadowMapAtlas.NewFrame(vShadowAtlasSize, m_uMaxLightShadows);

			return true;
		}
		//---------------------------------------------------------------------------------------------------
		inline bool HelixProjectedShadowRenderPassDX11::OnPerCamera(const Scene::CameraComponent& _Camera)
		{
			auto foundIt = std::find_if(m_ShadowCaster.begin(), m_ShadowCaster.end(), [&](const Scene::LightComponent*& pLight)
			{
				return pLight != nullptr && pLight->IsShadowCamera(&_Camera);
			});
			if (foundIt == m_ShadowCaster.end())
			{
				return false;
			}

			Scene::LightComponent* pLight = const_cast<Scene::LightComponent*>(*foundIt);
			if (pLight->ShadowCaster == false || pLight->Type != m_kLightType)
			{
				/// if type check is not true -> skip camera, wrong pass
				return false;
			}

			Scene::CameraComponent* pCamera = Scene::CameraManager::Instance()->GetCameraByName("MainCamera");
			if (pCamera != nullptr)
			{
				Scene::CullingFrustum Frustum = pCamera->GetCullingFrustum();

				bool bOverlaps = physx::PxGeometryQuery::overlap(
					Frustum.GetAABB(), Frustum.GetTransform(),
					Physics::GeoSphere(pLight->GetRange()*0.2f), pLight->GetParent()->GetTransform());
				if (bOverlaps == false)
				{
					return false;
				}
			}

			const Scene::CameraProperties& Cam = _Camera.GetRenderingProperties();
			SetCameraConstants(m_CBPerCamera, Cam);

			/// Create NDC [-1,1] -> Texture Space [0,1] transform
			Math::float4x4 T = {
				{ 0.5f,  0.0f, 0.0f, 0.0f },
				{ 0.0f, -0.5f, 0.0f, 0.0f },
				{ 0.0f,  0.0f, 1.0f, 0.0f },
				{ 0.5f,  0.5f, 0.0f, 1.0f }
			};

			// -> shadow transform S
			Math::float4x4 S = T * Cam.m_mViewProj;
			S = S.getTranspose();

			AtlasEntry CurrentTexCoords;
			if (ShadowMapAtlas.GetNextCoordinates(CurrentTexCoords) != TextureAtlas::AtlasState_Valid)
			{
				HFATAL("Too many lights!");
			}

			Scene::ShadowCasterProperties Caster;
			Caster.mShadowTransform = S;// m_CBPerCamera->mView;
			Caster.vShadowMapStartTexCoord = CurrentTexCoords.vStartTexCoord;
			Caster.vShadowMapEndTexCoord = CurrentTexCoords.vEndTexCoord;
			Caster.fLightCameraNearDist = Cam.m_fNearDistance;
			Caster.fLightCameraFarDist = Cam.m_fFarDistance;
			pLight->SetShadowProperties(Caster);

			/// change viewport
			DepthStencilTextureDX11& ShadowTarget = GetDepthStencilTexture();
			Viewport VPort = ShadowTarget.GetViewport();
			VPort.fWidth = CurrentTexCoords.vTextureSize.x;
			VPort.fHeight = CurrentTexCoords.vTextureSize.y;
			VPort.fTopLeftX = CurrentTexCoords.vViewPortPosition.x;
			VPort.fTopLeftY = CurrentTexCoords.vViewPortPosition.y;
			ShadowTarget.SetViewport(VPort);

			UpdateViewports(ShadowTarget);

			return true;
		}
		//---------------------------------------------------------------------------------------------------
		inline bool HelixProjectedShadowRenderPassDX11::OnPerObject(const RenderObjectDX11& _RenderObject, const MaterialDX11& _Material)
		{
			SetObjectConstants(m_CBPerObject, _RenderObject); // Dont set MaterialPoperties

			return true;
		}
		//---------------------------------------------------------------------------------------------------

	} // Display
} // Helix

#endif //HELIXPROJECTEDSHADOWRENDERPASSDX11_H
