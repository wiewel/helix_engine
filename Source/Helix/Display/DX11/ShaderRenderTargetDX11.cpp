//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "ShaderRenderTargetDX11.h"
#include "ShaderInterfaceDX11.h"
#include "TextureCacheDX11.h"

using namespace Helix::Display;
//---------------------------------------------------------------------------------------------------

ShaderRenderTargetDX11::ShaderRenderTargetDX11(const std::string& _sRenderTargetName, const std::string& _sMappedName, const std::string& _sMappedCopyName) :
	m_sName(_sRenderTargetName), m_sMappedName(_sMappedName), m_sMappedCopyName(_sMappedCopyName)
{
}
//---------------------------------------------------------------------------------------------------

ShaderRenderTargetDX11::~ShaderRenderTargetDX11()
{
	if (m_pInterface != nullptr)
	{
		m_pInterface->UnregisterRenderTarget(m_sName, m_sShaderInstanceName);
	}
}
//---------------------------------------------------------------------------------------------------

bool ShaderRenderTargetDX11::Initialize(ShaderInterfaceDX11* _pInterface, const std::string& _sShaderInstanceName)
{
	if (m_pInterface == nullptr)
	{
		m_pInterface = _pInterface;
		m_sShaderInstanceName = _sShaderInstanceName;
		return m_pInterface->RegisterRenderTarget(this, m_sShaderInstanceName, m_uSlot);
	}

	HERROR("Texture %s has already been initialized!", CSTR(m_sName));
	return false;
}
//---------------------------------------------------------------------------------------------------
void ShaderRenderTargetDX11::SetTexture(const RenderTargetTextureDX11& _Texture)
{
	if (_Texture)
	{
		HASSERT(_Texture.GetRenderTargetView() != nullptr, "%s has no RTV but is assigned to shader rentertarget texture %s",
			CSTR(_Texture.GetDescription().m_sFilePathOrName), CSTR(m_sName));
	}

	m_RenderTargetTexture = _Texture;
}
//---------------------------------------------------------------------------------------------------
bool ShaderRenderTargetDX11::Copy(bool _bShaderResourceOnly)
{
	if (m_RenderTargetTexture)
	{
		if (m_RenderTargetCopy.operator bool() == false)
		{
			TextureDesc Desc = m_RenderTargetTexture.GetDescription();
			Desc.m_kUsageFlag = ResourceUsageFlag_Default;
			
			if (_bShaderResourceOnly)
			{
				Desc.m_kBindFlag = ResourceBindFlag_ShaderResource;
			}

			// get new default texture from resource pool
			m_RenderTargetCopy = TextureCacheDX11::Instance()->Get<DefaultTextureDX11>(Desc);
		}

		if (m_RenderTargetCopy)
		{
			if(m_RenderTargetCopy.CopyPixelData(m_RenderTargetTexture) == false)
			{
				/// Copying texture failed
				TextureDesc TargetDesc = m_RenderTargetTexture.GetDescription();
				TextureDesc CopyDesc = m_RenderTargetCopy.GetDescription();
				if (TargetDesc.m_uWidth != CopyDesc.m_uWidth
					|| TargetDesc.m_uHeight != CopyDesc.m_uHeight
					|| TargetDesc.m_uDepth != CopyDesc.m_uDepth)
				{
					m_RenderTargetCopy.Resize(TargetDesc.m_uWidth, TargetDesc.m_uHeight, TargetDesc.m_uDepth);
					return m_RenderTargetCopy.CopyPixelData(m_RenderTargetTexture);
				}
				return false;
			}
			return true;
		}
	}	

	HERROR("Failed to copy rendertarget %s to %s", CSTR(m_sName), CSTR(m_sMappedCopyName));

	return false;
}
//---------------------------------------------------------------------------------------------------

