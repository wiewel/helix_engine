//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "GPUBufferInstanceDX11.h"

using namespace Helix;
using namespace Helix::Display;
//---------------------------------------------------------------------------------------------------

GPUBufferInstanceDX11::GPUBufferInstanceDX11(const ShaderBufferDesc& _Desc, bool _bWriteable) :
#ifdef HNUCLEO
	m_SyncGroup(_Desc.BindInfo.sName),
#endif
	m_Description(_Desc),
	m_bWriteable(_bWriteable)
{
}
//---------------------------------------------------------------------------------------------------

GPUBufferInstanceDX11::~GPUBufferInstanceDX11()
{
	Clear();
}

//---------------------------------------------------------------------------------------------------
void GPUBufferInstanceDX11::Clear()
{
#ifdef HNUCLEO
	m_SyncGroup.ClearVariables();
	HSAFE_VECTOR_DELETE(m_ShaderVars);
	m_ShaderVars.clear();
#endif

	HSAFE_DELETE(m_pBuffer);
}
//---------------------------------------------------------------------------------------------------

#ifdef HNUCLEO
void GPUBufferInstanceDX11::AddVariables(const std::vector<ShaderVariableDesc>& _Vars)
{
	for (const ShaderVariableDesc& Var : _Vars)
	{
		if (SyncShaderVariable::GetVarType(Var) != Datastructures::VarType_Unknown)
		{
			SyncShaderVariable* pVar = new SyncShaderVariable(Var, *this);
			m_ShaderVars.push_back(pVar);
			m_SyncGroup.AddVariable(pVar);
		}

		if (Var.Children.empty() == false)
		{
			AddVariables(Var.Children);
		}
	}
}
#endif
//---------------------------------------------------------------------------------------------------

bool GPUBufferInstanceDX11::Initialize(
	uint32_t _uElementCount,
	const uint8_t* _pInitialData,
	const uint32_t& _uInitialSize)
{
	Clear();
	
	m_uElementCount = _uElementCount;

	BufferDesc Desc = {};

	Desc.uSize = m_Description.BindInfo.uSize;
	Desc.uStructureByteStride = m_Description.GetStructureSize();

	if (Desc.uSize == 0u)
	{
		HERROR("Zero sized buffer detected: %s", CSTR(m_Description.BindInfo.sName));
		return false;
	}

	// structured buffers are tighly packed, not 16byte alligned
	if (Desc.uStructureByteStride != 0u && Desc.uStructureByteStride != Desc.uSize)
	{
		HERROR("StructureSize mismatch for buffer %s", CSTR(m_Description.BindInfo.sName));
		return false;
	}

	// constant buffers
	if (m_Description.kType == ShaderBufferType_ConstantBuffer)
	{
		Desc.kBindFlag = ResourceBindFlag_ConstantBuffer;
	}
	else
	{
		// all other buffers can be used as shader resource
		Desc.kBindFlag |= ResourceBindFlag_ShaderResource; // optional, additional
	}

	// unordered access (rw & structured)
	if (m_Description.kType == ShaderBufferType_UAV_RWTyped ||
		m_Description.kType == ShaderBufferType_UAV_RWStructured ||
		m_Description.kType == ShaderBufferType_UAV_Append_Structured ||
		m_Description.kType == ShaderBufferType_UAV_Consume_Structured || 
		m_Description.kType == ShaderBufferType_UAV_RWStructured_With_Counter)
	{
		Desc.kBindFlag |= ResourceBindFlag_UnorderedAccess;
	}

	// structured 
	if (m_Description.kType == ShaderBufferType_Structured ||
		m_Description.kType == ShaderBufferType_UAV_RWStructured ||
		m_Description.kType == ShaderBufferType_UAV_Append_Structured ||
		m_Description.kType == ShaderBufferType_UAV_Consume_Structured ||
		m_Description.kType == ShaderBufferType_UAV_RWStructured_With_Counter)
	{
		Desc.uSize *= _uElementCount;
		Desc.kResourceFlag |= MiscResourceFlag_Buffer_Structured;
	}

	// byteaddress buffers
	if (m_Description.kType == ShaderBufferType_UAV_RWByteAddress ||
		m_Description.kType == ShaderBufferType_ByteAddress)
	{
		Desc.kResourceFlag |= MiscResourceFlag_Buffer_AllowRawViews;
	}

	if (Desc.kResourceFlag ^ MiscResourceFlag_Buffer_Structured && _uElementCount > 1u)
	{
		HERROR("Buffer %s is not a StructuredBuffer but ElementCount greater 1 was supplied", CSTR(m_Description.BindInfo.sName));
		return false;
	}

	// append & consume
	switch (m_Description.kType)
	{
	case ShaderBufferType_UAV_Consume_Structured:
	case ShaderBufferType_UAV_Append_Structured:
		Desc.kUAVFlag = BufferUAVFlag_Append;
		break;
	case ShaderBufferType_UAV_RWStructured_With_Counter:
		Desc.kUAVFlag = BufferUAVFlag_Counter;
		break;
	default:
		break;
	}

	if (m_bWriteable)
	{
		Desc.kCPUAccessFlag = CPUAccessFlag_Write;
		Desc.kUsageFlag = ResourceUsageFlag_Dynamic;
		m_Data.resize(Desc.uSize);
	}
	else
	{
		Desc.kUsageFlag = ResourceUsageFlag_Default;
		Desc.kCPUAccessFlag = CPUAccessFlag_None;
		m_Data.resize(0u);
	}

	Desc.SubresourceData.pData = _pInitialData;
	Desc.SubresourceData.uRowPitch = std::min(_uInitialSize, Desc.uSize);

	// copy initial data to cpu shadow copy
	if (_pInitialData != nullptr && m_bWriteable)
	{
		memcpy_s(m_Data.data(), m_Data.size(), _pInitialData, Desc.SubresourceData.uRowPitch);
	}

	if (_uInitialSize > Desc.uSize)
	{
		HWARNING("Initial data size exceeds size of buffer %s [%u byte]", CSTR(m_Description.BindInfo.sName), Desc.uSize);
	}

	m_pBuffer = new GPUBufferDX11(m_Description.BindInfo.sName);
	if (m_pBuffer->Initialize(Desc) == false)
	{
		HSAFE_DELETE(m_pBuffer);
		return false;
	}

#ifdef HNUCLEO
	if (m_Description.kType == ShaderBufferType_ConstantBuffer)
	{
		AddVariables(m_Description.Variables);
	}
#endif

	return true;
}
//---------------------------------------------------------------------------------------------------

bool GPUBufferInstanceDX11::Resize(uint32_t _uElementCount)
{
	if (operator bool())
	{
		return m_uElementCount == _uElementCount || Initialize(_uElementCount);
	}
	else
	{
		HERRORD("Uninitialized Buffer %s can not be resized!", CSTR(m_Description.BindInfo.sName));
		return false;
	}
}
//---------------------------------------------------------------------------------------------------

bool GPUBufferInstanceDX11::Upload()
{
	if (operator bool() == false || m_Data.size() != m_Description.BindInfo.uSize)
	{
		HERROR("Uninitialized Buffer %s can not be uploaded!", CSTR(m_Description.BindInfo.sName));
		return false;
	}

	if (m_bChanged)
	{
		m_bChanged = false;
		return m_pBuffer->Apply(m_Data.data(), m_Description.BindInfo.uSize);
	}

	return true;
}
//---------------------------------------------------------------------------------------------------
Datastructures::SyncGroup* GPUBufferInstanceDX11::GetSyncGroup()
{
#ifdef HNUCLEO
	return &m_SyncGroup;
#else
	return nullptr;
#endif
}
//---------------------------------------------------------------------------------------------------
