//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef SHADERDX11_H
#define SHADERDX11_H

#include "ShaderInterfaceDX11.h"
#include "Resources\HLXSLFile.h"

namespace Helix
{
	// forward declarations
	namespace Resources
	{
		struct PermutationBufferRef;
	}

	namespace Display
	{
		template <class ShaderClass>
		struct ShaderCodeAssoc
		{
			ShaderCodeAssoc() {}
			ShaderCodeAssoc(const uint32_t& _uCRC32, ShaderClass* _pShader) :
				uCRC32(_uCRC32), pShader(_pShader){}
			uint32_t uCRC32 = 0u; // Code CRC32
			ShaderClass* pShader = nullptr;
		};

		// range permutation hash -> d3d shader
		template <class T>
		using TShaderRangeMap = concurrency::concurrent_unordered_map<uint64_t, ShaderCodeAssoc<T>>;

		// base permutation hash -> map of range shaders
		template <class T>
		using TShaderClassMap = concurrency::concurrent_unordered_map<uint64_t, TShaderRangeMap<T>>;

		// hash / permutation -> InputLayout
		using TInputlayoutMap = concurrency::concurrent_unordered_map<uint64_t, VertexLayoutDX11>;

		struct CompressedShaderCode
		{
			CompressedShaderCode(const hlx::bytes& _Code, const uint32_t& _uUncompressedSize) :
				Code(_Code), uUncompressedSize(_uUncompressedSize){}
			CompressedShaderCode() : uUncompressedSize(0u) {}

			hlx::bytes Code;
			uint32_t uUncompressedSize;
		};

		// crc32 of code -> compressed shader code
		using TCodeMap = concurrency::concurrent_unordered_map<uint32_t, CompressedShaderCode>;
		using TAvailablePermutations = std::vector<ShaderPermutationHash>;
		
		// Permuation -> selection count
		using TPermutationSelectionStats = std::unordered_map<ShaderPermutationHash, uint32_t, ShaderPermutationHash::HashFunctor>;

		class ShaderDX11
		{
			friend class RenderPassDX11;
		public:
			HDEBUGNAME("ShaderDX11");

			ShaderDX11(const hlx::string& _sName);
			~ShaderDX11();

			bool AddPermutation(
				const hlx::bytes& _Code,
				const uint32_t& _uUncompressedCodeSize,
				const uint32_t& _uCodeCRC32,
				const ShaderType& _kShaderStage,
				const ShaderPermutationHash& _PermHash,
				const ShaderInterfaceDesc* _pInterface,
				const VertexLayoutDX11& _VertexLayout);

			// Set IO permutation before selecting any shader class permutation to ensure all class permutation use the same IO permutation
			// and therefore the same input and output layouts across all shaders tages
			void SetIOPermutation(uint32_t _uIOPermutation);

			// thif function selects the best match for the given class permutation with respect to the IO permutation and returns the found permutation in _Permutation
			bool Select(const ShaderType& _kShaderStage,
				const uint32_t& _uClassPermutation,
				const std::vector<uint8_t>& _RangeIndices = {});

			bool ReSelectInterface();

			// this function clears the interface, forcing buffers to be recreated on next load
			// selected shaders and inputlayout are reset, d3d shader pools are destroyed
			void Clear();

			void SetPermutaitonsMasks(const Resources::TPermutationMasks& _PermutationMasks);
			void SetFunctions(const std::vector<ShaderFunction>& _Functions);

			ShaderInterfaceDX11* GetInterface();

			const VertexLayoutDX11& GetInputLayout() const;

			ShaderPtrDX11 GetShader(const ShaderType& _kShaderType) const;

			const std::vector<Display::ShaderFunction>& GetFunctions() const;

			const hlx::string& GetName() const;

			const TAvailablePermutations& GetAvailablePermutations(const ShaderType& _kShaderType) const;

			// filters out invalid permutations based on masks
			ShaderPermutationHash ReducePermutation(const ShaderPermutationHash& _Permutation, const ShaderType& _kShaderStage) const;

			void SetCodeCompression(Resources::CodeCompression _kUncompressMethod);

			const ShaderPermutationHash& GetSelectedPermutation(ShaderType _kStage);

			ShaderPermutationHash GetMostSelectedPermutation(ShaderType _kStage, _Out_ uint32_t& _uSelectCount) const;

			const TPermutationSelectionStats GetSelectedPermutations(ShaderType _kStage) const;

			bool HasComputeShader() const;

			// enables / disables execution of a shader stage
			void EnableStage(const ShaderType _kStage, bool _bEnable = true);

			void ReEnableStages();

		private:
			const hlx::string m_sName;

			bool m_bHasComputeShader = false;

			Resources::CodeCompression m_kCodeCompression = Resources::CodeCompression_LZMA;

			Resources::TPermutationMasks m_PermutationMasks;
			TAvailablePermutations m_AvailablePermutations[ShaderType_NumOf];

			// TODO: #ifdef PROFILE
			TPermutationSelectionStats m_PermutationSelectionStats[ShaderType_NumOf];

			std::vector<ShaderFunction> m_Functions;
			ShaderInterfaceDX11 m_Interface;
			TCodeMap m_CodeMap;

			uint32_t m_uSelectedIOPermutation = 0u;
			ShaderPermutationHash m_SelectedPermutations[ShaderType_NumOf] = {};

			VertexLayoutDX11 m_SelectedInputLayout = {};
			ShaderPtrDX11 m_SelectedShaders[ShaderType_NumOf] = {};
			bool m_ActiveStages[ShaderType_NumOf] = {};

			// permutation -> inputlayout
			TInputlayoutMap m_InputLayouts;

			TShaderClassMap<ID3D11VertexShader> m_VertexShaderPool;
			TShaderClassMap<ID3D11HullShader> m_HullShaderPool;
			TShaderClassMap<ID3D11DomainShader> m_DomainShaderPool;
			TShaderClassMap<ID3D11GeometryShader> m_GeometryShaderPool;
			TShaderClassMap<ID3D11PixelShader> m_PixelShaderPool;
			TShaderClassMap<ID3D11ComputeShader> m_ComputeShaderPool;
		};

		inline ShaderInterfaceDX11* ShaderDX11::GetInterface() { return &m_Interface;	}
		inline const VertexLayoutDX11& ShaderDX11::GetInputLayout() const {return m_SelectedInputLayout;}

		inline ShaderPtrDX11 ShaderDX11::GetShader(const ShaderType& _kShaderType) const
		{
			return m_ActiveStages[_kShaderType] ? m_SelectedShaders[_kShaderType] : nullptr;
		}

		inline const std::vector<Display::ShaderFunction>& ShaderDX11::GetFunctions() const	{return m_Functions;	}
		inline void ShaderDX11::SetFunctions(const std::vector<ShaderFunction>& _Functions)	{m_Functions = _Functions;}

		inline void ShaderDX11::SetPermutaitonsMasks(const Resources::TPermutationMasks& _PermutationMasks) {m_PermutationMasks = _PermutationMasks;}

		inline const hlx::string& ShaderDX11::GetName() const { return m_sName; }

		inline const TAvailablePermutations& ShaderDX11::GetAvailablePermutations(const ShaderType & _kShaderType) const
		{
			return m_AvailablePermutations[_kShaderType];
		}

		inline void ShaderDX11::SetIOPermutation(uint32_t _uIOPermutation) { m_uSelectedIOPermutation = _uIOPermutation; }

		inline void ShaderDX11::SetCodeCompression(Resources::CodeCompression _kUncompressMethod){m_kCodeCompression = _kUncompressMethod;}
		inline const ShaderPermutationHash& ShaderDX11::GetSelectedPermutation(ShaderType _kStage)	{return m_SelectedPermutations[_kStage];}
		inline const TPermutationSelectionStats Helix::Display::ShaderDX11::GetSelectedPermutations(ShaderType _kStage) const{return m_PermutationSelectionStats[_kStage];}
		inline bool ShaderDX11::HasComputeShader() const {return m_bHasComputeShader; }
		inline void ShaderDX11::EnableStage(const ShaderType _kStage, bool _bEnable){m_ActiveStages[_kStage] = _bEnable;}
	} // Display
} // Helix

#endif //ShaderDX11_H