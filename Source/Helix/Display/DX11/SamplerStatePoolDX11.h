//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef SAMPLERSTATEPOOLDX11_H
#define SAMPLERSTATEPOOLDX11_H

#include "RenderDeviceDX11.h"
#include "DataStructures\TplHashPool.h"
#include "hlx\src\Singleton.h"

namespace Helix
{
	namespace Display
	{
		// TODO: add mirror
		enum DefaultSamplerState
		{
			DefaultSamplerState_PointClamp = 0,
			DefaultSamplerState_PointWrap,
			DefaultSamplerState_LinearClamp,
			DefaultSamplerState_LinearWrap,
			DefaultSamplerState_AnisotropicClamp,
			DefaultSamplerState_AnisotropicWrap,
			DefaultSamplerState_ShadowMap,

			DefaultSamplerState_NumOf
		};

		struct DefaultSamplerStateDesc
		{
			DefaultSamplerState kState;
			const char* pName;
		};

		static DefaultSamplerStateDesc DefaultSamplerStates[DefaultSamplerState_NumOf] =
		{
			{ DefaultSamplerState_PointClamp, "SamplerPointClamp"},
			{ DefaultSamplerState_PointWrap, "SamplerPointWrap" },
			{ DefaultSamplerState_LinearClamp, "SamplerLinearClamp" },
			{ DefaultSamplerState_LinearWrap, "SamplerLinearWrap" },
			{ DefaultSamplerState_AnisotropicClamp, "SamplerAnisotropicClamp" },
			{ DefaultSamplerState_AnisotropicWrap, "SamplerAnisotropicWrap" },
			{ DefaultSamplerState_ShadowMap, "SamplerShadowMap" },
		};

		using TDX11SamplerStatePool = Datastructures::TplRefHashPool<uint64_t, D3D11_SAMPLER_DESC, ID3D11SamplerState>;
		class SamplerStatePoolDX11 : public TDX11SamplerStatePool, public hlx::Singleton<SamplerStatePoolDX11>
		{
		public:
			SamplerStatePoolDX11();
			~SamplerStatePoolDX11();
			
			uint64_t HashFunction(const D3D11_SAMPLER_DESC& _Desc);

			D3D11_SAMPLER_DESC GetDefaultDesc(DefaultSamplerState kDefaultState); // new

			void SetMaxAnisotropy(uint32_t _uMaxAnisotropy);
		private:

			ID3D11SamplerState* CreateFromDesc(const D3D11_SAMPLER_DESC& _Desc, const uint64_t& _Hash);

		private:
			RenderDeviceDX11* m_pRenderDevice = nullptr;

			uint32_t m_uMaxAnisotropy = 8;
		};

		inline void SamplerStatePoolDX11::SetMaxAnisotropy(uint32_t _uMaxAnisotropy) { m_uMaxAnisotropy = _uMaxAnisotropy; }

		using TDX11SamplerStateRef = Datastructures::TplHashPoolReference<SamplerStatePoolDX11>;
	} // Display
} // Helix

#endif // SAMPLERSTATEPOOL_H