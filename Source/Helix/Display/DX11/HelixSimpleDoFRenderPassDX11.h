//   Copyright 2020 Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef HELIXSIMPLEDOFRENDERPASSDX11_H
#define HELIXSIMPLEDOFRENDERPASSDX11_H

#include "RenderPassDX11.h"
#include "Math\MathFunctions.h"

#include "Display\Shaders\SimpleDoF.hlsl"
#include "CommonConstantBufferFunctions.h"

namespace Helix
{
	namespace Display
	{
		class HelixSimpleDoFRenderPassDX11 : public RenderPassDX11
		{
		public:
			HDEBUGNAME("HelixSimpleDoFRenderPassDX11");

			HelixSimpleDoFRenderPassDX11(DefaultInitializerType);
			HelixSimpleDoFRenderPassDX11(const std::string& _sInstanceName = {});
			~HelixSimpleDoFRenderPassDX11();

			bool OnInitalize(const hlx::TextToken& _CustomVariables) final;
			bool OnSelectDefault() final;

			bool OnPerFrame() final;
			bool OnPerCamera(const Scene::CameraComponent& _Camera) final;

			float GetFarBlurDepth() const;
			void SetFarBlurDepth(float _fFarBlurDepth);
			float GetNearBlurDepth() const;
			void SetNearBlurDepth(float _fNearBlurDepth);

			std::shared_ptr<RenderPassDX11> CreatePass(const std::string& _sInstanceName) const final;

		private:
			ShaderVariableDX11<Shaders::SimpleDoF::cbSimpleDoF> m_CBSimpleDoF = { "cbSimpleDoF" };

			TCBPerCamera m_CBPerCamera = { "cbPerCamera" };

			ShaderTextureDX11 m_SceneBlurredTexture = { "gSceneBlurredTex" };
			ShaderTextureDX11 m_DepthTexture = { "gDepthTex", "LINEAR_DEPTH" };
			ShaderTextureDX11 m_SceneTexture = { "gSceneTex" };

			ShaderRenderTargetDX11 m_DoFTarget = { "vColor" };

			float m_fFarBlurDepth = 15.0f;
			float m_fNearBlurDepth = 15.0f;
		};
		//---------------------------------------------------------------------------------------------------
		inline bool HelixSimpleDoFRenderPassDX11::OnSelectDefault()
		{
			bool bResult = true;

			uint32_t uInOutPermutation = m_bDebug ? Shaders::SimpleDoF::HPMIO_Debug : 0;
			GetShader()->SetIOPermutation(uInOutPermutation);

			bResult &= Select(ShaderType_PixelShader, 0);
			bResult &= Select(ShaderType_VertexShader, 0);

			return bResult;
		}
		//---------------------------------------------------------------------------------------------------
		inline bool HelixSimpleDoFRenderPassDX11::OnPerFrame()
		{
			bool bResult = true;

			uint32_t uInOutPermutation = m_bDebug ? Shaders::SimpleDoF::HPMIO_Debug : 0;
			GetShader()->SetIOPermutation(uInOutPermutation);

			bResult &= Select(ShaderType_PixelShader, 0);
			bResult &= Select(ShaderType_VertexShader, 0);

			m_CBSimpleDoF->fNearBlurDepth = m_fNearBlurDepth;
			m_CBSimpleDoF->fFarBlurDepth = m_fFarBlurDepth;

			HASSERT(m_DoFTarget.Copy(true), "Failed to copy DoF Target!");
			SetInputTexture(m_DoFTarget.GetCopy(), "gSceneTex", false);

			return bResult;
		}

		//---------------------------------------------------------------------------------------------------
		inline float HelixSimpleDoFRenderPassDX11::GetFarBlurDepth() const { return m_fFarBlurDepth; }
		inline void HelixSimpleDoFRenderPassDX11::SetFarBlurDepth(float _fFarBlurDepth) { m_fFarBlurDepth = _fFarBlurDepth; }
		//---------------------------------------------------------------------------------------------------
		inline float HelixSimpleDoFRenderPassDX11::GetNearBlurDepth() const { return m_fNearBlurDepth; }
		inline void HelixSimpleDoFRenderPassDX11::SetNearBlurDepth(float _fNearBlurDepth) { m_fNearBlurDepth = _fNearBlurDepth; }
		//---------------------------------------------------------------------------------------------------

	} // Display
} // Helix

#endif // HELIXSIMPLEDOFRENDERPASSDX11_H