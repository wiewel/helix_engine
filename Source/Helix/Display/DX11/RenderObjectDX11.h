//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef RENDEROBJECTDX11_H
#define RENDEROBJECTDX11_H

#include <memory>
#include "MeshClusterDX11.h"
#include "MaterialDX11.h"
#include "Math\MathTypes.h"
#include "DataStructures\SyncVariable.h"
#include "hlx\src\StandardDefines.h"
#include "DataStructures\BaseObject.h"
#include "Scene\RenderingSceneGuard.h"

namespace Helix
{
	// forward decl
	namespace Scene
	{
		class LightContext;
	};

	namespace Datastructures
	{
		struct GameObjectDesc;
		class GameObject;
	}

	namespace Display
	{
		using namespace DirectX;

		class RenderObjectDX11 : public Datastructures::IBaseObject
		{
			friend class Scene::LightContext;
			friend class Datastructures::GameObject;
		public:
			RenderObjectDX11(Datastructures::IBaseObject* _pParent = nullptr);
			virtual ~RenderObjectDX11();

			virtual void Uninitialize() override;

			// check if any assigned material uses _kPass, returns false if no material is assinged
			bool CheckRenderPass(uint64_t _kPass) const;

			virtual void GetObjectToWorldMatrix(_Out_ Math::float4x4& _mObjectToWorld) const;
			virtual void GetObjectToWorldMatrix(_Out_ Math::float4x4& _mObjectToWorld, _Out_ Math::float4x4& _mNormal) const;

			virtual void Initialize(const Datastructures::GameObjectDesc& _Desc);
			virtual Datastructures::GameObjectDesc CreateDescription() const;

			// Getter/Setter
			const MeshClusterDX11& GetMesh() const;
			MeshClusterDX11& GetMesh();
			void SetMesh(const MeshClusterDX11& _MeshCluster);

			// adds new sub material
			void AddMaterial(const MaterialDX11& _Material);
			// replaces all maetrials with the one provided in _Material
			void SetMaterial(const MaterialDX11& _Material);
			// replaces all materials with new ones provided in _Materials
			void SetMaterials(const std::vector<MaterialDX11>& _Materials);
			//returns all assigned materials
			const std::vector<MaterialDX11>& GetMaterials() const;
			std::vector<MaterialDX11>& GetMaterials();

			void SetPrimitiveTopology(PrimitiveTopology _kPrimitiveTopology);
			PrimitiveTopology GetPrimitiveTopology() const;

			void SetVertexCount(uint32_t _uCount);
			const uint32_t& GetVertexCount() const;
	
			void SetIndexCount(uint32_t _uCount);
			const uint32_t& GetIndexCount() const;

			uint32_t GetPolyCount() const;

			// from IBaseObject
			inline virtual Math::transform GetTransform() const override { return m_Transform; }
			inline virtual void SetTransform(const Math::transform& _Transform) override { m_Transform = _Transform; }

			// new interface functions
			inline virtual const Math::float3& GetRenderingScale() const { return m_vRenderingScale; }
			inline virtual void SetRenderingScale(const Math::float3& _vScale){	m_vRenderingScale = _vScale;}
	
			__declspec(property(get = GetRenderingScale, put = SetRenderingScale)) Math::float3 RenderingScale;
			
			// from IBaseObject
			virtual void SetName(const std::string& _sName) override;
			const std::string& GetName() const final; // game object does not override this

			void SetMeshFromDesc(const Datastructures::GameObjectDesc& _Desc);

			void BindToNucleo();

			void AddSyncComponent(Datastructures::SyncComponent* _pSyncComponent);

			uint64_t GetMaterialMask() const;

			void UpdateMaterialMask();

		protected:
			// helper function
			std::string GetNewMaterialName(const std::string& _sMeshName);
			std::string GetNewMaterialName(const std::string& _sClusterName, const std::string& _sMeshName);
		private:
			class GOIDType {};
			RenderObjectDX11(GOIDType, Datastructures::IBaseObject* _pParent = nullptr);

			void LoadMaterialsRelaxed(const std::vector<std::string>& _MatPropNames);

		private:
			// getter & setter for nucleo
#ifdef HNUCLEO
			void SetMeshFromFilePath(const std::string& _sFilePath);
			std::string GetMeshFilePath() const;

			void RemoveSubMesh();
			void AddSubMesh();

			std::string GetSubMeshMaterialPath() const;
			void SetSubMeshMaterialPath(const std::string& _sPath);
#endif
		protected:
			Math::float3 m_vRenderingScale = {1.f,1.f,1.f};
			Math::transform m_Transform = HTRANSFORM_IDENTITY;

		protected:
			std::string m_sName;

			MeshClusterDX11 m_MeshCluster = nullptr;
			std::vector<MaterialDX11> m_Materials;
			uint64_t m_uMaterialMask = 0;;

			static UniqueID<uint32_t> NewMatID;

#ifdef HNUCLEO
			std::string m_sSelectedSubMesh;
			Datastructures::SyncGroup* m_pMaterialGroup = nullptr;
			inline std::string GetSubMeshName(int _iIndex) const { return m_MeshCluster.GetSubMeshes()[_iIndex].m_sSubName; }
			void SelectSubMesh(int _iIndex, bool _bBlocking = true);

			Datastructures::SyncButton m_RemoveSubMesh = { "Remove SubMesh", [&](){	RemoveSubMesh();} };
			Datastructures::SyncButton m_AddSubMesh = { "Add SubMesh", [&](){ AddSubMesh();} };

			Datastructures::SyncVariable<std::string> m_SyncSelectedSubMesh = { m_sSelectedSubMesh, "SelectedSubMesh" };
			Datastructures::SyncList<SubMesh> m_SyncSubMeshes = { "SubMeshes", &m_MeshCluster.GetSubMeshes(),  [&](int i) {return GetSubMeshName(i); }, [&](int i) {SelectSubMesh(i); } };
			Datastructures::SyncVariable<physx::PxVec3> m_SyncRenderPosition = { m_Transform.p, "Position" };
			Datastructures::SyncVariable<physx::PxQuat> m_SyncRenderOrientation = { m_Transform.q, "Orientation" };

			SYNOBJ(Scale, RenderObjectDX11, Math::float3, GetRenderingScale, SetRenderingScale) m_SyncRenderingScale = { *this, "Scale" };
			SYNOBJ(Mesh, RenderObjectDX11, std::string, GetMeshFilePath, SetMeshFromFilePath) m_SyncMesh = { *this, "Mesh" };
			SYNOBJ(Material, RenderObjectDX11, std::string, GetSubMeshMaterialPath, SetSubMeshMaterialPath) m_SyncMaterial = { *this, "Sub Material" };

			SYNOBJ(Name, RenderObjectDX11, std::string, GetName, SetName) m_SyncName = { *this, "Name" };

			Datastructures::SyncGroup m_ObjectGroup = { "Object", {&m_SyncName, &m_SyncRenderingScale, &m_SyncMesh, &m_SyncSelectedSubMesh, &m_AddSubMesh, &m_RemoveSubMesh, &m_SyncSubMeshes, &m_SyncMaterial }};

			Datastructures::SyncDock m_ComponentDock = { "Components", {}};
			
			Datastructures::SyncDock m_ObjectDock = { "Object",{ &m_ObjectGroup }, Datastructures::DockAnchor_Right, true }; // OnTopTab
#endif

		private:
			uint32_t m_uVertexCount = 0;
			uint32_t m_uIndexCount = 0;
			PrimitiveTopology m_kPrimitiveTopology = PrimitiveTopology_Undefined;
		};

		inline bool RenderObjectDX11::CheckRenderPass(uint64_t _kPass) const
		{
			for (const MaterialDX11& Mat : m_Materials)
			{
				if (Mat.CheckRenderPass(_kPass))
				{
					return true;
				}
			}
			return false;
		}

		inline uint64_t RenderObjectDX11::GetMaterialMask() const
		{
			return m_uMaterialMask;
		}

		inline const MeshClusterDX11& RenderObjectDX11::GetMesh() const { return m_MeshCluster; }
		inline MeshClusterDX11& RenderObjectDX11::GetMesh() { return m_MeshCluster;}
		inline void RenderObjectDX11::SetMesh(const MeshClusterDX11& _MeshCluster) { m_MeshCluster = _MeshCluster; }

		inline void RenderObjectDX11::SetPrimitiveTopology(PrimitiveTopology _kPrimitiveTopology) { m_kPrimitiveTopology = _kPrimitiveTopology; }
		inline PrimitiveTopology RenderObjectDX11::GetPrimitiveTopology() const { return m_kPrimitiveTopology; }

		inline void RenderObjectDX11::SetVertexCount(uint32_t _uCount) { m_uVertexCount = _uCount; }
		inline const uint32_t& RenderObjectDX11::GetVertexCount() const { return m_uVertexCount /*== 0u && m_MeshCluster ? m_MeshCluster.GetVertexCount() : m_uVertexCount*/; }

		inline void RenderObjectDX11::SetIndexCount(uint32_t _uCount) {m_uIndexCount = _uCount;}
		inline const uint32_t& RenderObjectDX11::GetIndexCount() const {return m_uIndexCount /*== 0u && m_MeshCluster ? m_MeshCluster.GetIndexCount() : m_uIndexCount*/;}

		inline uint32_t RenderObjectDX11::GetPolyCount() const
		{
			if (m_uIndexCount > 0u)
			{
				return m_uIndexCount / 3;
			}
			else if (m_uVertexCount > 0u)
			{
				return m_uVertexCount / 3;
			}
			//else if(m_MeshCluster)
			//{
			//	return m_MeshCluster.GetPolyCount();
			//}

			return 0u;
		}
		
		inline void RenderObjectDX11::AddMaterial(const MaterialDX11& _Material) { m_Materials.push_back(_Material); UpdateMaterialMask(); }
		inline void RenderObjectDX11::SetMaterial(const MaterialDX11& _Material) { m_Materials = { _Material }; UpdateMaterialMask();}
		inline void RenderObjectDX11::SetMaterials(const std::vector<MaterialDX11>& _Materials)	{m_Materials = _Materials; UpdateMaterialMask();}
		inline const std::vector<MaterialDX11>& RenderObjectDX11::GetMaterials() const { return m_Materials; }
		inline std::vector<MaterialDX11>& RenderObjectDX11::GetMaterials() { return m_Materials; }

		inline void RenderObjectDX11::SetName(const std::string& _sName) { m_sName = _sName;}
		inline const std::string& RenderObjectDX11::GetName() const { return m_sName; }
	} // Display
} // Helix

#endif // RENDEROBJECTDX11_H