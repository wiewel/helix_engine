//   Copyright 2020 Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "HelixDeferredMergeRenderPassDX11.h"
#include "Display\Shaders\DeferredMerge.hlsl"
#include "Scene\LightContext.h"

using namespace Helix;
using namespace Helix::Display;
using namespace Helix::Scene;
using namespace Helix::Datastructures;

//---------------------------------------------------------------------------------------------------

HelixDeferredMergeRenderPassDX11::HelixDeferredMergeRenderPassDX11(DefaultInitializerType) : RenderPassDX11(DefaultInit, "DeferredMerge")
{
}

//---------------------------------------------------------------------------------------------------
HelixDeferredMergeRenderPassDX11::HelixDeferredMergeRenderPassDX11(const std::string& _sInstanceName) :
	RenderPassDX11("DeferredMerge", _sInstanceName,
	{ &m_DiffuseColorTexture, &m_DiffuseLightMap, &m_SpecularLightMap, &m_EmissiveLightMap, &m_SSAOMap/*, &m_BentNormalMap, &m_EnvMap*/ }, // Textures
	{ &m_MergeOutputTarget },// RenderTargets
	{ &m_CBPerCamera })  // Variables
{
}
//---------------------------------------------------------------------------------------------------
HelixDeferredMergeRenderPassDX11::~HelixDeferredMergeRenderPassDX11()
{
}
//---------------------------------------------------------------------------------------------------
std::shared_ptr<RenderPassDX11> HelixDeferredMergeRenderPassDX11::CreatePass(const std::string& _sInstanceName) const
{
	return std::make_shared<HelixDeferredMergeRenderPassDX11>(_sInstanceName);
}
//---------------------------------------------------------------------------------------------------
bool HelixDeferredMergeRenderPassDX11::OnInitalize(const hlx::TextToken& _CustomVariables)
{
	// Add RasterizerState
	RasterizerDesc RasDesc = {};
	RasDesc.m_kFillMode = FillMode_Solid;
	RasDesc.m_kCullMode = CullMode_None;
	RasDesc.m_bFrontCounterClockwise = false;

	SetRasterizerState(RasDesc);

	// Add BlendState
	RenderTargetBlendDesc RTBlendDesc = {};
	RTBlendDesc.m_bBlendEnable = false;
	RTBlendDesc.m_kSrcBlend = BlendVariable_Src_Alpha;
	RTBlendDesc.m_kDestBlend = BlendVariable_One;
	RTBlendDesc.m_kBlendOp = BlendOperation_Add;
	RTBlendDesc.m_kSrcBlendAlpha = BlendVariable_Zero;
	RTBlendDesc.m_kDestBlendAlpha = BlendVariable_Zero;
	RTBlendDesc.m_kBlendOpAlpha = BlendOperation_Add;
	RTBlendDesc.m_uRenderTargetWriteMask = Channel_All;

	BlendDesc AlphaBlendDesc = {};
	AlphaBlendDesc.m_bAlphaToCoverageEnable = false;
	AlphaBlendDesc.m_RenderTargets[0] = RTBlendDesc;
	AlphaBlendDesc.m_uSampleMask = 0xffffffff;
	AlphaBlendDesc.fBlendFactor = { 0.0f,0.0f,0.0f,0.0f };

	SetBlendState(AlphaBlendDesc);

	// Add DepthStencilState
	DepthStencilDesc DepthDesc = {};
	DepthDesc.m_bDepthEnable = false;
	DepthDesc.m_kDepthWriteMask = DepthWriteMask_Zero;
	DepthDesc.m_kDepthFunc = ComparisonFunction_Less;
	DepthDesc.m_bStencilEnable = false;

	SetDepthStencilState(DepthDesc, "DeferredMerge");

	// Default Parameter
	ClearRenderTargets(false);
		
	return m_bInitialized;
}
//---------------------------------------------------------------------------------------------------
bool HelixDeferredMergeRenderPassDX11::OnPerFrame()
{
	GetShader()->SetIOPermutation(m_bDebug ? Shaders::DeferredMerge::HPMIO_Debug : 0);

	uint32_t uClassPermutation = 0;
	
	if(m_bSSAO)
	{
		uClassPermutation |= Shaders::DeferredMerge::HPMPS_SSAO;

		//if (EnvMap != nullptr)
		//{
		//	// TODO: switch by bool -> LightContext (see SSAORenderPass)
		//	uClassPermutation |= Shaders::DeferredMerge::HPMPS_BentNormal;
		//}
	}

	Select(ShaderType_PixelShader, uClassPermutation);

	return true;
}
//---------------------------------------------------------------------------------------------------
bool HelixDeferredMergeRenderPassDX11::OnPerCamera(const CameraComponent& _Camera)
{
	SetCameraConstants(m_CBPerCamera, _Camera.GetRenderingProperties());

	return true;
}
//---------------------------------------------------------------------------------------------------