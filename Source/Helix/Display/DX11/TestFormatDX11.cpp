//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "TestFormatDX11.h"

#include "RenderDeviceDX11.h"

using namespace Helix::Display;

//---------------------------------------------------------------------------------------------------
TestFormatDX11::TestFormatDX11()
{
}
//---------------------------------------------------------------------------------------------------
TestFormatDX11::~TestFormatDX11()
{
}
//---------------------------------------------------------------------------------------------------
void TestFormatDX11::TestDepthStencilFormats(bool _bShaderResource)
{
	ID3D11Texture2D* pTexture2D;
	ID3D11Device* pDevice = RenderDeviceDX11::Instance()->GetDevice();
	hlx::string sResult;
	HRESULT Result = E_FAIL;
	const uint32_t uStart = static_cast<uint32_t>(DXGI_FORMAT_UNKNOWN + 1);
	const uint32_t uEnd = static_cast<uint32_t>(DXGI_FORMAT_B4G4R4A4_UNORM);

	const DXGI_FORMAT DSVFormat[4] = {
		DXGI_FORMAT_D32_FLOAT_S8X24_UINT,
		DXGI_FORMAT_D32_FLOAT,
		DXGI_FORMAT_D24_UNORM_S8_UINT,
		DXGI_FORMAT_D16_UNORM
	};

	D3D11_TEXTURE2D_DESC Desc = {};
	Desc.Width = 4;
	Desc.Height = 4;
	Desc.BindFlags = (_bShaderResource ? (D3D11_BIND_DEPTH_STENCIL | D3D11_BIND_SHADER_RESOURCE) : D3D11_BIND_DEPTH_STENCIL);
	Desc.Usage = D3D11_USAGE_DEFAULT;
	Desc.MipLevels = 1;
	Desc.ArraySize = 1;
	Desc.SampleDesc.Count = 1;

	D3D11_DEPTH_STENCIL_VIEW_DESC DSV_Desc = {};
	DSV_Desc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	DSV_Desc.Texture2D.MipSlice = 0;

	// iterate over 4 depth stencil view formats
	for (uint32_t i = 0; i < 4; ++i)
	{
		DSV_Desc.Format = DSVFormat[i];
		sResult += S("\nDepthStencilView - Format: ") + hlx::to_string(std::to_string(DSVFormat[i])) + S("\n");

		// iterate over all texture formats
		for (uint32_t j = uStart; j <= uEnd; ++j)
		{
			Desc.Format = static_cast<DXGI_FORMAT>(j);

			Result = pDevice->CreateTexture2D(&Desc, NULL, &pTexture2D);

			// if texture format was ok, we continue with DSV check
			if (Result == S_OK)
			{
				//Address of a pointer to an ID3D11DepthStencilView. Set this parameter to NULL to validate the other input parameters (the method will return S_FALSE if the other input parameters pass validation).
				Result = pDevice->CreateDepthStencilView(pTexture2D, &DSV_Desc, NULL);
				if (Result == S_FALSE)
				{
					sResult += S("\t") + hlx::to_string(std::to_string(Desc.Format)) + S("\n");
				}
			}

			HSAFE_RELEASE(pTexture2D);
		}
	}

	HLOG("%s", sResult.c_str());
}
//---------------------------------------------------------------------------------------------------
void TestFormatDX11::TestShaderResourceForDepthFormats()
{
	ID3D11Texture2D* pTexture2D;
	ID3D11Device* pDevice = RenderDeviceDX11::Instance()->GetDevice();
	hlx::string sResult;
	HRESULT Result = E_FAIL;
	const uint32_t uStart = static_cast<uint32_t>(DXGI_FORMAT_UNKNOWN + 1);
	const uint32_t uEnd = static_cast<uint32_t>(DXGI_FORMAT_B4G4R4A4_UNORM);

	const DXGI_FORMAT DSVFormat[4] = {
		DXGI_FORMAT_D32_FLOAT_S8X24_UINT,
		DXGI_FORMAT_D32_FLOAT,
		DXGI_FORMAT_D24_UNORM_S8_UINT,
		DXGI_FORMAT_D16_UNORM
	};

	D3D11_TEXTURE2D_DESC Desc = {};
	Desc.Width = 4;
	Desc.Height = 4;
	Desc.BindFlags = D3D11_BIND_DEPTH_STENCIL | D3D11_BIND_SHADER_RESOURCE;
	Desc.Usage = D3D11_USAGE_DEFAULT;
	Desc.MipLevels = 1;
	Desc.ArraySize = 1;
	Desc.SampleDesc.Count = 1;

	D3D11_DEPTH_STENCIL_VIEW_DESC DSV_Desc = {};
	DSV_Desc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	DSV_Desc.Texture2D.MipSlice = 0;

	D3D11_SHADER_RESOURCE_VIEW_DESC SRV_Desc = {};
	SRV_Desc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	SRV_Desc.Texture2D.MipLevels = 1;
	SRV_Desc.Texture2D.MostDetailedMip = 0;

	// iterate over 4 depth stencil view formats
	for (uint32_t i = 0; i < 4; ++i)
	{
		DSV_Desc.Format = DSVFormat[i];
		sResult += S("\nDepthStencilView - Format: ") + hlx::to_string(std::to_string(DSVFormat[i])) + S("\n");

		// iterate over all texture formats
		for (uint32_t j = uStart; j <= uEnd; ++j)
		{
			Desc.Format = static_cast<DXGI_FORMAT>(j);

			Result = pDevice->CreateTexture2D(&Desc, NULL, &pTexture2D);

			// if texture format was ok, we continue with DSV check
			if (Result == S_OK)
			{
				//Address of a pointer to an ID3D11DepthStencilView. Set this parameter to NULL to validate the other input parameters (the method will return S_FALSE if the other input parameters pass validation).
				Result = pDevice->CreateDepthStencilView(pTexture2D, &DSV_Desc, NULL);
				if (Result == S_FALSE)
				{
					// iterate over all texture formats for SRV
					for (uint32_t k = uStart; k <= uEnd; ++k)
					{
						SRV_Desc.Format = static_cast<DXGI_FORMAT>(k);
						
						Result = pDevice->CreateShaderResourceView(pTexture2D, &SRV_Desc, NULL);
						if (Result == S_FALSE)
						{
							sResult += S("\t") + hlx::to_string(std::to_string(SRV_Desc.Format)) + S("\n");
						}
					}
					sResult += S("\n");
				}
			}

			HSAFE_RELEASE(pTexture2D);
		}
	}
	HLOG("%s",sResult.c_str());
}
//---------------------------------------------------------------------------------------------------