//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef GPUBUFFERINSTANCEDX11
#define GPUBUFFERINSTANCEDX11

#include "GPUBufferDX11.h"
#include "..\SyncShaderVariable.h"

namespace Helix
{
	namespace Display
	{
		class GPUBufferInstanceDX11
		{
		public:
			HDEBUGNAME("GPUBufferInstanceDX11");

			// TODO: check where the constructor is used with writeable == true and where initial data is usually used
			GPUBufferInstanceDX11(const ShaderBufferDesc& _Desc, bool _bWriteable = true);
			virtual ~GPUBufferInstanceDX11();

			bool Initialize(
				uint32_t _uElementCount = 1u,
				const uint8_t* _pInitialData = nullptr,
				const uint32_t& _uInitialSize = 0ull);

			bool Resize(uint32_t _uElementCount);

			bool Upload();

			explicit operator bool() const;

			const GPUBufferDX11* GetBuffer() const;

			uint8_t* GetData();

			const ShaderBufferDesc& GetDescription() const;

			void SetDataChanged();
			const bool& GetDataChanged() const;

			// returns nullptr in non nucleo build
			Datastructures::SyncGroup* GetSyncGroup();

		private:
			void Clear();

#ifdef HNUCLEO
			void AddVariables(const std::vector<ShaderVariableDesc>& _Vars);
#endif

		private:

			bool m_bChanged = false;
			uint32_t m_uElementCount = 1;
			bool m_bWriteable = false;
			ShaderBufferDesc m_Description = {};
			std::vector<uint8_t> m_Data;
			GPUBufferDX11* m_pBuffer = nullptr;

#ifdef HNUCLEO
			Datastructures::SyncGroup m_SyncGroup;
			std::vector<SyncShaderVariable*> m_ShaderVars;
#endif
		};

		inline GPUBufferInstanceDX11::operator bool() const	{ return m_pBuffer != nullptr; }
		inline const GPUBufferDX11* GPUBufferInstanceDX11::GetBuffer() const {	return m_pBuffer; }
		inline uint8_t* GPUBufferInstanceDX11::GetData() {	return m_Data.data();}
		inline const ShaderBufferDesc& GPUBufferInstanceDX11::GetDescription() const { return m_Description; }
		inline void GPUBufferInstanceDX11::SetDataChanged()	{ m_bChanged = true; }
		inline const bool& GPUBufferInstanceDX11::GetDataChanged() const{return m_bChanged;}

	} // Display

} // Helix

#endif
