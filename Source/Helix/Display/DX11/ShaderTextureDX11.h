//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef SHADERTEXTUREDX11_H
#define	SHADERTEXTUREDX11_H

#include "TextureDX11.h"

namespace Helix
{
	namespace Display
	{
		class ShaderInterfaceDX11;

		class ShaderTextureDX11
		{
		public:
			HDEBUGNAME("ShaderTextureDX11");

			ShaderTextureDX11(const std::string& _sTextureName, const std::string& _sMappedName = {});
			~ShaderTextureDX11();

			bool Initialize(ShaderInterfaceDX11* _pInterface, const std::string& _sShaderInstanceName);

			bool SetTexture(const TextureDX11& _Texture, uint32_t _uIndex = 0);
			const TextureDX11& GetTexture(uint32_t _uIndex = 0) const;
			const uint32_t& GetTextureCount() const;

			const std::string& GetName() const;
			const std::string& GetMappedName() const;
			void SetMappedName(const std::string& _sMappedName);

		private:
			const std::string m_sName;
			std::string m_sMappedName;
			std::string m_sShaderInstanceName;
		
			ShaderInterfaceDX11* m_pInterface = nullptr;

			ShaderTextureDesc m_Description;
			std::vector<TextureDX11> m_CurrentTextures;
		};

		inline const uint32_t& ShaderTextureDX11::GetTextureCount() const {	return m_Description.BindInfo.uCount; }

		inline const TextureDX11& ShaderTextureDX11::GetTexture(uint32_t _uIndex) const
		{
			HASSERT(_uIndex < m_CurrentTextures.size(), "Invalid TextureSlot index %u", _uIndex);
			return m_CurrentTextures.at(_uIndex);		
		}

		inline const std::string& ShaderTextureDX11::GetName() const{ return m_sName; }
		inline const std::string& ShaderTextureDX11::GetMappedName() const { return m_sMappedName;	}
		inline void ShaderTextureDX11::SetMappedName(const std::string & _sMappedName) { m_sMappedName = _sMappedName; }
	} // Display
} // Helix

#endif // SHADERTEXTUREDX11_H
