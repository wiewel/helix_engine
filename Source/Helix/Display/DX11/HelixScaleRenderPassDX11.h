//   Copyright 2020 Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef HELIXSCALERENDERPASSDX11_H
#define HELIXSCALERENDERPASSDX11_H

#include "RenderPassDX11.h"

namespace Helix
{
	namespace Display
	{
		class HelixScaleRenderPassDX11 : public RenderPassDX11
		{
		public:
			HDEBUGNAME("HelixScaleRenderPassDX11");

			HelixScaleRenderPassDX11(DefaultInitializerType);
			HelixScaleRenderPassDX11(const std::string& _sInstanceName = {});
			~HelixScaleRenderPassDX11();

			bool OnInitalize(const hlx::TextToken& _CustomVariables) final;

			//bool OnPerFrame() final;
			std::shared_ptr<RenderPassDX11> CreatePass(const std::string& _sInstanceName) const final;

		private:

			ShaderTextureDX11 m_InputTexture = { "gInTex" };
			ShaderRenderTargetDX11 m_OutputTexture = { "vScaled", "SCALED_TEXTURE" };
		};

	} // Display
} // Helix

#endif // HELIXSCALERENDERPASSDX11_H