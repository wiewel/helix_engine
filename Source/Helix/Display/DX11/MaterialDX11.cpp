//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "MaterialDX11.h"
#include "TextureVariantsDX11.h"
#include "Resources\FileSystem.h"
#include "Resources\HMATPROPFile.h"
#include "Display\RenderPassRegister.h"

using namespace Helix::Display;
//---------------------------------------------------------------------------------------------------
MaterialDX11::MaterialDX11(std::nullptr_t) noexcept : TDX11MaterialRef(nullptr)
{
}
//---------------------------------------------------------------------------------------------------
MaterialDX11::MaterialDX11(const DefaultInitializerType& t) :
	TDX11MaterialRef(DefaultInit)
{
	m_Properties.kRenderPasses = MaterialPoolDX11::GetDefaultRenderPasses();
}
//---------------------------------------------------------------------------------------------------
MaterialDX11::MaterialDX11(const MaterialDesc& _Desc):
	TDX11MaterialRef(_Desc), m_Properties(_Desc.Properties)
{
}
//---------------------------------------------------------------------------------------------------
MaterialDX11::MaterialDX11(
	const std::string& _sNameOrFilePath,
	bool _bLoadTextures, bool _bLoadFromFile, bool _bPropFile,
	const std::string& _sAlbedo, const std::string& _sNormal,
	const std::string& _sMetallic, const std::string& _sRoughness,
	const std::string& _sHeight, const std::string& _sEmissive)
{
	MaterialDesc Desc = {};
	Desc.bLoadFromFile = _bLoadFromFile;
	Desc.bLoadTextures = _bLoadTextures;

	if (_bPropFile)
	{
		Resources::HMATPROPFile PropsFile(STR(_sNameOrFilePath));
		const Resources::HMatProps& Props = PropsFile.GetProperties();

		if (Props.sHMatName.empty())
		{
			HERROR("%s hmatprop does not reference any material!", CSTR(_sNameOrFilePath));
			Desc.sFilePathOrName = _sNameOrFilePath;
		}
		else
		{
			Desc.sFilePathOrName = Props.sHMatName;
		}

		m_Properties.kRenderPasses = 0u;
		for (const std::string& sPass : Props.RenderPasses)
		{
			m_Properties.kRenderPasses |= HPASS(sPass);
		}

		if (Props.bUseDefaultPasses/* && m_Properties.kRenderPasses == 0ull*/)
		{
			m_Properties.kRenderPasses = MaterialPoolDX11::GetDefaultRenderPasses();
		}

		m_Properties.sAssociatedSubMesh = Props.sSubMeshName;
		m_Properties.vAlbedo = Props.vColorAlbedo;

		m_Properties.vEmissiveColor = Props.vColorEmissive;
		m_Properties.vEmissiveOffset = Props.EmissiveMap.vOffset.xyz;
		m_Properties.vEmissiveScale = Props.EmissiveMap.vScale.xyz;

		m_Properties.fRoughness = Props.fRoughness;
		m_Properties.fRoughnessOffset = Props.RoughnessMap.vOffset.x;
		m_Properties.fRoughnessScale = Props.RoughnessMap.vScale.x;

		m_Properties.fMetallic = Props.fMetallic;
		m_Properties.fHeightScale = Props.HeightMap.vScale.x;
		m_Properties.fHorizonFade = Props.fHorizonFade;

		m_Properties.vTextureTiling = Props.vTextureTiling;

		m_Properties.bUseAlbedoMap = Props.AlbedoMap.bEnabled;
		m_Properties.bUseNormalMap = Props.NormalMap.bEnabled;
		m_Properties.bUseMetallicMap = Props.MetallicMap.bEnabled;
		m_Properties.bUseRoughnessMap = Props.RoughnessMap.bEnabled;
		m_Properties.bUseHeightMap = Props.HeightMap.bEnabled;
		m_Properties.bUseEmissiveMap = Props.EmissiveMap.bEnabled;
	}
	else
	{
		Desc.sFilePathOrName = _sNameOrFilePath;
	}

	Desc.sAlbedoMap = _sAlbedo;
	Desc.sNormalMap = _sNormal;
	Desc.sMetallicMap = _sMetallic;
	Desc.sRoughnessMap = _sRoughness;
	Desc.sHeightMap = _sHeight;
	Desc.sEmissiveMap = _sEmissive;

	m_pReference = MaterialPoolDX11::Instance()->Get(Desc, m_Hash);
}
//---------------------------------------------------------------------------------------------------
MaterialDX11 MaterialDX11::CreateNew(const std::string& _sNameOrFilePath, const bool _bUseDefaultPasses)
{
	MaterialDesc Desc = {};
	Desc.bLoadFromFile = false;
	Desc.bLoadTextures = false;
	Desc.sFilePathOrName = _sNameOrFilePath;
	
	if (_bUseDefaultPasses)
	{
		Desc.Properties.kRenderPasses = MaterialPoolDX11::GetDefaultRenderPasses();
	}

	return MaterialDX11(Desc);
}
//---------------------------------------------------------------------------------------------------
MaterialDX11::MaterialDX11(const MaterialDX11& _Material) :
	TDX11MaterialRef(_Material)
{
	m_Properties = _Material.GetProperties();
}
//---------------------------------------------------------------------------------------------------
MaterialDX11::~MaterialDX11()
{
}

//---------------------------------------------------------------------------------------------------
MaterialDX11& MaterialDX11::operator=(std::nullptr_t)
{
	TDX11MaterialRef::operator=(nullptr);
	m_Properties = {};
	return *this;
}
//---------------------------------------------------------------------------------------------------
MaterialDX11& MaterialDX11::operator=(const MaterialDX11& _Other)
{
	if (this == &_Other)
		return *this;

	TDX11MaterialRef::operator=(_Other);
	m_Properties = _Other.GetProperties();
	return *this;
}
//---------------------------------------------------------------------------------------------------
std::string MaterialDX11::GetPathOrName() const
{
	return m_pReference != nullptr ? m_pReference->sPathOrName : std::string();
}

#ifdef HNUCLEO
//---------------------------------------------------------------------------------------------------
void MaterialDX11::SetAlbedoMapFile(const std::string& _sPath)
{
	m_Properties.bUseAlbedoMap = _sPath.empty() == false;

	if (m_pReference != nullptr)
	{
		m_pReference->AlbedoMap = m_Properties.bUseAlbedoMap ? ImmutableTextureDX11(_sPath, 0, false, true) : nullptr;
	}
}
//---------------------------------------------------------------------------------------------------
std::string MaterialDX11::GetAlbedoMapFile() const
{
	return m_pReference != nullptr && GetAlbedoMap() ? GetAlbedoMap().GetDescription().m_sFilePathOrName : "";
}
//---------------------------------------------------------------------------------------------------
void MaterialDX11::SetMetallicMapFile(const std::string& _sPath)
{
	m_Properties.bUseMetallicMap = _sPath.empty() == false;

	if (m_pReference != nullptr)
	{
		m_pReference->MetallicMap = m_Properties.bUseMetallicMap ? ImmutableTextureDX11(_sPath, 0, false, true) : nullptr;
	}
}
//---------------------------------------------------------------------------------------------------
std::string MaterialDX11::GetMetallicMapFile() const
{
	return m_pReference != nullptr && GetMetallicMap() ? GetMetallicMap().GetDescription().m_sFilePathOrName : "";
}
//---------------------------------------------------------------------------------------------------
void MaterialDX11::SetRoughnessMapFile(const std::string& _sPath)
{
	m_Properties.bUseRoughnessMap = _sPath.empty() == false;

	if (m_pReference != nullptr)
	{
		m_pReference->RoughnessMap = m_Properties.bUseRoughnessMap ? ImmutableTextureDX11(_sPath, 0, false, true) : nullptr;
	}
}
//---------------------------------------------------------------------------------------------------
std::string MaterialDX11::GetRoughnessMapFile() const
{
	return m_pReference != nullptr && GetRoughnessMap() ? GetRoughnessMap().GetDescription().m_sFilePathOrName : "";
}
//---------------------------------------------------------------------------------------------------
void MaterialDX11::SetNormalMapFile(const std::string& _sPath)
{
	m_Properties.bUseNormalMap = _sPath.empty() == false;

	if (m_pReference != nullptr)
	{
		m_pReference->NormalMap = m_Properties.bUseNormalMap ? ImmutableTextureDX11(_sPath, 0, false, true) : nullptr;
	}
}
//---------------------------------------------------------------------------------------------------
std::string MaterialDX11::GetNormalMapFile() const
{
	return m_pReference != nullptr && GetNormalMap() ? GetNormalMap().GetDescription().m_sFilePathOrName : "";
}
//---------------------------------------------------------------------------------------------------
void MaterialDX11::SetHeightMapFile(const std::string& _sPath)
{
	m_Properties.bUseHeightMap = _sPath.empty() == false;

	if (m_pReference != nullptr)
	{
		m_pReference->HeightMap = m_Properties.bUseHeightMap ? ImmutableTextureDX11(_sPath, 0, false, true) : nullptr;
	}
}
//---------------------------------------------------------------------------------------------------
std::string MaterialDX11::GetHeightMapFile() const
{
	return m_pReference != nullptr && GetHeightMap() ? GetHeightMap().GetDescription().m_sFilePathOrName : "";
}

//---------------------------------------------------------------------------------------------------
void MaterialDX11::SetEmissiveMapFile(const std::string& _sPath)
{
	m_Properties.bUseEmissiveMap = _sPath.empty() == false;

	if (m_pReference != nullptr)
	{
		m_pReference->EmissiveMap = m_Properties.bUseEmissiveMap ? ImmutableTextureDX11(_sPath, 0, false, true) : nullptr;
	}
}
//---------------------------------------------------------------------------------------------------
std::string MaterialDX11::GetEmissiveMapFile() const
{
	return m_pReference != nullptr && GetEmissiveMap() ? GetEmissiveMap().GetDescription().m_sFilePathOrName : "";
}

//---------------------------------------------------------------------------------------------------
#endif