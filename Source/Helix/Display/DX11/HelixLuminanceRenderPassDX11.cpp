//   Copyright 2020 Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "HelixLuminanceRenderPassDX11.h"
#include "Display\Shaders\Luminance.hlsl"

using namespace Helix;
using namespace Helix::Display;
using namespace Helix::Scene;
using namespace Helix::Datastructures;

//---------------------------------------------------------------------------------------------------

HelixLuminanceRenderPassDX11::HelixLuminanceRenderPassDX11(DefaultInitializerType) : RenderPassDX11(DefaultInit, "Luminance")
{
}

//---------------------------------------------------------------------------------------------------
HelixLuminanceRenderPassDX11::HelixLuminanceRenderPassDX11(const std::string& _sInstanceName) :
	RenderPassDX11("Luminance", _sInstanceName,
	{ &m_InputTexture },
	{ &m_OutputTexture } )
{
}
//---------------------------------------------------------------------------------------------------
HelixLuminanceRenderPassDX11::~HelixLuminanceRenderPassDX11()
{
}
//---------------------------------------------------------------------------------------------------
bool HelixLuminanceRenderPassDX11::OnInitalize(const hlx::TextToken& _CustomVariables)
{	
	// Add RasterizerState
	RasterizerDesc RasDesc = {};
	RasDesc.m_kFillMode = FillMode_Solid;
	RasDesc.m_kCullMode = CullMode_None;
	RasDesc.m_bFrontCounterClockwise = false;

	SetRasterizerState(RasDesc);

	// Add BlendState
	RenderTargetBlendDesc RTBlendDesc = {};
	RTBlendDesc.m_bBlendEnable = false;

	BlendDesc AlphaBlendDesc = {};
	AlphaBlendDesc.m_bAlphaToCoverageEnable = false;
	AlphaBlendDesc.m_RenderTargets[0] = RTBlendDesc;
	AlphaBlendDesc.m_uSampleMask = 0xffffffff;
	AlphaBlendDesc.fBlendFactor = { 0.0f,0.0f,0.0f,0.0f };

	SetBlendState(AlphaBlendDesc);

	// Add DepthStencilState
	DepthStencilDesc DepthDesc = {};
	DepthDesc.m_bDepthEnable = false;
	DepthDesc.m_bStencilEnable = false;

	SetDepthStencilState(DepthDesc, "Luminance");

	// Default Parameter
	ClearRenderTargets(false);

	return m_bInitialized;
}
//---------------------------------------------------------------------------------------------------
std::shared_ptr<RenderPassDX11> HelixLuminanceRenderPassDX11::CreatePass(const std::string& _sInstanceName) const
{
	return std::make_shared<HelixLuminanceRenderPassDX11>(_sInstanceName);
}
//---------------------------------------------------------------------------------------------------