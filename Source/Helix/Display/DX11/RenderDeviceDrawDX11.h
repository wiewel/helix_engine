//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef RENDERDEVICEDRAWDX11_H
#define RENDERDEVICEDRAWDX11_H

// this header defines all functions needed to render a object inline without 
// breaking include restrictions for types that use RenderDeviceDX11 in their definition

#include "RenderDeviceDX11.h"
#include "RenderObjectDX11.h"
#include "ShaderDX11.h"
#include "ShaderTextureDX11.h"
#include "ShaderSamplerDX11.h"

namespace Helix
{
	namespace Display
	{
		class RenderDeviceDrawDX11
		{
		public:
			RenderDeviceDrawDX11(RenderDeviceDX11* _pRenderDevice);

			void SelectShaders();
			void SelectInputLayout();
			void SelectBuffers();
			bool SelectTextures();
			void SelectSamplers();

			// calls all select functions above, returns false if select textures failed
			bool Select();

			void ApplyVertexBuffers();
			void ApplyIndexBuffer();
			void ApplyShaderResources();
			void ApplyConstantBuffers();

			// Draw using vertex count specified in renderobject, ignoring vertex / index buffer data (e.g. using sv_id)
			void DrawRenderObject(const RenderObjectDX11& _RenderObject);
			// Draw using vertex / index buffer
			void DrawSubMesh(const SubMesh& _SubMesh, const GPUBufferDX11* _pVertexBuffer, const GPUBufferDX11* pIndexBuffer = nullptr, const std::string& _sObjectName = {});

			void SetVertexBuffer(const GPUBufferDX11& _Buffer, const uint32_t _uStrideSize, const uint32_t _uSlot, const uint32_t _uOffset = 0);
			void SetIndexBuffer(const GPUBufferDX11& _Buffer, const PixelFormat kIndexFormat, const uint32_t _uOffset = 0); //Offset(in bytes) from the start of the index buffer to the first index to use.

		private:
			RenderDeviceDX11* m_pRenderDevice = nullptr;
		};

		inline RenderDeviceDrawDX11::RenderDeviceDrawDX11(RenderDeviceDX11* _pRenderDevice) :
			m_pRenderDevice(_pRenderDevice)
		{
		}

		//---------------------------------------------------------------------------------------------------
		inline void RenderDeviceDrawDX11::ApplyShaderResources()
		{
			for (uint32_t i = 0; i < ShaderType_NumOf; ++i)
			{
				// Update SamplerStates:
				UpdateIndex& Index = m_pRenderDevice->m_UpdatedShaderResources[i];
				uint32_t uStartSlot = Index.GetStartIndex();
				uint32_t uCount = Index.GetCount();

				if (Index)
				{
					switch (i)
					{
					case ShaderType_VertexShader:
						m_pRenderDevice->m_pDeviceContext->VSSetShaderResources(uStartSlot, uCount, &m_pRenderDevice->m_pCurrentShaderResources[i][uStartSlot]);
						break;
					case ShaderType_HullShader:
						m_pRenderDevice->m_pDeviceContext->HSSetShaderResources(uStartSlot, uCount, &m_pRenderDevice->m_pCurrentShaderResources[i][uStartSlot]);
						break;
					case ShaderType_DomainShader:
						m_pRenderDevice->m_pDeviceContext->DSSetShaderResources(uStartSlot, uCount, &m_pRenderDevice->m_pCurrentShaderResources[i][uStartSlot]);
						break;
					case ShaderType_GeometryShader:
						m_pRenderDevice->m_pDeviceContext->GSSetShaderResources(uStartSlot, uCount, &m_pRenderDevice->m_pCurrentShaderResources[i][uStartSlot]);
						break;
					case ShaderType_PixelShader:
						m_pRenderDevice->m_pDeviceContext->PSSetShaderResources(uStartSlot, uCount, &m_pRenderDevice->m_pCurrentShaderResources[i][uStartSlot]);
						break;
					case ShaderType_ComputeShader:
						m_pRenderDevice->m_pDeviceContext->CSSetShaderResources(uStartSlot, uCount, &m_pRenderDevice->m_pCurrentShaderResources[i][uStartSlot]);
						break;
					default:
						break;
					}
				}

				// reset start & end slot
				Index.Reset();
			}
		}
		//---------------------------------------------------------------------------------------------------
		inline void RenderDeviceDrawDX11::ApplyConstantBuffers()
		{
			for (uint32_t i = 0; i < ShaderType_NumOf; ++i)
			{
				// UpdateConstantBuffers:
				UpdateIndex& Index = m_pRenderDevice->m_UpdatedConstantBuffers[i];
				uint32_t uStartSlot = Index.GetStartIndex();
				uint32_t uCount = Index.GetCount();

				if (Index)
				{
					switch (i)
					{
					case ShaderType_VertexShader:
						m_pRenderDevice->m_pDeviceContext->VSSetConstantBuffers(uStartSlot, uCount, &m_pRenderDevice->m_pCurrentConstantBuffers[i][uStartSlot]);
						break;
					case ShaderType_HullShader:
						m_pRenderDevice->m_pDeviceContext->HSSetConstantBuffers(uStartSlot, uCount, &m_pRenderDevice->m_pCurrentConstantBuffers[i][uStartSlot]);
						break;
					case ShaderType_DomainShader:
						m_pRenderDevice->m_pDeviceContext->DSSetConstantBuffers(uStartSlot, uCount, &m_pRenderDevice->m_pCurrentConstantBuffers[i][uStartSlot]);
						break;
					case ShaderType_GeometryShader:
						m_pRenderDevice->m_pDeviceContext->GSSetConstantBuffers(uStartSlot, uCount, &m_pRenderDevice->m_pCurrentConstantBuffers[i][uStartSlot]);
						break;
					case ShaderType_PixelShader:
						m_pRenderDevice->m_pDeviceContext->PSSetConstantBuffers(uStartSlot, uCount, &m_pRenderDevice->m_pCurrentConstantBuffers[i][uStartSlot]);
						break;
					case ShaderType_ComputeShader:
						m_pRenderDevice->m_pDeviceContext->CSSetConstantBuffers(uStartSlot, uCount, &m_pRenderDevice->m_pCurrentConstantBuffers[i][uStartSlot]);
						break;
					default:
						break;
					}
				}

				// reset start & end slot
				Index.Reset();
			}
		}
		//---------------------------------------------------------------------------------------------------
		inline bool RenderDeviceDrawDX11::Select()
		{
			SelectShaders();
			SelectInputLayout();
			SelectSamplers();
			SelectBuffers();
			//SelectRenderTargets(); This function is called in RenderPass::Apply() -> once per pass, not per object! -> Permutations should not change the rendertarget output!
			return SelectTextures();
		}
		//---------------------------------------------------------------------------------------------------
		inline void RenderDeviceDrawDX11::ApplyVertexBuffers()
		{
			if (m_pRenderDevice->m_UpdatedVertexBuffers)
			{
				uint32_t uStartSlot = m_pRenderDevice->m_UpdatedVertexBuffers.GetStartIndex();
				uint32_t uCount = m_pRenderDevice->m_UpdatedVertexBuffers.GetCount();

				//Calling this method using a buffer that is currently bound for writing(i.e.bound to the stream output pipeline stage)
				//will effectively bind NULL instead because a buffer cannot be bound as both an input and an output at the same time.

				m_pRenderDevice->m_pDeviceContext->IASetVertexBuffers(uStartSlot, uCount,
					&m_pRenderDevice->m_pCurrentVertexBuffers[uStartSlot],
					&m_pRenderDevice->m_uCurrentVertexStrideSizes[uStartSlot],
					&m_pRenderDevice->m_uCurrentVertexOffsets[uStartSlot]);

				m_pRenderDevice->m_UpdatedVertexBuffers.Reset();
			}
		}
		//---------------------------------------------------------------------------------------------------
		inline void RenderDeviceDrawDX11::ApplyIndexBuffer()
		{
			if (m_pRenderDevice->m_bIndexBufferUpdated)
			{
				m_pRenderDevice->m_pDeviceContext->IASetIndexBuffer(
					m_pRenderDevice->m_pCurrentIndexBuffer,
					m_pRenderDevice->m_kIndexBufferFormat,
					m_pRenderDevice->m_uCurrentIndexOffset);

				m_pRenderDevice->m_bIndexBufferUpdated = false;
			}
		}
		//---------------------------------------------------------------------------------------------------

		inline void RenderDeviceDrawDX11::SelectShaders()
		{
			HASSERTD(m_pRenderDevice->m_pSelectedShader != nullptr, "No shader selected!");

			for (uint32_t i = 0; i < ShaderType_NumOf; ++i)
			{
				ShaderType kShaderType = static_cast<ShaderType>(i);

				// shader already set
				ShaderPtrDX11 Shader = m_pRenderDevice->m_pSelectedShader->GetShader(kShaderType);
				if (m_pRenderDevice->m_ActiveShaders[i] == Shader)
				{
					continue;
				}

				++m_pRenderDevice->m_uShaderChanges;
				m_pRenderDevice->m_ActiveShaders[i] = Shader;

				switch (kShaderType)
				{
				case ShaderType_VertexShader:
					m_pRenderDevice->m_pDeviceContext->VSSetShader(Shader.pVertexShader, nullptr, 0);
					break;
				case ShaderType_HullShader:
					m_pRenderDevice->m_pDeviceContext->HSSetShader(Shader.pHullShader, nullptr, 0);
					break;
				case ShaderType_DomainShader:
					m_pRenderDevice->m_pDeviceContext->DSSetShader(Shader.pDomainShader, nullptr, 0);
					break;
				case ShaderType_GeometryShader:
					m_pRenderDevice->m_pDeviceContext->GSSetShader(Shader.pGeometryShader, nullptr, 0);
					break;
				case ShaderType_PixelShader:
					m_pRenderDevice->m_pDeviceContext->PSSetShader(Shader.pPixelShader, nullptr, 0);
					break;
				case ShaderType_ComputeShader:
					m_pRenderDevice->m_pDeviceContext->CSSetShader(Shader.pComputeShader, nullptr, 0);
					break;
				}
			}
		}
		//---------------------------------------------------------------------------------------------------
		inline void RenderDeviceDrawDX11::SelectBuffers()
		{
			HASSERTD(m_pRenderDevice->m_pSelectedShader != nullptr, "No shader selected!");

			ShaderInterfaceDX11* pInterface = m_pRenderDevice->m_pSelectedShader->GetInterface();

			for (uint32_t i = 0; i < ShaderType_NumOf; ++i)
			{
				const TSelectedBuffers& SelectedBuffers = pInterface->GetSelectedBuffers(static_cast<ShaderType>(i));
				const size_t uSize = SelectedBuffers.size();

				for (size_t b = 0; b < uSize; ++b)
				{
					const Selected<std::shared_ptr<GPUBufferInstanceDX11>>& BufferInstance = SelectedBuffers[b];

					BufferInstance.pRes->Upload();
					const GPUBufferDX11* pBuffer = BufferInstance.pRes->GetBuffer();
					const BufferDesc& Desc = pBuffer->GetDescription();

					uint32_t uSlot = BufferInstance.uSlot;

					HASSERTD(uSlot != HUNDEFINED32, "Invalid slot!");

					if (Desc.kBindFlag & ResourceBindFlag_ConstantBuffer)
					{
						if (m_pRenderDevice->m_pCurrentConstantBuffers[i][uSlot] != pBuffer->GetBuffer())
						{
							m_pRenderDevice->m_pCurrentConstantBuffers[i][uSlot] = pBuffer->GetBuffer();
							m_pRenderDevice->m_UpdatedConstantBuffers[i].Add(uSlot);
						}

						continue;
					}

					if (Desc.kBindFlag & ResourceBindFlag_ShaderResource)
					{
						HASSERTD(pBuffer->GetShaderResourceView() != nullptr, "Invalid (nullptr) ShaderResourceView!");

						if (m_pRenderDevice->m_pCurrentShaderResources[i][uSlot] != pBuffer->GetShaderResourceView())
						{
							m_pRenderDevice->m_pCurrentShaderResources[i][uSlot] = pBuffer->GetShaderResourceView();
							m_pRenderDevice->m_UpdatedShaderResources[i].Add(uSlot);
							m_pRenderDevice->m_PerPassUpdatedSRVs[i].Add(uSlot);
						}
					}

					if (Desc.kBindFlag & ResourceBindFlag_UnorderedAccess)
					{
						HASSERTD(pBuffer->GetUnorderedAccessView() != nullptr, "Invalid (nullptr) UnorderedAccessView!");
						if (m_pRenderDevice->m_pCurrentUnorderedAccessViews[uSlot] != pBuffer->GetUnorderedAccessView())
						{
							m_pRenderDevice->m_UpdatedUnorderedAccessViews.Add(uSlot);
						}
					}
				}
			}

			// dont apply SRVs just now because textures might set them too!
			ApplyConstantBuffers();

			// apply structured buffers
			if (m_pRenderDevice->m_UpdatedUnorderedAccessViews)
			{
				// An array of append and consume buffer offsets.A value of - 1 indicates to keep the current offset.Any other values set the hidden counter
				// for that appendable and consumeable UAV.pUAVInitialCounts is only relevant for UAVs that were created with either D3D11_BUFFER_UAV_FLAG_APPEND
				// or D3D11_BUFFER_UAV_FLAG_COUNTER specified when the UAV was created; otherwise, the argument is ignored.

				m_pRenderDevice->GetContext()->CSSetUnorderedAccessViews(
					m_pRenderDevice->m_UpdatedUnorderedAccessViews.GetStartIndex(),
					m_pRenderDevice->m_UpdatedUnorderedAccessViews.GetCount(),
					m_pRenderDevice->m_pCurrentUnorderedAccessViews,
					nullptr);

				m_pRenderDevice->m_UpdatedUnorderedAccessViews.Reset();
			}
		}

		//---------------------------------------------------------------------------------------------------
		inline bool RenderDeviceDrawDX11::SelectTextures()
		{
			HASSERTD(m_pRenderDevice->m_pSelectedShader != nullptr, "No shader selected!");

			ShaderInterfaceDX11* pInterface = m_pRenderDevice->m_pSelectedShader->GetInterface();

			bool bSuccess = true;

			for (uint32_t i = 0; i < ShaderType_NumOf; ++i)
			{
				const TSelectedTextures& SelectedTextures = pInterface->GetSelectedTextures(static_cast<ShaderType>(i));
				const size_t uSize = SelectedTextures.size();

				for (size_t t = 0; t < uSize; ++t)
				{
					const Selected<ShaderTextureDX11*>& Textures = SelectedTextures[t];

					const uint32_t uBindCount = Textures.pRes->GetTextureCount();
					for (uint32_t uSlotIndex = 0; uSlotIndex < uBindCount; ++uSlotIndex)
					{
						const TextureDX11& InputTex = Textures.pRes->GetTexture(uSlotIndex);

						//const TextureDX11& Tex(InputTex ? InputTex : TextureDX11(DefaultInit));

						if (InputTex == nullptr)
						{
							HERROR("Shader %s requires texture [%s], but non is provided", CSTR(m_pRenderDevice->m_pSelectedShader->GetName()), CSTR(Textures.pRes->GetName()));
							bSuccess = false;
							break;
						}

						ID3D11ShaderResourceView* pSRV = InputTex.GetShaderResourceView();
						if (pSRV == nullptr)
						{
							HFATALD("Invalid (nullptr) ShaderResourceView!");
							bSuccess = false;
							break;
						}

						const uint32_t uSlot = Textures.uSlot + uSlotIndex; // start slot for arrays

						if (m_pRenderDevice->m_pCurrentShaderResources[i][uSlot] != pSRV)
						{

							// check if textures is bound as rendertarget
#ifdef _DEBUG
							if (InputTex.GetRenderTargetView() != nullptr)
							{
								ID3D11RenderTargetView* pRTV = InputTex.GetRenderTargetView();

								for (uint32_t t = 0; t < D3D11_SIMULTANEOUS_RENDER_TARGET_COUNT; ++t)
								{
									HASSERT(m_pRenderDevice->m_pCurrentRenderTargets[t] != pRTV, "%s is already bound as RenderTarget!", CSTR(Textures.pRes->GetName()));
								}
							}
#endif // _DEBUG

							m_pRenderDevice->m_pCurrentShaderResources[i][uSlot] = pSRV;
							m_pRenderDevice->m_UpdatedShaderResources[i].Add(uSlot);
							m_pRenderDevice->m_PerPassUpdatedSRVs[i].Add(uSlot);
						}
					}
				}
			}

			return bSuccess;
			// dont apply SRVs just now because buffers might set them too!
		}
		//---------------------------------------------------------------------------------------------------
		inline void RenderDeviceDrawDX11::SelectSamplers()
		{
			HASSERTD(m_pRenderDevice->m_pSelectedShader != nullptr, "No shader selected!");

			ShaderInterfaceDX11* pInterface = m_pRenderDevice->m_pSelectedShader->GetInterface();
			for (uint32_t i = 0; i < ShaderType_NumOf; ++i)
			{
				const TSelectedSamplers& SelectedSamplers = pInterface->GetSelectedSamplers(static_cast<ShaderType>(i));
				const size_t uSize = SelectedSamplers.size();

				// get selected states
				for (size_t s = 0; s < uSize; ++s)
				{
					const Selected<ShaderSamplerDX11*>& Sampler = SelectedSamplers[s];
					const SamplerStateDX11& SamplerState = Sampler.pRes->GetSampler();

					uint32_t uSlot = Sampler.uSlot;

					HASSERTD(uSlot != HUNDEFINED32, "Invalid slot!");

					if (SamplerState.operator bool() == false)
					{
						HERROR("Sampler %s has not been assinged", CSTR(Sampler.pRes->GetName()));
						continue;
					}

					if (m_pRenderDevice->m_pCurrentSamplerStates[i][uSlot] != SamplerState.GetReference())
					{
						m_pRenderDevice->m_pCurrentSamplerStates[i][uSlot] = SamplerState.GetReference();
						m_pRenderDevice->m_UpdatedSamplerStates[i].Add(uSlot);
					}
				}

				// set selected states
				UpdateIndex& Index = m_pRenderDevice->m_UpdatedSamplerStates[i];
				uint32_t uStartSlot = Index.GetStartIndex();
				uint32_t uCount = Index.GetCount();

				if (Index)
				{
					switch (i)
					{
					case ShaderType_VertexShader:
						m_pRenderDevice->m_pDeviceContext->VSSetSamplers(uStartSlot, uCount, &m_pRenderDevice->m_pCurrentSamplerStates[i][uStartSlot]);
						break;
					case ShaderType_HullShader:
						m_pRenderDevice->m_pDeviceContext->HSSetSamplers(uStartSlot, uCount, &m_pRenderDevice->m_pCurrentSamplerStates[i][uStartSlot]);
						break;
					case ShaderType_DomainShader:
						m_pRenderDevice->m_pDeviceContext->DSSetSamplers(uStartSlot, uCount, &m_pRenderDevice->m_pCurrentSamplerStates[i][uStartSlot]);
						break;
					case ShaderType_GeometryShader:
						m_pRenderDevice->m_pDeviceContext->GSSetSamplers(uStartSlot, uCount, &m_pRenderDevice->m_pCurrentSamplerStates[i][uStartSlot]);
						break;
					case ShaderType_PixelShader:
						m_pRenderDevice->m_pDeviceContext->PSSetSamplers(uStartSlot, uCount, &m_pRenderDevice->m_pCurrentSamplerStates[i][uStartSlot]);
						break;
					case ShaderType_ComputeShader:
						m_pRenderDevice->m_pDeviceContext->CSSetSamplers(uStartSlot, uCount, &m_pRenderDevice->m_pCurrentSamplerStates[i][uStartSlot]);
						break;
					default:
						break;
					}
				}

				// reset start & end slot
				Index.Reset();
			}
		}
		//---------------------------------------------------------------------------------------------------
		inline void RenderDeviceDrawDX11::SelectInputLayout()
		{
			HASSERTD(m_pRenderDevice->m_pSelectedShader != nullptr, "No shader selected!");

			if (m_pRenderDevice->m_CurrentInputLayout.pInputLayout != m_pRenderDevice->m_pSelectedShader->GetInputLayout().pInputLayout)
			{
				m_pRenderDevice->m_CurrentInputLayout = m_pRenderDevice->m_pSelectedShader->GetInputLayout();
				m_pRenderDevice->m_pDeviceContext->IASetInputLayout(m_pRenderDevice->m_CurrentInputLayout.pInputLayout);
			}
		}
		//---------------------------------------------------------------------------------------------------
		inline void RenderDeviceDrawDX11::SetVertexBuffer(const GPUBufferDX11& _Buffer, const uint32_t _uStrideSize, const uint32_t _uSlot, const uint32_t _uOffset)
		{
			ID3D11Buffer* pBuffer = _Buffer.GetBuffer();

			// TODO: check if slot this slot corresponds to vertex stream, if yes we need to introduce a 
			// new slot for m_pCurrentVertexBuffers housekeeping since clusters refere to the same vertexbuffer with different slots

			if (m_pRenderDevice->m_pCurrentVertexBuffers[_uSlot] != pBuffer || m_pRenderDevice->m_uCurrentVertexOffsets[_uSlot] != _uOffset)
			{
				m_pRenderDevice->m_pCurrentVertexBuffers[_uSlot] = pBuffer;
				// Pointer to an array of offset values; one offset value for each buffer in the vertex - buffer array.
				// Each offset is the number of bytes between the first element of a vertex buffer and the first element that will be used.
				m_pRenderDevice->m_uCurrentVertexOffsets[_uSlot] = _uOffset;
				m_pRenderDevice->m_uCurrentVertexStrideSizes[_uSlot] = _uStrideSize;

				m_pRenderDevice->m_UpdatedVertexBuffers.Add(_uSlot);
			}
		}
		//---------------------------------------------------------------------------------------------------
		inline void RenderDeviceDrawDX11::SetIndexBuffer(const GPUBufferDX11& _Buffer, PixelFormat kIndexFormat, uint32_t _uOffset)
		{
			ID3D11Buffer* pBuffer = _Buffer.GetBuffer();

			if (m_pRenderDevice->m_pCurrentIndexBuffer != pBuffer || m_pRenderDevice->m_uCurrentIndexOffset != _uOffset)
			{
				m_pRenderDevice->m_pCurrentIndexBuffer = pBuffer;

				//Offset(in bytes) from the start of the index buffer to the first index to use.
				m_pRenderDevice->m_uCurrentIndexOffset = _uOffset;
				m_pRenderDevice->m_kIndexBufferFormat = static_cast<DXGI_FORMAT>(kIndexFormat);

				m_pRenderDevice->m_bIndexBufferUpdated = true;
			}
		}

		//---------------------------------------------------------------------------------------------------
		inline void RenderDeviceDrawDX11::DrawRenderObject(const RenderObjectDX11& _RenderObject)
		{
			if (Select() == false)
			{
				HERROR("SelectTextures failed for object %s", CSTR(_RenderObject.GetName()));
			}

			ApplyShaderResources();

			m_pRenderDevice->SetPrimitiveTopology(_RenderObject.GetPrimitiveTopology());
			m_pRenderDevice->m_pDeviceContext->Draw(_RenderObject.GetVertexCount(), 0);
		}

		//---------------------------------------------------------------------------------------------------
		inline void RenderDeviceDrawDX11::DrawSubMesh(const SubMesh& _SubMesh, const GPUBufferDX11* _pVertexBuffer, const GPUBufferDX11* pIndexBuffer, const std::string& _sObjectName)
		{
			if (Select() == false)
			{
				HERROR("SelectTextures failed for object %s", CSTR(_sObjectName));
			}

			ApplyShaderResources();

			// check if vertex layouts are compatible
			if (_SubMesh.m_kVertexDeclaration ^ m_pRenderDevice->m_CurrentInputLayout.kDeclaration)
			{
				// just display error for now, select other IO permutation in future if possible or switch mesh info
				HERROR("Mesh %s vertex layout [%u] does not match shader input layout [%u]!",
					CSTR(_SubMesh.m_sSubName), _SubMesh.m_kVertexDeclaration.GetFlag(), m_pRenderDevice->m_CurrentInputLayout.kDeclaration.GetFlag());

				return;
			}

			// set primitive topology
			m_pRenderDevice->SetPrimitiveTopology(_SubMesh.m_kPrimitiveTopology);

			// upload vertex buffer with offset;
			SetVertexBuffer(*_pVertexBuffer, _SubMesh.m_uVertexStrideSize, 0, _SubMesh.m_uVertexByteOffset);
			ApplyVertexBuffers();

			// Draw Indexed
			if (_SubMesh.m_uIndexCount > 0)
			{
				// upload complete indexbuffer without offset
				if (pIndexBuffer == nullptr)
				{
					HERROR("Mesh %s requires indices, but no index buffer was provided!", CSTR(_SubMesh.m_sSubName));
					return;
				}

				SetIndexBuffer(*pIndexBuffer, _SubMesh.m_kIndexBufferFormat, 0u);
				ApplyIndexBuffer();

				//	[in]  UINT IndexCount,			=> Number of indices to draw.
				//	[in]  UINT StartIndexLocation,	=> The location of the first index read by the GPU from the index buffer.
				//	[in]  INT BaseVertexLocation	=> A value added to each index before reading a vertex from the vertex buffer.

				// convert byte offset to start location/index
				m_pRenderDevice->m_pDeviceContext->DrawIndexed(_SubMesh.m_uIndexCount, _SubMesh.m_uIndexByteOffset / _SubMesh.m_uIndexElementSize, 0);
			}
			else // Draw Non-Indexed
			{
				//	[in]  UINT VertexCount,			=> Number of vertices to draw.
				//	[in]  UINT StartVertexLocation	=> Index of the first vertex, which is usually an offset in a vertex buffer; it could also be used as the first vertex id generated for a shader parameter marked with the SV_TargetId system-value semantic.
				m_pRenderDevice->m_pDeviceContext->Draw(_SubMesh.m_uVertexCount, 0u /*_SubMesh.m_uVertexByteOffset / _SubMesh.m_uVertexStrideSize*/);
			}
		}
	} // Display
} // Helix

#endif // !RENDERDEVICEDRAWDX11_H
