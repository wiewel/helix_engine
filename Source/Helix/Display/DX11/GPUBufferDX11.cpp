//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "GPUBufferDX11.h"
#include "RenderDeviceDX11.h"
#include "ViewDefinesDX11.h"

using namespace Helix::Display;

//---------------------------------------------------------------------------------------------------

GPUBufferDX11::GPUBufferDX11(const std::string& _sDbgName) :
	m_sDbgName(_sDbgName)
{
}
//---------------------------------------------------------------------------------------------------

GPUBufferDX11::~GPUBufferDX11()
{
	HSAFE_RELEASE(m_pShaderResourceView);
	HSAFE_RELEASE(m_pUnorderedAccessView);
	HSAFE_RELEASE(m_pBuffer);
}
//---------------------------------------------------------------------------------------------------

bool GPUBufferDX11::Initialize(const BufferDesc& _BufferDesc)
{
	m_Description = _BufferDesc;

	if (m_bInitialized)
	{
		HWARNING("Buffer %s already initialized", CSTR(m_sDbgName));
		return true;
	}

	//get render device if needed
	if (m_pRenderDevice == nullptr)
	{
		m_pRenderDevice = RenderDeviceDX11::Instance();
		HASSERT(m_pRenderDevice->IsInitialized(), "RenderDevice has not been initialized yet!");
	}

	HRESULT Result = S_OK;

	D3D11_BUFFER_DESC DX11BufferDesc = ConvertToD3D11Description(_BufferDesc);
	D3D11_SUBRESOURCE_DATA SubResouce = { _BufferDesc.SubresourceData.pData, _BufferDesc.SubresourceData.uRowPitch, _BufferDesc.SubresourceData.uDepthPitch };

	if (SubResouce.pSysMem != nullptr && SubResouce.SysMemPitch != DX11BufferDesc.ByteWidth)
	{
		HERROR("SubResource size %u does not match %s Buffer size %u", SubResouce.SysMemPitch, CSTR(m_sDbgName), DX11BufferDesc.ByteWidth);
		return false;
	}

	m_pBuffer = nullptr;
	m_pUnorderedAccessView = nullptr;
	m_pShaderResourceView = nullptr;

	HRERROR(Result = m_pRenderDevice->GetDevice()->CreateBuffer(&DX11BufferDesc, SubResouce.pSysMem == nullptr ? nullptr : &SubResouce, &m_pBuffer));
	if (FAILED(Result))
	{
		return false;
	}

	if (_BufferDesc.kBindFlag & ResourceBindFlag_ShaderResource)
	{
		D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc = {};
		srvDesc.Format = DXGI_FORMAT_UNKNOWN;

		switch (_BufferDesc.kUAVFlag)
		{
		case BufferUAVFlag_Raw:
			srvDesc.ViewDimension = D3D11_SRV_DIMENSION_BUFFEREX;
			srvDesc.Format = DXGI_FORMAT_R32_TYPELESS;
			srvDesc.BufferEx.FirstElement = 0;
			srvDesc.BufferEx.NumElements = DX11BufferDesc.ByteWidth / sizeof(uint32_t);
			srvDesc.BufferEx.Flags = D3D11_BUFFEREX_SRV_FLAG_RAW;
			break;
		case BufferUAVFlag_Counter:
		case BufferUAVFlag_Append:
			srvDesc.Format = DXGI_FORMAT_UNKNOWN;
		default:
			srvDesc.ViewDimension = D3D11_SRV_DIMENSION_BUFFER;
			srvDesc.Buffer.ElementWidth = DX11BufferDesc.StructureByteStride;
			srvDesc.Buffer.FirstElement = 0;
			srvDesc.Buffer.NumElements = DX11BufferDesc.ByteWidth / DX11BufferDesc.StructureByteStride;
			break;
		}

		HASSERT(DX11BufferDesc.ByteWidth % DX11BufferDesc.StructureByteStride == 0u, "%s Buffer size is not a multiple of the structure byte stride", CSTR(m_sDbgName));
		HRERROR(Result = m_pRenderDevice->GetDevice()->CreateShaderResourceView(m_pBuffer, &srvDesc, &m_pShaderResourceView));
	}

	if (FAILED(Result))
	{
		return false;
	}

	if (_BufferDesc.kBindFlag & ResourceBindFlag_UnorderedAccess)
	{
		D3D11_UNORDERED_ACCESS_VIEW_DESC uavDesc = {};
		uavDesc.Format = DXGI_FORMAT_UNKNOWN;
		uavDesc.ViewDimension = D3D11_UAV_DIMENSION_BUFFER;
		uavDesc.Buffer.NumElements = DX11BufferDesc.ByteWidth / DX11BufferDesc.StructureByteStride;
		uavDesc.Buffer.FirstElement = 0;
		uavDesc.Buffer.Flags = static_cast<D3D11_BUFFER_UAV_FLAG>(_BufferDesc.kUAVFlag); 

		//D3D11_BUFFER_UAV_FLAG_RAW
		//Resource contains raw, unstructured data.Requires the UAV format to be DXGI_FORMAT_R32_TYPELESS.

		//D3D11_BUFFER_UAV_FLAG_APPEND
		//Allow data to be appended to the end of the buffer.D3D11_BUFFER_UAV_FLAG_APPEND flag must also be used for any view that will be used as a AppendStructuredBuffer or a ConsumeStructuredBuffer.Requires the UAV format to be DXGI_FORMAT_UNKNOWN.

		//D3D11_BUFFER_UAV_FLAG_COUNTER
		//Adds a counter to the unordered - access - view buffer.D3D11_BUFFER_UAV_FLAG_COUNTER can only be used on a UAV that is a RWStructuredBuffer and it enables the functionality needed for the IncrementCounter and DecrementCounter methods in HLSL.Requires the UAV format to be DXGI_FORMAT_UNKNOWN.

		switch (_BufferDesc.kUAVFlag)
		{
		case BufferUAVFlag_Raw:
			uavDesc.Format = DXGI_FORMAT_R32_TYPELESS;
			uavDesc.Buffer.NumElements = DX11BufferDesc.ByteWidth / sizeof(uint32_t);
			break;
		case BufferUAVFlag_Counter:
		case BufferUAVFlag_Append:
			uavDesc.Format = DXGI_FORMAT_UNKNOWN;
		default:
			uavDesc.Buffer.NumElements = DX11BufferDesc.ByteWidth / DX11BufferDesc.StructureByteStride;
			break;
		}

		HASSERT(DX11BufferDesc.ByteWidth % DX11BufferDesc.StructureByteStride == 0u, "%s Buffer size is not a multiple of the structure byte stride", CSTR(m_sDbgName));
		HRERROR(Result = m_pRenderDevice->GetDevice()->CreateUnorderedAccessView(m_pBuffer, &uavDesc, &m_pUnorderedAccessView));
	}

	m_bInitialized = Result == S_OK;

	if (m_bInitialized)
	{
		HDXDEBUGNAME(m_pBuffer, m_sDbgName.c_str());
		HDXDEBUGNAME(m_pShaderResourceView, "%sSRV", m_sDbgName.c_str());
		HDXDEBUGNAME(m_pUnorderedAccessView, "%sUAV", m_sDbgName.c_str());
	}

	return m_bInitialized;
}
//---------------------------------------------------------------------------------------------------

bool GPUBufferDX11::Apply(const uint8_t* _pData, uint32_t _uSize, uint32_t _uOffset)
{
	HASSERT(_uSize > 0u, "Invalid input data size!");
	HASSERT(m_bInitialized, "Uninitialized Buffer!");
	HASSERT(_pData != nullptr, "Invalid Data!");

	if ((m_Description.kCPUAccessFlag & CPUAccessFlag_Write) == false)
	{
		HERROR("Buffer has no CPU access");
		return false;
	}

	ResourceMapType kMapType = ResourceMapType_Write;

	if (m_Description.kBindFlag & ResourceBindFlag_IndexBuffer || m_Description.kBindFlag & ResourceBindFlag_VertexBuffer)
	{
		if (m_bSubsequentUpdate)
		{
			kMapType = ResourceMapType_Write_No_Overwrite;
			//TODO: validate offset + length for no overwrite
		}
		else
		{
			//discard init data
			m_bSubsequentUpdate = true;
			kMapType = ResourceMapType_Write_Discard;
		}
	}
	else if (m_Description.kBindFlag & ResourceBindFlag_ConstantBuffer)
	{
		//discard complete content
		kMapType = ResourceMapType_Write_Discard;
	}

	D3D11_MAPPED_SUBRESOURCE mappedResource;

	//Default write
	if (S_OK == m_pRenderDevice->GetContext()->Map(m_pBuffer, 0u, static_cast<D3D11_MAP>(kMapType), 0u, &mappedResource))
	{
		memcpy(reinterpret_cast<uint8_t*>(mappedResource.pData) + _uOffset, _pData /*+ _uOffset*/, _uSize);
		m_pRenderDevice->GetContext()->Unmap(m_pBuffer, 0u);
		return true;
	}

	return false;
}
//---------------------------------------------------------------------------------------------------
