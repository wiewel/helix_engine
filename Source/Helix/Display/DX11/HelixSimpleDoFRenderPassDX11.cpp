//   Copyright 2020 Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "HelixSimpleDoFRenderPassDX11.h"
#include "Scene/VolumeCache.h"

using namespace Helix;
using namespace Helix::Display;
using namespace Helix::Scene;
using namespace Helix::Datastructures;

//---------------------------------------------------------------------------------------------------

HelixSimpleDoFRenderPassDX11::HelixSimpleDoFRenderPassDX11(DefaultInitializerType) : RenderPassDX11(DefaultInit, "SimpleDoF")
{
}

//---------------------------------------------------------------------------------------------------
HelixSimpleDoFRenderPassDX11::HelixSimpleDoFRenderPassDX11(const std::string& _sInstanceName) :
	RenderPassDX11("SimpleDoF", _sInstanceName,
	{ &m_SceneBlurredTexture, &m_DepthTexture, &m_SceneTexture}, // Textures
	{ &m_DoFTarget }, // RenderTargets
	{ &m_CBPerCamera, &m_CBSimpleDoF }) // Variables
{
}
//---------------------------------------------------------------------------------------------------
HelixSimpleDoFRenderPassDX11::~HelixSimpleDoFRenderPassDX11()
{
}
//---------------------------------------------------------------------------------------------------
std::shared_ptr<RenderPassDX11> HelixSimpleDoFRenderPassDX11::CreatePass(const std::string& _sInstanceName) const
{
	return std::make_shared<HelixSimpleDoFRenderPassDX11>(_sInstanceName);
}
//---------------------------------------------------------------------------------------------------
bool HelixSimpleDoFRenderPassDX11::OnInitalize(const hlx::TextToken& _CustomVariables)
{
	// Add RasterizerState
	RasterizerDesc RasDesc = {};
	RasDesc.m_kFillMode = FillMode_Solid;
	RasDesc.m_kCullMode = CullMode_None;
	RasDesc.m_bFrontCounterClockwise = false;

	SetRasterizerState(RasDesc);

	// Add BlendState
	RenderTargetBlendDesc RTBlendDesc = {};
	RTBlendDesc.m_bBlendEnable = false;
	RTBlendDesc.m_uRenderTargetWriteMask = Channel_All;

	BlendDesc AlphaBlendDesc = {};
	AlphaBlendDesc.m_bAlphaToCoverageEnable = false;
	AlphaBlendDesc.m_RenderTargets[0] = RTBlendDesc;
	AlphaBlendDesc.m_uSampleMask = 0xffffffff;
	AlphaBlendDesc.fBlendFactor = { 0.0f,0.0f,0.0f,0.0f };

	SetBlendState(AlphaBlendDesc);

	// Add DepthStencilState
	DepthStencilDesc DepthDesc = {};
	DepthDesc.m_bDepthEnable = false;
	DepthDesc.m_kDepthWriteMask = DepthWriteMask_Zero;
	DepthDesc.m_kDepthFunc = ComparisonFunction_Less;
	DepthDesc.m_bStencilEnable = false;

	SetDepthStencilState(DepthDesc, "SimpleDoF");

	// Default Parameter
	ClearRenderTargets(false);

	return m_bInitialized;
}
//---------------------------------------------------------------------------------------------------
bool HelixSimpleDoFRenderPassDX11::OnPerCamera(const CameraComponent& _Camera)
{
	SetCameraConstants(m_CBPerCamera, _Camera.GetRenderingProperties());

	RaycastHitInfo Hit;
	if(_Camera.Raycast(_Camera.GetPosition(), _Camera.GetLook(), _Camera.GetFarDistance(), &Hit) != nullptr)
	{
		m_CBSimpleDoF->fFocalPlane = Math::Clamp<float>(_Camera.GetPosition().distance(Hit.vPosition) - _Camera.GetNearDistance(), 0.0f, _Camera.GetFarDistance());
	}

	return true;
}
//---------------------------------------------------------------------------------------------------
