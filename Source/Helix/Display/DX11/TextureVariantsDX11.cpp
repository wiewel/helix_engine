//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "TextureVariantsDX11.h"
#include "TexturePoolDX11.h"
#include "ViewDefinesDX11.h"
#include "RenderDeviceDX11.h"

using namespace Helix::Display;

//---------------------------------------------------------------------------------------------------
// RenderTarget
//---------------------------------------------------------------------------------------------------
RenderTargetTextureDX11::RenderTargetTextureDX11(
	uint32_t _uWidth,
	uint32_t _uHeight,
	const std::string& _sName,
	PixelFormat _kPixelFormat,
	bool _bShaderResource,
	bool _bFullMipChain,
	uint32_t _uArraySize)
{
	TextureDesc Desc = {};
	Desc.m_sFilePathOrName = _sName;
	Desc.m_bLoadFromFile = false;
	Desc.m_bPooled = false;
	Desc.m_kBindFlag = ResourceBindFlag_RenderTarget;

	if (_bShaderResource)
	{
		Desc.m_kBindFlag |= ResourceBindFlag_ShaderResource;
	}

	Desc.m_kUsageFlag = ResourceUsageFlag_Default;
	Desc.m_kCPUAccessFlag = CPUAccessFlag_None; // ResourceUsageFlag_Default
	Desc.m_kResourceDimension = _uArraySize == 1u ? ResourceDimension_Texture2D : ResourceDimension_Texture2DArray;
	Desc.m_uArraySize = _uArraySize;
	Desc.m_uSampleCount = 1u;

	if (_bFullMipChain)
	{
		Desc.m_uMipLevels = 0u;
		Desc.m_kResourceFlag = MiscResourceFlag_GenerateMips;
	}
	else
	{
		Desc.m_uMipLevels = 1u;
	}

	Desc.m_uHeight = _uHeight;
	Desc.m_uWidth = _uWidth;
	Desc.m_kFormat = _kPixelFormat;

	m_pReference = TexturePoolDX11::Instance()->Get(Desc, m_Hash, false);
	if (m_pReference != nullptr)
	{
		m_pReference->m_bIsRWTexture = false;
		m_pReference->m_Viewport.fHeight = static_cast<float>(_uHeight);
		m_pReference->m_Viewport.fWidth = static_cast<float>(_uWidth);
	}
	SetDebugName(_sName);
}
//---------------------------------------------------------------------------------------------------
RenderTargetTextureDX11::RenderTargetTextureDX11(
	uint32_t _uWidth,
	const std::string& _sName,
	PixelFormat _kPixelFormat,
	bool _bShaderResource,
	uint32_t _uHeight,
	uint32_t _uDepth,
	uint32_t _uArraySize)
{
	TextureDesc Desc = {};
	Desc.m_sFilePathOrName = _sName;
	Desc.m_bLoadFromFile = false;
	Desc.m_bPooled = false;
	Desc.m_kBindFlag = ResourceBindFlag_UnorderedAccess;

	if (_bShaderResource)
	{
		Desc.m_kBindFlag |= ResourceBindFlag_ShaderResource;
	}

	Desc.m_kUsageFlag = ResourceUsageFlag_Default;
	Desc.m_kCPUAccessFlag = CPUAccessFlag_None;

	if (_uWidth != 0u && _uHeight != 0u && _uDepth != 0u)
	{
		Desc.m_kResourceDimension = ResourceDimension_Texture3D;
	}
	else if (_uWidth != 0u && _uHeight != 0u && _uDepth == 0u)
	{
		Desc.m_kResourceDimension = _uArraySize == 1u ? ResourceDimension_Texture2D : ResourceDimension_Texture2DArray;
	}
	else if (_uWidth != 0u && _uHeight == 0u && _uDepth == 0u)
	{
		Desc.m_kResourceDimension = _uArraySize == 1u ? ResourceDimension_Texture1D : ResourceDimension_Texture1DArray;
	}

	Desc.m_uArraySize = _uArraySize;
	Desc.m_uSampleCount = 1u;
	Desc.m_uMipLevels = 1u;

	Desc.m_uHeight = _uHeight;
	Desc.m_uWidth = _uWidth;
	Desc.m_uDepth = _uDepth;
	Desc.m_kFormat = _kPixelFormat;

	//https://msdn.microsoft.com/en-us/library/ff728749(v=vs.85).aspx
	switch (Desc.m_kFormat)
	{
		// supported formats
	case PixelFormat_R10G10B10A2_Typeless:
	case PixelFormat_R10G10B10A2_UNorm:
	case PixelFormat_R10G10B10A2_UInt:
	case PixelFormat_R8G8B8A8_Typeless:
	case PixelFormat_R8G8B8A8_UNorm:
	case PixelFormat_R8G8B8A8_UNorm_SRgb:
	case PixelFormat_R8G8B8A8_UInt:
	case PixelFormat_R8G8B8A8_SNorm:
	case PixelFormat_R8G8B8A8_SInt:
	case PixelFormat_R16G16_Typeless:
	case PixelFormat_R16G16_Float:
	case PixelFormat_R16G16_UNorm:
	case PixelFormat_R16G16_UInt :
	case PixelFormat_R16G16_SNorm:
	case PixelFormat_R16G16_SInt :
	case PixelFormat_R32_Typeless:
	//case PixelFormat_D32_Float:
	case PixelFormat_R32_Float:
	case PixelFormat_R32_UInt :
	case PixelFormat_R32_SInt :
	case PixelFormat_B8G8R8A8_UNorm:
	case PixelFormat_B8G8R8A8_Typeless:
	case PixelFormat_B8G8R8A8_UNorm_SRgb:
	case PixelFormat_B8G8R8X8_UNorm: 
	case PixelFormat_B8G8R8X8_Typeless: 
	case PixelFormat_B8G8R8X8_UNorm_SRgb:
		break;
	default:
		HERROR("Unsupported RWTexture Format %u (can not be converted to 32bit uint)", Desc.m_kFormat);
		break;
	}

	m_pReference = TexturePoolDX11::Instance()->Get(Desc, m_Hash, false);
	if (m_pReference != nullptr)
	{
		m_pReference->m_bIsRWTexture = true;
		//m_pReference->m_Viewport.fHeight = static_cast<float>(_uHeight);
		//m_pReference->m_Viewport.fWidth = static_cast<float>(_uWidth);
	}
	SetDebugName(_sName);
}
//---------------------------------------------------------------------------------------------------
RenderTargetTextureDX11::~RenderTargetTextureDX11()
{
}
//---------------------------------------------------------------------------------------------------
RenderTargetTextureDX11::RenderTargetTextureDX11(const RenderTargetTextureDX11& _Other) : TextureDX11(_Other)
{
}
//---------------------------------------------------------------------------------------------------
RenderTargetTextureDX11::RenderTargetTextureDX11(const TextureDX11& _Other)
{
	operator=(_Other);
}
//---------------------------------------------------------------------------------------------------
RenderTargetTextureDX11& RenderTargetTextureDX11::operator=(const RenderTargetTextureDX11& _Other)
{
	if (this == &_Other)
		return *this;

	TextureDX11::operator=(_Other);

	return *this;
}
//---------------------------------------------------------------------------------------------------
RenderTargetTextureDX11& RenderTargetTextureDX11::operator=(const TextureDX11& _Other)
{
	if (this == &_Other)
		return *this;

	if (_Other.GetTextureType() == TextureType_RenderTargetTexture)
	{
		RenderTargetTextureDX11::operator=(static_cast<const RenderTargetTextureDX11&>(_Other));
	}
	else
	{
		HFATAL("%s is not a RenderTargetTexture", CSTR(_Other.GetDescription().m_sFilePathOrName));
		TextureDX11::operator=(nullptr);
	}

	return *this;
}
//---------------------------------------------------------------------------------------------------
void RenderTargetTextureDX11::GenerateMips()
{
	HASSERTD(m_pReference->m_pResourceView != nullptr, "No shader resource view is available in call to GenerateMips!");
	RenderDeviceDX11::Instance()->GetContext()->GenerateMips(m_pReference->m_pResourceView);
}
//---------------------------------------------------------------------------------------------------
void RenderTargetTextureDX11::Clear(const Helix::Math::uint4* _pUAVUIntClearData)
{
	if (m_pReference->m_bIsRWTexture)
	{
		if (_pUAVUIntClearData != nullptr)
		{
			RenderDeviceDX11::Instance()->GetContext()->ClearUnorderedAccessViewUint(m_pReference->m_pUnorderedAccessView, _pUAVUIntClearData->Data);
		}
		else
		{
			RenderDeviceDX11::Instance()->GetContext()->ClearUnorderedAccessViewFloat(m_pReference->m_pUnorderedAccessView, &m_pReference->m_vClearColor[0]);
		}
	}
	else
	{
		RenderDeviceDX11::Instance()->GetContext()->ClearRenderTargetView(m_pReference->m_pRenderTargetView, &m_pReference->m_vClearColor[0]);
	}
}
//---------------------------------------------------------------------------------------------------
void RenderTargetTextureDX11::OnResize(uint32_t _uWidth, uint32_t _uHeight, uint32_t _uDepth)
{
	m_pReference->m_Viewport.fHeight = static_cast<float>(_uHeight);
	m_pReference->m_Viewport.fWidth = static_cast<float>(_uWidth);
}
//---------------------------------------------------------------------------------------------------
// DepthStencil
//---------------------------------------------------------------------------------------------------
DepthStencilTextureDX11::DepthStencilTextureDX11(
	uint32_t _uWidth,
	uint32_t _uHeight,
	const std::string& _sName,
	PixelFormat _kTypelessPixelFormat,
	bool _bShaderResource,
	uint32_t _uArraySize)
{
	TextureDesc Desc = {};
	Desc.m_bPooled = false;
	Desc.m_sFilePathOrName = _sName;
	Desc.m_bLoadFromFile = false;
	Desc.m_kBindFlag = static_cast<ResourceBindFlag>(_bShaderResource ? ResourceBindFlag_DepthStencil | ResourceBindFlag_ShaderResource : ResourceBindFlag_DepthStencil);
	Desc.m_kUsageFlag = ResourceUsageFlag_Default;
	Desc.m_kCPUAccessFlag = CPUAccessFlag_None; // ResourceUsageFlag_Default
	Desc.m_kResourceDimension = _uArraySize == 1 ? ResourceDimension_Texture2D : ResourceDimension_Texture2DArray;
	Desc.m_uArraySize = _uArraySize;
	Desc.m_uMipLevels = 1u;
	Desc.m_uSampleCount = 1u;

	Desc.m_uHeight = _uHeight;
	Desc.m_uWidth = _uWidth;
	Desc.m_kFormat = _kTypelessPixelFormat;

	m_pReference = TexturePoolDX11::Instance()->Get(Desc, m_Hash, false);

	if (m_pReference != nullptr)
	{
		m_pReference->m_Viewport.fHeight = static_cast<float>(_uHeight);
		m_pReference->m_Viewport.fWidth = static_cast<float>(_uWidth);
	}

	SetDebugName(_sName);
}
//---------------------------------------------------------------------------------------------------
DepthStencilTextureDX11::~DepthStencilTextureDX11()
{
}
//---------------------------------------------------------------------------------------------------
DepthStencilTextureDX11::DepthStencilTextureDX11(const DepthStencilTextureDX11& _Other) :
	TextureDX11(_Other)
{
}
//---------------------------------------------------------------------------------------------------
DepthStencilTextureDX11::DepthStencilTextureDX11(const TextureDX11& _Other)
{
	operator=(_Other);
}
//---------------------------------------------------------------------------------------------------
DepthStencilTextureDX11& DepthStencilTextureDX11::operator=(const DepthStencilTextureDX11& _Other)
{
	if (this == &_Other)
		return *this;

	TextureDX11::operator=(_Other);

	return *this;
}
//---------------------------------------------------------------------------------------------------
DepthStencilTextureDX11& DepthStencilTextureDX11::operator=(const TextureDX11& _Other)
{
	if (this == &_Other)
		return *this;

	if (_Other.GetTextureType() == TextureType_DepthStencilTexture)
	{
		DepthStencilTextureDX11::operator=(static_cast<const DepthStencilTextureDX11&>(_Other));
	}
	else
	{
		HFATAL("%s is not a DepthStencilTexture", CSTR(_Other.GetDescription().m_sFilePathOrName));
		TextureDX11::operator=(nullptr);
	}

	return *this;
}
//---------------------------------------------------------------------------------------------------
void DepthStencilTextureDX11::Clear()
{
	RenderDeviceDX11::Instance()->GetContext()->ClearDepthStencilView(m_pReference->m_pDepthStencilView, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, m_pReference->m_fClearDepth, m_pReference->m_uClearStencil);
}
//---------------------------------------------------------------------------------------------------
void DepthStencilTextureDX11::OnResize(uint32_t _uWidth, uint32_t _uHeight, uint32_t _uDepth)
{
	m_pReference->m_Viewport.fHeight = static_cast<float>(_uHeight);
	m_pReference->m_Viewport.fWidth = static_cast<float>(_uWidth);
}
//---------------------------------------------------------------------------------------------------
// BackBuffer
//---------------------------------------------------------------------------------------------------
BackBufferTextureDX11::~BackBufferTextureDX11()
{
}
//---------------------------------------------------------------------------------------------------
void BackBufferTextureDX11::Destroy()
{
	HASSERT(m_pReference != nullptr, "Backbuffer reference missing, you need to call Set(ID3D11Texture2D*)");
	if (m_pReference != nullptr)
	{
		m_pReference->Reset();
	}
}
//---------------------------------------------------------------------------------------------------
bool BackBufferTextureDX11::Set(ID3D11Texture2D* _pBackBuffer)
{
	D3D11_TEXTURE2D_DESC D3D11Desc = {};
	_pBackBuffer->GetDesc(&D3D11Desc);

	TextureDesc Desc = ConvertToHelixDescription(D3D11Desc);
	Desc.m_sFilePathOrName = "BackBuffer";
	Desc.m_bLoadFromFile = false;
	Desc.m_bPooled = false;
	Desc.m_bNoInit = true; // dont create views / resources

	if (m_pReference == nullptr)
	{
		// create new empty reference (NoInit)
		m_pReference = TexturePoolDX11::Instance()->Get(Desc, m_Hash);
	}
	else
	{
		HASSERT(m_pReference->m_pTexture2D == nullptr, "Backbuffer has not been destroyed correclty! (Texture still alive)");
		m_pReference->m_Description = Desc;
	}

	m_pReference->m_pTexture2D = _pBackBuffer;

	m_pReference->m_Viewport.fHeight = static_cast<float>(D3D11Desc.Height);
	m_pReference->m_Viewport.fWidth = static_cast<float>(D3D11Desc.Width);

	SetDebugName("BackBuffer");

	return CreateViews(*m_pReference);
}
//---------------------------------------------------------------------------------------------------
// ImmutableTextureDX11
//---------------------------------------------------------------------------------------------------
ImmutableTextureDX11::ImmutableTextureDX11(
	const std::string& _sFilePath,
	uint32_t _uMipLevels,
	bool _bForceSRgb,
	bool _bUseDefaultReferenceIfFailed)
{
	TextureDesc Desc = {};
	Desc.m_bPooled = true;
	Desc.m_sFilePathOrName = _sFilePath;
	Desc.m_bLoadFromFile = true;
	Desc.m_bForceSRgb = _bForceSRgb;
	Desc.m_kBindFlag =  ResourceBindFlag_ShaderResource;
	Desc.m_kUsageFlag = ResourceUsageFlag_Immutable;
	Desc.m_kCPUAccessFlag = CPUAccessFlag_None;
	Desc.m_kResourceDimension = ResourceDimension_Texture2D;
	Desc.m_uSampleCount = 1;

	Desc.m_uMipLevels = _uMipLevels;
	Desc.m_uArraySize = 1;

	m_pReference = TexturePoolDX11::Instance()->Get(Desc, m_Hash, _bUseDefaultReferenceIfFailed);
	SetDebugName(hlx::get_right_of(_sFilePath, "\\"));
}
//---------------------------------------------------------------------------------------------------
ImmutableTextureDX11::ImmutableTextureDX11(
	uint32_t _uWidth,
	uint32_t _uHeight,
	const std::string& _sName,
	const BufferSubresourceData& _InitialData,
	PixelFormat _kFormat,
	ResourceDimension _kResourceDimension,
	uint32_t _uMipLevels,
	bool _bForceSRgb)
{
	TextureDesc Desc = {};
	Desc.m_bPooled = true;
	Desc.m_sFilePathOrName = _sName;
	Desc.m_bLoadFromFile = false;
	Desc.m_bForceSRgb = _bForceSRgb;
	Desc.m_kBindFlag = ResourceBindFlag_ShaderResource;
	Desc.m_kUsageFlag = ResourceUsageFlag_Immutable;
	Desc.m_kCPUAccessFlag = CPUAccessFlag_None;
	Desc.m_kResourceDimension = _kResourceDimension;
	Desc.m_uSampleCount = 1;
	Desc.m_uWidth = _uWidth;
	Desc.m_uHeight = _uHeight;

	Desc.m_uMipLevels = _uMipLevels;
	Desc.m_uArraySize = 1;
	Desc.m_InitialData = _InitialData;
	Desc.m_kFormat = _kFormat;

	m_pReference = TexturePoolDX11::Instance()->Get(Desc, m_Hash, false);
	SetDebugName(hlx::get_right_of(_sName, "\\"));
}
//---------------------------------------------------------------------------------------------------
//Copy Constructor
ImmutableTextureDX11::ImmutableTextureDX11(const ImmutableTextureDX11& _Texture) :
	TextureDX11(_Texture)
{
}
//---------------------------------------------------------------------------------------------------
ImmutableTextureDX11::ImmutableTextureDX11(const TextureDX11& _Other)
{
	operator=(_Other);
}
//---------------------------------------------------------------------------------------------------
ImmutableTextureDX11& ImmutableTextureDX11::operator=(const ImmutableTextureDX11& _Other)
{
	if (this == &_Other)
		return *this;

	TextureDX11::operator=(_Other);
	return *this;
}
//---------------------------------------------------------------------------------------------------
ImmutableTextureDX11& ImmutableTextureDX11::operator=(const TextureDX11 & _Other)
{
	if (this == &_Other)
		return *this;

	if (_Other.GetTextureType() == TextureType_ImmutableTexture)
	{
		ImmutableTextureDX11::operator=(static_cast<const ImmutableTextureDX11&>(_Other));
	}
	else
	{
		HFATAL("%s is not ImmutableTexture", _Other.GetDescription().m_sFilePathOrName.c_str());
		TextureDX11::operator=(nullptr);
	}

	return *this;
}
//---------------------------------------------------------------------------------------------------
ImmutableTextureDX11::~ImmutableTextureDX11()
{
}
//---------------------------------------------------------------------------------------------------
DefaultTextureDX11::DefaultTextureDX11(
	const TextureDesc& _Desc,
	const std::string& _sName)
{
	TextureDesc Desc = _Desc;
	Desc.m_bPooled = false;
	Desc.m_sFilePathOrName = _sName;
	//Desc.m_bLoadFromFile = false;
	Desc.m_kBindFlag = ResourceBindFlag_ShaderResource;
	Desc.m_kUsageFlag = ResourceUsageFlag_Default;
	Desc.m_kCPUAccessFlag = CPUAccessFlag_None; // ResourceUsageFlag_Default

	m_pReference = TexturePoolDX11::Instance()->Get(Desc, m_Hash, false);
	SetDebugName(_sName);
}
//---------------------------------------------------------------------------------------------------
// TextureCubeDX11
//---------------------------------------------------------------------------------------------------
TextureCubeDX11::TextureCubeDX11(
	const std::string& _sFilePath,
	uint32_t _uMipLevels,
	bool _bForceSRgb,
	bool _bUseDefaultReferenceIfFailed)
{

	TextureDesc Desc = {};
	Desc.m_bPooled = true;
	Desc.m_sFilePathOrName = _sFilePath;
	Desc.m_bLoadFromFile = true;
	Desc.m_bForceSRgb = _bForceSRgb;
	Desc.m_kBindFlag = ResourceBindFlag_ShaderResource;
	Desc.m_kUsageFlag = ResourceUsageFlag_Immutable;
	Desc.m_kCPUAccessFlag = CPUAccessFlag_None;
	Desc.m_kResourceDimension = ResourceDimension_TextureCUBE;
	Desc.m_kResourceFlag = MiscResourceFlag_TextureCube;
	Desc.m_uSampleCount = 1;

	Desc.m_uMipLevels = _uMipLevels;
	Desc.m_uArraySize = 6;

	m_pReference = TexturePoolDX11::Instance()->Get(Desc, m_Hash, _bUseDefaultReferenceIfFailed);
	SetDebugName(hlx::get_right_of(_sFilePath, "\\"));
}
//---------------------------------------------------------------------------------------------------
TextureCubeDX11::~TextureCubeDX11()
{
}
//---------------------------------------------------------------------------------------------------
DefaultTextureDX11::DefaultTextureDX11(
	uint32_t _uWidth,
	uint32_t _uHeight,
	const std::string& _sName,
	PixelFormat _kPixelFormat)
{
	TextureDesc Desc = {};
	Desc.m_bPooled = false;
	Desc.m_uWidth = _uWidth;
	Desc.m_uHeight = _uHeight;
	Desc.m_sFilePathOrName = _sName;
	Desc.m_bLoadFromFile = false;
	Desc.m_kBindFlag = ResourceBindFlag_ShaderResource;
	Desc.m_kUsageFlag = ResourceUsageFlag_Default;
	Desc.m_kCPUAccessFlag = CPUAccessFlag_None; // ResourceUsageFlag_Default
	Desc.m_kResourceDimension = ResourceDimension_Texture2D;
	Desc.m_uSampleCount = 1;
	Desc.m_uArraySize = 1;
	Desc.m_kFormat = _kPixelFormat;

	m_pReference = TexturePoolDX11::Instance()->Get(Desc, m_Hash, false);
	SetDebugName(_sName);
}
//---------------------------------------------------------------------------------------------------
DefaultTextureDX11::~DefaultTextureDX11()
{
}
//---------------------------------------------------------------------------------------------------
DefaultTextureDX11::DefaultTextureDX11(const DefaultTextureDX11& _Texture) :
	TextureDX11(_Texture)
{
}
//---------------------------------------------------------------------------------------------------
DefaultTextureDX11::DefaultTextureDX11(const TextureDX11& _Other)
{
	operator=(_Other);
}
//---------------------------------------------------------------------------------------------------
DefaultTextureDX11& DefaultTextureDX11::operator=(const DefaultTextureDX11& _Other)
{
	if (this == &_Other)
		return *this;

	TextureDX11::operator=(_Other);
	return *this;
}
//---------------------------------------------------------------------------------------------------
DefaultTextureDX11& DefaultTextureDX11::operator=(const TextureDX11 & _Other)
{
	if (this == &_Other)
		return *this;

	if (_Other.GetTextureType() == TextureType_DefaultTexture)
	{
		DefaultTextureDX11::operator=(static_cast<const DefaultTextureDX11&>(_Other));
	}
	else
	{
		HFATAL("%s is not DefaultTexture", CSTR(_Other.GetDescription().m_sFilePathOrName));
		TextureDX11::operator=(nullptr);
	}

	return *this;
}
//---------------------------------------------------------------------------------------------------
bool DefaultTextureDX11::CopyPixelData(const TextureDX11& _Other)
{
	bool bResult = true;

	// Restrictions -> https://msdn.microsoft.com/en-us/library/windows/desktop/ff476392%28v=vs.85%29.aspx

	//Must be different resources.
	if (m_pReference->m_pResource == _Other.m_pReference->m_pResource)
	{
		bResult = false;
	}
	//Must be the same type.
	if (m_pReference->m_Description.m_kResourceDimension != _Other.m_pReference->m_Description.m_kResourceDimension)
	{
		bResult = false;
	}
	//Must have same multisample count and quality.
	if (m_pReference->m_Description.m_uSampleCount != _Other.m_pReference->m_Description.m_uSampleCount
		|| m_pReference->m_Description.m_uSampleQuality != _Other.m_pReference->m_Description.m_uSampleQuality)
	{
		bResult = false;
	}
	//Must have identical dimensions(including width, height, depth, and size as appropriate).
	if (m_pReference->m_Description.m_uWidth != _Other.m_pReference->m_Description.m_uWidth
		|| m_pReference->m_Description.m_uHeight != _Other.m_pReference->m_Description.m_uHeight
		|| m_pReference->m_Description.m_uDepth != _Other.m_pReference->m_Description.m_uDepth)
	{
		bResult = false;
	}
	//Must have compatible DXGI formats, which means the formats must be identical or at least from the same type group.
	//For example, a DXGI_FORMAT_R32G32B32_FLOAT texture can be copied to an DXGI_FORMAT_R32G32B32_UINT texture since both of these formats are in the DXGI_FORMAT_R32G32B32_TYPELESS group.
	//CopyResource can copy between a few format types.For more info, see Format Conversion using Direct3D 10.1.
	if (GetTypelessFormat(m_pReference->m_Description.m_kFormat) != GetTypelessFormat(_Other.m_pReference->m_Description.m_kFormat))
	{
		bResult = false;
	}

	//Can't be currently mapped.
	// TODO: find a way to check this

	if (bResult)
	{
		RenderDeviceDX11::Instance()->GetContext()->CopyResource(m_pReference->m_pResource, _Other.m_pReference->m_pResource);
	}

	return bResult;
}
//---------------------------------------------------------------------------------------------------