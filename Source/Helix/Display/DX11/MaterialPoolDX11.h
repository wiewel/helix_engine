//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef MATERIALPOOLDX11_H
#define MATERIALPOOLDX11_H

#include "DataStructures\TplHashPool.h"
#include "hlx\src\Singleton.h"
#include "TextureVariantsDX11.h"

namespace Helix
{
	namespace Display
	{
		struct MaterialDX11ReferenceInternal
		{
			ImmutableTextureDX11 AlbedoMap = nullptr;
			ImmutableTextureDX11 NormalMap = nullptr;
			ImmutableTextureDX11 MetallicMap = nullptr;
			ImmutableTextureDX11 RoughnessMap = nullptr;
			ImmutableTextureDX11 HeightMap = nullptr;
			ImmutableTextureDX11 EmissiveMap = nullptr;

			std::string sPathOrName = {};
		};

		class MaterialDX11;

		using TDX11MaterialPool = Datastructures::TplRefHashPool<uint64_t, MaterialDesc, MaterialDX11ReferenceInternal>;
		class MaterialPoolDX11 : public TDX11MaterialPool, public hlx::Singleton<MaterialPoolDX11>
		{
		public:
			HDEBUGNAME("TexturePoolDX11");

			MaterialPoolDX11();
			~MaterialPoolDX11();

			uint64_t HashFunction(const MaterialDesc& _Desc) final;

			static void SetDefaultRenderPasses(const uint64_t& _kDefaultPasses);
			static const uint64_t& GetDefaultRenderPasses();

			uint64_t GetDefaultKey() final;

		private:
			MaterialDX11ReferenceInternal* CreateFromDesc(const MaterialDesc& _Desc, const uint64_t& _Hash) final;

			static MaterialDX11 m_ErrorMaterial;

		private:
			static uint64_t m_kDefaultRenderPasses;
		};

		using TDX11MaterialRef = Datastructures::TplHashPoolReference<MaterialPoolDX11>;

		inline void MaterialPoolDX11::SetDefaultRenderPasses(const uint64_t& _kDefaultPasses)
		{
			m_kDefaultRenderPasses = _kDefaultPasses;
		}
		inline const uint64_t& MaterialPoolDX11::GetDefaultRenderPasses()
		{
			return m_kDefaultRenderPasses;
		}
	} // Display
} // Helix

#endif // MATERIALPOOLDX11_H