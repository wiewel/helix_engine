//   Copyright 2020 Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "HelixVerticalBlurRenderPassDX11.h"
#include "Math\MathFunctions.h"

using namespace Helix;
using namespace Helix::Display;
using namespace Helix::Scene;
using namespace Helix::Datastructures;
using namespace Helix::Math;

//---------------------------------------------------------------------------------------------------

HelixVerticalBlurRenderPassDX11::HelixVerticalBlurRenderPassDX11(DefaultInitializerType) : RenderPassDX11(DefaultInit, "VerticalBlur")
{
}

//---------------------------------------------------------------------------------------------------
HelixVerticalBlurRenderPassDX11::HelixVerticalBlurRenderPassDX11(const std::string& _sInstanceName) :
	RenderPassDX11("VerticalBlur", _sInstanceName,
	{ &m_InputTexture },
	{ &m_BlurOutput },
	{ &m_CBVerticalBlur/*, &m_CBPerCamera*/ })
{
}
//---------------------------------------------------------------------------------------------------
HelixVerticalBlurRenderPassDX11::~HelixVerticalBlurRenderPassDX11()
{
}
//---------------------------------------------------------------------------------------------------

std::shared_ptr<RenderPassDX11> HelixVerticalBlurRenderPassDX11::CreatePass(const std::string& _sInstanceName) const
{
	return std::make_shared<HelixVerticalBlurRenderPassDX11>(_sInstanceName);
}
//---------------------------------------------------------------------------------------------------
bool HelixVerticalBlurRenderPassDX11::OnInitalize(const hlx::TextToken& _CustomVariables)
{
	// Add RasterizerState
	RasterizerDesc RasDesc = {};
	RasDesc.m_kFillMode = FillMode_Solid;
	RasDesc.m_kCullMode = CullMode_None;
	RasDesc.m_bFrontCounterClockwise = false;

	SetRasterizerState(RasDesc);

	// Add BlendState
	RenderTargetBlendDesc RTBlendDesc = {};
	RTBlendDesc.m_bBlendEnable = false;
	RTBlendDesc.m_kSrcBlend = BlendVariable_Src_Alpha;
	RTBlendDesc.m_kDestBlend = BlendVariable_One;
	RTBlendDesc.m_kBlendOp = BlendOperation_Add;
	RTBlendDesc.m_kSrcBlendAlpha = BlendVariable_Zero;
	RTBlendDesc.m_kDestBlendAlpha = BlendVariable_Zero;
	RTBlendDesc.m_kBlendOpAlpha = BlendOperation_Add;
	RTBlendDesc.m_uRenderTargetWriteMask = Channel_All;

	BlendDesc AlphaBlendDesc = {};
	AlphaBlendDesc.m_bAlphaToCoverageEnable = false;
	AlphaBlendDesc.m_RenderTargets[0] = RTBlendDesc;
	AlphaBlendDesc.m_uSampleMask = 0xffffffff;
	AlphaBlendDesc.fBlendFactor = { 0.0f,0.0f,0.0f,0.0f };

	SetBlendState(AlphaBlendDesc);

	// Add DepthStencilState
	DepthStencilDesc DepthDesc = {};
	DepthDesc.m_bDepthEnable = false;
	DepthDesc.m_kDepthWriteMask = DepthWriteMask_Zero;
	DepthDesc.m_kDepthFunc = ComparisonFunction_Less;
	DepthDesc.m_bStencilEnable = false;

	SetDepthStencilState(DepthDesc, "VerticalBlur");

	// Default Parameter
	ClearRenderTargets(false);

	GenerateGauss(m_iBlurWidth, m_fSigma);
	
	return m_bInitialized;
}
//---------------------------------------------------------------------------------------------------
void HelixVerticalBlurRenderPassDX11::GenerateGauss(int32_t _iBlurWidth, float _fSigma)
{
	const int iBlurCenter = 7;
	if (_iBlurWidth < 0 || _iBlurWidth > 7)
	{
		HWARNING("Blur width of %d is not supported. Maximum is %d!", _iBlurWidth, iBlurCenter);
		_iBlurWidth = 7;
	}

	std::vector<float> Result;
	GaussianCurve((2 * _iBlurWidth) + 1, _fSigma, Result);

	for(int32_t i = -_iBlurWidth; i < _iBlurWidth; ++i)
	{
		int2 vIndex = GetGaussianWeightIndex(iBlurCenter + i);
		m_CBVerticalBlur->GaussianWeights[vIndex.x][vIndex.y] = Result.at(_iBlurWidth + i);
	}

	m_CBVerticalBlur->iBlurWidth = _iBlurWidth;
}
//---------------------------------------------------------------------------------------------------
bool HelixVerticalBlurRenderPassDX11::OnPerFrame()
{
	m_CBVerticalBlur->fInputTexDim = static_cast<float>(m_InputTexture.GetTexture().GetDescription().m_uHeight);
	m_CBVerticalBlur->fBlurIntensity = m_fBlurIntensity;


	return true;
}
//---------------------------------------------------------------------------------------------------
