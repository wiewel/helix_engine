//   Copyright 2020 Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef HELIXGIZMORENDERPASSDX11_H
#define HELIXGIZMORENDERPASSDX11_H

#include "RenderPassDX11.h"
#include "CommonConstantBufferFunctions.h"
#include "Display\Shaders\Gizmo.hlsl"

namespace Helix
{
	namespace Display
	{
		class HelixGizmoRenderPassDX11 : public RenderPassDX11
		{
			using TCBGizmoObject = ShaderVariableDX11<Shaders::Gizmo::cbGizmoObject>;

		public:
			HDEBUGNAME("HelixGizmoRenderPassDX11");

			HelixGizmoRenderPassDX11(DefaultInitializerType);
			HelixGizmoRenderPassDX11(const std::string& _sInstanceName = {});
			~HelixGizmoRenderPassDX11();

			bool OnInitalize(const hlx::TextToken& _CustomVariables) final;

			bool OnPerFrame() final;
			bool OnPerCamera(const Scene::CameraComponent& _Camera) final;
			bool OnPerObject(const RenderObjectDX11& _RenderObject, const MaterialDX11& _Material) final;

			std::shared_ptr<RenderPassDX11> CreatePass(const std::string& _sInstanceName) const final;

		private:
			ShaderRenderTargetDX11 m_Backbuffer = { "vColor", "BACKBUFFER" };

			TCBGizmoObject m_CBPerGizmo = { "cbGizmoObject" };
			TCBPerCamera m_CBPerCamera = { "cbPerCamera" };

			float m_fDistanceToCamera = 10.0f;
		};

		//---------------------------------------------------------------------------------------------------
		inline bool HelixGizmoRenderPassDX11::OnPerFrame()
		{
			uint32_t uClassPermutation = 0u;
			Select(ShaderType_PixelShader, uClassPermutation);
			Select(ShaderType_VertexShader, uClassPermutation);

			return true;
		}

		//---------------------------------------------------------------------------------------------------
		inline bool HelixGizmoRenderPassDX11::OnPerCamera(const Scene::CameraComponent& _Camera)
		{
			SetCameraConstants(m_CBPerCamera, _Camera.GetRenderingProperties());

			return true;
		}

		//---------------------------------------------------------------------------------------------------
		inline bool HelixGizmoRenderPassDX11::OnPerObject(const RenderObjectDX11& _RenderObject, const MaterialDX11& _Material)
		{
			/// Set World Matrix for Gizmo (only rotation and scaling part)
			_RenderObject.GetObjectToWorldMatrix(m_CBPerGizmo->mGizmoWorld);
			m_CBPerGizmo->vGizmoPosition = Math::float4(m_CBPerGizmo->mGizmoWorld.getPosition(), 1.0f);
			m_CBPerGizmo->mGizmoWorld.setPosition(Math::float3(0.f,0.f,0.f));

			/// Set Gizmo color by using the albedo material property color
			if (_Material.operator bool() == false)
			{
				/// when no material is set, use the ugliest color available
				m_CBPerGizmo->vGizmoColor = Math::float4(1.0f, 0.0f, 1.0f, 1.0f);
			}
			else
			{
				m_CBPerGizmo->vGizmoColor = Math::float4(_Material.GetProperties().vAlbedo, 1.0f);
			}

			return true;
		}
	} // Display
} // Helix

#endif // HELIXGIZMORENDERPASSDX11_H
