//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef TEXTURERESIZEEVENTMANAGERDX11_H
#define TEXTURERESIZEEVENTMANAGERDX11_H

#include "TextureDX11.h"
#include "hlx\src\Singleton.h"
#include <unordered_map>

namespace Helix
{
	namespace Display
	{
		struct TextureAndKey
		{
			TextureAndKey(TextureDX11* _pTexture = nullptr, uint64_t _uKey = 0u) :
				pTexture(_pTexture), uKey(_uKey) {}
			TextureDX11* pTexture;
			uint64_t uKey;
		};

		// event id (string hash) -> texture
		using TEventMap = std::unordered_multimap<uint64_t, TextureAndKey>;
		// Rename to something more meaningfull like AutoResizeManager etc.
		class TextureResizeEventManagerDX11 : public hlx::Singleton<TextureResizeEventManagerDX11>
		{
			// to be able to add/remove rendertargets without destroying the texture
			//friend class RenderPassDX11;

			friend class TextureDX11;
			friend struct TextureReferenceDX11;
		public:
			HDEBUGNAME("TextureResizeEventManagerDX11");

			TextureResizeEventManagerDX11();
			~TextureResizeEventManagerDX11();
			
			//this function will resize all textures which have "AutoResize" enabled and regsitered to this Event
			void Resize(const std::string& _sEventName, uint32_t _uWidth, uint32_t _uHeight = 0, uint32_t _uDepth = 0);

		private:
			void Add(const std::string& _sEventName, TextureDX11* _pTexture);
			void Remove(const std::string& _sEventName, const uint64_t& _TextureHash);

		private:
			std::mutex m_EraseMutex;
			// event id -> texture hash
			TEventMap m_ResizeEvents;
		};
		
	} // Display
} // Helix

#endif // RENDERTARGETMANAGERDX11_H