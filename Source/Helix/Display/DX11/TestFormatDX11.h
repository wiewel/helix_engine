//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef TESTFORMATDX11_H
#define TESTFORMATDX11_H

#include "hlx\src\Logger.h"

namespace Helix
{
	namespace Display
	{
		class TestFormatDX11
		{
		public:
			HDEBUGNAME("TestFormatDX11");

			TestFormatDX11();
			~TestFormatDX11();

			// Test which texture pixel formats fit to depth stencil view formats
			void TestDepthStencilFormats(bool _bShaderResource = true);

			void TestShaderResourceForDepthFormats();
		
		private:
			
		};
	} // Display
} // Helix

#endif //TESTFORMATDX11_H