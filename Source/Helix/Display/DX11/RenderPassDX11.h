//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef RENDERPASSDX11_H
#define RENDERPASSDX11_H

#include <vector>

#include "SamplerStateDX11.h"
#include "RasterizerStateDX11.h"
#include "DepthStencilStateDX11.h"
#include "BlendStateDX11.h"
#include "TextureVariantsDX11.h"

#include "ShaderVariableDX11.h"
#include "ShaderSamplerDX11.h"
#include "ShaderTextureDX11.h"
#include "ShaderRenderTargetDX11.h"

#include "RenderDeviceDrawDX11.h"

#ifdef HNUCLEO
#include "DataStructures\SyncVariable.h"
#endif

// forward decls
namespace hlx
{
	class TextToken;
}

namespace Helix
{
	// forward declarations
	namespace Scene
	{
		class CameraComponent;
		class LightContext;
	}

	namespace Resources
	{
		class RenderPassDesc;
	}

	namespace Display
	{
		class ShaderDX11;
		class RenderDeviceDX11;
		class RenderObjectDX11;

		class RenderPassDX11
		{
			friend class RenderProcedureManagerDX11;
		protected:
			RenderPassDX11(DefaultInitializerType, const std::string& _sTypeName);

		public:
			HDEBUGNAME("RenderPassDX11");

			explicit RenderPassDX11(
				const std::string& _sTypeName,
				const std::string& _sInstanceName,
				const std::vector<ShaderTextureDX11*>& _Textures = {},
				const std::vector<ShaderRenderTargetDX11*>& _RenderTargets = {},
				const std::vector<ShaderVariable*>& _Buffers = {},
				const std::vector<ShaderSamplerDX11*>& _Samplers = {});

			virtual ~RenderPassDX11();

			// create a new explicit renderpass instance e.g. return std::make_shared<HelixSolidRenderPassDX11>();
			virtual std::shared_ptr<RenderPassDX11> CreatePass(const std::string& _sInstanceName) const { return nullptr; };

			// move to private and make friendclass of renderproc!
			bool Initialize(const Resources::RenderPassDesc& _Desc);

			// calls OnIntialize with empty custom vars & OnSelectDefault etc
			// this function does not initalize RenderStates (blend, rasterizer, depthstencil)!
			bool DefaultInitialize();

			// loads a shader if non is set or reloads it if specified
			bool SetShader(const std::string& _sShaderName, const HashedStringValue& _sInstanceName, bool _bReload = false);

			// create a descritption from the current configuration
			Resources::RenderPassDesc CreateDescription() const;

			//Applys all states and renders all objects in camera frustums
			void Apply();

			void SetRasterizerState(const RasterizerDesc& _RasterizerState, const char* pDbgName = nullptr);
			void SetDepthStencilState(const DepthStencilDesc& _DepthStencilDesc, const char* pDbgName = nullptr);
			void SetBlendState(const BlendDesc& _BlendDesc, const char* pDbgName = nullptr);

			// assing shader input texture via name, returns false if the pass doesnt have a texture with this name
			bool SetInputTexture(const TextureDX11& _Texture, const std::string& _sName, const bool _bMatchMappedName = true, const uint32_t _uArrayIndex = 0);
			// assing shader output (rendertarget) texture via name, returns false if the pass doesnt have a rendertarget with this name
			bool SetOutputTexture(const RenderTargetTextureDX11& _RenderTarget, const std::string& _sName, const bool _bMatchMappedName = true);

			// set depthstencil texture
			void SetDepthStencilTexture(const DepthStencilTextureDX11& _DepthStencil);

			// returns assigned rendertarget or a copy of it, can be nullptr if no matching texture was found
			const TextureDX11* GetOutputTexture(const std::string& _sMappedName) const;

			// selects shader instance interface
			void ApplyInstance();

			// Rendering callbacks, return false to abort rendering this stage

			// called once per frame
			//this function is called once per "Apply"
			virtual bool OnPerFrame() { return true; };

			//this function is called for every camera in this pass
			virtual bool OnPerCamera(const Scene::CameraComponent& _Camera) { return true; };

			//this function is being called for every GameObject attached to a cam in this Pass, BEFORE rendering the mesh! (if they are rendered)
			virtual bool OnPerObject(const RenderObjectDX11& _RenderObject, const MaterialDX11& _Material) { return true; };

			// this function is called before Apply function
			virtual bool OnPreRender() { return true; };

			// this function is called after Apply function, (all rendering is done)
			virtual void OnPostRender() {};

			// this function is called before dispatching the compute shader (if this pass has a compute shader)
			// override this function and set the values of the output parameters to change execution behaviour
			// the developer needs to return true if OnDispatch and the dx dispatch function should be called again (indirect loop), false for one-shot execution
			virtual bool OnDispatch(_Out_ uint32_t& _ThreadGroupCountX, _Out_ uint32_t& _ThreadGroupCountY, _Out_ uint32_t& _ThreadGroupCountZ) { return false; };

			// this function will be called after each compute shader dispatch
			virtual void OnPostDispatch() {};

			// clear rendertargets before rendering
			void ClearRenderTargets(bool _bClearTargets);
			// clear depth stencil before rendering
			void ClearDepthStencil(bool _bClearDepthStencil);
			
			bool IsEnabled() const;
			void SetEnabled(bool _bEnabled);
			bool IsInitialized() const;

			void SetDebugMode(bool _bDebugMode);

			void UpdateViewports(const DepthStencilTextureDX11& _DepthStencil);

			// TODO: make available only to friendclass RenderProcedureManagerDX11
			std::vector<ShaderTextureDX11*>& GetInputTextures();

			const DepthStencilTextureDX11& GetDepthStencilTexture() const;
			DepthStencilTextureDX11& GetDepthStencilTexture();

			const std::string& GetDepthStencilMappedName() const;
			void SetDepthStencilMappedName(const std::string& _sMappedName);

			const uint64_t& GetIndentifier() const;

			const std::string& GetTypeName() const;
			const std::string& GetInstanceName() const;

			virtual std::string GetPermutationName(const ShaderPermutationHash& _Perm) const;

			bool IsPrototype() const;

			void BindToNucleo();

		protected:
			virtual void OnSerializeCustomVariables(_Out_ hlx::TextToken& _CustomVariables) const {};

			// called when OnInitalize was successfull
			virtual bool OnSelectDefault();

			//called on construction
			virtual bool OnInitalize(const hlx::TextToken& _CustomVariables) { return true; }

			std::shared_ptr<ShaderDX11> GetShader() const;

			bool Select(const ShaderType& _kShaderStage,
				const uint32_t& _uClassPermutation,
				const std::vector<uint8_t>& _RangeIndices = {});

		private:
			// TODO: maybe make anon inline function cpp to reduce call overhead
			void DrawObject(const RenderObjectDX11& _Object);

#ifdef HNUCLEO
			bool CompileShader(const std::string& _sPath);
#endif

		protected:
			const bool m_bIsPrototype;
			const std::string m_sTypeName;
			const HashedStringValue m_sInstanceName;
			const uint64_t m_kIdentifier;

		private:
			std::shared_ptr<ShaderDX11> m_pShader = nullptr;
			bool m_bComputePass = false;

			RenderDeviceDrawDX11 m_RenderDeviceDraw;

			// don't destroy these pointers, objects are held by child classes
			std::vector<ShaderTextureDX11*> m_Textures;
			std::vector<ShaderRenderTargetDX11*> m_RenderTargets;
			std::vector<ShaderVariable*> m_Buffers; // ShaderVariables
			std::vector<ShaderSamplerDX11*> m_Samplers;

			// automaticly created Samplers, destroyed by this class
			std::vector<ShaderSamplerDX11*> m_DefaultSamplers;

			RasterizerStateDX11 m_RasterizerState;
			DepthStencilStateDX11 m_DepthStencilState;
			BlendStateDX11 m_BlendState;

			DepthStencilTextureDX11 m_DepthStencilTexture = nullptr;
			std::string m_sDepthStencilMappedName;

			RenderDeviceDX11* m_pRenderDevice = nullptr;

			// Default parameters
			bool m_bClearDepthStencil = false;
			bool m_bClearRenderTargets = true;
			bool m_bEnabled = true;

		protected:
			bool m_bDebug = false;
			bool m_bInitialized = false;

			private:

#ifdef HNUCLEO
			Datastructures::SyncVariableConst<std::string> m_SyncTypeName = { m_sTypeName, "Name" };
			Datastructures::SyncVariableConst<std::string> m_SyncInstanceName = { m_sInstanceName.GetString(), "Instance" };
			Datastructures::SyncVariableConst<uint64_t> m_SyncIdentifier = { m_kIdentifier, "ID" };
			Datastructures::SyncVariable<bool> m_SyncEnabled = { m_bEnabled, "Enabled" };
			Datastructures::SyncVariable<bool> m_SyncClearDepthStencil = { m_bClearDepthStencil, "Clear Depth/Stencil" };
			Datastructures::SyncVariable<bool> m_SyncClearRenderTargets = { m_bClearRenderTargets, "Clear RenderTargets" };

			std::string m_sShaderPath;
			Datastructures::SyncVariable<std::string> m_SyncShaderPath = { m_sShaderPath, "Path" };

			bool m_bRecompile = true;
			Datastructures::SyncVariable<bool> m_SyncRecompile = { m_bRecompile, "Recompile" };

			Datastructures::SyncButton m_SyncReloadButton = { "Reload Shader", [&]() { if (m_bRecompile) { CompileShader(m_sShaderPath); } SetShader(m_sTypeName, m_sInstanceName, true); } };

			std::vector<Datastructures::SyncGroup*> m_SyncBufferGroups;
			Datastructures::SyncGroup m_SyncDefaultGroup = { "Default Settings",{ &m_SyncTypeName, &m_SyncInstanceName, &m_SyncIdentifier, &m_SyncEnabled, &m_SyncClearDepthStencil,&m_SyncClearRenderTargets, &m_SyncShaderPath, &m_SyncRecompile, &m_SyncReloadButton } };
			Datastructures::SyncDock m_SyncDock = { "Pass", {&m_SyncDefaultGroup} };
#endif
		};		

		inline bool RenderPassDX11::IsPrototype() const	{return m_bIsPrototype;	}

		inline std::shared_ptr<ShaderDX11> RenderPassDX11::GetShader() const { return m_pShader; }

		inline bool RenderPassDX11::Select(
			const ShaderType& _kShaderStage,
			const uint32_t& _uClassPermutation,
			const std::vector<uint8_t>& _RangeIndices)
		{
			return m_pShader->Select(_kShaderStage, _uClassPermutation, _RangeIndices);
		}

		inline void RenderPassDX11::SetDepthStencilTexture(const DepthStencilTextureDX11& _DepthStencil) { m_DepthStencilTexture = _DepthStencil; }

		// Getter/Setter
		inline void RenderPassDX11::ClearRenderTargets(bool _bClearTargets) { m_bClearRenderTargets = _bClearTargets; }
		inline void RenderPassDX11::ClearDepthStencil(bool _bClearDepthStencil)	{m_bClearDepthStencil = _bClearDepthStencil;}

		inline bool RenderPassDX11::IsEnabled() const { return m_bEnabled; }
		inline void RenderPassDX11::SetEnabled(bool _bEnabled)  { m_bEnabled = _bEnabled; }
		inline bool RenderPassDX11::IsInitialized() const { return m_bInitialized; }
		inline void RenderPassDX11::SetDebugMode(bool _bDebugMode) { m_bDebug = _bDebugMode; }
		inline void RenderPassDX11::UpdateViewports(const DepthStencilTextureDX11& _DepthStencil) { m_pRenderDevice->UpdateViewports(_DepthStencil); }

		inline std::vector<ShaderTextureDX11*>& RenderPassDX11::GetInputTextures() {return m_Textures; }
		inline const DepthStencilTextureDX11& RenderPassDX11::GetDepthStencilTexture() const { return m_DepthStencilTexture; }
		inline DepthStencilTextureDX11& RenderPassDX11::GetDepthStencilTexture() { return m_DepthStencilTexture; }
		inline const std::string& RenderPassDX11::GetDepthStencilMappedName() const { return m_sDepthStencilMappedName; }
		inline void RenderPassDX11::SetDepthStencilMappedName(const std::string & _sMappedName)	{ m_sDepthStencilMappedName = _sMappedName;}
		inline const uint64_t& RenderPassDX11::GetIndentifier() const	{return m_kIdentifier; }
		inline const std::string& RenderPassDX11::GetTypeName() const { return m_sTypeName; }

		inline const std::string& RenderPassDX11::GetInstanceName() const{	return m_sInstanceName.GetString();	}

	} // Display
} // Helix

#endif
