//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "SamplerStatePoolDX11.h"

#include "RenderDeviceDX11.h"
#include "hlx\src\Logger.h"
#include "hlx\src\StandardDefines.h"

using namespace Helix::Display;

SamplerStatePoolDX11::SamplerStatePoolDX11()
{
	m_pRenderDevice = RenderDeviceDX11::Instance();

	HASSERT(m_pRenderDevice->IsInitialized(), "RenderDevice has not been initialized yet!");
}
//---------------------------------------------------------------------------------------------------
SamplerStatePoolDX11::~SamplerStatePoolDX11()
{
}
//---------------------------------------------------------------------------------------------------
uint64_t SamplerStatePoolDX11::HashFunction(const D3D11_SAMPLER_DESC& _Desc)
{
	return BaseTypeHash(_Desc);
}
//---------------------------------------------------------------------------------------------------
ID3D11SamplerState* SamplerStatePoolDX11::CreateFromDesc(const D3D11_SAMPLER_DESC& _Desc, const uint64_t& _Hash)
{
	ID3D11SamplerState* pSampleState = nullptr;

	//Create new sampler sate
	HR2(m_pRenderDevice->GetDevice()->CreateSamplerState(&_Desc, &pSampleState));

	return pSampleState;
}
//---------------------------------------------------------------------------------------------------
D3D11_SAMPLER_DESC SamplerStatePoolDX11::GetDefaultDesc(DefaultSamplerState kDefaultState)
{
	D3D11_SAMPLER_DESC Desc = {};

	Desc.MipLODBias = 0.0f;
	Desc.MaxAnisotropy = m_uMaxAnisotropy;
	Desc.ComparisonFunc = D3D11_COMPARISON_NEVER;
	Desc.MinLOD = -FLT_MAX;
	Desc.MaxLOD = FLT_MAX;

	//Default dx border color
	Desc.BorderColor[0] = 1.f;
	Desc.BorderColor[1] = 1.f;
	Desc.BorderColor[2] = 1.f;
	Desc.BorderColor[3] = 1.f;

	switch (kDefaultState)
	{
	case DefaultSamplerState_PointClamp:
		Desc.Filter = D3D11_FILTER_MIN_MAG_MIP_POINT;
		Desc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
		break;
	case DefaultSamplerState_PointWrap:
		Desc.Filter = D3D11_FILTER_MIN_MAG_MIP_POINT;
		Desc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
		break;
	case DefaultSamplerState_LinearClamp:
		Desc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
		Desc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
		break;
	case DefaultSamplerState_LinearWrap:
		Desc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
		Desc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
		break;
	case DefaultSamplerState_AnisotropicClamp:
		Desc.Filter = D3D11_FILTER_ANISOTROPIC;
		Desc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
		break;
	case DefaultSamplerState_AnisotropicWrap:
		Desc.Filter = D3D11_FILTER_ANISOTROPIC;
		Desc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
		break;
	case DefaultSamplerState_ShadowMap:
		Desc.ComparisonFunc = D3D11_COMPARISON_LESS_EQUAL;
		Desc.Filter = D3D11_FILTER_COMPARISON_MIN_MAG_LINEAR_MIP_POINT;
		Desc.AddressU = D3D11_TEXTURE_ADDRESS_BORDER;

		Desc.MinLOD = 0.0f;
		Desc.MaxLOD = 0.0f;
		Desc.BorderColor[0] = 1.0f;
		Desc.BorderColor[1] = 1.0f;
		Desc.BorderColor[2] = 1.0f;
		Desc.BorderColor[3] = 1.0f;
		break;
	default:
		break;
	}

	Desc.AddressV = Desc.AddressW = Desc.AddressU;

	return Desc;
}
//---------------------------------------------------------------------------------------------------
