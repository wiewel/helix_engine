//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef TEXTURECACHEDX11_H
#define TEXTURECACHEDX11_H

#include "hlx\src\Singleton.h"
#include <concurrent_unordered_map.h>
#include "TextureVariantsDX11.h"

namespace Helix
{
	namespace Display
	{
		using TTextureCache = concurrency::concurrent_unordered_multimap<uint64_t, TextureDX11>;

		class TextureCacheDX11 : public hlx::Singleton<TextureCacheDX11>
		{
		public:
			TextureCacheDX11();
			~TextureCacheDX11();

			template <class TextureVariant = TextureDX11>
			TextureVariant Get(const TextureDesc& _Desc);

			// this will leave the orginal texture in an empty state
			void Return(TextureDX11&& _Texture);
		private:
			uint64_t HashFunction(const TextureDesc& _Desc);

		private:
			std::mutex m_EraseMutex;
			TTextureCache m_Cache;
		};

		template<class TextureVariant>
		inline TextureVariant TextureCacheDX11::Get(const TextureDesc& _Desc)
		{
			TextureDesc Desc = _Desc;
			Desc.m_bPooled = false; //disable pooling
			Desc.m_bLoadFromFile = false; // disable filesystem access (no immutable textures) this would slowdown the cache too much

			// compute hash, check if theres a matching texture
			uint64_t uHash = HashFunction(Desc);
			
			TextureVariant Ret(nullptr);

			TTextureCache::iterator it = m_Cache.find(uHash);
			if (it == m_Cache.end())
			{
				return TextureVariant(TextureDX11(Desc));
			}

			Ret = TextureVariant(it->second);

			{
				std::lock_guard<std::mutex> Lock(m_EraseMutex);
				m_Cache.unsafe_erase(it);
			}

			return Ret;
		}
	} // Display
} // Helix

#endif // !TEXTURECACHEDX11_H