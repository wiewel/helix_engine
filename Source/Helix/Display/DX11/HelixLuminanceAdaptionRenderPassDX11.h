//   Copyright 2020 Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef HELIXLUMINANCEADAPTIONRENDERPASSDX11_H
#define HELIXLUMINANCEADAPTIONRENDERPASSDX11_H

#include "RenderPassDX11.h"
#include "CommonConstantBufferFunctions.h"
#include "Display\Shaders\LuminanceAdaption.hlsl"

namespace Helix
{
	namespace Display
	{
		class HelixLuminanceAdaptionRenderPassDX11 : public RenderPassDX11
		{
		public:
			HDEBUGNAME("HelixLuminanceAdaptionRenderPassDX11");

			HelixLuminanceAdaptionRenderPassDX11(DefaultInitializerType);
			HelixLuminanceAdaptionRenderPassDX11(const std::string& _sInstanceName = {});
			~HelixLuminanceAdaptionRenderPassDX11();

			bool OnInitalize(const hlx::TextToken& _CustomVariables) final;

			bool OnPerFrame() final;

			bool OnPreRender() final;
			void OnPostRender() final;

			void SetAdaptionRate(const float _fAdaptionRate);
			float GetAdaptionRate() const;

			void SetAdaptionTargets(RenderTargetTextureDX11 _Target1, RenderTargetTextureDX11 _Target2);
			std::shared_ptr<RenderPassDX11> CreatePass(const std::string& _sInstanceName) const final;

		private:
			ShaderVariableDX11<Shaders::LuminanceAdaption::cbLuminanceAdaption> m_CBLuminanceAdaption = { "cbLuminanceAdaption" };

			ShaderTextureDX11 m_LastLuminance = { "gLastLuminance" };
			ShaderTextureDX11 m_CurrentLuminance = { "gCurrentLuminance", "LUMINANCE_MAP" };

			ShaderRenderTargetDX11 m_OutputTexture = { "fLuminanceAdaption", "LUMINANCE_ADAPTION_MAP" };

			TCBPerFrame m_CBPerFrame = { "cbPerFrame" };

		private:
			/// Cached Adaption Targets are used to store the luminance data of the last frame
			RenderTargetTextureDX11 m_AdaptionTargets[2];
			uint32_t m_uCurrentTarget = 0;

			float m_fAdaptionRate = 2.0f;
		};

		inline void HelixLuminanceAdaptionRenderPassDX11::SetAdaptionRate(const float _fAdaptionRate) { m_fAdaptionRate = _fAdaptionRate; }
		inline float HelixLuminanceAdaptionRenderPassDX11::GetAdaptionRate() const { return m_fAdaptionRate; }

		inline void HelixLuminanceAdaptionRenderPassDX11::SetAdaptionTargets(RenderTargetTextureDX11 _Target1, RenderTargetTextureDX11 _Target2)
		{
			m_AdaptionTargets[0] = _Target1;
			m_AdaptionTargets[1] = _Target2;
		}


	} // Display
} // Helix

#endif // HELIXLUMINANCEADAPTIONRENDERPASSDX11_H