//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "TextureDX11.h"
#include "hlx\src\Logger.h"
#include "DirectXTex\DDSTextureLoader\DDSTextureLoader.h"
#include "RenderDeviceDX11.h"
#include "hlx\src\FileStream.h"
#include "TextureResizeEventManagerDX11.h"

using namespace Helix::Display;

//---------------------------------------------------------------------------------------------------
TextureDX11::TextureDX11(const TextureDX11& _Other) :
	TDX11TextureRef(_Other),
	m_sDbgName(_Other.m_sDbgName)
{
}
//---------------------------------------------------------------------------------------------------
TextureDX11::TextureDX11(const TextureDesc& _Desc, const std::string& _sDbgName)
{
	m_pReference = TexturePoolDX11::Instance()->Get(_Desc, m_Hash);
	SetDebugName(_sDbgName);
}
//---------------------------------------------------------------------------------------------------
TextureDX11::~TextureDX11()
{
}
//---------------------------------------------------------------------------------------------------
TextureDX11& TextureDX11::operator=(std::nullptr_t)
{
	TDX11TextureRef::operator=(nullptr);
	m_sDbgName.resize(0);

	return *this;
}
//---------------------------------------------------------------------------------------------------
TextureDX11& TextureDX11::operator=(const TextureDX11& _Other)
{
	if (this == &_Other)
		return *this;

	TDX11TextureRef::operator=(_Other);

	m_sDbgName = _Other.m_sDbgName;

	return *this;
}
//---------------------------------------------------------------------------------------------------
void TextureDX11::SetDebugName(const std::string& _sDebugName)
{
	m_sDbgName = _sDebugName;
	if (m_pReference != nullptr)
	{
		HDXDEBUGNAME(m_pReference->m_pResource, m_sDbgName.c_str());
		HDXDEBUGNAME(m_pReference->m_pResourceView, (m_sDbgName + "SRV").c_str());
		HDXDEBUGNAME(m_pReference->m_pRenderTargetView, (m_sDbgName + "RTV").c_str());
		HDXDEBUGNAME(m_pReference->m_pDepthStencilView, (m_sDbgName + "DSV").c_str());
		HDXDEBUGNAME(m_pReference->m_pDepthStencilViewReadOnly, (m_sDbgName + "DSV_RO").c_str());
		HDXDEBUGNAME(m_pReference->m_pUnorderedAccessView, (m_sDbgName + "UAV").c_str());
	}
}
//---------------------------------------------------------------------------------------------------
void TextureDX11::SetResizeEvent(const std::string& _sEventName, const float& _fWidthScale, const float& _fHeightScale, const float& _fDepthScale)
{
	HASSERT(m_pReference != nullptr, "Invalid texture (reference) can not be resized!");

	if (m_pReference->m_ResizeProperties.sEventName.empty() == false)
	{
		HERROR("Texture %s already registered to event %s (implicit unregister)", CSTR(m_sDbgName), CSTR(m_pReference->m_ResizeProperties.sEventName));
		TextureResizeEventManagerDX11::Instance()->Remove(m_pReference->m_ResizeProperties.sEventName, GetKey());
	}

	m_pReference->m_ResizeProperties.sEventName = _sEventName;
	m_pReference->m_ResizeProperties.fWidthScale = _fWidthScale;
	m_pReference->m_ResizeProperties.fHeightScale = _fHeightScale;
	m_pReference->m_ResizeProperties.fDepthScale = _fDepthScale;

	TextureResizeEventManagerDX11::Instance()->Add(_sEventName, this);
}
//---------------------------------------------------------------------------------------------------
bool TextureDX11::ResizeEvent(uint32_t _uWidth, uint32_t _uHeight, uint32_t _uDepth)
{
	return Resize(
		static_cast<uint32_t>(_uWidth * m_pReference->m_ResizeProperties.fWidthScale),
		static_cast<uint32_t>(_uHeight * m_pReference->m_ResizeProperties.fHeightScale),
		static_cast<uint32_t>(_uDepth * m_pReference->m_ResizeProperties.fDepthScale));
}
//---------------------------------------------------------------------------------------------------
void TextureDX11::CheckTextureDescription(const TextureDesc& _TextureDesc)
{
	RenderDeviceDX11* pRenderDevice = RenderDeviceDX11::Instance();

	const DXGI_FORMAT Format = static_cast<DXGI_FORMAT>(_TextureDesc.m_kFormat);

	// MipMap-Checks
	UINT uFormatSupportMask = D3D11_FORMAT_SUPPORT_MIP_AUTOGEN;
	if (_TextureDesc.m_kResourceFlag.CheckFlag(MiscResourceFlag_GenerateMips) && pRenderDevice->GetDevice()->CheckFormatSupport(static_cast<DXGI_FORMAT>(_TextureDesc.m_kFormat), &uFormatSupportMask) != S_OK)
	{
		HWARNINGD("MipMap generation is not supported by used pixel format! %s", CSTR(_TextureDesc.m_sFilePathOrName));
	}

	uFormatSupportMask = D3D11_FORMAT_SUPPORT_MIP;
	if (_TextureDesc.m_uMipLevels != 1 && pRenderDevice->GetDevice()->CheckFormatSupport(Format, &uFormatSupportMask) != S_OK)
	{
		// MipLevels = 0 => generate a full set of subtextures
		HWARNINGD("MipMaps are not supported by used pixel format! %s", CSTR(_TextureDesc.m_sFilePathOrName));
	}

	// SRV-Checks
	if (_TextureDesc.m_kBindFlag & ResourceBindFlag_ShaderResource)
	{
		// DSV-Checks
		if (_TextureDesc.m_kBindFlag & ResourceBindFlag_DepthStencil)
		{
			// if a texture is a SR only four pixel formats remain for DSV creation
			if (IsDepthTypeless(_TextureDesc.m_kFormat) == false)
			{
				HWARNINGD("Texture needs to be initialized with one of the following pixel formats: PixelFormat_R32G8X24_Typeless, PixelFormat_R24G8_Typeless, PixelFormat_R32_Typeless, PixelFormat_R16_Typeless! %s", CSTR(_TextureDesc.m_sFilePathOrName));
			}
		}
		else // if there is no DSV check if the format is readable by shaders
		{
			uFormatSupportMask = D3D11_FORMAT_SUPPORT_SHADER_LOAD | D3D11_FORMAT_SUPPORT_SHADER_SAMPLE;
			if (pRenderDevice->GetDevice()->CheckFormatSupport(Format, &uFormatSupportMask) != S_OK)
			{
				HWARNINGD("ShaderResourceView generation is not supported by the used pixel format! %s", CSTR(_TextureDesc.m_sFilePathOrName));
			}
		}	
	} // if it is only a DSV
	else if (_TextureDesc.m_kBindFlag & ResourceBindFlag_DepthStencil)
	{
		uFormatSupportMask = D3D11_FORMAT_SUPPORT_DEPTH_STENCIL;
		if (pRenderDevice->GetDevice()->CheckFormatSupport(Format, &uFormatSupportMask) != S_OK &&
			IsDepthTypeless(_TextureDesc.m_kFormat) == false)
		{
			HWARNINGD("DepthStencilView generation is not supported by the used pixel format! Accepted formats are: PixelFormat_R32G8X24_Typeless, PixelFormat_R24G8_Typeless, PixelFormat_R32_Typeless, PixelFormat_R16_Typeless, PixelFormat_D32_Float_S8X24_UInt, PixelFormat_D24_UNorm_S8_UInt, PixelFormat_D32_Float, PixelFormat_D16_UNorm! %s", CSTR(_TextureDesc.m_sFilePathOrName));
		}
	}
	
	// RTV-Checks
	if (_TextureDesc.m_kBindFlag & ResourceBindFlag_RenderTarget)
	{
		uFormatSupportMask = D3D11_FORMAT_SUPPORT_RENDER_TARGET;
		if (pRenderDevice->GetDevice()->CheckFormatSupport(Format, &uFormatSupportMask) != S_OK)
		{
			HWARNINGD("RenderTargetView generation is not supported by the used pixel format! %s", CSTR(_TextureDesc.m_sFilePathOrName));
		}
	}
}
//---------------------------------------------------------------------------------------------------
bool TextureDX11::Initialize(TextureReferenceDX11& _TexRef, const TextureDesc& _TextureDesc, const BufferSubresourceData& _InitialData)
{
	RenderDeviceDX11* pRenderDevice = RenderDeviceDX11::Instance();

	HRESULT Result = S_OK;

 	HDBG(CheckTextureDescription(_TextureDesc));

	D3D11_SUBRESOURCE_DATA Data = {};
	Data.pSysMem = _InitialData.pData;
	Data.SysMemPitch = _InitialData.uRowPitch;
	Data.SysMemSlicePitch = _InitialData.uDepthPitch;

	D3D11_SUBRESOURCE_DATA* pData = (Data.pSysMem != nullptr && Data.SysMemPitch > 0u) ? &Data : nullptr;

	if (_TexRef.m_pResource == nullptr)
	{
		if (_TextureDesc.m_kResourceDimension == ResourceDimension_Texture1D || _TextureDesc.m_kResourceDimension == ResourceDimension_Texture1DArray)
		{
			D3D11_TEXTURE1D_DESC Desc;

			Desc.Width = _TextureDesc.m_uWidth;
			Desc.MipLevels = _TextureDesc.m_uMipLevels;
			Desc.ArraySize = _TextureDesc.m_uArraySize;
			Desc.Format = static_cast<DXGI_FORMAT>(_TextureDesc.m_kFormat);

			Desc.Usage = static_cast<D3D11_USAGE>(_TextureDesc.m_kUsageFlag);
			Desc.BindFlags = _TextureDesc.m_kBindFlag.GetFlag();
			Desc.CPUAccessFlags = _TextureDesc.m_kCPUAccessFlag.GetFlag();
			Desc.MiscFlags = _TextureDesc.m_kResourceFlag.GetFlag();

			HRERROR(Result = (pRenderDevice->GetDevice()->CreateTexture1D(&Desc, pData, &_TexRef.m_pTexture1D)));
		}
		else if (_TextureDesc.m_kResourceDimension == ResourceDimension_Texture2D || _TextureDesc.m_kResourceDimension == ResourceDimension_Texture2DArray)
		{
			D3D11_TEXTURE2D_DESC Desc;
			Desc.Width = _TextureDesc.m_uWidth;
			Desc.Height = _TextureDesc.m_uHeight;
			Desc.MipLevels = _TextureDesc.m_uMipLevels;
			Desc.ArraySize = _TextureDesc.m_uArraySize;
			Desc.Format = static_cast<DXGI_FORMAT>(_TextureDesc.m_kFormat);

			Desc.SampleDesc.Count = _TextureDesc.m_uSampleCount;
			Desc.SampleDesc.Quality = _TextureDesc.m_uSampleQuality;

			Desc.Usage = static_cast<D3D11_USAGE>(_TextureDesc.m_kUsageFlag);
			Desc.BindFlags = _TextureDesc.m_kBindFlag.GetFlag();
			Desc.CPUAccessFlags = _TextureDesc.m_kCPUAccessFlag.GetFlag();
			Desc.MiscFlags = _TextureDesc.m_kResourceFlag.GetFlag();

			HRERROR(Result = (pRenderDevice->GetDevice()->CreateTexture2D(&Desc, pData, &_TexRef.m_pTexture2D)));
		}
		else if (_TextureDesc.m_kResourceDimension == ResourceDimension_Texture3D)
		{
			D3D11_TEXTURE3D_DESC Desc;

			Desc.Width = _TextureDesc.m_uWidth;
			Desc.Height = _TextureDesc.m_uHeight;
			Desc.Depth = _TextureDesc.m_uDepth;
			Desc.MipLevels = _TextureDesc.m_uMipLevels;
			Desc.Format = static_cast<DXGI_FORMAT>(_TextureDesc.m_kFormat);
			Desc.Usage = static_cast<D3D11_USAGE>(_TextureDesc.m_kUsageFlag);
			Desc.BindFlags = _TextureDesc.m_kBindFlag.GetFlag();
			Desc.CPUAccessFlags = _TextureDesc.m_kCPUAccessFlag.GetFlag();
			Desc.MiscFlags = _TextureDesc.m_kResourceFlag.GetFlag();

			HRERROR(Result = (pRenderDevice->GetDevice()->CreateTexture3D(&Desc, pData, &_TexRef.m_pTexture3D)));
		}
	}

	return Result == S_OK && CreateViews(_TexRef);
}
//---------------------------------------------------------------------------------------------------
bool TextureDX11::Initialize(TextureReferenceDX11& _TexRef, const TextureDesc& _TextureDesc, const hlx::bytes& _Data)
{
	if (_Data.size() == 0)
	{
		HERROR("Texture data empty!");
		return false;
	}

	HASSERTD(_TexRef.m_pResource == nullptr && _TexRef.m_pResourceView == nullptr, "Texture already in use!");

	RenderDeviceDX11* pRenderDevice = RenderDeviceDX11::Instance();

	//These versions provide explicit control over the created resource's usage, binding flags, CPU access flags, and miscellaneous flags for advanced / expert scenarios.
	//The standard routines default to D3D11_USAGE_DEFAULT, D3D11_BIND_SHADER_RESOURCE, 0, and 0 respectively.
	//For cubemaps, the miscellaneous flags default to D3D11_RESOURCE_MISC_TEXTURECUBE.
	//For auto-gen mipmaps, the default binding flags are D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET and miscellaneous flags is set to D3D11_RESOURCE_MISC_GENERATE_MIPS.
	//There is also a 'forceSRGB' option for working around gamma issues with content that is in the sRGB or similar color space but is not encoded explicitly as an SRGB format.
	//Note that the 'maxsize' parameter is not at the end of the parameter list like it is in the non-Ex version.
	HRESULT Result = S_OK;

	D3D11_USAGE kUsage = static_cast<D3D11_USAGE>(_TextureDesc.m_kUsageFlag);
	D3D11_BIND_FLAG kBindFlags = static_cast<D3D11_BIND_FLAG> (_TextureDesc.m_kBindFlag.GetFlag());
	D3D11_CPU_ACCESS_FLAG kCPUAccessFlags = static_cast<D3D11_CPU_ACCESS_FLAG> (_TextureDesc.m_kCPUAccessFlag.GetFlag());
	D3D11_RESOURCE_MISC_FLAG kMiscFlags = static_cast<D3D11_RESOURCE_MISC_FLAG>(_TextureDesc.m_kResourceFlag.GetFlag());
	
	//create srv
	// This function generates full set of mip-map
	HRIF(Result = DirectX::CreateDDSTextureFromMemoryEx(pRenderDevice->GetDevice(), _Data.c_str(), _Data.size(), /*_TextureDesc.m_uMipLevels*/ 0, kUsage,
		kBindFlags, kCPUAccessFlags, kMiscFlags, _TextureDesc.m_bForceSRgb, &_TexRef.m_pResource, &_TexRef.m_pResourceView))
	{
		return false;
	}

	D3D11_RESOURCE_DIMENSION ResDim;
	_TexRef.m_pResource->GetType(&ResDim);

	if (ResDim == D3D11_RESOURCE_DIMENSION_TEXTURE1D)
	{
		D3D11_TEXTURE1D_DESC Desc;
		_TexRef.m_pTexture1D->GetDesc(&Desc);
		_TexRef.m_Description = ConvertToHelixDescription(Desc);
	}
	else if (ResDim == D3D11_RESOURCE_DIMENSION_TEXTURE2D)
	{
		D3D11_TEXTURE2D_DESC Desc;
		_TexRef.m_pTexture2D->GetDesc(&Desc);
		_TexRef.m_Description = ConvertToHelixDescription(Desc);

		if (_TexRef.m_Description.m_kResourceDimension == ResourceDimension_TextureCUBE)
		{
			HSAFE_RELEASE(_TexRef.m_pResourceView);
		}
	}
	else if (ResDim == D3D11_RESOURCE_DIMENSION_TEXTURE3D)
	{
		D3D11_TEXTURE3D_DESC Desc;
		_TexRef.m_pTexture3D->GetDesc(&Desc);
		_TexRef.m_Description = ConvertToHelixDescription(Desc);
	}

	_TexRef.m_Description.m_bForceSRgb = _TextureDesc.m_bForceSRgb;
	_TexRef.m_Description.m_bPooled = _TextureDesc.m_bPooled;
	_TexRef.m_Description.m_sFilePathOrName = _TextureDesc.m_sFilePathOrName;

	return Result == S_OK && CreateViews(_TexRef);
}
//---------------------------------------------------------------------------------------------------
bool TextureDX11::Initialize(TextureReferenceDX11& _TexRef, const hlx::bytes& _Data)
{
	if (_Data.empty())
	{
		HERROR("Texture data empty!");
		return false;
	}

	HASSERTD(_TexRef.m_pTexture2D == nullptr && _TexRef.m_pResourceView == nullptr, "Texture already in use!");

	RenderDeviceDX11* pRenderDevice = RenderDeviceDX11::Instance();

	HRESULT Result = S_OK;

	//create srv
	HRERROR(Result = DirectX::CreateDDSTextureFromMemory(pRenderDevice->GetDevice(), _Data.c_str(), _Data.size(), &_TexRef.m_pResource, &_TexRef.m_pResourceView));

	return  Result == S_OK;
}
//---------------------------------------------------------------------------------------------------
bool TextureDX11::Initialize(TextureReferenceDX11& _TexRef, const hlx::string& _sFilePath)
{
	hlx::fbytestream FileStream(_sFilePath, std::ios_base::in | std::ios_base::binary);

	if (FileStream.is_open() == false)
	{
		HERROR("Unable to open texture %s", _sFilePath.c_str());
		return false;
	}

	HLOGD("Loading %s...", _sFilePath.c_str());

	hlx::bytes Data = FileStream.get<hlx::bytes>(FileStream.size());

	return Initialize(_TexRef, Data);
}
//---------------------------------------------------------------------------------------------------

#ifndef MIPHELPER
#define MIPHELPER(_desc_member) \
_desc_member.MostDetailedMip = 0u; \
if (_TexRef.m_Description.m_uMipLevels == 0u) \
{ \
	_desc_member.MipLevels = HUNDEFINED32; \
} else { \
	_desc_member.MipLevels = _TexRef.m_Description.m_uMipLevels; \
}
#endif
//---------------------------------------------------------------------------------------------------
// Creates RTV, SRV, DSV
bool TextureDX11::CreateViews(_Out_ TextureReferenceDX11& _TexRef)
{
	RenderDeviceDX11* pRenderDevice = RenderDeviceDX11::Instance();

	HRESULT Result = S_OK;

	// Create ShaderResourceView
	if (_TexRef.m_Description.m_kBindFlag & ResourceBindFlag_ShaderResource && _TexRef.m_pResourceView == nullptr)
	{
		UINT uFormatSupportMask = D3D11_FORMAT_SUPPORT_SHADER_LOAD | D3D11_FORMAT_SUPPORT_SHADER_SAMPLE;
		if (pRenderDevice->GetDevice()->CheckFormatSupport(static_cast<DXGI_FORMAT>(_TexRef.m_Description.m_kFormat), &uFormatSupportMask))
		{
			// any resource will do as first parameter because of the union
			HRERROR(Result = (pRenderDevice->GetDevice()->CreateShaderResourceView(_TexRef.m_pResource, NULL, &_TexRef.m_pResourceView)));
		}
		else
		{
			D3D11_SHADER_RESOURCE_VIEW_DESC SRV_Desc = {};
			SRV_Desc.Format = static_cast<DXGI_FORMAT>(GetDepthSRVFormatFromTypeless(_TexRef.m_Description.m_kFormat));
			
			switch (_TexRef.m_Description.m_kResourceDimension)
			{
			case ResourceDimension_Texture1D:
				SRV_Desc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE1D;
				MIPHELPER(SRV_Desc.Texture1D);
				break;
			case ResourceDimension_Texture2D:
				SRV_Desc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
				MIPHELPER(SRV_Desc.Texture2D);
				break;
			case ResourceDimension_Texture3D:
				SRV_Desc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE3D;
				MIPHELPER(SRV_Desc.Texture3D);
				break;
			case ResourceDimension_TextureCUBE:
				SRV_Desc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURECUBE;
				MIPHELPER(SRV_Desc.TextureCube);
				break;
			case ResourceDimension_Texture1DArray:
				SRV_Desc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE1DARRAY;
				SRV_Desc.Texture1DArray.ArraySize = _TexRef.m_Description.m_uArraySize;
				SRV_Desc.Texture1DArray.FirstArraySlice = 0u;
				MIPHELPER(SRV_Desc.Texture1DArray);
				break;
			case ResourceDimension_Texture2DArray:
				SRV_Desc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2DARRAY;
				SRV_Desc.Texture2DArray.ArraySize = _TexRef.m_Description.m_uArraySize;
				SRV_Desc.Texture2DArray.FirstArraySlice = 0u;
				MIPHELPER(SRV_Desc.Texture2DArray);
				break;
			// UNSUPPORTED FORMATS:
			case ResourceDimension_TextureCUBEArray:
			case ResourceDimension_Texture2DMS:
			case ResourceDimension_Texture2DMSArray:
			default:
				HFATAL("Not implemented!");
				break;
			}

			HRERROR(Result = (pRenderDevice->GetDevice()->CreateShaderResourceView(_TexRef.m_pResource, &SRV_Desc, &_TexRef.m_pResourceView)));
		}
	}

	// Create RenderTargetView
	if (_TexRef.m_Description.m_kBindFlag & ResourceBindFlag_RenderTarget && _TexRef.m_pRenderTargetView == nullptr)
	{
		HRERROR(Result = pRenderDevice->GetDevice()->CreateRenderTargetView(_TexRef.m_pResource, nullptr, &_TexRef.m_pRenderTargetView));
	}

	// Create DepthStencilView
	if (_TexRef.m_Description.m_kBindFlag & ResourceBindFlag_DepthStencil && _TexRef.m_pDepthStencilView == nullptr)
	{
		D3D11_DEPTH_STENCIL_VIEW_DESC DSV_Desc = {};
		DSV_Desc.Format = static_cast<DXGI_FORMAT>(_TexRef.m_Description.m_kFormat);
		//DSV_Desc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
		//DSV_Desc.Texture2D.MipSlice = 0u;


		switch (_TexRef.m_Description.m_kResourceDimension)
		{
		case ResourceDimension_Texture1D:
			DSV_Desc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE1D;
			DSV_Desc.Texture1D.MipSlice = 0u;
			break;
		case ResourceDimension_Texture2D:
			DSV_Desc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
			DSV_Desc.Texture2D.MipSlice = 0u;
			break;
		case ResourceDimension_Texture1DArray:
			DSV_Desc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE1DARRAY;
			DSV_Desc.Texture1DArray.ArraySize = _TexRef.m_Description.m_uArraySize;
			DSV_Desc.Texture1DArray.FirstArraySlice = 0u;
			DSV_Desc.Texture1DArray.MipSlice = 0u;
			break;
		case ResourceDimension_Texture2DArray:
			DSV_Desc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DARRAY;
			DSV_Desc.Texture2DArray.ArraySize = _TexRef.m_Description.m_uArraySize;
			DSV_Desc.Texture2DArray.FirstArraySlice = 0u;
			DSV_Desc.Texture2DArray.MipSlice = 0u;
			break;
		/// UNSUPPORTED FORMATS:
		case ResourceDimension_TextureCUBE:
		case ResourceDimension_Texture3D:
		case ResourceDimension_TextureCUBEArray:
		case ResourceDimension_Texture2DMS:
		case ResourceDimension_Texture2DMSArray:
		default:
			HFATAL("Not implemented!");
			break;
		}


		UINT uFormatSupportMask = D3D11_FORMAT_SUPPORT_DEPTH_STENCIL;
		PixelFormat kTypelessFormat = GetDepthFormatFromTypeless(_TexRef.m_Description.m_kFormat);

		if (pRenderDevice->GetDevice()->CheckFormatSupport(static_cast<DXGI_FORMAT>(_TexRef.m_Description.m_kFormat), &uFormatSupportMask) == false)
		{
			DSV_Desc.Format = static_cast<DXGI_FORMAT>(kTypelessFormat);
		}

		HRERROR(Result = pRenderDevice->GetDevice()->CreateDepthStencilView(_TexRef.m_pResource, &DSV_Desc, &_TexRef.m_pDepthStencilView));

		// Create Read-Only version
		DSV_Desc.Flags = D3D11_DSV_READ_ONLY_DEPTH;

		if (IsStencilFormat(kTypelessFormat))
		{
			DSV_Desc.Flags |= D3D11_DSV_READ_ONLY_STENCIL;
		}

		HRERROR(Result = pRenderDevice->GetDevice()->CreateDepthStencilView(_TexRef.m_pResource, &DSV_Desc, &_TexRef.m_pDepthStencilViewReadOnly));
	}

	// Create UnorderedAccessView
	if (_TexRef.m_Description.m_kBindFlag & ResourceBindFlag_UnorderedAccess && _TexRef.m_pUnorderedAccessView == nullptr)
	{
		if (_TexRef.m_Description.m_kBindFlag & ResourceBindFlag_RenderTarget)
		{
			HERROR("Texture %s cannot have both RTV and UAV at the same time!", CSTR(_TexRef.m_Description.m_sFilePathOrName));
			return false;
		}

		// https://msdn.microsoft.com/en-us/library/ff728749(v=vs.85).aspx
		//Create a texture with the appropriate TYPELESS - dependent format that is specified in the previous scenario together with the required bind flags,
		//such as D3D11_BIND_UNORDERED_ACCESS | D3D11_BIND_SHADER_RESOURCE.
		//For in - place image editing, create a UAV with the DXGI_FORMAT_R32_UINT format.The Direct3D 11 API typically does not allow casting between different format "families."
		//However, the Direct3D 11 API makes an exception with the DXGI_FORMAT_R32_UINT format.

		//In the compute shader or pixel shader, use the appropriate inline format pack and unpack functions that are provided in the D3DX_DXGIFormatConvert.inl file.
		
		D3D11_UNORDERED_ACCESS_VIEW_DESC UAVDesc = {};
		UAVDesc.Format = DXGI_FORMAT_R32_UINT;

		switch (_TexRef.m_Description.m_kResourceDimension)
		{
		case ResourceDimension_Texture1D:
			UAVDesc.ViewDimension = D3D11_UAV_DIMENSION_TEXTURE1D;
			UAVDesc.Texture1D.MipSlice = 0u;
			break;
		case ResourceDimension_Texture2D:
			UAVDesc.ViewDimension = D3D11_UAV_DIMENSION_TEXTURE2D;
			UAVDesc.Texture2D.MipSlice = 0u;
			break;
		case ResourceDimension_Texture3D:
			UAVDesc.ViewDimension = D3D11_UAV_DIMENSION_TEXTURE3D;
			UAVDesc.Texture3D.MipSlice = 0u;
			break;
		case ResourceDimension_Texture1DArray:
			UAVDesc.ViewDimension = D3D11_UAV_DIMENSION_TEXTURE1DARRAY;
			UAVDesc.Texture1DArray.ArraySize = _TexRef.m_Description.m_uArraySize;
			UAVDesc.Texture1DArray.FirstArraySlice = 0u;
			break;
		case ResourceDimension_Texture2DArray:
			UAVDesc.ViewDimension = D3D11_UAV_DIMENSION_TEXTURE2DARRAY;
			UAVDesc.Texture2DArray.ArraySize = _TexRef.m_Description.m_uArraySize;
			UAVDesc.Texture2DArray.FirstArraySlice = 0u;
			break;
		default:
			HERROR("Unsupported UAV ViewDimension %u", _TexRef.m_Description.m_kResourceDimension);
			return false;
		}

		// Pointer to an D3D11_UNORDERED_ACCESS_VIEW_DESC that represents a shader-resource-view description.
		// Set this parameter to NULL to create a view that accesses the entire resource (using the format the resource was created with).
		HRERROR(Result = pRenderDevice->GetDevice()->CreateUnorderedAccessView(_TexRef.m_pResource, nullptr, &_TexRef.m_pUnorderedAccessView));
	}

	return Result == S_OK;
}
//---------------------------------------------------------------------------------------------------
bool TextureDX11::Resize(uint32_t _uWidth, uint32_t _uHeight, uint32_t _uDepth)
{
	HASSERTD(m_pReference->m_pResource != nullptr, "Invalid texture!");

	ID3D11Device* pRenderDevice = RenderDeviceDX11::Instance()->GetDevice();

	HRESULT Result = S_OK;

	if (m_pReference->m_Description.m_kResourceDimension == ResourceDimension_Texture1D)
	{
		if (_uWidth == 0)
		{
			return true;
		}

		D3D11_TEXTURE1D_DESC Desc;

		m_pReference->m_pTexture1D->GetDesc(&Desc);

		if (Desc.Width != _uWidth)
		{
			m_pReference->Reset();

			Desc.Width = _uWidth;
			m_pReference->m_Description.m_uWidth = _uWidth;

			HRERROR(Result = (pRenderDevice->CreateTexture1D(&Desc, nullptr, &m_pReference->m_pTexture1D)));
		}
	}
	else if (m_pReference->m_Description.m_kResourceDimension == ResourceDimension_Texture2D)
	{
		if (_uWidth == 0 || _uHeight == 0)
		{
			return true;
		}

		D3D11_TEXTURE2D_DESC Desc;

		m_pReference->m_pTexture2D->GetDesc(&Desc);

		if (Desc.Width != _uWidth || Desc.Height != _uHeight)
		{
			m_pReference->Reset();

			Desc.Width = _uWidth;
			Desc.Height = _uHeight;

			m_pReference->m_Description.m_uWidth = _uWidth;
			m_pReference->m_Description.m_uHeight = _uHeight;

			HRERROR(Result = (pRenderDevice->CreateTexture2D(&Desc, nullptr, &m_pReference->m_pTexture2D)));
		}
	}
	else if (m_pReference->m_Description.m_kResourceDimension == ResourceDimension_Texture3D)
	{
		if (_uWidth == 0 || _uHeight == 0 || _uDepth == 0)
		{
			return true;
		}

		D3D11_TEXTURE3D_DESC Desc;

		m_pReference->m_pTexture3D->GetDesc(&Desc);

		if (Desc.Width != _uWidth || Desc.Height != _uHeight || Desc.Depth != _uDepth)
		{
			m_pReference->Reset();

			Desc.Width = _uWidth;
			Desc.Height = _uHeight;
			Desc.Depth = _uDepth;

			m_pReference->m_Description.m_uWidth = _uWidth;
			m_pReference->m_Description.m_uHeight = _uHeight;
			m_pReference->m_Description.m_uDepth = _uDepth;

			HRERROR(Result = (pRenderDevice->CreateTexture3D(&Desc, nullptr, &m_pReference->m_pTexture3D)));
		}
	}

	OnResize(_uWidth, _uHeight, _uDepth);

	return Result == S_OK && CreateViews(*m_pReference);
}
//---------------------------------------------------------------------------------------------------

const TextureType TextureDX11::GetTextureType() const
{
	const TextureDesc& Desc = GetDescription();

	if (Desc.m_kBindFlag & ResourceBindFlag_RenderTarget &&
		Desc.m_kUsageFlag == ResourceUsageFlag_Default &&
		Desc.m_kCPUAccessFlag == CPUAccessFlag_None && 
		Desc.m_kResourceDimension == ResourceDimension_Texture2D)
	{
		if (Desc.m_sFilePathOrName == "BackBuffer")
		{
			return TextureType_BackBufferTexture;
		}
		else
		{
			return TextureType_RenderTargetTexture;
		}
	} 

	if (Desc.m_kBindFlag & ResourceBindFlag_DepthStencil &&
		Desc.m_kUsageFlag == ResourceUsageFlag_Default &&
		Desc.m_kCPUAccessFlag == CPUAccessFlag_None &&
		Desc.m_kResourceDimension == ResourceDimension_Texture2D)
	{
		return TextureType_DepthStencilTexture;
	}

	if (Desc.m_kBindFlag == ResourceBindFlag_ShaderResource &&
		Desc.m_kCPUAccessFlag == CPUAccessFlag_None &&
		Desc.m_kResourceDimension == ResourceDimension_Texture2D)
	{
		if (Desc.m_kUsageFlag == ResourceUsageFlag_Immutable)
		{
			return TextureType_ImmutableTexture;
		}
		else if (Desc.m_kUsageFlag == ResourceUsageFlag_Default)
		{
			return TextureType_DefaultTexture;
		}
	}

	return TextureType_Unknown;
}

//---------------------------------------------------------------------------------------------------

TextureReferenceDX11::~TextureReferenceDX11()
{
	if (m_ResizeProperties.sEventName.empty() == false)
	{
		TextureResizeEventManagerDX11::Instance()->Remove(m_ResizeProperties.sEventName, m_uKey);
	}

	Reset();
}
//---------------------------------------------------------------------------------------------------

void TextureReferenceDX11::Reset()
{
	HSAFE_RELEASE(m_pResourceView);
	HSAFE_RELEASE(m_pDepthStencilView);
	HSAFE_RELEASE(m_pDepthStencilViewReadOnly);
	HSAFE_RELEASE(m_pRenderTargetView);
	HSAFE_RELEASE(m_pUnorderedAccessView);
	//any texture pointer will do since this is a union
	HSAFE_RELEASE(m_pResource);
}
//---------------------------------------------------------------------------------------------------
