//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef SHADERRENDERTARGETDX11_H
#define SHADERRENDERTARGETDX11_H

#include "TextureVariantsDX11.h"

namespace Helix
{
	namespace Display
	{
		class ShaderInterfaceDX11;

		class ShaderRenderTargetDX11
		{
		public:
			HDEBUGNAME("ShaderRenderTargetDX11");

			ShaderRenderTargetDX11(
				const std::string& _sRenderTargetName,
				const std::string& _sMappedName = {},
				const std::string& _sMappedCopyName = {});

			~ShaderRenderTargetDX11();

			bool Initialize(ShaderInterfaceDX11* _pInterface, const std::string& _sShaderInstanceName);

			void SetTexture(const RenderTargetTextureDX11& _Texture);
			const RenderTargetTextureDX11& GetTexture() const;
			RenderTargetTextureDX11& GetTexture();

			bool Copy(bool _bShaderResourceOnly = false);
			const DefaultTextureDX11& GetCopy() const;

			const std::string& GetName() const;

			const std::string& GetMappedName() const;
			void SetMappedName(const std::string& _sMappedName);

			const std::string& GetMappedCopyName() const;
			void SetMappedCopyName(const std::string& _sMappedCopyName);

			const uint32_t& GetSlot() const;
		private:
			const std::string m_sName;
			std::string m_sShaderInstanceName;
			std::string m_sMappedName;
			std::string m_sMappedCopyName;
			uint32_t m_uSlot = HUNDEFINED32;

			ShaderInterfaceDX11* m_pInterface = nullptr;

			RenderTargetTextureDX11 m_RenderTargetTexture = nullptr; //Shader output texture
			DefaultTextureDX11 m_RenderTargetCopy = nullptr;
		};

		inline RenderTargetTextureDX11& ShaderRenderTargetDX11::GetTexture() { return m_RenderTargetTexture; }
		inline const RenderTargetTextureDX11& ShaderRenderTargetDX11::GetTexture() const { return m_RenderTargetTexture; }
		inline const std::string& ShaderRenderTargetDX11::GetName() const { return m_sName; }
		inline const std::string& ShaderRenderTargetDX11::GetMappedName() const { return m_sMappedName; }
		inline void ShaderRenderTargetDX11::SetMappedName(const std::string& _sMappedName) { m_sMappedName = _sMappedName; }
		inline const std::string& ShaderRenderTargetDX11::GetMappedCopyName() const	{return m_sMappedCopyName;}
		inline void ShaderRenderTargetDX11::SetMappedCopyName(const std::string& _sMappedCopyName) {m_sMappedCopyName = _sMappedCopyName;}
		inline const uint32_t & ShaderRenderTargetDX11::GetSlot() const	{return m_uSlot;}
		inline const DefaultTextureDX11& ShaderRenderTargetDX11::GetCopy() const {return m_RenderTargetCopy;}
	} // Display
} // Helix

#endif // !SHADERRENDERTARGETDX11_H
