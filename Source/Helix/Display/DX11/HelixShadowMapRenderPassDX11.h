//   Copyright 2020 Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef HELIXSHADOWMAPRENDERPASSDX11_H
#define HELIXSHADOWMAPRENDERPASSDX11_H

#include "RenderPassDX11.h"
#include "CommonConstantBufferFunctions.h"
#include "Scene\CameraComponent.h"

namespace Helix
{
	namespace Display
	{
		class HelixShadowMapRenderPassDX11 : public RenderPassDX11
		{
		public:
			HDEBUGNAME("HelixShadowMapRenderPassDX11");
			
			HelixShadowMapRenderPassDX11(DefaultInitializerType);
			HelixShadowMapRenderPassDX11(const std::string& _sInstanceName = {});
			~HelixShadowMapRenderPassDX11();

			bool OnInitalize(const hlx::TextToken& _CustomVariables) final;

			bool OnPerCamera(const Scene::CameraComponent& _Camera) final;
			bool OnPerObject(const RenderObjectDX11& _RenderObject, const MaterialDX11& _Material) final;

			bool OnPreRender() final;

			std::shared_ptr<RenderPassDX11> CreatePass(const std::string& _sInstanceName) const final;

		private:
			TCBPerCamera m_CBPerCamera = { "cbPerCamera" };
			TCBPerObject m_CBPerObject = { "cbPerObject" };

			Scene::CameraComponent* m_pLightSpaceCamera = nullptr;
			Scene::CameraComponent* m_pMainCamera = nullptr;
		};

		//---------------------------------------------------------------------------------------------------
		inline bool HelixShadowMapRenderPassDX11::OnPerObject(const RenderObjectDX11& _RenderObject, const MaterialDX11& _Material)
		{
			SetObjectConstants(m_CBPerObject, _RenderObject); // Dont set MaterialPoperties

			return true;
		}
	} // Display
} // Helix

#endif