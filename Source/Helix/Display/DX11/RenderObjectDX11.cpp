//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "RenderObjectDX11.h"
#include "Math\MathFunctions.h"
#include "DataStructures\GameObjectDescription.h"

using namespace Helix::Datastructures;
using namespace Helix::Display;
using namespace Helix::Math;

Helix::UniqueID<uint32_t> RenderObjectDX11::NewMatID;
//---------------------------------------------------------------------------------------------------
RenderObjectDX11::RenderObjectDX11(IBaseObject* _pParent) :
	IBaseObject(ObjectType_RenderObject, _pParent)
{
#ifdef HNUCLEO
	m_ObjectGroup.AddVariable(&m_SyncRenderPosition);
	m_ObjectGroup.AddVariable(&m_SyncRenderOrientation);
#endif
}
//---------------------------------------------------------------------------------------------------
RenderObjectDX11::RenderObjectDX11(GOIDType, IBaseObject* _pParent) :
	IBaseObject(ObjectType_GameObject, _pParent)
{
}

//---------------------------------------------------------------------------------------------------
RenderObjectDX11::~RenderObjectDX11()
{
}
//---------------------------------------------------------------------------------------------------

std::string RenderObjectDX11::GetNewMaterialName(const std::string& _sMeshName)
{
	return GetNewMaterialName(m_MeshCluster.GetClusterName(), _sMeshName);
}
//---------------------------------------------------------------------------------------------------
std::string RenderObjectDX11::GetNewMaterialName(const std::string& _sClusterName, const std::string& _sMeshName)
{
	return _sClusterName + "_" + _sMeshName + "_Mat" + std::to_string(NewMatID.NextID());
}
//---------------------------------------------------------------------------------------------------
#ifdef HNUCLEO
void RenderObjectDX11::SetMeshFromFilePath(const std::string& _sFilePath)
{
	Scene::RenderingSceneGuard Guard;

	if (_sFilePath.empty())
	{
		m_MeshCluster = MeshClusterDX11(nullptr);
	}
	else
	{
		m_MeshCluster = MeshClusterDX11(_sFilePath);
	}

	m_SyncSubMeshes.SetList(&m_MeshCluster.GetSubMeshes());

	if (m_MeshCluster)
	{
		m_Materials.clear();

		uint64_t uDefaultPasses = MaterialPoolDX11::GetDefaultRenderPasses();

		for (const SubMesh& Mesh : m_MeshCluster.GetSubMeshes())
		{
			// create new empty material
			m_Materials.push_back(std::move(MaterialDX11::CreateNew(GetNewMaterialName(Mesh.m_sSubName))));
			// assign submesh
			m_Materials.back().GetProperties().sAssociatedSubMesh = Mesh.m_sSubName;
			m_Materials.back().GetProperties().kRenderPasses = uDefaultPasses;
		}
		SelectSubMesh(0, false);
	}
}
//---------------------------------------------------------------------------------------------------
std::string RenderObjectDX11::GetMeshFilePath() const
{
	std::string sPath;
	if (m_MeshCluster)
	{
		sPath = m_MeshCluster.GetReferenceConst()->sClusterName;
	}

	return sPath;
}
//---------------------------------------------------------------------------------------------------

void RenderObjectDX11::RemoveSubMesh()
{
	if (m_MeshCluster.GetSubMeshes().size() <= 1u)
		return;

	Scene::RenderingSceneGuard Guard;

	m_MeshCluster.RemoveSubMesh(m_sSelectedSubMesh);
	auto it = std::remove_if(m_Materials.begin(), m_Materials.end(), [&](const MaterialDX11& Mat) { return Mat.GetProperties().sAssociatedSubMesh == m_sSelectedSubMesh; });
	if (it != m_Materials.end())
	{
		m_sSelectedSubMesh.clear();;
		m_ObjectDock.RemoveGroup(&it->GetMaterialGroup());
		m_Materials.erase(it);
	}
}
//---------------------------------------------------------------------------------------------------
void RenderObjectDX11::AddSubMesh()
{
	Scene::RenderingSceneGuard Guard;

	m_MeshCluster.AddSubMesh(m_sSelectedSubMesh);
	m_ObjectDock.RemoveGroup(m_pMaterialGroup);

	auto it = std::find_if(m_Materials.begin(), m_Materials.end(), [&](const MaterialDX11& Mat) { return Mat.GetProperties().sAssociatedSubMesh == m_sSelectedSubMesh; });
	if (it != m_Materials.end())
	{
		m_pMaterialGroup = &it->GetMaterialGroup();
	}
	else
	{
		m_Materials.push_back(std::move(MaterialDX11::CreateNew(GetNewMaterialName(m_sSelectedSubMesh))));

		MaterialDX11& Mat = m_Materials.back();
		Mat.GetProperties().sAssociatedSubMesh = m_sSelectedSubMesh;

		m_pMaterialGroup = &Mat.GetMaterialGroup();
	}
	m_ObjectDock.AddGroup(m_pMaterialGroup);
}
//---------------------------------------------------------------------------------------------------

std::string RenderObjectDX11::GetSubMeshMaterialPath() const
{
	if (m_sSelectedSubMesh.empty())
		return m_sSelectedSubMesh;

	auto it = std::find_if(m_Materials.begin(), m_Materials.end(), [&](const MaterialDX11& Mat) { return Mat.GetProperties().sAssociatedSubMesh == m_sSelectedSubMesh; });

	if (it != m_Materials.end())
	{
		return it->GetPathOrName();
	}

	return{};
}
//---------------------------------------------------------------------------------------------------

void RenderObjectDX11::SetSubMeshMaterialPath(const std::string& _sPath)
{
	if (m_sSelectedSubMesh.empty())
		return;

	auto it = std::find_if(m_Materials.begin(), m_Materials.end(), [&](const MaterialDX11& Mat) { return Mat.GetProperties().sAssociatedSubMesh == m_sSelectedSubMesh; });

	// material already assigned
	if (it != m_Materials.end())
	{
		if (_sPath.empty()) // clear material
		{
			*it = nullptr;
		}
		else if(it->GetPathOrName() != _sPath)  // load new one
		{
			MaterialDX11 NewMat(_sPath);
			if (NewMat && NewMat.IsDefaultReference() == false)
			{
				*it = std::move(NewMat);
			}
		}
	}
	else
	{
		if (_sPath.empty()) // insert empty
		{
			it = m_Materials.insert(m_Materials.end(), nullptr);
		}
		else // non found, insert new one
		{
			it = m_Materials.insert(m_Materials.end(), std::move(MaterialDX11(_sPath)));
		}		
	}

	it->GetProperties().sAssociatedSubMesh = m_sSelectedSubMesh;
}
//---------------------------------------------------------------------------------------------------
void RenderObjectDX11::SelectSubMesh(int _iIndex, bool _bBlocking)
{
	if (_iIndex >= m_MeshCluster.GetSubMeshes().size())
		return;

	if (_bBlocking)
	{
		Scene::RenderingSceneGuard::Lock();
	}

	m_sSelectedSubMesh = m_MeshCluster.GetSubMeshes()[_iIndex].m_sSubName;

	Datastructures::SyncGroup* pNewMatGroup = nullptr;
	auto it = std::find_if(m_Materials.begin(), m_Materials.end(), [&](const MaterialDX11& Mat) { return Mat.GetProperties().sAssociatedSubMesh == m_sSelectedSubMesh; });
	if (it != m_Materials.end())
	{
		pNewMatGroup = &it->GetMaterialGroup();
	}

	if (pNewMatGroup != m_pMaterialGroup)
	{
		if (m_pMaterialGroup != nullptr)
		{
			m_ObjectDock.RemoveGroup(m_pMaterialGroup);
		}

		m_ObjectDock.AddGroup(pNewMatGroup);
		m_pMaterialGroup = pNewMatGroup;
	}

	if (_bBlocking)
	{
		Scene::RenderingSceneGuard::Unlock();
	}
}
#endif
//---------------------------------------------------------------------------------------------------
void RenderObjectDX11::Uninitialize()
{
	m_sName.resize(0);
	m_Materials.resize(0);
	m_MeshCluster = nullptr;

	m_uVertexCount = 0;
	m_kPrimitiveTopology = PrimitiveTopology_Undefined;

	m_Transform = HTRANSFORM_IDENTITY;
	m_vRenderingScale = { 1.f,1.f,1.f };

#ifdef HNUCLEO
	m_ObjectDock.Unbind();
	m_ComponentDock.Unbind();
	m_ComponentDock.ClearGroups();
#endif // HNUCLEO

	IBaseObject::Uninitialize();
}
//---------------------------------------------------------------------------------------------------
void RenderObjectDX11::GetObjectToWorldMatrix(_Out_ float4x4& _mObjectToWorld) const
{
	GetTransformationMatrix(m_Transform, m_vRenderingScale, _mObjectToWorld);
}
//---------------------------------------------------------------------------------------------------
void RenderObjectDX11::GetObjectToWorldMatrix(_Out_ float4x4& _mObjectToWorld, _Out_ float4x4& _mNormal) const
{
	GetTransformationAndNormalMatrix(m_Transform, m_vRenderingScale, _mObjectToWorld, _mNormal);
}
//---------------------------------------------------------------------------------------------------
void RenderObjectDX11::Initialize(const GameObjectDesc& _Desc)
{
	//HASSERT(_Desc.kType == GameObjectType_RenderObject, "Invalid GameObjectType");

	SetName(_Desc.sObjectName);
	m_Transform = _Desc.Transform;
	m_vRenderingScale = _Desc.vRenderingScale;

	// meshes must be loaded before the shape to ensure the convex mesh is loaded from the h3d
	SetMeshFromDesc(_Desc);

#ifdef _FINAL
	for (const std::string& sMatName : _MatPropNames)
	{
		AddMaterial(sMatName);
	}
#else
	LoadMaterialsRelaxed(_Desc.sMaterialNames);
#endif // _FINAL

}
//---------------------------------------------------------------------------------------------------
void RenderObjectDX11::LoadMaterialsRelaxed(const std::vector<std::string>& _MatPropNames)
{
	const TMeshCluster& SubMeshes = m_MeshCluster.GetSubMeshes();
	size_t uSubMeshes = SubMeshes.size();

	// add materials
	for (const std::string& sMatName : _MatPropNames)
	{
		AddMaterial(sMatName);

		// check whether this material is actually referenced if it has a non empty submesh association
		std::string& sSubName = m_Materials.back().GetProperties().sAssociatedSubMesh;
		if (sSubName.empty() == false)
		{
			bool bFound = false;
			for (const SubMesh& Mesh : SubMeshes)
			{
				if (Mesh.m_sSubName == sSubName)
				{
					bFound = true;
					break;
				}
			}

			if (bFound == false)
			{
				HWARNING("Material %s (%s) does not match any SubMesh of object %s, removing reference!",
					CSTR(m_Materials.back().GetPathOrName()), CSTR(sSubName), CSTR(m_sName));
				sSubName.clear();
			}
		}
	}

	// fix submesh assignments
	for (const SubMesh& Mesh : SubMeshes)
	{
		auto it = std::find_if(m_Materials.begin(), m_Materials.end(),
			[&](const MaterialDX11& _Mat) {return _Mat.GetProperties().sAssociatedSubMesh == Mesh.m_sSubName || _Mat.GetProperties().sAssociatedSubMesh.empty(); });

		// check if each submesh has a material assigned
		if (it != m_Materials.end())
		{
			if (it->GetProperties().sAssociatedSubMesh.empty())
			{
				HWARNING("Unable to find a material associated to SubMesh %s, assigning Material %s", CSTR(Mesh.m_sSubName), CSTR(it->GetPathOrName()));
				it->GetProperties().sAssociatedSubMesh = Mesh.m_sSubName;
			}
		}
		else // no assigned or free material found, create new one
		{
			std::string sNewName = GetNewMaterialName(Mesh.m_sSubName);
			HWARNING("Unable to find a material associated to SubMesh %s, assigning new Material %s", CSTR(Mesh.m_sSubName), CSTR(sNewName));
			m_Materials.push_back(std::move(MaterialDX11::CreateNew(sNewName)));
			m_Materials.back().GetProperties().sAssociatedSubMesh = Mesh.m_sSubName;
		}
	}

	// remove unassigned materials
	{
		auto it = std::remove_if(m_Materials.begin(), m_Materials.end(), [](const MaterialDX11& _Mat) {return _Mat.GetProperties().sAssociatedSubMesh.empty(); });
		if (it != m_Materials.end())
		{
			//HWARNING("Object %s has unassigned Materials (%s), removing references!", CSTR(_Desc.sObjectName), CSTR(it->GetPathOrName()));
			m_Materials.erase(it, m_Materials.end());
		}
	}

	if (m_Materials.size() > uSubMeshes)
	{
		HWARNING("To many SubMaterials for Object %s", CSTR(m_sName));
	}

	if (uSubMeshes != 0 && m_Materials.empty())
	{
		HERROR("Object %s has no SubMeshes but no Materials assigned", CSTR(m_sName));
	}

	UpdateMaterialMask();
}
//---------------------------------------------------------------------------------------------------
void RenderObjectDX11::SetMeshFromDesc(const GameObjectDesc& _Desc)
{
	if (_Desc.sMeshClusterSubNames.empty() && _Desc.sMeshFileName.empty() == false)
	{
		SetMesh(_Desc.sMeshFileName);
	}
	else if (_Desc.sMeshClusterSubNames.empty() == false && _Desc.sMeshFileName.empty() == false)
	{
		SetMesh(MeshClusterDX11(_Desc.sMeshFileName, _Desc.sMeshClusterSubNames));
	}

	HNCL(SelectSubMesh(0));
}
//---------------------------------------------------------------------------------------------------

void RenderObjectDX11::BindToNucleo()
{
#ifdef HNUCLEO
	SelectSubMesh(0);
	m_ObjectDock.Bind();
	m_ComponentDock.Bind();
#endif
}
//---------------------------------------------------------------------------------------------------

void RenderObjectDX11::AddSyncComponent(SyncComponent* _pSyncComponent)
{
#ifdef HNUCLEO
	if (_pSyncComponent != nullptr)
	{
		m_ComponentDock.AddGroup(_pSyncComponent);
		//m_ComponentDock.Bind();
	}
#endif
}
//---------------------------------------------------------------------------------------------------
GameObjectDesc RenderObjectDX11::CreateDescription() const
{
	GameObjectDesc Desc;
	Desc.kType = GameObjectType_RenderObject;

	Desc.Transform = GetTransform();
	Desc.vRenderingScale = GetRenderingScale();
	Desc.sObjectName = m_sName;

	if (m_MeshCluster/* && m_MeshCluster.IsDefaultReference() == false*/)
	{
		Desc.sMeshFileName = m_MeshCluster.GetClusterName();
		for (const SubMesh& Mesh : m_MeshCluster.GetSubMeshes())
		{
			Desc.sMeshClusterSubNames.push_back(Mesh.m_sSubName);
		}
	}

	for (const MaterialDX11& Mat : m_Materials)
	{
		if (Mat/* && Mat.IsDefaultReference() == false*/)
		{
			const std::string sMatPropName = Mat.GetPathOrName() + "_" + std::to_string(Mat.GetExportHash());
			Desc.sMaterialNames.push_back(sMatPropName);
		}
	}

	return Desc;
}

//---------------------------------------------------------------------------------------------------
void RenderObjectDX11::UpdateMaterialMask()
{
	m_uMaterialMask = 0;

	for (const MaterialDX11& Mat : m_Materials)
	{
		m_uMaterialMask |= Mat.GetProperties().GetMask();
	}
}
//---------------------------------------------------------------------------------------------------
