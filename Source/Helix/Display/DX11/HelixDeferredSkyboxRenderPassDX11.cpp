//   Copyright 2020 Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "HelixDeferredSkyboxRenderPassDX11.h"
#include "Scene\LightContext.h"

using namespace Helix;
using namespace Helix::Display;
using namespace Helix::Scene;
using namespace Helix::Datastructures;
using namespace Helix::Math;
using namespace Helix::Shaders::ConstantBuffers;
using namespace Helix::Shaders::DeferredSkybox;

//---------------------------------------------------------------------------------------------------
HelixDeferredSkyboxRenderPassDX11::HelixDeferredSkyboxRenderPassDX11(const std::string& _sInstanceName) :
	RenderPassDX11("DeferredSkybox", _sInstanceName,
	{ &m_DepthTex, &m_Skybox, &m_RandomTexture }, // Textures
	{ &m_AlbedoTarget }, //RenderTargets
	{ /*&m_CBPerFrame, */ &m_CBPerCamera, &m_CBDeferredSkybox })  // Variables
{
}
//---------------------------------------------------------------------------------------------------
HelixDeferredSkyboxRenderPassDX11::HelixDeferredSkyboxRenderPassDX11(DefaultInitializerType) : RenderPassDX11(DefaultInit, "DeferredSkybox")
{
}
//---------------------------------------------------------------------------------------------------
HelixDeferredSkyboxRenderPassDX11::~HelixDeferredSkyboxRenderPassDX11()
{
}
//---------------------------------------------------------------------------------------------------
std::shared_ptr<RenderPassDX11> HelixDeferredSkyboxRenderPassDX11::CreatePass(const std::string& _sInstanceName) const
{
	return std::make_shared<HelixDeferredSkyboxRenderPassDX11>(_sInstanceName);
}
//---------------------------------------------------------------------------------------------------
bool HelixDeferredSkyboxRenderPassDX11::OnInitalize(const hlx::TextToken& _CustomVariables)
{
	// Add RasterizerState
	RasterizerDesc RasDesc = {};
	RasDesc.m_kFillMode = FillMode_Solid;
	RasDesc.m_kCullMode = CullMode_None;
	RasDesc.m_bFrontCounterClockwise = false;

	SetRasterizerState(RasDesc);

	// Add BlendState
	RenderTargetBlendDesc RTBlendDesc = {};
	RTBlendDesc.m_bBlendEnable = false;
	RTBlendDesc.m_uRenderTargetWriteMask = Channel_All;

	BlendDesc AlphaBlendDesc = {};
	AlphaBlendDesc.m_bAlphaToCoverageEnable = false;
	AlphaBlendDesc.m_RenderTargets[0] = RTBlendDesc;
	AlphaBlendDesc.m_uSampleMask = 0xffffffff;
	AlphaBlendDesc.fBlendFactor = { 0.0f,0.0f,0.0f,0.0f };

	SetBlendState(AlphaBlendDesc);

	// Add DepthStencilState
	DepthStencilDesc DepthDesc = {};
	DepthDesc.m_bDepthEnable = true;
	DepthDesc.m_kDepthWriteMask = DepthWriteMask_Zero;
	DepthDesc.m_kDepthFunc = ComparisonFunction_Equal;
	DepthDesc.m_bStencilEnable = false;

	SetDepthStencilState(DepthDesc, "DeferredSkybox");

	// Default Parameter
	ClearRenderTargets(false);

	return m_bInitialized;
}
//---------------------------------------------------------------------------------------------------
bool HelixDeferredSkyboxRenderPassDX11::OnPerFrame()
{
	const TextureCubeDX11& Skybox = LightContext::Instance()->GetSkybox();
	if (Skybox != nullptr && Skybox.IsDefaultReference() == false)
	{
		m_Skybox.SetTexture(Skybox);
	}
	else
	{
		return false;
	}

	uint32_t uIOPerm = m_bDebug ? Shaders::DeferredSkybox::HPMIO_Debug : 0u;

	LightDesc DirLight;
	if (LightContext::Instance()->GetMainDirLight(DirLight))
	{
		float3 vToSunDir = -1.0f * DirLight.vDirection;
		m_CBDeferredSkybox->vToSunDir = float4(vToSunDir, 0.0f);
		m_CBDeferredSkybox->vSunLightColor = DirLight.vColor;
		uIOPerm |= m_bUseLenseflare ? Shaders::DeferredSkybox::HPMIO_Lenseflare : 0u;
	}

	GetShader()->SetIOPermutation(uIOPerm);
	Select(ShaderType_PixelShader, 0);
	Select(ShaderType_VertexShader, 0);

	return true;
}
//---------------------------------------------------------------------------------------------------
bool HelixDeferredSkyboxRenderPassDX11::OnPerCamera(const CameraComponent& _Camera)
{
	SetCameraConstants(m_CBPerCamera, _Camera.GetRenderingProperties(), true);

	m_CBDeferredSkybox->vViewDir = _Camera.GetRenderingProperties().m_vLook;

	return true;
}
//---------------------------------------------------------------------------------------------------
