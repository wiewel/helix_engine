//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "RenderPassCreatorDX11.h"
#include "RenderPassDX11.h"

using namespace Helix::Display;

RenderPassCreatorDX11::RenderPassCreatorDX11()
{
}
//---------------------------------------------------------------------------------------------------

RenderPassCreatorDX11::~RenderPassCreatorDX11()
{
}
//---------------------------------------------------------------------------------------------------

std::shared_ptr<RenderPassDX11> RenderPassCreatorDX11::RegisterPrototype(const std::shared_ptr<RenderPassDX11>& _pPrototype)
{
	HASSERTD(_pPrototype->IsPrototype(), "RenderPass instance is not a prototype!");

	m_Prototypes.insert({ _pPrototype->GetTypeName(), _pPrototype });

	return nullptr;
}
//---------------------------------------------------------------------------------------------------

std::shared_ptr<RenderPassDX11> RenderPassCreatorDX11::CreatePass(const std::string& _sTypeName, const std::string& _sInstanceName) const
{
	TRenderPassPrototypeMap::const_iterator it = m_Prototypes.find(_sTypeName);

	if (it != m_Prototypes.cend())
	{
		return it->second->CreatePass(_sInstanceName);
	}

	return nullptr;
}
//---------------------------------------------------------------------------------------------------
