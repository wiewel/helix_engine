//   Copyright 2020 Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "HelixLuminanceAdaptionRenderPassDX11.h"
#include "Scene\RenderingScene.h"

using namespace Helix;
using namespace Helix::Display;
using namespace Helix::Scene;
using namespace Helix::Datastructures;

//---------------------------------------------------------------------------------------------------

HelixLuminanceAdaptionRenderPassDX11::HelixLuminanceAdaptionRenderPassDX11(DefaultInitializerType) : RenderPassDX11(DefaultInit, "LuminanceAdaption")
{
}

//---------------------------------------------------------------------------------------------------
HelixLuminanceAdaptionRenderPassDX11::HelixLuminanceAdaptionRenderPassDX11(const std::string& _sInstanceName) :
	RenderPassDX11("LuminanceAdaption", _sInstanceName,
	{ &m_LastLuminance, &m_CurrentLuminance },
	{ &m_OutputTexture },
	{ &m_CBLuminanceAdaption, &m_CBPerFrame })
{
}
//---------------------------------------------------------------------------------------------------
HelixLuminanceAdaptionRenderPassDX11::~HelixLuminanceAdaptionRenderPassDX11()
{
}
//---------------------------------------------------------------------------------------------------
std::shared_ptr<RenderPassDX11> HelixLuminanceAdaptionRenderPassDX11::CreatePass(const std::string& _sInstanceName) const
{
	return std::make_shared<HelixLuminanceAdaptionRenderPassDX11>(_sInstanceName);
}
//---------------------------------------------------------------------------------------------------
bool HelixLuminanceAdaptionRenderPassDX11::OnInitalize(const hlx::TextToken& _CustomVariables)
{
	// Add RasterizerState
	RasterizerDesc RasDesc = {};
	RasDesc.m_kFillMode = FillMode_Solid;
	RasDesc.m_kCullMode = CullMode_None;
	RasDesc.m_bFrontCounterClockwise = false;

	SetRasterizerState(RasDesc);

	// Add BlendState
	RenderTargetBlendDesc RTBlendDesc = {};
	RTBlendDesc.m_bBlendEnable = false;

	BlendDesc AlphaBlendDesc = {};
	AlphaBlendDesc.m_bAlphaToCoverageEnable = false;
	AlphaBlendDesc.m_RenderTargets[0] = RTBlendDesc;
	AlphaBlendDesc.m_uSampleMask = 0xffffffff;
	AlphaBlendDesc.fBlendFactor = { 0.0f,0.0f,0.0f,0.0f };

	SetBlendState(AlphaBlendDesc);

	// Add DepthStencilState
	DepthStencilDesc DepthDesc = {};
	DepthDesc.m_bDepthEnable = false;
	DepthDesc.m_bStencilEnable = false;

	SetDepthStencilState(DepthDesc, "Luminance");

	// Default Parameter
	ClearRenderTargets(false);

	return m_bInitialized;
}
//---------------------------------------------------------------------------------------------------
bool HelixLuminanceAdaptionRenderPassDX11::OnPerFrame()
{
	m_CBLuminanceAdaption->fAdaptionRate = m_fAdaptionRate;
	
	float fDeltaTime = static_cast<float>(RenderingScene::Instance()->GetDeltaTime());
	m_CBPerFrame->fDeltaTime = std::max(std::min(fDeltaTime, 0.5f), 0.0001f);

	return true;
}
//---------------------------------------------------------------------------------------------------
bool HelixLuminanceAdaptionRenderPassDX11::OnPreRender()
{
	uint32_t uLastTarget = m_uCurrentTarget;
	m_uCurrentTarget = (m_uCurrentTarget + 1) % 2;

	SetInputTexture(m_AdaptionTargets[uLastTarget], "gLastLuminance", false);
	SetOutputTexture(m_AdaptionTargets[m_uCurrentTarget], "LUMINANCE_ADAPTION_MAP");

	return true;
}
//---------------------------------------------------------------------------------------------------
void HelixLuminanceAdaptionRenderPassDX11::OnPostRender()
{
	m_OutputTexture.GetTexture().GenerateMips();
}
//---------------------------------------------------------------------------------------------------