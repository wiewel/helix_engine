//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "BlendStateDX11.h"
#include "hlx\src\Logger.h"
#include "ViewDefinesDX11.h"

using namespace Helix::Display;

//---------------------------------------------------------------------------------------------------
BlendStateDX11::BlendStateDX11(const BlendDesc& _BlendDesc, const char* _pDbgName) :
	m_Description(_BlendDesc)
{
	m_pReference = BlendStatePoolDX11::Instance()->Get(ConvertToD3D11Description(_BlendDesc), m_Hash);
	
	if (_pDbgName == nullptr)
	{
		HDXDEBUGNAME(m_pReference, "DXBlendState_%u", m_Hash);
	}
	else
	{
		HDXDEBUGNAME(m_pReference, "DXBlendState_%s", _pDbgName);
	}
}
//---------------------------------------------------------------------------------------------------
BlendStateDX11::~BlendStateDX11()
{
}
//---------------------------------------------------------------------------------------------------

BlendStateDX11::BlendStateDX11(const BlendStateDX11& _Other) : 
	TDX11BlendStateRef(_Other), m_Description(_Other.m_Description)
{
}
//---------------------------------------------------------------------------------------------------

BlendStateDX11& BlendStateDX11::operator=(const BlendStateDX11& _Other)
{
	if (this == &_Other)
		return *this;

	TDX11BlendStateRef::operator=(_Other);
	m_Description = _Other.m_Description;
	return *this;
}
//---------------------------------------------------------------------------------------------------