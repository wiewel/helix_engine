//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "hlx\src\Logger.h"
#include "Util\Functional.h"

#include "Resources\RenderPassDescription.h"
#include "Scene\CameraManager.h"
#include "Scene\CameraComponent.h"

#include "Display\RenderPassRegister.h"

#include "RenderPassDX11.h"
#include "ShaderDX11.h"
#include "RenderDeviceDX11.h"
#include "RenderObjectDX11.h"
#include "ShaderPermutationPoolDX11.h"

#include "hlx\src\TextToken.h"

#ifdef HNUCLEO
#include "Compiler\PermutationCompiler.h"
#include "Compiler\ShaderDocument.h"
#include "Resources\FileSystem.h"
#endif

using namespace Helix;
using namespace Helix::Display;
using namespace Helix::Scene;
using namespace Helix::Datastructures;
using namespace Helix::Resources;

//---------------------------------------------------------------------------------------------------
// protected
RenderPassDX11::RenderPassDX11(DefaultInitializerType, const std::string& _sTypeName) :
	m_bIsPrototype(true),
	m_sTypeName(_sTypeName),
	//m_sInstanceName(""),
	m_kIdentifier(RenderPassRegister::Instance()->GetIdentifier(_sTypeName)),
	m_RenderDeviceDraw(nullptr)
{
}

//---------------------------------------------------------------------------------------------------
RenderPassDX11::RenderPassDX11(
	const std::string& _sTypeName,
	const std::string& _sInstanceName,
	const std::vector<ShaderTextureDX11*>& _Textures,
	const std::vector<ShaderRenderTargetDX11*>& _RenderTargets,
	const std::vector<ShaderVariable*>& _Buffers,
	const std::vector<ShaderSamplerDX11*>& _Samplers) :
	m_bIsPrototype(false),
	m_sTypeName(_sTypeName),
	m_sInstanceName(_sInstanceName),
	m_kIdentifier(RenderPassRegister::Instance()->GetIdentifier(_sTypeName)),
	m_Textures(_Textures),
	m_RenderTargets(_RenderTargets),
	m_Buffers(_Buffers),
	m_Samplers(_Samplers),
	m_pRenderDevice(RenderDeviceDX11::Instance()),
	m_RenderDeviceDraw(RenderDeviceDX11::Instance())
{
}

//---------------------------------------------------------------------------------------------------
RenderPassDX11::~RenderPassDX11()
{
	HSAFE_VECTOR_DELETE(m_DefaultSamplers);
}
//---------------------------------------------------------------------------------------------------
bool RenderPassDX11::Initialize(const RenderPassDesc& _Desc)
{
	// skip if already initialized
	if (m_bInitialized)
	{
		return true;
	}

	if (m_kIdentifier != HPASS(_Desc.sPassType))
	{
		HFATAL("Invalid pass type identifier %s", CSTR(_Desc.sPassType));
		return false;
	}

	HASSERT(m_pRenderDevice->IsInitialized(), "RenderDevice has not been initialized yet!");

	m_bClearRenderTargets = _Desc.bClearRenderTargets;
	m_bEnabled = _Desc.bEnabled;

	SetShader(_Desc.sShaderName, m_sInstanceName);

	if (m_pShader == nullptr)
	{
		m_bInitialized = false;
		return m_bInitialized;
	}

	m_bComputePass = m_pShader->HasComputeShader();	
	ShaderInterfaceDX11* pInterface = m_pShader->GetInterface();

	m_bInitialized = true;

	// Init variables
	for (ShaderVariable* pVar : m_Buffers)
	{
		bool bSuccess = pVar->Initialize(pInterface, m_sInstanceName);
#ifdef _FINAL
		m_bInitialized &= bSuccess;
#endif
	}

	if (m_bInitialized == false)
	{
		HERROR("Failed to initialize variables for %s pass", m_pShader->GetName().c_str());
		return false;
	}

	// Init samplers
	if (m_Samplers.empty())
	{
		// Add default samplers and check if they exist in the interface
		for (uint32_t i = 0; i < DefaultSamplerState_NumOf; ++i)
		{
			ShaderSamplerDX11* pDefaultSampler = new ShaderSamplerDX11(DefaultSamplerStates[i].pName);
			// interface knows this sampler
			if (pDefaultSampler->Initialize(pInterface, m_sInstanceName, false))
			{
				m_Samplers.push_back(pDefaultSampler);
				m_DefaultSamplers.push_back(pDefaultSampler);
			}
			else
			{
				HSAFE_DELETE(pDefaultSampler);
			}
		}
	}
	else
	{
		for (ShaderSamplerDX11* pSampler : m_Samplers)
		{
			m_bInitialized &= pSampler->Initialize(pInterface, m_sInstanceName, true);

			// m_bUseDefaultSampler will be ignored if theres a description for this sampler
			if (m_bInitialized)
			{
				// use sampler config from description
				for (const SamplerDescEx& SDesc : _Desc.Samplers)
				{
					if (pSampler->GetName() == SDesc.sName)
					{
						pSampler->SetSampler(SamplerStateDX11(SDesc.Description));
						break;
					}
				}
			}
		}
	}

	if (m_bInitialized == false)
	{
		HERROR("Failed to initialize sampler states for %s pass", m_pShader->GetName().c_str());
		return false;
	}

	// Init textures
	for (ShaderTextureDX11* pTexture : m_Textures)
	{
		// apply inputmapping to inputtextures of this shader
		for (const InputMapping& Mapping : _Desc.InputMappings)
		{
			if (Mapping.sName == pTexture->GetName())
			{
				if (Mapping.sMappedName.empty() == false)
				{
					pTexture->SetMappedName(Mapping.sMappedName);
				}

				break;
			}
		}

		m_bInitialized &= pTexture->Initialize(pInterface, m_sInstanceName);
	}

	if (m_bInitialized == false)
	{
		HERROR("Failed to initialize textures for %s pass", m_pShader->GetName().c_str());
		return false;
	}

	BlendDesc ImmediateBlendDesc = {};
	ImmediateBlendDesc.fBlendFactor = _Desc.BlendDescription.fBlendFactor;
	ImmediateBlendDesc.m_bAlphaToCoverageEnable = _Desc.BlendDescription.bAlphaToCoverageEnable;
	ImmediateBlendDesc.m_bIndependentBlendEnable = _Desc.BlendDescription.bIndependentBlendEnable;
	ImmediateBlendDesc.m_uSampleMask = _Desc.BlendDescription.uSampleMask;

	// init rendertargets
	for (ShaderRenderTargetDX11* pShdrRenderTarget : m_RenderTargets) 
	{
		// apply inputmapping to renderagets of this shader
		for (const InputMapping& Mapping : _Desc.InputMappings)
		{
			if (Mapping.sName == pShdrRenderTarget->GetName())
			{
				if (Mapping.sMappedName.empty() == false)
				{
					pShdrRenderTarget->SetMappedName(Mapping.sMappedName);
				}

				if (Mapping.sMappedCopyName.empty() == false)
				{
					pShdrRenderTarget->SetMappedCopyName(Mapping.sMappedCopyName);
				}

				break;
			}
		}

		// Initialize shader texture with interface
		m_bInitialized &= pShdrRenderTarget->Initialize(pInterface, m_sInstanceName);

		// create and assign rendertargets
		for (const RenderTargetTextureDesc& RTDesc : _Desc.RenderTargets)
		{
			HASSERT(RTDesc.sName.empty() == false, "Invalid RenderTargetDesc Name (empty) for pass %s", CSTR(m_sInstanceName));

			if (RTDesc.sName == pShdrRenderTarget->GetName())
			{
				const uint32_t& uSlot = pShdrRenderTarget->GetSlot();
				if (uSlot < ImmediateBlendDesc.m_RenderTargets.size())
				{
					ImmediateBlendDesc.m_RenderTargets[uSlot] = RTDesc.BlendDesc;
				}

				if (RTDesc.bBackBuffer)
				{
					pShdrRenderTarget->SetTexture(*BackBufferTextureDX11::Instance());
				}
				else
				{
					RenderTargetTextureDX11 RenderTarget;
					
					if (RTDesc.bRWTexture)
					{
						RenderTarget = RenderTargetTextureDX11(RTDesc.uWidth, RTDesc.sName, RTDesc.kFormat, RTDesc.bShaderResource, RTDesc.uHeight);
					}
					else // Regular RenderTarget
					{
						RenderTarget = RenderTargetTextureDX11(RTDesc.uWidth, RTDesc.uWidth, RTDesc.sName, RTDesc.kFormat, RTDesc.bShaderResource);
					}

					if (RTDesc.ResizeProps.sEventName.empty() == false)
					{
						RenderTarget.SetResizeEvent(RTDesc.ResizeProps.sEventName, RTDesc.ResizeProps.fWidthScale, RTDesc.ResizeProps.fHeightScale);
					}
					
					RenderTarget.SetDefaultClearColor(RTDesc.vClearColor);

					pShdrRenderTarget->SetTexture(RenderTarget);
				}

				continue;
			}
		}
	}

	if (m_bInitialized == false)
	{
		HERROR("Failed to initialize rendertargets for %s pass", m_pShader->GetName().c_str());
		return false;
	}

	// set render states
	m_BlendState = BlendStateDX11(ImmediateBlendDesc);
	m_RasterizerState = RasterizerStateDX11(_Desc.RasterizerDescription);
	m_DepthStencilState = DepthStencilStateDX11(_Desc.DepthStencilDescription);

	// create depthstencil texture
	if (_Desc.DepthStencilDescription.m_bDepthEnable || _Desc.DepthStencilDescription.m_bStencilEnable)
	{
		const DepthStencilTextureDesc& TDesc = _Desc.DepthStencilTextureDescription;
		m_sDepthStencilMappedName = TDesc.sMappedName;

		m_DepthStencilTexture = DepthStencilTextureDX11(TDesc.uWidth, TDesc.uHeight, "DepthStencil_" + m_sInstanceName, TDesc.kFormat, TDesc.bShaderResource);

		m_DepthStencilTexture.SetDefaultClearDepth(TDesc.fClearDepth);
		m_DepthStencilTexture.SetDefaultClearStencil(TDesc.uClearStencil);

		if (TDesc.ResizeProps.sEventName.empty() == false)
		{
			m_DepthStencilTexture.SetResizeEvent(TDesc.ResizeProps.sEventName, TDesc.ResizeProps.fWidthScale, TDesc.ResizeProps.fHeightScale);
		}
	}

	// call custom child class initialize funtion to initialize any member
	m_bInitialized = OnInitalize(_Desc.CustomVariables);
	if (m_bInitialized == false)
	{
		HERROR("Failed to initialize %s pass", m_pShader->GetName().c_str());
		return false;
	}

	m_bInitialized = OnSelectDefault();
	if (m_bInitialized == false)
	{
		HERROR("Failed to select default permutations for %s", m_pShader->GetName().c_str());
		return false;
	}

	return m_bInitialized;
}
//---------------------------------------------------------------------------------------------------
bool RenderPassDX11::DefaultInitialize()
{
	HLOG("Default initializing pass %s %s", CSTR(m_sTypeName), CSTR(m_sInstanceName));
	// skip if already initialized
	if (m_bInitialized)
	{
		return true;
	}

	HASSERT(m_pRenderDevice->IsInitialized(), "RenderDevice has not been initialized yet!");

	// load shader if not already set by SetShader
	if (m_pShader == nullptr)
	{
		SetShader(m_sTypeName, m_sInstanceName);
	}

	if (m_pShader == nullptr)
	{
		m_bInitialized = false;
		return m_bInitialized;
	}
		
	m_bComputePass = m_pShader->HasComputeShader();

	ShaderInterfaceDX11* pInterface = m_pShader->GetInterface();

	m_bInitialized = true;

	// Init variables
	for (ShaderVariable* pVar : m_Buffers)
	{
		bool bSuccess = pVar->Initialize(pInterface, m_sInstanceName);
#ifdef _FINAL
		m_bInitialized &= bSuccess;
#endif
	}

	if (m_bInitialized == false)
	{
		HERROR("Failed to initialize variables for %s pass", m_pShader->GetName().c_str());
		return false;
	}

	// Init samplers
	if (m_Samplers.empty())
	{
		// Add default samplers and check if they exist in the interface
		for (uint32_t i = 0; i < DefaultSamplerState_NumOf; ++i)
		{
			ShaderSamplerDX11* pDefaultSampler = new ShaderSamplerDX11(DefaultSamplerStates[i].pName);
			// interface knows this sampler
			if (pDefaultSampler->Initialize(pInterface, m_sInstanceName, false))
			{
				m_Samplers.push_back(pDefaultSampler);
				m_DefaultSamplers.push_back(pDefaultSampler);
			}
			else
			{
				HSAFE_DELETE(pDefaultSampler);
			}
		}
	}
	else
	{
		for (ShaderSamplerDX11* pSampler : m_Samplers)
		{
			m_bInitialized &= pSampler->Initialize(pInterface, m_sInstanceName, true);
		}
	}

	if (m_bInitialized == false)
	{
		HERROR("Failed to initialize sampler states for %s pass", m_pShader->GetName().c_str());
		return false;
	}

	// Init textures
	for (ShaderTextureDX11* pTexture : m_Textures)
	{
		m_bInitialized &= pTexture->Initialize(pInterface, m_sInstanceName);
	}

	if (m_bInitialized == false)
	{
		HERROR("Failed to initialize textures for %s pass", m_pShader->GetName().c_str());
		return false;
	}

	// init rendertargets
	for (ShaderRenderTargetDX11* pShdrRenderTarget : m_RenderTargets)
	{
		// Initialize shader texture with interface
		m_bInitialized &= pShdrRenderTarget->Initialize(pInterface, m_sInstanceName);
	}

	if (m_bInitialized == false)
	{
		HERROR("Failed to initialize rendertargets for %s pass", m_pShader->GetName().c_str());
		return false;
	}

	// call custom child class initialize funtion to initialize any member
	m_bInitialized = OnInitalize({});
	if (m_bInitialized == false)
	{
		HERROR("Failed to initialize %s pass", m_pShader->GetName().c_str());
		return false;
	}

	m_bInitialized = OnSelectDefault();
	if (m_bInitialized == false)
	{
		HERROR("Failed to select default permutations for %s", m_pShader->GetName().c_str());
		return false;
	}

	return m_bInitialized;
}
//---------------------------------------------------------------------------------------------------
bool RenderPassDX11::SetShader(const std::string& _sShaderName, const Helix::HashedStringValue& _sInstanceName, bool _bReload)
{
#ifdef HNUCLEO
	// clean up dock / groups
	for (Datastructures::SyncGroup* pGroup : m_SyncBufferGroups)
	{
		m_SyncDock.RemoveGroup(pGroup);
	}
	m_SyncBufferGroups.clear();

#endif

	m_pShader = ShaderPermutationPoolDX11::Instance()->Load(STR(_sShaderName), _bReload);

	if (m_pShader != nullptr)
	{
		RenderingSceneGuard Lock;

		m_pShader->GetInterface()->SetInstanceName(_sInstanceName);
		m_pShader->GetInterface()->CreateBuffers();
	}

	if (_bReload)
	{
		RenderingSceneGuard Lock;

		ApplyInstance();
		OnSelectDefault();
	}

#ifdef HNUCLEO
	if (m_pShader != nullptr)
	{
		std::vector<std::shared_ptr<GPUBufferInstanceDX11>> Buffers = m_pShader->GetInterface()->GetBuffersForInstance(m_sInstanceName);
		for (std::shared_ptr<GPUBufferInstanceDX11>& pBuffer : Buffers)
		{
			Datastructures::SyncGroup* pGroup = pBuffer->GetSyncGroup();
			if (pGroup != nullptr)
			{
				m_SyncDock.AddGroup(pGroup);
				m_SyncBufferGroups.push_back(pGroup);
			}
		}
	}
#endif

	return m_pShader != nullptr;
}
//---------------------------------------------------------------------------------------------------
RenderPassDesc RenderPassDX11::CreateDescription() const
{
	RenderPassDesc Desc = {};

	Desc.sPassInstanceName = m_sInstanceName;
	Desc.sPassType = m_sTypeName;

	if (m_pShader == nullptr)
	{
		HERROR("Unable to create description: no valid shader has been set!");
		return Desc;
	}
	
	Desc.sShaderName = hlx::to_sstring(m_pShader->GetName());

	Desc.bEnabled = m_bEnabled;
	Desc.bClearRenderTargets = m_bClearRenderTargets;

	const BlendDesc& BlendDesc = m_BlendState.GetDescription();
	Desc.BlendDescription.bAlphaToCoverageEnable = BlendDesc.m_bAlphaToCoverageEnable;
	Desc.BlendDescription.bIndependentBlendEnable = BlendDesc.m_bIndependentBlendEnable;
	Desc.BlendDescription.fBlendFactor = BlendDesc.fBlendFactor;
	Desc.BlendDescription.uSampleMask = BlendDesc.m_uSampleMask;

	Desc.RasterizerDescription = m_RasterizerState.GetDescription();
	Desc.DepthStencilDescription = m_DepthStencilState.GetDescription();

	// serialize custom samplers
	for (const ShaderSamplerDX11* pSampler : m_Samplers)
	{
		if (pSampler->IsDefaultSampler() == false)
		{
			SamplerDescEx SDesc = {};
			SDesc.sName = pSampler->GetName();
			SDesc.Description = pSampler->GetSampler().GetDescription();
			Desc.Samplers.push_back(SDesc);
		}
	}

	// serialize rendertargets
	for (const ShaderRenderTargetDX11* pRenderTarget : m_RenderTargets)
	{
		InputMapping Mapping = {};
		Mapping.sName = pRenderTarget->GetName();
		Mapping.sMappedName = pRenderTarget->GetMappedName();
		Mapping.sMappedCopyName = pRenderTarget->GetMappedCopyName();

		Desc.InputMappings.push_back(Mapping);

		RenderTargetTextureDesc RTDesc = {};
		RTDesc.sName = Mapping.sName;

		// assing rendertarget blend desc
		const uint32_t& uSlot = pRenderTarget->GetSlot();
		if (uSlot < m_BlendState.GetDescription().m_RenderTargets.size())
		{
			RTDesc.BlendDesc = m_BlendState.GetDescription().m_RenderTargets[uSlot];
		}

		// get info from assigned texture
		const RenderTargetTextureDX11& RTTexture = pRenderTarget->GetTexture();
		if (RTTexture)
		{
			const TextureDesc& TexDesc = RTTexture.GetDescription();
			const TextureResizeProperties& ResizeProps = RTTexture.GetResizeProperties();

			RTDesc.bBackBuffer = RTTexture.GetTextureType() == TextureType_BackBufferTexture;
			RTDesc.bShaderResource = TexDesc.m_kBindFlag & ResourceBindFlag_ShaderResource;
			RTDesc.bRWTexture = RTTexture.IsRWTexture();
			RTDesc.kFormat = TexDesc.m_kFormat;

			if (RTDesc.bBackBuffer)
			{
				RTDesc.uWidth = 0u;
				RTDesc.uHeight = 0u;
			}
			else
			{
				RTDesc.uWidth = TexDesc.m_uWidth;
				RTDesc.uHeight = TexDesc.m_uHeight;
			}

			if (ResizeProps.sEventName.empty() == false)
			{
				RTDesc.ResizeProps = ResizeProps;
			}
		}

		Desc.RenderTargets.push_back(RTDesc);		
	}

	// serialize texture mappings
	for (const ShaderTextureDX11* pTexture : m_Textures)
	{
		InputMapping Mapping = {};
		Mapping.sName = pTexture->GetName();
		Mapping.sMappedName = pTexture->GetMappedName();

		Desc.InputMappings.push_back(Mapping);
	}

	// custom variables
	Desc.CustomVariables.setKey("CustomVariables");
	OnSerializeCustomVariables(Desc.CustomVariables);

	return Desc;
}
//---------------------------------------------------------------------------------------------------
void RenderPassDX11::Apply()
{
	HASSERTD(m_bInitialized, "RenderPass has not been initialized yet!");
	
	m_pRenderDevice->SetShader(m_pShader);
	m_pShader->ReEnableStages();

	if(OnPerFrame() == false)
		return;

	if (m_bClearRenderTargets)
	{
		for (ShaderRenderTargetDX11* pRenderTarget : m_RenderTargets)
		{
			RenderTargetTextureDX11& Texture = pRenderTarget->GetTexture();
			if (Texture)
			{
				Texture.Clear();
			}
		}
	}

	if (m_bClearDepthStencil && m_DepthStencilTexture)
	{
		m_DepthStencilTexture.Clear();
	}

	// depthstencil state MUST be set before the texture!
	m_pRenderDevice->SetDepthStencilState(m_DepthStencilState);

	m_pRenderDevice->SelectRenderTargets(m_DepthStencilTexture);

	m_pRenderDevice->SetBlendState(m_BlendState);

	m_pRenderDevice->SetRasterizerState(m_RasterizerState);

	if (m_bComputePass)
	{
		static constexpr uint32_t MaxCount = D3D11_CS_DISPATCH_MAX_THREAD_GROUPS_PER_DIMENSION;
		
		bool bExecute = true;

		while (bExecute)
		{
			uint32_t uThreadGroupCountX = 1u;
			uint32_t uThreadGroupCountY = 1u;
			uint32_t uThreadGroupCountZ = 1u;

			// execute before selecting resources to be able to change the active permutation in OnDispatch
			bExecute = OnDispatch(uThreadGroupCountX, uThreadGroupCountY, uThreadGroupCountZ);

			m_RenderDeviceDraw.SelectShaders();
			m_RenderDeviceDraw.SelectSamplers();
			m_RenderDeviceDraw.SelectBuffers();

			if (m_RenderDeviceDraw.SelectTextures() == false)
			{
				HERROR("SelectTextures failed for compute pass %s", CSTR(m_sTypeName));
			}

			HASSERT(uThreadGroupCountX < MaxCount && uThreadGroupCountY < MaxCount && uThreadGroupCountZ < MaxCount, "Invalid Parameters for compute shader Dispatch (>0xffff)");
		
			m_pRenderDevice->GetContext()->Dispatch(uThreadGroupCountX, uThreadGroupCountY, uThreadGroupCountZ);

			OnPostDispatch();
		}
	}
	else // Regular Render Pass
	{
		std::vector<CameraComponent*> ActiveCameras;

		Scene::CameraManager::Instance()->GetActiveCameras(ActiveCameras, m_kIdentifier);

		for (CameraComponent* pCamera : ActiveCameras)
		{
			if(OnPerCamera(*pCamera) == false)
				continue;

			// Render GameObjects
			TVisibleObjectDistances& GameObjects = pCamera->GetVisibleObjects(m_kIdentifier);
			for (const GOCamDist& GO : GameObjects)
			{
				DrawObject(*GO.pObject);
			}

			// Render RenderObjects
			for (const RenderObjectDX11* pObject : pCamera->GetRenderObjects())
			{
				// check if the object has any material that uses this pass (render objects dont get sorted into pass lists)
				if (pObject->CheckRenderPass(m_kIdentifier))
				{
					DrawObject(*pObject);
				}
			}
		}
	}	
	
	// save render outputs if needed
	for (ShaderRenderTargetDX11* pRenderTarget : m_RenderTargets)
	{
		if (pRenderTarget->GetMappedCopyName().empty() == false)
		{
			pRenderTarget->Copy();
		}
	}
}
//--------------------------------------------------------------------------------------------------

void RenderPassDX11::DrawObject(const RenderObjectDX11& _Object)
{
	const std::vector<MaterialDX11>& Materials = _Object.GetMaterials();

	if (Materials.empty())
	{
		HERROR("Drawing %s without any material is not supported", CSTR(_Object.GetName()));
		return;
	}

	const MeshClusterDX11& Cluster = _Object.GetMesh();
	
	if (Cluster && Cluster.GetSubMeshes().empty() == false)
	{
		const GPUBufferDX11* pVertexBuffer = Cluster.GetVertexBuffer();
		if (pVertexBuffer == nullptr)
		{
			HERROR("MeshCluster has no vertex data!");
			return;
		}

		const GPUBufferDX11* pIndexBuffer = Cluster.GetIndexBuffer();

		bool bFound = false;

		for (const SubMesh& Mesh : Cluster.GetSubMeshes())
		{
			for (const MaterialDX11& Mat : Materials)
			{
				if (Mat.CheckRenderPass(m_kIdentifier))
				{
					if (Mesh.m_sSubName == Mat.GetProperties().sAssociatedSubMesh ||
						Materials.size() == 1u)
					{
						if(OnPerObject(_Object, Mat) == false)
							continue;

						m_RenderDeviceDraw.DrawSubMesh(Mesh, pVertexBuffer, pIndexBuffer, _Object.GetName());

						// found the matching material
						bFound = true;
						break;
					}
				}
			}
		}

		if (bFound == false)
		{
			HERROR("No associated material was found for cluster %s", CSTR(Cluster.GetReferenceConst()->sClusterName));
		}
	}
	else if(_Object.GetVertexCount() > 0u) // draw render object
	{
		for (const MaterialDX11& Mat : Materials)
		{
			if (Mat.CheckRenderPass(m_kIdentifier))
			{
				if (OnPerObject(_Object, Mat) == false)
					continue;

				m_RenderDeviceDraw.DrawRenderObject(_Object);
			}
		}
	}
	//else
	//{
	//	HERROR("Object %s does not have any vertex/mesh data to draw", CSTR(_Object.GetName()));
	//}
}

//--------------------------------------------------------------------------------------------------
void RenderPassDX11::SetRasterizerState(const RasterizerDesc& _RasterizerDesc, const char* _pDbgName)
{
	m_RasterizerState = RasterizerStateDX11(_RasterizerDesc, _pDbgName);
}
//--------------------------------------------------------------------------------------------------
void RenderPassDX11::SetDepthStencilState(const DepthStencilDesc& _DepthStencilDesc, const char* _pDbgName)
{
	m_DepthStencilState = DepthStencilStateDX11(_DepthStencilDesc, _pDbgName);
}
//--------------------------------------------------------------------------------------------------
void RenderPassDX11::SetBlendState(const BlendDesc& _BlendDesc, const char* _pDbgName)
{
	m_BlendState = BlendStateDX11(_BlendDesc, _pDbgName);
}
//---------------------------------------------------------------------------------------------------

bool RenderPassDX11::SetInputTexture(const TextureDX11& _Texture, const std::string& _sName, const bool _bMatchMappedName, const uint32_t _uArrayIndex)
{
	for (ShaderTextureDX11* pShaderTexture : m_Textures)
	{
		const std::string& sNameToMatch = _bMatchMappedName ? pShaderTexture->GetMappedName() : pShaderTexture->GetName();
		if (sNameToMatch == _sName)
		{
			pShaderTexture->SetTexture(_Texture, _uArrayIndex);
			return true;
		}
	}

	HERROR("No shader input texture with name %s found for pass %s", CSTR(_sName), CSTR(m_sTypeName));
	return false;
}
//---------------------------------------------------------------------------------------------------
bool RenderPassDX11::SetOutputTexture(const RenderTargetTextureDX11& _RenderTarget, const std::string& _sName, const bool _bMatchMappedName)
{
	for (ShaderRenderTargetDX11* pShaderRenderTarget : m_RenderTargets)
	{
		const std::string& sNameToMatch = _bMatchMappedName ? pShaderRenderTarget->GetMappedName() : pShaderRenderTarget->GetName();
		if (sNameToMatch == _sName)
		{
			pShaderRenderTarget->SetTexture(_RenderTarget);
			return true;
		}
	}

	HERROR("No rendertarget texture with name %s found for pass %s", _sName.c_str(), m_sTypeName.c_str());
	return false;
}

//---------------------------------------------------------------------------------------------------

const TextureDX11* RenderPassDX11::GetOutputTexture(const std::string& _sMappedName) const
{
	for (const ShaderRenderTargetDX11* pRenderTarget : m_RenderTargets)
	{
		if (pRenderTarget->GetMappedName() == _sMappedName)
		{
			return &pRenderTarget->GetTexture();
		}
		else if (pRenderTarget->GetMappedCopyName() == _sMappedName)
		{
			return &pRenderTarget->GetCopy();
		}
	}
	
	return nullptr;
}
//---------------------------------------------------------------------------------------------------
void RenderPassDX11::ApplyInstance()
{
	if(m_pShader != nullptr)
	{
		m_pShader->GetInterface()->SetInstanceName(m_sInstanceName);
		// not neccessary if reloading is done from the gui
		//m_pShader->GetInterface()->CreateBuffers(); // in case the shader was reloaded
		m_pShader->ReSelectInterface();
	}
}
//---------------------------------------------------------------------------------------------------
bool RenderPassDX11::OnSelectDefault()
{
	// select default permutation
	m_pShader->SetIOPermutation(0u);

	bool bSelected = true;

	for (const ShaderFunction& Func : m_pShader->GetFunctions())
	{
		bSelected &= m_pShader->Select(ShaderModelVersions[Func.kTargetVersion].kType, 0u);
	}

	return bSelected;
}
//---------------------------------------------------------------------------------------------------
std::string RenderPassDX11::GetPermutationName(const ShaderPermutationHash& _Perm) const
{
	return m_sTypeName +
		" IO " + std::to_string(_Perm.uIOPermutation) +
		" Class " + std::to_string(_Perm.uClassPermutation) +
		" Range " + std::to_string(_Perm.GetRangeHash());
}
//---------------------------------------------------------------------------------------------------
void RenderPassDX11::BindToNucleo()
{
#ifdef HNUCLEO
	m_SyncDock.Bind();
#endif
}
//---------------------------------------------------------------------------------------------------

#ifdef HNUCLEO
bool RenderPassDX11::CompileShader(const std::string& _sPath)
{	
	hlx::string sPath(STR(_sPath));

	Compiler::ShaderDocument ShaderFile(sPath);

	if (ShaderFile.IsOpen() == false)
	{
		return false;
	}

	Resources::FileSystem::Instance()->AddMiscFilesFromDirectory(Resources::FileSystem::GetDirectoryFromPath(STR(_sPath)));

	// add default functions
	ShaderFile.AddShaderFunction({ "VShader", Display::ShaderModel_VS_5_0 });
	ShaderFile.AddShaderFunction({ "PShader", Display::ShaderModel_PS_5_0 });
	ShaderFile.AddShaderFunction({ "DShader", Display::ShaderModel_DS_5_0 });
	ShaderFile.AddShaderFunction({ "GShader", Display::ShaderModel_GS_5_0 });
	ShaderFile.AddShaderFunction({ "HShader", Display::ShaderModel_HS_5_0 });
	ShaderFile.AddShaderFunction({ "CShader", Display::ShaderModel_CS_5_0 });

	Compiler::TCompilerFlags kFlags = Compiler::CompilerFlag_Enable_Strictness | Compiler::CompilerFlag_Optimization_Level3;
	ShaderFile.SetCompilationFlags(kFlags);

	ShaderFile.Parse();

	Compiler::PermutationCompiler Compiler(nullptr, true);
	Resources::ShaderCompilationResult& CompileOutput = Compiler.GetCompilationResult();

	if (Compiler.Compile(ShaderFile))
	{
		hlx::string sOutputFile = FileSystem::Instance()->GetKnownDirectoryPath(HelixDirectories_Shaders) + FileSystem::GetFileNameWithoutExtension(sPath) + STR(".hlxsl");

		hlx::fbytestream OutputFile(sOutputFile, std::ios::out | std::ios::binary);
		if (OutputFile.is_open() == false)
		{
			HERROR("Failed to open output file %s", CSTR(sOutputFile));
			return false;
		}

		HLOG("Assembling %s", CSTR(CompileOutput.GetShaderName()));

		hlx::bytes OutputBuffer;
		hlx::bytestream OutputStream(OutputBuffer);

		Resources::HLXSLFile HLXSL;

		HLXSL.Write(OutputStream, CompileOutput);

		HLOG("Writing %s [%.2fkb]", CSTR(sOutputFile), (float)OutputBuffer.size() / 1024.f);

		OutputFile.put<hlx::bytes>(OutputBuffer);
		OutputFile.close();

		HLOG("Compilation finished =)");

		return true;
	}

	return false;
}
#endif
//--------------------------------------------------------------------------------------------------
