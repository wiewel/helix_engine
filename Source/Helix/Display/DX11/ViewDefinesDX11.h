//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef VIEWDEFINESDX11_H
#define VIEWDEFINESDX11_H

#include <cstdarg>
#include <stdlib.h>

#include <tchar.h>
#include <strsafe.h>

#include "Display\ViewDefines.h"
#include <d3d11.h>
#include <d3d11shader.h>
#include "hlx\src\StringHelpers.h"
#include "hlx\src\StandardDefines.h"
#include "hlx\src\CheckedArrayIterator.h"
#include "Util\BaseHash.h"

//#ifdef _DEBUG

#define HDXDEBUGNAME(_pObject, _format, ...) { SetDebugNameDX11(_pObject, _format, __VA_ARGS__); }
#define HDXDEBUGBREAK(_pObject, _name) { BreakOnDebugNameDX11(_pObject, _name); }
#define HDXGETDEBUGNAME(_pObject) GetDebugNameDX11(_pObject)
//---------------------------------------------------------------------------------------------------
template<class T>
static inline void SetDebugNameDX11(T* _pObject, const char* _pFormat, ...)
{
	if (_pObject != nullptr && _pFormat != nullptr)
	{
		char pMsgBuffer[256] = { 0 };

		va_list args;
		va_start(args, _pFormat);
		StringCbVPrintfA(pMsgBuffer, sizeof(pMsgBuffer), _pFormat, args);
		va_end(args);

		_pObject->SetPrivateData(WKPDID_D3DDebugObjectName, static_cast<UINT>(strlen(pMsgBuffer)), pMsgBuffer);
	}
}
//---------------------------------------------------------------------------------------------------
template<class T>
static inline void BreakOnDebugNameDX11(T* _pObject, const char* _pName)
{
	if (_pObject != nullptr && _pName != nullptr)
	{
		char pMsgBuffer[256] = { 0 };
		UINT uSize = 256;

		_pObject->GetPrivateData(WKPDID_D3DDebugObjectName, &uSize, pMsgBuffer);

		if (strcmp(pMsgBuffer, _pName) == 0)
		{
			DebugBreak();
		}
	}
}
//---------------------------------------------------------------------------------------------------
template<class T>
static inline const char* GetDebugNameDX11(T* _pObject)
{
	if (_pObject != nullptr)
	{
		static char pMsgBuffer[256] = { 0 };
		UINT uSize = 256;

		_pObject->GetPrivateData(WKPDID_D3DDebugObjectName, &uSize, pMsgBuffer);

		return pMsgBuffer;
	}

	return nullptr;
}
//---------------------------------------------------------------------------------------------------
//#else
//#define HDXDEBUGNAME(_pObject, _format, ...)
//#define HDXDEBUGBREAK(_pObject, _name)
//#define HDXGETDEBUGNAME(_pObject) nullptr
//#endif

namespace Helix
{
	namespace Display
	{
		union ShaderPtrDX11
		{
			ID3D11DeviceChild* pShader = nullptr;
			ID3D11VertexShader* pVertexShader;
			ID3D11HullShader* pHullShader;
			ID3D11DomainShader* pDomainShader;
			ID3D11GeometryShader* pGeometryShader;
			ID3D11PixelShader* pPixelShader;
			ID3D11ComputeShader* pComputeShader;

			ShaderPtrDX11() {};
			ShaderPtrDX11(std::nullptr_t) {};
			inline ShaderPtrDX11& operator=(std::nullptr_t) { pShader = nullptr; return *this;}
			inline bool operator==(const ShaderPtrDX11& _Other) const { return pShader == _Other.pShader; }
			inline bool operator!=(const ShaderPtrDX11& _Other) const { return pShader != _Other.pShader; }
			inline bool operator==(std::nullptr_t) const { return pShader == nullptr; }
			inline bool operator!=(std::nullptr_t) const { return pShader != nullptr; }
			inline explicit operator bool() const { return pShader != nullptr; }
		};

		struct VertexLayoutDX11
		{
			TVertexDeclaration kDeclaration = VertexLayout_Unknown;
			ID3D11InputLayout* pInputLayout = nullptr;
		};

		//---------------------------------------------------------------------------------------------------
		static inline TextureDesc ConvertToHelixDescription(const D3D11_TEXTURE1D_DESC& _D3D11Desc)
		{
			TextureDesc Desc = {};
			Desc.m_kBindFlag = static_cast<Helix::Display::ResourceBindFlag>(_D3D11Desc.BindFlags);
			Desc.m_kCPUAccessFlag = static_cast<Helix::Display::CPUAccessFlag>(_D3D11Desc.CPUAccessFlags);
			Desc.m_kFormat = static_cast<Helix::Display::PixelFormat>(_D3D11Desc.Format);
			Desc.m_kResourceFlag = static_cast<Helix::Display::MiscResourceFlag>(_D3D11Desc.MiscFlags);
			Desc.m_kResourceDimension = Helix::Display::ResourceDimension_Texture1D;
			Desc.m_kUsageFlag = static_cast<Helix::Display::ResourceUsageFlag>(_D3D11Desc.Usage);
			Desc.m_uArraySize = _D3D11Desc.ArraySize;
			Desc.m_uWidth = _D3D11Desc.Width;
			Desc.m_uMipLevels = _D3D11Desc.MipLevels;

			Desc.m_bNoInit = false;
			Desc.m_bLoadFromFile = false;
			Desc.m_bPooled = false;
			Desc.m_bForceSRgb = false;

			return Desc;
		}
		//---------------------------------------------------------------------------------------------------
		static inline TextureDesc ConvertToHelixDescription(const D3D11_TEXTURE2D_DESC& _D3D11Desc)
		{
			TextureDesc Desc = {};
			Desc.m_kBindFlag = static_cast<Helix::Display::ResourceBindFlag>(_D3D11Desc.BindFlags);
			Desc.m_kCPUAccessFlag = static_cast<Helix::Display::CPUAccessFlag>(_D3D11Desc.CPUAccessFlags);
			Desc.m_kFormat = static_cast<Helix::Display::PixelFormat>(_D3D11Desc.Format);
			Desc.m_kResourceFlag = static_cast<Helix::Display::MiscResourceFlag>(_D3D11Desc.MiscFlags);
			if (Desc.m_kResourceFlag.CheckFlag(MiscResourceFlag_TextureCube))
			{
				Desc.m_kResourceDimension = Helix::Display::ResourceDimension_TextureCUBE;
			}
			else
			{
				Desc.m_kResourceDimension = Helix::Display::ResourceDimension_Texture2D;
			}
			Desc.m_kUsageFlag = static_cast<Helix::Display::ResourceUsageFlag>(_D3D11Desc.Usage);
			Desc.m_uArraySize = _D3D11Desc.ArraySize;
			Desc.m_uHeight = _D3D11Desc.Height;
			Desc.m_uWidth = _D3D11Desc.Width;
			Desc.m_uMipLevels = _D3D11Desc.MipLevels;
			Desc.m_uSampleCount = _D3D11Desc.SampleDesc.Count;
			Desc.m_uSampleQuality = _D3D11Desc.SampleDesc.Quality;

			Desc.m_bNoInit = false;
			Desc.m_bLoadFromFile = false;
			Desc.m_bPooled = false;
			Desc.m_bForceSRgb = false;

			return Desc;
		}
		//---------------------------------------------------------------------------------------------------
		static inline TextureDesc ConvertToHelixDescription(const D3D11_TEXTURE3D_DESC& _D3D11Desc)
		{
			TextureDesc Desc = {};
			Desc.m_kBindFlag = static_cast<Helix::Display::ResourceBindFlag>(_D3D11Desc.BindFlags);
			Desc.m_kCPUAccessFlag = static_cast<Helix::Display::CPUAccessFlag>(_D3D11Desc.CPUAccessFlags);
			Desc.m_kFormat = static_cast<Helix::Display::PixelFormat>(_D3D11Desc.Format);
			Desc.m_kResourceFlag = static_cast<Helix::Display::MiscResourceFlag>(_D3D11Desc.MiscFlags);
			Desc.m_kResourceDimension = Helix::Display::ResourceDimension_Texture3D;
			Desc.m_kUsageFlag = static_cast<Helix::Display::ResourceUsageFlag>(_D3D11Desc.Usage);
			Desc.m_uHeight = _D3D11Desc.Height;
			Desc.m_uWidth = _D3D11Desc.Width;
			Desc.m_uMipLevels = _D3D11Desc.MipLevels;
			Desc.m_uDepth = _D3D11Desc.Depth;

			Desc.m_bNoInit = false;
			Desc.m_bLoadFromFile = false;
			Desc.m_bPooled = false;
			Desc.m_bForceSRgb = false;

			return Desc;
		}

		//---------------------------------------------------------------------------------------------------
		static inline SamplerDesc ConvertToHelixDescription(const D3D11_SAMPLER_DESC& _D3D11Desc)
		{
			SamplerDesc Desc = {};
			Desc.m_kFilter = static_cast<TextureFilter>(_D3D11Desc.Filter);
			Desc.m_kAddressU = static_cast<TextureAddressMode>(_D3D11Desc.AddressU);
			Desc.m_kAddressV = static_cast<TextureAddressMode>(_D3D11Desc.AddressV);
			Desc.m_kAddressW = static_cast<TextureAddressMode>(_D3D11Desc.AddressW);
			Desc.m_fMipLODBias = _D3D11Desc.MipLODBias;
			Desc.m_uMaxAnisotropy = _D3D11Desc.MaxAnisotropy;
			Desc.m_kComparisonFunction = static_cast<ComparisonFunction>(_D3D11Desc.ComparisonFunc);
			std::copy(std::cbegin(_D3D11Desc.BorderColor),std::cend(_D3D11Desc.BorderColor), std::begin(Desc.m_vBorderColor));
			Desc.m_fMaxLOD = _D3D11Desc.MaxLOD;
			Desc.m_fMinLOD = _D3D11Desc.MinLOD;

			return Desc;
		}

		//---------------------------------------------------------------------------------------------------
		static inline D3D11_SAMPLER_DESC ConvertToD3D11Description(const SamplerDesc& _HelixDesc)
		{
			D3D11_SAMPLER_DESC Desc = {};

			Desc.Filter = static_cast<D3D11_FILTER>(_HelixDesc.m_kFilter);
			Desc.AddressU = static_cast<D3D11_TEXTURE_ADDRESS_MODE>(_HelixDesc.m_kAddressU);
			Desc.AddressV = static_cast<D3D11_TEXTURE_ADDRESS_MODE>(_HelixDesc.m_kAddressV);
			Desc.AddressW = static_cast<D3D11_TEXTURE_ADDRESS_MODE>(_HelixDesc.m_kAddressW);
			Desc.MipLODBias = _HelixDesc.m_fMipLODBias;
			Desc.MaxAnisotropy = _HelixDesc.m_uMaxAnisotropy;
			Desc.ComparisonFunc = static_cast<D3D11_COMPARISON_FUNC>(_HelixDesc.m_kComparisonFunction);
			std::copy(std::cbegin(_HelixDesc.m_vBorderColor), std::cend(_HelixDesc.m_vBorderColor), hlx::make_checked(Desc.BorderColor));
			Desc.MinLOD = _HelixDesc.m_fMinLOD;
			Desc.MaxLOD = _HelixDesc.m_fMaxLOD;

			return Desc;
		}

		//---------------------------------------------------------------------------------------------------
		static inline RasterizerDesc ConvertToHelixDescription(const D3D11_RASTERIZER_DESC& _D3D11Desc)
		{
			RasterizerDesc Desc = {};

			Desc.m_kFillMode = static_cast<FillMode>(_D3D11Desc.FillMode);
			Desc.m_kCullMode = static_cast<CullMode>(_D3D11Desc.CullMode);

			Desc.m_bFrontCounterClockwise = _D3D11Desc.FrontCounterClockwise == TRUE;
			Desc.m_iDepthBias = _D3D11Desc.DepthBias;
			Desc.m_fDepthBiasClamp = _D3D11Desc.DepthBiasClamp;
			Desc.m_fSlopeScaledDepthBias = _D3D11Desc.SlopeScaledDepthBias;
			Desc.m_bDepthClipEnable = _D3D11Desc.DepthClipEnable == TRUE;
			Desc.m_bScissorEnable = _D3D11Desc.ScissorEnable == TRUE;
			Desc.m_bMultisampleEnable = _D3D11Desc.MultisampleEnable == TRUE;
			Desc.m_bAntialiasedLineEnable = _D3D11Desc.AntialiasedLineEnable == TRUE;

			return Desc;
		}

		//---------------------------------------------------------------------------------------------------
		static inline D3D11_RASTERIZER_DESC ConvertToD3D11Description(const RasterizerDesc& _HelixDesc)
		{
			D3D11_RASTERIZER_DESC Desc = {};

			Desc.FillMode = static_cast<D3D11_FILL_MODE>(_HelixDesc.m_kFillMode);
			Desc.CullMode = static_cast<D3D11_CULL_MODE>(_HelixDesc.m_kCullMode);

			Desc.FrontCounterClockwise = _HelixDesc.m_bFrontCounterClockwise ? TRUE : FALSE;
			Desc.DepthBias = _HelixDesc.m_iDepthBias;
			Desc.DepthBiasClamp = _HelixDesc.m_fDepthBiasClamp;
			Desc.SlopeScaledDepthBias = _HelixDesc.m_fSlopeScaledDepthBias;
			Desc.DepthClipEnable = _HelixDesc.m_bDepthClipEnable ? TRUE : FALSE;
			Desc.ScissorEnable = _HelixDesc.m_bScissorEnable ? TRUE : FALSE;
			Desc.MultisampleEnable = _HelixDesc.m_bMultisampleEnable ? TRUE : FALSE;
			Desc.AntialiasedLineEnable = _HelixDesc.m_bAntialiasedLineEnable ? TRUE : FALSE;

			return Desc;
		}
		//---------------------------------------------------------------------------------------------------

		static inline DepthStencilDesc ConvertToHelixDescription(const D3D11_DEPTH_STENCIL_DESC& _D3D11Desc)
		{
			DepthStencilDesc Desc = {};

			Desc.m_bDepthEnable = _D3D11Desc.DepthEnable == TRUE;
			Desc.m_kDepthWriteMask = static_cast<DepthWriteMask>(_D3D11Desc.DepthWriteMask);
			Desc.m_kDepthFunc = static_cast<ComparisonFunction>(_D3D11Desc.DepthFunc);
			Desc.m_bStencilEnable = _D3D11Desc.StencilEnable == TRUE;
			Desc.m_uStencilReadMask = _D3D11Desc.StencilReadMask == TRUE;
			Desc.m_uStencilWriteMask = _D3D11Desc.StencilWriteMask == TRUE;

			Desc.m_FrontFace.m_kStencilFailOp = static_cast<StencilOperation>(_D3D11Desc.FrontFace.StencilFailOp);
			Desc.m_FrontFace.m_kStencilDepthFailOp = static_cast<StencilOperation>(_D3D11Desc.FrontFace.StencilDepthFailOp);
			Desc.m_FrontFace.m_kStencilPassOp = static_cast<StencilOperation>(_D3D11Desc.FrontFace.StencilPassOp);
			Desc.m_FrontFace.m_kStencilFunc = static_cast<ComparisonFunction>(_D3D11Desc.FrontFace.StencilFunc);

			Desc.m_BackFace.m_kStencilFailOp = static_cast<StencilOperation>(_D3D11Desc.BackFace.StencilFailOp);
			Desc.m_BackFace.m_kStencilDepthFailOp = static_cast<StencilOperation>(_D3D11Desc.BackFace.StencilDepthFailOp);
			Desc.m_BackFace.m_kStencilPassOp = static_cast<StencilOperation>(_D3D11Desc.BackFace.StencilPassOp);
			Desc.m_BackFace.m_kStencilFunc = static_cast<ComparisonFunction>(_D3D11Desc.BackFace.StencilFunc);

			return Desc;
		}
		//---------------------------------------------------------------------------------------------------
		static inline D3D11_DEPTH_STENCIL_DESC ConvertToD3D11Description(const DepthStencilDesc& _HelixDesc)
		{
			D3D11_DEPTH_STENCIL_DESC Desc = {};

			Desc.DepthEnable = _HelixDesc.m_bDepthEnable ? TRUE : FALSE;
			Desc.DepthWriteMask = static_cast<D3D11_DEPTH_WRITE_MASK>(_HelixDesc.m_kDepthWriteMask);
			Desc.DepthFunc = static_cast<D3D11_COMPARISON_FUNC>(_HelixDesc.m_kDepthFunc);
			Desc.StencilEnable = _HelixDesc.m_bStencilEnable ? TRUE : FALSE;
			Desc.StencilReadMask = _HelixDesc.m_uStencilReadMask;
			Desc.StencilWriteMask = _HelixDesc.m_uStencilWriteMask;

			Desc.FrontFace.StencilFailOp = static_cast<D3D11_STENCIL_OP>(_HelixDesc.m_FrontFace.m_kStencilFailOp);
			Desc.FrontFace.StencilDepthFailOp = static_cast<D3D11_STENCIL_OP>(_HelixDesc.m_FrontFace.m_kStencilDepthFailOp);
			Desc.FrontFace.StencilPassOp = static_cast<D3D11_STENCIL_OP>(_HelixDesc.m_FrontFace.m_kStencilPassOp);
			Desc.FrontFace.StencilFunc = static_cast<D3D11_COMPARISON_FUNC>(_HelixDesc.m_FrontFace.m_kStencilFunc);

			Desc.BackFace.StencilFailOp = static_cast<D3D11_STENCIL_OP>(_HelixDesc.m_BackFace.m_kStencilFailOp);
			Desc.BackFace.StencilDepthFailOp = static_cast<D3D11_STENCIL_OP>(_HelixDesc.m_BackFace.m_kStencilDepthFailOp);
			Desc.BackFace.StencilPassOp = static_cast<D3D11_STENCIL_OP>(_HelixDesc.m_BackFace.m_kStencilPassOp);
			Desc.BackFace.StencilFunc = static_cast<D3D11_COMPARISON_FUNC>(_HelixDesc.m_BackFace.m_kStencilFunc);

			return Desc;
		}

		//---------------------------------------------------------------------------------------------------
		static inline BlendDesc ConvertToHelixDescription(const D3D11_BLEND_DESC& _D3D11Desc)
		{
			BlendDesc Desc = {};

			Desc.m_bAlphaToCoverageEnable = _D3D11Desc.AlphaToCoverageEnable == TRUE;
			Desc.m_bIndependentBlendEnable = _D3D11Desc.IndependentBlendEnable == TRUE;

			for (uint32_t i = 0; i < std::min(_countof(_D3D11Desc.RenderTarget), Desc.m_RenderTargets.size()); ++i)
			{
				Desc.m_RenderTargets[i].m_bBlendEnable = _D3D11Desc.RenderTarget[i].BlendEnable == TRUE;
				Desc.m_RenderTargets[i].m_kBlendOp = static_cast<BlendOperation>(_D3D11Desc.RenderTarget[i].BlendOp);
				Desc.m_RenderTargets[i].m_kBlendOpAlpha = static_cast<BlendOperation>(_D3D11Desc.RenderTarget[i].BlendOpAlpha);
				Desc.m_RenderTargets[i].m_kDestBlend = static_cast<BlendVariable>(_D3D11Desc.RenderTarget[i].DestBlend);
				Desc.m_RenderTargets[i].m_kDestBlendAlpha = static_cast<BlendVariable>(_D3D11Desc.RenderTarget[i].DestBlendAlpha);
				Desc.m_RenderTargets[i].m_kSrcBlend = static_cast<BlendVariable>(_D3D11Desc.RenderTarget[i].SrcBlend);
				Desc.m_RenderTargets[i].m_kSrcBlendAlpha = static_cast<BlendVariable>(_D3D11Desc.RenderTarget[i].SrcBlendAlpha);
				Desc.m_RenderTargets[i].m_uRenderTargetWriteMask = _D3D11Desc.RenderTarget[i].RenderTargetWriteMask;
			}

			return Desc;
		}

		//---------------------------------------------------------------------------------------------------

		static inline D3D11_BLEND_DESC ConvertToD3D11Description(const BlendDesc& _HelixDesc)
		{
			D3D11_BLEND_DESC Desc = {};

			Desc.AlphaToCoverageEnable = _HelixDesc.m_bAlphaToCoverageEnable ? TRUE : FALSE;
			Desc.IndependentBlendEnable = _HelixDesc.m_bIndependentBlendEnable ? TRUE : FALSE;

			for (uint32_t i = 0; i < std::min(_countof(Desc.RenderTarget), _HelixDesc.m_RenderTargets.size()); ++i)
			{
				Desc.RenderTarget[i].BlendEnable = _HelixDesc.m_RenderTargets[i].m_bBlendEnable ? TRUE : FALSE;
				Desc.RenderTarget[i].BlendOp = static_cast<D3D11_BLEND_OP>(_HelixDesc.m_RenderTargets[i].m_kBlendOp);
				Desc.RenderTarget[i].BlendOpAlpha = static_cast<D3D11_BLEND_OP>(_HelixDesc.m_RenderTargets[i].m_kBlendOpAlpha);
				Desc.RenderTarget[i].DestBlend = static_cast<D3D11_BLEND>(_HelixDesc.m_RenderTargets[i].m_kDestBlend);
				Desc.RenderTarget[i].DestBlendAlpha = static_cast<D3D11_BLEND>(_HelixDesc.m_RenderTargets[i].m_kDestBlendAlpha);
				Desc.RenderTarget[i].SrcBlend = static_cast<D3D11_BLEND>(_HelixDesc.m_RenderTargets[i].m_kSrcBlend);
				Desc.RenderTarget[i].SrcBlendAlpha = static_cast<D3D11_BLEND>(_HelixDesc.m_RenderTargets[i].m_kSrcBlendAlpha);
				Desc.RenderTarget[i].RenderTargetWriteMask = _HelixDesc.m_RenderTargets[i].m_uRenderTargetWriteMask;
			}

			return Desc;
		}

		//---------------------------------------------------------------------------------------------------

		static inline D3D11_BUFFER_DESC ConvertToD3D11Description(const BufferDesc& _HelixDesc)
		{
			D3D11_BUFFER_DESC DX11BufferDesc = { };

			DX11BufferDesc.ByteWidth = _HelixDesc.uSize;
			DX11BufferDesc.StructureByteStride = _HelixDesc.uStructureByteStride;

			DX11BufferDesc.Usage = static_cast<D3D11_USAGE> (_HelixDesc.kUsageFlag);
			DX11BufferDesc.BindFlags = static_cast<D3D11_BIND_FLAG> (_HelixDesc.kBindFlag.GetFlag());
			DX11BufferDesc.CPUAccessFlags = static_cast<D3D11_CPU_ACCESS_FLAG> (_HelixDesc.kCPUAccessFlag.GetFlag());
			DX11BufferDesc.MiscFlags = static_cast<D3D11_RESOURCE_MISC_FLAG>(_HelixDesc.kResourceFlag.GetFlag());

			return DX11BufferDesc;
		}

		//---------------------------------------------------------------------------------------------------
		static inline InputElementDesc ConvertToHelixDescription(const D3D11_SIGNATURE_PARAMETER_DESC& _D3D11Desc)
		{
			InputElementDesc Desc = {};

			Desc.sSemanticName = std::string(_D3D11Desc.SemanticName);
			Desc.uSemanticIndex = _D3D11Desc.SemanticIndex;
			Desc.uRegister = _D3D11Desc.Register;
			Desc.kSystemValueType = static_cast<SystemValue>(_D3D11Desc.SystemValueType);
			Desc.kShaderComponentType = static_cast<ShaderComponentType>(_D3D11Desc.ComponentType);
			Desc.uMask = _D3D11Desc.Mask;
			Desc.uReadWriteMask = _D3D11Desc.ReadWriteMask;
			Desc.uStream = _D3D11Desc.Stream;
			Desc.kMinPrecision = static_cast<ShaderMinPrecision>(_D3D11Desc.MinPrecision);

			return Desc;
		}

		//---------------------------------------------------------------------------------------------------

		static inline DXGI_FORMAT GetInputElementFormat(ShaderComponentType _kComponentType, uint8_t _uMask)
		{
			switch (_kComponentType)
			{			
			case Helix::Display::ShaderComponentType_UInt32:
				if (_uMask == 1)
				{
					return DXGI_FORMAT_R32_UINT;
				}
				else if (_uMask <=3)
				{
					return DXGI_FORMAT_R32G32_UINT;
				}
				else if (_uMask <= 7)
				{
					return DXGI_FORMAT_R32G32B32_UINT;
				}
				else if (_uMask <= 15)
				{
					return DXGI_FORMAT_R32G32B32A32_UINT;
				}
			case Helix::Display::ShaderComponentType_SInt32:
				if (_uMask == 1)
				{
					return DXGI_FORMAT_R32_SINT;
				}
				else if (_uMask <= 3)
				{
					return DXGI_FORMAT_R32G32_SINT;
				}
				else if (_uMask <= 7)
				{
					return DXGI_FORMAT_R32G32B32_SINT;
				}
				else if (_uMask <= 15)
				{
					return DXGI_FORMAT_R32G32B32A32_SINT;
				}
			case Helix::Display::ShaderComponentType_Float32:
				if (_uMask == 1)
				{
					return DXGI_FORMAT_R32_FLOAT;
				}
				else if (_uMask <= 3)
				{
					return DXGI_FORMAT_R32G32_FLOAT;
				}
				else if (_uMask <= 7)
				{
					return DXGI_FORMAT_R32G32B32_FLOAT;
				}
				else if (_uMask <= 15)
				{
					return DXGI_FORMAT_R32G32B32A32_FLOAT;
				}
			case Helix::Display::ShaderComponentType_Unknown:
			default:
				HERROR("Unknown ComponentType!");
				break;
			}

			return DXGI_FORMAT_UNKNOWN;
		}

		//---------------------------------------------------------------------------------------------------

		//D3D11_INPUT_ELEMENT_DESC::SemanticName is valid as long as InputElementDesc exists
		static inline D3D11_INPUT_ELEMENT_DESC ConvertToD3D11Description(const InputElementDesc& _HelixDesc)
		{
			D3D11_INPUT_ELEMENT_DESC Desc = {};
			Desc.SemanticName = _HelixDesc.sSemanticName.c_str();
			Desc.SemanticIndex = _HelixDesc.uSemanticIndex;
			Desc.Format = GetInputElementFormat(_HelixDesc.kShaderComponentType, _HelixDesc.uMask);
			Desc.InputSlot = _HelixDesc.uStream;
			Desc.AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;

			if (_HelixDesc.kSystemValueType == SystemValue_InstanceID || hlx::starts_with(_HelixDesc.sSemanticName, "INSTANCE", false))
			{
				Desc.InputSlotClass = D3D11_INPUT_PER_INSTANCE_DATA;
				Desc.InstanceDataStepRate = 1;
			}
			else
			{
				Desc.InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
				Desc.InstanceDataStepRate = 0;
			}

			return Desc;
		}

		//---------------------------------------------------------------------------------------------------

		static inline std::vector<D3D11_INPUT_ELEMENT_DESC> ConvertToD3D11Description(const std::vector<InputElementDesc>& _HelixDescs)
		{
			std::vector<D3D11_INPUT_ELEMENT_DESC> InputElements;

			for (const InputElementDesc& Desc : _HelixDescs)
			{
				switch (Desc.kSystemValueType)
				{				
				case SystemValue_VertexID:
				case SystemValue_PrimitveID:
				case SystemValue_InstanceID:
					continue;
				}

				InputElements.push_back(ConvertToD3D11Description(Desc));
			}

			return InputElements;
		}

		//---------------------------------------------------------------------------------------------------
		static inline TVertexDeclaration GenerateVertexDeclarationFromInputLayout(const std::vector<InputElementDesc>& _HelixDescs)
		{
			TVertexDeclaration VertexDecl = VertexLayout_Unknown;
			for (const InputElementDesc& Desc : _HelixDescs)
			{
				// create vertex layout from element desc
				VertexLayout kElement = VertexLayout_Unknown;
				if (hlx::starts_with(Desc.sSemanticName, "POSITION"))
				{
					if (Desc.uMask == 1)
					{
						HERROR("Unknown vertex element format (Mask 1)");
						return VertexLayout_Unknown;
					}
					else if (Desc.uMask <= 3)
					{
						kElement = VertexLayout_Pos_XY;
					}
					else if (Desc.uMask <= 7)
					{
						kElement = VertexLayout_Pos_XYZ;
					}
					else if (Desc.uMask <= 15)
					{
						kElement = VertexLayout_Pos_XYZW;
					}
				}
				else if (hlx::starts_with(Desc.sSemanticName, "NORMAL"))
				{
					kElement = VertexLayout_Normal;
				}
				else if (hlx::starts_with(Desc.sSemanticName, "TANGENT"))
				{
					kElement = VertexLayout_Tangent;
				}
				else if (hlx::starts_with(Desc.sSemanticName, "BITANGENT"))
				{
					kElement = VertexLayout_Bitangent;
				}
				else if (hlx::starts_with(Desc.sSemanticName, "TEXCOORD"))
				{
					kElement = VertexLayout_UV;
				}

				// try to make sure vertex elements occure in ascending order
				if (kElement <= VertexDecl.GetFlag())
				{
					//HERROR("Unknown vertex element order for %s", Desc.sSemanticName.c_str());
					return VertexLayout_Unknown;
				}
				else
				{
					VertexDecl |= kElement;
				}
			}

			return VertexDecl;
		}
		static inline uint64_t ComputeInputLayoutHash(const std::vector<InputElementDesc>& _HelixDescs)
		{
			uint64_t uHash = 0;
			std::hash<std::string> HashFunc;

			for (const InputElementDesc& Desc : _HelixDescs)
			{
				// Compute hash
				uint32_t uSize = offsetof(InputElementDesc, sSemanticName);
				uHash ^= DefaultHashFunction((uint8_t*)&Desc, uSize); // everything until semantic name
				uHash ^= HashFunc(Desc.sSemanticName); // semantic name,
				// ignore variable name
			}

			return uHash;
		}

		//---------------------------------------------------------------------------------------------------

		static inline ShaderBufferType ConvertToHelixType(const D3D_SHADER_INPUT_TYPE& _kResourceType)
		{
			switch (_kResourceType)
			{
			case D3D_SIT_CBUFFER: return ShaderBufferType_ConstantBuffer;
			case D3D_SIT_TBUFFER: return ShaderBufferType_TextureBuffer;
			case D3D_SIT_UAV_RWTYPED: return ShaderBufferType_UAV_RWTyped;
			case D3D_SIT_STRUCTURED: return ShaderBufferType_Structured;
			case D3D_SIT_UAV_RWSTRUCTURED: return ShaderBufferType_UAV_RWStructured;
			case D3D_SIT_BYTEADDRESS: return ShaderBufferType_ByteAddress;
			case D3D_SIT_UAV_RWBYTEADDRESS: return ShaderBufferType_UAV_RWByteAddress;
			case D3D_SIT_UAV_APPEND_STRUCTURED: return ShaderBufferType_UAV_Append_Structured;
			case D3D_SIT_UAV_CONSUME_STRUCTURED: return ShaderBufferType_UAV_Consume_Structured;
			case D3D_SIT_UAV_RWSTRUCTURED_WITH_COUNTER: return ShaderBufferType_UAV_RWStructured_With_Counter;
			default: return ShaderBufferType_Unknown;
			}
		}

	} // Display
} // Helix


#endif //VIEWDEFINESDX11_H