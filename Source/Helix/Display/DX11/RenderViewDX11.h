//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef RENDERVIEWDX11_H
#define RENDERVIEWDX11_H
 
#include "Display\RenderView.h"
#include "hlx\src\StandardDefines.h"

struct ID3D11Device;
struct ID3D11DeviceContext;

namespace Helix
{
	namespace Display
	{
		// forward decls
		class RenderDeviceDX11;
		class DXGIManager;

		class RenderViewDX11 : public RenderView
		{
		public:
			HDEBUGNAME("RenderViewDX11");

			RenderViewDX11();

			~RenderViewDX11();
			///Disable copy constructor
			RenderViewDX11(RenderViewDX11& _RenderView) = delete;

			bool Initialize(HWND _hWnd) final;
			void Shutdown() final;

			virtual bool OnInitialize(HWND _hWnd) { return true; };
			virtual void OnShutdown() {};

			void ResizeRenderer(const uint32_t _uXResolution, const uint32_t _uYResolution) final;

			void GetRendererResolution(_Out_ uint32_t& _uXResolution, _Out_ uint32_t& _uYResolution) const final;

			void SetFullscreen(bool _bFullscreen = true) final;

			void ChangeRendererSettings(uint32_t _uDisplayModeId, uint32_t _uAdapterId, uint32_t _uOutputId) final;

			//virtual HICON GetIcon(HINSTANCE _hInstance) = 0;

		private:

			void CreateBackBuffer();

			void ResizeBuffers(const uint32_t _uXResolution, const uint32_t _uYResolution);
			void ResizeTarget(bool _bFlickerFix = false);

		private:
			ID3D11Device*			m_pDevice = nullptr;
			ID3D11DeviceContext*	m_pDeviceContext = nullptr;
			RenderDeviceDX11* m_pRenderDevice = nullptr;

			DXGIManager*			m_pDXGIManager = nullptr;
		};
	} // Display
} // Helix

#endif //RENDERVIEWDX11_H