//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "MeshClusterDX11.h"
#include "MeshPoolDX11.h"
#include "Physics\GeometryShapeFactory.h"

using namespace Helix::Physics;
using namespace Helix::Display;
//---------------------------------------------------------------------------------------------------
// MeshClusterReferenceDX11
//---------------------------------------------------------------------------------------------------
MeshClusterReferenceDX11::~MeshClusterReferenceDX11()
{
	HSAFE_DELETE(m_pIndexBuffer);
	HSAFE_DELETE(m_pVertexBuffer);

	if (ConvexMeshChecksums.empty() == false)
	{
		Physics::GeometryShapeFactory* pFactory = Physics::GeometryShapeFactory::Instance();

		for (const uint32_t& uCRC32 : ConvexMeshChecksums)
		{
			pFactory->ReleaseConvexMesh(uCRC32);
		}

		ConvexMeshChecksums.clear();
	}
}

//---------------------------------------------------------------------------------------------------
// MeshClusterDX11
//---------------------------------------------------------------------------------------------------

MeshClusterDX11::MeshClusterDX11(const DefaultInitializerType& t) :
	TDX11MeshClusterRef(t)
{
	if (m_pReference != nullptr)
	{
		m_SubMeshes = m_pReference->m_SubMeshes;
		//CheckContinuity();
	}
}
//---------------------------------------------------------------------------------------------------
//Loads complete cluster from file
MeshClusterDX11::MeshClusterDX11(const std::string& m_sFilePathOrName, bool _bLoadFromFile, bool _bWriteable, bool _bPooled)
{
	MeshClusterDesc Desc;
	Desc.m_sFilePathOrName = m_sFilePathOrName;
	Desc.m_bLoadFromFile = _bLoadFromFile;
	Desc.m_bPooled = _bPooled;

	if (_bWriteable)
	{
		Desc.m_kCPUAccessFlag = CPUAccessFlag_Write;
		Desc.m_kUsageFlag = ResourceUsageFlag_Dynamic;
	}
	else
	{
		Desc.m_kUsageFlag = ResourceUsageFlag_Default;
		Desc.m_kCPUAccessFlag = CPUAccessFlag_None;
	}

	m_pReference = MeshPoolDX11::Instance()->Get(Desc, m_Hash);
	if (m_pReference != nullptr)
	{
		m_SubMeshes = m_pReference->m_SubMeshes;
		//CheckContinuity();
	}
}
//---------------------------------------------------------------------------------------------------
//Load one sub mesh from file, complete cluster is in memory
MeshClusterDX11::MeshClusterDX11(const std::string& _sFilePath, const std::string& _sSubMeshName, bool _bWriteable, bool _bPooled) :
	MeshClusterDX11(_sFilePath, std::vector<std::string>({_sSubMeshName}), _bWriteable, _bPooled)
{
}
//---------------------------------------------------------------------------------------------------
MeshClusterDX11::MeshClusterDX11(const std::string& _sFilePath, const std::vector<std::string>& _SubMeshNames, bool _bWriteable, bool _bPooled) :
	MeshClusterDX11(_sFilePath, true, _bWriteable, _bPooled)
{
	if (m_SubMeshes.size() == 1u)
	{
		if (m_SubMeshes.front().m_sSubName == "HELiXERRORCUBE")
		{
			//CheckContinuity();
			return;
		}
	}

	std::vector<std::string> SubMeshNames(_SubMeshNames);
	TMeshCluster RequiredMeshes;
	RequiredMeshes.reserve(_SubMeshNames.size());

	// remove unneeded submeshes
	for (TMeshCluster::iterator mit = m_SubMeshes.begin(); mit != m_SubMeshes.end();)
	{
		bool bFound = false;
		for (std::vector<std::string>::iterator nit = SubMeshNames.begin(); nit != SubMeshNames.end();)
		{
			if (mit->m_sSubName == *nit)
			{
				bFound = true;
				nit = SubMeshNames.erase(nit);
				RequiredMeshes.push_back(*mit);
				mit = m_SubMeshes.erase(mit);
			}
			else
			{
				++nit;
			}
		}

		if (SubMeshNames.empty())
		{
			break;
		}

		if (bFound == false)
		{
			++mit;
		}
	}

	m_SubMeshes.swap(RequiredMeshes);

	//CheckContinuity();
}
//---------------------------------------------------------------------------------------------------
//create empty mesh cluster
MeshClusterDX11::MeshClusterDX11(const std::string& _sObjectName, const TMeshCluster& _MeshCluster, bool _bWriteable, bool _bPooled)
{
	MeshClusterDesc Desc;
	Desc.m_sFilePathOrName = _sObjectName;
	Desc.m_bLoadFromFile = false;
	Desc.m_bPooled = _bPooled;

	if (_bWriteable)
	{
		Desc.m_kCPUAccessFlag = CPUAccessFlag_Write;
		Desc.m_kUsageFlag = ResourceUsageFlag_Dynamic;
	}
	else
	{
		Desc.m_kUsageFlag = ResourceUsageFlag_Default;
		Desc.m_kCPUAccessFlag = CPUAccessFlag_None;
	}

	m_pReference = MeshPoolDX11::Instance()->Get(Desc, m_Hash);
	if (m_pReference != nullptr)
	{
		m_SubMeshes = m_pReference->m_SubMeshes;
		//CheckContinuity();
	}
}
//---------------------------------------------------------------------------------------------------
//create pre filled mesh cluster
MeshClusterDX11::MeshClusterDX11(const MeshClusterDesc& _Desc) :
	TDX11MeshClusterRef(_Desc)
{
	if (m_pReference != nullptr)
	{
		m_SubMeshes = m_pReference->m_SubMeshes;
		//CheckContinuity();
	}
}
//---------------------------------------------------------------------------------------------------
// Copy constructor
MeshClusterDX11::MeshClusterDX11(const MeshClusterDX11& _Other) :
	TDX11MeshClusterRef(_Other),
	//m_bIsContinuous(_Other.m_bIsContinuous),
	//m_ContinuousMeshInfo(_Other.m_ContinuousMeshInfo),
	m_SubMeshes(_Other.m_SubMeshes)
{
}
//---------------------------------------------------------------------------------------------------
// cleanup
MeshClusterDX11::~MeshClusterDX11()
{
}
//---------------------------------------------------------------------------------------------------
MeshClusterDX11& MeshClusterDX11::operator=(const MeshClusterDX11& _Other)
{
	if (this != &_Other)
	{
		TDX11MeshClusterRef::operator=(_Other);

		//m_bIsContinuous = _Other.m_bIsContinuous;
		//m_ContinuousMeshInfo = _Other.m_ContinuousMeshInfo;
		m_SubMeshes = _Other.m_SubMeshes;
	}

	return *this;
}
//---------------------------------------------------------------------------------------------------
MeshClusterDX11& MeshClusterDX11::operator=(std::nullptr_t)
{
	TDX11MeshClusterRef::operator=(nullptr);

	//m_bIsContinuous = false;
	//m_ContinuousMeshInfo = {};
	m_SubMeshes.resize(0);

	return *this;
}
//---------------------------------------------------------------------------------------------------
//void MeshClusterDX11::CheckContinuity()
//{
//	if (m_SubMeshes.empty())
//	{
//		m_bIsContinuous = false;
//		return;
//	}
//
//	TMeshCluster::const_iterator it = m_SubMeshes.cbegin();
//
//	m_bIsContinuous = true;
//	m_ContinuousMeshInfo = *it;
//
//	for (++it; it != m_SubMeshes.end(); ++it)
//	{
//		if (m_ContinuousMeshInfo.m_kPrimitiveTopology != it->m_kPrimitiveTopology)
//		{
//			m_bIsContinuous = false;
//		}
//
//		// validate vertex layout and strict monotony of vertex data
//		if (it->m_kVertexDeclaration != m_ContinuousMeshInfo.m_kVertexDeclaration ||
//			it->m_uVertexByteOffset != m_ContinuousMeshInfo.m_uVertexByteOffset + m_ContinuousMeshInfo.m_uVertexCount * m_ContinuousMeshInfo.m_uVertexStrideSize)
//		{
//			m_bIsContinuous = false;
//		}
//
//		m_ContinuousMeshInfo.m_uVertexCount += it->m_uVertexCount;
//
//		if (it->m_kIndexBufferFormat != m_ContinuousMeshInfo.m_kIndexBufferFormat ||
//			it->m_uIndexByteOffset != m_ContinuousMeshInfo.m_uIndexByteOffset + m_ContinuousMeshInfo.m_uIndexCount * m_ContinuousMeshInfo.m_uIndexElementSize)
//		{
//			m_bIsContinuous = false;
//		}
//
//		m_ContinuousMeshInfo.m_uIndexCount += it->m_uIndexCount;
//	}
//}
//---------------------------------------------------------------------------------------------------
void MeshClusterDX11::AddSubMesh(const SubMesh & _Mesh)
{
	m_SubMeshes.push_back(_Mesh);
	//CheckContinuity();
}
//---------------------------------------------------------------------------------------------------
void MeshClusterDX11::AddSubMesh(const std::string& _SubMeshName)
{
	if (m_pReference == nullptr)
		return;

	for (const SubMesh& Mesh : m_pReference->m_SubMeshes)
	{
		if (Mesh.m_sSubName == _SubMeshName)
		{
			m_SubMeshes.push_back(Mesh);
			return;
		}
	}
}
//---------------------------------------------------------------------------------------------------
void MeshClusterDX11::InsertSubMesh(const SubMesh& _Mesh, uint32_t _iOffset)
{
	if (_iOffset < m_SubMeshes.size())
	{
		m_SubMeshes.insert(m_SubMeshes.begin() + _iOffset, _Mesh);
		//CheckContinuity();
	}
}
//---------------------------------------------------------------------------------------------------

void MeshClusterDX11::RemoveSubMesh(const std::string& _SubName)
{
	for (TMeshCluster::iterator it = m_SubMeshes.begin(), end = m_SubMeshes.end(); it != end; ++it)
	{
		if (it->m_sSubName == _SubName)
		{
			m_SubMeshes.erase(it);
			//CheckContinuity();
			return;
		}
	}
}
//---------------------------------------------------------------------------------------------------

void MeshClusterDX11::ClearSubMeshes()
{
	m_SubMeshes.resize(0);
	//CheckContinuity();
}
//---------------------------------------------------------------------------------------------------
