//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "ShaderPermutationPoolDX11.h"

#include "RenderDeviceDX11.h"

#include "Resources\HLXSLFile.h"
#include "Resources\FileSystem.h"
#include "Resources\InputLayoutSerializer.h"
#include "Resources\ShaderInterfaceSerialzer.h"
#include "Resources\LZMA.h"

#include "Scene\RenderingSceneGuard.h"

using namespace Helix::Scene;
using namespace Helix::Display;
using namespace Helix::Resources;

ShaderPermutationPoolDX11::ShaderPermutationPoolDX11()
{
}
//---------------------------------------------------------------------------------------------------

ShaderPermutationPoolDX11::~ShaderPermutationPoolDX11()
{
	Clear();
}
//---------------------------------------------------------------------------------------------------

bool ShaderPermutationPoolDX11::UncompressShaderCode(const hlx::bytes& _InputData, hlx::bytes& _OutputData, size_t _uUncompressedSize, CodeCompression _kMethod)
{
	switch (_kMethod)
	{
	case CodeCompression_None:
		_OutputData = _InputData;
		return true;
	case CodeCompression_LZMA:
		return LZMA::Uncompress(_InputData, _OutputData, _uUncompressedSize);
	case CodeCompression_Unknown:
		// D3DCompressShaders not supported because we dont want to link D3DCompiler in the engine.
	default:
		HERROR("Unknown compression method %u", _kMethod);
		return false;
	}
}
//---------------------------------------------------------------------------------------------------

void ShaderPermutationPoolDX11::Clear()
{
	m_Shaders.clear();

	HFOREACH(it, end, m_InputLayouts)
		HSAFE_RELEASE(it->second.pInputLayout)
	HFOREACH_END

	m_InputLayouts.clear();
}
//---------------------------------------------------------------------------------------------------
void ShaderPermutationPoolDX11::OnShutdown()
{
	Clear();
}
//---------------------------------------------------------------------------------------------------
// load the shader from HLXSL file
bool ShaderPermutationPoolDX11::LoadHLXSL(const hlx::string& _sShaderName, HLXSLFile& _ShaderFile)
{
	hlx::fbytestream HLXSLStream;
	if (Resources::FileSystem::Instance()->OpenFileStream(HelixDirectories_Shaders, _sShaderName + S(".hlxsl"), std::ios::in | std::ios::binary, HLXSLStream) == false)
	{
		HERROR("Failed to open shader %s", _sShaderName.c_str());
		return false;
	}

	hlx::bytes&& InputBuffer = HLXSLStream.get<hlx::bytes>(HLXSLStream.size());
	HLXSLStream.close();

	hlx::bytestream InputStream(InputBuffer);

	if (_ShaderFile.Read(InputStream) == false)
	{
		HERROR("Failed to load shader %s", _sShaderName.c_str());
		return false;
	}

	return true;
}
//---------------------------------------------------------------------------------------------------

std::shared_ptr<ShaderDX11> ShaderPermutationPoolDX11::Load(const hlx::string& _sShaderName, bool _bReload)
{
	RenderingSceneGuard Lock;
	//RenderDeviceDX11::Instance()->Lock();

	ID3D11Device* pDevice = RenderDeviceDX11::Instance()->GetDevice();
	HRESULT hr = S_OK;

	std::shared_ptr<ShaderDX11> pShader = nullptr;
	HLXSLFile Shader;

	TShaderMap::iterator it = m_Shaders.find(_sShaderName);
	if (it != m_Shaders.end())
	{
		pShader = it->second;
	}

	if (pShader == nullptr)
	{
		if (LoadHLXSL(_sShaderName, Shader) == false)
			return nullptr;

		pShader = std::make_shared<ShaderDX11>(_sShaderName);
		m_Shaders[_sShaderName] = pShader;
	}
	else if (_bReload)
	{
		// clear interfaces
		RenderDeviceDX11::Instance()->ClearShaders();
		pShader->Clear();

		if (LoadHLXSL(_sShaderName, Shader) == false)
			return nullptr;
	}
	else // no need to reload interface etc.
	{
		return pShader;
	}

	// set new code compression method (might have changed since last load)
	pShader->SetCodeCompression(Shader.GetCodeCompression());
	pShader->SetPermutaitonsMasks(Shader.GetPermutationsMasks());
	pShader->SetFunctions(Shader.GetFunctions());

	for (uint32_t i = 0; i < Shader.GetPermutationCount(); ++i)
	{	
		const PermutationBufferRef/*&&*/ PermRef = Shader.GetPermuation(i);
		const HLXSLPermutationIndex& PermIndex = Shader.GetPermutationIndex(i);

		ShaderType kShaderType = ShaderType_None;

		if (PermRef.pFunction == nullptr || PermRef.pCode == nullptr || PermRef.pIndex == nullptr)
		{
			HERROR("Invalid Permutation buffer reference %u", i);
			return nullptr;
		}

		kShaderType = ShaderModelVersions[PermRef.pFunction->kTargetVersion].kType;

		VertexLayoutDX11 VertexLayout = {};

		// create inputlayout if needed
		if (kShaderType == ShaderType_VertexShader && PermRef.pInputLayout != nullptr)
		{
			// create new layout
			InputLayoutSerializer Serializer;
			hlx::const_bytestream InputStream(*PermRef.pInputLayout);
			if (Serializer.Read(InputStream) == false)
			{
				return nullptr;
			}

			const std::vector<InputElementDesc>& HelixInputElements = Serializer.GetInputElementDescription();
			uint64_t uHash = ComputeInputLayoutHash(HelixInputElements);
			std::vector<D3D11_INPUT_ELEMENT_DESC>&& DX11InputElements = ConvertToD3D11Description(HelixInputElements);

			{
				TInputlayoutMap::iterator it = m_InputLayouts.find(uHash);
				if (it != m_InputLayouts.end())
				{
					VertexLayout = it->second;
				}
				else if (DX11InputElements.empty() == false)
				{
					hlx::bytes Code;					
					if (UncompressShaderCode(*PermRef.pCode, Code, PermIndex.uUncompressedCodeSize, Shader.GetCodeCompression()) == false ||
						FAILED(hr = pDevice->CreateInputLayout(&DX11InputElements[0], static_cast<UINT>(DX11InputElements.size()), Code.data(), Code.size(), &VertexLayout.pInputLayout)))
					{
						HRERROR(hr);

						if (FAILED(hr) == false)
						{
							HFATAL("Failed to uncompress shader code");
						}

						return nullptr;
					}

					HDXDEBUGNAME(VertexLayout.pInputLayout, "InputLayout_%ul", uHash);

					VertexLayout.kDeclaration = GenerateVertexDeclarationFromInputLayout(HelixInputElements);

					m_InputLayouts.insert({ uHash, VertexLayout });
				}			
			}
		}

		//---------------------------------------------------------------------------------------------------
		// Create shader variable interface
		//---------------------------------------------------------------------------------------------------

		{
			TInterfaceMap::iterator iit = m_Interfaces.find(PermIndex.uInterfaceHash);

			if (iit == m_Interfaces.end())
			{
				ShaderInterfaceDesc Interface = {};

				if (PermRef.pInterface != nullptr)
				{
					ShaderInterfaceSerializer Deserializer;
					hlx::const_bytestream InterfaceStream(*(PermRef.pInterface));

					if (Deserializer.Read(InterfaceStream) == false)
					{
						HERROR("Failed to read ShaderInterface for %s", _sShaderName.c_str());
						return nullptr;
					}

					Interface = Deserializer.GetInterface();
				}
				// add empty interface in case theres an matching outputlayout later
				iit = m_Interfaces.insert({ PermIndex.uInterfaceHash, Interface }).first;
			}

			// creation / aquiring didnt fail
			if (iit != m_Interfaces.end())
			{
				//---------------------------------------------------------------------------------------------------
				// Add OutputLayout -> Pixelshader rendertarget setup
				//---------------------------------------------------------------------------------------------------
				if (PermRef.pOutputLayout != nullptr && iit->second.RenderTargets.empty())
				{
					InputLayoutSerializer Serializer;
					hlx::const_bytestream OutputLayoutStream(*PermRef.pOutputLayout);
					if (Serializer.Read(OutputLayoutStream) == false)
					{
						return nullptr;
					}

					iit->second.RenderTargets = GetRenderTargetsFromOutputSignature(Serializer.GetInputElementDescription());
				}

				if (PermRef.pCode != nullptr)
				{
					if (pShader->AddPermutation(
						*PermRef.pCode,
						PermRef.pIndex->uUncompressedCodeSize,
						PermRef.pIndex->uCodeHash,
						kShaderType,
						PermRef.pIndex->Hash,
						&iit->second,
						VertexLayout) == false)
					{
						return nullptr;
					}
				}				
			}
		}
	}

	HLOG("Sucessfully loaded %s with %u permutations =)", _sShaderName.c_str(),  Shader.GetPermutationCount());
	
	return pShader;
}
//---------------------------------------------------------------------------------------------------
std::vector<BindInfoDesc> ShaderPermutationPoolDX11::GetRenderTargetsFromOutputSignature(const std::vector<InputElementDesc>& _OutputSignature)
{
	std::vector<BindInfoDesc> RenderTargets;

	for (const InputElementDesc& Desc : _OutputSignature)
	{
		if (Desc.kSystemValueType != SystemValue_Target)
		{
			continue;
		}

		BindInfoDesc BindInfo = {};
		BindInfo.sName = Desc.sVariableName;
		BindInfo.uSlot = Desc.uRegister;

		RenderTargets.push_back(BindInfo);
	}
		
	return RenderTargets;
}
//---------------------------------------------------------------------------------------------------
