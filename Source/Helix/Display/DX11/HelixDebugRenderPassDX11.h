//   Copyright 2020 Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef HELIXDEBUGRENDERPASSDX11_H
#define HELIXDEBUGRENDERPASSDX11_H

#include "RenderPassDX11.h"
#include "CommonConstantBufferFunctions.h"
#include "Display\Shaders\UnicolorVertex.hlsl"

namespace Helix
{
	namespace Display
	{
		class HelixDebugRenderPassDX11 : public RenderPassDX11
		{
		public:
			HDEBUGNAME("HelixDebugRenderPassDX11");

			HelixDebugRenderPassDX11(DefaultInitializerType);
			HelixDebugRenderPassDX11(const std::string& _sInstanceName = {});
			virtual ~HelixDebugRenderPassDX11();

			bool OnInitalize(const hlx::TextToken& _CustomVariables) final;

			bool OnPerFrame() final;
			bool OnPerCamera(const Scene::CameraComponent& _Camera) final;
			//bool OnPerObject(const RenderObjectDX11& _RenderObject, const MaterialDX11& _Material) final;

			std::shared_ptr<RenderPassDX11> CreatePass(const std::string& _sInstanceName) const final;

		private:
			ShaderRenderTargetDX11 m_Backbuffer = { "vColor", "BACKBUFFER" };

			//TCBPerObject m_CBPerObject = { "cbPerObject" };
			TCBPerCamera m_CBPerCamera = { "cbPerCamera" };
		};

		//---------------------------------------------------------------------------------------------------
		inline bool HelixDebugRenderPassDX11::OnPerFrame()
		{
			uint32_t uClassPermutation = 0u;
			Select(ShaderType_PixelShader, uClassPermutation);
			Select(ShaderType_VertexShader, uClassPermutation);

			return true;
		}
		//---------------------------------------------------------------------------------------------------
		inline bool HelixDebugRenderPassDX11::OnPerCamera(const Scene::CameraComponent& _Camera)
		{
			SetCameraConstants(m_CBPerCamera, _Camera.GetRenderingProperties());

			return true;
		}
		//---------------------------------------------------------------------------------------------------
		//inline bool HelixDebugRenderPassDX11::OnPerObject(const RenderObjectDX11& _RenderObject, const MaterialDX11& _Material)
		//{
		//	SetObjectConstants(m_CBPerObject, _RenderObject);

		//	return true;
		//}
		//---------------------------------------------------------------------------------------------------
	} // Display
} // Helix

#endif /// HELIXDEBUGRENDERPASSDX11_H