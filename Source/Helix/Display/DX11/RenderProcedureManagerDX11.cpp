//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "RenderProcedureManagerDX11.h"

#include "Scene\RenderingScene.h"
#include "Scene\PhysicsScene.h"
#include "Scene\ScriptScene.h"
#include "Scene\CameraManager.h"

#include "Util\Functional.h"
#include "Util\AdvancedProfiler.h"
#include "ShaderPermutationPoolDX11.h"

using namespace Helix::Display;
using namespace Helix::Scene;
using namespace Helix::Resources;

//---------------------------------------------------------------------------------------------------
RenderProcedureManagerDX11::RenderProcedureManagerDX11()
{
	HNCL(m_SyncRendererDock.Bind());
}
//---------------------------------------------------------------------------------------------------

RenderProcedureManagerDX11::RenderProcedureManagerDX11(const hlx::TextToken& _RendererToken)
{
	Load(RendererDesc(_RendererToken));
	HNCL(m_SyncRendererDock.Bind());
}
//---------------------------------------------------------------------------------------------------

RenderProcedureManagerDX11::RenderProcedureManagerDX11(const RendererDesc& _Desc)
{
	Load(_Desc);
	HNCL(m_SyncRendererDock.Bind());
}
//---------------------------------------------------------------------------------------------------
RenderProcedureManagerDX11::~RenderProcedureManagerDX11()
{
	Reset();
}
//---------------------------------------------------------------------------------------------------

bool RenderProcedureManagerDX11::Load(const RendererDesc& _Desc, bool _bCompleteReload)
{
	// todo: make threadsafe (block while rendering)
	if (_bCompleteReload)
	{
		Reset();
	}

	for (const RenderProcedureDesc& ProcDesc : _Desc.Procedures)
	{
		if (AddProcedure(ProcDesc) == false)
		{
			return false;
		}
	}

	return true;
}
//---------------------------------------------------------------------------------------------------

void RenderProcedureManagerDX11::Reset()
{
	// todo: make threadsafe (block while rendering)
	m_Passes.clear();
	m_Procedures.clear();

#ifdef HNUCLEO
	m_ProcPasses.clear();
	m_pSelectedPass = nullptr;
	m_pSelectedProc = nullptr;
#endif
}

//---------------------------------------------------------------------------------------------------
bool RenderProcedureManagerDX11::AddProcedure(std::shared_ptr<RenderProcedureDX11> _pRenderProcedure)
{
	if (_pRenderProcedure->DefaultInitialize())
	{
		m_Procedures.push_back(_pRenderProcedure);
		return true;
	}

	return false;
}
//---------------------------------------------------------------------------------------------------
bool RenderProcedureManagerDX11::AddProcedure(const RenderProcedureDesc& _Desc)
{
	std::shared_ptr<RenderProcedureDX11> pProc = std::make_shared<RenderProcedureDX11>();

	if (pProc->Initialize(_Desc))
	{
		m_Procedures.push_back(pProc);
		return true;
	}

	return false;
}
//---------------------------------------------------------------------------------------------------
RendererDesc RenderProcedureManagerDX11::CreateDescription() const
{
	RendererDesc Desc = {};
	
	for (const std::shared_ptr<RenderProcedureDX11>& pRenderProc : m_Procedures)
	{
		Desc.Procedures.push_back(pRenderProc->CreateDescription());
	}

	return Desc;
}
//---------------------------------------------------------------------------------------------------
void RenderProcedureManagerDX11::Apply()
{
	// m_Passes could be a local variable
	m_Passes.resize(0);

	// rebuild active passes
	for (std::shared_ptr<RenderProcedureDX11>& pProc : m_Procedures)
	{
		if (pProc->IsEnabled())
		{
			for (std::shared_ptr<RenderPassDX11>& pPass : pProc->GetPasses())
			{
				if (pPass->IsEnabled())
				{
					m_Passes.push_back(pPass);
				}
			}
		}
	}

	size_t uPassCount = m_Passes.size();

	for (uint32_t i = 0u; i < uPassCount; ++i)
	{
		HGUIPROFILE(Pass)

		std::shared_ptr<RenderPassDX11>& pCurPass = m_Passes.at(i);

		pCurPass->ApplyInstance();

		// map previous outputs to inputs of this shader pass
		for (ShaderTextureDX11* pInputTexture : pCurPass->GetInputTextures())
		{
			const std::string& sMappedName = pInputTexture->GetMappedName();

			// Try to assign each mapped output to an input
			if (sMappedName.empty() == false)
			{
				bool bMapped = false;
				// for all previous passes (even from the last frame)
				for (uint32_t p = 1u; p < uPassCount && bMapped == false; ++p)
				{
					uint32_t uPrev = (i - p) % uPassCount; // underflow % size -> end of the vector -> previous frame
					std::shared_ptr<RenderPassDX11>& pPrevPass = m_Passes.at(uPrev);

					// transfer rendertarget output (or a copy) from previous pass to shader input texture of current pass
					// map rendertarget to shader resource
					if (pPrevPass->GetDepthStencilMappedName() == sMappedName)
					{
						bMapped = pInputTexture->SetTexture(pPrevPass->GetDepthStencilTexture());
					}
					else // map depthstencil to shader resource
					{
						const TextureDX11* pTexOut = pPrevPass->GetOutputTexture(sMappedName);
						bMapped = pTexOut != nullptr ? pInputTexture->SetTexture(*pTexOut) : false;
					}
				}

				if (bMapped == false)
				{
					HERROR("Failed to map RTV %s to SRV %s", CSTR(sMappedName), CSTR(pInputTexture->GetName()));
				}
			}
		}

		// execute the pass to produce inputs for the next pass
		if (pCurPass->OnPreRender())
		{
			pCurPass->Apply();

			pCurPass->OnPostRender();		
		}

		HGUIPROFILEDYNEX_END(Pass, (pCurPass->GetTypeName() + pCurPass->GetInstanceName()), pCurPass->GetTypeName(), "Passes");
	}
}
//---------------------------------------------------------------------------------------------------
void RenderProcedureManagerDX11::PrintPermutationReport() const
{
	for (const std::shared_ptr<RenderProcedureDX11>& pProc : m_Procedures)
	{
		if (pProc->IsEnabled())
		{
			for (std::shared_ptr<RenderPassDX11>& pPass : pProc->GetPasses())
			{
				if (pPass->IsEnabled())
				{
					const std::shared_ptr<ShaderDX11>& pShader = pPass->GetShader();
					HLOG("VS %s PS %s",
						pShader->GetName().c_str(),
						CSTR(pPass->GetPermutationName(pShader->GetSelectedPermutation(ShaderType_VertexShader))),
						CSTR(pPass->GetPermutationName(pShader->GetSelectedPermutation(ShaderType_PixelShader))));
				}
			}
		}
	}
}
//---------------------------------------------------------------------------------------------------

void RenderProcedureManagerDX11::OnShutdown()
{
	Reset();
}
//---------------------------------------------------------------------------------------------------
