//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef RENDERPASSCREATORDX11_H
#define RENDERPASSCREATORDX11_H

#include "hlx\src\Singleton.h"
#include <concurrent_unordered_map.h>

namespace Helix
{
	namespace Display
	{
		// forward decls
		class RenderPassDX11;

		using TRenderPassPrototypeMap = concurrency::concurrent_unordered_map<std::string, std::shared_ptr<RenderPassDX11>>;

		class RenderPassCreatorDX11 : public hlx::Singleton<RenderPassCreatorDX11>
		{
		public:
			RenderPassCreatorDX11();
			~RenderPassCreatorDX11();

			// returns a renderpass implementation if _bCreatePass is true, nullptr otherwise
			std::shared_ptr<RenderPassDX11> RegisterPrototype(const std::shared_ptr<RenderPassDX11>& _pPrototype);

			// returns a renderpass implementation if a prototype with matching typename was found, nullptr otherwise
			// this should be used when creating a pass from the description
			std::shared_ptr<RenderPassDX11> CreatePass(const std::string& _sTypeName, const std::string& _sInstanceName) const;

		private:
			TRenderPassPrototypeMap m_Prototypes;
		};
	} // Display
} // Helix

#endif // !RENDERPASSCREATORDX11_H

