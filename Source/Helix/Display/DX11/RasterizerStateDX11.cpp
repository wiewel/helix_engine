//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "RasterizerStateDX11.h"
#include "hlx\src\Logger.h"
#include "ViewDefinesDX11.h"

using namespace Helix::Display;

//---------------------------------------------------------------------------------------------------
RasterizerStateDX11::RasterizerStateDX11(const RasterizerDesc& _RasterizerDesc, const char* _pDbgName) :
	m_Description(_RasterizerDesc)
{
	m_pReference = RasterizerStatePoolDX11::Instance()->Get(ConvertToD3D11Description(_RasterizerDesc), m_Hash);
	
	if (_pDbgName == nullptr)
	{
		HDXDEBUGNAME(m_pReference, "DXRasterizerState_%u", m_Hash);
	}
	else
	{
		HDXDEBUGNAME(m_pReference, "DXRasterizerState_%s", _pDbgName);
	}
}
//---------------------------------------------------------------------------------------------------
RasterizerStateDX11::~RasterizerStateDX11()
{
}
//---------------------------------------------------------------------------------------------------

RasterizerStateDX11::RasterizerStateDX11(const RasterizerStateDX11& _Other) :
	TDX11RasterizerStateRef(_Other), m_Description(_Other.m_Description)
{
}
//---------------------------------------------------------------------------------------------------
RasterizerStateDX11& Helix::Display::RasterizerStateDX11::operator=(const RasterizerStateDX11 & _Other)
{
	if (this == &_Other)
		return *this;

	TDX11RasterizerStateRef::operator=(_Other);
	m_Description = _Other.m_Description;
	return *this;
}

//---------------------------------------------------------------------------------------------------