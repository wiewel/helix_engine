//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef COMMONRENDERPASSES_H
#define COMMONRENDERPASSES_H

// standard include for all common render passes
#include "Display\RenderPassRegister.h"

namespace Helix
{
	namespace Display
	{
		// These helper stucts are evaluated (only once) when calling the functor ():
		// uint64_t id = g_kSolidPass; 
		static const PassID g_kSolidPass = "Solid";
		static const PassID g_kSkyboxPass = "DeferredSkybox";
		static const PassID g_kDeferredLightingPass = "DeferredLighting";
		static const PassID g_kDeferredMergePass = "DeferredMerge";
		static const PassID g_kShadowMapPass = "ShadowMap";
		static const PassID g_kDepthPrePass = "DepthPrePass";
		static const PassID g_kParaboloidShadowPass = "ParaboloidShadow";
		static const PassID g_kProjectedShadowPass = "ProjectedShadow";
		static const PassID g_kFluxVisualizerPass = "FluxVisualizer";
		static const PassID g_kFullscreenTexturePass = "FullscreenTexture";
		static const PassID g_kHorizontalBlurPass = "HorizontalBlur";
		static const PassID g_kVerticalBlurPass = "VerticalBlur";
		static const PassID g_kSimpleDoFPass = "SimpleDoF";
		static const PassID g_kGlitchPass = "Glitch";
		static const PassID g_kSSAOPass = "SSAO";
		static const PassID g_kScalePass = "Scale";
		static const PassID g_kLuminancePass = "Luminance";
		static const PassID g_kLuminanceAdaptionPass = "LuminanceAdaption";
		static const PassID g_kBloomPass = "Bloom";
		static const PassID g_kHDRCompositePass = "HDRComposite";
		static const PassID g_kParticlePass = "Particle";
		static const PassID g_kOceanPass = "Ocean";
		static const PassID g_kDebugLinesPass = "UnicolorVertex";
		static const PassID g_kGizmoPass = "Gizmo";

	} // Display
} // Helix

#endif // COMMONRENDERPASSES_H
