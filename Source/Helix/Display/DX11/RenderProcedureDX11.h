//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef RENDERPROCEDUREDX11_H
#define RENDERPROCEDUREDX11_H

#include "RenderPassDX11.h"
#include "Resources\RendererDescription.h"
#include "CommonRenderPasses.h"

namespace Helix
{
	namespace Display
	{
		class RenderProcedureDX11
		{
		public:
			RenderProcedureDX11(const std::string& _sName = {});
			~RenderProcedureDX11();

			// and returns a std::shared_ptr<RenderPassDX11> of a specialized renderpass matching to the typename
			bool Initialize(const Resources::RenderProcedureDesc& _Desc);

			// calls Default init of passes, this is only for manual use
			bool DefaultInitialize();

			void AddPass(std::shared_ptr<RenderPassDX11> _pRenderPass);

			Resources::RenderProcedureDesc CreateDescription() const;

			void SetEnabled(bool _bEnabled);
			bool IsEnabled() const;

			const std::string& GetName() const;

			std::vector<std::shared_ptr<RenderPassDX11>>& GetPasses();

		private:
			std::vector<std::shared_ptr<RenderPassDX11>> m_RenderPasses;

			bool m_bEnabled = true;
			std::string m_sName;
		};

		inline void RenderProcedureDX11::SetEnabled(bool _bEnabled)	{ m_bEnabled = _bEnabled;}
		inline bool RenderProcedureDX11::IsEnabled() const	{return m_bEnabled;}
		inline const std::string& RenderProcedureDX11::GetName() const	{return m_sName;}
		inline std::vector<std::shared_ptr<RenderPassDX11>>& RenderProcedureDX11::GetPasses() {	return m_RenderPasses; }

		

	} // Display
} // Helix

#endif // RENDERPROCEDUREDX11_H