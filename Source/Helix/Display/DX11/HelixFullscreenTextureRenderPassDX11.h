//   Copyright 2020 Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef HELIXFULLSCREENTEXTURERENDERPASSDX11_H
#define HELIXFULLSCREENTEXTURERENDERPASSDX11_H

#include "RenderPassDX11.h"

#include "CommonConstantBufferFunctions.h"
#include "Display\Shaders\FullscreenTexture.hlsl"

namespace Helix
{
	namespace Display
	{
		class HelixFullscreenTextureRenderPassDX11 : public RenderPassDX11
		{
		public:
			HDEBUGNAME("HelixFullscreenTextureRenderPassDX11");

			HelixFullscreenTextureRenderPassDX11(DefaultInitializerType);
			HelixFullscreenTextureRenderPassDX11(const std::string& _sInstanceName = {});
			virtual ~HelixFullscreenTextureRenderPassDX11();

			bool OnInitalize(const hlx::TextToken& _CustomVariables) final;
			bool OnPerObject(const RenderObjectDX11& _RenderObject, const MaterialDX11& _Material) final;

			bool OnPerCamera(const Scene::CameraComponent& _Camera) final;
			std::shared_ptr<RenderPassDX11> CreatePass(const std::string& _sInstanceName) const final;

		private:
			ShaderVariableDX11<Shaders::FullscreenTexture::cbFullscreenTexture> m_CBFullscreenTex = { "cbFullscreenTexture" };
			TCBPerObject m_CBPerObject = { "cbPerObject" };
			ShaderTextureDX11 m_FullscreenTexture = { "gTex" };
			ShaderRenderTargetDX11 m_RenderTarget = { "vColor" };

			TCBPerCamera m_CBPerCamera = { "cbPerCamera" };
		};

	} // Display
} // Helix

#endif // HELIXFULLSCREENTEXTURERENDERPASSDX11_H