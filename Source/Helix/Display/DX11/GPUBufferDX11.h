//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef GPUBUFFERDX11_H
#define GPUBUFFERDX11_H

#include "Display\GPUBuffer.h"

#include "hlx\src\StandardDefines.h"
#include "hlx\src\Logger.h"

// forward decls
struct ID3D11Buffer;
struct ID3D11ShaderResourceView;
struct ID3D11UnorderedAccessView;

namespace Helix
{
	namespace Display
	{
		// forward decls:
		class RenderDeviceDX11;

		class GPUBufferDX11 : public GPUBuffer
		{
		public:
			HDEBUGNAME("GPUBufferDX11");

			GPUBufferDX11(const std::string& _sDbgName = "");
			~GPUBufferDX11();

			GPUBufferDX11(const GPUBufferDX11& rhs) = delete;
			GPUBufferDX11& operator=(const GPUBufferDX11& rhs) = delete;

			bool Initialize(const BufferDesc& _BufferDesc) final;

			bool Apply(const uint8_t* _pData, uint32_t _uSize = 0, uint32_t _uOffset = 0) final;

			//bool HasInitialData() const;

			ID3D11Buffer* GetBuffer() const;
			ID3D11ShaderResourceView* GetShaderResourceView() const;
			ID3D11UnorderedAccessView* GetUnorderedAccessView() const;

		protected:			
			RenderDeviceDX11* m_pRenderDevice = nullptr;
			ID3D11Buffer* m_pBuffer = nullptr;
			ID3D11ShaderResourceView* m_pShaderResourceView = nullptr;
			ID3D11UnorderedAccessView* m_pUnorderedAccessView = nullptr;

			bool m_bHasInitialData = false;

			bool m_bSubsequentUpdate = false;

			std::string m_sDbgName;
		};

		inline ID3D11Buffer* GPUBufferDX11::GetBuffer() const { return m_pBuffer; }

		inline ID3D11ShaderResourceView * GPUBufferDX11::GetShaderResourceView() const { return m_pShaderResourceView; }

		inline ID3D11UnorderedAccessView * GPUBufferDX11::GetUnorderedAccessView() const { return m_pUnorderedAccessView; }

	} // Display
} // Helix
#endif // GPUBUFFERDX11_H
