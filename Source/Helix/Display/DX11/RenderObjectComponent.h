//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef RENDEROBJECTCOMPONENT_H
#define RENDEROBJECTCOMPONENT_H

#include "Datastructures\Component.h"
#include "RenderObjectDX11.h"

namespace Helix
{
	namespace Display
	{
		class IRenderObjectComponent : public Datastructures::IComponent
		{
			COMPTYPENAME(IRenderObjectComponent, RenderObjectDX11, 'Rndr')

		protected:
			IRenderObjectComponent(
				RenderObjectDX11* _pParent,
				Datastructures::TComponentCallbacks _kCallbacks = Datastructures::ComponentCallback_None);

		public:
			// template register constructor
			//IRenderObjectComponent(DefaultInitializerType);
			inline IRenderObjectComponent::IRenderObjectComponent(DefaultInitializerType) : IComponent(DefaultInit),m_pParent(nullptr){	}

			// deserialization constructor
			IRenderObjectComponent(RenderObjectDX11* _pParent, const Datastructures::ObjectType _kObjectType, Datastructures::TComponentCallbacks _kCallbacks);

			RenderObjectDX11* GetParent() const final;

		private:
			RenderObjectDX11* const m_pParent;
		};

		inline RenderObjectDX11* IRenderObjectComponent::GetParent() const { return m_pParent; }

	} // Display
} // Helix

#endif // !RENDEROBJECTCOMPONENT_H
