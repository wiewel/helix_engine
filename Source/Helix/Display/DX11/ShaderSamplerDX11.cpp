//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "ShaderSamplerDX11.h"
#include "ShaderInterfaceDX11.h"

using namespace Helix::Display;
//---------------------------------------------------------------------------------------------------

ShaderSamplerDX11::ShaderSamplerDX11(const std::string& _sSamplerName, bool _bUseDefaultSampler) :
	m_sName(_sSamplerName), m_bUseDefaultSampler(_bUseDefaultSampler)
{

}
//---------------------------------------------------------------------------------------------------

ShaderSamplerDX11::~ShaderSamplerDX11()
{
	if (m_pInterface != nullptr)
	{
		m_pInterface->UnregisterSampler(m_sName, m_sShaderInstanceName);
	}
}
//---------------------------------------------------------------------------------------------------

bool ShaderSamplerDX11::Initialize(ShaderInterfaceDX11* _pInterface, const std::string& _sShaderInstanceName, const bool _bWarnIfNotFound)
{
	if (m_pInterface == nullptr)
	{
		m_pInterface = _pInterface;
		m_sShaderInstanceName = _sShaderInstanceName;
	}
	else
	{
		HWARNINGD("Sampler %s has already been initialized!", CSTR(m_sName));
		return true;
	}

	if (m_bUseDefaultSampler)
	{
		for (uint32_t i = 0; i < DefaultSamplerState_NumOf; ++i)
		{
			if (std::string(DefaultSamplerStates[i].pName) == m_sName)
			{
				m_CurrentSampler = SamplerStateDX11(DefaultSamplerStates[i].kState);
				break;
			}
		}

		if (m_CurrentSampler.operator bool() == false)
		{
			HERROR("%s is not a default sampler state", CSTR(m_sName));
			return false;
		}
	}

	return m_pInterface->RegisterSampler(this, m_sShaderInstanceName, _bWarnIfNotFound);
}
//---------------------------------------------------------------------------------------------------
