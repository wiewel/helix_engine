//   Copyright 2020 Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef HELIXPARTICLERENDERPASSDX11_H
#define HELIXPARTICLERENDERPASSDX11_H

#include "RenderPassDX11.h"

#include "Display\Shaders\Particle.hlsl"
#include "CommonConstantBufferFunctions.h"

namespace Helix
{
	namespace Display
	{
		class HelixParticleRenderPassDX11 : public RenderPassDX11
		{
		public:
			HDEBUGNAME("HelixParticleRenderPassDX11");

			HelixParticleRenderPassDX11(DefaultInitializerType);
			HelixParticleRenderPassDX11(const std::string& _sInstanceName = {});
			~HelixParticleRenderPassDX11();

			bool OnInitalize(const hlx::TextToken& _CustomVariables) final;

			bool OnPerCamera(const Scene::CameraComponent& _Camera) final;
			bool OnPerObject(const RenderObjectDX11& _RenderObject, const MaterialDX11& _Material) final;
			std::shared_ptr<RenderPassDX11> CreatePass(const std::string& _sInstanceName) const final;

		private:
			ShaderVariableDX11<Shaders::Particle::cbParticle> m_CBParticle = { "cbParticle" };
			TCBPerCamera m_CBPerCamera = { "cbPerCamera" };

			ShaderTextureDX11 m_AlbedoMap = { "gAlbedoMap" };
			ShaderTextureDX11 m_DepthTexture = { "gDepthTex", "LINEAR_DEPTH" };

			ShaderRenderTargetDX11 m_RenderTarget = { "vColor", "BACKBUFFER" };
		};

	} // Display
} // Helix

#endif // HELIXPARTICLERENDERPASSDX11_H