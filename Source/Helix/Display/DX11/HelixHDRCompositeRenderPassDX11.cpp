//   Copyright 2020 Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "HelixHDRCompositeRenderPassDX11.h"

using namespace Helix;
using namespace Helix::Display;
using namespace Helix::Scene;
using namespace Helix::Datastructures;

//---------------------------------------------------------------------------------------------------

HelixHDRCompositeRenderPassDX11::HelixHDRCompositeRenderPassDX11(DefaultInitializerType) : RenderPassDX11(DefaultInit, "HDRComposite")
{
}

//---------------------------------------------------------------------------------------------------
HelixHDRCompositeRenderPassDX11::HelixHDRCompositeRenderPassDX11(const std::string& _sInstanceName) :
	RenderPassDX11("HDRComposite", _sInstanceName,
	{ &m_ColorMap, &m_LuminanceMap, &m_BloomMap },
	{ &m_OutputTexture },
	{ &m_CBHDRComposite })
{
}
//---------------------------------------------------------------------------------------------------
HelixHDRCompositeRenderPassDX11::~HelixHDRCompositeRenderPassDX11()
{
}
//---------------------------------------------------------------------------------------------------
std::shared_ptr<RenderPassDX11> HelixHDRCompositeRenderPassDX11::CreatePass(const std::string& _sInstanceName) const
{
	return std::make_shared<HelixHDRCompositeRenderPassDX11>(_sInstanceName);
}
//---------------------------------------------------------------------------------------------------
bool HelixHDRCompositeRenderPassDX11::OnInitalize(const hlx::TextToken& _CustomVariables)
{
	// Add RasterizerState
	RasterizerDesc RasDesc = {};
	RasDesc.m_kFillMode = FillMode_Solid;
	RasDesc.m_kCullMode = CullMode_None;
	RasDesc.m_bFrontCounterClockwise = false;

	SetRasterizerState(RasDesc);

	// Add BlendState
	RenderTargetBlendDesc RTBlendDesc = {};
	RTBlendDesc.m_bBlendEnable = false;

	BlendDesc AlphaBlendDesc = {};
	AlphaBlendDesc.m_bAlphaToCoverageEnable = false;
	AlphaBlendDesc.m_RenderTargets[0] = RTBlendDesc;
	AlphaBlendDesc.m_uSampleMask = 0xffffffff;
	AlphaBlendDesc.fBlendFactor = { 0.0f,0.0f,0.0f,0.0f };

	SetBlendState(AlphaBlendDesc);

	// Add DepthStencilState
	DepthStencilDesc DepthDesc = {};
	DepthDesc.m_bDepthEnable = false;
	DepthDesc.m_bStencilEnable = false;

	SetDepthStencilState(DepthDesc, "HDRComposite");

	// Default Parameter
	ClearRenderTargets(false);

	return m_bInitialized;
}
//---------------------------------------------------------------------------------------------------
bool HelixHDRCompositeRenderPassDX11::OnPerFrame()
{
	m_CBHDRComposite->fBloomIntensity = m_fBloomIntensity;
	m_CBHDRComposite->fLuminanceMipLevel = m_fLuminanceMipLevel;
	m_CBHDRComposite->fExposureKeyValue = m_fExposureKeyValue;

	uint32_t uClassPermutation = 0u;
	if(m_bAutoExposure)
	{
		uClassPermutation |= Shaders::HDRComposite::HPMPS_AutoExposure;
	}
	Select(ShaderType_PixelShader, uClassPermutation);

	return true;
}
//---------------------------------------------------------------------------------------------------
