//   Copyright 2020 Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "HelixShadowMapRenderPassDX11.h"
#include "Display\Shaders\ShadowMap.hlsl"
#include "Math\MathTypes.h"
#include "Scene\LightContext.h"
#include "Scene\CameraManager.h"
#include "Util\Functional.h"

using namespace Helix;
using namespace Helix::Display;
using namespace Helix::Scene;
using namespace Helix::Math;

//---------------------------------------------------------------------------------------------------

HelixShadowMapRenderPassDX11::HelixShadowMapRenderPassDX11(DefaultInitializerType) : RenderPassDX11(DefaultInit, "ShadowMap")
{
}

//---------------------------------------------------------------------------------------------------
HelixShadowMapRenderPassDX11::HelixShadowMapRenderPassDX11(const std::string& _sInstanceName) :
	RenderPassDX11( "ShadowMap", _sInstanceName,
					{}, // Textures
					{}, // RenderTargtes
					{ &m_CBPerCamera, &m_CBPerObject }) // Variables
{
}
//---------------------------------------------------------------------------------------------------
HelixShadowMapRenderPassDX11::~HelixShadowMapRenderPassDX11()
{
	HSAFE_FUNC_RELEASE(m_pLightSpaceCamera, Destroy);
}
//---------------------------------------------------------------------------------------------------
bool HelixShadowMapRenderPassDX11::OnInitalize(const hlx::TextToken& _CustomVariables)
{
	// Add RasterizerState
	RasterizerDesc RasDesc = {};
	RasDesc.m_kFillMode = FillMode_Solid;
	RasDesc.m_kCullMode = CullMode_Back;
	RasDesc.m_bFrontCounterClockwise = false;
	RasDesc.m_iDepthBias = 100000;
	RasDesc.m_fDepthBiasClamp = 0.0f;
	RasDesc.m_fSlopeScaledDepthBias = 1.0f;

	SetRasterizerState(RasDesc);

	// Add BlendState
	RenderTargetBlendDesc RTBlendDesc = {};
	RTBlendDesc.m_bBlendEnable = false;
	RTBlendDesc.m_kSrcBlend = BlendVariable_Src_Alpha;
	RTBlendDesc.m_kDestBlend = BlendVariable_One;
	RTBlendDesc.m_kBlendOp = BlendOperation_Add;
	RTBlendDesc.m_kSrcBlendAlpha = BlendVariable_Zero;
	RTBlendDesc.m_kDestBlendAlpha = BlendVariable_Zero;
	RTBlendDesc.m_kBlendOpAlpha = BlendOperation_Add;
	RTBlendDesc.m_uRenderTargetWriteMask = Channel_All;

	BlendDesc AlphaBlendDesc = {};
	AlphaBlendDesc.m_bAlphaToCoverageEnable = false;
	AlphaBlendDesc.m_RenderTargets[0] = RTBlendDesc;
	AlphaBlendDesc.m_uSampleMask = 0xffffffff;
	AlphaBlendDesc.fBlendFactor = { 0.0f,0.0f,0.0f,0.0f };

	SetBlendState(AlphaBlendDesc);

	// Add DepthStencilState
	DepthStencilDesc DepthDesc = {};
	DepthDesc.m_bDepthEnable = true;
	DepthDesc.m_kDepthWriteMask = DepthWriteMask_All;
	DepthDesc.m_kDepthFunc = ComparisonFunction_Less;
	DepthDesc.m_bStencilEnable = false;

	SetDepthStencilState(DepthDesc, "ShadowMap");

	// Default Parameter
	ClearRenderTargets(false);
	ClearDepthStencil(true);

	return m_bInitialized;
}
//---------------------------------------------------------------------------------------------------
bool HelixShadowMapRenderPassDX11::OnPerCamera(const CameraComponent& _LightCamera)
{
	LightDesc MainLight;
	if (LightContext::Instance()->GetMainDirLight(MainLight) == false)
	{
		return false;
	}

	//static float fRANDOM_DEBUG_VARIABLE = 12.0f;

	/// 1. Construct "scene" bounding sphere at camera location
	float fSceneRadius = (m_pMainCamera->GetFarDistance() - m_pMainCamera->GetNearDistance());
	float3 vScenePosWS = float3(0, 0, 0);
	//fSceneRadius /= fRANDOM_DEBUG_VARIABLE; // heuristic, play with this value

	/// 2. Construct Light view and projection
	float3 vLightDir = MainLight.vDirection.getNormalized();
	float3 vLightPosWS = /*-2.0f **/ -1.0f * fSceneRadius * vLightDir + vScenePosWS;
	float3 vTargetPosWS = vScenePosWS;
	float3 vLightUp = float3(0, 1, 0);
	float4x4 mLightView;

	MatrixLookAtLH(vLightPosWS, vTargetPosWS, vLightUp, mLightView);
	mLightView = mLightView.getTranspose();
	float3 vTargetPosLS = mLightView.transform(vTargetPosWS);

	/// 3. Create orthogonal frustum in light space, based on vTargetPosLS
	float l = vTargetPosLS.x - fSceneRadius;
	float b = vTargetPosLS.y - fSceneRadius;
	float n = vTargetPosLS.z - fSceneRadius;
	float r = vTargetPosLS.x + fSceneRadius;
	float t = vTargetPosLS.y + fSceneRadius;
	float f = vTargetPosLS.z + fSceneRadius;

	float fLightNearZ = n;
	float fLightFarZ = f;

	float4x4 mLightProjection;
	MatrixOrthographicOffCenterLH(l, r, b, t, n, f, mLightProjection);

	/// 4. Create NDC [-1,1] -> Texture Space [0,1] transform
	float4x4 T = {
		{ 0.5f,  0.0f, 0.0f, 0.0f },
		{ 0.0f, -0.5f, 0.0f, 0.0f },
		{ 0.0f,  0.0f, 1.0f, 0.0f },
		{ 0.5f,  0.5f, 0.0f, 1.0f }
	};

	// -> shadow transform S
	float4x4 S = T * mLightProjection * mLightView;
	S = S.getTranspose();

	/// 5. Set CB
	float4x4 mLightViewProj = mLightProjection * mLightView;
	m_CBPerCamera->mViewProj = mLightViewProj;

	LightContext::Instance()->SetShadowTransform(S); // used in deferred lighting

	return true;
}
//---------------------------------------------------------------------------------------------------
bool HelixShadowMapRenderPassDX11::OnPreRender()
{
	if (m_pMainCamera == nullptr)
	{
		m_pMainCamera = Scene::CameraManager::Instance()->GetCameraByName("MainCamera");
	}

	if (m_pLightSpaceCamera == nullptr)
	{
		float fAspectRatio = GetDepthStencilTexture() ? (GetDepthStencilTexture().GetViewport().fWidth / GetDepthStencilTexture().GetViewport().fHeight) : 1.f;
		Datastructures::GameObject* pLightCamera = Datastructures::GameObjectPool::Instance()->Create().get();
		m_pLightSpaceCamera = pLightCamera->AddComponent<Scene::CameraComponent>("MainLightCamera", 80.f, 16.f / 9.f, 0.1f, 150.f);
		m_pLightSpaceCamera->SetFlag(m_kIdentifier);
	}

	return true;
}
//---------------------------------------------------------------------------------------------------

std::shared_ptr<RenderPassDX11> HelixShadowMapRenderPassDX11::CreatePass(const std::string& _sInstanceName) const
{
	return std::make_shared<HelixShadowMapRenderPassDX11>(_sInstanceName);
}
//---------------------------------------------------------------------------------------------------
