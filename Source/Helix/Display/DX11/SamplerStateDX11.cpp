//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "SamplerStateDX11.h"
#include "hlx\src\Logger.h"
#include "ViewDefinesDX11.h"

using namespace Helix::Display;
//---------------------------------------------------------------------------------------------------
SamplerStateDX11::SamplerStateDX11(DefaultSamplerState kDefaultState)
{
	SamplerStatePoolDX11* pSamplerPool = SamplerStatePoolDX11::Instance();

	m_pReference = pSamplerPool->Get(pSamplerPool->GetDefaultDesc(kDefaultState), m_Hash);
	m_Description = ConvertToHelixDescription(pSamplerPool->GetDefaultDesc(kDefaultState));

	switch (kDefaultState)
	{
	case DefaultSamplerState_PointClamp:
		HDXDEBUGNAME(m_pReference, "SamplerState_PointClamp");
		break;
	case DefaultSamplerState_PointWrap:
		HDXDEBUGNAME(m_pReference, "SamplerState_PointWrap");
		break;
	case DefaultSamplerState_LinearClamp:
		HDXDEBUGNAME(m_pReference, "SamplerState_LinearClamp");
		break;
	case DefaultSamplerState_LinearWrap:
		HDXDEBUGNAME(m_pReference, "SamplerState_LinearWrap");
		break;
	case DefaultSamplerState_AnisotropicClamp:
		HDXDEBUGNAME(m_pReference, "SamplerState_AniosotropicClamp");
		break;
	case DefaultSamplerState_AnisotropicWrap:
		HDXDEBUGNAME(m_pReference, "SamplerState_AnisotropicWrap");
		break;
	case DefaultSamplerState_ShadowMap:
		HDXDEBUGNAME(m_pReference, "SamplerState_ShadowMap");
		break;
	default:
		break;
	}
}
//---------------------------------------------------------------------------------------------------
SamplerStateDX11::SamplerStateDX11(const SamplerDesc& _Desc, const char* _pDbgName) :
	m_Description(_Desc)
{
	SamplerStatePoolDX11::Instance()->Get(ConvertToD3D11Description(_Desc), m_Hash);
	HDXDEBUGNAME(m_pReference, _pDbgName);	
}
//---------------------------------------------------------------------------------------------------
SamplerStateDX11::~SamplerStateDX11()
{
}
//---------------------------------------------------------------------------------------------------
SamplerStateDX11::SamplerStateDX11(const SamplerStateDX11& _Other) :
	TDX11SamplerStateRef(_Other), m_Description(_Other.m_Description)
{
}
//---------------------------------------------------------------------------------------------------
SamplerStateDX11& SamplerStateDX11::operator=(const SamplerStateDX11& _Other)
{
	if (this == &_Other)
		return *this;

	TDX11SamplerStateRef::operator=(_Other);
	m_Description = _Other.m_Description;
	return *this;
}
//---------------------------------------------------------------------------------------------------
