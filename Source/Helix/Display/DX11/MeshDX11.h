//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef MESHDX11_H
#define MESHDX11_H

#include <vector>
#include <d3d11.h>
#include "Math\XMMath.h"
#include "Util\HelixDefines.h"
#include "Display\Mesh.h"
#include "Util/Logger.h"
#include "VertexBufferDX11.h"
#include "IndexBufferDX11.h"

#include "ViewDefinesDX11.h"

namespace Helix
{
	namespace Display
	{
		class MeshDX11 : public Mesh
		{
			//---------------------------------------------------------------------------------------------------
			// No copying
			MeshDX11(const MeshDX11& rhs) = delete;
			MeshDX11& operator=(const MeshDX11& rhs) = delete;
			//---------------------------------------------------------------------------------------------------
		public:
			HDEBUGNAME("MeshDX11");
			//---------------------------------------------------------------------------------------------------
			MeshDX11(bool _bDynamic = false);
			~MeshDX11();
			//---------------------------------------------------------------------------------------------------
			virtual void Draw();

			//---------------------------------------------------------------------------------------------------
			static std::shared_ptr<MeshDX11> MeshDX11::Generate2DQuad(const float _fScreenWidthPercentage, const float _fScreenHeightPercentage);
			static std::shared_ptr<MeshDX11> GenerateCube(float _fSize);
			//void GenerateTriangle();
			static std::shared_ptr<MeshDX11> GenerateSphere(float _fDiameter, size_t _uTessellation);
			//---------------------------------------------------------------------------------------------------
			VertexBufferDX11* GetVertexBuffer() const;
			IndexBufferDX11* GetIndexBuffer() const;
		private:
			virtual void InitializeIndexBuffer(PixelFormat _kIndexFormat);
			virtual void InitializeVertexBuffer();

			static void ComputeTangentBasis(const XMFLOAT3& _vP0, const XMFLOAT3& _vP1, const XMFLOAT3& _vP2,
				const XMFLOAT2& _vUV0, const XMFLOAT2& _vUV1, const XMFLOAT2& _vUV2,
				XMFLOAT3 &_vNormal, XMFLOAT3 &_vTangent, XMFLOAT3 &_vBitangent);
		private:
			ID3D11DeviceContext* m_pDeviceContext = nullptr;

			VertexBufferDX11* m_pVertexBuffer = nullptr;
			IndexBufferDX11* m_pIndexBuffer = nullptr;
		};
		
		inline VertexBufferDX11* MeshDX11::GetVertexBuffer() const{ return m_pVertexBuffer; }
		inline IndexBufferDX11* MeshDX11::GetIndexBuffer() const{ return m_pIndexBuffer; }

	}; // namespace Display
}; // namespace Helix

#endif // MESHDX11_H
