//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef MATERIALDX11_H
#define MATERIALDX11_H

#include "MaterialPoolDX11.h"
#include "DataStructures\SyncVariable.h"

namespace Helix
{
	namespace Display
	{
		class MaterialDX11 : public TDX11MaterialRef
		{
		public:
			// Default constructor
			MaterialDX11(std::nullptr_t p = nullptr) noexcept;
			MaterialDX11(const DefaultInitializerType&);

			// Create new material based on description
			MaterialDX11(const MaterialDesc& _Desc);
			// Load material from file
			MaterialDX11(
				const std::string& _sNameOrFilePath,
				bool _bLoadTextures = true,
				bool _bLoadFromFile = true,
				bool _bPropFile = true,
				const std::string& _sAlbedo = "",
				const std::string& _sNormal = "",
				const std::string& _sMetallic = "",
				const std::string& _sRoughness = "",
				const std::string& _sHeight = "",
				const std::string& _sEmissive = "");

			// Cleanup
			~MaterialDX11();

			// Increments the ref count
			MaterialDX11(const MaterialDX11& _Material);
			// Increments the ref count
			MaterialDX11& operator=(const MaterialDX11& _Other);
			MaterialDX11& operator=(std::nullptr_t);

			// creates new material with valid reference (given a unique name was supplied)
			static MaterialDX11 CreateNew(const std::string& _sNameOrFilePath, const bool _bUseDefaultPasses = true);

			const MaterialProperties& GetProperties() const;
			MaterialProperties& GetProperties();

			const ImmutableTextureDX11& GetAlbedoMap() const;
			const ImmutableTextureDX11& GetNormalMap() const;
			const ImmutableTextureDX11& GetMetallicMap() const;
			const ImmutableTextureDX11& GetRoughnessMap() const;
			const ImmutableTextureDX11& GetHeightMap() const;
			const ImmutableTextureDX11& GetEmissiveMap() const;

			HNCL(Datastructures::SyncGroup& GetMaterialGroup());

			std::string GetPathOrName() const;

			bool CheckRenderPass(uint64_t _kPass) const;
			void EnableRenderPass(uint64_t _kPass);
			void DisableRenderPass(uint64_t _kPass);

			const uint64_t& GetExportHash() const;
			void SetExportHash(const uint64_t& _uHash) const;
		private:

#ifdef HNUCLEO
			// these functions are just for use in nucleo!
			void SetAlbedoMapFile(const std::string& _sPath);
			std::string GetAlbedoMapFile() const;

			void SetMetallicMapFile(const std::string& _sPath);
			std::string GetMetallicMapFile() const;

			void SetNormalMapFile(const std::string& _sPath);
			std::string GetNormalMapFile() const;

			void SetRoughnessMapFile(const std::string& _sPath);
			std::string GetRoughnessMapFile() const;

			void SetHeightMapFile(const std::string& _sPath);
			std::string GetHeightMapFile() const;

			void SetEmissiveMapFile(const std::string& _sPath);
			std::string GetEmissiveMapFile() const;
#endif

		private:
			MaterialProperties m_Properties = {};
			mutable uint64_t m_uExportHash = 0u;

#ifdef HNUCLEO
			SYNOBJ(Albedo, MaterialDX11, std::string, GetAlbedoMapFile, SetAlbedoMapFile) m_SyncAlbedoTex = {*this, "Albedo Map"};
			SYNOBJ(Metallic, MaterialDX11, std::string, GetMetallicMapFile, SetMetallicMapFile) m_SyncMetallicTex = { *this, "Metallic Map" };
			SYNOBJ(Roughness, MaterialDX11, std::string, GetRoughnessMapFile, SetRoughnessMapFile) m_SyncRoughnessTex = { *this, "Roughness Map" };
			SYNOBJ(Normal, MaterialDX11, std::string, GetNormalMapFile, SetNormalMapFile) m_SyncNormalTex = { *this, "Normal Map" };
			SYNOBJ(Height, MaterialDX11, std::string, GetHeightMapFile, SetHeightMapFile) m_SyncHeightTex = { *this, "Height Map" };
			SYNOBJ(Emissive, MaterialDX11, std::string, GetEmissiveMapFile, SetEmissiveMapFile) m_SyncEmissiveTex = { *this, "Emissive Map" };

			Datastructures::SyncVariable<Math::float3> m_SyncAlbedo = { m_Properties.vAlbedo, "Albedo Color" };

			Datastructures::SyncVariable<Math::float3> m_SyncEmissiveColor = { m_Properties.vEmissiveColor, "Emissive Color" };
			Datastructures::SyncVariable<Math::float3> m_SyncEmissiveOffset = { m_Properties.vEmissiveOffset, "Emissive Offset" };
			Datastructures::SyncVariable<Math::float3> m_SyncEmissiveScale = { m_Properties.vEmissiveScale, "Emissive Scale" };

			Datastructures::SyncVariable<float> m_SyncMetallic = { m_Properties.fMetallic, "Metallic" };
			Datastructures::SyncVariable<float> m_SyncRoughness = { m_Properties.fRoughness, "Roughness" };
			Datastructures::SyncVariable<float> m_SyncRoughnessOffset = { m_Properties.fRoughnessOffset, "RoughnessOffset" };
			Datastructures::SyncVariable<float> m_SyncRoughnessScale = { m_Properties.fRoughnessScale, "RoughnessScale" };
			Datastructures::SyncVariable<float> m_SyncHeightScale= { m_Properties.fHeightScale, "HeightScale" };
			Datastructures::SyncVariable<float> m_SyncHorizonFade = { m_Properties.fHorizonFade, "HorizonFade" };
			Datastructures::SyncVariable<Math::float2> m_SyncTextureTiling = { m_Properties.vTextureTiling, "TextureTiling" };
			Datastructures::SyncVariable<std::string> m_SyncSubMeshName = { m_Properties.sAssociatedSubMesh, "SubMesh", Datastructures::WidgetLayout_Horizontal, true }; // readonly

			Datastructures::SyncGroup m_SyncMaterialGroup = {"Material Poperties",
			{&m_SyncSubMeshName, &m_SyncAlbedoTex, &m_SyncMetallicTex,&m_SyncRoughnessTex,&m_SyncNormalTex,&m_SyncHeightTex, &m_SyncEmissiveTex,
			&m_SyncAlbedo, &m_SyncEmissiveColor, &m_SyncEmissiveOffset, &m_SyncEmissiveScale, &m_SyncMetallic, &m_SyncRoughness,&m_SyncRoughnessOffset,
			&m_SyncRoughnessScale,&m_SyncHeightScale, &m_SyncHorizonFade,&m_SyncTextureTiling } };
#endif
		};

		inline const MaterialProperties& MaterialDX11::GetProperties() const { return m_Properties; }
		inline MaterialProperties& MaterialDX11::GetProperties() { return  m_Properties; }

		inline bool MaterialDX11::CheckRenderPass(uint64_t _kPass) const{return m_Properties.kRenderPasses.CheckFlag(_kPass);}
		inline void MaterialDX11::EnableRenderPass(uint64_t _kPass)	{m_Properties.kRenderPasses.SetFlag(_kPass);}
		inline void MaterialDX11::DisableRenderPass(uint64_t _kPass){ m_Properties.kRenderPasses.ClearFlag(_kPass); }

		inline const uint64_t& MaterialDX11::GetExportHash() const{return m_uExportHash;}
		inline void MaterialDX11::SetExportHash(const uint64_t& _uHash)	const {m_uExportHash = _uHash;}

		inline const ImmutableTextureDX11& MaterialDX11::GetAlbedoMap() const { return m_pReference->AlbedoMap; }
		inline const ImmutableTextureDX11& MaterialDX11::GetNormalMap() const { return m_pReference->NormalMap; }
		inline const ImmutableTextureDX11& MaterialDX11::GetMetallicMap() const { return m_pReference->MetallicMap; }
		inline const ImmutableTextureDX11& MaterialDX11::GetRoughnessMap() const { return m_pReference->RoughnessMap; }
		inline const ImmutableTextureDX11& MaterialDX11::GetHeightMap() const { return m_pReference->HeightMap; }
		inline const ImmutableTextureDX11& MaterialDX11::GetEmissiveMap() const { return m_pReference->EmissiveMap; }

		HNCL(inline Datastructures::SyncGroup& MaterialDX11::GetMaterialGroup() {return m_SyncMaterialGroup;})

	} // Display
} // Helix

#endif //MATERIALDX11_H