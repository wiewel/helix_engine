//   Copyright 2020 Fabian Wahlster
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef APSSTATISTICS_H
#define APSSTATISTICS_H

#include "Util\Timer.h"
#include <vector>
#include "RenderPassDX11.h"

namespace Helix
{
	namespace Display
	{
		// forward decl
		class RenderPassDX11;

		struct SelectedPassPerm
		{
			RenderPassDX11* pPass = nullptr;
			ShaderPermutationHash Permutation;
			uint32_t uSelectCount = 0u;
		};

		struct IntervalStats
		{
			std::vector<SelectedPassPerm> SelectedPerms;
			uint32_t uShaderChanges = 0u;
			int64_t iFrameTimes = 0;
			uint32_t uSampelCount = 0u;
			int64_t iGlobalStartTime = 0;
			uint32_t uPolyCount = 0u;
			float fTotalSelectedAvgPF = 0.f;
			float fSceneCost = 0.f;
			uint32_t uAPS = 0u;
			float fAPSFindTime = 0.f;
		};

		class APSStatistics
		{
		public:
			APSStatistics(float _fRuntimeInSec = 60.f, float _fIntervalInSec =  0.1f);

			~APSStatistics();

			// returns false if benchmark stopped
			bool Add(int64_t _iFrameTime,
				uint32_t _uShaderChanges,
				float _fTotalSelectedAvgPF,
				float _fSceneCost,
				uint32_t _uPolyCount,
				const std::vector<SelectedPassPerm>& _SelectedPassPerms,
				bool _bAPS,
				float _fAPSFindTime);

			void Save(const std::string& _sTargetCSVPath);

			float SpanToSec(const int64_t& _SpanCount) const;

		private:
			int64_t m_iFrequency = 0;
			int64_t m_iRuntime = 0;
			int64_t m_iIntervalLength = 0;
			uint32_t m_uCount = 0u;
			std::vector<IntervalStats> m_Statistics;
			IntervalStats m_CurInterval;
		};
		//---------------------------------------------------------------------------------------------------

		inline float APSStatistics::SpanToSec(const int64_t& _SpanCount) const
		{
			return static_cast<float>(_SpanCount) / static_cast<float>(m_iFrequency);
		}
		//---------------------------------------------------------------------------------------------------

		inline bool APSStatistics::Add(
			int64_t _iFrameTime,
			uint32_t _uShaderChanges,
			float _fTotalSelectedAvgPF,
			float _fSceneCost,
			uint32_t _uPolyCount,
			const std::vector<SelectedPassPerm>& _SelectedPassPerms,
			bool _bAPS,
			float _fAPSFindTime)
		{
			if (m_Statistics.size() >= m_uCount)
			{
				return false;
			}

			int64_t iCurTime = Timer::GetCurrentCount();

			if (m_CurInterval.iGlobalStartTime == 0)
			{
				m_CurInterval.iGlobalStartTime = iCurTime;
			}

			if ((iCurTime - m_CurInterval.iGlobalStartTime) <= m_iIntervalLength)
			{
				bool bFound = false;
				for (const SelectedPassPerm& NewPerms: _SelectedPassPerms)
				{
					for (SelectedPassPerm& OldPerms : m_CurInterval.SelectedPerms)
					{
						if (NewPerms.pPass == OldPerms.pPass)
						{
							if (OldPerms.uSelectCount < NewPerms.uSelectCount)
							{
								OldPerms = NewPerms;
							}

							bFound = true;
							break;
						}
					}

					if (bFound == false)
					{
						m_CurInterval.SelectedPerms.push_back(NewPerms);
					}
				}

				m_CurInterval.fAPSFindTime += _fAPSFindTime;
				m_CurInterval.uAPS += _bAPS ? 1u : 0u;
				m_CurInterval.uPolyCount += _uPolyCount;
				m_CurInterval.fSceneCost += _fSceneCost;
				m_CurInterval.fTotalSelectedAvgPF += _fTotalSelectedAvgPF;
				m_CurInterval.uShaderChanges += _uShaderChanges;
				m_CurInterval.iFrameTimes += _iFrameTime;
				++m_CurInterval.uSampelCount;
			}
			else
			{
				// store previous interval info
				m_Statistics.push_back(m_CurInterval);

				// reset / initialize
				m_CurInterval.fAPSFindTime = _fAPSFindTime;
				m_CurInterval.uAPS = _bAPS ? 1u : 0u;
				m_CurInterval.uPolyCount = _uPolyCount;
				m_CurInterval.SelectedPerms = _SelectedPassPerms;
				m_CurInterval.fSceneCost = _fSceneCost;
				m_CurInterval.fTotalSelectedAvgPF = _fTotalSelectedAvgPF;
				m_CurInterval.iGlobalStartTime = iCurTime;
				m_CurInterval.uSampelCount = 1u;
				m_CurInterval.iFrameTimes = _iFrameTime;
				m_CurInterval.uShaderChanges = _uShaderChanges;
			}

			return true;
		}
	} // Display
} // Helix


#endif // !APSSTATISTICS_H
