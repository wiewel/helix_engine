//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "MeshPoolDX11.h"

#include "GPUBufferDX11.h"
#include "MeshClusterDX11.h"
#include "MaterialDX11.h"
#include "Resources\H3DFile.h"
#include "Resources\FileSystem.h"
#include "Display\Geometry.h"
#include "Physics\GeometryShapeFactory.h"

using namespace Helix::Physics;
using namespace Helix::Display;

MeshClusterDX11 MeshPoolDX11::m_ErrorCube = nullptr;

//---------------------------------------------------------------------------------------------------
MeshPoolDX11::MeshPoolDX11()
{
}
//---------------------------------------------------------------------------------------------------
MeshPoolDX11::~MeshPoolDX11()
{
}
//---------------------------------------------------------------------------------------------------
uint64_t MeshPoolDX11::HashFunction(const MeshClusterDesc& _Desc)
{
	HASSERTD(_Desc.m_sFilePathOrName.empty() == false, "Invalid empty file path or name");

	// TODO: check out why offsetof fails in release mode
	uint64_t uHash = 0;//DefaultHashFunction(reinterpret_cast<const uint8_t*>(&_Desc), offsetof(MeshClusterDesc, m_sFilePathOrName));

	uHash ^= StrHash(Resources::FileSystem::GetShortName(STR(_Desc.m_sFilePathOrName)));

	// make this texture unique even if the description is equal
	if (_Desc.m_bPooled == false)
	{
		uHash ^= m_ID.NextID();
	}

	//_Desc.m_bPooled is ignored in the checksum

	return uHash;
}
//---------------------------------------------------------------------------------------------------
MeshClusterReferenceDX11* MeshPoolDX11::CreateFromDesc(const MeshClusterDesc& _Desc, const uint64_t& _Hash)
{
	if (_Desc.m_bLoadFromFile)
	{
		return LoadFromFile(_Desc);
	}
	else
	{
		return CreateNew(_Desc);
	}
}
//---------------------------------------------------------------------------------------------------
uint64_t MeshPoolDX11::GetDefaultKey()
{
	if (m_ErrorCube == nullptr)
	{
		m_ErrorCube = Geometry::GenerateCube(1.f, "HELiXERRORCUBE");
	}

	return m_ErrorCube.GetKey();
}
//---------------------------------------------------------------------------------------------------
MeshClusterReferenceDX11* MeshPoolDX11::CreateNew(const MeshClusterDesc& _Desc)
{
	BufferDesc VertexDesc = {};
	BufferDesc IndexDesc = {};

	const bool bUAV = _Desc.m_kAdditionalBindFlags & ResourceBindFlag_UnorderedAccess;

	VertexDesc.kBindFlag = _Desc.m_kAdditionalBindFlags | ResourceBindFlag_VertexBuffer;
	VertexDesc.kCPUAccessFlag = _Desc.m_kCPUAccessFlag;
	VertexDesc.kResourceFlag = bUAV ? MiscResourceFlag_Buffer_AllowRawViews : MiscResourceFlag_None;
	VertexDesc.kUsageFlag = _Desc.m_kUsageFlag;
	VertexDesc.SubresourceData = _Desc.m_VertexSubresourceData;
	VertexDesc.uSize = 0;
	VertexDesc.kUAVFlag = bUAV ? BufferUAVFlag_Raw : BufferUAVFlag_None;

	IndexDesc.kBindFlag = _Desc.m_kAdditionalBindFlags | ResourceBindFlag_IndexBuffer;
	IndexDesc.kCPUAccessFlag = _Desc.m_kCPUAccessFlag;
	IndexDesc.kResourceFlag = bUAV ? MiscResourceFlag_Buffer_AllowRawViews : MiscResourceFlag_None;
	IndexDesc.kUsageFlag = _Desc.m_kUsageFlag;
	IndexDesc.SubresourceData = _Desc.m_IndexSubresourceData;
	IndexDesc.uSize = 0;
	IndexDesc.kUAVFlag = bUAV ? BufferUAVFlag_Raw : BufferUAVFlag_None;

	// Create new:
	uint32_t uCalculatedVertexBufferSize = 0;
	uint32_t uCalculatedIndexBufferSize = 0;

	// compute minimum index & vertex buffer size
	for (const SubMesh& Mesh : _Desc.m_MeshCluster)
	{
		uCalculatedVertexBufferSize += Mesh.m_uVertexCount * Mesh.m_uVertexStrideSize;
		if (VertexDesc.uStructureByteStride == 0)
		{
			VertexDesc.uStructureByteStride = Mesh.m_uVertexStrideSize;
		}
		else if (VertexDesc.uStructureByteStride == Mesh.m_uVertexStrideSize)
		{
			HWARNING("Inconsistent vertex stride size in cluster %s", CSTR(_Desc.m_sFilePathOrName));
		}
		
		uCalculatedIndexBufferSize += Mesh.m_uIndexCount * Mesh.m_uIndexElementSize;
		if (IndexDesc.uStructureByteStride == 0)
		{
			IndexDesc.uStructureByteStride = Mesh.m_uIndexElementSize;
		}
		else if (IndexDesc.uStructureByteStride == Mesh.m_uIndexElementSize)
		{
			HWARNING("Inconsistent index stride size in cluster %s", CSTR(_Desc.m_sFilePathOrName));
		}
	}

	// ensure minimum size
	VertexDesc.uSize = uCalculatedVertexBufferSize;

	// ensure minimum size
	IndexDesc.uSize = uCalculatedIndexBufferSize;

	//Create new Mesh Reference
	MeshClusterReferenceDX11* pMeshRef = new MeshClusterReferenceDX11();
	pMeshRef->sClusterName = _Desc.m_sFilePathOrName;

	bool bFailed = true;

	//Create vertex buffer
	if (VertexDesc.uSize > 0)
	{
		pMeshRef->m_pVertexBuffer = new GPUBufferDX11("VertexBuffer_" + Resources::FileSystem::GetFileNameWithoutExtension(_Desc.m_sFilePathOrName));
		bFailed = pMeshRef->m_pVertexBuffer->Initialize(VertexDesc) == false;
	}

	//Create index buffer
	if (bFailed == false && IndexDesc.uSize > 0)
	{
		pMeshRef->m_pIndexBuffer = new GPUBufferDX11("IndexBuffer_" + Resources::FileSystem::GetFileNameWithoutExtension(_Desc.m_sFilePathOrName));
		bFailed = pMeshRef->m_pIndexBuffer->Initialize(IndexDesc) == false;
	}

	if (bFailed)
	{
		HERROR("Failed to create vertex/index buffer (zero size)");
		HSAFE_DELETE(pMeshRef);
		return nullptr;
	}

	pMeshRef->m_SubMeshes = _Desc.m_MeshCluster;

	return pMeshRef;
}
//---------------------------------------------------------------------------------------------------
MeshClusterReferenceDX11* MeshPoolDX11::LoadFromFile(const MeshClusterDesc& _Desc)
{
	BufferDesc VertexDesc = {};
	BufferDesc IndexDesc = {};

	const bool bUAV = _Desc.m_kAdditionalBindFlags & ResourceBindFlag_UnorderedAccess;

	VertexDesc.kBindFlag = _Desc.m_kAdditionalBindFlags | ResourceBindFlag_VertexBuffer;
	VertexDesc.kCPUAccessFlag = _Desc.m_kCPUAccessFlag;
	VertexDesc.kResourceFlag = bUAV ? MiscResourceFlag_Buffer_AllowRawViews : MiscResourceFlag_None;
	VertexDesc.kUsageFlag = _Desc.m_kUsageFlag;
	VertexDesc.uSize = 0;
	VertexDesc.kUAVFlag = bUAV ? BufferUAVFlag_Raw : BufferUAVFlag_None;

	IndexDesc.kBindFlag = _Desc.m_kAdditionalBindFlags | ResourceBindFlag_IndexBuffer;
	IndexDesc.kCPUAccessFlag = _Desc.m_kCPUAccessFlag;
	IndexDesc.kResourceFlag = bUAV ? MiscResourceFlag_Buffer_AllowRawViews : MiscResourceFlag_None;
	IndexDesc.kUsageFlag = _Desc.m_kUsageFlag;
	IndexDesc.uSize = 0;
	IndexDesc.kUAVFlag = bUAV ? BufferUAVFlag_Raw : BufferUAVFlag_None;

	hlx::fbytestream FileStream;
	if (Resources::FileSystem::Instance()->OpenFileStream(Resources::HelixDirectories_Models, STR(_Desc.m_sFilePathOrName), std::ios_base::in | std::ios_base::binary, FileStream) == false)
	{
		// failed to open h3d file
		return nullptr;
	}

	{
		// load file at once
		hlx::bytes&& Buffer = FileStream.get<hlx::bytes>(FileStream.size());
		FileStream.close();

		hlx::bytestream BStream(Buffer);
		Resources::H3DFile H3DFile;

		TMeshCluster SubMeshes;

		if (H3DFile.LoadHeader(BStream) == false ||
			H3DFile.LoadDirectlyFromStream(BStream, SubMeshes, VertexDesc.SubresourceData, IndexDesc.SubresourceData) == false)
		{
			return nullptr;
		}

		//Create new Mesh Reference
		MeshClusterReferenceDX11* pMeshRef = new MeshClusterReferenceDX11();
		pMeshRef->sClusterName = H3DFile.GetName();
		//pMeshRef->sFilePath = _Desc.m_sFilePathOrName;

		// Load physx mesh to convexmeshpool
		if (H3DFile.GetHeader().kFlags & Resources::H3DFlag_PhysxMesh)
		{
			Resources::ConvexMeshCluster ConvexMeshCluster;
			if (H3DFile.Load(BStream, ConvexMeshCluster))
			{
				GeometryShapeFactory::Instance()->AddConvexMesh(ConvexMeshCluster);

				for (const Resources::ConvexMeshCluster::ConvexMesh& CMesh : ConvexMeshCluster.Meshes)
				{
					pMeshRef->ConvexMeshChecksums.push_back(CMesh.uCRC32);
				}
			}
		}

		VertexDesc.uSize = VertexDesc.SubresourceData.uRowPitch;
		IndexDesc.uSize = IndexDesc.SubresourceData.uRowPitch;

		bool bFailed = true;

		//Create vertex buffer
		if (VertexDesc.uSize > 0)
		{
			pMeshRef->m_pVertexBuffer = new GPUBufferDX11("VertexBuffer_" + Resources::FileSystem::GetFileNameWithoutExtension(_Desc.m_sFilePathOrName));
			bFailed = pMeshRef->m_pVertexBuffer->Initialize(VertexDesc) == false;
		}

		//Create index buffer
		if (bFailed == false && IndexDesc.uSize  > 0)
		{
			pMeshRef->m_pIndexBuffer = new GPUBufferDX11("IndexBuffer_" + Resources::FileSystem::GetFileNameWithoutExtension(_Desc.m_sFilePathOrName));
			bFailed = pMeshRef->m_pIndexBuffer->Initialize(IndexDesc) == false;
		}

		if (bFailed)
		{
			HERROR("Failed to create vertex/index buffer");
			HSAFE_DELETE(pMeshRef);
			return nullptr;
		}

		pMeshRef->m_SubMeshes = SubMeshes;

		return pMeshRef;
		// BStream (Buffer) will be released after this point:
	}	

	return nullptr;
}
//---------------------------------------------------------------------------------------------------