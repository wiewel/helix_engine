//   Copyright 2020 Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef HELIXDEFERREDSKYBOXRENDERPASSDX11_H
#define HELIXDEFERREDSKYBOXRENDERPASSDX11_H

#include "RenderPassDX11.h"
#include "CommonConstantBufferFunctions.h"
#include "Display\Shaders\DeferredSkybox.hlsl"

namespace Helix
{
	namespace Display
	{
		class HelixDeferredSkyboxRenderPassDX11 : public RenderPassDX11
		{
			using TCBDeferredSkybox = ShaderVariableDX11<Shaders::DeferredSkybox::cbDeferredSkybox>;
		public:
			HDEBUGNAME("HelixDeferredSkyboxRenderPassDX11");

			HelixDeferredSkyboxRenderPassDX11(DefaultInitializerType);
			HelixDeferredSkyboxRenderPassDX11(const std::string& _sInstanceName = {});
			virtual ~HelixDeferredSkyboxRenderPassDX11();

			bool OnInitalize(const hlx::TextToken& _CustomVariables) final;
			bool OnPerFrame() final;
			bool OnPerCamera(const Scene::CameraComponent& _Camera) final;

			void UseLenseflare(bool _bEnable);
			std::shared_ptr<RenderPassDX11> CreatePass(const std::string& _sInstanceName) const final;

		private:

			ShaderTextureDX11 m_DepthTex = { "gDepthTex", "LINEAR_DEPTH" };
			ShaderTextureDX11 m_Skybox = { "gSkybox" };
			ShaderTextureDX11 m_RandomTexture = { "gRandomTexture" };

			ShaderRenderTargetDX11 m_AlbedoTarget = { "vAlbedo" };

			//TCBPerFrame m_CBPerFrame = { "cbPerFrame" };
			TCBPerCamera m_CBPerCamera = { "cbPerCamera" };
			TCBDeferredSkybox m_CBDeferredSkybox = { "cbDeferredSkybox" };

		private:
			bool m_bUseLenseflare = true;
		};

		inline void HelixDeferredSkyboxRenderPassDX11::UseLenseflare(bool _bEnable) { m_bUseLenseflare = _bEnable; }
	} // Display
} // Helix

#endif // HELIXDEFERREDSKYBOXRENDERPASSDX11_H