//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef TEXTUREDX11_H
#define TEXTUREDX11_H

#include "Display\ViewDefines.h"
#include "TexturePoolDX11.h"

// forward declarations
namespace hlx
{
	class bytes;
}

struct ID3D11Resource;
struct ID3D11Texture1D;
struct ID3D11Texture2D;
struct ID3D11Texture3D;

struct ID3D11RenderTargetView;
struct ID3D11DepthStencilView;
struct ID3D11ShaderResourceView;
struct ID3D11UnorderedAccessView;

namespace Helix
{
	namespace Display
	{
		struct TextureReferenceDX11
		{
			TextureReferenceDX11(const TextureDesc& _Desc, const uint64_t& _uKey) :
				m_Description(_Desc), m_uKey(_uKey){}

			~TextureReferenceDX11();

			void Reset();

		public:
			const uint64_t m_uKey;
			TextureDesc m_Description = {};
			TextureResizeProperties m_ResizeProperties = {};

			// RenderTarget tex
			bool m_bIsRWTexture = false;
			Viewport m_Viewport = {};
			Math::float4 m_vClearColor = {}; // { 0.1f, 0.3f, 0.6f, 1.0f }

			//DepthStencil
			float m_fClearDepth = 1.f;
			uint8_t m_uClearStencil = 0;

			union
			{
				ID3D11Resource* m_pResource = nullptr;
				ID3D11Texture1D* m_pTexture1D;
				ID3D11Texture2D* m_pTexture2D;
				ID3D11Texture3D* m_pTexture3D;
			};

			ID3D11RenderTargetView* m_pRenderTargetView = nullptr;
			ID3D11DepthStencilView* m_pDepthStencilView = nullptr;
			ID3D11DepthStencilView* m_pDepthStencilViewReadOnly = nullptr;
			ID3D11ShaderResourceView* m_pResourceView = nullptr;
			ID3D11UnorderedAccessView* m_pUnorderedAccessView = nullptr;
		};

		enum TextureType
		{
			TextureType_DefaultTexture = 0,
			TextureType_ImmutableTexture,
			TextureType_BackBufferTexture,
			TextureType_DepthStencilTexture,
			TextureType_RenderTargetTexture,
			TextureType_Unknown
		};

		class TextureDX11 : public TDX11TextureRef
		{
			friend class TexturePoolDX11;
			friend class RenderTargetTextureDX11;
			friend class DepthStencilTextureDX11;
			friend class BackBufferTextureDX11;
			friend class ImmutableTextureDX11;
			friend class TextureCubeDX11;
			friend class DefaultTextureDX11;
			friend class TextureResizeEventManagerDX11;
			friend class RenderDeviceDrawDX11;
			friend class RenderDeviceDX11;
			friend class ShaderRenderTargetDX11;

		public:
			HDEBUGNAME("TextureDX11");			
 
			// default constructor
			inline constexpr TextureDX11() noexcept : TDX11TextureRef(nullptr), m_sDbgName() {}
			inline constexpr TextureDX11(std::nullptr_t) noexcept : TDX11TextureRef(nullptr),m_sDbgName() {}
			inline constexpr TextureDX11(const DefaultInitializerType&) : TDX11TextureRef(DefaultInit), m_sDbgName(){}

			TextureDX11(const TextureDesc& _Desc, const std::string& _sDbgName = {});

			// destructor
			virtual ~TextureDX11();
			
			// Increments the ref count, does not copy the pixel data!
			TextureDX11& operator=(const TextureDX11& _Other);
			// Increments the ref count, does not copy the pixel data!
			TextureDX11(const TextureDX11& _Texture);

			TextureDX11& operator=(std::nullptr_t);
			
			void SetDebugName(const std::string& _sDebugName);

			void SetResizeEvent(const std::string& _sEventName, const float& _fWidthScale = 1.f, const float& _fHeightScale = 1.f, const float& _fDepthScale = 1.f);
			//Todo: Lock texture
			bool Resize(uint32_t _uWidth, uint32_t _uHeight = 0, uint32_t _uDepth = 0);

			// This function returns the type corresponding to the texture variants
			const TextureType GetTextureType() const;

			const TextureDesc& GetDescription() const;

			const TextureResizeProperties& GetResizeProperties() const;

		private:
			ID3D11RenderTargetView* GetRenderTargetView() const;
			ID3D11DepthStencilView* GetDepthStencilView(bool _bReadOnly = false) const;
			ID3D11ShaderResourceView* GetShaderResourceView() const;
			ID3D11UnorderedAccessView* GetUnorderedAccessView() const;

			bool ResizeEvent(uint32_t _uWidth, uint32_t _uHeight = 0, uint32_t _uDepth = 0);

			// Check TexDesc
			static void CheckTextureDescription(const TextureDesc& _TextureDesc);

			//Create texture
			static bool Initialize(_Out_ TextureReferenceDX11& _TexRef, const TextureDesc& _TextureDesc, const BufferSubresourceData& _InitialData);

			//Load from file
			static bool Initialize(_Out_ TextureReferenceDX11& _TexRef, const TextureDesc& _TextureDesc, const hlx::bytes& _Data);

			static bool Initialize(_Out_ TextureReferenceDX11& _TexRef, const hlx::bytes& _Data);

			static bool Initialize(_Out_ TextureReferenceDX11& _TexRef, const hlx::string& _sFilePath);

			// Creates RTV, SRV, DSV
			static bool CreateViews(_Out_ TextureReferenceDX11& _TexRef);

			// called after Resize() override this to resize the viewport
			virtual void OnResize(uint32_t _uWidth, uint32_t _uHeight, uint32_t _uDepth) {};

		private:
			std::string m_sDbgName;
		};

		inline const TextureDesc& TextureDX11::GetDescription() const { return m_pReference->m_Description; }
		inline const TextureResizeProperties& TextureDX11::GetResizeProperties() const	{ return m_pReference->m_ResizeProperties; }
		inline ID3D11RenderTargetView* TextureDX11::GetRenderTargetView() const { return m_pReference->m_pRenderTargetView; }
		inline ID3D11DepthStencilView* TextureDX11::GetDepthStencilView(bool _bReadOnly) const { return (_bReadOnly ? m_pReference->m_pDepthStencilViewReadOnly : m_pReference->m_pDepthStencilView); }
		inline ID3D11ShaderResourceView* TextureDX11::GetShaderResourceView() const { return m_pReference->m_pResourceView; }
		inline ID3D11UnorderedAccessView* TextureDX11::GetUnorderedAccessView() const {	return m_pReference->m_pUnorderedAccessView;}
	}
}

#endif //TEXTUREDX11_H