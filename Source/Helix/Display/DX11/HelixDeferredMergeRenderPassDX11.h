//   Copyright 2020 Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef HELIXDEFERREDMERGERENDERPASSDX11_H
#define HELIXDEFERREDMERGERENDERPASSDX11_H

#include "RenderPassDX11.h"

#include "CommonConstantBufferFunctions.h"

namespace Helix
{
	namespace Display
	{
		class HelixDeferredMergeRenderPassDX11 : public RenderPassDX11
		{
		public:
			HDEBUGNAME("HelixDeferredMergeRenderPassDX11");

			HelixDeferredMergeRenderPassDX11(DefaultInitializerType);
			HelixDeferredMergeRenderPassDX11(const std::string& _sInstanceName = {});
			virtual ~HelixDeferredMergeRenderPassDX11();

			bool OnInitalize(const hlx::TextToken& _CustomVariables) final;

			bool OnPerFrame() final;
			bool OnPerCamera(const Scene::CameraComponent& _Camera) final;

			void EnableSSAO(const bool& _bEnableSSAO);
			std::shared_ptr<RenderPassDX11> CreatePass(const std::string& _sInstanceName) const final;

		private:
			// From Solid Pass
			ShaderTextureDX11 m_DiffuseColorTexture = { "gAlbedoMap", "MTL_ALBEDO"};

			// From Lighting Pass
			ShaderTextureDX11 m_DiffuseLightMap = { "gDiffuseLightMap", "DIFFUSE_LIGHTMAP"};
			ShaderTextureDX11 m_SpecularLightMap = { "gSpecularLightMap" , "SPECULAR_LIGHTMAP" };
			ShaderTextureDX11 m_EmissiveLightMap = { "gEmissiveLightMap" , "EMISSIVE_LIGHTMAP" };

			// From SSAO Pass
			ShaderTextureDX11 m_SSAOMap = { "gSSAOMap" };
			//ShaderTextureDX11 m_BentNormalMap = { "gBentNormalMap", "SSAO_BENT_NORMAL_MAP" };

			//// Additional Input
			//ShaderTextureDX11 m_EnvMap = { "gEnvMap" };

			// Rendertargets
			ShaderRenderTargetDX11 m_MergeOutputTarget = { "vMergeOutput", "LIT_SCENE" };
			
			TCBPerCamera m_CBPerCamera = { "cbPerCamera" };

			bool m_bSSAO = true;
		};

		inline void HelixDeferredMergeRenderPassDX11::EnableSSAO(const bool& _bEnableSSAO) { m_bSSAO = _bEnableSSAO; }

	} // Display
} // Helix

#endif // HELIXDEFERREDMERGERENDERPASSDX11_H