//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "TexturePoolDX11.h"
#include "TextureDX11.h"
#include "Resources\FileSystem.h"

using namespace Helix::Display;

TextureDX11 TexturePoolDX11::m_ErrorTexture = nullptr;

//---------------------------------------------------------------------------------------------------
TexturePoolDX11::TexturePoolDX11()
{
}
//---------------------------------------------------------------------------------------------------
TexturePoolDX11::~TexturePoolDX11()
{
}
//---------------------------------------------------------------------------------------------------
void TexturePoolDX11::Initialize(const std::string& _sTextureDirectory)
{
	m_sTextureDirectory = _sTextureDirectory;
}
//---------------------------------------------------------------------------------------------------
uint64_t TexturePoolDX11::HashFunction(const TextureDesc& _Desc)
{
	HASSERTD(_Desc.m_sFilePathOrName.empty() == false, "Invalid empty file path or name");

	uint64_t uHash = DefaultHashFunction(reinterpret_cast<const uint8_t*>(&_Desc), offsetof(TextureDesc, m_sFilePathOrName));

	uHash ^= StrHash(Resources::FileSystem::GetShortName(STR(_Desc.m_sFilePathOrName)));

	// make this texture unique even if the description is equal
	if (_Desc.m_bPooled == false)
	{
		uHash ^= m_ID.NextID();
	}

	//_Desc.m_bPooled is ignored in the checksum

	return uHash;
}
//---------------------------------------------------------------------------------------------------
TextureReferenceDX11* TexturePoolDX11::CreateFromDesc(const TextureDesc& _Desc, const uint64_t& _Hash)
{
	TextureReferenceDX11* pTextureRef = new TextureReferenceDX11(_Desc, _Hash);

	bool bInitialized = true;
	// Create new empty texture
	if (_Desc.m_bLoadFromFile == false)
	{
		bInitialized = _Desc.m_bNoInit || TextureDX11::Initialize(*pTextureRef, _Desc, _Desc.m_InitialData);
	}
	else
	{
		hlx::fbytestream FileStream;
		if (Resources::FileSystem::Instance()->OpenFileStream(Resources::HelixDirectories_Textures, hlx::to_string(_Desc.m_sFilePathOrName), std::ios_base::in | std::ios_base::binary, FileStream) == false)
		{
			bInitialized = false;
		}
		else
		{
			hlx::bytes&& Data = FileStream.get<hlx::bytes>(FileStream.size());
			FileStream.close();
			bInitialized = TextureDX11::Initialize(*pTextureRef, _Desc, Data);
		}
	}

	if (bInitialized == false)
	{
		HSAFE_DELETE(pTextureRef);
	}

	return pTextureRef;
}
//---------------------------------------------------------------------------------------------------

//void TexturePoolDX11::OnShutdown()
//{
//	//Clear(false);
//}
//---------------------------------------------------------------------------------------------------
uint64_t TexturePoolDX11::GetDefaultKey()
{
	if (m_ErrorTexture == nullptr)
	{
		TextureDesc Desc = {};
		Desc.m_bLoadFromFile = false;
		Desc.m_bNoInit = false;
		Desc.m_sFilePathOrName = "HELiXERRORTEXTURE";
		Desc.m_kBindFlag = ResourceBindFlag_ShaderResource;
		Desc.m_kCPUAccessFlag = CPUAccessFlag_None;
		Desc.m_kFormat = PixelFormat_B8G8R8A8_UNorm;
		Desc.m_kResourceFlag = MiscResourceFlag_None;
		Desc.m_kResourceDimension = Helix::Display::ResourceDimension_Texture2D;
		Desc.m_kUsageFlag = ResourceUsageFlag_Immutable;
		Desc.m_uArraySize = 1u;
		Desc.m_uHeight = 256u;
		Desc.m_uWidth = 256u;
		Desc.m_uMipLevels = 1u;
		Desc.m_uSampleCount = 1u;

		Math::B8G8R8A8_UNorm* pData = new Math::B8G8R8A8_UNorm[256*256];

		Desc.m_InitialData.pData = pData;
		Desc.m_InitialData.uRowPitch =  256 * sizeof(Math::B8G8R8A8_UNorm);
		Desc.m_InitialData.uDepthPitch = 256 * 256 * sizeof(Math::B8G8R8A8_UNorm);

		memset(pData, 0, Desc.m_InitialData.uDepthPitch);

		//for (uint32_t x = 0; x < 256; ++x)
		//{
		//	for (uint32_t y = 0; y < 256; ++y)
		//	{
		//		pData[x*256 + y] = Math::B8G8R8A8_UNorm(0.f, 0.f, 0.f, 0.f); // some strange gradient
		//	}
		//}

		m_ErrorTexture = TextureDX11(Desc, "HELiXERRORTEXTURE");

		delete[] pData;
	}

	return m_ErrorTexture.GetKey();
}
//---------------------------------------------------------------------------------------------------