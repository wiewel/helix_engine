//   Copyright 2020 Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "HelixDeferredLightingRenderPassDX11.h"
#include "Scene\LightContext.h"

using namespace Helix;
using namespace Helix::Display;
using namespace Helix::Scene;
using namespace Helix::Datastructures;
using namespace Helix::Math;
using namespace Helix::Shaders::ConstantBuffers;
using namespace Helix::Shaders::DeferredLighting;

//---------------------------------------------------------------------------------------------------
HelixDeferredLightingRenderPassDX11::HelixDeferredLightingRenderPassDX11(const std::string& _sInstanceName) :
	RenderPassDX11("DeferredLighting", _sInstanceName,
	{ &m_DepthTex, &m_PointShadowMap, &m_SpotShadowMap, &m_NormalWSMap, &m_MetallicMap, &m_RoughnessMap, &m_AlbedoMap, &m_ShadowMap, &m_EnvMap/*, &m_BentNormalMap*/ }, // Textures
	{ &m_DiffuseTarget, &m_SpecularTarget/*, &m_NormalDebug*/ }, //RenderTargets
	{ &m_CBDirectionalLights, &m_CBPointLights, &m_CBSpotLights/*, &m_CBPerFrame */, &m_CBPerCamera, &m_CBDeferredLighting, &m_CBPointLightShadows, &m_CBSpotLightShadows })  // Variables
{
}
//---------------------------------------------------------------------------------------------------
HelixDeferredLightingRenderPassDX11::HelixDeferredLightingRenderPassDX11(DefaultInitializerType) : RenderPassDX11(DefaultInit, "DeferredLighting")
{
}
//---------------------------------------------------------------------------------------------------
HelixDeferredLightingRenderPassDX11::~HelixDeferredLightingRenderPassDX11()
{
}
//---------------------------------------------------------------------------------------------------
bool HelixDeferredLightingRenderPassDX11::OnInitalize(const hlx::TextToken& _CustomVariables)
{
	// Add RasterizerState
	RasterizerDesc RasDesc = {};
	RasDesc.m_kFillMode = FillMode_Solid;
	RasDesc.m_kCullMode = CullMode_None;
	RasDesc.m_bFrontCounterClockwise = false;

	SetRasterizerState(RasDesc);

	// Add BlendState
	RenderTargetBlendDesc RTBlendDesc = {};
	RTBlendDesc.m_bBlendEnable = false;
	RTBlendDesc.m_kSrcBlend = BlendVariable_Src_Alpha;
	RTBlendDesc.m_kDestBlend = BlendVariable_One;
	RTBlendDesc.m_kBlendOp = BlendOperation_Add;
	RTBlendDesc.m_kSrcBlendAlpha = BlendVariable_Zero;
	RTBlendDesc.m_kDestBlendAlpha = BlendVariable_Zero;
	RTBlendDesc.m_kBlendOpAlpha = BlendOperation_Add;
	RTBlendDesc.m_uRenderTargetWriteMask = Channel_All;

	BlendDesc AlphaBlendDesc = {};
	AlphaBlendDesc.m_bAlphaToCoverageEnable = false;
	AlphaBlendDesc.m_RenderTargets[0] = RTBlendDesc;
	AlphaBlendDesc.m_uSampleMask = 0xffffffff;
	AlphaBlendDesc.fBlendFactor = { 0.0f,0.0f,0.0f,0.0f };

	SetBlendState(AlphaBlendDesc);

	// Add DepthStencilState
	DepthStencilDesc DepthDesc = {};
	DepthDesc.m_bDepthEnable = true;
	DepthDesc.m_kDepthWriteMask = DepthWriteMask_Zero;
	DepthDesc.m_kDepthFunc = ComparisonFunction_Greater;
	DepthDesc.m_bStencilEnable = false;

	SetDepthStencilState(DepthDesc, "DeferredLighting");

	// Default Parameter
	ClearRenderTargets(true);

	return m_bInitialized;
}

//---------------------------------------------------------------------------------------------------
bool HelixDeferredLightingRenderPassDX11::OnPerCamera(const CameraComponent& _Camera)
{
	SetCameraConstants(m_CBPerCamera, _Camera.GetRenderingProperties());

	/// Set Lights
	std::vector<uint8_t> RangeIndices = { 0,0,0 };
	GetShader()->SetIOPermutation(0u);

	LightContext* pLightContext = LightContext::Instance();

	std::vector<LightDesc> DirectionalLights;
	std::vector<LightDesc> PointLights;
	std::vector<LightDesc> SpotLights;
	pLightContext->GetLights(DirectionalLights, PointLights, SpotLights);

	// Apply per frame data
	//m_CBPerFrame->vGlobalAmbient = pLightContext->GetAmbientColor();
	m_CBDeferredLighting->mShadowTransform = pLightContext->GetShadowTransform();

	///// Paraboloid Shadow Mapping
	//LightContext::ShadowCaster ShadowCaster = pLightContext->GetShadowCaster();
	//m_CBDeferredLighting->mLightView = ShadowCaster.mLightView;
	//m_CBDeferredLighting->fLightNearDist = ShadowCaster.fLightCameraNearDist;
	//m_CBDeferredLighting->fLightFarDist = ShadowCaster.fLightCameraFarDist;

	// Directional Lights
	for (uint32_t i = 0; i < GetArrayLength(m_CBDirectionalLights->Array); ++i)
	{
		cbDirectionalLightTYPE& LightOut = m_CBDirectionalLights->Array[i];
		LightOut.vColor = float3(0, 0, 0);
		LightOut.vDirection = float3(0, 0, 0);
	}

	uint32_t uDirLightCount = static_cast<uint32_t>(std::min(DirectionalLights.size(), GetArrayLength(m_CBDirectionalLights->Array)));
	for (uint32_t i = 0; i < uDirLightCount; ++i)
	{
		cbDirectionalLightTYPE& LightOut = m_CBDirectionalLights->Array[i];
		const LightDesc& LightIn = DirectionalLights.at(i);
		LightOut.vColor = LightIn.vColor;
		LightOut.vDirection = LightIn.vDirection;
	}
	m_CBDirectionalLights->m_uCurrentArraySize = uDirLightCount;

	// Point Lights
	for (uint32_t i = 0; i < GetArrayLength(m_CBPointLights->Array); ++i)
	{
		cbPointLightTYPE& LightOut = m_CBPointLights->Array[i];
		LightOut.vColor = float3(0, 0, 0);
		LightOut.vPosition = float3(0, 0, 0);
		LightOut.fDecayStart = 0.f;
		LightOut.fRange = 0.f;
		LightOut.iShadowIndex = -1;
	}

	uint32_t uPointLightShadow = 0u;
	uint32_t uPointLightCount = static_cast<uint32_t>(std::min(PointLights.size(), GetArrayLength(m_CBPointLights->Array)));
	for (uint32_t i = 0; i < uPointLightCount; ++i)
	{
		cbPointLightTYPE& LightOut = m_CBPointLights->Array[i];
		const LightDesc& LightIn = PointLights.at(i);
		LightOut.vColor = LightIn.vColor;
		LightOut.vPosition = LightIn.vPosition;
		LightOut.fDecayStart = LightIn.fDecayStart;
		LightOut.fRange = LightIn.fRange;

		if(LightIn.bCastShadows && uPointLightShadow < MAXPOINTLIGHTSHADOWS)
		{
			LightOut.iShadowIndex = static_cast<int32_t>(uPointLightShadow);
			cbPointLightShadowTYPE& ShadowOut = m_CBPointLightShadows->Array[uPointLightShadow];

			++uPointLightShadow;

			ShadowOut.mShadowTransformation = LightIn.ShadowProps.mShadowTransform;
			ShadowOut.fLightFarDist = LightIn.ShadowProps.fLightCameraFarDist;
			ShadowOut.fLightNearDist = LightIn.ShadowProps.fLightCameraNearDist;
			ShadowOut.vShadowMapStartTexCoord = LightIn.ShadowProps.vShadowMapStartTexCoord;
			ShadowOut.vShadowMapEndTexCoord = LightIn.ShadowProps.vShadowMapEndTexCoord;
		}
		else
		{
			LightOut.iShadowIndex = -1;
		}
	}
	m_CBPointLights->m_uCurrentArraySize = uPointLightCount;


	// Spot Lights
	for (uint32_t i = 0; i < GetArrayLength(m_CBSpotLights->Array); ++i)
	{
		cbSpotLightTYPE& LightOut = m_CBSpotLights->Array[i];
		LightOut.vColor = float3(0, 0, 0);
		LightOut.vDirection = float3(0, 0, 0);
		LightOut.vPosition = float3(0, 0, 0);
		LightOut.fDecayStart = 0.f;
		LightOut.fRange = 0.f;
		LightOut.fSpotAngle = 0.f;
		LightOut.iShadowIndex = -1;
	}

	int32_t iSpotLightShadow = 0;
	uint32_t uSpotLightCount = static_cast<uint32_t>(std::min(SpotLights.size(), GetArrayLength(m_CBSpotLights->Array)));
	for (uint32_t i = 0; i < uSpotLightCount; ++i)
	{
		cbSpotLightTYPE& LightOut = m_CBSpotLights->Array[i];
		const LightDesc& LightIn = SpotLights.at(i);
		LightOut.vColor = LightIn.vColor;
		LightOut.vDirection = LightIn.vDirection;
		LightOut.vPosition = LightIn.vPosition;
		LightOut.fDecayStart = LightIn.fDecayStart;
		LightOut.fRange = LightIn.fRange;
		LightOut.fSpotAngle = LightIn.fSpotAngle;

		if (LightIn.bCastShadows && iSpotLightShadow < MAXSPOTLIGHTSHADOWS)
		{
			LightOut.iShadowIndex = iSpotLightShadow;
			cbSpotLightShadowTYPE& ShadowOut = m_CBSpotLightShadows->Array[iSpotLightShadow];

			++iSpotLightShadow;

			ShadowOut.mShadowTransformation = LightIn.ShadowProps.mShadowTransform;
			ShadowOut.fLightFarDist = LightIn.ShadowProps.fLightCameraFarDist;
			ShadowOut.fLightNearDist = LightIn.ShadowProps.fLightCameraNearDist;
			ShadowOut.vShadowMapStartTexCoord = LightIn.ShadowProps.vShadowMapStartTexCoord;
			ShadowOut.vShadowMapEndTexCoord = LightIn.ShadowProps.vShadowMapEndTexCoord;
		}
		else
		{
			LightOut.iShadowIndex = -1;
		}
	}
	m_CBSpotLights->m_uCurrentArraySize = uSpotLightCount;

	uint8_t uMaxDirIndex = static_cast<uint8_t>(ceil(uDirLightCount / static_cast<float>(DirLightRange::uStep)));
	RangeIndices[0] = std::min(uMaxDirIndex, static_cast<uint8_t>(DirLightRange::uCount - 1u));

	uint8_t uMaxPointIndex = static_cast<uint8_t>(ceil(uPointLightCount / static_cast<float>(PointLightRange::uStep)));
	RangeIndices[1] = std::min(uMaxPointIndex, static_cast<uint8_t>(PointLightRange::uCount - 1u));

	uint8_t uMaxSpotIndex = static_cast<uint8_t>(ceil(uSpotLightCount / static_cast<float>(SpotLightRange::uStep)));
	RangeIndices[2] = std::min(uMaxSpotIndex, static_cast<uint8_t>(SpotLightRange::uCount - 1u));

	uint32_t uClassPermutation = 0u;

	// shadow mapping
	if(m_ShadowMap.GetTexture())
	{
		uClassPermutation |= Shaders::DeferredLighting::HPMPS_ShadowMap;
	}

	// env mapping
	const TextureCubeDX11& EnvMap = LightContext::Instance()->GetEnvironmentMap();
	if (EnvMap != nullptr)
	{
		m_EnvMap.SetTexture(EnvMap);
		uClassPermutation |= Shaders::DeferredLighting::HPMPS_EnvironmentMapping;
	}

	Select(ShaderType_PixelShader, uClassPermutation, RangeIndices);

	return true;
}
//---------------------------------------------------------------------------------------------------

std::string HelixDeferredLightingRenderPassDX11::GetPermutationName(const ShaderPermutationHash& _Perm) const
{
	std::string sName = m_sTypeName + ' ';

	std::string sPerm;

	if (_Perm.uClassPermutation & Shaders::DeferredLighting::HPMPS_ShadowMap)
	{
		sPerm += "ShadowMap ";
	}
	//if (_Perm.uClassPermutation & Shaders::DeferredLighting::HPMPS_BentNormal)
	//{
	//	sPerm += "BentNormal ";
	//}
	if (_Perm.uClassPermutation & Shaders::DeferredLighting::HPMPS_EnvironmentMapping)
	{
		sPerm += "EnvMap ";
	}

	for (const uint8_t& r : _Perm.Ranges)
	{
		sPerm += std::to_string(r) + ' ';
	}

	// haha sperm xD
	return sName + (sPerm.empty() ? "NULL" : sPerm);
}
//---------------------------------------------------------------------------------------------------

std::shared_ptr<RenderPassDX11> HelixDeferredLightingRenderPassDX11::CreatePass(const std::string& _sInstanceName) const
{
	return std::make_shared<HelixDeferredLightingRenderPassDX11>(_sInstanceName);
}

//---------------------------------------------------------------------------------------------------
