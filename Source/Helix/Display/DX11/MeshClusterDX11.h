//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef MESHCLUSTERDX11_H
#define MESHCLUSTERDX11_H

#include "Display\ViewDefines.h"
#include "hlx\src\StandardDefines.h"
#include "GPUBufferDX11.h"
#include "MeshPoolDX11.h"

namespace Helix
{
	namespace Display
	{
		// forward decls:
		class GPUBufferDX11;

		struct MeshClusterReferenceDX11
		{
			MeshClusterReferenceDX11() {}
			~MeshClusterReferenceDX11();

			GPUBufferDX11* m_pVertexBuffer = nullptr;
			GPUBufferDX11* m_pIndexBuffer = nullptr;

			std::string sClusterName;
			TMeshCluster m_SubMeshes;
			std::vector<uint32_t> ConvexMeshChecksums;
			//std::string sFilePath;
		};

		class MeshClusterDX11 : public TDX11MeshClusterRef
		{
			friend class RenderDeviceDX11;
		public:
			HDEBUGNAME("MeshClusterDX11");

			// Default constructor
			inline /*constexpr*/ MeshClusterDX11() noexcept : TDX11MeshClusterRef(nullptr) {};
			inline /*constexpr*/ MeshClusterDX11(std::nullptr_t) noexcept : TDX11MeshClusterRef(nullptr) {};
			MeshClusterDX11(const DefaultInitializerType&);

			//Loads complete cluster from file
			MeshClusterDX11(const std::string& _sFilePathOrName, bool _bLoadFromFile = true, bool _bWriteable = false, bool _bPooled = true);

			//Load one sub mesh from file, complete cluster is in memory
			MeshClusterDX11(const std::string& _sFilePath, const std::string& _sSubMeshName, bool _bWriteable = false, bool _bPooled = true);

			//Load a set of sub meshs from file, complete cluster is in memory
			MeshClusterDX11(const std::string& _sFilePath, const std::vector<std::string>& _SubMeshNames, bool _bWriteable = false, bool _bPooled = true);

			//create empty mesh cluster
			MeshClusterDX11(const std::string& _sObjectName, const TMeshCluster& _MeshCluster, bool _bWriteable = true, bool _bPooled = true);

			//create pre filled mesh cluster
			MeshClusterDX11(const MeshClusterDesc& _Desc);

			// Copy constructor
			MeshClusterDX11(const MeshClusterDX11& _Other);

			// cleanup
			~MeshClusterDX11();

			// Assignment operator
			MeshClusterDX11& MeshClusterDX11::operator=(const MeshClusterDX11& _Other);
			MeshClusterDX11& MeshClusterDX11::operator=(std::nullptr_t);

			// VertexStride should conform with the vertex declaration of the mesh
			template<typename VertexStride> // write/update (wirte_no_overwrite) vertex buffer of a specific submesh; returns true if data was written successfully
			bool WriteVertexData(const std::string& _sSubMeshName, const std::vector<VertexStride>& _VertexData);

			// IndexElement should be uint16_t or  uint32_t
			template<typename IndexElement> // write/update (wirte_no_overwrite) index buffer of a specific submesh; returns true if data was written successfully
			bool WriteIndexData(const std::string& _sSubMeshName, const std::vector<IndexElement>& _IndexData);

			////returns parameters for rendering in one call
			//bool IsContinuous(_Out_ SubMesh& _ContinuousMeshInfo) const;
			//bool IsContinuous() const;

			//const uint32_t GetPolyCount() const;
			//const uint32_t& GetVertexCount() const;
			//const uint32_t& GetIndexCount() const;

			// returns empty string if reference is invalid
			std::string GetClusterName() const;

			const TMeshCluster& GetSubMeshes() const;

			//Visible to RenderDeviceDX11
			const GPUBufferDX11* GetVertexBuffer() const;
			const GPUBufferDX11* GetIndexBuffer() const;

			void AddSubMesh(const SubMesh& _Mesh);
			void AddSubMesh(const std::string& _SubMeshName);
			void InsertSubMesh(const SubMesh& _Mesh, uint32_t _iOffset);
			void RemoveSubMesh(const std::string& _SubName);
			void ClearSubMeshes();

		private:

			// checks if the index and vertex data layout is continuous over the whole mesh data
			//void CheckContinuity();

		private:
			//bool m_bIsContinuous = false;
			//SubMesh m_ContinuousMeshInfo = {};

			TMeshCluster m_SubMeshes;
		};

		inline const GPUBufferDX11* MeshClusterDX11::GetVertexBuffer() const {	return m_pReference != nullptr ? m_pReference->m_pVertexBuffer : nullptr;	}
		inline const GPUBufferDX11* MeshClusterDX11::GetIndexBuffer() const { return m_pReference != nullptr ? m_pReference->m_pIndexBuffer : nullptr; }
		inline std::string MeshClusterDX11::GetClusterName() const	{return m_pReference == nullptr ? std::string() : m_pReference->sClusterName;}
		inline const TMeshCluster& MeshClusterDX11::GetSubMeshes() const {return m_SubMeshes;}

		//inline bool MeshClusterDX11::IsContinuous() const { return m_bIsContinuous; }

		//// TODO: check other primitive topologies
		//inline const uint32_t MeshClusterDX11::GetPolyCount() const { return m_ContinuousMeshInfo.m_uIndexCount > 0u ? m_ContinuousMeshInfo.m_uIndexCount / 3 : m_ContinuousMeshInfo.m_uVertexCount / 3;}
		//inline const uint32_t& MeshClusterDX11::GetVertexCount() const { return m_ContinuousMeshInfo.m_uVertexCount;}
		//inline const uint32_t& MeshClusterDX11::GetIndexCount() const {return m_ContinuousMeshInfo.m_uIndexCount;}

		//inline bool MeshClusterDX11::IsContinuous(SubMesh& _ContinuousMeshInfo) const
		//{
		//	if (m_bIsContinuous)
		//	{
		//		_ContinuousMeshInfo = m_ContinuousMeshInfo;
		//		return true;
		//	}
		//	
		//	return false;
		//}
		
		template<typename VertexStride>
		inline bool MeshClusterDX11::WriteVertexData(const std::string& _sSubMeshName, const std::vector<VertexStride>& _VertexData)
		{
			HASSERT(m_pReference != nullptr && m_pReference->m_pVertexBuffer != nullptr && _VertexData.empty() == false, "Invalid VertexBuffer data");

			for (const SubMesh& Mesh : m_SubMeshes)
			{
				if (Mesh.m_sSubName == _sSubMeshName &&
					Mesh.m_uVertexStrideSize == sizeof(VertexStride) &&
					Mesh.m_uVertexCount >= _VertexData.size())
				{
					return m_pReference->m_pVertexBuffer->Apply(reinterpret_cast<const uint8_t*>(_VertexData.data()), static_cast<uint32_t>(_VertexData.size()) * Mesh.m_uVertexStrideSize, Mesh.m_uVertexByteOffset);
				}
			}

			return false;
		}

		template<typename IndexElement>
		inline bool MeshClusterDX11::WriteIndexData(const std::string & _sSubMeshName, const std::vector<IndexElement>& _IndexData)
		{
			HASSERT(m_pReference != nullptr && m_pReference->m_pIndexBuffer != nullptr && _IndexData.empty() == false, "Invalid IndexBuffer data");

			for (const SubMesh& Mesh : m_SubMeshes)
			{
				if (Mesh.m_sSubName == _sSubMeshName &&
					Mesh.m_uIndexElementSize == sizeof(IndexElement) &&
					Mesh.m_uIndexCount >= _IndexData.size())
				{
					return m_pReference->m_pIndexBuffer->Apply(reinterpret_cast<const uint8_t*>(_IndexData.data()), static_cast<uint32_t>(_IndexData.size()) * Mesh.m_uIndexElementSize, Mesh.m_uIndexByteOffset);
				}
			}

			return false;
		}
	} // Display
} // Helix

#endif // MESHCLUSTERDX11_H