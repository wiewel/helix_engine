//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "DeferredRendererDX11.h"
#include "Scene\LightContext.h"
#include "Scene\RenderingScene.h"

#include "RenderPassCreatorDX11.h"
#include "RenderProcedureDX11.h"

#include "HelixSolidRenderPassDX11.h"
#include "HelixShadowMapRenderPassDX11.h"
#include "HelixDepthPrePassRenderPassDX11.h"
#include "HelixDeferredSkyboxRenderPassDX11.h"
#include "HelixParticleRenderPassDX11.h"
#include "HelixVerticalBlurRenderPassDX11.h"
#include "HelixHorizontalBlurRenderPassDX11.h"
#include "HelixSimpleDoFRenderPassDX11.h"
#include "HelixFullscreenTextureRenderPassDX11.h"
#include "HelixGlitchRenderPassDX11.h"
#include "HelixSSAORenderPassDX11.h"
#include "HelixParaboloidShadowRenderPassDX11.h"
#include "HelixProjectedShadowRenderPassDX11.h"
#include "HelixDeferredMergeRenderPassDX11.h"
#include "HelixDeferredLightingRenderPassDX11.h"
#include "HelixOceanRenderPassDX11.h"
#include "HelixDebugRenderPassDX11.h"
#include "HelixGizmoRenderPassDX11.h"

#include "HelixScaleRenderPassDX11.h"
#include "HelixLuminanceRenderPassDX11.h"
#include "HelixLuminanceAdaptionRenderPassDX11.h"
#include "HelixBloomRenderPassDX11.h"
#include "HelixHDRCompositeRenderPassDX11.h"

using namespace Helix;
using namespace Display;
using namespace Scene;

DeferredRendererDX11::DeferredRendererDX11()
{
}
//---------------------------------------------------------------------------------------------------
DeferredRendererDX11::~DeferredRendererDX11()
{
}
//---------------------------------------------------------------------------------------------------
bool DeferredRendererDX11::Initialize()
{
	InitializeTextures();

	InitializePasses();
	bool bInitialized = InitializeProcedures();

	return bInitialized;
}
//---------------------------------------------------------------------------------------------------
void DeferredRendererDX11::InitializeTextures()
{
	// Generate Random texture
	GenerateRandomTexture1D();
	GenerateRandomTexture2D();

	PixelFormat kLightMapFormat = m_bHDR ? PixelFormat_R16G16B16A16_Float : PixelFormat_R8G8B8A8_UNorm;

	m_pBackBuffer = BackBufferTextureDX11::Instance();
	m_pBackBuffer->SetDefaultClearColor(Math::float4(0, 0, 0, 0));
	uint32_t uWidth = m_pBackBuffer->GetDescription().m_uWidth;
	uint32_t uHeight = m_pBackBuffer->GetDescription().m_uHeight;

	if(m_bShadowMap)
	{
		uint32_t uShadowMapRes = std::max(uWidth * 2u, uHeight * 2u); // todo: add to options view and config
		m_ShadowMap = DepthStencilTextureDX11(uShadowMapRes, uShadowMapRes, "ShadowMap", PixelFormat_R32_Typeless);
		m_ShadowMap.SetDefaultClearDepth(1.f);
	}

	m_DepthBuffer = DepthStencilTextureDX11(uWidth, uHeight, "DepthStencil", PixelFormat_R32_Typeless); // PixelFormat_R32_Typeless // PixelFormat_R24G8_Typeless
	m_DepthBuffer.SetResizeEvent("BackBufferResize");
	m_DepthBuffer.SetDefaultClearDepth(1.f);


	m_GizmoDepthBuffer = DepthStencilTextureDX11(uWidth, uHeight, "GizmoDepth", PixelFormat_R32_Typeless);
	m_GizmoDepthBuffer.SetResizeEvent("BackBufferResize");
	m_GizmoDepthBuffer.SetDefaultClearDepth(1.f);

	const uint32_t uParaTexSize = 4096u;
	m_PointShadowDepth = DepthStencilTextureDX11(uParaTexSize, uParaTexSize, "ParaboloidPointShadowDepth", PixelFormat_R32_Typeless, true, 2u);
	m_PointShadowDepth.SetDefaultClearDepth(1.f);

	m_SpotShadowDepth = DepthStencilTextureDX11(uParaTexSize, uParaTexSize, "ProjectedSpotShadowDepth", PixelFormat_R32_Typeless, true);
	m_SpotShadowDepth.SetDefaultClearDepth(1.f);

	m_LinearDepthTarget = RenderTargetTextureDX11(uWidth, uHeight, "LinearDepthTarget", PixelFormat_R32_Float);
	m_LinearDepthTarget.SetResizeEvent("BackBufferResize");
	m_LinearDepthTarget.SetDefaultClearColor(Math::float4(1, 1, 1, 1));

	//m_ParaboloidShadowTarget = RenderTargetTextureDX11(uParaTexSize, uParaTexSize, "ParaboloidShadowTarget", PixelFormat_R32_Float, true, false, 2u);
	//m_ParaboloidShadowTarget.SetDefaultClearColor(Math::float4(0, 0, 0, 0));

	m_AlbedoTarget = RenderTargetTextureDX11(uWidth, uHeight, "AlbedoTarget");
	m_AlbedoTarget.SetResizeEvent("BackBufferResize");

	m_LitSceneHDR = RenderTargetTextureDX11(uWidth, uHeight, "LitSceneHDRTarget", PixelFormat_R16G16B16A16_Float);
	m_LitSceneHDR.SetResizeEvent("BackBufferResize");

	m_NormalWSTarget = RenderTargetTextureDX11(uWidth, uHeight, "NormalWSTarget", PixelFormat_R16G16_SNorm);
	m_NormalWSTarget.SetResizeEvent("BackBufferResize");
	m_NormalWSTarget.SetDefaultClearColor(Math::float4(0, 0, 0, 0));

	//m_NormalDebugTarget = RenderTargetTextureDX11(uWidth, uHeight, "NormalDebugTarget", PixelFormat_R16G16B16A16_Float);
	//m_NormalDebugTarget.SetResizeEvent("BackBufferResize");
	//m_NormalDebugTarget.SetDefaultClearColor(Math::float4(0, 0, 0, 0));

	m_MetallicTarget = RenderTargetTextureDX11(uWidth, uHeight, "MetallicHorizonTarget", PixelFormat_R8G8_UNorm); // PixelFormat_R8G8_UNorm // PixelFormat_R32_Float
	m_MetallicTarget.SetResizeEvent("BackBufferResize");
	m_MetallicTarget.SetDefaultClearColor(Math::float4(0, 0, 0, 0));

	m_RoughnessTarget = RenderTargetTextureDX11(uWidth, uHeight, "RoughnessTarget", PixelFormat_R32_Float);
	m_RoughnessTarget.SetResizeEvent("BackBufferResize");
	m_RoughnessTarget.SetDefaultClearColor(Math::float4(0, 0, 0, 0));

	float fSSAOScale = 1.0f;
	m_SSAOTarget = RenderTargetTextureDX11(static_cast<uint32_t>(uWidth * fSSAOScale), static_cast<uint32_t>(uHeight * fSSAOScale), "SSAOTarget", PixelFormat_R32_Float);
	m_SSAOTarget.SetResizeEvent("BackBufferResize", fSSAOScale, fSSAOScale);
	m_SSAOTarget.SetDefaultClearColor(Math::float4(1, 1, 1, 1));

	//m_SSAOBentTarget = RenderTargetTextureDX11(uWidth * fSSAOScale, uHeight * fSSAOScale, "SSAOBentTarget");
	//m_SSAOBentTarget.SetResizeEvent("BackBufferResize", fSSAOScale, fSSAOScale);
	//m_SSAOBentTarget.SetDefaultClearColor(Math::float4(1, 1, 1, 1));

	m_BlurTarget = RenderTargetTextureDX11(static_cast<uint32_t>(uWidth * fSSAOScale), static_cast<uint32_t>(uHeight * fSSAOScale), "BlurTarget");
	m_BlurTarget.SetResizeEvent("BackBufferResize", fSSAOScale, fSSAOScale);

	m_DiffuseLightMapTarget = RenderTargetTextureDX11(uWidth, uHeight, "DiffuseLightMapTarget", kLightMapFormat);
	m_DiffuseLightMapTarget.SetResizeEvent("BackBufferResize");
	m_DiffuseLightMapTarget.SetDefaultClearColor(Math::float4(0, 0, 0, 0));

	m_SpecularLightMapTarget = RenderTargetTextureDX11(uWidth, uHeight, "SpecularLightMapTarget", kLightMapFormat);
	m_SpecularLightMapTarget.SetResizeEvent("BackBufferResize");
	m_SpecularLightMapTarget.SetDefaultClearColor(Math::float4(0, 0, 0, 0));

	m_EmissiveLightMapTarget = RenderTargetTextureDX11(uWidth, uHeight, "EmissiveLightMapTarget", kLightMapFormat);
	m_EmissiveLightMapTarget.SetResizeEvent("BackBufferResize");
	m_EmissiveLightMapTarget.SetDefaultClearColor(Math::float4(0, 0, 0, 0));	

	/// PostProcessing Textures
	/// -> HDR
	m_CurrentLuminanceTarget = nullptr;

	m_CurrentLuminanceTarget = RenderTargetTextureDX11(1024, 1024, "CurrentLuminance", PixelFormat_R32_Float);
	m_CurrentLuminanceTarget.SetDefaultClearColor(Math::float4(0, 0, 0, 0));

	m_AdaptiveLuminanceTarget[0] = RenderTargetTextureDX11(1024, 1024, "AdaptiveLuminance0", PixelFormat_R32_Float, true, true);
	m_AdaptiveLuminanceTarget[0].SetDefaultClearColor(Math::float4(0, 0, 0, 0));
	m_AdaptiveLuminanceTarget[1] = RenderTargetTextureDX11(1024, 1024, "AdaptiveLuminance1", PixelFormat_R32_Float, true, true);
	m_AdaptiveLuminanceTarget[1].SetDefaultClearColor(Math::float4(0, 0, 0, 0));

	m_BloomTarget = RenderTargetTextureDX11(uWidth, uHeight, "BloomTarget", PixelFormat_R16G16B16A16_Float);
	m_BloomTarget.SetResizeEvent("BackBufferResize");
	m_BloomTarget.SetDefaultClearColor(Math::float4(0, 0, 0, 0));

	m_DownScaleTarget[0] = RenderTargetTextureDX11(uWidth / 2, uHeight / 2, "DownScale0", PixelFormat_R16G16B16A16_Float);
	m_DownScaleTarget[0].SetResizeEvent("BackBufferResize", 0.5f, 0.5f);
	m_DownScaleTarget[0].SetDefaultClearColor(Math::float4(0, 0, 0, 0));
	m_DownScaleTarget[1] = RenderTargetTextureDX11(uWidth / 4, uHeight / 4, "DownScale1", PixelFormat_R16G16B16A16_Float);
	m_DownScaleTarget[1].SetResizeEvent("BackBufferResize", 0.25f, 0.25f);
	m_DownScaleTarget[1].SetDefaultClearColor(Math::float4(0, 0, 0, 0));
	m_DownScaleTarget[2] = RenderTargetTextureDX11(uWidth / 8, uHeight / 8, "DownScale2", PixelFormat_R16G16B16A16_Float);
	m_DownScaleTarget[2].SetResizeEvent("BackBufferResize", 0.125f, 0.125f);
	m_DownScaleTarget[2].SetDefaultClearColor(Math::float4(0, 0, 0, 0));

	m_DownScaledBlurTarget[0] = RenderTargetTextureDX11(uWidth / 8, uHeight / 8, "BlurTargetDownScaled0", PixelFormat_R16G16B16A16_Float);
	m_DownScaledBlurTarget[0].SetResizeEvent("BackBufferResize", 0.125f, 0.125f);
	m_DownScaledBlurTarget[0].SetDefaultClearColor(Math::float4(0, 0, 0, 0));

	/// Used for DoF
	m_DownScaleTarget[3] = RenderTargetTextureDX11(uWidth / 2, uHeight / 2, "DownScale3", PixelFormat_R16G16B16A16_Float);
	m_DownScaleTarget[3].SetResizeEvent("BackBufferResize", 0.5f, 0.5f);
	m_DownScaleTarget[3].SetDefaultClearColor(Math::float4(0, 0, 0, 0));

	m_DownScaledBlurTarget[1] = RenderTargetTextureDX11(uWidth / 2, uHeight / 2, "BlurTargetDownScaled1", PixelFormat_R16G16B16A16_Float);
	m_DownScaledBlurTarget[1].SetResizeEvent("BackBufferResize", 0.5f, 0.5f);
	m_DownScaledBlurTarget[1].SetDefaultClearColor(Math::float4(0, 0, 0, 0));

	if (m_bPostEffects)
	{
		m_PostEffectTarget = RenderTargetTextureDX11(uWidth, uHeight, "PostEffectTarget", PixelFormat_R8G8B8A8_UNorm);
		m_PostEffectTarget.SetResizeEvent("BackBufferResize");
		m_PostEffectTarget.SetDefaultClearColor(Math::float4(0, 0, 0, 0));
	}
}
//---------------------------------------------------------------------------------------------------
void DeferredRendererDX11::InitializePasses()
{
	//m_pRenderPassDepth = std::make_shared<HelixDepthPrePassRenderPassDX11>();
	//m_pRenderPassDepth->SetShader("DepthPrePass");
	//m_pRenderPassDepth->SetDepthStencilTexture(m_DepthBuffer);
	//m_pRenderPassDepth->SetOutputTexture(m_LinearDepthTarget, "LINEAR_DEPTH");
	//m_pRenderPassDepth->SetDebugMode(m_bDebugShaders);

	if(m_pRenderPassSolid == nullptr)
	{
		m_pRenderPassSolid = std::make_shared<HelixSolidRenderPassDX11>();
	}

	m_pRenderPassSolid->SetDepthStencilTexture(m_DepthBuffer);
	m_pRenderPassSolid->SetOutputTexture(m_AlbedoTarget, "MTL_ALBEDO");
	m_pRenderPassSolid->SetOutputTexture(m_NormalWSTarget, "WORLDSPACE_NORMAL");
	m_pRenderPassSolid->SetOutputTexture(m_MetallicTarget, "MTL_METALLIC");
	m_pRenderPassSolid->SetOutputTexture(m_RoughnessTarget, "MTL_ROUGHNESS");
	m_pRenderPassSolid->SetOutputTexture(m_LinearDepthTarget, "LINEAR_DEPTH");
	m_pRenderPassSolid->SetOutputTexture(m_EmissiveLightMapTarget, "EMISSIVE_LIGHTMAP");
	m_pRenderPassSolid->SetDebugMode(m_bDebugShaders);

	if (m_bShadowMap)
	{
		m_pRenderPassShadow = std::make_shared<HelixShadowMapRenderPassDX11>();
		m_pRenderPassShadow->SetDepthStencilTexture(m_ShadowMap);
		m_pRenderPassShadow->SetDebugMode(m_bDebugShaders);
	}

	/// Shadow map pass for point lights
	m_pRenderPassPointShadows = std::make_shared<HelixParaboloidShadowRenderPassDX11>();
	m_pRenderPassPointShadows->SetLightType(LightType_Point);
	m_pRenderPassPointShadows->SetDepthStencilTexture(m_PointShadowDepth);
	m_pRenderPassPointShadows->SetDebugMode(m_bDebugShaders);
	/// Shadow map pass for spot lights
	m_pRenderPassSpotShadows = std::make_shared<HelixProjectedShadowRenderPassDX11>();
	m_pRenderPassSpotShadows->SetLightType(LightType_Spot);
	m_pRenderPassSpotShadows->SetDepthStencilTexture(m_SpotShadowDepth);
	m_pRenderPassSpotShadows->SetDebugMode(m_bDebugShaders);
	
	m_pRenderPassLighting = std::make_shared<HelixDeferredLightingRenderPassDX11>();
	m_pRenderPassLighting->SetDepthStencilTexture(m_DepthBuffer);
	m_pRenderPassLighting->SetInputTexture(m_PointShadowDepth, "gParaboloidPointShadowMap", false);
	m_pRenderPassLighting->SetInputTexture(m_SpotShadowDepth, "gProjectedSpotShadowMap", false);

	if (m_bShadowMap)
	{
		m_pRenderPassLighting->SetInputTexture(m_ShadowMap, "gShadowMap", false);
	}
	m_pRenderPassLighting->SetOutputTexture(m_DiffuseLightMapTarget, "DIFFUSE_LIGHTMAP");
	m_pRenderPassLighting->SetOutputTexture(m_SpecularLightMapTarget, "SPECULAR_LIGHTMAP");
	//m_pRenderPassLighting->SetOutputTexture(m_NormalDebugTarget, "NORMAL_DEBUG");
	m_pRenderPassLighting->SetDebugMode(m_bDebugShaders);
	m_pRenderPassLighting->SetShadowOffsets(0.22f, 0.01f);

	if(m_bSSAO)
	{
		m_pRenderPassSSAO = std::make_shared<HelixSSAORenderPassDX11>();
		m_pRenderPassSSAO->SetDepthStencilTexture(m_DepthBuffer);
		m_pRenderPassSSAO->SetOutputTexture(m_SSAOTarget, "vOcclusion", false);
		//m_pRenderPassSSAO->SetOutputTexture(m_SSAOBentTarget, "SSAO_BENT_NORMAL_MAP");
		m_pRenderPassSSAO->SetDebugMode(m_bDebugShaders);

		m_pRenderPassHorizontalBlur[0] = std::make_shared<HelixHorizontalBlurRenderPassDX11>("Inst0");
		m_pRenderPassHorizontalBlur[0]->SetInputTexture(m_SSAOTarget, "gTex", false);
		m_pRenderPassHorizontalBlur[0]->SetOutputTexture(m_BlurTarget, "HORIZONTAL_BLUR");
		m_pRenderPassHorizontalBlur[0]->SetDebugMode(m_bDebugShaders);
		m_pRenderPassHorizontalBlur[0]->SetupBlur(6u, 0.8f, 2.0f);

		m_pRenderPassVerticalBlur[0] = std::make_shared<HelixVerticalBlurRenderPassDX11>("Inst0");
		m_pRenderPassVerticalBlur[0]->SetOutputTexture(m_SSAOTarget, "BLURRED_TEX");
		m_pRenderPassVerticalBlur[0]->SetDebugMode(m_bDebugShaders);
		m_pRenderPassHorizontalBlur[0]->SetupBlur(6u, 0.8f, 2.0f);
	}

	m_pRenderPassSkybox = std::make_shared<HelixDeferredSkyboxRenderPassDX11>();
	m_pRenderPassSkybox->SetInputTexture(m_RandomTexture2D, "gRandomTexture", false);
	m_pRenderPassSkybox->SetDepthStencilTexture(m_DepthBuffer);
	m_pRenderPassSkybox->SetDebugMode(m_bDebugShaders);

	m_pRenderPassDebug = std::make_shared<HelixDebugRenderPassDX11>();
	m_pRenderPassDebug->SetDepthStencilTexture(m_DepthBuffer);
	m_pRenderPassDebug->SetOutputTexture(*m_pBackBuffer, "BACKBUFFER");

	m_pRenderPassGizmo = std::make_shared<HelixGizmoRenderPassDX11>();
	m_pRenderPassGizmo->SetDepthStencilTexture(m_GizmoDepthBuffer);
	m_pRenderPassGizmo->SetOutputTexture(*m_pBackBuffer, "BACKBUFFER");

	m_pRenderPassMerge = std::make_shared<HelixDeferredMergeRenderPassDX11>();
	if(m_bSSAO)
	{
		m_pRenderPassMerge->SetInputTexture(m_SSAOTarget, "gSSAOMap", false);
	}

	m_pRenderPassMerge->SetDebugMode(m_bDebugShaders);
	m_pRenderPassMerge->EnableSSAO(m_bSSAO);

	if (m_bHDR == false)
	{
		m_pRenderPassSkybox->SetOutputTexture(*m_pBackBuffer, "vAlbedo", false);
		m_pRenderPassMerge->SetOutputTexture(*m_pBackBuffer, "LIT_SCENE");
		return;
	}

	/// HDR PART
	m_pRenderPassSkybox->SetOutputTexture(m_LitSceneHDR, "vAlbedo", false);
	m_pRenderPassMerge->SetOutputTexture(m_LitSceneHDR, "LIT_SCENE");

	/// DoF Pass with Blur
	if (m_bDepthOfField)
	{
		m_pRenderPassScale[5] = std::make_shared<HelixScaleRenderPassDX11>("Inst5");
		m_pRenderPassScale[5]->SetInputTexture(m_LitSceneHDR, "gInTex", false);
		m_pRenderPassScale[5]->SetOutputTexture(m_DownScaleTarget[3], "SCALED_TEXTURE");

		for (int i = 5; i < 7; ++i)
		{
			m_pRenderPassHorizontalBlur[i] = std::make_shared<HelixHorizontalBlurRenderPassDX11>("Inst" + std::to_string(i));
			m_pRenderPassHorizontalBlur[i]->SetInputTexture(m_DownScaleTarget[3], "gTex", false);
			m_pRenderPassHorizontalBlur[i]->SetOutputTexture(m_DownScaledBlurTarget[1], "HORIZONTAL_BLUR");
			m_pRenderPassHorizontalBlur[i]->SetDebugMode(m_bDebugShaders);

			m_pRenderPassVerticalBlur[i] = std::make_shared<HelixVerticalBlurRenderPassDX11>("Inst" + std::to_string(i));
			m_pRenderPassVerticalBlur[i]->SetOutputTexture(m_DownScaleTarget[3], "BLURRED_TEX");
			m_pRenderPassVerticalBlur[i]->SetDebugMode(m_bDebugShaders);
		}
		m_pRenderPassHorizontalBlur[5]->SetupBlur(6u, 0.8f, 1.0f); // (6u, 0.8f, 1.5f)
		m_pRenderPassVerticalBlur[5]->SetupBlur(6u, 0.8f, 1.0f); // (6u, 0.8f, 1.5f)
		m_pRenderPassHorizontalBlur[6]->SetupBlur(6u, 0.8f, 1.0f);
		m_pRenderPassVerticalBlur[6]->SetupBlur(6u, 0.8f, 1.0f);

		m_pRenderPassSimpleDoF = std::make_shared<HelixSimpleDoFRenderPassDX11>();
		m_pRenderPassSimpleDoF->SetInputTexture(m_DownScaleTarget[3], "gSceneBlurredTex", false);
		m_pRenderPassSimpleDoF->SetOutputTexture(m_LitSceneHDR, "vColor", false);
		m_pRenderPassSimpleDoF->SetDebugMode(m_bDebugShaders);
	}

	/// Calculate luminance map and adapt to luminance changes
	m_pRenderPassLuminance = std::make_shared<HelixLuminanceRenderPassDX11>();
	m_pRenderPassLuminance->SetInputTexture(m_LitSceneHDR, "gInputTex", false);
	m_pRenderPassLuminance->SetOutputTexture(m_CurrentLuminanceTarget, "fLuminance", false);

	m_pRenderPassLuminanceAdaption = std::make_shared<HelixLuminanceAdaptionRenderPassDX11>();
	m_pRenderPassLuminanceAdaption->SetAdaptionTargets(m_AdaptiveLuminanceTarget[0], m_AdaptiveLuminanceTarget[1]);

	m_pRenderPassBloom = std::make_shared<HelixBloomRenderPassDX11>();
	m_pRenderPassBloom->SetInputTexture(m_LitSceneHDR, "gColorMap", false);
	m_pRenderPassBloom->SetOutputTexture(m_BloomTarget, "vBloom", false);

	m_pRenderPassScale[0] = std::make_shared<HelixScaleRenderPassDX11>("Inst0");
	m_pRenderPassScale[0]->SetInputTexture(m_BloomTarget, "gInTex", false);
	m_pRenderPassScale[0]->SetOutputTexture(m_DownScaleTarget[0], "SCALED_TEXTURE");

	m_pRenderPassScale[1] = std::make_shared<HelixScaleRenderPassDX11>("Inst1");
	m_pRenderPassScale[1]->SetInputTexture(m_DownScaleTarget[0], "gInTex", false);
	m_pRenderPassScale[1]->SetOutputTexture(m_DownScaleTarget[1], "SCALED_TEXTURE");

	m_pRenderPassScale[2] = std::make_shared<HelixScaleRenderPassDX11>("Inst2");
	m_pRenderPassScale[2]->SetInputTexture(m_DownScaleTarget[1], "gInTex", false);
	m_pRenderPassScale[2]->SetOutputTexture(m_DownScaleTarget[2], "SCALED_TEXTURE");

	for(int i = 1; i < 5; ++i)
	{
		m_pRenderPassHorizontalBlur[i] = std::make_shared<HelixHorizontalBlurRenderPassDX11>("Inst" + std::to_string(i));
		m_pRenderPassHorizontalBlur[i]->SetInputTexture(m_DownScaleTarget[2], "gTex", false);
		m_pRenderPassHorizontalBlur[i]->SetOutputTexture(m_DownScaledBlurTarget[0], "HORIZONTAL_BLUR");
		m_pRenderPassHorizontalBlur[i]->SetDebugMode(m_bDebugShaders);
		m_pRenderPassHorizontalBlur[i]->SetupBlur(6u, 0.8f, 1.0f);

		m_pRenderPassVerticalBlur[i] = std::make_shared<HelixVerticalBlurRenderPassDX11>("Inst" + std::to_string(i));
		m_pRenderPassVerticalBlur[i]->SetOutputTexture(m_DownScaleTarget[2], "BLURRED_TEX");
		m_pRenderPassVerticalBlur[i]->SetDebugMode(m_bDebugShaders);
		m_pRenderPassVerticalBlur[i]->SetupBlur(6u, 0.8f, 1.0f);
	}
	m_pRenderPassHorizontalBlur[1]->SetupBlur(6u, 0.8f, 2.0f);
	m_pRenderPassVerticalBlur[1]->SetupBlur(6u, 0.8f, 2.0f);

	m_pRenderPassScale[3] = std::make_shared<HelixScaleRenderPassDX11>("Inst3");
	m_pRenderPassScale[3]->SetInputTexture(m_DownScaleTarget[2], "gInTex", false);
	m_pRenderPassScale[3]->SetOutputTexture(m_DownScaleTarget[1], "SCALED_TEXTURE");

	m_pRenderPassScale[4] = std::make_shared<HelixScaleRenderPassDX11>("Inst4");
	m_pRenderPassScale[4]->SetInputTexture(m_DownScaleTarget[1], "gInTex", false);
	m_pRenderPassScale[4]->SetOutputTexture(m_DownScaleTarget[0], "SCALED_TEXTURE");

	m_pRenderPassHDR = std::make_shared<HelixHDRCompositeRenderPassDX11>();
	m_pRenderPassHDR->SetInputTexture(m_DownScaleTarget[0], "gBloomMap", false);
	m_pRenderPassHDR->EnableAutoExposure(m_bAutoExposure);

	if(m_bPostEffects == false)
	{
		m_pRenderPassHDR->SetOutputTexture(*m_pBackBuffer, "HDR_COMPOSITE");
		return;
	}

	m_pRenderPassHDR->SetOutputTexture(m_PostEffectTarget, "HDR_COMPOSITE");

	/// Post FX
	m_pRenderPassGlitch = std::make_shared<HelixGlitchRenderPassDX11>();
	m_pRenderPassGlitch->SetInputTexture(m_PostEffectTarget, "gInTex", false);
	m_pRenderPassGlitch->SetOutputTexture(*m_pBackBuffer, "vColor", false);
	m_pRenderPassGlitch->SetDebugMode(m_bDebugShaders);

}
//---------------------------------------------------------------------------------------------------
bool DeferredRendererDX11::InitializeProcedures()
{
	m_RenderProcedure = std::make_shared<RenderProcedureDX11>("SolidProc");
	m_LightingProcedure = std::make_shared<RenderProcedureDX11>("LightingProc");
	m_MergeProcedure = std::make_shared<RenderProcedureDX11>("MergeProc");
	m_PostProcessingProcedure = std::make_shared<RenderProcedureDX11>("PostProc");
	m_GizmoProcedure = std::make_shared<RenderProcedureDX11>("GizmoProc");

	// Add the pass to the RenderProc
	if (m_bShadowMap)
	{
		m_RenderProcedure->AddPass(m_pRenderPassShadow);
	}
	m_RenderProcedure->AddPass(m_pRenderPassPointShadows);
	m_RenderProcedure->AddPass(m_pRenderPassSpotShadows);

	//m_RenderProcedure->AddPass(m_pRenderPassDepth);
	m_RenderProcedure->AddPass(m_pRenderPassSolid);

	// Add passes to lighting procedure
	if (m_bSSAO)
	{
		m_LightingProcedure->AddPass(m_pRenderPassSSAO);
		m_LightingProcedure->AddPass(m_pRenderPassHorizontalBlur[0]);
		m_LightingProcedure->AddPass(m_pRenderPassVerticalBlur[0]);
	}
	m_LightingProcedure->AddPass(m_pRenderPassLighting);

	m_MergeProcedure->AddPass(m_pRenderPassMerge);
	m_MergeProcedure->AddPass(m_pRenderPassSkybox);

	// Add passes to post processing procedure
	if(m_bHDR)
	{
		if (m_bDepthOfField)
		{
			/// DoF
			m_PostProcessingProcedure->AddPass(m_pRenderPassScale[5]);
			for (int i = 5; i < 7; ++i)
			{
				m_PostProcessingProcedure->AddPass(m_pRenderPassHorizontalBlur[i]);
				m_PostProcessingProcedure->AddPass(m_pRenderPassVerticalBlur[i]);
			}
			m_PostProcessingProcedure->AddPass(m_pRenderPassSimpleDoF);
		}

		/// Luminance
		m_PostProcessingProcedure->AddPass(m_pRenderPassLuminance);
		m_PostProcessingProcedure->AddPass(m_pRenderPassLuminanceAdaption);

		m_PostProcessingProcedure->AddPass(m_pRenderPassBloom);
		m_PostProcessingProcedure->AddPass(m_pRenderPassScale[0]);
		m_PostProcessingProcedure->AddPass(m_pRenderPassScale[1]);
		m_PostProcessingProcedure->AddPass(m_pRenderPassScale[2]);

		for (int i = 1; i < 5; ++i)
		{
			m_PostProcessingProcedure->AddPass(m_pRenderPassHorizontalBlur[i]);
			m_PostProcessingProcedure->AddPass(m_pRenderPassVerticalBlur[i]);
		}

		m_PostProcessingProcedure->AddPass(m_pRenderPassScale[3]);
		m_PostProcessingProcedure->AddPass(m_pRenderPassScale[4]);

		m_PostProcessingProcedure->AddPass(m_pRenderPassHDR);
	}
	if (m_bPostEffects)
	{
		m_PostProcessingProcedure->AddPass(m_pRenderPassGlitch);
	}
	//m_PostProcessingProcedure->AddPass(m_pRenderPassDebug);

	m_GizmoProcedure->AddPass(m_pRenderPassDebug);
	m_GizmoProcedure->AddPass(m_pRenderPassGizmo);

	RenderProcedureManagerDX11& Renderer = RenderingScene::Instance()->GetRenderer();

	bool bInitialized = Renderer.AddProcedure(m_RenderProcedure);
	bInitialized &= Renderer.AddProcedure(m_LightingProcedure);
	bInitialized &= Renderer.AddProcedure(m_MergeProcedure);
	bInitialized &= Renderer.AddProcedure(m_PostProcessingProcedure);
	bInitialized &= Renderer.AddProcedure(m_GizmoProcedure);

	//HASSERTD(bInitialized == true, "Failed on renderer initialization!");

	//hlx::TextToken SerializedRenderer = Renderer.CreateDescription().Serialize();
	//SerializedRenderer.serialize("renderer.helix");

	//Resources::RendererDesc Desc(SerializedRenderer);
	//return m_Renderer.Load(Desc, true);

	return bInitialized;
}
//---------------------------------------------------------------------------------------------------
void DeferredRendererDX11::GenerateRandomTexture1D()
{
	BufferSubresourceData Subres = {};
	const uint32_t uRandomTextureSize = 1024;
	Subres.uRowPitch = uRandomTextureSize * sizeof(Math::float4);

	std::vector<Math::float4> RandomData;
	RandomData.reserve(uRandomTextureSize);

	std::srand(static_cast<uint32_t>(std::time(nullptr))); // use current time as seed for random generator
	const float fInvRandMax = 2.0f / static_cast<float>(RAND_MAX);

	for (uint32_t i = 0; i < uRandomTextureSize; ++i)
	{
		// Generate values between -1.0f and 1.0f
		float fRandomA = static_cast<float>(std::rand()) * fInvRandMax - 1.0f;
		float fRandomB = static_cast<float>(std::rand()) * fInvRandMax - 1.0f;
		float fRandomC = static_cast<float>(std::rand()) * fInvRandMax - 1.0f;
		float fRandomD = static_cast<float>(std::rand()) * fInvRandMax - 1.0f;

		RandomData.push_back({ fRandomA,fRandomB,fRandomC,fRandomD });
	}

	Subres.pData = &RandomData[0];

	m_RandomTexture1D = ImmutableTextureDX11(uRandomTextureSize, 1, "RandomTexture", Subres, PixelFormat_R32G32B32A32_Float, ResourceDimension_Texture1D);
}
//---------------------------------------------------------------------------------------------------
void DeferredRendererDX11::GenerateRandomTexture2D()
{
	BufferSubresourceData Subres = {};
	const uint32_t uRandomTextureSize = 256;
	Subres.uRowPitch = uRandomTextureSize * sizeof(Math::float4);

	std::vector<Math::float4> RandomData;
	RandomData.reserve(uRandomTextureSize*uRandomTextureSize);

	std::srand(static_cast<uint32_t>(std::time(nullptr))); // use current time as seed for random generator
	const float fInvRandMax = 1.0f / static_cast<float>(RAND_MAX);

	for (uint32_t i = 0; i < uRandomTextureSize*uRandomTextureSize; ++i)
	{
		// Generate values between 0.0f and 1.0f
		float fRandomA = static_cast<float>(std::rand()) * fInvRandMax;// -1.0f;
		float fRandomB = static_cast<float>(std::rand()) * fInvRandMax;// -1.0f;
		float fRandomC = static_cast<float>(std::rand()) * fInvRandMax;// -1.0f;
		float fRandomD = static_cast<float>(std::rand()) * fInvRandMax;// -1.0f;

		RandomData.push_back({ fRandomA,fRandomB,fRandomC,fRandomD });
	}

	Subres.pData = &RandomData[0];

	m_RandomTexture2D = ImmutableTextureDX11(uRandomTextureSize, uRandomTextureSize, "RandomTexture", Subres, PixelFormat_R32G32B32A32_Float, ResourceDimension_Texture2D);
}
//---------------------------------------------------------------------------------------------------
// SETTER
void DeferredRendererDX11::SetSolidRenderPass(std::shared_ptr<HelixSolidRenderPassDX11> _pSolidPass)
{
	if(m_pRenderPassSolid == nullptr)
	{
		m_pRenderPassSolid = _pSolidPass;
		HLOG("Registered custom solid render pass");
	}
}
//---------------------------------------------------------------------------------------------------
