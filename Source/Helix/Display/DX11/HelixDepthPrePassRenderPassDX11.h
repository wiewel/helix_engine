//   Copyright 2020 Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef HELIXDEPTHPREPASSRENDERPASSDX11_H
#define HELIXDEPTHPREPASSRENDERPASSDX11_H

#include "RenderPassDX11.h"
#include "CommonConstantBufferFunctions.h"

namespace Helix
{
	namespace Display
	{
		class HelixDepthPrePassRenderPassDX11 : public RenderPassDX11
		{
		public:
			HDEBUGNAME("HelixDepthPrePassRenderPassDX11");

			HelixDepthPrePassRenderPassDX11(DefaultInitializerType);
			HelixDepthPrePassRenderPassDX11(const std::string& _sInstanceName = {});
			virtual ~HelixDepthPrePassRenderPassDX11();

			bool OnInitalize(const hlx::TextToken& _CustomVariables) final;

			bool OnPerCamera(const Scene::CameraComponent& _Camera) final;
			bool OnPerObject(const RenderObjectDX11& _RenderObject, const MaterialDX11& _Material) final;
			std::shared_ptr<RenderPassDX11> CreatePass(const std::string& _sInstanceName) const final;

		private:
			ShaderRenderTargetDX11 m_LinearDepthTarget = { "vLinearDepth", "LINEAR_DEPTH" };

			TCBPerCamera m_CBPerCamera = { "cbPerCamera" };
			TCBPerObject m_CBPerObject = { "cbPerObject" };
		};

		//---------------------------------------------------------------------------------------------------

		inline bool HelixDepthPrePassRenderPassDX11::OnPerCamera(const Scene::CameraComponent& _Camera)
		{
			SetCameraConstants(m_CBPerCamera, _Camera.GetRenderingProperties());

			return true;
		}
		//---------------------------------------------------------------------------------------------------

		inline bool HelixDepthPrePassRenderPassDX11::OnPerObject(const RenderObjectDX11& _RenderObject, const MaterialDX11& _Material)
		{
			SetObjectConstants(m_CBPerObject, _RenderObject); // Dont set MaterialPoperties

			return true;
		}

	} // Display
} // Helix

#endif