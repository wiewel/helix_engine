//   Copyright 2020 Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef HELIXHDRCOMPOSITERENDERPASSDX11_H
#define HELIXHDRCOMPOSITERENDERPASSDX11_H

#include "RenderPassDX11.h"
#include "Display\Shaders\HDRComposite.hlsl"

namespace Helix
{
	namespace Display
	{
		class HelixHDRCompositeRenderPassDX11 : public RenderPassDX11
		{
		public:
			HDEBUGNAME("HelixHDRCompositeRenderPassDX11");

			HelixHDRCompositeRenderPassDX11(DefaultInitializerType);
			HelixHDRCompositeRenderPassDX11(const std::string& _sInstanceName = {});
			~HelixHDRCompositeRenderPassDX11();

			bool OnInitalize(const hlx::TextToken& _CustomVariables) final;

			bool OnPerFrame() final;

			void EnableAutoExposure(const bool _bAutoExposure);
			bool IsAutoExposureEnabled() const;

			void SetBloomIntensity(const float _fIntensity);
			float GetBloomIntensity() const;

			void SetLuminanceMipLevel(const float _fMipLevel);
			float GetLuminanceMipLevel() const;

			void SetExposureKeyLevel(const float _fExposureKeyValue);
			float GetExposureKeyLevel() const;
			std::shared_ptr<RenderPassDX11> CreatePass(const std::string& _sInstanceName) const final;

		private:
			ShaderVariableDX11<Shaders::HDRComposite::cbHDRComposite> m_CBHDRComposite = { "cbHDRComposite" };

			ShaderTextureDX11 m_ColorMap = { "gColorMap", "LIT_SCENE" };
			ShaderTextureDX11 m_LuminanceMap = { "gLuminanceMap", "LUMINANCE_ADAPTION_MAP" };
			ShaderTextureDX11 m_BloomMap = { "gBloomMap" };

			ShaderRenderTargetDX11 m_OutputTexture = { "vHDRComposite", "HDR_COMPOSITE" };

		private:
			bool m_bAutoExposure = false;
			float m_fBloomIntensity = 0.2f;
			float m_fLuminanceMipLevel = 10.0f; // least detailed mip level for 1024 texture -> average luminance
			float m_fExposureKeyValue = 0.05f; // value between [0-1]
		};

		inline void HelixHDRCompositeRenderPassDX11::EnableAutoExposure(const bool _bAutoExposure) { m_bAutoExposure = _bAutoExposure; }
		inline bool HelixHDRCompositeRenderPassDX11::IsAutoExposureEnabled() const { return m_bAutoExposure; }

		inline void HelixHDRCompositeRenderPassDX11::SetBloomIntensity(const float _fIntensity) { m_fBloomIntensity = _fIntensity; }
		inline float HelixHDRCompositeRenderPassDX11::GetBloomIntensity() const { return m_fBloomIntensity; }

		inline void HelixHDRCompositeRenderPassDX11::SetLuminanceMipLevel(const float _fMipLevel) { m_fLuminanceMipLevel = _fMipLevel; }
		inline float HelixHDRCompositeRenderPassDX11::GetLuminanceMipLevel() const { return m_fLuminanceMipLevel; }

		inline void HelixHDRCompositeRenderPassDX11::SetExposureKeyLevel(const float _fExposureKeyValue) { m_fExposureKeyValue = _fExposureKeyValue; }
		inline float HelixHDRCompositeRenderPassDX11::GetExposureKeyLevel() const { return m_fExposureKeyValue; }


	} // Display
} // Helix

#endif // HELIXHDRCOMPOSITERENDERPASSDX11_H