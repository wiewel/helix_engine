//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "MeshDX11.h"
#include "RenderDeviceDX11.h"

using namespace DirectX;
using namespace Helix::Display;

//---------------------------------------------------------------------------------------------------
MeshDX11::MeshDX11(bool _bDynamic) :
	Mesh(_bDynamic)
{
}
//---------------------------------------------------------------------------------------------------
void MeshDX11::InitializeIndexBuffer(PixelFormat _kIndexFormat)
{
	HASSERT(m_pIndexBuffer == nullptr, "IndexBuffer already exists!");

	m_pIndexBuffer = new IndexBufferDX11(_kIndexFormat);

	uint8_t* pData = m_IndexBuffer.m_uElementCount > 0 ? &m_IndexBuffer.m_Data[0] : nullptr;

	if (m_pIndexBuffer->Initialize(pData, m_IndexBuffer.m_uMaxElementCount * m_IndexBuffer.m_uElementDescSize, m_bDynamic))
	{
		if (m_pDeviceContext == nullptr)
		{
			m_pDeviceContext = RenderDeviceDX11::Instance()->GetContext();
		}
	}	
}
//---------------------------------------------------------------------------------------------------
void MeshDX11::InitializeVertexBuffer()
{
	HASSERT(m_pVertexBuffer == nullptr, "VertexBuffer already exists!");

	m_pVertexBuffer = new VertexBufferDX11(m_VertexBuffer.m_uElementDescSize);
	
	// workaround
	m_pVertexBuffer->ChangeSlot(0);

	uint8_t* pData = m_VertexBuffer.m_uElementCount > 0 ? &m_VertexBuffer.m_Data[0] : nullptr;
	
	if (m_pVertexBuffer->Initialize(pData, m_VertexBuffer.m_uMaxElementCount * m_VertexBuffer.m_uElementDescSize, m_bDynamic))
	{
		if (m_pDeviceContext == nullptr)
		{
			m_pDeviceContext = RenderDeviceDX11::Instance()->GetContext();
		}
	}	
}
//---------------------------------------------------------------------------------------------------
MeshDX11::~MeshDX11()
{
	HSAFE_DELETE(m_pVertexBuffer);
	HSAFE_DELETE(m_pIndexBuffer);
}
//---------------------------------------------------------------------------------------------------
void MeshDX11::Draw()
{
	m_pDeviceContext->IASetPrimitiveTopology(static_cast<D3D11_PRIMITIVE_TOPOLOGY>(m_kPrimitiveTopology));

	//Update buffers:
	if (m_pVertexBuffer)
	{
		int32_t uUpdatedElements = m_VertexBuffer.m_uLastUpdatedElement - m_VertexBuffer.m_uFirstUpdatedElement;

		if (uUpdatedElements > 0)
		{
			m_pVertexBuffer->Apply(&m_VertexBuffer.m_Data[0], uUpdatedElements * m_VertexBuffer.m_uElementDescSize, m_VertexBuffer.m_uFirstUpdatedElement * m_VertexBuffer.m_uElementDescSize);
			m_VertexBuffer.m_uLastUpdatedElement = 0;
			m_VertexBuffer.m_uFirstUpdatedElement = 0;
		}
	}	

	if (m_pIndexBuffer != nullptr)
	{
		int32_t uUpdatedElements = m_IndexBuffer.m_uLastUpdatedElement - m_IndexBuffer.m_uFirstUpdatedElement;

		if (uUpdatedElements > 0)
		{
			m_pIndexBuffer->Apply(&m_IndexBuffer.m_Data[0], uUpdatedElements * m_IndexBuffer.m_uElementDescSize, m_IndexBuffer.m_uFirstUpdatedElement * m_IndexBuffer.m_uElementDescSize);
			m_IndexBuffer.m_uLastUpdatedElement = 0;
			m_IndexBuffer.m_uFirstUpdatedElement = 0;
		}		
	}


		// not instanced, so set single vertex buffer
		// TODO: dynamic uStride calculation
		//const uint32_t uStride[1] = { m_uVertexDescSize };
		//const uint32_t uOffset[1] = { 0 };
		//ID3D11Buffer* VertexBuffers[] = { m_pVertexBuffer };
		//m_pDeviceContext->IASetVertexBuffers(0, 1, VertexBuffers, uStride, uOffset);

		if (m_pIndexBuffer != nullptr)
		{
			// set index buffer
			//m_pDeviceContext->IASetIndexBuffer(m_pIndexBuffer, DXGI_FORMAT_R32_UINT, 0);

			//void DrawIndexed( 
			//	[in]  UINT IndexCount,			=> Number of indices to draw.
			//	[in]  UINT StartIndexLocation,	=> The location of the first index read by the GPU from the index buffer.
			//	[in]  INT BaseVertexLocation	=> A value added to each index before reading a vertex from the vertex buffer.
			//);
			m_pDeviceContext->DrawIndexed(m_IndexBuffer.m_uElementCount, 0, 0);
		}
		else
		{
			//void Draw(
			//	[in]  UINT VertexCount,			=> Number of vertices to draw.
			//	[in]  UINT StartVertexLocation	=> Index of the first vertex, which is usually an offset in a vertex buffer; it could also be used as the first vertex id generated for a shader parameter marked with the SV_TargetId system-value semantic.
			//);
			m_pDeviceContext->Draw(m_VertexBuffer.m_uElementCount, 0);
		}
}
//---------------------------------------------------------------------------------------------------
std::shared_ptr<MeshDX11> MeshDX11::Generate2DQuad(const float _fScreenWidthPercentage, const float _fScreenHeightPercentage)
{
	std::shared_ptr<MeshDX11> pMesh;

	// Calculate the screen coordinates of the left side of the window.
	const float fLeft = -1.0f;
	// Calculate the screen coordinates of the top of the window.
	const float fTop = 1.0f;

	// Calculate the screen coordinates of the right side of the window.
	const float fRight = fLeft + 2.0f * _fScreenWidthPercentage;	
	// Calculate the screen coordinates of the bottom of the window.
	const float fBottom = fTop - 2.0f * _fScreenHeightPercentage;

	std::vector<VertexSimplePositionTexture> VertexBufferData;
	std::vector<uint32_t> IndexBufferData;
	const uint32_t uVPTDescSize = sizeof(VertexSimplePositionTexture);

	// First Triangle
	// Bottom Left
	VertexBufferData.push_back(VertexSimplePositionTexture(fLeft, fBottom, 0.0f, 1.0f));
	// Top Left
	VertexBufferData.push_back(VertexSimplePositionTexture(fLeft, fTop, 0.0f, 0.0f));
	// Bottom Right
	VertexBufferData.push_back(VertexSimplePositionTexture(fRight, fBottom, 1.0f, 1.0f));	
	// Top Right
	VertexBufferData.push_back(VertexSimplePositionTexture(fRight, fTop, 1.0f, 0.0f));
	
	//
	// Create the indices.
	//
	IndexBufferData.push_back(0);
	IndexBufferData.push_back(1);
	IndexBufferData.push_back(2);
	IndexBufferData.push_back(3);	

	pMesh = std::make_shared<MeshDX11>();

	pMesh->SetIndexBufferData<uint32_t>(IndexBufferData);
	pMesh->SetVertexBufferData<VertexSimplePositionTexture>(VertexBufferData);
	pMesh->SetPrimitiveTopology(PrimitiveTopology_TriangleStrip);

	return pMesh;
}
//---------------------------------------------------------------------------------------------------
std::shared_ptr<MeshDX11> MeshDX11::GenerateCube(float _fSize)
{
	std::shared_ptr<MeshDX11> pMesh;

	// 'Radius'
	_fSize *= 0.5f;

#define Luna
#ifdef Luna
#pragma region LunaCube

	std::vector<VertexPositionNTTexture> VertexBufferData;
	std::vector<uint32_t> IndexBufferData;
	const uint32_t uVPNTDescSize = sizeof(VertexPositionNTTexture);

	// Fill in the front face vertex data.
	VertexBufferData.push_back(VertexPositionNTTexture(-_fSize, -_fSize, -_fSize, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f));
	VertexBufferData.push_back(VertexPositionNTTexture(-_fSize, +_fSize, -_fSize, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f));
	VertexBufferData.push_back(VertexPositionNTTexture(+_fSize, +_fSize, -_fSize, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f));
	VertexBufferData.push_back(VertexPositionNTTexture(+_fSize, -_fSize, -_fSize, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f));

	// Fill in the back face vertex data.
	VertexBufferData.push_back(VertexPositionNTTexture(-_fSize, -_fSize, +_fSize, 0.0f, 0.0f, 1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 1.0f));
	VertexBufferData.push_back(VertexPositionNTTexture(+_fSize, -_fSize, +_fSize, 0.0f, 0.0f, 1.0f, -1.0f, 0.0f, 0.0f, 0.0f, 1.0f));
	VertexBufferData.push_back(VertexPositionNTTexture(+_fSize, +_fSize, +_fSize, 0.0f, 0.0f, 1.0f, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f));
	VertexBufferData.push_back(VertexPositionNTTexture(-_fSize, +_fSize, +_fSize, 0.0f, 0.0f, 1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f));

	// Fill in the top face vertex data.
	VertexBufferData.push_back(VertexPositionNTTexture(-_fSize, +_fSize, -_fSize, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f));
	VertexBufferData.push_back(VertexPositionNTTexture(-_fSize, +_fSize, +_fSize, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f));
	VertexBufferData.push_back(VertexPositionNTTexture(+_fSize, +_fSize, +_fSize, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f));
	VertexBufferData.push_back(VertexPositionNTTexture(+_fSize, +_fSize, -_fSize, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f));

	// Fill in the bottom face vertex data.
	VertexBufferData.push_back(VertexPositionNTTexture(-_fSize, -_fSize, -_fSize, 0.0f, -1.0f, 0.0f, -1.0f, 0.0f, 0.0f, 1.0f, 1.0f));
	VertexBufferData.push_back(VertexPositionNTTexture(+_fSize, -_fSize, -_fSize, 0.0f, -1.0f, 0.0f, -1.0f, 0.0f, 0.0f, 0.0f, 1.0f));
	VertexBufferData.push_back(VertexPositionNTTexture(+_fSize, -_fSize, +_fSize, 0.0f, -1.0f, 0.0f, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f));
	VertexBufferData.push_back(VertexPositionNTTexture(-_fSize, -_fSize, +_fSize, 0.0f, -1.0f, 0.0f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f));

	// Fill in the left face vertex data.
	VertexBufferData.push_back(VertexPositionNTTexture(-_fSize, -_fSize, +_fSize, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f));
	VertexBufferData.push_back(VertexPositionNTTexture(-_fSize, +_fSize, +_fSize, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f));
	VertexBufferData.push_back(VertexPositionNTTexture(-_fSize, +_fSize, -_fSize, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f));
	VertexBufferData.push_back(VertexPositionNTTexture(-_fSize, -_fSize, -_fSize, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f, -1.0f, 1.0f, 1.0f));

	// Fill in the right face vertex data.
	VertexBufferData.push_back(VertexPositionNTTexture(+_fSize, -_fSize, -_fSize, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f));
	VertexBufferData.push_back(VertexPositionNTTexture(+_fSize, +_fSize, -_fSize, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f));
	VertexBufferData.push_back(VertexPositionNTTexture(+_fSize, +_fSize, +_fSize, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f));
	VertexBufferData.push_back(VertexPositionNTTexture(+_fSize, -_fSize, +_fSize, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f));

	//
	// Create the indices.
	//

	UINT i[36];

	// Fill in the front face index data
	i[0] = 0; i[1] = 1; i[2] = 2;
	i[3] = 0; i[4] = 2; i[5] = 3;

	// Fill in the back face index data
	i[6] = 4; i[7] = 5; i[8] = 6;
	i[9] = 4; i[10] = 6; i[11] = 7;

	// Fill in the top face index data
	i[12] = 8; i[13] = 9; i[14] = 10;
	i[15] = 8; i[16] = 10; i[17] = 11;

	// Fill in the bottom face index data
	i[18] = 12; i[19] = 13; i[20] = 14;
	i[21] = 12; i[22] = 14; i[23] = 15;

	// Fill in the left face index data
	i[24] = 16; i[25] = 17; i[26] = 18;
	i[27] = 16; i[28] = 18; i[29] = 19;

	// Fill in the right face index data
	i[30] = 20; i[31] = 21; i[32] = 22;
	i[33] = 20; i[34] = 22; i[35] = 23;
	
	IndexBufferData.assign(&i[0], &i[36]);

	pMesh = std::make_shared<MeshDX11>(); //(/*(uint8_t*)&VertexBufferData[0], IndexBufferData, uVPNTDescSize, static_cast<uint32_t>(VertexBufferData.size())*/);
	
	pMesh->SetIndexBufferData<uint32_t>(IndexBufferData);
	pMesh->SetVertexBufferData<VertexPositionNTTexture>(VertexBufferData);
	pMesh->SetPrimitiveTopology(PrimitiveTopology_TriangleList);

	return pMesh;
#pragma endregion

#elif defined Complex

#pragma region ComplexCube
	// Constants
	const int32_t iFaceCount = 6;

	static const XMVECTOR FaceNormals[iFaceCount] =
	{
		{ 0, 0, 1 },
		{ 0, 0, -1 },
		{ 1, 0, 0 },
		{ -1, 0, 0 },
		{ 0, 1, 0 },
		{ 0, -1, 0 },
	};
	static const XMFLOAT2 TextureCoordinates[4] =
	{
		{ 1, 0 },
		{ 1, 1 },
		{ 0, 1 },
		{ 0, 0 },
	};

	// Buffer
	std::vector<VertexPositionNTTexture> VertexBufferData;
	std::vector<uint32_t> IndexBufferData;
	const uint32_t uVPNTDescSize = sizeof(VertexPositionNTTexture);
			
	VertexPositionNTTexture Vertex[4] = {};

	// Create each face in turn.
	for (int i = 0; i < iFaceCount; ++i)
	{
		XMVECTOR Normal = FaceNormals[i];

		// Get two vectors perpendicular both to the face normal and to each other.
		XMVECTOR Basis = (i >= 4) ? g_XMIdentityR2 : g_XMIdentityR1;

		XMVECTOR Side1 = XMVector3Cross(Normal, Basis);
		XMVECTOR Side2 = XMVector3Cross(Normal, Side1);

		// Six indices (two triangles) per face.
		uint32_t vbase = static_cast<uint32_t>(VertexBufferData.size());
		IndexBufferData.push_back(vbase + 0);
		IndexBufferData.push_back(vbase + 1);
		IndexBufferData.push_back(vbase + 2);

		IndexBufferData.push_back(vbase + 0);
		IndexBufferData.push_back(vbase + 2);
		IndexBufferData.push_back(vbase + 3);

		// Calculate Position, Normal, TexCoord

		// Four vertices per face
		XMStoreFloat3(&Vertex[0].m_vPosition, (Normal - Side1 - Side2) * _fSize);
		XMStoreFloat3(&Vertex[0].m_vNormal, Normal);
		Vertex[0].m_vTexCoord = TextureCoordinates[0];
		//VertexBufferData.push_back(VertexData);

		XMStoreFloat3(&Vertex[1].m_vPosition, (Normal - Side1 + Side2) * _fSize);
		XMStoreFloat3(&Vertex[1].m_vNormal, Normal);
		Vertex[1].m_vTexCoord = TextureCoordinates[1];
		//VertexBufferData.push_back(VertexData);

		XMStoreFloat3(&Vertex[2].m_vPosition, (Normal + Side1 + Side2) * _fSize);
		XMStoreFloat3(&Vertex[2].m_vNormal, Normal);
		Vertex[2].m_vTexCoord = TextureCoordinates[2];
		//VertexBufferData.push_back(VertexData);

		XMStoreFloat3(&Vertex[3].m_vPosition, (Normal + Side1 - Side2) * _fSize);
		XMStoreFloat3(&Vertex[3].m_vNormal, Normal);
		Vertex[3].m_vTexCoord = TextureCoordinates[3];
		//VertexBufferData.push_back(VertexData);

		// Calculate Tangent and Bitangent
		// First face: 012
		XMFLOAT3 vDummy;
		ComputeTangentBasis(Vertex[0].m_vPosition, Vertex[1].m_vPosition, Vertex[2].m_vPosition, Vertex[0].m_vTexCoord, Vertex[1].m_vTexCoord, Vertex[2].m_vTexCoord, Vertex[0].m_vNormal, Vertex[0].m_vTangent, vDummy);//Vertex[0].m_vBiTangent);
		//ComputeTangentBasis(Vertex[1].m_vPosition, Vertex[2].m_vPosition, Vertex[0].m_vPosition, Vertex[1].m_vTexCoord, Vertex[2].m_vTexCoord, Vertex[0].m_vTexCoord, Vertex[1].m_vNormal, Vertex[1].m_vTangent, Vertex[1].m_vBiTangent);
		//ComputeTangentBasis(Vertex[2].m_vPosition, Vertex[0].m_vPosition, Vertex[1].m_vPosition, Vertex[2].m_vTexCoord, Vertex[0].m_vTexCoord, Vertex[1].m_vTexCoord, Vertex[2].m_vNormal, Vertex[2].m_vTangent, Vertex[2].m_vBiTangent);

		// Second face: 023
		//ComputeTangentBasis(Vertex[3].m_vPosition, Vertex[0].m_vPosition, Vertex[2].m_vPosition, Vertex[3].m_vTexCoord, Vertex[0].m_vTexCoord, Vertex[2].m_vTexCoord, Vertex[3].m_vNormal, Vertex[3].m_vTangent, Vertex[3].m_vBiTangent);

		Vertex[1].m_vTangent = Vertex[0].m_vTangent;
		Vertex[2].m_vTangent = Vertex[0].m_vTangent;
		Vertex[3].m_vTangent = Vertex[0].m_vTangent;

		// Save in VertexBufferData
		VertexBufferData.push_back(Vertex[0]);
		VertexBufferData.push_back(Vertex[1]);
		VertexBufferData.push_back(Vertex[2]);
		VertexBufferData.push_back(Vertex[3]);
	}

	pMesh = new MeshDX11( (uint8_t*)&VertexBufferData[0], IndexBufferData, uVPNTDescSize, static_cast<uint32_t>(VertexBufferData.size()));
	
	pMesh->SetPrimitiveTopology(PrimitiveTopology_TriangleList);

	return pMesh;
#pragma endregion

#else

#pragma region SimpleCube

	//VertexPosition Vertices[8] =
	//{
	//	{ XMFLOAT4( -_fSize, -_fSize, -_fSize, 1.0f) }, // 0
	//	{ XMFLOAT4( -_fSize,  _fSize, -_fSize, 1.0f) }, // 1
	//	{ XMFLOAT4(  _fSize,  _fSize, -_fSize, 1.0f) }, // 2
	//	{ XMFLOAT4(  _fSize, -_fSize, -_fSize, 1.0f) }, // 3
	//	{ XMFLOAT4( -_fSize, -_fSize,  _fSize, 1.0f) }, // 4
	//	{ XMFLOAT4( -_fSize,  _fSize,  _fSize, 1.0f) }, // 5
	//	{ XMFLOAT4(  _fSize,  _fSize,  _fSize, 1.0f) }, // 6
	//	{ XMFLOAT4(  _fSize, -_fSize,  _fSize, 1.0f) }  // 7
	//};

	////uint32_t Indicies[36] =
	////{
	////	0, 1, 2, 0, 2, 3,
	////	4, 6, 5, 4, 7, 6,
	////	4, 5, 1, 4, 1, 0,
	////	3, 2, 6, 3, 6, 7,
	////	1, 5, 6, 1, 6, 2,
	////	4, 0, 3, 4, 3, 7
	////};

	//std::vector<uint32_t> IndexBuffer =
	//{
	//	0, 1, 2, 0, 2, 3,
	//	4, 6, 5, 4, 7, 6,
	//	4, 5, 1, 4, 1, 0,
	//	3, 2, 6, 3, 6, 7,
	//	1, 5, 6, 1, 6, 2,
	//	4, 0, 3, 4, 3, 7
	//};


	//pMesh = new MeshDX11( (uint8_t*)Vertices, IndexBuffer, sizeof(VertexPosition), 8);
	//
	//pMesh->SetPrimitiveTopology(PrimitiveTopology_TriangleList);

	//return pMesh;
#pragma endregion

#endif
}
//---------------------------------------------------------------------------------------------------
//void MeshDX11::GenerateTriangle(float _fSize, _Out_ std::vector<T>& _VertexBufferData, _Out_ std::vector<uint32_t>& _IndexBufferData)
//{
//	_VertexBufferData.clear();
//	_IndexBufferData.clear();
//
//	_fSize *= 0.5f;
//
//	T VertexData = T();
//	FillVertexData(VertexData, XMFLOAT3(-_fSize, -_fSize, 0.0f));
//	//VertexData.m_vVertexPosition = XMFLOAT3(-_fSize, -_fSize, 0.0f);
//	_VertexBufferData.push_back(VertexData);
//
//	VertexData.Clear();
//	FillVertexData(VertexData, XMFLOAT3(0.f, _fSize, 0.0f));
//	//VertexData.m_vVertexPosition = XMFLOAT3(0.f, _fSize, 0.0f);
//	_VertexBufferData.push_back(VertexData);
//
//	VertexData.Clear();
//	FillVertexData(VertexData, XMFLOAT3(_fSize, -_fSize, 0.0f));
//	//VertexData.m_vVertexPosition = XMFLOAT3(_fSize, -_fSize, 0.0f);
//	_VertexBufferData.push_back(VertexData);
//
//	//_VertexBufferData.push_back(VertexPosition(XMFLOAT3(-_fSize, -_fSize, 0.0f)));
//	//_VertexBufferData.push_back(VertexPosition(XMFLOAT3(0.f, _fSize, 0.0f)));
//	//_VertexBufferData.push_back(VertexPosition(XMFLOAT3(_fSize, -_fSize, 0.0f)));
//
//	_IndexBufferData.push_back(0);
//	_IndexBufferData.push_back(1);
//	_IndexBufferData.push_back(2);
//}
//---------------------------------------------------------------------------------------------------
std::shared_ptr<MeshDX11> MeshDX11::GenerateSphere(float _fDiameter, size_t _uTessellation)
{
	std::vector<VertexPositionNormalTexture> VertexBufferData;
	std::vector<uint32_t> IndexBufferData;

	const uint32_t uVPNTDescSize = sizeof(VertexPositionNormalTexture);

	_fDiameter *= 0.5f;

	if (_uTessellation < 3) 
	{
		HFATAL("Tesselation parameter to small (<3)!");
		_uTessellation = 3;
	}
	
	size_t uVerticalSegments = _uTessellation;
	size_t uHorizontalSegments = _uTessellation * 2;

	// Create rings of Vertices at progressively higher latitudes.
	for (size_t i = 0; i <= uVerticalSegments; ++i)
	{
		float fV = 1 - static_cast<float>(i) / uVerticalSegments;

		float fLatitude = (i * XM_PI / uVerticalSegments) - XM_PIDIV2;
		float fdY, fdXZ;

		XMScalarSinCos(&fdY, &fdXZ, fLatitude);

		// Create a single ring of Vertices at this latitude.
		for (size_t j = 0; j <= uHorizontalSegments; ++j)
		{
			float fU = static_cast<float>(j) / uHorizontalSegments;

			float fLongitude = j * XM_2PI / uHorizontalSegments;
			float fdX, fdZ;

			XMScalarSinCos(&fdX, &fdZ, fLongitude);

			fdX *= fdXZ;
			fdZ *= fdXZ;

			XMVECTOR vNormal = XMVectorSet(fdX, fdY, fdZ, 0);
			XMVECTOR vTextureCoordinate = XMVectorSet(fU, fV, 0, 0);

			VertexBufferData.push_back(VertexPositionNormalTexture(vNormal * _fDiameter, vNormal, vTextureCoordinate));
		}
	}

	// Fill the index buffer with triangles joining each pair of latitude rings.
	size_t uStride = uHorizontalSegments + 1;

	for (size_t i = 0; i < uVerticalSegments; i++)
	{
		for (size_t j = 0; j <= uHorizontalSegments; j++)
		{
			size_t uNextI = i + 1;
			size_t uNextJ = (j + 1) % uStride;

			IndexBufferData.push_back(i * uStride + j);
			IndexBufferData.push_back(uNextI * uStride + j);
			IndexBufferData.push_back(i * uStride + uNextJ);

			IndexBufferData.push_back(i * uStride + uNextJ);
			IndexBufferData.push_back(uNextI * uStride + j);
			IndexBufferData.push_back(uNextI * uStride + uNextJ);
		}
	}

	std::shared_ptr<MeshDX11> pMesh = std::make_shared<MeshDX11>(); //(/*(uint8_t*)&VertexBufferData[0], IndexBufferData, sizeof(VertexPositionNormalTexture), VertexBufferData.size()*/);

	pMesh->SetIndexBufferData<uint32_t>(IndexBufferData);
	pMesh->SetVertexBufferData<VertexPositionNormalTexture>(VertexBufferData);
	pMesh->SetPrimitiveTopology(PrimitiveTopology_TriangleList);

	return pMesh;
}
//---------------------------------------------------------------------------------------------------
// TODO: delete this function, is moved to xmmath
void MeshDX11::ComputeTangentBasis(	const XMFLOAT3& _vP0, const XMFLOAT3& _vP1, const XMFLOAT3& _vP2, 
									const XMFLOAT2& _vUV0, const XMFLOAT2& _vUV1, const XMFLOAT2& _vUV2,
									XMFLOAT3 &_vNormal, XMFLOAT3 &_vTangent, XMFLOAT3 &_vBitangent )
{
	XMVECTOR vP0 = XMLoadFloat3(&_vP0);
	XMVECTOR vP1 = XMLoadFloat3(&_vP1);
	XMVECTOR vP2 = XMLoadFloat3(&_vP2);
	XMVECTOR UV0 = XMLoadFloat2(&_vUV0);
	XMVECTOR UV1 = XMLoadFloat2(&_vUV1);
	XMVECTOR UV2 = XMLoadFloat2(&_vUV2);

	XMVECTOR e1 = vP1 - vP0;
	XMVECTOR e0 = vP2 - vP0;

	XMVECTOR vNormal = XMVector3Cross(e0, e1);

	//using Eric Lengyel's approach with a few modifications
	//from Mathematics for 3D Game Programmming and Computer Graphics
	// want to be able to transform a vector in Object Space to Tangent Space
	// such that the x-axis cooresponds to the 's' direction and the
	// y-axis corresponds to the 't' direction, and the z-axis corresponds
	// to <0,0,1>, straight up out of the texture map

	//let P = v1 - v0
	XMVECTOR P = vP1 - vP0;
	//let Q = v2 - v0
	XMVECTOR Q = vP2 - vP0;
	
	float s1 = XMVectorGetX(UV1) - XMVectorGetX(UV0); //UV1.x - UV0.x;
	float t1 = XMVectorGetY(UV1) - XMVectorGetY(UV0); //UV1.y - UV0.y;
	float s2 = XMVectorGetX(UV2) - XMVectorGetX(UV0); //UV2.x - UV0.x;
	float t2 = XMVectorGetY(UV2) - XMVectorGetY(UV0); //UV2.y - UV0.y;


	//we need to solve the equation
	// P = s1*T + t1*B
	// Q = s2*T + t2*B
	// for T and B

	//this is a linear system with six unknowns and six equations, for TxTyTz BxByBz
	//[px,py,pz] = [s1,t1] * [Tx,Ty,Tz]
	// qx,qy,qz     s2,t2     Bx,By,Bz

	//multiplying both sides by the inverse of the s,t matrix gives
	//[Tx,Ty,Tz] = 1/(s1t2-s2t1) *  [t2,-t1] * [px,py,pz]
	// Bx,By,Bz                      -s2,s1	    qx,qy,qz  

	//solve this for the unormalized T and B to get from tangent to object space

	float tmp = 0.0f;
	if (fabsf(s1*t2 - s2*t1) <= 0.0001f)
	{
		tmp = 1.0f;
	}
	else
	{
		tmp = 1.0f / (s1*t2 - s2*t1);
	}

	XMFLOAT3 vP;
	XMStoreFloat3(&vP, P);
	XMFLOAT3 vQ;
	XMStoreFloat3(&vQ, Q);

	_vTangent.x = (t2*vP.x - t1*vQ.x) *tmp;
	_vTangent.y = (t2*vP.y - t1*vQ.y) *tmp;
	_vTangent.z = (t2*vP.z - t1*vQ.z) *tmp;

	_vBitangent.x = (s1*vQ.x - s2*vP.x) *tmp;
	_vBitangent.y = (s1*vQ.y - s2*vP.y) *tmp;
	_vBitangent.z = (s1*vQ.z - s2*vP.z) *tmp;

	vNormal = XMVector3Normalize(vNormal);
	XMStoreFloat3(&_vNormal, vNormal);

	XMStoreFloat3(&_vTangent, XMVector3Normalize(XMLoadFloat3(&_vTangent)));
	XMStoreFloat3(&_vBitangent, XMVector3Normalize(XMLoadFloat3(&_vBitangent)));
}