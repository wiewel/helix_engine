//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef DEPTHSTENCILSTATEDX11_H
#define DEPTHSTENCILSTATEDX11_H

#include "DepthStencilStatePoolDX11.h"
#include "Display\ViewDefines.h"

namespace Helix
{
	namespace Display
	{
		class DepthStencilStateDX11 : public TDX11DepthStencilStateRef
		{
		public:
			HDEBUGNAME("DepthStencilStateDX11");

			// default constructor
			constexpr DepthStencilStateDX11() noexcept : TDX11DepthStencilStateRef(nullptr), m_uStencilRef(0) {}
			constexpr DepthStencilStateDX11(std::nullptr_t) noexcept : TDX11DepthStencilStateRef(nullptr), m_uStencilRef(0) {}

			DepthStencilStateDX11(const DepthStencilDesc& _DepthStencilDesc, const char* _pDbgName = nullptr);

			// copy constructor, Increments the ref count
			DepthStencilStateDX11(const DepthStencilStateDX11& _Other);

			~DepthStencilStateDX11();

			// Assignment,  Increments the ref count
			DepthStencilStateDX11& operator=(const DepthStencilStateDX11& _Other);

			// decrements ref count
			DepthStencilStateDX11& operator=(std::nullptr_t);

			bool IsInitialized() const;

			void SetStencilRef(uint32_t _uStencilRef);
			uint32_t GetStencilRef() const;		
			const DepthStencilDesc& GetDescription() const;

		private:
			uint32_t m_uStencilRef = 0u;
			DepthStencilDesc m_Description = {};
		};

		inline void DepthStencilStateDX11::SetStencilRef(uint32_t _uStencilRef) { m_uStencilRef = _uStencilRef; }
		inline uint32_t DepthStencilStateDX11::GetStencilRef() const { return m_uStencilRef; }
		inline const DepthStencilDesc& DepthStencilStateDX11::GetDescription() const { return m_Description; }	
		inline bool DepthStencilStateDX11::IsInitialized() const { return this->operator bool(); }

	} // Display
} // Helix

#endif // DepthStencilStateDX11_H