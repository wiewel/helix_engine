//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef MESHPOOLDX11_H
#define MESHPOOLDX11_H

#include "DataStructures\TplHashPool.h"
#include "hlx\src\Singleton.h"
#include "Util\UniqueID.h"


namespace Helix
{
	namespace Display
	{
		struct MeshClusterReferenceDX11;
		class MeshClusterDX11;
		//struct MeshClusterDesc;

		using TDX11MeshPool = Datastructures::TplRefHashPool< uint64_t, MeshClusterDesc, MeshClusterReferenceDX11>;
		class MeshPoolDX11 : public TDX11MeshPool, public hlx::Singleton<MeshPoolDX11>
		{
		public:
			HDEBUGNAME("MeshPoolDX11");

			MeshPoolDX11();
			~MeshPoolDX11();

			uint64_t HashFunction(const MeshClusterDesc& _Desc) final;

			uint64_t GetDefaultKey() final;
		private:

			MeshClusterReferenceDX11* CreateFromDesc(const MeshClusterDesc& _Desc, const uint64_t& _Hash) final;

			MeshClusterReferenceDX11* CreateNew(const MeshClusterDesc& _Desc);
			MeshClusterReferenceDX11* LoadFromFile(const MeshClusterDesc& _Desc);

		private:
			UniqueID<uint64_t> m_ID;

			static MeshClusterDX11 m_ErrorCube;
		};

		using TDX11MeshClusterRef = Datastructures::TplHashPoolReference<MeshPoolDX11>;
	} // Display
} // Helix

#endif //MESHPOOLDX11_H