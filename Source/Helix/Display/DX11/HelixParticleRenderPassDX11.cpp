//   Copyright 2020 Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "HelixParticleRenderPassDX11.h"
#include "Display\Shaders\Particle.hlsl"

using namespace Helix;
using namespace Helix::Display;
using namespace Helix::Scene;
using namespace Helix::Datastructures;

//---------------------------------------------------------------------------------------------------

HelixParticleRenderPassDX11::HelixParticleRenderPassDX11(DefaultInitializerType) : RenderPassDX11(DefaultInit, "Particle")
{
}

//---------------------------------------------------------------------------------------------------
HelixParticleRenderPassDX11::HelixParticleRenderPassDX11(const std::string& _sInstanceName) :
	RenderPassDX11("Particle", _sInstanceName, { &m_AlbedoMap, &m_DepthTexture }, { &m_RenderTarget }, {&m_CBPerCamera, &m_CBPerCamera})
{
}
//---------------------------------------------------------------------------------------------------
HelixParticleRenderPassDX11::~HelixParticleRenderPassDX11()
{
}
//---------------------------------------------------------------------------------------------------
bool HelixParticleRenderPassDX11::OnInitalize(const hlx::TextToken& _CustomVariables)
{
	// Add RasterizerState
	RasterizerDesc RasDesc = {};
	RasDesc.m_kFillMode = FillMode_Solid;
	RasDesc.m_kCullMode = CullMode_None;
	RasDesc.m_bFrontCounterClockwise = false;

	SetRasterizerState(RasDesc);

	// Add BlendState
	RenderTargetBlendDesc RTBlendDesc = {};
	RTBlendDesc.m_bBlendEnable = true;
	RTBlendDesc.m_kSrcBlend = BlendVariable_Src_Alpha;
	RTBlendDesc.m_kDestBlend = BlendVariable_One;
	RTBlendDesc.m_kBlendOp = BlendOperation_Add;
	RTBlendDesc.m_kSrcBlendAlpha = BlendVariable_Zero;
	RTBlendDesc.m_kDestBlendAlpha = BlendVariable_Zero;
	RTBlendDesc.m_kBlendOpAlpha = BlendOperation_Add;
	RTBlendDesc.m_uRenderTargetWriteMask = Channel_All;

	BlendDesc AlphaBlendDesc = {};
	AlphaBlendDesc.m_bAlphaToCoverageEnable = false;
	AlphaBlendDesc.m_RenderTargets[0] = RTBlendDesc;
	AlphaBlendDesc.m_uSampleMask = 0xffffffff;
	AlphaBlendDesc.fBlendFactor = { 0.0f,0.0f,0.0f,0.0f };

	SetBlendState(AlphaBlendDesc);

	// Add DepthStencilState
	DepthStencilDesc DepthDesc = {};
	DepthDesc.m_bDepthEnable = true;
	DepthDesc.m_kDepthWriteMask = DepthWriteMask_Zero;
	DepthDesc.m_kDepthFunc = ComparisonFunction_Less;
	DepthDesc.m_bStencilEnable = false;

	SetDepthStencilState(DepthDesc, "Particle");

	// Default Parameter
	ClearRenderTargets(false);
	//SetDepthStencilViewReadOnly(true); // TODO: add member variables of relevant State Descriptions to RenderPassDX11 and make ReadOnlyDSV bool dependent on "DepthDesc.m_kDepthWriteMask = DepthWriteMask_Zero;"

	return m_bInitialized;
}
//---------------------------------------------------------------------------------------------------
bool HelixParticleRenderPassDX11::OnPerCamera(const CameraComponent& _Camera)
{
	SetCameraConstants(m_CBPerCamera, _Camera.GetRenderingProperties());

	return true;
}
//---------------------------------------------------------------------------------------------------
bool HelixParticleRenderPassDX11::OnPerObject(const RenderObjectDX11& _RenderObject, const MaterialDX11& _Material)
{
	if (_Material.operator bool() == false)
	{
		return false;
	}
	
	const MaterialProperties& MaterialProps = _Material.GetProperties();

	m_CBParticle->vSize = _RenderObject.GetRenderingScale().xy;
	m_CBParticle->vColorScale = Math::float4(MaterialProps.vAlbedo, 1); // keep alpha

	if (_Material.GetAlbedoMap() != nullptr)
	{
		m_AlbedoMap.SetTexture(_Material.GetAlbedoMap());
	}

	return true;
}
//---------------------------------------------------------------------------------------------------
std::shared_ptr<RenderPassDX11> HelixParticleRenderPassDX11::CreatePass(const std::string& _sInstanceName) const
{
	return std::make_shared<HelixParticleRenderPassDX11>(_sInstanceName);
}
//---------------------------------------------------------------------------------------------------