//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef DEFERREDRENDERERDX11_H
#define DEFERREDRENDERERDX11_H

#include "TextureVariantsDX11.h"

namespace Helix
{
	namespace Display
	{
		// forward decls
		class RenderProcedureDX11;

		class HelixShadowMapRenderPassDX11;
		class HelixDepthPrePassRenderPassDX11;
		class HelixDeferredSkyboxRenderPassDX11;
		class HelixParticleRenderPassDX11;
		class HelixSolidRenderPassDX11;
		class HelixVerticalBlurRenderPassDX11;
		class HelixHorizontalBlurRenderPassDX11;
		class HelixSimpleDoFRenderPassDX11;
		class HelixFullscreenTextureRenderPassDX11;
		class HelixSSAORenderPassDX11;
		class HelixDeferredMergeRenderPassDX11;
		class HelixDeferredLightingRenderPassDX11;
		class HelixOceanRenderPassDX11;
		class HelixDebugRenderPassDX11;
		class HelixGizmoRenderPassDX11;
		class HelixGlitchRenderPassDX11;

		class HelixParaboloidShadowRenderPassDX11;
		class HelixProjectedShadowRenderPassDX11;

		class HelixScaleRenderPassDX11;
		class HelixLuminanceRenderPassDX11;
		class HelixLuminanceAdaptionRenderPassDX11;
		class HelixBloomRenderPassDX11;
		class HelixHDRCompositeRenderPassDX11;
		class HelixOceanRenderPassDX11;

		class DeferredRendererDX11
		{
		public:
			HDEBUGNAME("DeferredRendererDX11");
			DeferredRendererDX11();
			~DeferredRendererDX11();

			bool Initialize();

			void SetDebugMode(bool _bDebug);
			void EnableDepthOfField(bool _bEnableDoF);
			void EnableHDR(bool _bHDR);

			void SetSolidRenderPass(std::shared_ptr<HelixSolidRenderPassDX11> _pSolidPass);

		private:
			void InitializeTextures();

			void InitializePasses();
			bool InitializeProcedures();

			void GenerateRandomTexture1D();
			void GenerateRandomTexture2D();

		private:

			/// Procedures
			std::shared_ptr<RenderProcedureDX11> m_RenderProcedure = nullptr;
			std::shared_ptr<RenderProcedureDX11> m_LightingProcedure = nullptr;
			std::shared_ptr<RenderProcedureDX11> m_PostProcessingProcedure = nullptr;
			std::shared_ptr<RenderProcedureDX11> m_MergeProcedure = nullptr;
			std::shared_ptr<RenderProcedureDX11> m_GizmoProcedure = nullptr;
			//std::shared_ptr<RenderProcedureDX11> m_GUIProcedure = nullptr;

			/// Passes
			std::shared_ptr<HelixShadowMapRenderPassDX11> m_pRenderPassShadow = nullptr;
			//std::shared_ptr<HelixDepthPrePassRenderPassDX11> m_pRenderPassDepth = nullptr;
			std::shared_ptr<HelixParaboloidShadowRenderPassDX11> m_pRenderPassPointShadows = nullptr;
			std::shared_ptr<HelixProjectedShadowRenderPassDX11> m_pRenderPassSpotShadows = nullptr;

			std::shared_ptr<HelixSolidRenderPassDX11> m_pRenderPassSolid = nullptr;
			std::shared_ptr<HelixDeferredSkyboxRenderPassDX11> m_pRenderPassSkybox = nullptr;

			//std::shared_ptr<HelixParticleRenderPassDX11> m_pRenderPassParticle = nullptr;
			std::shared_ptr<HelixHorizontalBlurRenderPassDX11> m_pRenderPassHorizontalBlur[7];
			std::shared_ptr<HelixVerticalBlurRenderPassDX11> m_pRenderPassVerticalBlur[7];
			std::shared_ptr<HelixSimpleDoFRenderPassDX11> m_pRenderPassSimpleDoF = nullptr;
			std::shared_ptr<HelixGlitchRenderPassDX11> m_pRenderPassGlitch = nullptr;

			//std::shared_ptr<HelixFullscreenTextureRenderPassDX11> m_pRenderPass2DTex = nullptr;
			std::shared_ptr<HelixSSAORenderPassDX11> m_pRenderPassSSAO = nullptr;
			std::shared_ptr<HelixDeferredLightingRenderPassDX11> m_pRenderPassLighting = nullptr;
			std::shared_ptr<HelixDeferredMergeRenderPassDX11> m_pRenderPassMerge = nullptr;
			//std::shared_ptr<HelixOceanRenderPassDX11> m_pRenderPassOcean = nullptr;

			std::shared_ptr<HelixLuminanceRenderPassDX11> m_pRenderPassLuminance = nullptr;
			std::shared_ptr<HelixLuminanceAdaptionRenderPassDX11> m_pRenderPassLuminanceAdaption = nullptr;

			std::shared_ptr<HelixBloomRenderPassDX11> m_pRenderPassBloom = nullptr;
			std::shared_ptr<HelixScaleRenderPassDX11> m_pRenderPassScale[6];

			std::shared_ptr<HelixHDRCompositeRenderPassDX11> m_pRenderPassHDR = nullptr;

			std::shared_ptr<HelixDebugRenderPassDX11> m_pRenderPassDebug = nullptr;
			std::shared_ptr<HelixGizmoRenderPassDX11> m_pRenderPassGizmo = nullptr;
			std::shared_ptr<HelixOceanRenderPassDX11> m_pRenderPassOcean = nullptr;

			/// Textures
			ImmutableTextureDX11 m_RandomTexture1D = nullptr;
			ImmutableTextureDX11 m_RandomTexture2D = nullptr;

			BackBufferTextureDX11* m_pBackBuffer = nullptr;
			DepthStencilTextureDX11 m_DepthBuffer = nullptr;
			DepthStencilTextureDX11 m_ShadowMap = nullptr;
			DepthStencilTextureDX11 m_GizmoDepthBuffer = nullptr;

			DepthStencilTextureDX11 m_PointShadowDepth = nullptr;
			DepthStencilTextureDX11 m_SpotShadowDepth = nullptr;

			//RenderTargetTextureDX11 m_ParaboloidShadowTarget = nullptr;
			RenderTargetTextureDX11 m_LinearDepthTarget = nullptr;
			RenderTargetTextureDX11 m_AlbedoTarget = nullptr;
			RenderTargetTextureDX11 m_NormalWSTarget = nullptr;

			//RenderTargetTextureDX11 m_NormalDebugTarget = nullptr;

			RenderTargetTextureDX11 m_MetallicTarget = nullptr;
			RenderTargetTextureDX11 m_RoughnessTarget = nullptr;

			RenderTargetTextureDX11 m_DiffuseLightMapTarget = nullptr;
			RenderTargetTextureDX11 m_SpecularLightMapTarget = nullptr;
			RenderTargetTextureDX11 m_EmissiveLightMapTarget = nullptr;

			RenderTargetTextureDX11 m_BlurTarget = nullptr;
			RenderTargetTextureDX11 m_SSAOTarget = nullptr;
			//RenderTargetTextureDX11 m_SSAOBentTarget = nullptr;

			RenderTargetTextureDX11 m_LitSceneHDR = nullptr;

			/// PostProcessing Textures
			/// -> HDR
			RenderTargetTextureDX11 m_CurrentLuminanceTarget = nullptr;
			RenderTargetTextureDX11 m_AdaptiveLuminanceTarget[2];

			RenderTargetTextureDX11 m_BloomTarget = nullptr;
			RenderTargetTextureDX11 m_DownScaleTarget[4];
			RenderTargetTextureDX11 m_DownScaledBlurTarget[2];

			RenderTargetTextureDX11 m_PostEffectTarget = nullptr;

			bool m_bDebugShaders = false;
			bool m_bHDR = true;
			bool m_bAutoExposure = false;
			bool m_bShadowMap = false;
			bool m_bDepthOfField = true;
			bool m_bSSAO = true;

			bool m_bPostEffects = false;
		};

		inline void DeferredRendererDX11::SetDebugMode(bool _bDebug) { m_bDebugShaders = _bDebug; }
		inline void DeferredRendererDX11::EnableDepthOfField(bool _bEnableDoF) { m_bDepthOfField = _bEnableDoF; }
		inline void DeferredRendererDX11::EnableHDR(bool _bHDR) { m_bHDR = _bHDR; }

	} // Display
} // Helix

#endif //DEFERREDRENDERERDX11_H