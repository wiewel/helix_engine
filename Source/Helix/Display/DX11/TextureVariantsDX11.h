//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef TEXTUREVARIANTSDX11_H
#define TEXTUREVARIANTSDX11_H

#include "TextureDX11.h"

namespace Helix
{
	namespace Display
	{
		//---------------------------------------------------------------------------------------------------
		// RenderTargetTextureDX11 are not pooled!
		class RenderTargetTextureDX11 : public TextureDX11
		{
		public:
			RenderTargetTextureDX11(uint32_t _uWidth,
									uint32_t _uHeight,
									const std::string& _sName,
									PixelFormat _kPixelFormat = PixelFormat_B8G8R8A8_UNorm,
									bool _bShaderResource = true,
									bool _bFullMipChain = false,
									uint32_t _uArraySize = 1u);

			// RWTexture constructor
			RenderTargetTextureDX11(
				uint32_t _uWidth,
				const std::string& _sName,
				PixelFormat _kPixelFormat = PixelFormat_B8G8R8A8_UNorm,
				bool _bShaderResource = true,
				uint32_t _uHeight = 0u,
				uint32_t _uDepth = 0u, // can be 0
				uint32_t _uArraySize = 1u);

			~RenderTargetTextureDX11();

			inline constexpr RenderTargetTextureDX11() noexcept : TextureDX11(nullptr){}
			inline constexpr RenderTargetTextureDX11(std::nullptr_t) noexcept : TextureDX11(nullptr){}
			inline RenderTargetTextureDX11& operator=(std::nullptr_t) { TextureDX11::operator=(nullptr); return *this; }

			RenderTargetTextureDX11(const RenderTargetTextureDX11& _Other);

			// this copy constructor asserts if the underlying Description does not match RenderTarget specs
			RenderTargetTextureDX11(const TextureDX11& _Other);

			RenderTargetTextureDX11& operator=(const RenderTargetTextureDX11& _Other);

			// this copy operator asserts if the underlying Description does not match RenderTarget specs
			RenderTargetTextureDX11& operator=(const TextureDX11& _Other);

			void GenerateMips();

			inline void SetDefaultClearColor(const Math::float4& _vClearColor){	m_pReference->m_vClearColor = _vClearColor.getSaturated();}


			// if RT is a RWTexture and _pUAVUIntClearData is valid 4 uints will be used to clear the UAV,  m_vClearColor (4 Floats) otherwise
			// if its a regular RenderTarget m_vClearColor will be used
			void Clear(const Math::uint4* _pUAVUIntClearData = nullptr);

			inline const Viewport& GetViewport() const { return m_pReference->m_Viewport; }
			inline void SetViewport(const Viewport& _Viewport) { m_pReference->m_Viewport = _Viewport; };

			void OnResize(uint32_t _uWidth, uint32_t _uHeight, uint32_t _uDepth) final;
			
			inline bool IsRWTexture() const { return m_pReference->m_bIsRWTexture; }
		};
		//---------------------------------------------------------------------------------------------------
		// DepthStencilTextureDX11 are not pooled!
		class DepthStencilTextureDX11 : public TextureDX11
		{
		public:
			DepthStencilTextureDX11(uint32_t _uWidth,
									uint32_t _uHeight,
									const std::string& _sName = "DepthStencil",
									PixelFormat _kTypelessPixelFormat = PixelFormat_R32_Typeless,
									bool _bShaderResource = true,
									uint32_t _uArraySize = 1u); // Resource is typeless

			~DepthStencilTextureDX11();

			inline constexpr DepthStencilTextureDX11() noexcept : TextureDX11(nullptr) {}
			inline constexpr DepthStencilTextureDX11(std::nullptr_t) noexcept : TextureDX11(nullptr) {}
			inline DepthStencilTextureDX11& operator=(std::nullptr_t) { TextureDX11::operator=(nullptr); }

			DepthStencilTextureDX11(const DepthStencilTextureDX11& _Other);
			// this copy constructor asserts if the underlying Description does not match DepthStencil specs
			DepthStencilTextureDX11(const TextureDX11& _Other);

			DepthStencilTextureDX11& operator=(const DepthStencilTextureDX11& _Other);
			// this copy operator asserts if the underlying Description does not match DepthStencil specs
			DepthStencilTextureDX11& operator=(const TextureDX11& _Other);
			
			inline void SetDefaultClearDepth(float _fClearDepth) { m_pReference->m_fClearDepth = _fClearDepth; }
			inline void SetDefaultClearStencil(uint8_t _uClearStencil) { m_pReference->m_uClearStencil = _uClearStencil; }

			void Clear();

			inline const Viewport& GetViewport() const { return m_pReference->m_Viewport; }
			void SetViewport(const Viewport& _Viewport) { m_pReference->m_Viewport = _Viewport; };

			void OnResize(uint32_t _uWidth, uint32_t _uHeight, uint32_t _uDepth) final;
		};
		//---------------------------------------------------------------------------------------------------
		// BackBufferTextureDX11 is not pooled!
		class BackBufferTextureDX11 : public RenderTargetTextureDX11, public hlx::Singleton<BackBufferTextureDX11>
		{
			friend class RenderViewDX11;
		public:

			inline constexpr BackBufferTextureDX11() noexcept : RenderTargetTextureDX11(nullptr){}
			~BackBufferTextureDX11();

			BackBufferTextureDX11(const RenderTargetTextureDX11&) = delete;
			BackBufferTextureDX11(const BackBufferTextureDX11&) = delete;
			BackBufferTextureDX11(const TextureDX11&) = delete;

			BackBufferTextureDX11(std::nullptr_t) = delete;
			BackBufferTextureDX11& operator=(std::nullptr_t) = delete;
			
			BackBufferTextureDX11& operator=(const BackBufferTextureDX11&) = delete;
			BackBufferTextureDX11& operator=(const RenderTargetTextureDX11&) = delete;
			BackBufferTextureDX11& operator=(const TextureDX11&) = delete;

			// visible only to RenderViewDX11
		private:
			//Destroys old backbuffer (calls Release)
			void Destroy();

			// Sets new backbuffer (make sure to Destroy the old one before!)
			bool Set(ID3D11Texture2D* _pBackBuffer);
		};
		//---------------------------------------------------------------------------------------------------
		// ImmutableTextureDX11 are ALWAYS pooled! 
		class ImmutableTextureDX11 : public TextureDX11
		{
		public:
			inline constexpr ImmutableTextureDX11() noexcept : TextureDX11(nullptr) {}
			inline constexpr ImmutableTextureDX11(std::nullptr_t) noexcept : TextureDX11(nullptr) {}
			inline ImmutableTextureDX11& operator=(std::nullptr_t) { TextureDX11::operator=(nullptr); return *this; }

			ImmutableTextureDX11(const std::string& _sFilePath,
								 uint32_t _uMipLevels = 0u,
								 bool _bForceSRgb = false,
				bool _bUseDefaultReferenceIfFailed = true);

			ImmutableTextureDX11(uint32_t _uWidth,
								 uint32_t _uHeight, 
								 const std::string& _sName,
								 const BufferSubresourceData& _InitialData,
								 PixelFormat _kFormat = PixelFormat_R32G32B32A32_Float,
								 ResourceDimension _kResourceDimension = ResourceDimension_Texture2D,
								 uint32_t _uMipLevels = 1,
								 bool _bForceSRgb = false);

			ImmutableTextureDX11(const ImmutableTextureDX11& _Other);
			// this copy constructor asserts if the underlying Description does not match DepthStencil specs
			ImmutableTextureDX11(const TextureDX11& _Other);

			ImmutableTextureDX11& operator=(const ImmutableTextureDX11& _Other);
			// this copy operator asserts if the underlying Description does not match DepthStencil specs
			ImmutableTextureDX11& operator=(const TextureDX11& _Other);

			~ImmutableTextureDX11();
		};
		//---------------------------------------------------------------------------------------------------
		// TextureCubeDX11 are ALWAYS pooled! 
		class TextureCubeDX11 : public TextureDX11
		{
		public:
			TextureCubeDX11(const std::string& _sFilePath,
				uint32_t _uMipLevels = 0u,
				bool _bForceSRgb = false,
				bool _bUseDefaultReferenceIfFailed = true);

			~TextureCubeDX11();

			inline constexpr TextureCubeDX11() noexcept : TextureDX11(nullptr) {}
			inline constexpr TextureCubeDX11(std::nullptr_t) noexcept : TextureDX11(nullptr) {}
			inline TextureCubeDX11& operator=(std::nullptr_t) { TextureDX11::operator=(nullptr); return *this; }
		};
		//---------------------------------------------------------------------------------------------------
		// DefaultTextureDX11 are not pooled!
		class DefaultTextureDX11 : public TextureDX11
		{
		public:
			DefaultTextureDX11( const TextureDesc& _Desc,
								const std::string& _sName);

			DefaultTextureDX11( uint32_t _uWidth,
								uint32_t _uHeight,
								const std::string& _sName,
								PixelFormat _kPixelFormat = PixelFormat_B8G8R8A8_UNorm);

			~DefaultTextureDX11();

			inline constexpr DefaultTextureDX11() noexcept : TextureDX11(nullptr){}
			inline constexpr DefaultTextureDX11(std::nullptr_t) noexcept : TextureDX11(nullptr){}

			DefaultTextureDX11(const DefaultTextureDX11& _Texture);
			// this copy constructor asserts if the underlying Description does not match Default specs
			DefaultTextureDX11(const TextureDX11& _Texture);

			DefaultTextureDX11& operator=(const DefaultTextureDX11& _Other);
			// this copy operator asserts if the underlying Description does not match Default specs
			DefaultTextureDX11& operator=(const TextureDX11& _Other);

			inline DefaultTextureDX11& operator=(std::nullptr_t) { TextureDX11::operator=(nullptr);return *this;}

	
		public:
			bool CopyPixelData(const TextureDX11& _Other);
		};
		//---------------------------------------------------------------------------------------------------

	} // Display
} // Helix

#endif