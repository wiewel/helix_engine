//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef RASTERIZERSTATEPOOLDX11_H
#define RASTERIZERSTATEPOOLDX11_H

#include "DataStructures\TplHashPool.h"
#include "hlx\src\Singleton.h"

// forward decls
struct ID3D11RasterizerState;

namespace Helix
{
	namespace Display
	{
		// forward decls
		class RenderDeviceDX11;

		using TDX11RasterizerPool = Datastructures::TplRefHashPool< uint64_t, D3D11_RASTERIZER_DESC, ID3D11RasterizerState>;
		class RasterizerStatePoolDX11 : public TDX11RasterizerPool, public hlx::Singleton<RasterizerStatePoolDX11>
		{
		public:
			RasterizerStatePoolDX11();
			~RasterizerStatePoolDX11();
			
			uint64_t HashFunction(const D3D11_RASTERIZER_DESC& _Desc);

		private:

			ID3D11RasterizerState* CreateFromDesc(const D3D11_RASTERIZER_DESC& _Desc, const uint64_t& _Hash);

		private:
			RenderDeviceDX11* m_pRenderDevice = nullptr;
		};

		using TDX11RasterizerStateRef = Datastructures::TplHashPoolReference<RasterizerStatePoolDX11>;
	} // Display
} // Helix

#endif // RASTERIZERSTATEPOOLDX11_H