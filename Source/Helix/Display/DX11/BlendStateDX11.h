//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef BLENDSTATEDX11_H
#define BLENDSTATEDX11_H

#include "BlendStatePoolDX11.h"
#include "Display\ViewDefines.h"

namespace Helix
{
	namespace Display
	{
		class BlendStateDX11 : public TDX11BlendStateRef
		{
		public:
			HDEBUGNAME("BlendStateDX11");

			constexpr BlendStateDX11() noexcept : TDX11BlendStateRef(nullptr) {}
			constexpr BlendStateDX11(std::nullptr_t) noexcept : TDX11BlendStateRef(nullptr) {}

			BlendStateDX11(const BlendDesc& _BlendDesc, const char* _pDbgName = nullptr);
			~BlendStateDX11();

			BlendStateDX11(const BlendStateDX11& _Other);
			BlendStateDX11& operator=(const BlendStateDX11& _Other);

			bool IsInitialized() const;
			
			void SetBlendFactor(const Math::float4& _vBlendFactor);
			void SetSampleMask(const uint32_t& _uSampleMask);

			const BlendDesc& GetDescription() const;

		private:
			BlendDesc m_Description = {};
		};

		inline bool BlendStateDX11::IsInitialized() const { return m_pReference != nullptr; }

		inline void BlendStateDX11::SetBlendFactor(const Math::float4& _vBlendFactor)
		{
			m_Description.fBlendFactor[0] = _vBlendFactor.r;
			m_Description.fBlendFactor[1] = _vBlendFactor.g;
			m_Description.fBlendFactor[2] = _vBlendFactor.b;
			m_Description.fBlendFactor[3] = _vBlendFactor.a;
		}
		inline void BlendStateDX11::SetSampleMask(const uint32_t& _uSampleMask) { m_Description.m_uSampleMask = _uSampleMask; }
		inline const BlendDesc& BlendStateDX11::GetDescription() const	{ return m_Description;	}
	} // Display
} // Helix

#endif // BlendStateDX11_H