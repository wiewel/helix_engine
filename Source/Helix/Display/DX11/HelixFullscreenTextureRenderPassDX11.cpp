//   Copyright 2020 Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "HelixFullscreenTextureRenderPassDX11.h"
#include "Math\MathFunctions.h"

using namespace Helix;
using namespace Helix::Display;
using namespace Helix::Scene;
using namespace Helix::Datastructures;

//---------------------------------------------------------------------------------------------------

HelixFullscreenTextureRenderPassDX11::HelixFullscreenTextureRenderPassDX11(DefaultInitializerType) : RenderPassDX11(DefaultInit, "FullscreenTexture")
{
}

//---------------------------------------------------------------------------------------------------
HelixFullscreenTextureRenderPassDX11::HelixFullscreenTextureRenderPassDX11(const std::string& _sInstanceName) :
	RenderPassDX11("FullscreenTexture", _sInstanceName, { &m_FullscreenTexture }, { &m_RenderTarget }, {&m_CBPerCamera })
{
}
//---------------------------------------------------------------------------------------------------
HelixFullscreenTextureRenderPassDX11::~HelixFullscreenTextureRenderPassDX11()
{
}
//---------------------------------------------------------------------------------------------------
bool HelixFullscreenTextureRenderPassDX11::OnInitalize(const hlx::TextToken& _CustomVariables)
{
	// Add RasterizerState
	RasterizerDesc RasDesc = {};
	RasDesc.m_kFillMode = FillMode_Solid;
	RasDesc.m_kCullMode = CullMode_None;
	RasDesc.m_bFrontCounterClockwise = false;

	SetRasterizerState(RasDesc);

	// Add BlendState
	RenderTargetBlendDesc RTBlendDesc = {};
	RTBlendDesc.m_bBlendEnable = false;
	RTBlendDesc.m_kSrcBlend = BlendVariable_Src_Alpha;
	RTBlendDesc.m_kDestBlend = BlendVariable_One;
	RTBlendDesc.m_kBlendOp = BlendOperation_Add;
	RTBlendDesc.m_kSrcBlendAlpha = BlendVariable_Zero;
	RTBlendDesc.m_kDestBlendAlpha = BlendVariable_Zero;
	RTBlendDesc.m_kBlendOpAlpha = BlendOperation_Add;
	RTBlendDesc.m_uRenderTargetWriteMask = Channel_All;

	BlendDesc AlphaBlendDesc = {};
	AlphaBlendDesc.m_bAlphaToCoverageEnable = false;
	AlphaBlendDesc.m_RenderTargets[0] = RTBlendDesc;
	AlphaBlendDesc.m_uSampleMask = 0xffffffff;
	AlphaBlendDesc.fBlendFactor = { 0, 0, 0, 0 };

	SetBlendState(AlphaBlendDesc);

	// Add DepthStencilState
	DepthStencilDesc DepthDesc = {};
	DepthDesc.m_bDepthEnable = false;
	DepthDesc.m_kDepthWriteMask = DepthWriteMask_Zero;
	DepthDesc.m_kDepthFunc = ComparisonFunction_Less;
	DepthDesc.m_bStencilEnable = false;

	SetDepthStencilState(DepthDesc, "2DTexture");

	// Default Parameter
	ClearRenderTargets(false);
	//SetDepthStencilViewReadOnly(true); // TODO: add member variables of relevant State Descriptions to RenderPassDX11 and make ReadOnlyDSV bool dependent on "DepthDesc.m_kDepthWriteMask = DepthWriteMask_Zero;"

	return m_bInitialized;
}
//---------------------------------------------------------------------------------------------------
bool HelixFullscreenTextureRenderPassDX11::OnPerObject(const RenderObjectDX11& _RenderObject, const MaterialDX11& _Material)
{
	SetObjectConstants(m_CBPerObject, _RenderObject, nullptr); // dont set material poperties

	if (_Material == nullptr)
	{
		return false;
	}

	const ImmutableTextureDX11& DiffuseTex = _Material.GetAlbedoMap();
	if (DiffuseTex != nullptr)
	{
		m_CBFullscreenTex->vFullscreenTextureResolution = Math::float4(static_cast<float>(DiffuseTex.GetDescription().m_uWidth), static_cast<float>(DiffuseTex.GetDescription().m_uHeight), 0.0f, 0.0f);

		m_FullscreenTexture.SetTexture(DiffuseTex);
	}

	uint32_t uClassPermutation = 0;

	if (DiffuseTex != nullptr)
	{
		if (IsSingleChannel(DiffuseTex.GetDescription().m_kFormat))
		{
			uClassPermutation |= Shaders::FullscreenTexture::HPMPS_R_Excl;
		}
		else if (IsThreeChannel(DiffuseTex.GetDescription().m_kFormat))
		{
			uClassPermutation |= Shaders::FullscreenTexture::HPMPS_RGB_Excl;
		}
	}

	Select(ShaderType_PixelShader, uClassPermutation);

	return true;
}
//---------------------------------------------------------------------------------------------------
bool HelixFullscreenTextureRenderPassDX11::OnPerCamera(const CameraComponent& _Camera)
{
	SetCameraConstants(m_CBPerCamera, _Camera.GetRenderingProperties());

	return true;
}
//---------------------------------------------------------------------------------------------------

std::shared_ptr<RenderPassDX11> HelixFullscreenTextureRenderPassDX11::CreatePass(const std::string& _sInstanceName) const
{
	return std::make_shared<HelixFullscreenTextureRenderPassDX11>(_sInstanceName);
}
//---------------------------------------------------------------------------------------------------
