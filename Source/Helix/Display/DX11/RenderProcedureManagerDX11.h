//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef RENDERPROCEDUREMANAGERDX11_H
#define RENDERPROCEDUREMANAGERDX11_H

#include "RenderProcedureDX11.h"
#include "DataStructures\SyncVariable.h"
#include "RenderDeviceShutdownCallbackDX11.h"

namespace Helix
{
	namespace Display
	{
		class RenderProcedureManagerDX11 : RenderDeviceShutdownCallbackDX11
		{
		public:
			RenderProcedureManagerDX11();
			RenderProcedureManagerDX11(const hlx::TextToken& _RendererToken);
			RenderProcedureManagerDX11(const Resources::RendererDesc& _Desc);

			~RenderProcedureManagerDX11();

			bool Load(const Resources::RendererDesc& _Desc, bool _bCompleteReload = false);

			// resets assigned Passes & Procedures, dont call while rendering is still running!
			void Reset();

			// adds a procedure at the end of the execution list, 
			bool AddProcedure(const Resources::RenderProcedureDesc& _Desc);

			// Manually add (and initialize) a procedure to the end of the vector
			bool AddProcedure(std::shared_ptr<RenderProcedureDX11> _pRenderProcedure);

			Resources::RendererDesc CreateDescription() const;

			void Apply();

			void PrintPermutationReport() const;

		private:
			void OnShutdown() final;

		private:
			std::vector<std::shared_ptr<RenderProcedureDX11>> m_Procedures;
			std::vector<std::shared_ptr<RenderPassDX11>> m_Passes;

#ifdef HNUCLEO
			std::shared_ptr<RenderPassDX11> m_pSelectedPass = nullptr;
			std::shared_ptr<RenderProcedureDX11> m_pSelectedProc = nullptr;

			std::vector<std::shared_ptr<RenderPassDX11>> m_ProcPasses;

			inline std::string GetPassName(int _iIndex) const { return m_ProcPasses[_iIndex]->GetTypeName() + m_ProcPasses[_iIndex]->GetInstanceName(); }
			inline void SelectPassName(int _iIndex) { m_pSelectedPass = m_ProcPasses[_iIndex]; m_pSelectedPass->BindToNucleo(); }
			Datastructures::SyncList<std::shared_ptr<RenderPassDX11>> m_SyncPasses = { "Passes", &m_ProcPasses, [&](int i) {return GetPassName(i); },[&](int i) {SelectPassName(i); } };

			inline std::string GetProcName(int _iIndex) const { return m_Procedures[_iIndex] != nullptr ? m_Procedures[_iIndex]->GetName() : ""; }
			inline void SelectProcName(int _iIndex)
			{
				if (m_pSelectedProc != m_Procedures[_iIndex])
				{
					m_pSelectedProc = m_Procedures[_iIndex];
					m_ProcPasses = m_pSelectedProc->GetPasses();
				}
			}

			Datastructures::SyncList<std::shared_ptr<RenderProcedureDX11>> m_SyncProcs = { "Procedures", &m_Procedures, [&](int i) {return GetProcName(i); },[&](int i) {SelectProcName(i); } };

			Datastructures::SyncGroup m_SyncRendererGroup = { "Render Precedures", {&m_SyncProcs, &m_SyncPasses } };
			Datastructures::SyncDock m_SyncRendererDock = { "Renderer", {&m_SyncRendererGroup} };
#endif
		};
	} // Display
} // Helix

#endif // RENDERPROCEDUREMANAGERDX11_H