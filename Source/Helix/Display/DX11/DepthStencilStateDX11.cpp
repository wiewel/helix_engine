//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "DepthStencilStateDX11.h"
#include "hlx\src\Logger.h"
#include "ViewDefinesDX11.h"

using namespace Helix::Display;

//---------------------------------------------------------------------------------------------------
DepthStencilStateDX11::DepthStencilStateDX11(const DepthStencilDesc& _DepthStencilDesc, const char* _pDbgName) :
	m_Description(_DepthStencilDesc)
{
	m_pReference = DepthStencilStatePoolDX11::Instance()->Get(ConvertToD3D11Description(_DepthStencilDesc), m_Hash);

	if (_pDbgName == nullptr)
	{
		HDXDEBUGNAME(m_pReference, "DXDepthStencilState_%u", m_Hash);
	}
	else 
	{
		HDXDEBUGNAME(m_pReference, "DXDepthStencilState_%s", _pDbgName);
	}
}
//---------------------------------------------------------------------------------------------------
// copy constructor, Increments the ref count
DepthStencilStateDX11::DepthStencilStateDX11(const DepthStencilStateDX11& _Other) :
	TDX11DepthStencilStateRef(_Other), m_Description(_Other.m_Description), m_uStencilRef(_Other.m_uStencilRef)
{
}
//---------------------------------------------------------------------------------------------------
DepthStencilStateDX11::~DepthStencilStateDX11()
{
}
//---------------------------------------------------------------------------------------------------

DepthStencilStateDX11& DepthStencilStateDX11::operator=(const DepthStencilStateDX11& _Other)
{
	if (this == &_Other)
		return *this;

	TDX11DepthStencilStateRef::operator=(_Other);
	m_Description = _Other.m_Description;
	m_uStencilRef = _Other.m_uStencilRef;
	return *this;
}
//---------------------------------------------------------------------------------------------------

DepthStencilStateDX11& DepthStencilStateDX11::operator=(std::nullptr_t)
{
	TDX11DepthStencilStateRef::operator=(nullptr);
	m_Description = {};
	m_uStencilRef = 0u;
	return *this;
}
//---------------------------------------------------------------------------------------------------
