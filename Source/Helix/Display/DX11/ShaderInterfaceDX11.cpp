//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "ShaderInterfaceDX11.h"

#include "Display\ShaderVariable.h"
#include "ShaderTextureDX11.h"
#include "ShaderRenderTargetDX11.h"
#include "ShaderSamplerDX11.h"

using namespace Helix::Display;

ShaderInterfaceDX11::ShaderInterfaceDX11()
{
}
//---------------------------------------------------------------------------------------------------

ShaderInterfaceDX11::~ShaderInterfaceDX11()
{
	HASSERTD(m_RenderTargetTextures.empty(), "Some ShaderRenderTargets have not been destroyed!");
	HASSERTD(m_Textures.empty(), "Some ShaderTextures have not been destroyed!");
	HASSERTD(m_Samplers.empty(), "Some ShaderSamplers have not been destroyed!");
	HASSERTD(m_Variables.empty(), "Some ShaderVariables have not been destroyed!");

	Clear();
}

//---------------------------------------------------------------------------------------------------

bool ShaderInterfaceDX11::SetInterface(const uint64_t& _uPermutation, const ShaderType& _kShaderStage, const ShaderInterfaceDesc* _pDesc)
{
	m_InterfacePermutations[_kShaderStage][_uPermutation] = _pDesc;

	// validate buffers
	for (const ShaderBufferDesc& InputBuffer : _pDesc->Buffers)
	{
		bool bFound = false;
		for (const ShaderBufferDesc& ExistingBuffer : m_Interface.Buffers)
		{
			if (InputBuffer.BindInfo.sName == ExistingBuffer.BindInfo.sName)
			{
				bFound = true;

				if (InputBuffer != ExistingBuffer)
				{
					HERROR("Buffer %s definition changed!", CSTR(ExistingBuffer.BindInfo.sName));
					return false;
				}

				break;
			}
		}

		if (bFound == false)
		{
			m_Interface.Buffers.push_back(InputBuffer);
		}
	}

	// validate textures
	for (const ShaderTextureDesc& InputTexture : _pDesc->Textures)
	{
		bool bFound = false;

		if (InputTexture.bRWTexture)
		{
			// RW texture, treated as rendertarget
			for (const BindInfoDesc& ExistingRenderTarget : m_Interface.RenderTargets)
			{
				if (InputTexture.BindInfo.sName == ExistingRenderTarget.sName)
				{
					bFound = true;

					if (InputTexture.BindInfo != ExistingRenderTarget)
					{
						HERROR("RenderTarget %s definition changed!", CSTR(ExistingRenderTarget.sName));
						return false;
					}

					break;
				}
			}

			if (bFound == false)
			{
				m_Interface.RenderTargets.push_back(InputTexture.BindInfo);
			}			
		}
		else // NORMAL shader input textue
		{
			for (const ShaderTextureDesc& ExistingTexture : m_Interface.Textures)
			{
				if (InputTexture.BindInfo.sName == ExistingTexture.BindInfo.sName)
				{
					bFound = true;

					if (InputTexture.BindInfo != ExistingTexture.BindInfo ||
						InputTexture.kDimension != ExistingTexture.kDimension ||
						InputTexture.kReturnType != ExistingTexture.kReturnType)
					{
						HERROR("Texture %s definition changed!", CSTR(ExistingTexture.BindInfo.sName));
						return false;
					}

					break;
				}
			}

			if (bFound == false)
			{
				m_Interface.Textures.push_back(InputTexture);
			}
		}		
	}

	// validate samplers
	for (const ShaderSamplerDesc& InputSampler : _pDesc->Samplers)
	{
		bool bFound = false;
		for (const ShaderSamplerDesc& ExistingSampler : m_Interface.Samplers)
		{
			if (InputSampler.BindInfo.sName == ExistingSampler.BindInfo.sName)
			{
				bFound = true;

				if (InputSampler.BindInfo != ExistingSampler.BindInfo)
				{
					HERROR("Sampler %s definition changed!", CSTR(ExistingSampler.BindInfo.sName));
					return false;
				}

				break;
			}
		}

		if (bFound == false)
		{
			m_Interface.Samplers.push_back(InputSampler);
		}
	}

	// validate rendertargets
	for (const BindInfoDesc& InputRenderTarget : _pDesc->RenderTargets)
	{
		bool bFound = false;
		for (const BindInfoDesc& ExistingRenderTarget: m_Interface.RenderTargets)
		{
			if (InputRenderTarget.sName == ExistingRenderTarget.sName)
			{
				bFound = true;

				if (InputRenderTarget != ExistingRenderTarget)
				{
					HERROR("RenderTarget %s definition changed!", CSTR(ExistingRenderTarget.sName));
					return false;
				}

				break;
			}
		}

		if (bFound == false)
		{
			m_Interface.RenderTargets.push_back(InputRenderTarget);
		}
	}

	return true;
}
//---------------------------------------------------------------------------------------------------

bool ShaderInterfaceDX11::SelectInterface(const uint64_t& _uPermutation, const ShaderType& _kShaderStage)
{
	std::unordered_map<uint64_t, const ShaderInterfaceDesc*>::const_iterator it = m_InterfacePermutations[_kShaderStage].find(_uPermutation);

	if (it == m_InterfacePermutations[_kShaderStage].cend())
	{
		HERROR("No interface found for permutation %ul", _uPermutation);
		return false;
	}

	// reset previsously selected shader inputs
	m_SelectedBuffers[_kShaderStage].resize(0);
	m_SelectedTextures[_kShaderStage].resize(0);
	m_SelectedSamplers[_kShaderStage].resize(0);
	if (_kShaderStage == ShaderType_PixelShader)
	{
		// rendertargets cleard when pixelshader is selected
		m_SelectedRenderTargets.resize(0);
	}

	// buffers
	for (const ShaderBufferDesc& Desc : it->second->Buffers)
	{
		const BindInfoDesc& BindInfo = Desc.BindInfo;

		TBufferInstMap::iterator inst_it = m_Buffers.find(m_sInstanceName);
		if (inst_it != m_Buffers.end())
		{
			TBufferMap::iterator bit = inst_it->second.find(BindInfo.sName);

			if (bit != inst_it->second.end())
			{
				const GPUBufferDX11* pBuffer = bit->second->GetBuffer();
				if (pBuffer == nullptr)
				{
					HERROR("Uninitialized GPUBufferInstance %s %s", CSTR(BindInfo.sName), CSTR(m_sInstanceName));
					return false;
				}

				// add to currently selected buffers
				m_SelectedBuffers[_kShaderStage].push_back({ bit->second, BindInfo.uSlot });			
			}
		}
		else
		{
			HWARNINGD("Buffer %s has not been initialized, select not possible (instance not found)", CSTR(BindInfo.sName));
			continue;
		}
	}

	// textures
	for (const ShaderTextureDesc& Desc : it->second->Textures)
	{
		const BindInfoDesc& BindInfo = Desc.BindInfo;
		uint64_t uBindHash = BindInfo.sName ^ m_sInstanceName;

		if (Desc.bRWTexture && _kShaderStage == ShaderType_PixelShader) 
		{
			// rw texture treated as rendertarget
			std::unordered_map<uint64_t, ShaderRenderTargetDX11*>::iterator tit = m_RenderTargetTextures.find(uBindHash);
			if (tit != m_RenderTargetTextures.end())
			{
				// add to currently selected textures
				m_SelectedRenderTargets.push_back({ tit->second, BindInfo.uSlot });
			}
		}
		else // normal shader input texture
		{
			std::unordered_map<uint64_t, ShaderTextureDX11*>::iterator tit = m_Textures.find(uBindHash);
			if (tit != m_Textures.end())
			{
				// add to currently selected textures
				m_SelectedTextures[_kShaderStage].push_back({ tit->second, BindInfo.uSlot });
			}
		}
	}

	// rendertargets
	if (_kShaderStage == ShaderType_PixelShader)
	{
		for (const BindInfoDesc& BindInfo : it->second->RenderTargets)
		{
			uint64_t uBindHash = BindInfo.sName ^ m_sInstanceName;

			std::unordered_map<uint64_t, ShaderRenderTargetDX11*>::iterator tit = m_RenderTargetTextures.find(uBindHash);
			if (tit != m_RenderTargetTextures.end())
			{
				// add to currently selected textures
				m_SelectedRenderTargets.push_back({ tit->second, BindInfo.uSlot });
			}
		}	
	}

	// samplers
	for (const ShaderSamplerDesc& Desc : it->second->Samplers)
	{
		const BindInfoDesc& BindInfo = Desc.BindInfo;
		uint64_t uBindHash = BindInfo.sName ^ m_sInstanceName;

		std::unordered_map<uint64_t, ShaderSamplerDX11*>::iterator sit = m_Samplers.find(uBindHash);
		if (sit != m_Samplers.end())
		{
			// add to currently selected sampler
			m_SelectedSamplers[_kShaderStage].push_back({ sit->second,BindInfo.uSlot });
		}
	}

	return true;
}
//---------------------------------------------------------------------------------------------------
bool ShaderInterfaceDX11::RegisterTexture(ShaderTextureDX11* _pTexture, const std::string& _sShaderInstanceName, _Out_ ShaderTextureDesc& _OutDesc)
{
	const std::string sName = _pTexture->GetName() + _sShaderInstanceName;
	uint64_t uHash = StrHash(_pTexture->GetName()) ^ StrHash(_sShaderInstanceName);

	if (m_Textures.count(uHash) != 0)
	{
		HERROR("Texture %s has already been registered!", CSTR(sName));
		return false;
	}

	for (const ShaderTextureDesc& Desc : m_Interface.Textures)
	{
		if (Desc.BindInfo.sName.GetString() == _pTexture->GetName())
		{
			m_Textures.insert({ uHash, _pTexture });
			_OutDesc = Desc;
			return true;
		}
	}
	
	HERROR("Texture %s not found in interface", CSTR(sName));

	return false;
}

//---------------------------------------------------------------------------------------------------
bool ShaderInterfaceDX11::RegisterRenderTarget(ShaderRenderTargetDX11* _pRenderTargetInterface, const std::string& _sShaderInstanceName, _Out_ uint32_t& _uSlot)
{
	const std::string sName = _pRenderTargetInterface->GetName() + _sShaderInstanceName;
	uint64_t uHash = StrHash(_pRenderTargetInterface->GetName()) ^ StrHash(_sShaderInstanceName);

	if (m_RenderTargetTextures.count(uHash) != 0)
	{
		HERROR("RenderTarget %s has already been registered!", CSTR(sName));
		return false;
	}

	for (const BindInfoDesc& Desc : m_Interface.RenderTargets)
	{
		if (Desc.sName.GetString() == _pRenderTargetInterface->GetName())
		{
			m_RenderTargetTextures.insert({ uHash, _pRenderTargetInterface });
			_uSlot = Desc.uSlot;
			return true;
		}
	}

	_uSlot = HUNDEFINED32;

	HERROR("RenderTarget %s not found in interface", CSTR(sName));

	return false;
}
//---------------------------------------------------------------------------------------------------
void ShaderInterfaceDX11::UnregisterRenderTarget(const std::string& _sRenderTarget, const std::string& _sInstanceName)
{
	m_RenderTargetTextures.erase(StrHash(_sRenderTarget) ^ StrHash(_sInstanceName));
}
//---------------------------------------------------------------------------------------------------
void ShaderInterfaceDX11::UnregisterTexture(const std::string& _sTexture, const std::string& _sInstanceName)
{
	m_Textures.erase(StrHash(_sTexture) ^ StrHash(_sInstanceName));
}
//---------------------------------------------------------------------------------------------------

bool ShaderInterfaceDX11::RegisterSampler(ShaderSamplerDX11* _pSamplerInterface, const std::string& _sShaderInstanceName, const bool _bWarnIfNotFound)
{
	const std::string sName = _pSamplerInterface->GetName() + _sShaderInstanceName;
	uint64_t uHash = StrHash(_pSamplerInterface->GetName()) ^ StrHash(_sShaderInstanceName);

	if (m_Samplers.count(uHash) != 0)
	{
		HERROR("Sampler %s has already been registered!", CSTR(sName));
		return false;
	}

	for (const ShaderSamplerDesc& Desc : m_Interface.Samplers)
	{
		if (Desc.BindInfo.sName.GetString() == _pSamplerInterface->GetName())
		{
			m_Samplers.insert({ uHash, _pSamplerInterface });
			return true;
		}
	}

	if (_bWarnIfNotFound)
	{
		HWARNING("Sampler %s not found in interface", CSTR(sName));
	}
	
	return false;
}
//---------------------------------------------------------------------------------------------------
void ShaderInterfaceDX11::UnregisterSampler(const std::string& _sSampler, const std::string& _sInstanceName)
{
	m_Samplers.erase(StrHash(_sSampler) ^ StrHash(_sInstanceName));
}
//---------------------------------------------------------------------------------------------------
bool ShaderInterfaceDX11::Clear()
{
	m_sInstanceName = {};

	// destroy buffers
	//m_Buffers.clear();

	// invalidate variables
	for (auto bit : m_Variables)
	{
		for (auto vit : bit.second)
		{
			vit.second->Invalidate();
		}
	}
	// variables need to be updated!

	// forget interface
	m_Interface.RenderTargets.resize(0);
	m_Interface.Buffers.resize(0);
	m_Interface.Textures.resize(0);
	m_Interface.Samplers.resize(0);

	m_SelectedRenderTargets.resize(0);
	for (uint32_t i = 0; i < ShaderType_NumOf; ++i)
	{
		m_SelectedBuffers[i].resize(0);
		m_SelectedTextures[i].resize(0);
		m_SelectedSamplers[i].resize(0);

		m_InterfacePermutations[i].clear();
	}

	return true;
}

//---------------------------------------------------------------------------------------------------

std::shared_ptr<GPUBufferInstanceDX11> ShaderInterfaceDX11::RegisterVariable(
	ShaderVariable* _pVariableInterface,
	const std::string& _sVarPath,
	const std::string& _sShaderInstanceName,
	_Out_ ShaderVariableDesc& _OutDesc,
	_Out_ uint32_t& _uStructureSize)
{
	std::vector<std::string> Path = hlx::split(_sVarPath, '/');

	if (Path.empty())
	{
		HERROR("Invalid variable path");
		return nullptr;
	}

	const std::string& sBuffer = Path.front();
	uint64_t uInstanceHash = StrHash(_sShaderInstanceName);
	uint64_t uBufferHash =  StrHash(sBuffer);

	// search for instance
	TBufferInstMap::iterator inst_it = m_Buffers.find(uInstanceHash);
	if (inst_it == m_Buffers.end())
	{
		HERROR("Buffer %s %s not found for variable %s (instance)!", CSTR(sBuffer), CSTR(_sShaderInstanceName), CSTR(_sVarPath));
		return nullptr;
	}

	// search for buffer in instance
	TBufferMap::iterator bit = inst_it->second.find(uBufferHash);
	if (bit == inst_it->second.end())
	{
		HERROR("Buffer %s %s not found for variable %s!", CSTR(sBuffer), CSTR(_sShaderInstanceName), CSTR(_sVarPath));
		return nullptr;
	}

	bool bFound = false;

	// search for variable description in interface
	const ShaderBufferDesc& Buffer = bit->second->GetDescription();

	_OutDesc.sName = Buffer.BindInfo.sName;
	_OutDesc.uSize = Buffer.BindInfo.uSize;
	_OutDesc.kClass = ShaderVariableClass_Struct;
	_OutDesc.uElements = 0;

	_uStructureSize = Buffer.GetStructureSize();

	if (Path.size() == 1)
	{
		// found is also true if you are not searching for a variable but for the whole buffer name
		bFound = true;
	}
	else
	{
		for (const ShaderVariableDesc& Var : Buffer.Variables)
		{
			if (GetVariableFromPath(Var, Path, 1, _OutDesc))
			{
				bFound = true;
				break;
			}
		}
	}

	if (bFound == false)
	{
		HERROR("Variable %s no found in current selected interface", CSTR(_sVarPath));
		return nullptr;
	}

	uint64_t uBindHash = uBufferHash ^ uInstanceHash; // buffer hash

	// search for buffer instance
	auto vit = m_Variables.find(uBindHash);
	if (vit != m_Variables.end())
	{
		// buffer already has some active variables
		vit->second.insert({ _sVarPath, _pVariableInterface }); // save interface
	}
	else
	{
		// no variable has been associated with this buffer yet, create new mapping
		std::unordered_map<std::string, ShaderVariable*> Vars;
		Vars.insert({ _sVarPath, _pVariableInterface });

		m_Variables.insert({ uBindHash, std::move(Vars) });
	}

	return bit->second;
}
//---------------------------------------------------------------------------------------------------
void ShaderInterfaceDX11::UnregisterVariable(ShaderVariable* _pVariableInterface, const std::string& _sVarPath, const std::string& _sShaderInstanceName)
{
	if (_pVariableInterface->m_pBufferInstance == nullptr)
	{
		return;
	}

	const std::string& sBuffer = _pVariableInterface->m_pBufferInstance->GetDescription().BindInfo.sName;
	uint64_t uBufferInstanceHash = StrHash(sBuffer) ^ StrHash(_sShaderInstanceName);

	auto bit = m_Variables.find(uBufferInstanceHash);

	if (bit != m_Variables.end())
	{
		bit->second.erase(_sVarPath);
		if (bit->second.empty())
		{
			m_Variables.erase(bit);
		}
	}
	else
	{
		HERROR("Buffer %s %s for variable %s not found", CSTR(sBuffer), CSTR(_sShaderInstanceName), CSTR(_sVarPath));
	}
}
//---------------------------------------------------------------------------------------------------

void ShaderInterfaceDX11::UpdateVariablesForBuffer(std::shared_ptr<GPUBufferInstanceDX11>& _pBuffer, const uint64_t uBindHash)
{
	const ShaderBufferDesc& BufferDesc = _pBuffer->GetDescription();
	uint32_t uStructureSize = BufferDesc.GetStructureSize();

	auto vit = m_Variables.find(uBindHash);

	if (vit != m_Variables.end())
	{
		for (auto var : vit->second)
		{
			ShaderVariable* pVar = var.second;

			std::vector<std::string> Path = hlx::split(pVar->m_sVarPath, "/");
			HASSERT(Path.empty() == false, "fuck");

			if (Path.size() == 1u) // variable uses whole constant buffer
			{
				if (pVar->m_uVarSize != BufferDesc.BindInfo.uSize)
				{
					HFATAL("Size does not match for variable %s expected %uB got %uB", CSTR(pVar->m_sVarPath), pVar->m_uVarSize, BufferDesc.BindInfo.uSize);
				}
				else
				{
					pVar->Update(_pBuffer);
				}
			}
			else
			{
				ShaderVariableDesc VarDesc;
				for (const ShaderVariableDesc& Var : BufferDesc.Variables)
				{
					if (GetVariableFromPath(Var, Path, 1, VarDesc))
					{
						if (pVar->UpdateDesc(VarDesc)) // validates size & type
						{
							pVar->Update(_pBuffer);
						}

						break;
					}
				}
			}
		}
	}	
}

//---------------------------------------------------------------------------------------------------
bool ShaderInterfaceDX11::GetVariableFromPath(const ShaderVariableDesc& _ParentVar, const std::vector<std::string>& _Path, uint32_t _uIndex, ShaderVariableDesc& _OutVar)
{
	if (_ParentVar.sName != _Path[_uIndex])
	{
		return false;
	}
	else if (_Path.size() - 1 == _uIndex)
	{
		_OutVar = _ParentVar;
		return true;
	}

	for (const ShaderVariableDesc& Var : _ParentVar.Children)
	{
		if (GetVariableFromPath(Var, _Path, _uIndex + 1, _OutVar))
		{
			return true;
		}
	}

	return false;
}
//---------------------------------------------------------------------------------------------------
inline std::shared_ptr<GPUBufferInstanceDX11> ShaderInterfaceDX11::CreateBuffer(const ShaderBufferDesc& _Desc)
{
	// create new
	std::shared_ptr<GPUBufferInstanceDX11> pBufferInstance = std::make_shared<GPUBufferInstanceDX11>(_Desc);

	// initialize if not a structured buffer
	switch (_Desc.kType)
	{
	case ShaderBufferType_ConstantBuffer:
	case ShaderBufferType_TextureBuffer:
	case ShaderBufferType_UAV_RWTyped:
	case ShaderBufferType_ByteAddress:
	case ShaderBufferType_UAV_RWByteAddress:
		if (pBufferInstance->Initialize() == false)
		{
			return false;
		}
		break;
	}

	return pBufferInstance;
}
//---------------------------------------------------------------------------------------------------

bool ShaderInterfaceDX11::CreateBuffers()
{
	TBufferInstMap::iterator inst_it = m_Buffers.find(m_sInstanceName);
	if (inst_it == m_Buffers.end())
	{
		inst_it = m_Buffers.insert({ m_sInstanceName,{} }).first; //empty map
	}

	// remove buffers which are not in the interface anymore
	for (TBufferMap::iterator it = inst_it->second.begin(); it != inst_it->second.end();)
	{
		bool bFound = false;
		for (const ShaderBufferDesc& InputBuffer : m_Interface.Buffers)
		{
			if (InputBuffer.BindInfo.sName == it->second->GetDescription().BindInfo.sName)
			{
				bFound = true;
				break;
			}
		}

		if (bFound == false)
		{
			it->second = nullptr;
			it = inst_it->second.erase(it);
		}
		else
		{
			++it;
		}
	}

	// create new buffers / update existing ones
	for (const ShaderBufferDesc& InputBuffer : m_Interface.Buffers)
	{
		uint64_t uBindHash = InputBuffer.BindInfo.sName ^ m_sInstanceName;
		std::shared_ptr<GPUBufferInstanceDX11> pBufferInstance = nullptr;

		// search for buffer in instance
		TBufferMap::iterator bit = inst_it->second.find(InputBuffer.BindInfo.sName);
		if (bit == inst_it->second.end())
		{
			pBufferInstance = CreateBuffer(InputBuffer);

			inst_it->second.insert({ InputBuffer.BindInfo.sName, pBufferInstance });
		}
		else
		{			
			// validate existing buffer
			const ShaderBufferDesc& Desc = bit->second->GetDescription();

			if (Desc == InputBuffer)
			{
				pBufferInstance = bit->second;
			}
			else
			{			
				// create new one
				pBufferInstance = CreateBuffer(InputBuffer);
				bit->second = pBufferInstance;
			}
		}

		UpdateVariablesForBuffer(pBufferInstance, uBindHash);
	}

	return true;
}
//---------------------------------------------------------------------------------------------------

std::vector<std::shared_ptr<GPUBufferInstanceDX11>> ShaderInterfaceDX11::GetBuffersForInstance(const Helix::HashedStringValue& _sInstanceName)
{
	std::vector<std::shared_ptr<GPUBufferInstanceDX11>> Buffers;

	TBufferInstMap::iterator inst_it = m_Buffers.find(_sInstanceName);

	if (inst_it != m_Buffers.end())
	{
		for (const TBufferMap::value_type& kv : inst_it->second)
		{
			Buffers.push_back(kv.second);
		}
	}

	return Buffers;
}

//---------------------------------------------------------------------------------------------------

bool ShaderInterfaceDX11::InitStructuredBuffer(const std::string& _sBufferName, const std::string& _sShaderInstanceName, uint32_t _uElementCount)
{
	uint64_t uInstanceHash = StrHash(_sShaderInstanceName);
	uint64_t uBufferHash = StrHash(_sBufferName);

	// search for instance
	TBufferInstMap::iterator inst_it = m_Buffers.find(uInstanceHash);
	if (inst_it == m_Buffers.end())
	{
		HERROR("StructuredBuffer %s not found for instance %s (instance)!", CSTR(_sBufferName), CSTR(_sShaderInstanceName));
		return nullptr;
	}

	// search for buffer in instance
	TBufferMap::iterator bit = inst_it->second.find(uBufferHash);
	if (bit == inst_it->second.end())
	{
		HERROR("StructuredBuffer %s not found for instance %s!", CSTR(_sBufferName), CSTR(_sShaderInstanceName));
		return nullptr;
	}

	// init / update existing variables
	if (bit->second->Initialize(_uElementCount))
	{
		uint8_t* pBase = bit->second->GetData();
		// update variables for this buffer
		auto vit = m_Variables.find(uBufferHash);

		if (vit != m_Variables.end())
		{
			for (auto var : vit->second)
			{
				var.second->Update(bit->second);
			}
		}
	}
	else
	{
		return false;
	}

	return true;
}
//---------------------------------------------------------------------------------------------------
