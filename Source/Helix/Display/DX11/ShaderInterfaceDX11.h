//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef SHADERINTERFACEDX11
#define SHADERINTERFACEDX11

#include "ViewDefinesDX11.h"

#include "GPUBufferInstanceDX11.h"
#include <unordered_map>

namespace Helix
{
	namespace Display
	{
		class ShaderSamplerDX11;
		class ShaderTextureDX11;
		class ShaderRenderTargetDX11;
		class ShaderVariable;

		template <class T>
		struct Selected
		{
			Selected() {}
			Selected(const T& _pSelected, const uint32_t& _uSlot) :
				pRes(_pSelected), uSlot(_uSlot)	{}

			T pRes = {};
			uint32_t uSlot = HUNDEFINED32;
		};

		using TSelectedBuffers = std::vector<Selected<std::shared_ptr<GPUBufferInstanceDX11>>>;
		using TSelectedTextures = std::vector<Selected<ShaderTextureDX11*>>;
		using TSelectedRenderTargets = std::vector<Selected<ShaderRenderTargetDX11*>>;
		using TSelectedSamplers = std::vector<Selected<ShaderSamplerDX11*>>;

		class ShaderInterfaceDX11
		{
		public:
			ShaderInterfaceDX11();
			~ShaderInterfaceDX11();

			bool SetInterface(const uint64_t& _uPermutation, const ShaderType& _kShaderStage, const ShaderInterfaceDesc* _InterfaceDesc);

			bool SelectInterface(const uint64_t& _uPermutation, const ShaderType& _kShaderStage);

			bool RegisterTexture(ShaderTextureDX11* _pTextureInterface, const std::string& _sShaderInstanceName, _Out_ ShaderTextureDesc& _OutDesc);
			void UnregisterTexture(const std::string& _sTexture, const std::string& _sInstanceName);

			bool RegisterRenderTarget(ShaderRenderTargetDX11* _pRenderTargetInterface, const std::string& _sShaderInstanceName, _Out_ uint32_t& _uSlot);
			void UnregisterRenderTarget(const std::string& _sRenderTarget, const std::string& _sInstanceName);

			bool RegisterSampler(ShaderSamplerDX11* _pSamplerInterface, const std::string& _sShaderInstanceName, const bool _bWarnIfNotFound);
			void UnregisterSampler(const std::string& _sSampler, const std::string& _sInstanceName);

			// Variable path is separated by '/'
			std::shared_ptr<GPUBufferInstanceDX11> RegisterVariable(ShaderVariable* _pVariableInterface, const std::string& _sVarPath, const std::string& _sShaderInstanceName, _Out_ ShaderVariableDesc& _OutDesc, _Out_ uint32_t& _uStructureSize);
			void UnregisterVariable(ShaderVariable* _pVariableInterface, const std::string& _sVarPath, const std::string& _sShaderInstanceName);

			// structured buffer have to be manually initialized with a ElementCount > 1
			bool InitStructuredBuffer(const std::string& _sBufferName, const std::string& _sShaderInstanceName, uint32_t _uElementCount);

			//update base pointer and structure size, validate type size
			void UpdateVariablesForBuffer(std::shared_ptr<GPUBufferInstanceDX11>& _pBuffer, const uint64_t uBindHash);

			bool GetVariableFromPath(_In_ const ShaderVariableDesc& _ParentVar, const std::vector<std::string>& _Path, uint32_t _uIndex, _Out_ ShaderVariableDesc& _OutVar);

			// create buffers referenced in master interface m_Interface, returns false if creation failed or definition changed
			// note that this is triggered everytime the instance name is changed
			bool CreateBuffers();
			
			// returns buffers instances associated with this instance
			std::vector<std::shared_ptr<GPUBufferInstanceDX11>> GetBuffersForInstance(const HashedStringValue& _sInstanceName);

			// destroys interface including buffers, invalidated variables (which will be updated on select again)
			// this function should only be called before reloading the shader and thus rebuilding the interface
			bool Clear();

			const TSelectedBuffers& GetSelectedBuffers(const ShaderType& _kShaderType) const;
			const TSelectedTextures& GetSelectedTextures(const ShaderType& _kShaderType) const;
			const TSelectedRenderTargets& GetSelectedRenderTargets() const;
			const TSelectedSamplers& GetSelectedSamplers(const ShaderType& _kShaderType) const;
		
			void SetInstanceName(const HashedStringValue& _sInstanceName);

		private:
			std::shared_ptr<GPUBufferInstanceDX11> CreateBuffer(const ShaderBufferDesc& _Desc);

		private:
			// permutation hash -> reduced bind interface InterfaceBindInfo
			std::unordered_map<uint64_t,const ShaderInterfaceDesc*> m_InterfacePermutations[ShaderType_NumOf];

			TSelectedBuffers m_SelectedBuffers[ShaderType_NumOf];
			TSelectedTextures m_SelectedTextures[ShaderType_NumOf];
			TSelectedRenderTargets m_SelectedRenderTargets; // pixel shader only
			TSelectedSamplers m_SelectedSamplers[ShaderType_NumOf];

			// master interface containing all shader inputs
			ShaderInterfaceDesc m_Interface;

			// 1st key mapps to instance name 2nd key mapps to buffer name

			using TBufferMap = std::unordered_map<uint64_t, std::shared_ptr<GPUBufferInstanceDX11>>;
			using TBufferInstMap = std::unordered_map<uint64_t, TBufferMap>;
			TBufferInstMap m_Buffers;

			// uint64_t => buffer name & instance hash mapping to all variables bound to this buffer
			// std::string => variable path (including buffer name)

			std::unordered_map<uint64_t, std::unordered_map<std::string, ShaderVariable*>> m_Variables; // former multimap
			std::unordered_map<uint64_t, ShaderTextureDX11*> m_Textures;
			std::unordered_map<uint64_t, ShaderRenderTargetDX11*> m_RenderTargetTextures;
			std::unordered_map<uint64_t, ShaderSamplerDX11*> m_Samplers;

			HashedStringValue m_sInstanceName;
		};
		//---------------------------------------------------------------------------------------------------
		inline void ShaderInterfaceDX11::SetInstanceName(const HashedStringValue& _sInstanceName)
		{
			m_sInstanceName = _sInstanceName;
		}
		inline const TSelectedBuffers& ShaderInterfaceDX11::GetSelectedBuffers(const ShaderType& _kShaderType) const
		{
			return m_SelectedBuffers[_kShaderType];
		}
		inline const TSelectedTextures& ShaderInterfaceDX11::GetSelectedTextures(const ShaderType& _kShaderType) const
		{
			return m_SelectedTextures[_kShaderType];
		}
		inline const TSelectedRenderTargets& ShaderInterfaceDX11::GetSelectedRenderTargets() const
		{
			return m_SelectedRenderTargets;
		}
		inline const TSelectedSamplers& ShaderInterfaceDX11::GetSelectedSamplers(const ShaderType& _kShaderType) const
		{
			return m_SelectedSamplers[_kShaderType];
		}
	} // Display
} // Helix

#endif