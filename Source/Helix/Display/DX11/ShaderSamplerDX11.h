//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef SHADERSAMPLERDX11_H
#define	SHADERSAMPLERDX11_H

#include "SamplerStateDX11.h"

namespace Helix
{
	namespace Display
	{
		class ShaderInterfaceDX11;

		class ShaderSamplerDX11
		{
		public:
			HDEBUGNAME("ShaderSamplerDX11");

			ShaderSamplerDX11(const std::string& _sSamplerName, bool _bUseDefaultSampler = true);
			~ShaderSamplerDX11();

			bool Initialize(ShaderInterfaceDX11* _pInterface, const std::string& _sShaderInstanceName, const bool _bWarnIfNotFound);

			void SetSampler(const SamplerStateDX11& _Texture);
			const SamplerStateDX11& GetSampler() const;

			const std::string& GetName() const;

			bool IsDefaultSampler() const;
		private:
			const std::string m_sName;
			const bool m_bUseDefaultSampler;
			std::string m_sShaderInstanceName;

			ShaderInterfaceDX11* m_pInterface = nullptr;

			ShaderSamplerDesc m_Description;
			SamplerStateDX11 m_CurrentSampler;
		};

		inline void ShaderSamplerDX11::SetSampler(const SamplerStateDX11& _Sampler)	{ m_CurrentSampler = _Sampler; }
		inline bool ShaderSamplerDX11::IsDefaultSampler() const { return m_bUseDefaultSampler; }
		inline const SamplerStateDX11& ShaderSamplerDX11::GetSampler() const { return m_CurrentSampler; }
		inline const std::string& ShaderSamplerDX11::GetName() const { return m_sName; }
	} // Display
} // Helix

#endif // SHADERSAMPLERDX11_H
