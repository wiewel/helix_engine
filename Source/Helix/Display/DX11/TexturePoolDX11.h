//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef TEXTUREPOOLDX11_H
#define TEXTUREPOOLDX11_H

#include "DataStructures\TplHashPool.h"
#include "hlx\src\Singleton.h"
#include "Util\UniqueID.h"
#include "RenderDeviceShutdownCallbackDX11.h"

namespace Helix
{
	namespace Display
	{
		struct TextureReferenceDX11;
		struct TextureDesc;
		class TextureDX11;

		using TDX11TexturePool = Datastructures::TplRefHashPool<uint64_t, TextureDesc, TextureReferenceDX11>;
		
		class TexturePoolDX11 : public TDX11TexturePool, public hlx::Singleton<TexturePoolDX11>/*, RenderDeviceShutdownCallbackDX11*/
		{
		public:
			HDEBUGNAME("TexturePoolDX11");

			TexturePoolDX11();
			~TexturePoolDX11();

			void Initialize(const std::string& _sTextureDirectory);

			uint64_t HashFunction(const TextureDesc& _Desc) final;

			uint64_t GetDefaultKey() final;
		private:
			TextureReferenceDX11* CreateFromDesc(const TextureDesc& _Desc, const uint64_t& _Hash) final;
			
			//void OnShutdown() final;
		private:
			std::string m_sTextureDirectory;

			static TextureDX11 m_ErrorTexture;

			UniqueID<uint64_t> m_ID;
		};

		using TDX11TextureRef = Datastructures::TplHashPoolReference<TexturePoolDX11>;
	} // Display
} // Helix

#endif // TEXTUREPOOLDX11_H