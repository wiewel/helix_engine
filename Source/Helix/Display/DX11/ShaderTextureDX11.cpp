//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "ShaderTextureDX11.h"
#include "ShaderInterfaceDX11.h"

using namespace Helix::Display;
//---------------------------------------------------------------------------------------------------

ShaderTextureDX11::ShaderTextureDX11(const std::string& _sTextureName, const std::string& _sMappedName) :
	m_sName(_sTextureName), m_sMappedName(_sMappedName), m_CurrentTextures(1u)
{
}
//---------------------------------------------------------------------------------------------------

ShaderTextureDX11::~ShaderTextureDX11()
{
	if (m_pInterface != nullptr)
	{
		m_pInterface->UnregisterTexture(m_sName, m_sShaderInstanceName);
	}
}
//---------------------------------------------------------------------------------------------------

bool ShaderTextureDX11::Initialize(ShaderInterfaceDX11* _pInterface, const std::string& _sShaderInstanceName)
{
	if (m_pInterface == nullptr)
	{
		m_pInterface = _pInterface;
		if (m_pInterface->RegisterTexture(this, _sShaderInstanceName, m_Description))
		{
			m_sShaderInstanceName = _sShaderInstanceName;
			if (m_Description.BindInfo.uCount < 1)
			{
				HERROR("Invalid BindCount (0) for texture %s", CSTR(m_sName));
				m_CurrentTextures.resize(1);
			}
			else
			{
				m_CurrentTextures.resize(m_Description.BindInfo.uCount);
			}
			return true;
		}
	}

	HERROR("Texture %s has already been initialized!", CSTR(m_sName));
	return false;
}
//---------------------------------------------------------------------------------------------------
bool ShaderTextureDX11::SetTexture(const TextureDX11& _Texture, uint32_t _uIndex)
{
	HASSERT(_uIndex < m_CurrentTextures.size(), "Invalid TextureSlot index %u", _uIndex);
	m_CurrentTextures[_uIndex] = _Texture;
	return true;
}
//---------------------------------------------------------------------------------------------------
