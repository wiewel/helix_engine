//   Copyright 2020 Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef HELIXBLOOMRENDERPASSDX11_H
#define HELIXBLOOMRENDERPASSDX11_H

#include "RenderPassDX11.h"
#include "Display\Shaders\Bloom.hlsl"

namespace Helix
{
	namespace Display
	{
		class HelixBloomRenderPassDX11 : public RenderPassDX11
		{
		public:
			HDEBUGNAME("HelixBloomRenderPassDX11");

			HelixBloomRenderPassDX11(DefaultInitializerType);
			HelixBloomRenderPassDX11(const std::string& _sInstanceName = {});
			~HelixBloomRenderPassDX11();

			bool OnInitalize(const hlx::TextToken& _CustomVariables) final;

			bool OnPerFrame() final;

			//void EnableAutoExposure(const bool _bAutoExposure);
			//bool IsAutoExposureEnabled() const;

			void SetBloomThreshold(const float _fThreshold);
			float GetBloomThreshold() const;

			void SetLuminanceMipLevel(const float _fMipLevel);
			float GetLuminanceMipLevel() const;

			void SetExposureKeyLevel(const float _fExposureKeyValue);
			float GetExposureKeyLevel() const;
			std::shared_ptr<RenderPassDX11> CreatePass(const std::string& _sInstanceName) const final;

		private:
			ShaderVariableDX11<Shaders::Bloom::cbBloom> m_CBBloom = { "cbBloom" };

			ShaderTextureDX11 m_ColorMap = { "gColorMap" };
			ShaderTextureDX11 m_LuminanceMap = { "gLuminanceMap", "LUMINANCE_ADAPTION_MAP" };

			ShaderRenderTargetDX11 m_OutputTexture = { "vBloom", "BLOOM_MAP" };

		private:
			//bool m_bAutoExposure = false;

			float m_fBloomThreshold = 3.5f; //[0-10]
			float m_fLuminanceMipLevel = 10.0f; // least detailed mip level for 1024 texture -> average luminance
			float m_fExposureKeyValue = 0.025f;
		};

		//inline void HelixBloomRenderPassDX11::EnableAutoExposure(const bool _bAutoExposure) { m_bAutoExposure = _bAutoExposure; }
		//inline bool HelixBloomRenderPassDX11::IsAutoExposureEnabled() const { return m_bAutoExposure; }

		inline void HelixBloomRenderPassDX11::SetBloomThreshold(const float _fThreshold) { m_fBloomThreshold = _fThreshold; }
		inline float HelixBloomRenderPassDX11::GetBloomThreshold() const { return m_fBloomThreshold; }
		
		inline void HelixBloomRenderPassDX11::SetLuminanceMipLevel(const float _fMipLevel) { m_fLuminanceMipLevel = _fMipLevel; }
		inline float HelixBloomRenderPassDX11::GetLuminanceMipLevel() const { return m_fLuminanceMipLevel; }

		inline void HelixBloomRenderPassDX11::SetExposureKeyLevel(const float _fExposureKeyValue) { m_fExposureKeyValue = _fExposureKeyValue; }
		inline float HelixBloomRenderPassDX11::GetExposureKeyLevel() const { return m_fExposureKeyValue; }

	} // Display
} // Helix

#endif // HELIXLUMINANCERENDERPASSDX11_H