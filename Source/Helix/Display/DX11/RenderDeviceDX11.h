//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef RENDERDEVICEDX11_H
#define RENDERDEVICEDX11_H

#include <d3d11.h>
#include <vector>

#include "Display\RenderDevice.h"
#include "hlx\src\Singleton.h"
#include "hlx\src\StandardDefines.h"
#include "ViewDefinesDX11.h"
#include "Util\UpdateIndex.h"

// forward decls:
namespace Helix
{
	namespace Display
	{
		//Forward decls
		class DXGIManager;

		class DepthStencilTextureDX11;
		class ShaderDX11;
		class RenderObjectDX11;
		class DepthStencilStateDX11;
		class BlendStateDX11;
		class RasterizerStateDX11;
		class GPUBufferDX11;
		class RenderDeviceShutdownCallbackDX11;
		
		class RenderDeviceDX11 : public RenderDevice, public hlx::Singleton<RenderDeviceDX11>
		{
		public:
			friend class RenderDeviceDrawDX11;
			friend class RenderPassDX11;
			friend class RenderViewDX11;
			friend class ShaderPermutationPoolDX11;
			friend class RenderDeviceShutdownCallbackDX11;

			HDEBUGNAME("RenderDeviceDX11");

			RenderDeviceDX11();
			~RenderDeviceDX11();

			void Initialize(const bool _bDebug) final;
			void Shutdown() final;
			bool IsInitialized() final;

			ID3D11Device* GetDevice() const;
			ID3D11DeviceContext* GetContext() const;

			uint32_t GetAndResetShaderChangeCount();

		private:
			// unsets all shaders
			void ClearShaders();

			// unsets all render targets
			void ClearRenderTargets();
			void ClearRenderTarget(const uint32_t _uSlot);
			void ClearDepthStencil();

			// called at the beginning of each pass (once)
			void SelectRenderTargets(const DepthStencilTextureDX11& _DepthStencil);

			/// update viewports based on currently bound render targets
			void UpdateViewports(const DepthStencilTextureDX11& _DepthStencil);

			// applys depthstencil state directly
			void SetDepthStencilState(const DepthStencilStateDX11& _DepthStencilState);

			void SetShader(const std::shared_ptr<ShaderDX11>& _pShader);

			// Clear all shader resources
			void ClearShaderResources();			
			
			//applys topology immediately
			void SetPrimitiveTopology(const PrimitiveTopology& _kPrimitiveTopology);

			// applys blendstate
			void SetBlendState(const BlendStateDX11& _BlendState);

			// apply rasterizer state
			void SetRasterizerState(const RasterizerStateDX11& _RasterizerState);

			void RegisterShutdownCallback(RenderDeviceShutdownCallbackDX11* _pCallback);
			void UnregisterShutdownCallback(RenderDeviceShutdownCallbackDX11* _pCallback);

		private:
			std::vector<RenderDeviceShutdownCallbackDX11*> m_ShutdownCallbacks;

			//currently set vertex layout
			VertexLayoutDX11 m_CurrentInputLayout = {};
			
			//currently set shaders
			std::shared_ptr<ShaderDX11> m_pSelectedShader = nullptr;
			ShaderPtrDX11 m_ActiveShaders[ShaderType_NumOf] = {};

			// RasterizerState
			ID3D11RasterizerState* m_pCurrentRasterizerState = nullptr;
			
			// BlendState
			ID3D11BlendState* m_pCurrentBlendState = nullptr;
			std::array<float, 4> m_CurrentBlendFactor = { 0.0f,0.0f,0.0f,0.0f };
			uint32_t m_uCurrentSampleMask = HUNDEFINED32;

			// DepthStencil
			ID3D11DepthStencilState* m_pCurrentDepthStencilState = nullptr;
			uint32_t m_uCurrentDepthStencilRef = 0;
			DepthStencilDesc m_CurrentDepthStencilDesc = {};
			ID3D11DepthStencilView* m_pCurrentDepthStencilView = nullptr;

			// RenderTargets
			ID3D11RenderTargetView* m_pCurrentRenderTargets[D3D11_SIMULTANEOUS_RENDER_TARGET_COUNT];		
			D3D11_VIEWPORT m_CurrentViewports[D3D11_SIMULTANEOUS_RENDER_TARGET_COUNT];

			//ShaderResources (Textures & Buffers)
			ID3D11ShaderResourceView* m_pCurrentShaderResources[ShaderType_NumOf][D3D11_COMMONSHADER_INPUT_RESOURCE_SLOT_COUNT]; // e.g. textures
			UpdateIndex m_UpdatedShaderResources[ShaderType_NumOf]; // PerObject basis
			UpdateIndex m_PerPassUpdatedSRVs[ShaderType_NumOf]; // Per Pass basis

			//Unordered Access Views (Buffers & RWTextures)
			ID3D11UnorderedAccessView* m_pCurrentUnorderedAccessViews[D3D11_PS_CS_UAV_REGISTER_COUNT];
			UpdateIndex m_UpdatedUnorderedAccessViews;

			//SamplerStates
			ID3D11SamplerState* m_pCurrentSamplerStates[ShaderType_NumOf][D3D11_COMMONSHADER_SAMPLER_SLOT_COUNT];
			UpdateIndex m_UpdatedSamplerStates[ShaderType_NumOf];

			//ConstantBuffers:
			ID3D11Buffer* m_pCurrentConstantBuffers[ShaderType_NumOf][D3D11_COMMONSHADER_CONSTANT_BUFFER_API_SLOT_COUNT];
			UpdateIndex m_UpdatedConstantBuffers[ShaderType_NumOf];

			//VertexBuffers
			ID3D11Buffer* m_pCurrentVertexBuffers[D3D11_IA_VERTEX_INPUT_RESOURCE_SLOT_COUNT] = { nullptr };
			uint32_t m_uCurrentVertexOffsets[D3D11_IA_VERTEX_INPUT_RESOURCE_SLOT_COUNT] = { 0 };
			uint32_t m_uCurrentVertexStrideSizes[D3D11_IA_VERTEX_INPUT_RESOURCE_SLOT_COUNT] = { 0 };
			UpdateIndex m_UpdatedVertexBuffers;

			//IndexBuffer
			ID3D11Buffer* m_pCurrentIndexBuffer = nullptr;
			uint32_t m_uCurrentIndexOffset = 0;
			DXGI_FORMAT m_kIndexBufferFormat = DXGI_FORMAT_R32_UINT; // U16 or U32
			bool m_bIndexBufferUpdated = true;

			PrimitiveTopology m_kPrimitiveTopology = PrimitiveTopology_Undefined;
			

			ID3D11Debug* m_pDebug = nullptr;
			ID3D11InfoQueue* m_pInfoQueue = nullptr;

			ID3D11Device* m_pDevice = nullptr;
			ID3D11DeviceContext* m_pDeviceContext = nullptr;
			DXGIManager* m_pDXGIManager = nullptr;

			uint32_t m_uShaderChanges = 0u;
		};

		inline ID3D11Device* RenderDeviceDX11::GetDevice() const { return m_pDevice; }
		inline ID3D11DeviceContext* RenderDeviceDX11::GetContext() const { return m_pDeviceContext; }
		inline uint32_t RenderDeviceDX11::GetAndResetShaderChangeCount()
		{
			uint32_t uCount = m_uShaderChanges;
			m_uShaderChanges = 0u;
			return uCount;
		}
		inline bool RenderDeviceDX11::IsInitialized() { return m_pDevice != nullptr && m_pDeviceContext != nullptr; }

		inline void RenderDeviceDX11::SetShader(const std::shared_ptr<ShaderDX11>& _pShader){ m_pSelectedShader = _pShader; }

		inline void RenderDeviceDX11::SetPrimitiveTopology(const PrimitiveTopology& _kPrimitiveTopology)
		{
			if (_kPrimitiveTopology != m_kPrimitiveTopology)
			{
				m_kPrimitiveTopology = _kPrimitiveTopology;
				m_pDeviceContext->IASetPrimitiveTopology(static_cast<D3D11_PRIMITIVE_TOPOLOGY>(m_kPrimitiveTopology));
			}
		}
	} // Display
} // Helix

#endif // RENDERDEVICE_H