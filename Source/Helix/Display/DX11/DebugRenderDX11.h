//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef DEBUGRENDERDX11_H
#define DEBUGRENDERDX11_H

#include "hlx\src\Singleton.h"
#include "Display\DX11\RenderObjectDX11.h"
#include "Math\MathTypes.h"
#include "Display\VertexLayouts.h"
#include <unordered_map>
#include <utility>

namespace Helix
{
	namespace Display
	{
		class DebugRenderDX11 : public hlx::Singleton<DebugRenderDX11>
		{
		private:
			struct LineType
			{
				uint32_t uByteOffset;
				std::vector<VertexPosXYZColor> Vertices;
			};
			using EntryType = std::pair<uint32_t, LineType>; 

		public:
			HDEBUGNAME("DebugRenderDX11");

			struct LineIndices
			{
				LineIndices() : uFirst(HUNDEFINED32), uLast(HUNDEFINED32) {}

				LineIndices(uint32_t _uFirst, uint32_t _uLast) :
					uFirst(_uFirst), uLast(_uLast) {}

				//LineIndices(const LineIndices& _Other) :
				//	uFirst(_Other.uFirst), uLast(_Other.uLast) {}

				LineIndices(LineIndices& _Other) :
					uFirst(_Other.uFirst), uLast(_Other.uLast)
				{
					const_cast<uint32_t&>(_Other.uFirst) = HUNDEFINED32;
					const_cast<uint32_t&>(_Other.uLast) = HUNDEFINED32;
				}

				LineIndices& operator=(LineIndices& _Other)
				{
					const_cast<uint32_t&>(uFirst) = _Other.uFirst;
					const_cast<uint32_t&>(uLast) = _Other.uLast;
					const_cast<uint32_t&>(_Other.uFirst) = HUNDEFINED32;
					const_cast<uint32_t&>(_Other.uLast) = HUNDEFINED32;

					return *this;
				}

				const uint32_t uFirst;
				const uint32_t uLast;

				explicit operator bool() const { return uFirst != HUNDEFINED32 && uLast != HUNDEFINED32; }
			};

			DebugRenderDX11();
			~DebugRenderDX11();

		public:
			uint32_t DrawLine(const Math::float3& _vStart, const Math::float3& _vEnd, const Math::float3& _vColor = Math::float3(1.0f,0,0));

			LineIndices DrawBox(std::array<Math::float3, 8>& _Corners, const Math::float3& _vColor = Math::float3(1.0f, 0, 0));

			void RemoveDebugObject(LineIndices& _Indices);
			void RemoveDebugObject(const uint32_t& _uIdentifier);

			void HideAll(bool _bHide);
			void EnableDepthClipping(bool _bClipping);

		private:
			SubMesh GetLineDesc(const EntryType& _Line);
			void CreateMesh();
			void Realloc();

			uint32_t DrawLineInternal(const Math::float3& _vStart, const Math::float3& _vEnd, const Math::float3& _vColor, bool _bLockRendering);
		private:
			const uint32_t m_uByteOffset;
			uint32_t m_uMaxEntries = 100u;
			uint32_t m_uFirstFree = 0u;
			std::vector<EntryType> m_Entries;

			RenderObjectDX11 m_DebugLines;

			bool m_bDepthClipping = false;
		};

		inline void DebugRenderDX11::EnableDepthClipping(bool _bClipping) { m_bDepthClipping = _bClipping; }
	} /// Display
} /// Helix


#endif /// !DEBUGRENDERDX11_H



