//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef SHADERVARIABLEDX11
#define SHADERVARIABLEDX11

#include "Display\ShaderVariable.h"

namespace Helix
{
	namespace Display
	{
		template <typename VarType>
		class ShaderVariableDX11 : public ShaderVariable
		{
		private:
			VarType m_TempVar;

		public:
			ShaderVariableDX11(const std::string& _sVarPath) : ShaderVariable(_sVarPath, static_cast<uint32_t>(sizeof(VarType))) {}
			~ShaderVariableDX11() {}

			// restrict copying
			ShaderVariableDX11(const ShaderVariableDX11& _Other) = delete;
			ShaderVariableDX11& operator=(const ShaderVariableDX11& _Other) = delete;

			// if possible allways use this operator because it checks if the data really changed
			inline VarType& operator=(const VarType& _Val)
			{
				m_TempVar = _Val;

				if (m_pBase != nullptr && m_pBufferInstance != nullptr)
				{
					VarType* pVar = reinterpret_cast<VarType*>(m_pBase + m_Description.uStartOffset);
					if (memcmp(pVar, &_Val, sizeof(VarType)) != 0)
					{
						m_pBufferInstance->SetDataChanged();
						*pVar = _Val;
					}

					return *pVar;
				}
				else
				{
					return m_TempVar;
				}
			}

			// check if this is used some where, this can cause value inconsitencies
			inline VarType* operator->()
			{
				if (m_pBase != nullptr && m_pBufferInstance != nullptr)
				{
					m_pBufferInstance->SetDataChanged();
					return reinterpret_cast<VarType*>(m_pBase + m_Description.uStartOffset);
				}
				else
				{
					return &m_TempVar;
				}
			}

			inline const VarType* operator->() const
			{
				if (m_pBase != nullptr && m_pBufferInstance != nullptr)
				{
					return reinterpret_cast<const VarType*>(m_pBase + m_Description.uStartOffset);
				}
				else
				{
					return &m_TempVar;
				}
			}

			// for structured buffers
			inline VarType& operator[](const uint32_t& n)
			{
				if (m_pBase != nullptr && m_pBufferInstance != nullptr)
				{
					m_pBufferInstance->SetDataChanged();

					if (m_uStructureSize == 0)
					{
						HASSERTD(n < m_Description.uElements, "Invalid index %u max %u", n, m_Description.uElements)
							return *reinterpret_cast<VarType*>(m_pBase + (sizeof(VarType)* n) + m_Description.uStartOffset);
					}
					else
					{
						return *reinterpret_cast<VarType*>(m_pBase + (m_uStructureSize * n) + m_Description.uStartOffset);
					}
				}
				else
				{
					return m_TempVar;
				}
			}

			inline const VarType& operator[](const uint32_t& n) const
			{
				if (m_pBase != nullptr && m_pBufferInstance != nullptr)
				{
					if (m_uStructureSize == 0)
					{
						HASSERTD(n < m_Description.uElements, "Invalid index %u max %u", n, m_Description.uElements)
							return *reinterpret_cast<const VarType*>(m_pBase + (sizeof(VarType)* n) + m_Description.uStartOffset);
					}
					else
					{
						return *reinterpret_cast<const VarType*>(m_pBase + (m_uStructureSize * n) + m_Description.uStartOffset);
					}
				}
				else
				{
					return m_TempVar;
				}
			}

		private:
			inline void OnUpdate() final
			{
				// write last buffered value
				operator=(m_TempVar);
			}

			inline void OnInvalidate() final
			{
				// save last value from buffer
				if (m_pBase != nullptr && m_pBufferInstance != nullptr)
				{					
					m_TempVar = *reinterpret_cast<VarType*>(m_pBase + m_Description.uStartOffset);
				}
			}
		};
	} // Display
} // Helix

#endif