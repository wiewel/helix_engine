//   Copyright 2020 Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef HELIXSSAORENDERPASSDX11_H
#define HELIXSSAORENDERPASSDX11_H

#include "RenderPassDX11.h"
#include "Math\MathFunctions.h"

#include "Display\Shaders\SSAO.hlsl"
#include "CommonConstantBufferFunctions.h"

namespace Helix
{
	namespace Display
	{
		class HelixSSAORenderPassDX11 : public RenderPassDX11
		{
		public:
			HDEBUGNAME("HelixSSAORenderPassDX11");

			HelixSSAORenderPassDX11(DefaultInitializerType);
			HelixSSAORenderPassDX11(const std::string& _sInstanceName = {});
			~HelixSSAORenderPassDX11();

			bool OnInitalize(const hlx::TextToken& _CustomVariables) final;
			bool OnSelectDefault() final;

			bool OnPerFrame() final;
			bool OnPerCamera(const Scene::CameraComponent& _Camera) final;

			void EnableSSAO(const bool _bEnabled);
			bool IsSSAOEnabled() const;

			void SetSSAORadius(const float _fRadius);
			void SetSSAOPower(const float _fPower);
			float GetSSAORadius() const;
			float GetSSAOPower() const;
			std::shared_ptr<RenderPassDX11> CreatePass(const std::string& _sInstanceName) const final;

		private:
			ShaderVariableDX11<Shaders::SSAO::cbSSAO> m_CBSSAO = {"cbSSAO"};
			TCBPerCamera m_CBPerCamera = { "cbPerCamera" };

			ShaderTextureDX11 m_DepthTexture = { "gDepthTex", "LINEAR_DEPTH" };
			ShaderTextureDX11 m_NormalWSTexture = { "gNormalTex", "WORLDSPACE_NORMAL"};

			ShaderRenderTargetDX11 m_OcclusionTarget = { "vOcclusion", "SSAO_MAP" };
			//ShaderRenderTargetDX11 m_BentNormalTarget = { "vBentNormal", "SSAO_BENT_NORMAL_MAP" };

			// radius in world units (meter)
			float m_fSSAORadius = 0.15f;
			float m_fSSAOPower = 1.5f;

			bool m_bEnabled = true;
		};

		inline void HelixSSAORenderPassDX11::EnableSSAO(const bool _bEnabled) { m_bEnabled = _bEnabled; }
		inline bool HelixSSAORenderPassDX11::IsSSAOEnabled() const { return m_bEnabled; }

		inline void HelixSSAORenderPassDX11::SetSSAORadius(const float _fRadius) { m_fSSAORadius = Math::Clamp<float>(_fRadius, 0.1f, FLT_MAX); }
		inline void HelixSSAORenderPassDX11::SetSSAOPower(const float _fPower) { m_fSSAOPower = Math::Clamp<float>(_fPower, 1.0f, FLT_MAX); }

		inline float HelixSSAORenderPassDX11::GetSSAORadius() const { return m_fSSAORadius; }
		inline float HelixSSAORenderPassDX11::GetSSAOPower() const { return m_fSSAOPower; }
	} // Display
} // Helix

#endif // HELIXSSAORENDERPASSDX11_H