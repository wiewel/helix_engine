//   Copyright 2020 Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "HelixSSAORenderPassDX11.h"

using namespace Helix;
using namespace Helix::Display;
using namespace Helix::Scene;
using namespace Helix::Datastructures;

//---------------------------------------------------------------------------------------------------

HelixSSAORenderPassDX11::HelixSSAORenderPassDX11(DefaultInitializerType) : RenderPassDX11(DefaultInit, "SSAO")
{
}

//---------------------------------------------------------------------------------------------------
HelixSSAORenderPassDX11::HelixSSAORenderPassDX11(const std::string& _sInstanceName) :
	RenderPassDX11("SSAO", _sInstanceName,
	{ &m_DepthTexture, &m_NormalWSTexture }, // InputTextures
	{ &m_OcclusionTarget, /*&m_BentNormalTarget*/ }, // RenderTargets
	{ &m_CBPerCamera, &m_CBSSAO }) // CBuffers
{
}
//---------------------------------------------------------------------------------------------------
HelixSSAORenderPassDX11::~HelixSSAORenderPassDX11()
{
}
//---------------------------------------------------------------------------------------------------
std::shared_ptr<RenderPassDX11> HelixSSAORenderPassDX11::CreatePass(const std::string& _sInstanceName) const
{
	return std::make_shared<HelixSSAORenderPassDX11>(_sInstanceName);
}
//---------------------------------------------------------------------------------------------------
bool HelixSSAORenderPassDX11::OnInitalize(const hlx::TextToken& _CustomVariables)
{
	// Add RasterizerState
	RasterizerDesc RasDesc = {};
	RasDesc.m_kFillMode = FillMode_Solid;
	RasDesc.m_kCullMode = CullMode_None;
	RasDesc.m_bFrontCounterClockwise = false;

	SetRasterizerState(RasDesc);

	// Add BlendState
	RenderTargetBlendDesc RTBlendDesc = {};
	RTBlendDesc.m_bBlendEnable = false;
	RTBlendDesc.m_uRenderTargetWriteMask = Channel_All;

	BlendDesc AlphaBlendDesc = {};
	AlphaBlendDesc.m_bAlphaToCoverageEnable = false;
	AlphaBlendDesc.m_RenderTargets[0] = RTBlendDesc;
	AlphaBlendDesc.m_uSampleMask = 0xffffffff;
	AlphaBlendDesc.fBlendFactor = { 0.0f,0.0f,0.0f,0.0f };

	SetBlendState(AlphaBlendDesc);

	// Add DepthStencilState
	DepthStencilDesc DepthDesc = {};
	DepthDesc.m_bDepthEnable = false; // true;
	DepthDesc.m_kDepthWriteMask = DepthWriteMask_Zero;
	DepthDesc.m_kDepthFunc = ComparisonFunction_Greater;
	DepthDesc.m_bStencilEnable = false;

	SetDepthStencilState(DepthDesc, "SSAO");

	// Default Parameter
	ClearRenderTargets(true);

	// Generate Kernel
	std::srand(static_cast<uint32_t>(std::time(0))); // use current time as seed for random generator
	const float fInvRandMax = 1.0f / static_cast<float>(RAND_MAX);

	for (uint32_t i = 0; i < SSAO_KERNEL_SIZE; ++i)
	{
		// Generate...
		float fRandom = static_cast<float>(std::rand()) * fInvRandMax;
		m_CBSSAO->vSSAOKernel[i].x = fRandom * 2.0f - 1.0f; // [-1,1]

		fRandom = static_cast<float>(std::rand()) * fInvRandMax;
		m_CBSSAO->vSSAOKernel[i].y = fRandom * 2.0f - 1.0f; // [-1,1]

		fRandom = static_cast<float>(std::rand()) * fInvRandMax;
		m_CBSSAO->vSSAOKernel[i].z = fRandom; // [0,1]

		// not needed... CB is ........
		m_CBSSAO->vSSAOKernel[i].w = 0.0f;

		m_CBSSAO->vSSAOKernel[i].normalize();

		// ... and scale
		float fScale = static_cast<float>(i) / static_cast<float>(SSAO_KERNEL_SIZE);
		// LERP:ret lerp(x, y, s) -> x + s(y-x)
		fScale = 0.1f + (fScale*fScale) * 0.9f;
		m_CBSSAO->vSSAOKernel[i] *= fScale;
	}

	for (uint32_t i = 0; i < NOISE_SIZE; ++i)
	{
		float fRandom = static_cast<float>(std::rand()) * fInvRandMax;
		m_CBSSAO->vSSAONoise[i].x = fRandom;

		fRandom = static_cast<float>(std::rand()) * fInvRandMax;
		m_CBSSAO->vSSAONoise[i].y = fRandom;

		// not needed... CB is ........
		m_CBSSAO->vSSAONoise[i].z = 0.0f;
		m_CBSSAO->vSSAONoise[i].w = 0.0f;

		m_CBSSAO->vSSAONoise[i] = m_CBSSAO->vSSAONoise[i] * 2.0f - 1.0f;

		m_CBSSAO->vSSAONoise[i].normalize();
	}

	m_CBSSAO->fSSAORadius = 0.0f;
	m_CBSSAO->fSSAOPower = 1.0f;
	
	return m_bInitialized;
}
//---------------------------------------------------------------------------------------------------
bool HelixSSAORenderPassDX11::OnSelectDefault()
{
	bool bResult = true;

	uint32_t uInOutPermutation = m_bDebug ? Shaders::SSAO::HPMIO_Debug : 0;
	GetShader()->SetIOPermutation(uInOutPermutation);

	uint32_t uClassPermutation = 0u;//Shaders::SSAO::HPMPS_BentNormal;
	bResult &= Select(ShaderType_PixelShader, uClassPermutation);
	bResult &= Select(ShaderType_VertexShader, 0);

	return bResult;
}
//---------------------------------------------------------------------------------------------------
bool HelixSSAORenderPassDX11::OnPerFrame()
{
	uint32_t uInOutPermutation = m_bDebug ? Shaders::SSAO::HPMIO_Debug : 0;
	GetShader()->SetIOPermutation(uInOutPermutation);

	uint32_t uClassPermutation = 0u;//Shaders::SSAO::HPMPS_BentNormal;
	Select(ShaderType_PixelShader, uClassPermutation);
	Select(ShaderType_VertexShader, 0);

	return true;
}
//---------------------------------------------------------------------------------------------------
bool HelixSSAORenderPassDX11::OnPerCamera(const CameraComponent& _Camera)
{
	SetCameraConstants(m_CBPerCamera, _Camera.GetRenderingProperties());

	if(m_bEnabled)
	{
		m_CBSSAO->fSSAORadius = m_fSSAORadius / (_Camera.GetFarDistance() - _Camera.GetNearDistance());
		m_CBSSAO->fSSAOPower = m_fSSAOPower;
	}
	else
	{
		m_CBSSAO->fSSAORadius = 0;
	}

	return m_bEnabled;
}
//---------------------------------------------------------------------------------------------------
