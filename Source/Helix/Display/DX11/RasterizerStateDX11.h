//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef RASTERIZERSTATEDX11_H
#define RASTERIZERSTATEDX11_H

#include "RasterizerStatePoolDX11.h"
#include "Display\ViewDefines.h"

namespace Helix
{
	namespace Display
	{
		class RasterizerStateDX11 : public TDX11RasterizerStateRef
		{
		public:
			HDEBUGNAME("RasterizerStateDX11");

			constexpr RasterizerStateDX11() noexcept : TDX11RasterizerStateRef(nullptr) {}
			constexpr RasterizerStateDX11(std::nullptr_t) noexcept : TDX11RasterizerStateRef(nullptr) {}

			RasterizerStateDX11(const RasterizerDesc& _RasterizerDesc, const char* _pDbgName = nullptr);
			~RasterizerStateDX11();

			RasterizerStateDX11(const RasterizerStateDX11& _Other);
			RasterizerStateDX11& operator=(const RasterizerStateDX11& _Other);

			bool IsInitialized() const;

			const RasterizerDesc& GetDescription() const;

		private:
			RasterizerDesc m_Description = {};
		};

		inline bool RasterizerStateDX11::IsInitialized() const { return this->operator bool(); }
		inline const RasterizerDesc& RasterizerStateDX11::GetDescription() const { return m_Description; }
		
	} // Display
} // Helix

#endif // RasterizerStateDX11_H