//   Copyright 2020 Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "HelixScaleRenderPassDX11.h"
#include "Display\Shaders\Scale.hlsl"

using namespace Helix;
using namespace Helix::Display;
using namespace Helix::Scene;
using namespace Helix::Datastructures;

//---------------------------------------------------------------------------------------------------

HelixScaleRenderPassDX11::HelixScaleRenderPassDX11(DefaultInitializerType) : RenderPassDX11(DefaultInit, "Scale")
{
}

//---------------------------------------------------------------------------------------------------
HelixScaleRenderPassDX11::HelixScaleRenderPassDX11(const std::string& _sInstanceName) :
	RenderPassDX11("Scale", _sInstanceName,
	{ &m_InputTexture },
	{ &m_OutputTexture } )
{
}
//---------------------------------------------------------------------------------------------------
HelixScaleRenderPassDX11::~HelixScaleRenderPassDX11()
{
}
//---------------------------------------------------------------------------------------------------
bool HelixScaleRenderPassDX11::OnInitalize(const hlx::TextToken& _CustomVariables)
{
	// Add RasterizerState
	RasterizerDesc RasDesc = {};
	RasDesc.m_kFillMode = FillMode_Solid;
	RasDesc.m_kCullMode = CullMode_None;
	RasDesc.m_bFrontCounterClockwise = false;

	SetRasterizerState(RasDesc);

	// Add BlendState
	RenderTargetBlendDesc RTBlendDesc = {};
	RTBlendDesc.m_bBlendEnable = false;

	BlendDesc AlphaBlendDesc = {};
	AlphaBlendDesc.m_bAlphaToCoverageEnable = false;
	AlphaBlendDesc.m_RenderTargets[0] = RTBlendDesc;
	AlphaBlendDesc.m_uSampleMask = 0xffffffff;
	AlphaBlendDesc.fBlendFactor = { 0.0f,0.0f,0.0f,0.0f };

	SetBlendState(AlphaBlendDesc);

	// Add DepthStencilState
	DepthStencilDesc DepthDesc = {};
	DepthDesc.m_bDepthEnable = false;
	DepthDesc.m_bStencilEnable = false;

	SetDepthStencilState(DepthDesc, "Scale");

	// Default Parameter
	ClearRenderTargets(false);

	return m_bInitialized;
}
//---------------------------------------------------------------------------------------------------
std::shared_ptr<RenderPassDX11> HelixScaleRenderPassDX11::CreatePass(const std::string& _sInstanceName) const
{
	return std::make_shared<HelixScaleRenderPassDX11>(_sInstanceName);
}
//---------------------------------------------------------------------------------------------------
