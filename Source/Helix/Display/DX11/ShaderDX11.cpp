//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "ShaderDX11.h"
#include "Resources\HLXSLFile.h"
#include "Resources\InputLayoutSerializer.h"
#include "ShaderPermutationPoolDX11.h"
#include "RenderDeviceDX11.h"

using namespace Helix::Resources;
using namespace Helix::Display;
//---------------------------------------------------------------------------------------------------

ShaderDX11::ShaderDX11(const hlx::string& _sName) :
	m_sName(_sName)
{
	ReEnableStages();
}
//---------------------------------------------------------------------------------------------------

#ifndef HSAFE_SHADER_MAP_RELEASE
#define HSAFE_SHADER_MAP_RELEASE(_pool) \
for (auto& it1 : _pool) \
{ \
	for (auto& it2 : it1.second) \
	{ \
		HSAFE_RELEASE(it2.second.pShader); \
	} \
}
#endif

//---------------------------------------------------------------------------------------------------

ShaderDX11::~ShaderDX11()
{
	HSAFE_SHADER_MAP_RELEASE(m_VertexShaderPool);
	HSAFE_SHADER_MAP_RELEASE(m_HullShaderPool);
	HSAFE_SHADER_MAP_RELEASE(m_DomainShaderPool);
	HSAFE_SHADER_MAP_RELEASE(m_GeometryShaderPool);
	HSAFE_SHADER_MAP_RELEASE(m_PixelShaderPool);
	HSAFE_SHADER_MAP_RELEASE(m_ComputeShaderPool);
}
//---------------------------------------------------------------------------------------------------

ShaderPermutationHash ShaderDX11::ReducePermutation(const ShaderPermutationHash& _Permutation, const ShaderType& _kShaderStage) const
{
	ShaderPermutationHash ReducedPerm = {};
	ReducedPerm.uClassPermutation = _Permutation.uClassPermutation & m_PermutationMasks[_kShaderStage];
	ReducedPerm.uIOPermutation = _Permutation.uIOPermutation & m_PermutationMasks.back();

	// TODO: reduce ranges
	ReducedPerm.Ranges = _Permutation.Ranges;

	return ReducedPerm;
}

//---------------------------------------------------------------------------------------------------

bool ShaderDX11::AddPermutation(
	const hlx::bytes& _Code,
	const uint32_t& _uUncompressedCodeSize,
	const uint32_t& _uCodeCRC32,
	const ShaderType& _kShaderStage,
	const ShaderPermutationHash& _PermHash,
	const ShaderInterfaceDesc* _pInterface,
	const VertexLayoutDX11& _VertexLayout)
{
	uint64_t uRangeHash = _PermHash.GetRangeHash();
	if (m_CodeMap.count(_uCodeCRC32) == 0u)
	{
		m_CodeMap[_uCodeCRC32] = { _Code, _uUncompressedCodeSize };
	}

	switch (_kShaderStage)
	{
	case ShaderType_VertexShader:
		m_VertexShaderPool[_PermHash.uBasePermutation][uRangeHash] = { _uCodeCRC32, nullptr };
		break;
	case ShaderType_HullShader:
		m_HullShaderPool[_PermHash.uBasePermutation][uRangeHash] = { _uCodeCRC32, nullptr };
		break;
	case ShaderType_DomainShader:
		m_DomainShaderPool[_PermHash.uBasePermutation][uRangeHash] = { _uCodeCRC32, nullptr };
		break;
	case ShaderType_GeometryShader:
		m_GeometryShaderPool[_PermHash.uBasePermutation][uRangeHash] = { _uCodeCRC32, nullptr };
		break;
	case ShaderType_PixelShader:
		m_PixelShaderPool[_PermHash.uBasePermutation][uRangeHash] = { _uCodeCRC32, nullptr };
		break;
	case ShaderType_ComputeShader:
		m_bHasComputeShader = true;
		m_ComputeShaderPool[_PermHash.uBasePermutation][uRangeHash] = { _uCodeCRC32, nullptr };
		break;
	default:
		break;
	}

	//HLOGD("%s %u %u %llu %s", m_sName.c_str(), _PermHash.uClassPermutation, _PermHash.uIOPermutation, uRangeHash, ShaderTypeToString(_kShaderStage).c_str());

	//---------------------------------------------------------------------------------------------------
	// Add d3d input layout
	//---------------------------------------------------------------------------------------------------

	if (_VertexLayout.pInputLayout != nullptr)
	{
		m_InputLayouts[_PermHash.uBasePermutation] = _VertexLayout;
	}

	//---------------------------------------------------------------------------------------------------
	// Create shader variable interface
	//---------------------------------------------------------------------------------------------------

	// TODO: instance name & range permutation
	if (m_Interface.SetInterface(_PermHash.uBasePermutation, _kShaderStage, _pInterface) == false)
	{
		HERROR("Failed to initalize ShaderInterface for %s", m_sName.c_str());
		return false;
	}

	m_AvailablePermutations[_kShaderStage].push_back(_PermHash);

	return true;
}

//---------------------------------------------------------------------------------------------------
// helper constructs
#ifndef EXTRACTSHADER
#define EXTRACTSHADER(Stage) \
hlx::bytes Code; \
if (ShaderPermutationPoolDX11::UncompressShaderCode(cit->second.Code, Code, cit->second.uUncompressedSize, m_kCodeCompression) && \
	FAILED(hr = pDevice->Create##Stage##Shader(Code.data(), Code.size(), nullptr, &Shader.p##Stage##Shader)) == false){\
	HDXDEBUGNAME(Shader.p##Stage##Shader, "%s_p_%u_io_%u", hlx::to_sstring(m_sName).c_str(), ReducedPerm.uClassPermutation, ReducedPerm.uIOPermutation); \
	rit->second.pShader = Shader.p##Stage##Shader; \
	ShaderPermutationPoolDX11::Instance()->IncrementActiveShaders();\
}
#endif

#ifndef GETSHADERDX11
#define GETSHADERDX11(Stage) \
TShaderClassMap<ID3D11##Stage##Shader>::iterator sit = m_##Stage##ShaderPool.find(ReducedPerm.uBasePermutation); \
if (sit != m_##Stage##ShaderPool.end())\
{\
	TShaderRangeMap<ID3D11##Stage##Shader>::iterator rit = sit->second.find(uRangeHash);\
	if (rit != sit->second.end())\
	{\
		if (rit->second.pShader == nullptr)\
		{\
			TCodeMap::iterator cit = m_CodeMap.find(rit->second.uCRC32);\
			if (cit != m_CodeMap.end())\
			{\
				EXTRACTSHADER(Stage)\
			}\
			else\
			{\
				HERROR("Code not found for %s Permutation %llu %lu", m_sName.c_str(), ReducedPerm.uBasePermutation, ReducedPerm.GetRangeHash());\
			}\
		}\
		else\
		{\
			Shader.pShader = rit->second.pShader;\
		}\
	}\
	else\
	{\
		HERROR("Range permutation %llu not found for %s", uRangeHash, m_sName.c_str());\
	}\
}\
else\
{\
	HERROR("Class permutation %llu not found for %s", ReducedPerm.uBasePermutation, m_sName.c_str());\
}
#endif

//---------------------------------------------------------------------------------------------------
bool ShaderDX11::ReSelectInterface()
{
	bool bResult = true;
	
	for(const ShaderFunction& Func : m_Functions)
	{
		ShaderType Type = ShaderModelVersions[Func.kTargetVersion].kType;
		bResult &= m_Interface.SelectInterface(m_SelectedPermutations[Type].uBasePermutation, Type);

#ifdef _PROFILE
		//m_PermutationSelectionStats[Type].clear();
		for (TPermutationSelectionStats::value_type& Perm : m_PermutationSelectionStats[Type])
		{
			Perm.second = 0u;
		}
#endif
	}

	return bResult;
}
//---------------------------------------------------------------------------------------------------
bool ShaderDX11::Select(
	const ShaderType& _kShaderStage,
	const uint32_t& _uClassPermutation,
	const std::vector<uint8_t>& _RangeIndices)
{
	// skip stage if disabled
	if (m_ActiveStages[_kShaderStage] == false)
	{
		return true;
	}

	// reduce to valid permutation
	ShaderPermutationHash ReducedPerm = ReducePermutation(ShaderPermutationHash(_uClassPermutation, m_uSelectedIOPermutation, _RangeIndices), _kShaderStage);

	// permutation already selected
	if (m_SelectedPermutations[_kShaderStage] == ReducedPerm &&
		m_SelectedShaders[_kShaderStage] != nullptr) // check if this is not the first select
	{
		return true;
	}

#ifdef _PROFILE
	TPermutationSelectionStats& SelectedPermStats = m_PermutationSelectionStats[_kShaderStage];
	TPermutationSelectionStats::iterator itr = SelectedPermStats.find(ReducedPerm);
	if (itr == SelectedPermStats.end())
	{
		SelectedPermStats[ReducedPerm] = 1u;
	}
	else
	{
		itr->second++;
	}
#endif

	m_SelectedPermutations[_kShaderStage] = ReducedPerm;

	ID3D11Device* pDevice = RenderDeviceDX11::Instance()->GetDevice();

	//---------------------------------------------------------------------------------------------------
	// Create shader d3d and add interface
	//---------------------------------------------------------------------------------------------------

	HRESULT hr = S_OK;
	ShaderPtrDX11 Shader = {};

	uint64_t uRangeHash = ReducedPerm.GetRangeHash();
	if (_kShaderStage == ShaderType_VertexShader)
	{
		GETSHADERDX11(Vertex);
	}
	else if (_kShaderStage == ShaderType_HullShader)
	{
		GETSHADERDX11(Hull)
	}
	else if (_kShaderStage == ShaderType_DomainShader)
	{
		GETSHADERDX11(Domain)
	}
	else if (_kShaderStage == ShaderType_GeometryShader)
	{
		GETSHADERDX11(Geometry)
	}
	else if (_kShaderStage == ShaderType_PixelShader)
	{
		GETSHADERDX11(Pixel)
	}
	else if (_kShaderStage == ShaderType_ComputeShader)
	{
		GETSHADERDX11(Compute)
	}

	if (FAILED(hr))
	{
		HRERROR(hr);

		if (FAILED(hr) == false)
		{
			HERROR("Failed to uncompress shader %s code", m_sName.c_str());
		}

		return false;
	}

	if (Shader == nullptr)
	{
		HERROR("%s Permutation %X %X %llu not found for shader %s",
			ShaderTypeToString[_kShaderStage],
			ReducedPerm.uClassPermutation,
			ReducedPerm.uIOPermutation,
			uRangeHash,
			m_sName.c_str());

		return false;
	}

	m_SelectedShaders[_kShaderStage] = Shader;

	// select InputLayout
	if (_kShaderStage == ShaderType_VertexShader)
	{
		TInputlayoutMap::iterator it = m_InputLayouts.end();
		it = m_InputLayouts.find(ReducedPerm.uBasePermutation);
		if (it != m_InputLayouts.end())
		{
			m_SelectedInputLayout = it->second;
		}
		else
		{
			m_SelectedInputLayout = {};
		}
	}

	// TODO: select interface also based on range permutation
	// select variable interface
	if (m_Interface.SelectInterface(ReducedPerm.uBasePermutation, _kShaderStage) == false)
	{
		return false;
	}

	return true;
}

//---------------------------------------------------------------------------------------------------

void ShaderDX11::Clear()
{
	m_Functions.resize(0u);
	m_bHasComputeShader = false;
	m_kCodeCompression = Resources::CodeCompression_LZMA;
	m_PermutationMasks.fill(0u);

	m_CodeMap.clear();
	m_Interface.Clear();

	m_SelectedInputLayout = {};
	m_uSelectedIOPermutation = 0u;

	for (uint32_t i = 0; i < ShaderType_NumOf; ++i)
	{
		m_SelectedPermutations[i] = {};
		m_SelectedShaders[i] = nullptr;
		//m_ActiveStages[i] = true;
		m_AvailablePermutations[i].resize(0u);
	}

	// permutation pool holds inputlayous, so we dont need to release them here
	m_InputLayouts.clear();

	HSAFE_SHADER_MAP_RELEASE(m_VertexShaderPool);
	HSAFE_SHADER_MAP_RELEASE(m_HullShaderPool);
	HSAFE_SHADER_MAP_RELEASE(m_DomainShaderPool);
	HSAFE_SHADER_MAP_RELEASE(m_GeometryShaderPool);
	HSAFE_SHADER_MAP_RELEASE(m_PixelShaderPool);
	HSAFE_SHADER_MAP_RELEASE(m_ComputeShaderPool);

	m_VertexShaderPool.clear();
	m_HullShaderPool.clear();
	m_DomainShaderPool.clear();
	m_GeometryShaderPool.clear();
	m_PixelShaderPool.clear();
	m_ComputeShaderPool.clear();
}

//---------------------------------------------------------------------------------------------------
ShaderPermutationHash ShaderDX11::GetMostSelectedPermutation(ShaderType _kStage, uint32_t& _uSelectCount) const
{
	_uSelectCount = 0u;
	ShaderPermutationHash MostUsedPerm = {};

	for (const TPermutationSelectionStats::value_type& Perm : m_PermutationSelectionStats[_kStage])
	{
		if (Perm.second >= _uSelectCount)
		{
			_uSelectCount = Perm.second;
			MostUsedPerm = Perm.first;
		}
	}

	return MostUsedPerm;
}
//---------------------------------------------------------------------------------------------------
void ShaderDX11::ReEnableStages()
{
	for (uint32_t i = 0; i < ShaderType_NumOf; ++i)
	{
		m_ActiveStages[i] = true;
	}
}
//---------------------------------------------------------------------------------------------------
