//   Copyright 2020 Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "HelixSolidRenderPassDX11.h"
#include "hlx\src\Timer.h"

using namespace Helix;
using namespace Helix::Display;
using namespace Helix::Scene;

//---------------------------------------------------------------------------------------------------

HelixSolidRenderPassDX11::HelixSolidRenderPassDX11(DefaultInitializerType) : RenderPassDX11(DefaultInit, "Solid")
{
}

//---------------------------------------------------------------------------------------------------
HelixSolidRenderPassDX11::HelixSolidRenderPassDX11(const std::string& _sInstanceName) :
	RenderPassDX11("Solid", _sInstanceName,
	{ &m_AlbedoMap, &m_NormalMap, &m_MetallicMap, &m_RoughnessMap, &m_HeightMap, &m_EmissiveMap }, // InputTextures
	{ &m_AlbedoTarget, &m_NormalWSTarget, &m_MetallicTarget, &m_RoughnessTarget, &m_LinearDepthTarget, &m_EmissiveTarget }, // Output Textures (RenderTargets)
	{ &m_CBSolid, &m_CBPerCamera, &m_CBPerObject/*, &m_CBPerFrame*/ }) // Variables
{
}
//---------------------------------------------------------------------------------------------------
HelixSolidRenderPassDX11::~HelixSolidRenderPassDX11()
{
}

//---------------------------------------------------------------------------------------------------
std::shared_ptr<RenderPassDX11> HelixSolidRenderPassDX11::CreatePass(const std::string& _sInstanceName) const
{
	return std::make_shared<HelixSolidRenderPassDX11>(_sInstanceName);
}
//---------------------------------------------------------------------------------------------------
bool HelixSolidRenderPassDX11::OnInitalize(const hlx::TextToken& _CustomVariables)
{
	// Add RasterizerState
	RasterizerDesc RasDesc = {};
	RasDesc.m_kFillMode = FillMode_Solid;
	RasDesc.m_kCullMode = CullMode_Back;
	RasDesc.m_bFrontCounterClockwise = false;

	SetRasterizerState(RasDesc);

	// Add BlendState
	RenderTargetBlendDesc RTBlendDesc = {};
	RTBlendDesc.m_bBlendEnable = false;
	RTBlendDesc.m_kSrcBlend = BlendVariable_Src_Alpha;
	RTBlendDesc.m_kDestBlend = BlendVariable_One;
	RTBlendDesc.m_kBlendOp = BlendOperation_Add;
	RTBlendDesc.m_kSrcBlendAlpha = BlendVariable_Zero;
	RTBlendDesc.m_kDestBlendAlpha = BlendVariable_Zero;
	RTBlendDesc.m_kBlendOpAlpha = BlendOperation_Add;
	RTBlendDesc.m_uRenderTargetWriteMask = Channel_All;

	BlendDesc AlphaBlendDesc = {};
	AlphaBlendDesc.m_bAlphaToCoverageEnable = false;
	AlphaBlendDesc.m_RenderTargets[0] = RTBlendDesc;
	AlphaBlendDesc.m_uSampleMask = 0xffffffff;
	AlphaBlendDesc.fBlendFactor = { 0.0f,0.0f,0.0f,0.0f };

	SetBlendState(AlphaBlendDesc);

	// Add DepthStencilState
	DepthStencilDesc DepthDesc = {};
	DepthDesc.m_bDepthEnable = true;
	DepthDesc.m_kDepthWriteMask = DepthWriteMask_All;
	DepthDesc.m_kDepthFunc = ComparisonFunction_Less;
	DepthDesc.m_bStencilEnable = false;

	SetDepthStencilState(DepthDesc, "Solid");

	// Default Parameter
	ClearRenderTargets(true);
	ClearDepthStencil(true);

	return m_bInitialized;
}
//---------------------------------------------------------------------------------------------------
std::string HelixSolidRenderPassDX11::GetPermutationName(const ShaderPermutationHash& _Perm) const
{
	std::string sName = m_sTypeName + ' ';

	std::string sPerm;

	if (_Perm.uClassPermutation & Shaders::Solid::HPMPS_AlbedoMap)
	{
		sPerm += "Albedo ";
	}
	if (_Perm.uClassPermutation & Shaders::Solid::HPMPS_NormalMap)
	{
		sPerm += "Normal ";
	}
	if (_Perm.uClassPermutation & Shaders::Solid::HPMPS_MetallicMap)
	{
		sPerm += "Metallic ";
	}
	if (_Perm.uClassPermutation & Shaders::Solid::HPMPS_RoughnessMap)
	{
		sPerm += "Roughness ";
	}
	if (_Perm.uClassPermutation & Shaders::Solid::HPMPS_ParallaxMap)
	{
		sPerm += "Parallax ";
	}
	if (_Perm.uClassPermutation & Shaders::Solid::HPMPS_EmissiveMap)
	{
		sPerm += "Emissive ";
	}

	// haha sperm xD
	return sName + (sPerm.empty() ? "NULL" : sPerm);
}
//---------------------------------------------------------------------------------------------------
bool HelixSolidRenderPassDX11::OnPerFrame()
{
	float fGlobalTime = hlx::Timer::GetGlobalTimeF();
	m_CBPerFrame->fTime = fGlobalTime;
	m_CBPerFrame->fDeltaTime = fGlobalTime - m_fPreviousGlobalTime;
	m_fPreviousGlobalTime = fGlobalTime;

	return true;
}
//---------------------------------------------------------------------------------------------------
