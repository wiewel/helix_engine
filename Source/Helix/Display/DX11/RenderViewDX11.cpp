//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "RenderViewDX11.h"

#include "hlx\src\Logger.h"
#include "RenderDeviceDX11.h"
#include "TextureResizeEventManagerDX11.h"
#include "TextureVariantsDX11.h"
#include "Display\DXGIManager.h"

using namespace Helix::Display;

//---------------------------------------------------------------------------------------------------
RenderViewDX11::RenderViewDX11()
{
	m_pDXGIManager = DXGIManager::Instance();
	m_pRenderDevice = RenderDeviceDX11::Instance();
}
//---------------------------------------------------------------------------------------------------
RenderViewDX11::~RenderViewDX11()
{	
	if (m_pRenderDevice != nullptr)
	{
		Shutdown();
	}
}
//---------------------------------------------------------------------------------------------------
bool RenderViewDX11::Initialize(HWND _hWnd)
{
	m_pRenderDevice->Initialize(GetDebug());

	if (m_pRenderDevice->IsInitialized() == false)
	{
		return false;
	}

	m_pDevice = m_pRenderDevice->GetDevice();
	m_pDeviceContext = m_pRenderDevice->GetContext();

	HRESULT Result = S_OK;

	// Create swapchain	
	if (m_pDXGIManager->InitSwapChain(m_pDevice, _hWnd, m_ViewSettings) == nullptr)
	{
		return false;
	}

	// Set the RenderTarget (BackBuffer)
	CreateBackBuffer();

	if (m_ViewSettings.bFullscreen)
	{
		SetFullscreen();
	}

	return OnInitialize(_hWnd);
}
//---------------------------------------------------------------------------------------------------
void RenderViewDX11::Shutdown()
{
	OnShutdown();

	BackBufferTextureDX11::Instance()->Destroy();

	HSAFE_FUNC_RELEASE(m_pDXGIManager, Shutdown);
	HSAFE_FUNC_RELEASE(m_pRenderDevice, Shutdown);
}
//---------------------------------------------------------------------------------------------------
void RenderViewDX11::ResizeRenderer(const uint32_t _uXResolution, const uint32_t _uYResolution)
{
	ResizeBuffers(_uXResolution, _uYResolution);
}
//---------------------------------------------------------------------------------------------------
void RenderViewDX11::CreateBackBuffer()
{
	IDXGISwapChain* pSwapChain = m_pDXGIManager->GetSwapChain();

	if (pSwapChain != nullptr)
	{
		ID3D11Texture2D* pBackBuffer = nullptr;
		// Set the RenderTarget (BackBuffer)
		HR(pSwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), reinterpret_cast<void**>(&pBackBuffer)));

		D3D11_TEXTURE2D_DESC Desc = {};
		pBackBuffer->GetDesc(&Desc);

		// initial 'resize' to create Rendertargets assigned to renderpasses
		TextureResizeEventManagerDX11::Instance()->Resize("BackBufferResize", Desc.Width, Desc.Height);

		BackBufferTextureDX11::Instance()->Set(pBackBuffer);
	}
	else
	{
		HFATAL("No swapchain set!");
	}
}
//---------------------------------------------------------------------------------------------------
/**
*	ResizeTarget() resizes the swapchain to new resolution given by current DisplayMode
*	NOTE: should always be called before SetFullscreen()!
*/
void RenderViewDX11::ResizeTarget(bool _bFlickerFix)
{
	IDXGISwapChain* pSwapChain = m_pDXGIManager->GetSwapChain();

	if (pSwapChain != nullptr)
	{
		DXGI_MODE_DESC ModeDesc;

		if (_bFlickerFix)
		{
			// -> http://msdn.microsoft.com/en-us/library/windows/desktop/ee417025%28v=vs.85%29.aspx
			// -> forces swapchain to calculate refresh rate dynamically

			DXGI_SWAP_CHAIN_DESC TempSCD;
			pSwapChain->GetDesc(&TempSCD);
			ModeDesc = TempSCD.BufferDesc;
			ModeDesc.RefreshRate.Denominator = 0;
			ModeDesc.RefreshRate.Numerator = 0;
		}
		else
		{
			ModeDesc = m_pDXGIManager->GetMainDisplayMode();
		}

		RenderDeviceDX11::Instance()->Lock();

		HR(pSwapChain->ResizeTarget(&ModeDesc));

		TextureResizeEventManagerDX11::Instance()->Resize("BackBufferResize", ModeDesc.Width, ModeDesc.Height);
		RenderDeviceDX11::Instance()->Unlock();
	}
	else
	{
		HWARNINGD("No swapchain set!");
	}
}
//---------------------------------------------------------------------------------------------------
/**
*	SetFullscreen(bool _bFullscreen) switches the swapchain to fullscreen mode if _bFullscreen is true and executes an refresh rate fix
*	-> https://msdn.microsoft.com/en-us/library/windows/desktop/ee417025%28v=vs.85%29.aspx#full-screen_issues
*/
void RenderViewDX11::SetFullscreen(bool _bFullscreen)
{
	HRESULT Result;
	IDXGISwapChain* pSwapChain = m_pDXGIManager->GetSwapChain();
	
	if (pSwapChain != nullptr)
	{
		m_ViewSettings.bFullscreen = _bFullscreen;

		ResizeTarget();
		Result = pSwapChain->SetFullscreenState(m_ViewSettings.bFullscreen, /*m_ViewSettings.bFullscreen ? m_pDXGIManager->GetMainOutput() :*/ NULL);
		
		if (m_ViewSettings.bFullscreen)
		{
			// When the swapchain gets the command to change to fullscreen it is advisable to call ResizeTarget again with the RefreshRate member of DXGI_MODE_DESC zeroed out.
			// This results to a no-operation instruction to DXGI but can avoid refresh rate problems.
			// -> https://msdn.microsoft.com/en-us/library/windows/desktop/ee417025%28v=vs.85%29.aspx#full-screen_issues
			ResizeTarget(true);
		}

		if (FAILED(Result))
		{
			m_ViewSettings.bFullscreen = !_bFullscreen;
			
			HERROR("Failed on fullscreen switch with error message %d!", Result);
		}
	}
	else 
	{
		HWARNINGD("No swapchain set!");
	}
}

//---------------------------------------------------------------------------------------------------
/**
*	ResizeBuffers(const uint32_t _uXResolution, const uint32_t _uYResolution) changes the resolution of the backbuffer to the new resolution
*	-> http://msdn.microsoft.com/en-us/library/windows/desktop/bb205075%28v=vs.85%29.aspx#Handling_Window_Resizing
*   -> http://msdn.microsoft.com/en-us/library/windows/desktop/ee417025%28v=vs.85%29.aspx
*/
void RenderViewDX11::ResizeBuffers(const uint32_t _uXResolution, const uint32_t _uYResolution)
{
	//No resize
	if (_uXResolution == 0 || _uYResolution == 0)
	{
		return;
	}

	IDXGISwapChain* pSwapChain = m_pDXGIManager->GetSwapChain();

	// release all references to swapchain (mostly only RenderTargetView -> backbuffer)
	if (m_pDeviceContext != nullptr && pSwapChain != nullptr)
	{
		RenderDeviceDX11::Instance()->Lock();

		// Set render targets to nullptr
		RenderDeviceDX11::Instance()->ClearRenderTargets();
		RenderDeviceDX11::Instance()->ClearDepthStencil();
		
		//AutoResize textures (except BackBuffer)
		TextureResizeEventManagerDX11::Instance()->Resize("BackBufferResize",_uXResolution, _uYResolution);

		//manually release the old backbuffer
		BackBufferTextureDX11::Instance()->Destroy();

		// [in] uint32_t BufferCount, [in] uint32_t Width, [in] uint32_t Height, ...
		HR(pSwapChain->ResizeBuffers(0, _uXResolution, _uYResolution, DXGI_FORMAT_UNKNOWN, DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH)); // if width and height are 0 it automatically chooses HWND's width and height; preserves existing buffer count and format
		
		ID3D11Texture2D* pBackBuffer = nullptr;
		HR(pSwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), reinterpret_cast<void**>(&pBackBuffer)));

		//set the new backbuffer
		BackBufferTextureDX11::Instance()->Set(pBackBuffer);
		
		RenderDeviceDX11::Instance()->Unlock();
	}
	else
	{
		HWARNINGD("No device context or swapchain set!");
	}

}
//---------------------------------------------------------------------------------------------------
void RenderViewDX11::GetRendererResolution(uint32_t& _uXResolution, uint32_t& _uYResolution) const
{
	_uXResolution = m_pDXGIManager->GetMainDisplayMode().Width;
	_uYResolution = m_pDXGIManager->GetMainDisplayMode().Height;
}
//---------------------------------------------------------------------------------------------------

void RenderViewDX11::ChangeRendererSettings(uint32_t _uDisplayModeId, uint32_t _uAdapterId, uint32_t _uOutputId)
{
	m_pDXGIManager->SetMainDisplayModeByIndex(_uDisplayModeId);
	m_pDXGIManager->SetMainAdapterByIndex(_uAdapterId);
	m_pDXGIManager->SetMainOutputByIndex(_uOutputId);
}
//---------------------------------------------------------------------------------------------------
