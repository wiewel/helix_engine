//   Copyright 2020 Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "HelixDebugRenderPassDX11.h"

using namespace Helix;
using namespace Helix::Display;

//---------------------------------------------------------------------------------------------------

HelixDebugRenderPassDX11::HelixDebugRenderPassDX11(DefaultInitializerType) : RenderPassDX11(DefaultInit, "UnicolorVertex")
{
}

//---------------------------------------------------------------------------------------------------
HelixDebugRenderPassDX11::HelixDebugRenderPassDX11(const std::string& _sInstanceName) :
	RenderPassDX11("UnicolorVertex", _sInstanceName,
	{ }, // InputTextures
	{ &m_Backbuffer }, // Output Textures (RenderTargets)
	{ &m_CBPerCamera/*, &m_CBPerObject*/} ) // Variables
{
}
//---------------------------------------------------------------------------------------------------
HelixDebugRenderPassDX11::~HelixDebugRenderPassDX11()
{
}
//---------------------------------------------------------------------------------------------------
std::shared_ptr<RenderPassDX11> HelixDebugRenderPassDX11::CreatePass(const std::string& _sInstanceName) const
{
	return std::make_shared<HelixDebugRenderPassDX11>(_sInstanceName);
}
//---------------------------------------------------------------------------------------------------
bool HelixDebugRenderPassDX11::OnInitalize(const hlx::TextToken& _CustomVariables)
{
	// Add RasterizerState
	RasterizerDesc RasDesc = {};
	RasDesc.m_kFillMode = FillMode_Solid;
	RasDesc.m_kCullMode = CullMode_Back;
	RasDesc.m_bFrontCounterClockwise = false;

	SetRasterizerState(RasDesc);

	// Add BlendState
	RenderTargetBlendDesc RTBlendDesc = {};
	RTBlendDesc.m_bBlendEnable = false;

	BlendDesc AlphaBlendDesc = {};
	AlphaBlendDesc.m_bAlphaToCoverageEnable = false;
	AlphaBlendDesc.m_RenderTargets[0] = RTBlendDesc;
	AlphaBlendDesc.m_uSampleMask = 0xffffffff;
	AlphaBlendDesc.fBlendFactor = { 0.0f,0.0f,0.0f,0.0f };

	SetBlendState(AlphaBlendDesc);

	// Add DepthStencilState
	DepthStencilDesc DepthDesc = {};
	DepthDesc.m_bDepthEnable = false;
	DepthDesc.m_kDepthWriteMask = DepthWriteMask_Zero;
	DepthDesc.m_kDepthFunc = ComparisonFunction_LessEqual;
	DepthDesc.m_bStencilEnable = false;

	SetDepthStencilState(DepthDesc, "DebugRender");

	// Default Parameter
	ClearRenderTargets(false);

	return m_bInitialized;
}
//---------------------------------------------------------------------------------------------------
