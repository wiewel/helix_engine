//   Copyright 2020 Fabian Wahlster
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "APSStatistics.h"
#include "stdrzr\source\FileStream.h"

using namespace Helix::Display;
//---------------------------------------------------------------------------------------------------

APSStatistics::APSStatistics(float _fRuntimeInSec, float _fIntervalInSec)
{
	QueryPerformanceFrequency(reinterpret_cast<LARGE_INTEGER*>(&m_iFrequency));

	m_iIntervalLength = static_cast<int64_t>(_fIntervalInSec * m_iFrequency);
	m_iRuntime = static_cast<int64_t>(_fRuntimeInSec * m_iFrequency);

	m_uCount = static_cast<uint32_t>(_fRuntimeInSec / _fIntervalInSec);
	m_Statistics.reserve(m_uCount);
}
//---------------------------------------------------------------------------------------------------

APSStatistics::~APSStatistics()
{
	m_Statistics.clear();
}

//---------------------------------------------------------------------------------------------------

void APSStatistics::Save(const std::string& _sTargetCSVPath)
{
	// nothing to save
	if (m_Statistics.empty())
	{
		return;
	}

	// add last unfinished interal
	m_Statistics.push_back(m_CurInterval);

	std::ostringstream Output;

	int64_t iStartTime = m_Statistics.front().iGlobalStartTime;

	//Output << "Runtime;IntervalTime;IntervalCount\n";
	//Output << SpanToSec(m_iRuntime) << ';';
	//Output << SpanToSec(m_iIntervalLength) << ';';
	//Output << m_uCount << std::endl;

	Output << "Time;FPS;SelectedPF;SceneCost;APSTime;PolyCount;ShaderChanges;APS;Permutations\n"; // 

	float fAvgFPS = 0.f;
	for (const IntervalStats& Stats : m_Statistics)
	{
		float fTime = SpanToSec(Stats.iGlobalStartTime - iStartTime);
		Output << std::to_string(fTime) << ';';

		float fSampleCount = static_cast<float>(Stats.uSampelCount);
		float fFrameTime = SpanToSec(static_cast<int64_t>(static_cast<double>(Stats.iFrameTimes) / fSampleCount));
		float fFPS = 1.f / fFrameTime;
		Output << std::to_string(fFPS) << ';'; // FPS

		Output << Stats.fTotalSelectedAvgPF / fSampleCount << ';'; // PF

		float fSceneCost = Stats.fSceneCost / fSampleCount;
		Output << std::to_string(fSceneCost) << ';'; // SceneCost

		float fAPSTime = Stats.fAPSFindTime / fSampleCount;
		//float fAPSPercent = (fAPSTime / fFrameTime) * 100.f;

		Output << std::to_string(fAPSTime) << ';';
		Output << static_cast<uint32_t>(Stats.uPolyCount / fSampleCount) << ';'; // PolyCount
		Output << static_cast<uint32_t>(static_cast<float>(Stats.uShaderChanges) / fSampleCount) << ';'; // ShaderChanges
		// APS
		if ((static_cast<float>(Stats.uAPS) / fSampleCount) >= 0.5f)
		{
			Output << "1;";
		}
		else
		{
			Output << "0;";
		}

		//SelectedPermutations
		for (const SelectedPassPerm& Perm : Stats.SelectedPerms)
		{
			Output << Perm.pPass->GetPermutationName(Perm.Permutation) << ' ';
		}

		Output << std::endl;

		fAvgFPS += fFrameTime;
	}

	fAvgFPS /= static_cast<float>(m_Statistics.size());
	fAvgFPS = 1.f / fAvgFPS;

	//Output << "AvgFPS: " << fAvgFPS << "\n";

	stdrzr::fbytestream FStream(_sTargetCSVPath, std::ios_base::out);

	if (FStream.is_open())
	{
		FStream.put(Output.str());
		FStream.close();
	}

	m_Statistics.clear();
}