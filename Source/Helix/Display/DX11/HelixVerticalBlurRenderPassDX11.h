//   Copyright 2020 Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef HELIXVERTICALBLURRENDERPASSDX11_H
#define HELIXVERTICALBLURRENDERPASSDX11_H

#include "Math\MathTypes.h"
#include "RenderPassDX11.h"
#include "CommonConstantBufferFunctions.h"
#include "Display\Shaders\VerticalBlur.hlsl"

namespace Helix
{
	namespace Display
	{
		class HelixVerticalBlurRenderPassDX11 : public RenderPassDX11
		{
			using TCBVerticalBlur = ShaderVariableDX11<Shaders::VerticalBlur::cbVerticalBlur>;

		public:
			HDEBUGNAME("HelixVerticalBlurRenderPassDX11");

			HelixVerticalBlurRenderPassDX11(DefaultInitializerType);
			HelixVerticalBlurRenderPassDX11(const std::string& _sInstanceName = {});
			~HelixVerticalBlurRenderPassDX11();

			bool OnInitalize(const hlx::TextToken& _CustomVariables) final;
			//bool OnPerCamera(const Scene::CameraComponent& _Camera) final;
			bool OnPerFrame() final;

			void SetupBlur(uint32_t _uBlurWidth, float _fSigma, float _fIntensity);
			std::shared_ptr<RenderPassDX11> CreatePass(const std::string& _sInstanceName) const final;

		private:
			void GenerateGauss(int32_t _iBlurWidth, float _fSigma);
			Math::int2 GetGaussianWeightIndex(int32_t _iIndex) const;

		private:
			ShaderTextureDX11 m_InputTexture = { "gTex", "HORIZONTAL_BLUR" };
			ShaderRenderTargetDX11 m_BlurOutput = { "vBlurColor", "BLURRED_TEX" };

			TCBVerticalBlur m_CBVerticalBlur = { "cbVerticalBlur" };
			//TCBPerCamera m_CBPerCamera = { "cbPerCamera" };

			int32_t m_iBlurWidth = 2;
			float m_fSigma = 2.0f;
			float m_fBlurIntensity = 1.0f; /// strided blur -> skips pixels
		};

		inline void HelixVerticalBlurRenderPassDX11::SetupBlur(uint32_t _uBlurWidth, float _fSigma, float _fIntensity)
		{
			m_iBlurWidth = static_cast<int32_t>(_uBlurWidth);
			m_fSigma = _fSigma;
			m_fBlurIntensity = _fIntensity;
		}

		inline Math::int2 HelixVerticalBlurRenderPassDX11::GetGaussianWeightIndex(int32_t _iIndex) const
		{
			Math::int2 vReturn;
			vReturn.x = _iIndex / 4;
			vReturn.y = _iIndex % 4;
			return vReturn;
		}

	} // Display
} // Helix

#endif // HELIXVERTICALBLURRENDERPASSDX11_H