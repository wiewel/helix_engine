//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "DepthStencilStatePoolDX11.h"

#include "RenderDeviceDX11.h"
#include "hlx\src\Logger.h"
#include "hlx\src\StandardDefines.h"

using namespace Helix::Display;

DepthStencilStatePoolDX11::DepthStencilStatePoolDX11()
{
	m_pRenderDevice = RenderDeviceDX11::Instance();

	HASSERT(m_pRenderDevice->IsInitialized(), "RenderDevice has not been initialized yet!");
}
//---------------------------------------------------------------------------------------------------
DepthStencilStatePoolDX11::~DepthStencilStatePoolDX11()
{
}
//---------------------------------------------------------------------------------------------------
uint64_t DepthStencilStatePoolDX11::HashFunction(const D3D11_DEPTH_STENCIL_DESC& _Desc)
{
	return BaseTypeHash(_Desc);
}
//---------------------------------------------------------------------------------------------------
ID3D11DepthStencilState* DepthStencilStatePoolDX11::CreateFromDesc(const D3D11_DEPTH_STENCIL_DESC& _Desc, const uint64_t& _Hash)
{
	ID3D11DepthStencilState* pDepthStencilState = nullptr;
	HRESULT Result = S_OK;

	if (FAILED(Result = m_pRenderDevice->GetDevice()->CreateDepthStencilState(&_Desc, &pDepthStencilState)))
	{
		HR(Result);
		return nullptr;
	}

	return pDepthStencilState;
}
//---------------------------------------------------------------------------------------------------