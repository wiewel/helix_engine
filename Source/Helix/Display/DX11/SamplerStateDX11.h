//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef SAMPLERSTATEDX11_H
#define SAMPLERSTATEDX11_H

#include "SamplerStatePoolDX11.h"
#include "Display\ViewDefines.h"

namespace Helix
{
	namespace Display
	{
		class SamplerStateDX11 : public TDX11SamplerStateRef
		{
		public:
			HDEBUGNAME("SamplerStateDX11");

			// default constructor
			constexpr SamplerStateDX11() noexcept : TDX11SamplerStateRef(nullptr){}
			constexpr SamplerStateDX11(std::nullptr_t) noexcept : TDX11SamplerStateRef(nullptr){}

			// Constructor for perdefined default states:
			SamplerStateDX11(DefaultSamplerState kDefaultState); // new

			SamplerStateDX11(const SamplerDesc& _Desc, const char* _pDbgName = nullptr);

			~SamplerStateDX11();

			SamplerStateDX11(const SamplerStateDX11& rhs);
			SamplerStateDX11& operator=(const SamplerStateDX11& rhs);

			bool IsInitialized() const;

			const SamplerDesc& GetDescription() const;
		private:
			SamplerDesc m_Description = {};
		};

		inline const SamplerDesc & SamplerStateDX11::GetDescription() const {return m_Description;	}
		inline bool SamplerStateDX11::IsInitialized() const { return m_pReference != nullptr; }

	} // Display
} // Helix


#endif // SAMPLERSTATEDX11_H