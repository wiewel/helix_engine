//   Copyright 2020 Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef HELIXSOLIDRENDERPASSDX11_H
#define HELIXSOLIDRENDERPASSDX11_H

#include "RenderPassDX11.h"
#include "CommonConstantBufferFunctions.h"
#include "Display\Shaders\Solid.hlsl"

#include "Scene\LightComponent.h"

namespace Helix
{
	namespace Display
	{
		class HelixSolidRenderPassDX11 : public RenderPassDX11
		{
			using TCBSolid = ShaderVariableDX11<Shaders::Solid::cbSolid>;

		public:
			HDEBUGNAME("HelixSolidRenderPassDX11");

			HelixSolidRenderPassDX11(DefaultInitializerType);
			HelixSolidRenderPassDX11(const std::string& _sInstanceName = {});
			~HelixSolidRenderPassDX11();

			bool OnInitalize(const hlx::TextToken& _CustomVariables) final;

			bool OnPerFrame() final;
			bool OnPerCamera(const Scene::CameraComponent& _Camera) final;
			bool OnPerObject(const RenderObjectDX11& _RenderObject, const MaterialDX11& _Material) override;

			std::string GetPermutationName(const ShaderPermutationHash& _Perm) const final;
			std::shared_ptr<RenderPassDX11> CreatePass(const std::string& _sInstanceName) const final;

		private:

			ShaderTextureDX11 m_AlbedoMap = { "gAlbedoMap" };
			ShaderTextureDX11 m_NormalMap = { "gNormalMap" };
			ShaderTextureDX11 m_MetallicMap = { "gMetallicMap" };
			ShaderTextureDX11 m_RoughnessMap = { "gRoughnessMap" };
			ShaderTextureDX11 m_HeightMap = { "gHeightMap" };
			ShaderTextureDX11 m_EmissiveMap = { "gEmissiveMap" };

			ShaderRenderTargetDX11 m_AlbedoTarget = { "vAlbedo", "MTL_ALBEDO" };
			ShaderRenderTargetDX11 m_NormalWSTarget = { "vNormalWS", "WORLDSPACE_NORMAL" }; // Normal in WorldSpace
			ShaderRenderTargetDX11 m_MetallicTarget = { "vMetallicHorizon" , "MTL_METALLIC" };
			ShaderRenderTargetDX11 m_EmissiveTarget = { "vEmissiveColor" , "EMISSIVE_LIGHTMAP" };
			ShaderRenderTargetDX11 m_RoughnessTarget = { "fRoughness" , "MTL_ROUGHNESS" };
			ShaderRenderTargetDX11 m_LinearDepthTarget = { "fLinearDepth", "LINEAR_DEPTH" };

			TCBPerObject m_CBPerObject = { "cbPerObject" };
			TCBPerCamera m_CBPerCamera = { "cbPerCamera" };
			TCBPerFrame m_CBPerFrame = { "cbPerFrame" };

			float m_fPreviousGlobalTime = 0.0f;

		protected:
			TCBSolid m_CBSolid = { "cbSolid" };
		};
		//---------------------------------------------------------------------------------------------------
		inline bool HelixSolidRenderPassDX11::OnPerCamera(const Scene::CameraComponent& _Camera)
		{
			SetCameraConstants(m_CBPerCamera, _Camera.GetRenderingProperties());

			return true;
		}
		//---------------------------------------------------------------------------------------------------
		inline bool HelixSolidRenderPassDX11::OnPerObject(const RenderObjectDX11& _RenderObject, const MaterialDX11& _Material)
		{
			SetObjectConstants(m_CBPerObject, _RenderObject, _Material);

			uint32_t uClassPermutation = 0u;

			if (_Material)
			{
				const MaterialProperties& MaterialProps = _Material.GetProperties();

				if (MaterialProps.bUseAlbedoMap && _Material.GetAlbedoMap() != nullptr)
				{
					m_AlbedoMap.SetTexture(_Material.GetAlbedoMap());
					uClassPermutation |= Shaders::Solid::HPMPS_AlbedoMap;
				}

				if (MaterialProps.bUseNormalMap &&  _Material.GetNormalMap() != nullptr)
				{
					m_NormalMap.SetTexture(_Material.GetNormalMap());
					uClassPermutation |= Shaders::Solid::HPMPS_NormalMap;
				}

				if (MaterialProps.bUseMetallicMap && _Material.GetMetallicMap() != nullptr)
				{
					m_MetallicMap.SetTexture(_Material.GetMetallicMap());
					uClassPermutation |= Shaders::Solid::HPMPS_MetallicMap;
				}

				if (MaterialProps.bUseRoughnessMap && _Material.GetRoughnessMap() != nullptr)
				{
					m_RoughnessMap.SetTexture(_Material.GetRoughnessMap());
					uClassPermutation |= Shaders::Solid::HPMPS_RoughnessMap;
				}

				if (MaterialProps.bUseHeightMap && _Material.GetHeightMap() != nullptr)
				{
					m_HeightMap.SetTexture(_Material.GetHeightMap());
					uClassPermutation |= Shaders::Solid::HPMPS_ParallaxMap;
				}

				if (MaterialProps.bUseEmissiveMap && _Material.GetEmissiveMap() != nullptr)
				{
					m_EmissiveMap.SetTexture(_Material.GetEmissiveMap());
					uClassPermutation |= Shaders::Solid::HPMPS_EmissiveMap;
				}
			}

			Select(ShaderType_PixelShader, uClassPermutation);

			return true;
		}
	} // Display
} // Helix

#endif