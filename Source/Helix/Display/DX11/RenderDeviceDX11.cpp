//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "RenderDeviceDX11.h"
#include "RenderDeviceShutdownCallbackDX11.h"

#include "Display\DXGIManager.h"

#include "TextureVariantsDX11.h"
#include "ShaderDX11.h"
#include "ShaderTextureDX11.h"
#include "ShaderRenderTargetDX11.h"
#include "ShaderSamplerDX11.h"
#include "GPUBufferDX11.h"
#include "SamplerStateDX11.h"
#include "MaterialDX11.h"
#include "RenderPassDX11.h"
#include "DepthStencilStateDX11.h"
#include "BlendStateDX11.h"
#include "RasterizerStateDX11.h"

using namespace Helix::Display;

//---------------------------------------------------------------------------------------------------
RenderDeviceDX11::RenderDeviceDX11()
{
	for (uint32_t i = 0; i < ShaderType_NumOf; ++i)
	{
		for (uint32_t j = 0; j < D3D11_COMMONSHADER_INPUT_RESOURCE_SLOT_COUNT; ++j)
		{
			m_pCurrentShaderResources[i][j] =  nullptr;
		}

		for (uint32_t j = 0; j < D3D11_COMMONSHADER_CONSTANT_BUFFER_API_SLOT_COUNT; ++j)
		{
			m_pCurrentConstantBuffers[i][j] = nullptr;
		}

		for (uint32_t j = 0; j < D3D11_COMMONSHADER_SAMPLER_SLOT_COUNT; ++j)
		{
			m_pCurrentSamplerStates[i][j] = nullptr;
		}
	}

	for (uint32_t i = 0; i < D3D11_SIMULTANEOUS_RENDER_TARGET_COUNT; ++i)
	{
		m_pCurrentRenderTargets[i] = nullptr;
		m_CurrentViewports[i] = {};
	}

	for (uint32_t i = 0; i < D3D11_PS_CS_UAV_REGISTER_COUNT; ++i)
	{
		m_pCurrentUnorderedAccessViews[i] = nullptr;
	}

	for (uint32_t i = 0; i < D3D11_IA_VERTEX_INPUT_RESOURCE_SLOT_COUNT; ++i)
	{
		m_pCurrentVertexBuffers[i] = nullptr;
		m_uCurrentVertexOffsets[i] = 0u;
		m_uCurrentVertexStrideSizes[i] = 0u;
	}

	m_pDXGIManager = DXGIManager::Instance();
}
//---------------------------------------------------------------------------------------------------
RenderDeviceDX11::~RenderDeviceDX11()
{
	Shutdown();
}
//---------------------------------------------------------------------------------------------------
void RenderDeviceDX11::Initialize(const bool _bDebug)
{
	HASSERTD(m_pDevice == nullptr && m_pDeviceContext == nullptr, "Device already initialized");

	HRESULT Result;

	// TODO: change feature level here
	D3D_FEATURE_LEVEL pFeatureLevel = D3D_FEATURE_LEVEL_11_0; //http://www.indiedev.de/wiki/Feature_Level
	D3D_FEATURE_LEVEL vRequestedFeatureLevels[] =
	{
		D3D_FEATURE_LEVEL_11_0,
	};

	uint32_t uNumFeatureLevels = static_cast<uint32_t>(GetArrayLength(vRequestedFeatureLevels));

	uint32_t uDeviceFlags = 0;

	if(_bDebug)
	{
		uDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
	}

	HR(Result = D3D11CreateDevice(
		m_pDXGIManager->GetMainAdapter(),
		D3D_DRIVER_TYPE_UNKNOWN, //D3D_DRIVER_TYPE_HARDWARE,
		nullptr,
		uDeviceFlags,
		vRequestedFeatureLevels,
		uNumFeatureLevels,
		D3D11_SDK_VERSION,
		&m_pDevice,
		&pFeatureLevel,
		&m_pDeviceContext));

	if (_bDebug)
	{
		HR(Result = m_pDevice->QueryInterface(IID_PPV_ARGS(&m_pDebug)));
		HR(Result = m_pDevice->QueryInterface(IID_PPV_ARGS(&m_pInfoQueue)));

		//m_pInfoQueue->SetBreakOnSeverity(D3D11_MESSAGE_SEVERITY_CORRUPTION, true);
		//m_pInfoQueue->SetBreakOnSeverity(D3D11_MESSAGE_SEVERITY_ERROR, true);
		//m_pInfoQueue->SetBreakOnSeverity(D3D11_MESSAGE_SEVERITY_WARNING, true);
		//https://msdn.microsoft.com/de-de/library/windows/desktop/ff476186%28v=vs.85%29.aspx
		m_pInfoQueue->SetBreakOnID(D3D11_MESSAGE_ID_DEVICE_OMSETRENDERTARGETS_HAZARD, TRUE);

		D3D11_MESSAGE_ID HideIds[] =
		{
			D3D11_MESSAGE_ID_SETPRIVATEDATA_CHANGINGPARAMS,
			// Add more message IDs here as needed
		};

		D3D11_INFO_QUEUE_FILTER Filter;
		memset(&Filter, 0, sizeof(Filter));
		Filter.DenyList.NumIDs = _countof(HideIds);
		Filter.DenyList.pIDList = HideIds;
		m_pInfoQueue->AddStorageFilterEntries(&Filter);
	}

	HDXDEBUGNAME(m_pDevice, "DXDevice");
	HDXDEBUGNAME(m_pDeviceContext, "DXDeviceContext");
}
//---------------------------------------------------------------------------------------------------
void RenderDeviceDX11::Shutdown()
{
	for (RenderDeviceShutdownCallbackDX11* _pCallback : m_ShutdownCallbacks)
	{
		_pCallback->OnShutdown();
	}

	m_ShutdownCallbacks.clear();

	// Direct3D11 defers the destruction of objects, an application cannot rely upon objects immediately being destroyed.
	// Calling Flush will destroy any objects whose destruction has been deferred. If an application requires synchronous
	// destruction of an object the application should release all its references, call ID3D11DeviceContext::ClearState, and then call Flush.
	if (m_pDeviceContext != nullptr)
	{
		m_pDeviceContext->ClearState();
		m_pDeviceContext->Flush();
	}
	
	if (m_pDebug != nullptr)
	{
		m_pDebug->ReportLiveDeviceObjects(D3D11_RLDO_DETAIL);
		m_pDebug->Release();
		m_pDebug = nullptr;
	}

	HSAFE_RELEASE(m_pDeviceContext);
	HSAFE_RELEASE(m_pDevice);	

	HSAFE_RELEASE(m_pInfoQueue);
}

//---------------------------------------------------------------------------------------------------

void RenderDeviceDX11::SetRasterizerState(const RasterizerStateDX11& _RasterizerState)
{
	if (_RasterizerState)
	{
		ID3D11RasterizerState* pRasterizerState = _RasterizerState.GetReference();
		if (m_pCurrentRasterizerState != pRasterizerState)
		{
			m_pDeviceContext->RSSetState(pRasterizerState);
			m_pCurrentRasterizerState = pRasterizerState;
		}
	}
}
//---------------------------------------------------------------------------------------------------

void RenderDeviceDX11::RegisterShutdownCallback(RenderDeviceShutdownCallbackDX11* _pCallback)
{
	m_ShutdownCallbacks.push_back(_pCallback);
}
//---------------------------------------------------------------------------------------------------

void RenderDeviceDX11::UnregisterShutdownCallback(RenderDeviceShutdownCallbackDX11* _pCallback)
{
	using IT = std::vector<RenderDeviceShutdownCallbackDX11*>::iterator;
	for (IT it = m_ShutdownCallbacks.begin(); it != m_ShutdownCallbacks.end(); ++it)
	{
		if (*it == _pCallback)
		{
			m_ShutdownCallbacks.erase(it);
			break;
		}
	}
}

//---------------------------------------------------------------------------------------------------
// needs to be called before SelectTextures because it might unbind SRVs
void RenderDeviceDX11::SelectRenderTargets(const DepthStencilTextureDX11& _DepthStencil)
{
	HASSERTD(m_pSelectedShader != nullptr, "No shader selected!");

	ShaderInterfaceDX11* pInterface = m_pSelectedShader->GetInterface();

	D3D11_VIEWPORT DXViewport = { 0.f,0.f,0.f,0.f,0.f,0.f };

	// reset staging rendertargets:
	// since rendertargets are assigned to slots directly in the shader (though sv_target#) there might be gaps between used slots
	// we want to avoid previously used targets to be bound again so we introduce a staging array which is filled with nullptrs
	// and later only the used slots are replaces leaving the gaps with nullptr => unbinding previously used targets
	ID3D11RenderTargetView* pStagingRenderTargets[D3D11_SIMULTANEOUS_RENDER_TARGET_COUNT];
	for (uint32_t i = 0u; i < D3D11_SIMULTANEOUS_RENDER_TARGET_COUNT; ++i)
	{
		pStagingRenderTargets[i] = nullptr;
		m_CurrentViewports[i] = DXViewport;
	}

	ID3D11UnorderedAccessView* pStagingRWTextures[D3D11_PS_CS_UAV_REGISTER_COUNT];
	for (uint32_t i = 0u; i < D3D11_PS_CS_UAV_REGISTER_COUNT; ++i)
	{
		pStagingRWTextures[i] = nullptr;
	}

	//---------------------------------------------------------------------------------------------------
	// collect active rendertargets & rw texures
	//---------------------------------------------------------------------------------------------------
	bool bViewportsUpdated = false;
	uint32_t uRTs = 0u;
	uint32_t uRWs = 0u;
	uint32_t uMaxRTSlot = 0u;
	uint32_t uStartRWSlot = D3D11_PS_CS_UAV_REGISTER_COUNT;

	for (const Selected<ShaderRenderTargetDX11*>& RenderTarget : pInterface->GetSelectedRenderTargets())
	{
		const uint32_t uSlot = RenderTarget.uSlot;
		const RenderTargetTextureDX11& Tex = RenderTarget.pRes->GetTexture();

		HASSERT(Tex.operator bool(), "Shader %s requires rendertarget [%s], but non is provided", CSTR(m_pSelectedShader->GetName()), CSTR(RenderTarget.pRes->GetName()));
		HASSERT(uSlot < D3D11_SIMULTANEOUS_RENDER_TARGET_COUNT, "Too many render targets!");

		if (Tex.IsRWTexture())
		{
			// RW texture
			uRWs++;
			uStartRWSlot = std::min(uStartRWSlot, uSlot);
			pStagingRWTextures[uSlot - uRTs] = Tex.GetUnorderedAccessView();
		}
		else
		{
			uRTs++;
			uMaxRTSlot = std::max(uMaxRTSlot, uSlot);

			pStagingRenderTargets[uSlot] = Tex.GetRenderTargetView();

			const Viewport& HelixViewport = Tex.GetViewport();
			DXViewport.TopLeftX = HelixViewport.fTopLeftX;
			DXViewport.TopLeftY = HelixViewport.fTopLeftY;
			DXViewport.Height = HelixViewport.fHeight;
			DXViewport.Width = HelixViewport.fWidth;
			DXViewport.MaxDepth = HelixViewport.fMaxDepth;
			DXViewport.MinDepth = HelixViewport.fMinDepth;

			m_CurrentViewports[uSlot] = DXViewport;

			bViewportsUpdated = true;
		}
	}

	// assign depthstencil viewport if no valid viewport has been set to the first slot
	if (_DepthStencil != nullptr &&
		(m_CurrentViewports[0].Height == 0.f ||
		m_CurrentViewports[0].Width == 0.f))
	{
		const Viewport& HelixViewport = _DepthStencil.GetViewport();
		DXViewport.TopLeftX = HelixViewport.fTopLeftX;
		DXViewport.TopLeftY = HelixViewport.fTopLeftY;
		DXViewport.Height = HelixViewport.fHeight;
		DXViewport.Width = HelixViewport.fWidth;
		DXViewport.MaxDepth = HelixViewport.fMaxDepth;
		DXViewport.MinDepth = HelixViewport.fMinDepth;

		m_CurrentViewports[0] = DXViewport;

		bViewportsUpdated = true;
	}

	if (bViewportsUpdated)
	{
		m_pDeviceContext->RSSetViewports(uMaxRTSlot + 1u, m_CurrentViewports);
	}
	
	bool bUpdated = false;

	// copy current slot setup back to housekepping array
	for (uint32_t i = 0u; i < D3D11_SIMULTANEOUS_RENDER_TARGET_COUNT; ++i)
	{
		if (m_pCurrentRenderTargets[i] != pStagingRenderTargets[i])
		{
			bUpdated = true;
		}

		m_pCurrentRenderTargets[i] = pStagingRenderTargets[i];
	}

	// copy current slot setup back to housekepping array
	for (uint32_t i = 0u; i < D3D11_PS_CS_UAV_REGISTER_COUNT; ++i)
	{
		if (m_pCurrentUnorderedAccessViews[i] != pStagingRWTextures[i])
		{
			bUpdated = true;
		}

		m_pCurrentUnorderedAccessViews[i] = pStagingRWTextures[i];
	}

	//---------------------------------------------------------------------------------------------------
	// get depth stencil view
	//---------------------------------------------------------------------------------------------------

	ID3D11DepthStencilView* pDSV = nullptr;

	if (_DepthStencil)
	{
		// Check if Texture is a DepthStencil
		if (m_CurrentDepthStencilDesc.m_bDepthEnable || m_CurrentDepthStencilDesc.m_bStencilEnable)
		{
			// read-only
			if (m_CurrentDepthStencilDesc.m_kDepthWriteMask == DepthWriteMask_Zero ||
				m_CurrentDepthStencilDesc.m_uStencilWriteMask == 0)
			{
				pDSV = _DepthStencil.GetDepthStencilView(true);
			}
			else // write
			{
				pDSV = _DepthStencil.GetDepthStencilView(false);
			}
		}
	}
	else
	{
		pDSV = nullptr;
	}

	bUpdated |= pDSV != m_pCurrentDepthStencilView;

	m_pCurrentDepthStencilView = pDSV;

	// set rendertargets for this shader
	if (bUpdated)
	{		
		ClearShaderResources();

		if (uRWs == 0u)
		{
			m_pDeviceContext->OMSetRenderTargets(uMaxRTSlot + 1u, &m_pCurrentRenderTargets[0], m_pCurrentDepthStencilView);	
		}
		else
		{
			m_pDeviceContext->OMSetRenderTargetsAndUnorderedAccessViews(
				uMaxRTSlot+1u, &m_pCurrentRenderTargets[0], m_pCurrentDepthStencilView,
				uStartRWSlot, uRWs, m_pCurrentUnorderedAccessViews, nullptr);
		}
	}
	//https://msdn.microsoft.com/de-de/library/windows/desktop/ff476465(v=vs.85).aspx

	//For pixel shaders, the render targets and unordered - access views share the same resource slots when being written out.This means that UAVs must be given an offset so that they are placed in the slots after the render target views that are being bound.

	//	RTVs, DSV, and UAVs cannot be set independently; they all need to be set at the same time.

	//	Two RTVs conflict if they share a subresource(and therefore share the same resource).

	//	Two UAVs conflict if they share a subresource(and therefore share the same resource).

	//	An RTV conflicts with a UAV if they share a subresource or share a bind point.

	//	OMSetRenderTargetsAndUnorderedAccessViews operates properly in the following situations :

	//NumViews != D3D11_KEEP_RENDER_TARGETS_AND_DEPTH_STENCIL and NumUAVs != D3D11_KEEP_UNORDERED_ACCESS_VIEWS

	//	The following conditions must be true for OMSetRenderTargetsAndUnorderedAccessViews to succeed and for the runtime to pass the bind information to the driver :
	//NumViews <= 8
	//	UAVStartSlot >= NumViews
	//	UAVStartSlot + NumUAVs <= 8
	//	There must be no conflicts in the set of all ppRenderTargetViews and ppUnorderedAccessView.
	//	ppDepthStencilView must match the render - target views.For more information about resource views, see Introduction to a Resource in Direct3D 11.

	//	OMSetRenderTargetsAndUnorderedAccessViews performs the following tasks :
	//Unbinds all currently bound conflicting resources(stream - output target resources(SOTargets), compute shader(CS) UAVs, shader - resource views(SRVs)).
	//	Binds ppRenderTargetViews, ppDepthStencilView, and ppUnorderedAccessView.
}

//---------------------------------------------------------------------------------------------------

void RenderDeviceDX11::UpdateViewports(const DepthStencilTextureDX11& _DepthStencil)
{
	HASSERTD(m_pSelectedShader != nullptr, "No shader selected!");

	ShaderInterfaceDX11* pInterface = m_pSelectedShader->GetInterface();

	D3D11_VIEWPORT DXViewport = { 0.f,0.f,0.f,0.f,0.f,0.f };

	for (uint32_t i = 0u; i < D3D11_SIMULTANEOUS_RENDER_TARGET_COUNT; ++i)
	{
		m_CurrentViewports[i] = DXViewport;
	}

	uint32_t uRTs = 0u;
	uint32_t uMaxRTSlot = 0u;

	for (const Selected<ShaderRenderTargetDX11*>& RenderTarget : pInterface->GetSelectedRenderTargets())
	{
		const uint32_t uSlot = RenderTarget.uSlot;
		const RenderTargetTextureDX11& Tex = RenderTarget.pRes->GetTexture();

		HASSERT(Tex.operator bool(), "Shader %s requires rendertarget [%s], but non is provided", CSTR(m_pSelectedShader->GetName()), CSTR(RenderTarget.pRes->GetName()));
		HASSERT(uSlot < D3D11_SIMULTANEOUS_RENDER_TARGET_COUNT, "Too many render targets!");

		if (Tex.IsRWTexture() == false)
		{
			uRTs++;
			uMaxRTSlot = std::max(uMaxRTSlot, uSlot);

			const Viewport& HelixViewport = Tex.GetViewport();
			DXViewport.TopLeftX = HelixViewport.fTopLeftX;
			DXViewport.TopLeftY = HelixViewport.fTopLeftY;
			DXViewport.Height = HelixViewport.fHeight;
			DXViewport.Width = HelixViewport.fWidth;
			DXViewport.MaxDepth = HelixViewport.fMaxDepth;
			DXViewport.MinDepth = HelixViewport.fMinDepth;

			m_CurrentViewports[uSlot] = DXViewport;
		}
	}

	if (_DepthStencil != nullptr &&
		(m_CurrentViewports[0].Height == 0.f ||
			m_CurrentViewports[0].Width == 0.f))
	{
		const Viewport& HelixViewport = _DepthStencil.GetViewport();
		DXViewport.TopLeftX = HelixViewport.fTopLeftX;
		DXViewport.TopLeftY = HelixViewport.fTopLeftY;
		DXViewport.Height = HelixViewport.fHeight;
		DXViewport.Width = HelixViewport.fWidth;
		DXViewport.MaxDepth = HelixViewport.fMaxDepth;
		DXViewport.MinDepth = HelixViewport.fMinDepth;

		m_CurrentViewports[0] = DXViewport;
	}

	m_pDeviceContext->RSSetViewports(uMaxRTSlot + 1u, m_CurrentViewports);
}

//---------------------------------------------------------------------------------------------------
void RenderDeviceDX11::SetDepthStencilState(const DepthStencilStateDX11& _DepthStencilState)
{
	if (_DepthStencilState)
	{
		if (m_pCurrentDepthStencilState != _DepthStencilState.GetReference() ||
			m_uCurrentDepthStencilRef != _DepthStencilState.GetStencilRef())
		{
			m_pCurrentDepthStencilState = _DepthStencilState.GetReference();
			m_uCurrentDepthStencilRef = _DepthStencilState.GetStencilRef();
			m_CurrentDepthStencilDesc = _DepthStencilState.GetDescription();

			// apply state directly
			m_pDeviceContext->OMSetDepthStencilState(m_pCurrentDepthStencilState, m_uCurrentDepthStencilRef);
		}
	}
}
//---------------------------------------------------------------------------------------------------

void RenderDeviceDX11::SetBlendState(const BlendStateDX11& _BlendState)
{
	if (_BlendState)
	{
		ID3D11BlendState* pBlendState = _BlendState.GetReference();
		const BlendDesc& Desc = _BlendState.GetDescription();

		if (m_pCurrentBlendState != pBlendState ||
			m_CurrentBlendFactor != Desc.fBlendFactor ||
			m_uCurrentSampleMask != Desc.m_uSampleMask)
		{
			m_CurrentBlendFactor = Desc.fBlendFactor;
			m_uCurrentSampleMask = Desc.m_uSampleMask;
			m_pCurrentBlendState = pBlendState;

			m_pDeviceContext->OMSetBlendState(pBlendState, m_CurrentBlendFactor.data(), m_uCurrentSampleMask);
		}
	}
}
//---------------------------------------------------------------------------------------------------
void RenderDeviceDX11::ClearShaders()
{
	m_pSelectedShader = nullptr;

	for (uint32_t i = 0; i < ShaderType_NumOf; ++i)
	{
		m_ActiveShaders[i] = nullptr;
	}

	m_pDeviceContext->VSSetShader(nullptr, nullptr, 0);
	m_pDeviceContext->HSSetShader(nullptr, nullptr, 0);
	m_pDeviceContext->DSSetShader(nullptr, nullptr, 0);
	m_pDeviceContext->GSSetShader(nullptr, nullptr, 0);
	m_pDeviceContext->PSSetShader(nullptr, nullptr, 0);
	m_pDeviceContext->CSSetShader(nullptr, nullptr, 0);
}

//---------------------------------------------------------------------------------------------------
void RenderDeviceDX11::ClearRenderTargets()
{
	for (uint32_t i = 0; i < D3D11_SIMULTANEOUS_RENDER_TARGET_COUNT; ++i)
	{
		m_pCurrentRenderTargets[i] = nullptr;
	}

	m_pDeviceContext->OMSetRenderTargets(D3D11_SIMULTANEOUS_RENDER_TARGET_COUNT, m_pCurrentRenderTargets, m_pCurrentDepthStencilView);
}
//---------------------------------------------------------------------------------------------------
void RenderDeviceDX11::ClearRenderTarget(const uint32_t _uSlot)
{
	HASSERTD(_uSlot < D3D11_SIMULTANEOUS_RENDER_TARGET_COUNT, "Invalid RenderTarget slot!");

	m_pCurrentRenderTargets[_uSlot] = nullptr;
	m_pDeviceContext->OMSetRenderTargets(D3D11_SIMULTANEOUS_RENDER_TARGET_COUNT, m_pCurrentRenderTargets, m_pCurrentDepthStencilView);
}
//---------------------------------------------------------------------------------------------------
void RenderDeviceDX11::ClearDepthStencil()
{
	m_pCurrentDepthStencilView = nullptr;
	m_pDeviceContext->OMSetRenderTargets(D3D11_SIMULTANEOUS_RENDER_TARGET_COUNT, m_pCurrentRenderTargets, m_pCurrentDepthStencilView);
}
//---------------------------------------------------------------------------------------------------
void RenderDeviceDX11::ClearShaderResources()
{
	for (uint32_t s = 0; s < ShaderType_NumOf; ++s)
	{
		UpdateIndex& Index = m_PerPassUpdatedSRVs[s];
		if (Index)
		{
			uint32_t uEndIndex = Index.GetEndIndex();
			uint32_t uStartIndex = Index.GetStartIndex();
			uint32_t uCount  = Index.GetCount();

			for (uint32_t i = uStartIndex; i <= uEndIndex; ++i)
			{
				m_pCurrentShaderResources[s][i] = nullptr;
			}

			switch (s)
			{
			case ShaderType_VertexShader:
				m_pDeviceContext->VSSetShaderResources(uStartIndex, uCount, &m_pCurrentShaderResources[s][uStartIndex]);
				break;
			case ShaderType_HullShader:
				m_pDeviceContext->HSSetShaderResources(uStartIndex, uCount, &m_pCurrentShaderResources[s][uStartIndex]);
				break;
			case ShaderType_DomainShader:
				m_pDeviceContext->DSSetShaderResources(uStartIndex, uCount, &m_pCurrentShaderResources[s][uStartIndex]);
				break;
			case ShaderType_GeometryShader:
				m_pDeviceContext->GSSetShaderResources(uStartIndex, uCount, &m_pCurrentShaderResources[s][uStartIndex]);
				break;
			case ShaderType_PixelShader:
				m_pDeviceContext->PSSetShaderResources(uStartIndex, uCount, &m_pCurrentShaderResources[s][uStartIndex]);
				break;
			case ShaderType_ComputeShader:
				m_pDeviceContext->CSSetShaderResources(uStartIndex, uCount, &m_pCurrentShaderResources[s][uStartIndex]);
				break;
			default:
				break;
			}

			Index.Reset();
		}
	}
}

//---------------------------------------------------------------------------------------------------