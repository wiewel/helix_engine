//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef COMMONCONSTANTBUFFERFUNCTIONS_H
#define COMMONCONSTANTBUFFERFUNCTIONS_H

#include "Display\Shaders\ConstantBuffers.hlsli"
#include "Display\DX11\ShaderVariableDX11.h"
#include "Scene\CameraComponent.h"

namespace Helix
{
	namespace Display
	{
		using TCBPerCamera = ShaderVariableDX11<Shaders::ConstantBuffers::cbPerCamera>;
		using TCBPerFrame = ShaderVariableDX11<Shaders::ConstantBuffers::cbPerFrame>;
		using TCBPerObject = ShaderVariableDX11<Shaders::ConstantBuffers::cbPerObject>;
		using TCBDirLights = ShaderVariableDX11<Shaders::ConstantBuffers::cbDirectionalLight>;
		using TCBPointLights = ShaderVariableDX11<Shaders::ConstantBuffers::cbPointLight>;
		using TCBSpotLights = ShaderVariableDX11<Shaders::ConstantBuffers::cbSpotLight>;
		using TCBPointLightShadows = ShaderVariableDX11<Shaders::ConstantBuffers::cbPointLightShadow>;
		using TCBSpotLightShadows = ShaderVariableDX11<Shaders::ConstantBuffers::cbSpotLightShadow>;

		static inline void SetCameraConstants(TCBPerCamera& _CBPerCamera, const Scene::CameraProperties& _Camera, bool _bInfiniteProjection = false)
		{
			_CBPerCamera->mView = _Camera.m_mView;
			_CBPerCamera->mViewInv = _Camera.m_mView.getInverse();

			_CBPerCamera->mProj = _Camera.m_mProj;
			if (_bInfiniteProjection)
			{
				Scene::CameraComponent::ConvertToInfiniteProj(_CBPerCamera->mProj, _Camera.m_fNearDistance);
				_CBPerCamera->mViewProj = _CBPerCamera->mProj * _CBPerCamera->mView;
			}
			else
			{
				_CBPerCamera->mViewProj = _Camera.m_mViewProj;
			}
			_CBPerCamera->mProjInv = _CBPerCamera->mProj.getInverse();
			_CBPerCamera->mViewProjInv = _CBPerCamera->mViewProj.getInverse();

			_CBPerCamera->vCameraPos = Math::float4(_Camera.m_vPosition, 1.0f);
			_CBPerCamera->qCameraOrientation.x = _Camera.m_qOrientation.x;
			_CBPerCamera->qCameraOrientation.y = _Camera.m_qOrientation.y;
			_CBPerCamera->qCameraOrientation.z = _Camera.m_qOrientation.z;
			_CBPerCamera->qCameraOrientation.w = _Camera.m_qOrientation.w;


			//_CBPerCamera->vViewDir = Math::float4(_Camera.GetLook(), 0.0f);

			_CBPerCamera->fNearDist = _Camera.m_fNearDistance;
			_CBPerCamera->fFarDist = _Camera.m_fFarDistance;
			_CBPerCamera->fFarNearDist = _Camera.m_fFarDistance - _Camera.m_fNearDistance;
			_CBPerCamera->fTHFOV = tanf(_Camera.m_fFovY / 2.0f);

			_CBPerCamera->fAspectRatio = _Camera.m_fAspectRatio;
			_CBPerCamera->vLookDir = _Camera.m_vLook;
		}

		// to not set Material constants, simply pass nullptr instead of a material
		static inline void SetObjectConstants(TCBPerObject& _CBPerObject, const RenderObjectDX11& _Object)
		{
			_Object.GetObjectToWorldMatrix(_CBPerObject->mWorld, _CBPerObject->mNormal);
		}

		static inline void SetObjectConstants(TCBPerObject& _CBPerObject, const RenderObjectDX11& _Object, const MaterialDX11& _Material)
		{
			_Object.GetObjectToWorldMatrix(_CBPerObject->mWorld, _CBPerObject->mNormal);

			if (_Material)
			{
				const MaterialProperties& MaterialProps = _Material.GetProperties();

				_CBPerObject->MaterialProps.vAlbedo = MaterialProps.vAlbedo;

				_CBPerObject->MaterialProps.fRoughness = MaterialProps.fRoughness;
				_CBPerObject->MaterialProps.fRoughnessOffset = MaterialProps.fRoughnessOffset;
				_CBPerObject->MaterialProps.fRoughnessScale = MaterialProps.fRoughnessScale;

				_CBPerObject->MaterialProps.fMetallic = MaterialProps.fMetallic;

				_CBPerObject->MaterialProps.fHorizonFade = MaterialProps.fHorizonFade;

				_CBPerObject->MaterialProps.fHeightScale = MaterialProps.fHeightScale;

				_CBPerObject->MaterialProps.vTextureTiling = MaterialProps.vTextureTiling;

				_CBPerObject->MaterialProps.vEmissiveColor = MaterialProps.vEmissiveColor;
				_CBPerObject->MaterialProps.vEmissiveOffset = MaterialProps.vEmissiveOffset;
				_CBPerObject->MaterialProps.vEmissiveScale = MaterialProps.vEmissiveScale;
			}
		}
	} // Display
} // Helix
#endif // !COMMONCONSTANTBUFFERFUNCTIONS_H