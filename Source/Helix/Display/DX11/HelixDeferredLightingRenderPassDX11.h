//   Copyright 2020 Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef HELIXDEFERREDLIGHTINGRENDERPASSDX11_H
#define HELIXDEFERREDLIGHTINGRENDERPASSDX11_H

#include "RenderPassDX11.h"
#include "CommonConstantBufferFunctions.h"
#include "Display\Shaders\DeferredLighting.hlsl"

namespace Helix
{
	namespace Display
	{
		class HelixDeferredLightingRenderPassDX11 : public RenderPassDX11
		{
			using TCBDeferredLighting = ShaderVariableDX11<Shaders::DeferredLighting::cbDeferredLighting>;

		public:
			HDEBUGNAME("HelixDeferredLightingRenderPassDX11");

			HelixDeferredLightingRenderPassDX11(DefaultInitializerType);
			HelixDeferredLightingRenderPassDX11(const std::string& _sInstanceName = {});
			virtual ~HelixDeferredLightingRenderPassDX11();

			bool OnInitalize(const hlx::TextToken& _CustomVariables) final;
			bool OnSelectDefault() final;

			bool OnPerFrame() final;
			bool OnPerCamera(const Scene::CameraComponent& _Camera) final;

			std::string GetPermutationName(const ShaderPermutationHash& _Perm) const final;
			std::shared_ptr<RenderPassDX11> CreatePass(const std::string& _sInstanceName) const final;

			void SetShadowOffsets(float _fSpotShadowOffset, float _fPointShadowOffset);

		private:

			ShaderTextureDX11 m_DepthTex = { "gDepthTex", "LINEAR_DEPTH" };
			ShaderTextureDX11 m_NormalWSMap = { "gNormalWSMap", "WORLDSPACE_NORMAL" };
			ShaderTextureDX11 m_MetallicMap = { "gMetallicMap", "MTL_METALLIC" };
			ShaderTextureDX11 m_RoughnessMap = { "gRoughnessMap", "MTL_ROUGHNESS" };
			ShaderTextureDX11 m_AlbedoMap = { "gAlbedoMap", "MTL_ALBEDO" };

			// Additional Input
			ShaderTextureDX11 m_ShadowMap = { "gShadowMap"/*, "SHADOW_MAP"*/ };
			ShaderTextureDX11 m_PointShadowMap = { "gParaboloidPointShadowMap" };
			ShaderTextureDX11 m_SpotShadowMap = { "gProjectedSpotShadowMap" };

			ShaderTextureDX11 m_EnvMap = { "gEnvMap" };
			//ShaderTextureDX11 m_BentNormalMap = { "gBentNormalMap", "SSAO_BENT_NORMAL_MAP" };

			ShaderRenderTargetDX11 m_DiffuseTarget = { "vDiffuse", "DIFFUSE_LIGHTMAP" };
			ShaderRenderTargetDX11 m_SpecularTarget = { "vSpecular" , "SPECULAR_LIGHTMAP" };
			//ShaderRenderTargetDX11 m_NormalDebug = { "vDEBUGNORMAL" , "NORMAL_DEBUG" };

			TCBDirLights m_CBDirectionalLights = { "cbDirectionalLight" };
			TCBPointLights m_CBPointLights = { "cbPointLight" };
			TCBSpotLights m_CBSpotLights = { "cbSpotLight" };
			//TCBPerFrame m_CBPerFrame = { "cbPerFrame" };
			TCBPerCamera m_CBPerCamera = { "cbPerCamera" };

			TCBPointLightShadows m_CBPointLightShadows = { "cbPointLightShadow" };
			TCBSpotLightShadows m_CBSpotLightShadows = { "cbSpotLightShadow" };

			TCBDeferredLighting m_CBDeferredLighting = { "cbDeferredLighting" };

			float m_fSpotShadowOffset = 0.005f;
			float m_fPointShadowOffset = 0.005f;
		};
		//---------------------------------------------------------------------------------------------------
		inline void HelixDeferredLightingRenderPassDX11::SetShadowOffsets(float _fSpotShadowOffset, float _fPointShadowOffset)
		{
			m_fSpotShadowOffset = _fSpotShadowOffset;
			m_fPointShadowOffset = _fPointShadowOffset;
		}

		//---------------------------------------------------------------------------------------------------
		inline bool HelixDeferredLightingRenderPassDX11::OnSelectDefault()
		{
			bool bResult = true;

			uint32_t uInOutPermutation = m_bDebug ? Shaders::DeferredLighting::HPMIO_Debug : 0;
			GetShader()->SetIOPermutation(uInOutPermutation);

			bResult &= Select(ShaderType_PixelShader, 0);
			bResult &= Select(ShaderType_VertexShader, 0);

			return bResult;
		}
		//---------------------------------------------------------------------------------------------------
		inline bool HelixDeferredLightingRenderPassDX11::OnPerFrame()
		{
			m_CBDeferredLighting->fPointShadowDepthOffset = m_fPointShadowOffset;
			m_CBDeferredLighting->fSpotShadowDepthOffset = m_fSpotShadowOffset;

			uint32_t uInOutPermutation = m_bDebug ? Shaders::DeferredLighting::HPMIO_Debug : 0;
			GetShader()->SetIOPermutation(uInOutPermutation);
			return true;
		}
		//---------------------------------------------------------------------------------------------------

	} // Display
} // Helix

#endif // HELIXDEFERREDLIGHTINGRENDERPASSDX11_H