//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "TextureCacheDX11.h"

using namespace Helix::Display;

//---------------------------------------------------------------------------------------------------

TextureCacheDX11::TextureCacheDX11()
{
}

//---------------------------------------------------------------------------------------------------

TextureCacheDX11::~TextureCacheDX11()
{
}
//---------------------------------------------------------------------------------------------------

void TextureCacheDX11::Return(TextureDX11&& _Texture)
{
	// check for valid texture
	if (_Texture.operator bool() == false)
	{
		return;
	}

	const TextureDesc& Desc = _Texture.GetDescription();

	uint64_t uHash = HashFunction(Desc);

	auto Range = m_Cache.equal_range(uHash);

	// sanity check, put into _DEBUG context?
	for (TTextureCache::iterator it = Range.first; it != Range.second; ++it)
	{
		if (it->second.GetKey() == _Texture.GetKey())
		{
			HFATAL("Texture already cached!");
			return;
		}
	}

	m_Cache.insert(std::move(std::make_pair(uHash, std::move(_Texture))));
}

//---------------------------------------------------------------------------------------------------
uint64_t TextureCacheDX11::HashFunction(const TextureDesc& _Desc)
{
	return DefaultHashFunction(reinterpret_cast<const uint8_t*>(&_Desc), offsetof(TextureDesc, m_sFilePathOrName));
}
//---------------------------------------------------------------------------------------------------
