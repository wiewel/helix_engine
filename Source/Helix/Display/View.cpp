//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "View.h"
#include "hlx\src\Logger.h"
#include "DX11\RenderViewDX11.h"

using namespace Helix::Display;

View::View(RenderViewDX11* _RenderView) :
	m_pRenderView(_RenderView) // renderView = renderView before construtor gets called
{
}

View::~View()
{
}

HWND View::Initialize(HINSTANCE _hInstance, int _iCmdShow, const char* _sGameTitle)
{
	if (DirectX::SSE4::XMVerifySSE4Support() == false)
	{
		HFATAL("Your CPU does not support SSE4");
		return false;
	}
	
	m_hInstanceParent = _hInstance;
	m_sGameTitle = _sGameTitle;

	WNDCLASSEX wc;
	ZeroMemory(&wc, sizeof(WNDCLASSEX));

	wc.cbSize = sizeof(WNDCLASSEX);
	wc.style = CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc = WindowProc;
	wc.hInstance = _hInstance;
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)/*COLOR_WINDOW*/NULL;
	wc.lpszClassName = S("HelixWindowClass");
	//wc.hIcon = m_pRenderView->GetIcon(_hInstance);
	//wc.hIconSm = m_pRenderView->GetIcon(_hInstance);

	RegisterClassEx(&wc);

	// TODO: read window dimensions from config
	// TODO: read window/fullscreen mode info from config
	// TODO: read fake fullscreen info from config

	if(m_pRenderView != nullptr)
	{
		uint32_t uXRes;
		uint32_t uYRes;
		m_pRenderView->GetRendererResolution(uXRes, uYRes);

		RECT wr = { 0, 0, static_cast<LONG>(uXRes), static_cast<LONG>(uYRes) };

		AdjustWindowRect(&wr, WS_OVERLAPPEDWINDOW, false);

		// TODO: read window title (game title) from config OR hardcode it for each game

		DWORD dwStyle = (WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU /*| WS_THICKFRAME*/ | WS_MINIMIZEBOX /*| WS_MAXIMIZEBOX*/); // same as WS_OVERLAPPEDWINDOW without WS_THICKFRAME

		m_hWnd = CreateWindowExA(NULL,
			"HelixWindowClass",
			m_sGameTitle,
			dwStyle,
			(GetSystemMetrics(SM_CXSCREEN) - (wr.right - wr.left)) / 2,
			(GetSystemMetrics(SM_CYSCREEN) - (wr.bottom - wr.top)) / 2,
			wr.right - wr.left,
			wr.bottom - wr.top,
			NULL,
			NULL,
			_hInstance,
			this);

		if (m_hWnd == NULL)
		{
			HR2(GetLastError());
			return false;
		}

		//HICON hIcon = m_pRenderView->GetIcon();
		//if(hIcon != nullptr)
		//{
		//	//Change both icons to the same icon handle.
		//	SendMessage(m_hWnd, WM_SETICON, ICON_SMALL, (LPARAM)hIcon);
		//	SendMessage(m_hWnd, WM_SETICON, ICON_BIG, (LPARAM)hIcon);

		//	//This will ensure that the application icon gets changed too.
		//	SendMessage(GetWindow(m_hWnd, GW_OWNER), WM_SETICON, ICON_SMALL, (LPARAM)hIcon);
		//	SendMessage(GetWindow(m_hWnd, GW_OWNER), WM_SETICON, ICON_BIG, (LPARAM)hIcon);
		//}


		//WNDPROC WndProc = (WNDPROC)SetWindowLongPtr(m_hWnd, GWLP_WNDPROC, (LONG_PTR)WindowProc);
		
		hlx::Logger::Instance()->SetHWND(m_hWnd);

		ShowCursor(true);
		// TODO: add FakeFullscreen feature
		//bool bFakeFullscreen = false;
		//if (bFakeFullscreen)
		//{
		//	SetWindowLongPtr(m_hWnd, GWL_STYLE, 0);
		//}

		ShowWindow(m_hWnd, _iCmdShow);

		return m_hWnd;
	}
	else 
	{
		HFATALD("Invalid argument [nullptr]!");
		return 0;
	}
}

int View::Run()
{
	MSG msg;
	uint32_t frameCounter = 0;
	bool bNoFurtherProcessing = false;
	while (true)
	{
		++frameCounter;

		while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			switch (msg.message)
			{
				case WM_QUIT:
				{
					return static_cast<int>(msg.wParam);
				} break;
				case WM_SIZE:
				{
					OnSizeChange(msg.lParam, msg.wParam);
					
					SetWindowPos(m_hWnd,NULL,0,0,0,0,SWP_NOSIZE);
					UpdateWindow(m_hWnd);
				} break;
				case WM_KEYDOWN:
				{
					OnKeyDown(msg.lParam, msg.wParam); // VK-Codes in _wParam
				} break;
				default: 
				{
					TranslateMessage(&msg);
					DispatchMessage(&msg);
				}
			}
		}

		//m_pRenderView->RenderFrame();
	}

	return E_FAIL;
}

void View::Shutdown()
{
	UnregisterClass(S("HelixWindowClass"), m_hInstanceParent);
	m_pRenderView->Shutdown();
}

/**
*	CALLBACKS from WindowProcedure in main.cpp
*/
void View::OnSizeChange(LPARAM _lParam, WPARAM _wParam)
{
	if (m_pRenderView)
	{
		//m_pRenderView->ResizeRenderer(_lParam, _wParam);
		
		m_pRenderView->ResizeRenderer(LOWORD(_lParam), HIWORD(_lParam)); //LOWORD(_lParam), HIWORD(_lParam));
		//m_pRenderView->ChangeRendererResolution(LOWORD(_lParam), HIWORD(_lParam), false);

		//renderView->SetFullscreen(wParam==SIZE_MAXIMIZED?true:false);
	}
}
void View::OnKeyDown(LPARAM _lParam, WPARAM _wParam)
{
	switch (_wParam)
	{
		case VK_ESCAPE:
		{
			PostQuitMessage(0);
		} break;		
		case VK_UP:
		{
			if (m_pRenderView)
			{
				m_pRenderView->SetFullscreen();
			}
		} break;
		case VK_DOWN:
		{
			if (m_pRenderView)
			{
				m_pRenderView->SetFullscreen(false);
			}
		} break;
		case VK_RIGHT:
		{
			if (m_pRenderView)
			{
				m_pRenderView->ResizeRenderer(1280, 720);
				//m_pRenderView->ChangeRendererSettings(20, 0, 0);
				//m_pRenderView->RefreshRenderer();
			}
		} break;
		case VK_LEFT:
		{
			if (m_pRenderView)
			{
				m_pRenderView->ResizeRenderer(1680, 1050);
				//m_pRenderView->ChangeRendererSettings(40, 0, 0);
				//m_pRenderView->RefreshRenderer();
			}
		} break;
		default:
		{
		}
	}
}


LRESULT CALLBACK View::WindowProc(HWND _hWnd, uint32_t _uMessage, WPARAM _wParam, LPARAM _lParam)
{	
	switch (_uMessage)
	{
		case WM_SIZE:
		{
			PostMessage(_hWnd, _uMessage, _wParam, _lParam);
			return 0;
			//OnSizeChange(_lParam, _wParam); // Size in _lParam, windowed/fullscreen in _wParam
		} break;
		case WM_DESTROY:
		{
			PostQuitMessage(0);
			return 0;
		} break;
	}
	return DefWindowProc( _hWnd, _uMessage, _wParam, _lParam);
}

