//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher, David Labode
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef HMATFILE_H
#define HMATFILE_H

//#include "HMATPROPFile.h"
#include "hlx\src\ByteStream.h"

namespace Helix
{
	namespace Resources
	{
		enum ImageChannel : uint32_t
		{
			ImageChannel_r = 0,
			ImageChannel_g,
			ImageChannel_b,
			ImageChannel_m,
			ImageChannel_l,
			ImageChannel_z
		};

		struct HMat
		{
			/// Mtl Name
			std::string sName;

			/// Color Map
			std::string sMapAlbedo;

			/// Normal Map
			std::string sMapNormal;

			/// Metallic Map
			std::string sMapMetallic;

			/// Roughness Map
			std::string sMapRoughness;

			/// Emissive Map
			std::string sMapEmissive;

			/// Height Map for tesselation / parallax
			std::string sMapHeight; // Maya ShaderFX -> use AO Map as Height-Map

			inline uint64_t Key() const
			{
				std::hash<std::string> Hasher;

				uint64_t uHash = Hasher(sName);
				uHash ^= Hasher(sMapAlbedo);
				uHash ^= Hasher(sMapNormal);
				uHash ^= Hasher(sMapMetallic);
				uHash ^= Hasher(sMapRoughness);
				uHash ^= Hasher(sMapEmissive);
				uHash ^= Hasher(sMapHeight);
				
				return uHash;
			}
		};

		class HMATFile
		{
		public:
			HMATFile();
			HMATFile(const HMat& _OtherMaterial);
			~HMATFile();

			void Read(hlx::bytestream& _Stream);
			void Write(hlx::bytestream& _Stream);

			const HMat& GetMaterial() const;
			HMat& GetMaterial();

		private:
			HMat m_Material;
		};

		inline const HMat& HMATFile::GetMaterial() const { return m_Material; }
		inline HMat& HMATFile::GetMaterial() { return m_Material; }

	} // Resources
} // Helix

#endif // HMATFILE_H