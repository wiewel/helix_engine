//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef FILESYSTEM_H
#define FILESYSTEM_H

#include "hlx\src\StandardDefines.h"
#include "hlx\src\Singleton.h"
#include "hlx\src\FileStream.h"

#include <vector>
#include <string>
#include <unordered_map>

namespace Helix
{
	namespace Resources
	{
		enum HelixDirectories
		{
			HelixDirectories_BasePath = 0,
			HelixDirectories_Bin,
			HelixDirectories_EngineConfigs,
			HelixDirectories_Shaders,
			HelixDirectories_Data,
			HelixDirectories_Models,
			HelixDirectories_Materials,
			HelixDirectories_MaterialProperties,
			HelixDirectories_Textures,
			HelixDirectories_Levels,
			HelixDirectories_Sounds,
			HelixDirectories_NumOf
		};

		struct HelixDir
		{
			HelixDirectories kDirectory;
			const TCHAR* pPath;
			const TCHAR* pExtension; // only index files with this ending (supports wildcards)
			bool bIndexed;
			bool bRecursiveIndexed;
			bool bProjectDependant;
		};

		static const HelixDir HelixDirectoryNames[HelixDirectories_NumOf] =
		{
			{ HelixDirectories_BasePath, S("\\"), S(""), false, false, false},
			{ HelixDirectories_Bin, S("\\Bin\\"), S(""), false, false, false },
			{ HelixDirectories_EngineConfigs, S("\\Configs\\"), S(".cfg|.stylesheet"), true, false, true },
			{ HelixDirectories_Shaders, S("\\Bin\\Shaders\\"),S(".hlxsl"), true, false, false},
			{ HelixDirectories_Data, S("\\Data\\"), S(""), true, true, false},
			{ HelixDirectories_Models, S("\\Data\\Models\\"), S(".h3d"), true, false, false },
			{ HelixDirectories_Materials, S("\\Data\\Materials\\"), S(".hmat"), true, false , false },
			{ HelixDirectories_MaterialProperties, S("\\Data\\Materials\\"), S(".hmatprop"), true, false , false },
			{ HelixDirectories_Textures, S("\\Data\\Textures\\"), S(".dds"), true, false, false },
			{ HelixDirectories_Levels, S("\\Data\\Levels\\"), S(".hell"), true, false, false },
			{ HelixDirectories_Sounds, S("\\Data\\Sounds\\"), S(".wav"), true, false, false },
		};

		class FileSystem : public hlx::Singleton<FileSystem>
		{
		public:
			HDEBUGNAME("FileSystem");

			FileSystem();
			~FileSystem();

			//Initialized all known paths
			bool Initialize(
				const hlx::string& _sBasePath = {},
				const hlx::string& _sProjectName = {},
				bool _bCreateDirs = true);

			bool IsInitialized() const;

			bool AddMiscFilesFromDirectory(const hlx::string& _sAbsoluteDirectoryPath, const hlx::string& _sExtension = S(""), bool _bRecursive = false);

			// returns true if file was opend successfully
			bool OpenFileStream(HelixDirectories _kDirectory, const hlx::string& _sFileName, const int _iOpenMode, _Out_ hlx::fbytestream& _FileStream);
			// function for misc files
			bool OpenFileStream(const hlx::string& _sFileName, const int _iOpenMode, _Out_ hlx::fbytestream& _FileStream);

			const hlx::string& GetKnownDirectoryPath(HelixDirectories _kDirectory = HelixDirectories_BasePath) const;

			bool GetFullPath(HelixDirectories _kDirectory, const hlx::string& _sFileName, _Out_ hlx::string& _sFilePath);
			// function for misc files
			bool GetFullPath(const hlx::string& _sFileName, _Out_ hlx::string& _sFilePath);

			// STATIC FUNCTIONS:
			// gets all files with extensions seperated by '|' from a directory
			static std::vector<hlx::string> GetFilesFromDirectory(const hlx::string& _sDirectoryName, const hlx::string& _sExtension = S(""), bool _bRecursive = false);

			// check if a file or directory exists
			static bool PathExists(const hlx::string& _sPath);
			static bool PathExists(const hlx::string& _sPath, uint32_t& _uAttributes);

			static bool CreateDirectoryRecursive(const hlx::string& _sDirectory);

			static hlx::string GetFileNameWithoutExtension(const hlx::string& _sFilePath);

			static hlx::string GetFileName(const hlx::string& _sFilePath);

			static hlx::string GetDirectoryFromPath(const hlx::string& _sFilePath);

			static hlx::string GetShortName(const hlx::string& _sFileNameOrPath);

#ifdef UNICODE
			static std::string GetShortName(const std::string& _sFileNameOrPath);
			static std::string GetFileNameWithoutExtension(const std::string& _sFilePath);
#endif

			bool AddFileToIndex(const HelixDirectories _kDirectory, const hlx::string& _sFilePath, bool _bSuppressDuplicateWarning = true);

		private:
			bool AddFilesFromDirectory(const uint32_t _uKnownDirIndex, const hlx::string& _sAbsoluteDirectoryPath, const hlx::string& _sExtension = S(""), bool _bRecursive = false);

		private:

			bool m_bInitialized = false;
			hlx::string m_Directories[HelixDirectories_NumOf];
			hlx::string m_sBasePath;
			hlx::string m_sProjectName;
			
			// filename -> complete file path, no duplicate filenames => unique file naming!
			std::unordered_map<hlx::string, hlx::string> m_Files[HelixDirectories_NumOf];
			std::unordered_map<hlx::string, hlx::string> m_MiscFiles;
		};

		inline bool FileSystem::IsInitialized() const { return m_bInitialized; }

		inline const hlx::string& FileSystem::GetKnownDirectoryPath(HelixDirectories _kDirectory) const { return m_Directories[_kDirectory]; }
		
	} // Resources
} // Helix


#endif // FILESYSTEM_H