//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "RenderPassDescription.h"
#include "CommonTypeTextSerializer.h"

using namespace Helix::Resources;
using namespace Helix::Display;

//---------------------------------------------------------------------------------------------------
static TextureResizeProperties ParseResizeProps(const hlx::TextToken& _InputToken)
{
	TextureResizeProperties Props = {};
	Props.sEventName = _InputToken.getValue("ResizeEvent");
	Props.fWidthScale = _InputToken.get<float>("WidthScale", 1.f);
	Props.fHeightScale = _InputToken.get<float>("HeightScale", 1.f);

	return Props;
}
//---------------------------------------------------------------------------------------------------

static void SerializeResizeProps(const TextureResizeProperties& _Props, hlx::TextToken& _OutputToken)
{
	_OutputToken.set("ResizeEvent", _Props.sEventName);
	_OutputToken.set("WidthScale", _Props.fWidthScale);
	_OutputToken.set("HeightScale", _Props.fHeightScale);
}
//---------------------------------------------------------------------------------------------------

RenderPassDesc::RenderPassDesc(const hlx::TextToken& _InputToken)
{
	sShaderName = _InputToken.getValue("ShaderName");
	sPassInstanceName = _InputToken.getValue("InstanceName");
	sPassType = _InputToken.getValue("PassType");
	bEnabled = _InputToken.get<bool>("Enabled");

	bClearRenderTargets = _InputToken.get<bool>("ClearRenderTargets", true);
	bClearDepthStencil = _InputToken.get<bool>("ClearDepthStencil", false);

	_InputToken.getTokenConst("CustomVariables", CustomVariables);

	hlx::TextToken TempToken;

	_InputToken.getTokenConst("RasterizerDescription", TempToken);
	RasterizerDescription = ParseRasterizerDesc(TempToken);

	_InputToken.getTokenConst("BlendDescription", TempToken);
	BlendDescription = ParseBlendDesc(TempToken);

	_InputToken.getTokenConst("DepthStencilDescription", TempToken);
	DepthStencilDescription = ParseDepthStencilDesc(TempToken);

	_InputToken.getTokenConst("DepthStencilTexture", TempToken);
	DepthStencilTextureDescription = ParseDepthStencilTextureDesc(TempToken);

	auto TargetRange = _InputToken.getAllTokens("RenderTarget");

	// Parse rendertargets
	for (std::multimap<std::string, hlx::TextToken>::const_iterator it = TargetRange.first; it != TargetRange.second; ++it)
	{
		RenderTargets.push_back(ParseRenderTargetTextureDesc(it->second));
	}

	TargetRange = _InputToken.getAllTokens("InputMapping");

	//Parse InputMappings
	for (std::multimap<std::string, hlx::TextToken>::const_iterator it = TargetRange.first; it != TargetRange.second; ++it)
	{
		const hlx::TextToken& Token = it->second;
		InputMapping Mapping = {};

		Mapping.sName = Token.getValue("Name");
		Mapping.sMappedName = Token.getValue("MappedName");
		Mapping.sMappedCopyName = Token.getValue("MappedCopyName");

		InputMappings.push_back(Mapping);
	}

	TargetRange = _InputToken.getAllTokens("Sampler");

	// Parse samplers
	for (std::multimap<std::string, hlx::TextToken>::const_iterator it = TargetRange.first; it != TargetRange.second; ++it)
	{
		const hlx::TextToken& Token = it->second;
		SamplerDescEx Desc = {};

		Desc.sName = Token.getValue("Name");
		Desc.Description = ParseSamplerDesc(Token);

		Samplers.push_back(Desc);
	}
}
//---------------------------------------------------------------------------------------------------

hlx::TextToken RenderPassDesc::Serialize() const
{
	hlx::TextToken Token("RenderPass");

	Token.set("ShaderName", sShaderName);
	Token.set("InstanceName", sPassInstanceName);
	Token.set("PassType", sPassType);
	Token.set("Enabled", bEnabled);
	Token.set("ClearRenderTargets", bClearRenderTargets);
	Token.set("ClearDepthStencil", bClearDepthStencil);

	if (CustomVariables.IsEmpty() == false)
	{
		Token.setToken("CustomVariables", CustomVariables);
	}

	Token.setToken("RasterizerDescription", SerializeRasterizerDesc(RasterizerDescription));
	Token.setToken("BlendDescription", SerializeBlendDesc(BlendDescription));
	Token.setToken("DepthStencilDescription", SerializeDepthStencilDesc(DepthStencilDescription));
	Token.setToken("DepthStencilTexture", SerializeDepthStencilTextureDesc(DepthStencilTextureDescription));

	for (const RenderTargetTextureDesc& Desc : RenderTargets)
	{
		Token.addToken("RenderTarget", SerializeRenderTargetTextureDesc(Desc));
	}

	for (const InputMapping& Mapping : InputMappings)	
	{
		if (Mapping.sMappedName.empty() && Mapping.sMappedCopyName.empty())
		{
			// skipp empty mappings
			continue;
		}

		hlx::TextToken IMToken("InputMapping");

		IMToken.set("Name", Mapping.sName);
		IMToken.set("MappedName", Mapping.sMappedName);
		IMToken.set("MappedCopyName", Mapping.sMappedCopyName);
		
		Token.addToken("InputMapping", IMToken);
	}

	for (const SamplerDescEx& Desc : Samplers)
	{
		hlx::TextToken SToken = SerializeSamplerDesc(Desc.Description);
		SToken.set("Name", Desc.sName);
		
		Token.addToken("Sampler", SToken);
	}

	return Token;
}
//---------------------------------------------------------------------------------------------------

DepthStencilDesc RenderPassDesc::ParseDepthStencilDesc(const hlx::TextToken& _InputToken)
{
	DepthStencilDesc Desc = {};

	Desc.m_bDepthEnable = _InputToken.get<bool>("DepthEnable");
	Desc.m_kDepthWriteMask = _InputToken.get<DepthWriteMask>("DepthWriteMask", DepthWriteMask_Zero);
	Desc.m_kDepthFunc = _InputToken.get<ComparisonFunction>("DepthFunc", ComparisonFunction_LessEqual);
	Desc.m_bStencilEnable = _InputToken.get<bool>("StencilEnable", false);
	Desc.m_uStencilReadMask = _InputToken.get<uint8_t>("StencilReadMask", 0xff);
	Desc.m_uStencilWriteMask = _InputToken.get<uint8_t>("StencilWriteMask", 0xff);

	hlx::TextToken TempToken;
	_InputToken.getTokenConst("FrontFace", TempToken);

	Desc.m_FrontFace.m_kStencilFailOp = TempToken.get<StencilOperation>("StencilFailOp", StencilOperation_Keep);
	Desc.m_FrontFace.m_kStencilDepthFailOp = TempToken.get<StencilOperation>("StencilDepthFailOp", StencilOperation_Incr);
	Desc.m_FrontFace.m_kStencilPassOp = TempToken.get<StencilOperation>("StencilPassOp", StencilOperation_Keep);
	Desc.m_FrontFace.m_kStencilFunc = TempToken.get<ComparisonFunction>("StencilFunc", ComparisonFunction_Always);

	_InputToken.getTokenConst("BackFace", TempToken);
	Desc.m_BackFace.m_kStencilFailOp = TempToken.get<StencilOperation>("StencilFailOp", StencilOperation_Keep);
	Desc.m_BackFace.m_kStencilDepthFailOp = TempToken.get<StencilOperation>("StencilDepthFailOp", StencilOperation_Decr);
	Desc.m_BackFace.m_kStencilPassOp = TempToken.get<StencilOperation>("StencilPassOp", StencilOperation_Keep);
	Desc.m_BackFace.m_kStencilFunc = TempToken.get<ComparisonFunction>("StencilFunc", ComparisonFunction_Always);

	return Desc;
}
//---------------------------------------------------------------------------------------------------


hlx::TextToken RenderPassDesc::SerializeDepthStencilDesc(const DepthStencilDesc& _Desc)
{
	hlx::TextToken Token("DepthStencilDescription");

	Token.set("DepthEnable", _Desc.m_bDepthEnable);
	Token.set("DepthWriteMask", _Desc.m_kDepthWriteMask);
	Token.set("DepthFunc", _Desc.m_kDepthFunc);
	Token.set("StencilEnable", _Desc.m_bStencilEnable);
	Token.set("StencilReadMask", _Desc.m_uStencilReadMask);
	Token.set("StencilWriteMask", _Desc.m_uStencilWriteMask);

	auto fn = [](const DepthStencilOperationDesc& _DSO, const std::string& _sKey) -> hlx::TextToken
	{
		hlx::TextToken DSOToken(_sKey);
		DSOToken.set("StencilFailOp", _DSO.m_kStencilFailOp);
		DSOToken.set("StencilDepthFailOp", _DSO.m_kStencilDepthFailOp);
		DSOToken.set("StencilPassOp", _DSO.m_kStencilPassOp);
		DSOToken.set("StencilFunc", _DSO.m_kStencilFunc);
		return DSOToken;
	};

	Token.setToken("FrontFace", fn(_Desc.m_FrontFace, "FrontFace"));
	Token.setToken("BackFace", fn(_Desc.m_FrontFace, "BackFace"));

	return Token;
}

//---------------------------------------------------------------------------------------------------

BlendDescReduced RenderPassDesc::ParseBlendDesc(const hlx::TextToken& _InputToken)
{
	BlendDescReduced Desc = {};

	hlx::VecFloat4 BlendFactor = _InputToken.getVector<float, 4>("BlendFactor");
	std::copy(std::cbegin(BlendFactor.Data), std::cend(BlendFactor.Data), std::begin(Desc.fBlendFactor));

	Desc.uSampleMask = _InputToken.get<uint32_t>("SampleMask", 0xffffffff);

	Desc.bAlphaToCoverageEnable = _InputToken.get<bool>("AlphaToCoverageEnable");
	Desc.bIndependentBlendEnable = _InputToken.get<bool>("IndependentBlendEnable");

	return Desc;
}
//---------------------------------------------------------------------------------------------------

hlx::TextToken RenderPassDesc::SerializeBlendDesc(const BlendDescReduced& _Desc)
{
	hlx::TextToken Token("BlendDescription");

	Token.setVector("BlendFactor", hlx::VecFloat4(_Desc.fBlendFactor));
	Token.set("SampleMask", _Desc.uSampleMask);
	Token.set("AlphaToCoverageEnable", _Desc.bAlphaToCoverageEnable);
	Token.set("IndependentBlendEnable", _Desc.bIndependentBlendEnable);

	return Token;
}
//---------------------------------------------------------------------------------------------------

RasterizerDesc RenderPassDesc::ParseRasterizerDesc(const hlx::TextToken& _InputToken)
{
	RasterizerDesc Desc = {};

	Desc.m_kFillMode = _InputToken.get<FillMode>("FillMode", FillMode_Solid);
	Desc.m_kCullMode = _InputToken.get<CullMode>("CullMode", CullMode_Back);

	Desc.m_bFrontCounterClockwise = _InputToken.get<bool>("FrontCounterClockwise", false);
	Desc.m_iDepthBias = _InputToken.get<int32_t>("DepthBias", 0);
	Desc.m_fDepthBiasClamp = _InputToken.get<float>("DepthBiasClamp", 0.f);
	Desc.m_fSlopeScaledDepthBias = _InputToken.get<float>("SlopeScaledDepthBias", 0.f);
	Desc.m_bDepthClipEnable = _InputToken.get<bool>("DepthClipEnable", true);
	Desc.m_bScissorEnable = _InputToken.get<bool>("ScissorEnable", false);
	Desc.m_bMultisampleEnable = _InputToken.get<bool>("MultisampleEnable", false);
	Desc.m_bAntialiasedLineEnable = _InputToken.get<bool>("AntialiasedLineEnable", false);

	return Desc;
}
//---------------------------------------------------------------------------------------------------

hlx::TextToken RenderPassDesc::SerializeRasterizerDesc(const RasterizerDesc& _Desc)
{
	hlx::TextToken Token("RasterizerDescription");

	Token.set("FillMode", _Desc.m_kFillMode);
	Token.set("CullMode", _Desc.m_kCullMode);
	Token.set("FrontCounterClockwise", _Desc.m_bFrontCounterClockwise);
	Token.set("DepthBias", _Desc.m_iDepthBias);
	Token.set("DepthBiasClamp", _Desc.m_fDepthBiasClamp);
	Token.set("SlopeScaledDepthBias", _Desc.m_fSlopeScaledDepthBias);
	Token.set("DepthClipEnable", _Desc.m_bDepthClipEnable);
	Token.set("ScissorEnable", _Desc.m_bScissorEnable);
	Token.set("MultisampleEnable", _Desc.m_bMultisampleEnable);
	Token.set("AntialiasedLineEnable", _Desc.m_bAntialiasedLineEnable);

	return Token;
}
//---------------------------------------------------------------------------------------------------

SamplerDesc RenderPassDesc::ParseSamplerDesc(const hlx::TextToken& _InputToken)
{
	SamplerDesc Desc = {};

	Desc.m_kFilter = _InputToken.get<TextureFilter>("Filter", TextureFilter_Min_Mag_Mip_Linear);// trilinear
	Desc.m_kAddressU = _InputToken.get<TextureAddressMode>("AddressModeU", TextureAddressMode_Clamp);
	Desc.m_kAddressV = _InputToken.get<TextureAddressMode>("AddressModeV", TextureAddressMode_Clamp);
	Desc.m_kAddressW = _InputToken.get<TextureAddressMode>("AddressModeW", TextureAddressMode_Clamp);
	Desc.m_fMipLODBias = _InputToken.get<float>("MipLODBias", 0.f);
	Desc.m_uMaxAnisotropy = _InputToken.get<uint32_t>("MaxAnisotropy", 1u);
	Desc.m_kComparisonFunction = _InputToken.get<ComparisonFunction>("ComparisonFunction", ComparisonFunction_Never);

	hlx::VecFloat4 BorderColor = _InputToken.getVector<float, 4>("BorderColor", { 1.0f,1.0f,1.0f,1.0f });
	std::copy(std::cbegin(BorderColor.Data), std::cend(BorderColor.Data), std::begin(Desc.m_vBorderColor));

	Desc.m_fMinLOD = _InputToken.get<float>("MinLOD", -FLT_MAX);
	Desc.m_fMaxLOD = _InputToken.get<float>("MaxLOD", FLT_MAX);

	return Desc;
}
//---------------------------------------------------------------------------------------------------

hlx::TextToken RenderPassDesc::SerializeSamplerDesc(const SamplerDesc& _Desc)
{
	hlx::TextToken Token("Sampler");

	Token.set("Filter", _Desc.m_kFilter);
	Token.set("AddressModeU", _Desc.m_kAddressU);
	Token.set("AddressModeV", _Desc.m_kAddressV);
	Token.set("AddressModeW", _Desc.m_kAddressW);
	Token.set("MipLODBias", _Desc.m_fMipLODBias);
	Token.set("MaxAnisotropy", _Desc.m_uMaxAnisotropy);
	Token.set("ComparisonFunction", _Desc.m_kComparisonFunction);
	Token.setVector("BorderColor", hlx::VecFloat4(_Desc.m_vBorderColor));
	Token.set("MinLOD", _Desc.m_fMinLOD);
	Token.set("MaxLOD", _Desc.m_fMaxLOD);

	return Token;
}
//---------------------------------------------------------------------------------------------------

DepthStencilTextureDesc RenderPassDesc::ParseDepthStencilTextureDesc(const hlx::TextToken& _InputToken)
{
	DepthStencilTextureDesc Desc = {};

	Desc.ResizeProps = ParseResizeProps(_InputToken);
	Desc.sMappedName = _InputToken.getValue("MappedName");
	Desc.bShaderResource = _InputToken.get<bool>("IsShaderResource", true);
	Desc.kFormat = _InputToken.get<PixelFormat>("Format", PixelFormat_R32_Float);
	Desc.uWidth = _InputToken.get<uint32_t>("Width", 16u);
	Desc.uHeight = _InputToken.get<uint32_t>("Height", 16u);
	Desc.uStencilRef = _InputToken.get<uint32_t>("StencilRef", 0u);
	Desc.fClearDepth = _InputToken.get<float>("ClearDepth", 1.f);
	Desc.uClearStencil = _InputToken.get<uint8_t>("ClearStencil", 0u);

	return Desc;
}
//---------------------------------------------------------------------------------------------------

hlx::TextToken RenderPassDesc::SerializeDepthStencilTextureDesc(const DepthStencilTextureDesc& _Desc)
{
	hlx::TextToken Token("DepthStencilTexture");

	SerializeResizeProps(_Desc.ResizeProps, Token);
	Token.set("MappedName", _Desc.sMappedName);
	Token.set("IsShaderResource", _Desc.bShaderResource);
	Token.set("Format", _Desc.kFormat);
	Token.set("Width", _Desc.uWidth);
	Token.set("Height", _Desc.uHeight);
	Token.set("StencilRef", _Desc.uStencilRef);
	Token.set("ClearDepth", _Desc.fClearDepth);
	Token.set("ClearStencil", _Desc.uClearStencil);

	return Token;
}
//---------------------------------------------------------------------------------------------------

RenderTargetTextureDesc RenderPassDesc::ParseRenderTargetTextureDesc(const hlx::TextToken& _InputToken)
{
	RenderTargetTextureDesc Desc = {};

	Desc.sName = _InputToken.getValue("Name");
	Desc.uIndex = _InputToken.get<uint32_t>("Index");

	if (Desc.uIndex > 7u)
	{
		HERROR("Invalid RenderTarget Index %u (>7)!", Desc.uIndex);
	}

	Desc.ResizeProps = ParseResizeProps(_InputToken);
	Desc.bBackBuffer = _InputToken.get<bool>("IsBackBuffer", false);
	Desc.bShaderResource = _InputToken.get<bool>("IsShaderResource", true);
	Desc.bRWTexture = _InputToken.get<bool>("IsRWTexture", false);

	// TODO: parse from string
	Desc.kFormat = _InputToken.get<PixelFormat>("Format", PixelFormat_B8G8R8A8_UNorm);
	Desc.uWidth = _InputToken.get<uint32_t>("Width", 16u);
	Desc.uHeight = _InputToken.get<uint32_t>("Height", 16u);

	Desc.vClearColor = to(_InputToken.getVector<float, 4>("ClearColor", { 0.f, 0.f,0.f,1.f }));

	hlx::TextToken BlendToken;
	if (_InputToken.getTokenConst("BlendDesc", BlendToken))
	{
		Desc.BlendDesc.m_bBlendEnable = BlendToken.get<bool>("BlendEnable", false);
		Desc.BlendDesc.m_kSrcBlend = BlendToken.get<BlendVariable>("SrcBlend", BlendVariable_Src_Alpha);
		Desc.BlendDesc.m_kDestBlend = BlendToken.get<BlendVariable>("DestBlend", BlendVariable_One);
		Desc.BlendDesc.m_kBlendOp = BlendToken.get<BlendOperation>("BlendOp", BlendOperation_Add);
		Desc.BlendDesc.m_kSrcBlendAlpha = BlendToken.get<BlendVariable>("SrcBlendAlpha", BlendVariable_Zero);
		Desc.BlendDesc.m_kDestBlendAlpha = BlendToken.get<BlendVariable>("DestBlendAlpha", BlendVariable_Zero);
		Desc.BlendDesc.m_kBlendOpAlpha = BlendToken.get<BlendOperation>("BlendOpAlpha", BlendOperation_Add);
		Desc.BlendDesc.m_uRenderTargetWriteMask = BlendToken.get<uint8_t>("RenderTargetWriteMask", Channel_All);
	}

	return Desc;
}
//---------------------------------------------------------------------------------------------------

hlx::TextToken RenderPassDesc::SerializeRenderTargetTextureDesc(const RenderTargetTextureDesc& _Desc)
{
	hlx::TextToken Token("RenderTarget");

	Token.set("Name", _Desc.sName);
	Token.set("Index", _Desc.uIndex);

	SerializeResizeProps(_Desc.ResizeProps, Token);

	Token.set("IsBackBuffer", _Desc.bBackBuffer);
	Token.set("IsShaderResource", _Desc.bShaderResource);
	Token.set("IsRWTexture", _Desc.bRWTexture);
	Token.set("Format", _Desc.kFormat);
	Token.set("Width", _Desc.uWidth);
	Token.set("Height", _Desc.uHeight);
	Token.setVector("ClearColor", to(_Desc.vClearColor));

	if (_Desc.BlendDesc.IsDefault() == false)
	{
		hlx::TextToken BlendToken("BlendDesc");
			
		BlendToken.set("BlendEnable", _Desc.BlendDesc.m_bBlendEnable);
		BlendToken.set("SrcBlend", _Desc.BlendDesc.m_kSrcBlend);
		BlendToken.set("DestBlend", _Desc.BlendDesc.m_kDestBlend);
		BlendToken.set("BlendOp", _Desc.BlendDesc.m_kBlendOp);
		BlendToken.set("SrcBlendAlpha", _Desc.BlendDesc.m_kSrcBlendAlpha);
		BlendToken.set("DestBlendAlpha", _Desc.BlendDesc.m_kDestBlendAlpha);
		BlendToken.set("BlendOpAlpha", _Desc.BlendDesc.m_kBlendOpAlpha);
		BlendToken.set("RenderTargetWriteMask", _Desc.BlendDesc.m_uRenderTargetWriteMask);

		Token.setToken("BlendDesc", BlendToken);
	}

	return Token;
}
//---------------------------------------------------------------------------------------------------
