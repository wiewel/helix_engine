//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef SHADERCOMPILATIONRESULT_H
#define SHADERCOMPILATIONRESULT_H

#include <concurrent_unordered_map.h>
#include "Display\ViewDefines.h"

// forward declarations
namespace hlx
{
	class bytes;
}

namespace Helix
{
	namespace Resources
	{
		struct HLXSLPermutation
		{
			Display::ShaderPermutationHash Hash = {};
			uint32_t uCodeHash = 0; // CRCs of data
			uint32_t uInterfaceHash = 0;
			uint32_t uInputLayoutHash = 0;
			uint32_t uOutputLayoutHash = 0;
			uint32_t uUncompressedCodeSize = 0;
		};

		struct HLXSLPermutationIndex
		{
			Display::ShaderPermutationHash Hash = {};
			uint32_t uFunctionIndex = 0;

			uint32_t uCodeHash = 0; // CRCs of data
			uint32_t uUncompressedCodeSize = 0;
			uint32_t uCodeIndex = 0;

			uint32_t uInterfaceHash = 0;
			uint32_t uInterfaceIndex = 0;

			uint32_t uInputLayoutHash = 0;
			uint32_t uInputLayoutIndex = 0;

			uint32_t uOutputLayoutHash = 0;
			uint32_t uOutputLayoutIndex = 0;
		};

		// crc32 -> data
		using TParallelUnoderedByteMap = concurrency::concurrent_unordered_map<uint32_t, hlx::bytes>;
		// function -> permutations
		using TPermutationMap = concurrency::concurrent_unordered_multimap<size_t, HLXSLPermutation>;

		using TPermutationMasks = std::array<uint32_t, Display::ShaderType_NumOf + 1>;

		enum CodeCompression : uint32_t
		{
			CodeCompression_None = 0,
			CodeCompression_LZMA,
			CodeCompression_Unknown
		};

		class ShaderCompilationResult
		{
			friend class HLXSLFile;
		public:

			void SetShaderName(const std::string& _sShaderName);
			const std::string& GetShaderName() const;

			void AddFunction(const Display::ShaderFunction& _Function);

			void AddPermutation(const Display::ShaderFunction& _Function, const HLXSLPermutation& _NewPermutation);

			void SetPermutationMask(const uint32_t& _uMask, const uint32_t& _uShaderTypeIndex);

			// returns crc32 of code
			uint32_t AddCode(hlx::bytes&& Code, _Out_ uint32_t& _uCompressedSizeOut, uint32_t uOrigCRC = 0u);

			uint32_t AddInterface(hlx::bytes&& Interface);

			uint32_t AddLayout(hlx::bytes&& Layout);

			void Reset();

			void PrintStats(const uint32_t& _uTotalPermutations);

			// removes all permutations with range indice != 0u for the _uRangeIndex-th range
			void PrunePermutationsWithRange(const size_t& _uFunction, const uint32_t& _uRangeIndex);

			// Compression settings
			void SetCompressionMethod(const CodeCompression& _kMethod);
			void SetLZMACompressionLevel(int32_t _iLevel);
			void SetLZMADictionarySize(uint32_t _uSizeInByte);

			const CodeCompression& GetCompressionMethod() const;

		private:
			int32_t m_iLZMACompressionLevel = 4;
			uint32_t m_uLZMADictionarySize = 1024u*1024u; // 1MB

			CodeCompression m_kCompressionMethod = CodeCompression_LZMA;

			std::string m_sShaderName = "DefaultShaderName";
			
			std::vector<Display::ShaderFunction> m_Functions;

			TPermutationMap m_CompiledPermutations;

			TParallelUnoderedByteMap m_CodeBuffer;
			TParallelUnoderedByteMap m_InterfaceBuffer;
			TParallelUnoderedByteMap m_InputOutputLayoutBuffer;

			TPermutationMasks m_uPermutationsMasks = { 0 };
		};

		inline void ShaderCompilationResult::SetShaderName(const std::string & _sShaderName) { m_sShaderName = _sShaderName; }
		inline const std::string & ShaderCompilationResult::GetShaderName() const { return m_sShaderName; }
		inline void ShaderCompilationResult::SetLZMACompressionLevel(int32_t _iLevel){m_iLZMACompressionLevel = _iLevel;}
		inline void ShaderCompilationResult::SetLZMADictionarySize(uint32_t _uSizeInByte){	m_uLZMADictionarySize = _uSizeInByte;}
		inline const CodeCompression& ShaderCompilationResult::GetCompressionMethod() const	{return m_kCompressionMethod;}
		inline void ShaderCompilationResult::SetCompressionMethod(const CodeCompression& _kMethod){	m_kCompressionMethod = _kMethod;}
	}
} // Helix

#endif // SHADERCOMPILATIONRESULT_H