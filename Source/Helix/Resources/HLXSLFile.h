//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef HLXSLFILE_H
#define HLXSLFILE_H

#include "Display\ViewDefines.h"
#include "ShaderCompilationResult.h"
#include <array>
#include "hlx\src\ByteStream.h"

// forward declarations

namespace Helix
{
	namespace Resources
	{
		struct HLXSLPermutationIndex;

		enum HLXSLVersion : uint32_t
		{
			HLXSLVersion_v1 = 0,
			HLXSLVersion_Unknown
		};

		struct PermutationBufferRef
		{
			PermutationBufferRef(const HLXSLPermutationIndex* _pIndex, const Display::ShaderFunction* _pFunction,
				const hlx::bytes* _pCode, const hlx::bytes* _pInterface,
				const hlx::bytes* _pInputLayout, const hlx::bytes* _pOutputLayout) :
				pIndex(_pIndex), pFunction(_pFunction), pCode(_pCode), pInterface(_pInterface), pInputLayout(_pInputLayout), pOutputLayout(_pOutputLayout) {}

			const HLXSLPermutationIndex* pIndex = nullptr;
			const hlx::bytes* pCode = nullptr;
			const hlx::bytes* pInterface = nullptr;
			const hlx::bytes* pInputLayout = nullptr;
			const hlx::bytes* pOutputLayout = nullptr;
			const Display::ShaderFunction* pFunction = nullptr;
		};

		class HLXSLFile
		{
		public:
			HLXSLFile();
			~HLXSLFile();

			bool Read(hlx::bytestream& _Stream);
			bool Write(hlx::bytestream& _Stream, const ShaderCompilationResult& _CompiledShaders, const HLXSLVersion _kVersion = HLXSLVersion_v1);

			const PermutationBufferRef GetPermuation(const uint32_t& _uPermutationIndex) const;
			const HLXSLPermutationIndex& GetPermutationIndex(const uint32_t& _uPermutationIndex) const;

			const uint32_t& GetPermutationCount() const;

			const TPermutationMasks& GetPermutationsMasks() const;

			const std::string& GetShaderName() const;

			const std::vector<Display::ShaderFunction>& GetFunctions() const;

			const CodeCompression GetCodeCompression() const;

		private:

			//---------------------------------------------------------------------------------------------------
			// HEADER
			//---------------------------------------------------------------------------------------------------

			static constexpr uint32_t HLXSLMagic = 0x666;

			HLXSLVersion m_kVersion = HLXSLVersion_Unknown;
			CodeCompression m_kCodeCompressionMethod = CodeCompression_None;
			uint32_t m_uFunctionCount = 0; //entrypoints
			uint32_t m_uPermutationCount = 0;
			uint32_t m_uPermutationMaskCount = 0;
			uint32_t m_uCodeCount = 0;
			uint32_t m_uInterfaceCount = 0;
			uint32_t m_uLayoutCount = 0;
			std::string m_sShaderName; // Main identifier of the shader

			TPermutationMasks m_uPermutationsMasks = { 0 };

			std::vector<Display::ShaderFunction> m_Functions;
			std::vector<HLXSLPermutationIndex> m_PermutationIndices;

			//---------------------------------------------------------------------------------------------------
			
			std::vector<hlx::bytes> m_CodeBuffer;
			std::vector<hlx::bytes> m_InterfaceBuffer;
			std::vector<hlx::bytes> m_LayoutBuffer;
			//---------------------------------------------------------------------------------------------------

		};

		inline const uint32_t& Helix::Resources::HLXSLFile::GetPermutationCount() const	{ return m_uPermutationCount;}
		inline const HLXSLPermutationIndex& HLXSLFile::GetPermutationIndex(const uint32_t& _uPermutationIndex) const { return m_PermutationIndices[_uPermutationIndex]; }
		inline const TPermutationMasks& HLXSLFile::GetPermutationsMasks() const { return m_uPermutationsMasks; }
		inline const std::string & HLXSLFile::GetShaderName() const { return m_sShaderName; }
		inline const std::vector<Display::ShaderFunction>& HLXSLFile::GetFunctions() const	{ return m_Functions; }
		inline const CodeCompression HLXSLFile::GetCodeCompression() const{return m_kCodeCompressionMethod;}
	} // Resources
} // Helix

#endif // HLXSLFILE_H