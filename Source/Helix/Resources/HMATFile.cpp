//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher, David Labode
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "HMATFile.h"
#include "hlx\src\StringHelpers.h"
#include "hlx\src\TextToken.h"

using namespace Helix;
using namespace Resources;

HMATFile::HMATFile() 
{	
}
//---------------------------------------------------------------------------------------------------------------------
HMATFile::HMATFile(const HMat& _OtherMaterial) :
	m_Material(_OtherMaterial)
{
}
//---------------------------------------------------------------------------------------------------------------------
HMATFile::~HMATFile() 
{
}

//---------------------------------------------------------------------------------------------------------------------
void HMATFile::Read(hlx::bytestream& _Stream)
{
	hlx::TextToken MaterialToken = hlx::TextToken(_Stream, "Material");

	m_Material.sName = MaterialToken.getValue("Name");
	m_Material.sMapAlbedo = MaterialToken.getValue("AlbedoMapPath");
	m_Material.sMapNormal = MaterialToken.getValue("NormalMapPath");
	m_Material.sMapMetallic = MaterialToken.getValue("MetallicMapPath");
	m_Material.sMapRoughness = MaterialToken.getValue("RoughnessMapPath");	
	m_Material.sMapEmissive = MaterialToken.getValue("EmissiveMapPath");	
	m_Material.sMapHeight = MaterialToken.getValue("HeightMapPath");
}
//---------------------------------------------------------------------------------------------------------------------
void HMATFile::Write(hlx::bytestream& _Stream)
{
	hlx::TextToken MaterialToken("Material");

	MaterialToken.set("Name", m_Material.sName);

	MaterialToken.set("AlbedoMapPath", m_Material.sMapAlbedo);
	MaterialToken.set("NormalMapPath", m_Material.sMapNormal);
	MaterialToken.set("MetallicMapPath", m_Material.sMapMetallic);
	MaterialToken.set("RoughnessMapPath", m_Material.sMapRoughness);
	MaterialToken.set("EmissiveMapPath", m_Material.sMapEmissive);
	MaterialToken.set("HeightMapPath", m_Material.sMapHeight);
	MaterialToken.serialize(_Stream);
}
//---------------------------------------------------------------------------------------------------------------------