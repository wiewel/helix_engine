//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef COMMONTYPETEXTSERIALIZER_H
#define COMMONTYPETEXTSERIALIZER_H

#include "hlx\src\Vector.h"
#include "hlx\src\TextToken.h"

#include "Math\MathTypes.h"

namespace Helix
{
	namespace Resources
	{
		// hlx -> helix / float
		inline Math::float2 to(const hlx::VecFloat2& v)
		{
			return{ v[0], v[1] };
		}

		inline Math::float3 to(const hlx::VecFloat3& v)
		{
			return{ v[0], v[1], v[2] };
		}

		inline Math::float4 to(const hlx::VecFloat4& v)
		{
			return{ v[0], v[1], v[2], v[3] };
		}

		inline Math::quaternion to_quat(const hlx::VecFloat4& v)
		{
			return{ v[0], v[1], v[2], v[3] };
		}

		// uint, i wish we had c++ concepts
		inline Math::uint2 to(const hlx::VecUInt2& v)
		{
			return{ v[0], v[1] };
		}

		inline Math::uint3 to(const hlx::VecUInt3& v)
		{
			return{ v[0], v[1], v[2] };
		}

		inline Math::uint4 to(const hlx::VecUInt4& v)
		{
			return{ v[0], v[1], v[2], v[3] };
		}

		// int
		inline Math::int2 to(const hlx::VecInt2& v)
		{
			return{ v[0], v[1] };
		}

		inline Math::int3 to(const hlx::VecInt3& v)
		{
			return{ v[0], v[1], v[2] };
		}

		inline Math::int4 to(const hlx::VecInt4& v)
		{
			return{ v[0], v[1], v[2], v[3] };
		}

		// helix -> hlx / float
		inline hlx::VecFloat2 to(const Math::float2& v)
		{
			return{ v.x, v.y };
		}

		inline hlx::VecFloat3 to(const Math::float3& v)
		{
			return{ v.x, v.y, v.z };
		}

		inline hlx::VecFloat4 to(const Math::float4& v)
		{
			return{ v.x, v.y, v.z, v.w };
		}

		inline hlx::VecFloat4 to(const Math::quaternion& v)
		{
			return{ v.x, v.y, v.z, v.w };
		}

		// uint
		inline hlx::VecUInt2 to(const Math::uint2& v)
		{
			return{ v.x, v.y };
		}

		inline hlx::VecUInt3 to(const Math::uint3& v)
		{
			return{ v.x, v.y, v.z };
		}

		inline hlx::VecUInt4 to(const Math::uint4& v)
		{
			return{ v.x, v.y, v.z, v.w };
		}

		// int
		inline hlx::VecInt2 to(const Math::int2& v)
		{
			return{ v.x, v.y };
		}

		inline hlx::VecInt3 to(const Math::int3& v)
		{
			return{ v.x, v.y, v.z };
		}

		inline hlx::VecInt4 to(const Math::int4& v)
		{
			return{ v.x, v.y, v.z, v.w };
		}

		//---------------------------------------------------------------------------------------------------

		inline Math::transform GetTransform (const std::string& _sTransfromName, const hlx::TextToken& _Token)
		{
			Math::transform vTransform;
			if (_Token.getKey() == _sTransfromName)
			{
				vTransform.p = to(_Token.getVector<float, 3>("Position"));
				vTransform.q = to_quat(_Token.getVector<float, 4>("Orientation", HQUATERNION_IDENTITY));
				return vTransform.getNormalized();
			}

			hlx::TextToken TmpToken;
			if (_Token.getTokenConst(_sTransfromName, TmpToken))
			{
				vTransform.p = to(TmpToken.getVector<float, 3>("Position"));
				vTransform.q = to_quat(TmpToken.getVector<float, 4>("Orientation", HQUATERNION_IDENTITY));
			}

			return vTransform.getNormalized();
		}
		//---------------------------------------------------------------------------------------------------

		inline void SetTranfsorm (const std::string& _sTransfromName, hlx::TextToken& _Token, const Math::transform& _Transform)
		{
			if (_Transform.p.isZero() && _Transform.q.isUnit())
			{
				return;
			}

			if (_Token.getKey() == _sTransfromName)
			{
				_Token.setVector("Position", to(_Transform.p));
				_Token.setVector("Orientation", to(_Transform.q));
			}
			else
			{
				hlx::TextToken Transform(_sTransfromName);
				Transform.setVector("Position", to(_Transform.p));
				Transform.setVector("Orientation", to(_Transform.q));
				_Token.addToken(_sTransfromName, Transform);
			}
		}
		//---------------------------------------------------------------------------------------------------

	} // Resources
} // Helix

#endif // !COMMONTYPETEXTSERIALIZER_H
