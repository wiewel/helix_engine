//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher, David Labode
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef H3DFILE_H
#define H3DFILE_H

#include "H3DHeader.h"
#include "hlx\src\ByteStream.h"

// Forward declarations:

namespace Helix
{
	namespace H3D
	{
		class OBJFile;
		class H3DSceneNode;
	}

	namespace Resources
	{
		struct ConvexMeshCluster
		{
			struct ConvexMesh
			{
				uint32_t uCRC32;
				hlx::bytes Data;
			};

			std::vector<ConvexMesh> Meshes;
		};

		class H3DFile
		{
			friend class H3D::OBJFile;
			friend class H3D::H3DSceneNode;
		public:
			H3DFile();
			~H3DFile();

			// Call this function before any other in this class!
			bool LoadHeader(_In_ hlx::bytestream& _Stream);

			Display::TMeshCluster GetMeshCluster() const;

			// parses a .h3d file to a prefilled MeshClusterDesc, you need to hold the stream alive until all buffers have been initialized!
			bool LoadDirectlyFromStream(_In_ hlx::bytestream& _Stream, _Out_ Display::TMeshCluster& _MeshClusterOutput, _Out_ Display::BufferSubresourceData& _VertexData, _Out_ Display::BufferSubresourceData& _IndexData);

			// load & convert .h3d file, vertex and index data will be stored in this object -> pointers in BufferSubresourceData are invalid as soons as the H3DFile object is being destroyed!
			bool Load(_In_ hlx::bytestream& _Stream, _Out_ Display::TMeshCluster& _MeshClusterOutput, _Out_ Display::BufferSubresourceData& _VertexData, _Out_ Display::BufferSubresourceData& _IndexData,
				_In_ const Display::TVertexDeclaration& _kTargetDeclaration, const Display::PixelFormat& _kTargetIndexFormat = Display::PixelFormat_R16_UInt);

			// loads cooked physx meshes
			bool Load(_In_ hlx::bytestream& _Stream, _Out_ ConvexMeshCluster& _ConvexClusterOutput);

			bool Save(_In_ hlx::bytestream& _Stream);

			void SetName(const std::string& _sName);

			const H3DHeader& GetHeader() const;

			const std::string& GetName() const;

		private:
			H3DHeader m_Header;
			std::vector<H3DHeader::SubMesh> m_MeshCluster;

			hlx::bytes m_VertexData;
			hlx::bytes m_IndexData;
			hlx::bytes m_PhysxData;
		};

		inline void H3DFile::SetName(const std::string & _sName) { m_Header.sName = _sName; }
		inline const H3DHeader& H3DFile::GetHeader() const 	{ return m_Header; }
		inline const std::string& H3DFile::GetName() const { return m_Header.sName; }
	} // Resources
} // Helix

#endif // H3DFILE_H