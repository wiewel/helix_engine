//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef COMMONTYPESERIALIZER_H
#define COMMONTYPESERIALIZER_H

#include "hlx\src\ByteStream.h"
#include "Math\MathTypes.h"
#include "Physics\GeometryDescriptions.h"
#include "Scene\LightDefines.h"

namespace Helix
{
	namespace Resources
	{
#pragma region Transform
		// READ:
		inline hlx::bytestream& operator >> (hlx::bytestream& stream, Math::transform& b)
		{
			stream.get(b.q);
			stream.get(b.p);
			return stream;
		}

		// const stream read
		inline hlx::const_bytestream& operator >> (hlx::const_bytestream& stream, Math::transform& b)
		{
			stream.get(b.q);
			stream.get(b.p);
			return stream;
		}

		// CONST const stream read
		inline const hlx::const_bytestream& operator >> (const hlx::const_bytestream& stream, Math::transform& b)
		{
			stream.get(b.q);
			stream.get(b.p);
			return stream;
		}

		// WRITE
		inline hlx::bytestream& operator<<(hlx::bytestream& stream, const  Math::transform& b)
		{
			stream.put(b.q);
			stream.put(b.p);
			return stream;
		}
#pragma endregion

#pragma region GeometryDesc
		// READ:
		inline hlx::bytestream& operator >> (hlx::bytestream& stream, Physics::GeometryDesc& b)
		{
			stream.get(b.kType);

			switch (b.kType)
			{
			case Physics::GeometryType_Box:
				stream.get(b.Box.vHalfExtents);
				break;
			case Physics::GeometryType_Sphere:
				stream.get(b.Sphere.fRadius);
				break;
			case Physics::GeometryType_Capsule:
				stream.get(b.Capsule.fRadius);
				stream.get(b.Capsule.fHalfHeight);
				break;
			case Physics::GeometryType_ConvexMesh:
				stream.get(b.ConvexMesh.uCRC32);
				break;
			}

			stream.get(b.Material.fStaticFriction);
			stream.get(b.Material.fDynamicFriction);
			stream.get(b.Material.fRestitution);

			stream.get(b.Material.kRestitutionCombineMode);
			stream.get(b.Material.kFrictionCombineMode);
			stream.get(b.Material.kFrictionFlags);

			stream.get(b.vScale);
			stream >> b.LocalTransform;

			return stream;
		}

		// const stream read
		inline hlx::const_bytestream& operator >> (hlx::const_bytestream& stream, Physics::GeometryDesc& b)
		{
			stream.get(b.kType);

			switch (b.kType)
			{
			case Physics::GeometryType_Box:
				stream.get(b.Box.vHalfExtents);
				break;
			case Physics::GeometryType_Sphere:
				stream.get(b.Sphere.fRadius);
				break;
			case Physics::GeometryType_Capsule:
				stream.get(b.Capsule.fRadius);
				stream.get(b.Capsule.fHalfHeight);
				break;
			case Physics::GeometryType_ConvexMesh:
				stream.get(b.ConvexMesh.uCRC32);
				break;
			}

			stream.get(b.Material.fStaticFriction);
			stream.get(b.Material.fDynamicFriction);
			stream.get(b.Material.fRestitution);

			stream.get(b.Material.kRestitutionCombineMode);
			stream.get(b.Material.kFrictionCombineMode);
			stream.get(b.Material.kFrictionFlags);

			stream.get(b.vScale);
			stream >> b.LocalTransform;

			return stream;
		}

		// CONST const stream read
		inline const hlx::const_bytestream& operator >> (const hlx::const_bytestream& stream, Physics::GeometryDesc& b)
		{
			stream.get(b.kType);

			switch (b.kType)
			{
			case Physics::GeometryType_Box:
				stream.get(b.Box.vHalfExtents);
				break;
			case Physics::GeometryType_Sphere:
				stream.get(b.Sphere.fRadius);
				break;
			case Physics::GeometryType_Capsule:
				stream.get(b.Capsule.fRadius);
				stream.get(b.Capsule.fHalfHeight);
				break;
			case Physics::GeometryType_ConvexMesh:
				stream.get(b.ConvexMesh.uCRC32);
				break;
			}

			stream.get(b.Material.fStaticFriction);
			stream.get(b.Material.fDynamicFriction);
			stream.get(b.Material.fRestitution);

			stream.get(b.Material.kRestitutionCombineMode);
			stream.get(b.Material.kFrictionCombineMode);
			stream.get(b.Material.kFrictionFlags);

			stream.get(b.vScale);
			stream >> b.LocalTransform;

			return stream;
		}

		// WRITE
		inline hlx::bytestream& operator<<(hlx::bytestream& stream, const  Physics::GeometryDesc& b)
		{
			stream.put(b.kType);

			switch (b.kType)
			{
			case Physics::GeometryType_Box:
				stream.put(b.Box.vHalfExtents);
				break;
			case Physics::GeometryType_Sphere:
				stream.put(b.Sphere.fRadius);
				break;
			case Physics::GeometryType_Capsule:
				stream.put(b.Capsule.fRadius);
				stream.put(b.Capsule.fHalfHeight);
				break;
			case Physics::GeometryType_ConvexMesh:
				stream.put(b.ConvexMesh.uCRC32);
				break;
			}

			stream.put(b.Material.fStaticFriction);
			stream.put(b.Material.fDynamicFriction);
			stream.put(b.Material.fRestitution);

			stream.put(b.Material.kRestitutionCombineMode);
			stream.put(b.Material.kFrictionCombineMode);
			stream.put(b.Material.kFrictionFlags);

			stream.put(b.vScale);
			stream << b.LocalTransform;

			return stream;
		}
#pragma endregion
	}// Resources
} 

#endif // COMMONTYPESERIALIZER_H