//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef RESOURCEFILEFORMAT_H
#define RESOURCEFILEFORMAT_H

#include "hlx\src\FileStream.h"
#include "hlx\src\Logger.h"
#include "Util\Flag.h"

namespace Helix {

	namespace Resources
	{
#define HRIF_VERSION 1
#define HRPF_VERSION 1
#define HRIF_MAGIC 0xA55FACE
#define HRPF_MAGIC 0xB00BA55
#define HR_INVALID 0xFFFFFFFF
		//ResourceIndexFile Format:

		//ResourceIndexFileHeader
		//directoryIndexSize x ResourceEntry 

		//ResourcePackageFile Format
		//ResourcePackageFileHeader
		//bytes File1
		//bytes File2
		//...

		//Extracted "local" files are in the subfolder packageIdentifier

		//header of the ResourceIndexFile (packageIdentifier.rif)
		struct ResourceIndexFileHeader
		{
			uint32_t magic; // 0 HRIF_MAGIC
			uint32_t entryCount; // 3 number of ResourceEntries
			uint32_t fileVersion; // 4
			uint16_t headerVersion;// 1
			uint16_t packageCount; // 2	number of archives associated with this file
			std::string packageIdentifier; // 5	filename prefix

			ResourceIndexFileHeader()
			{
				magic = HRIF_MAGIC;
				headerVersion = HRIF_VERSION;
				entryCount = 0;
				fileVersion = 0;
				packageCount = 0;
			}

			uint32_t Size()
			{
				return sizeof(uint32_t) * 3 + sizeof(uint16_t) * 2 + static_cast<uint32_t>(packageIdentifier.size()) + sizeof(char);
			}

			HDEBUGNAME("ResourceIndexFileHeader");

			bool Read(hlx::fbytestream& stream)
			{
				magic = stream.get<uint32_t>();

				if (magic != HRIF_MAGIC)
				{
					HERROR("Signature %X does not match %X", magic, HRIF_MAGIC);
					return false;
				}

				headerVersion = stream.get<uint32_t>();

				if (headerVersion != HRIF_VERSION)
				{
					HERROR("HeaderVersion %d not supported", headerVersion);
					return false;
				}

				packageCount = stream.get<uint32_t>();
				entryCount = stream.get<uint32_t>();
				fileVersion = stream.get<uint32_t>();
				packageIdentifier = stream.get<std::string>();

				return true;
			}

			void Write(hlx::fbytestream& stream)
			{
				stream.put<uint32_t>(magic);
				stream.put<uint32_t>(headerVersion);
				stream.put<uint32_t>(packageCount);
				stream.put<uint32_t>(entryCount);
				stream.put<uint32_t>(fileVersion);
				stream.put<std::string>(packageIdentifier);
				stream.put<char>('\0');
			}
		};

		Flag32E(ResourceEntryFlag)
		{
			ResourceEntryFlag_Directory = 1,
			ResourceEntryFlag_File = 1 << 1,
			ResourceEntryFlag_Compressed = 1 << 2,
			ResourceEntryFlag_Encrypted = 1 << 3,
			ResourceEntryFlag_LocalHasPriority = 1 << 4
		};

		//something like the file type
		enum ResourceType : uint32_t
		{
			ResourceType_Config = 0,
			ResourceType_Script = 1,
			ResourceType_Model = 2,
			ResourceType_Texture = 3,
			ResourceType_Shader = 4,
			ResourceType_Misc = 5
		};

		struct ResourceEntry
		{
			Flag<ResourceEntryFlag> entryType;
			ResourceType resourceType;

			uint32_t fileID;
			uint32_t parent; //parent id
			uint32_t child; //child id
			uint32_t next; //next id

			std::string name; // file or directory name with extension

			//only if entryType != directory
			uint16_t packageIndex; // indicates in which package file the file is stored
			uint32_t checksum; // crc32 of the data in the package, not the preload! use _mm_crc32_u32 ?
			uint32_t size; // size of file the package
			uint32_t m_uUncompressedSize;
			uint32_t m_uDecryptedSize;
			uint32_t preloadSize; // if this is anything other than 0 size has to be 0. its either preload or package file
			uint32_t offset; // offset of file the package

			//not written in the rif file but "calculated" while reading the entry
			uint32_t preloadOffset;

			ResourceEntry()
			{
				Reset();
			}

			void Reset()
			{
				entryType = ResourceEntryFlag_Directory;
				resourceType = ResourceType_Misc;
				fileID = HR_INVALID;
				parent = HR_INVALID;
				child = HR_INVALID;
				next = HR_INVALID;
				checksum = 0;
				size = 0;
				preloadSize = 0;
				m_uUncompressedSize = 0;
				m_uDecryptedSize = 0;
				offset = HR_INVALID;
				preloadOffset = HR_INVALID;
			}

			HDEBUGNAME("ResourceEntry");

			bool Read(hlx::fbytestream& stream)
			{
				entryType = static_cast<ResourceEntryFlag>(stream.get<uint32_t>());
				resourceType = static_cast<ResourceType>(stream.get<uint32_t>());
				fileID = stream.get<uint32_t>();
				parent = stream.get<uint32_t>();
				child = stream.get<uint32_t>();
				next = stream.get<uint32_t>();
				name = stream.get<std::string>();

				//check if the entry is NOT a directory
				if (entryType != ResourceEntryFlag_Directory)
				{
					packageIndex = stream.get<uint32_t>();
					checksum = stream.get<uint32_t>();
					size = stream.get<uint32_t>();
					m_uUncompressedSize = stream.get<uint32_t>();
					m_uDecryptedSize = stream.get<uint32_t>();
					preloadSize = stream.get<uint32_t>();
					offset = stream.get<uint32_t>();

					if (preloadSize > 0)
					{
						if (size > 0)
						{
							HERROR("%s has preload data but also package data", CSTR(name));
							return false;
						}

						preloadOffset = (uint32_t)stream.tellg(); //save the offset
						if (preloadOffset + preloadSize <= stream.size())
						{
							stream.seekg(preloadSize, std::ios::cur); //skip the preload data
						}
						else
						{
							HERROR("Unexpected end of file writing entry %s", CSTR(name));
							return false;
						}						
					}
				}

				return true;
			}

			void Write(hlx::fbytestream& stream)
			{
				stream.put<uint32_t>(entryType.GetFlag());
				stream.put<uint32_t>(resourceType);
				stream.put<uint32_t>(fileID);
				stream.put<uint32_t>(parent);
				stream.put<uint32_t>(child);
				stream.put<uint32_t>(next);
				stream.put<std::string>(name);
				stream.put<char>('\0');

				if (entryType != ResourceEntryFlag_Directory)
				{
					stream.put<uint16_t>(packageIndex);
					stream.put<uint32_t>(checksum);
					stream.put<uint32_t>(size);
					stream.put<uint32_t>(m_uUncompressedSize);
					stream.put<uint32_t>(m_uDecryptedSize);
					stream.put<uint32_t>(preloadSize);
					stream.put<uint32_t>(offset);
				}
			}

			void Write(hlx::fbytestream& stream, const hlx::bytes& preload)
			{
				if (entryType == ResourceEntryFlag_Directory)
				{
					return;
				}

				preloadSize = static_cast<uint32_t>(preload.size());
				size = 0;

				//write entry
				Write(stream);

				stream.put<hlx::bytes>(preload);
			}
		};

		//header of the ResourcePackageFile (packageIdentifier_packageIndex.rpf)
		struct ResourcePackageFileHeader
		{
			uint32_t magic; // 0 HRPF_MAGIX
			uint32_t fileVersion; // 3 should match the .rif file version
			uint16_t headerVersion; // 1 HRPF_VERSION
			uint16_t packageIndex; // 2 values between 0 and ResourceIndexFileHeader.packageCount 
			std::string packageIdentifier; // 4 should match the .rif identifier

			ResourcePackageFileHeader()
			{
				magic = HRPF_MAGIC;
				headerVersion = HRPF_VERSION;
				fileVersion = 0;
				packageIndex = 0;
			}

			uint32_t Size()
			{
				return sizeof(uint32_t) * 2 + sizeof(uint16_t) * 2 + static_cast<uint32_t>(packageIdentifier.size()) + sizeof(char);
			}

			HDEBUGNAME("ResourcePackageFileHeader");

			bool Read(hlx::fbytestream& stream)
			{
				magic = stream.get<uint32_t>();

				if (magic != HRPF_MAGIC)
				{
					HERROR("Signature %X does not match %X", magic, HRPF_MAGIC);
					return false;
				}

				headerVersion = stream.get<uint16_t>();

				if (headerVersion != HRPF_VERSION)
				{
					HERROR("HeaderVersion %d not supported", headerVersion);
					return false;
				}

				packageIndex = stream.get<uint16_t>();
				fileVersion = stream.get<uint32_t>();
				packageIdentifier = stream.get<std::string>();

				return true;
			}

			void Write(hlx::fbytestream& stream)
			{
				stream.put<uint32_t>(magic);
				stream.put<uint16_t>(headerVersion);
				stream.put<uint16_t>(packageIndex);
				stream.put<uint32_t>(fileVersion);
				stream.put<std::string>(packageIdentifier);
				stream.put<char>('\0');
			}
		};

	} // Resources

} // Helix

#endif //RESOURCEFILEFORMAT_H