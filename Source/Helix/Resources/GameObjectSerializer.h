//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef GAMEOBJECTSERIALIZER_H
#define GAMEOBJECTSERIALIZER_H

#include "CommonTypeTextSerializer.h"
#include "DataStructures\GameObjectDescription.h"

namespace Helix
{
	namespace Resources
	{
		struct GameObjectSerializer
		{
			static hlx::TextToken Serialize(const Datastructures::GameObjectDesc& _Desc);
			static Datastructures::GameObjectDesc Deserialize(const hlx::TextToken& _GOToken);
		};
	} // Resources
} // Helix

#endif // !GAMEOBJECTSERIALIZER_H
