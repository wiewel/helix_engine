//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef SHADERINTERFACESERIALIZER_H
#define SHADERINTERFACESERIALIZER_H

#include "Display\ViewDefines.h"
#include "hlx\src\ByteStream.h"

// forward declarations

namespace Helix
{
	namespace Resources
	{
		class ShaderInterfaceSerializer
		{
		public:
			ShaderInterfaceSerializer();
			~ShaderInterfaceSerializer();

			template<typename BYTES>
			bool Read(hlx::basestream<BYTES>& _Stream);
			void Write(hlx::bytestream& _Stream);

			bool HasMembers() const;

			void AddBuffer(const Display::ShaderBufferDesc& _Buffer);
			void AddTexture(const Display::ShaderTextureDesc& _Texture);
			void AddSampler(const Display::ShaderSamplerDesc& _Sampler);

			const Display::ShaderInterfaceDesc& GetInterface() const;

		private:
			// writes content of one variable
			void WriteShaderVariable(hlx::bytestream& _Stream, const Display::ShaderVariableDesc& _Variable);
			// recursively writes all members
			void WriteShaderMemberVariables(hlx::bytestream& _Stream, const Display::ShaderVariableDesc& _Variable);
			
			// reads content of one variable
			template<typename BYTES>
			Display::ShaderVariableDesc ReadShaderVariable(hlx::basestream<BYTES>& _Stream);
			// recursively reads all members
			template<typename BYTES>
			Display::ShaderVariableDesc ReadShaderMemberVariables(hlx::basestream<BYTES>& _Stream);
		private:
			uint32_t m_uSamplerCount = 0u;
			uint32_t m_uTextureCount = 0u;
			uint32_t m_uBufferCount = 0u;

			Display::ShaderInterfaceDesc m_ShaderInterface;
		};

		inline bool ShaderInterfaceSerializer::HasMembers() const
		{
			return m_ShaderInterface.Buffers.empty() == false || m_ShaderInterface.Samplers.empty() == false || m_ShaderInterface.Textures.empty() == false;
		}

		inline const Display::ShaderInterfaceDesc& ShaderInterfaceSerializer::GetInterface() const
		{
			return m_ShaderInterface;
		}		

		// Stream helpers:

		//---------------------------------------------------------------------------------------------------
		// read
		template <typename BYTES>
		inline void read_bindinfo(hlx::basestream<BYTES>& _Stream, Display::BindInfoDesc& _Desc)
		{
			std::string sName;
			_Stream >> sName;
			_Desc.sName = sName;

			_Stream >> _Desc.uSlot;
			_Stream >> _Desc.uSize;
			_Stream >> _Desc.uCount;
		}

		//---------------------------------------------------------------------------------------------------
		template<typename BYTES>
		inline bool ShaderInterfaceSerializer::Read(hlx::basestream<BYTES>& _Stream)
		{
			_Stream >> m_uSamplerCount;
			_Stream >> m_uTextureCount;
			_Stream >> m_uBufferCount;

			m_ShaderInterface.Samplers.resize(m_uSamplerCount);
			for (ShaderSamplerDesc& Sampler : m_ShaderInterface.Samplers)
			{
				read_bindinfo(_Stream, Sampler.BindInfo);
			}

			m_ShaderInterface.Textures.resize(m_uTextureCount);
			for (ShaderTextureDesc& Texture : m_ShaderInterface.Textures)
			{
				read_bindinfo(_Stream, Texture.BindInfo);

				_Stream >> Texture.kDimension;
				_Stream >> Texture.uNumOfSamples;
				_Stream >> Texture.kReturnType;
				_Stream >> Texture.bRWTexture;
			}

			m_ShaderInterface.Buffers.resize(m_uBufferCount);
			for (ShaderBufferDesc& Buffer : m_ShaderInterface.Buffers)
			{
				//_Stream >> Buffer.BindInfo;
				read_bindinfo(_Stream, Buffer.BindInfo);

				_Stream >> Buffer.kType;
				if (Buffer.kType >= ShaderBufferType_Unknown)
				{
					HERROR("Unknown buffer type %X", Buffer.Variables);
					return false;
				}

				uint32_t uVariableCount = 0u;
				// read variable count
				_Stream >> uVariableCount;

				Buffer.Variables.resize(uVariableCount);
				for (ShaderVariableDesc& Variable : Buffer.Variables)
				{
					Variable = ReadShaderMemberVariables(_Stream);
				}
			}

			return true;
		}

		//---------------------------------------------------------------------------------------------------

		template<typename BYTES>
		inline Display::ShaderVariableDesc ShaderInterfaceSerializer::ReadShaderVariable(hlx::basestream<BYTES>& _Stream)
		{
			ShaderVariableDesc Variable = {};

			_Stream >> Variable.sName;
			_Stream >> Variable.uStartOffset;
			_Stream >> Variable.uSize;
			_Stream >> Variable.uFlags;

			_Stream >> Variable.uRows;
			_Stream >> Variable.uColumns;
			_Stream >> Variable.uElements;
			_Stream >> Variable.uMembers;
			_Stream >> Variable.uStructureOffset;

			_Stream >> Variable.kClass;
			_Stream >> Variable.kType;

			_Stream >> Variable.uMembers;

			return Variable;
		}
		//---------------------------------------------------------------------------------------------------

		template<typename BYTES>
		inline Display::ShaderVariableDesc ShaderInterfaceSerializer::ReadShaderMemberVariables(hlx::basestream<BYTES>& _Stream)
		{
			ShaderVariableDesc Variable = ReadShaderVariable(_Stream);

			for (uint32_t i = 0u; i < Variable.uMembers; ++i)
			{
				Variable.Children.push_back(ReadShaderMemberVariables(_Stream));
			}

			return Variable;
		}

	} // Compiler
} // Helix

#endif // SHADERINTERFACESERIALIZER_H