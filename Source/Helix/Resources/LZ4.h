//   Copyright 2020 Fabian Wahlster
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef LZ4_H
#define LZ4_H

#include "hlx\src\Bytes.h"
#include "LZ4\include\lz4.h"

namespace Helix
{
	namespace Resources
	{
		class LZ4
		{
		public:
			static bool Compress(const hlx::bytes& _InputData, hlx::bytes& _OutputData, int32_t _uCompressionAcelleration = 1);
			static bool Uncompress(const hlx::bytes& _InputData, hlx::bytes& _OutputData, size_t _uUncompressedSize);
		};

		inline bool LZ4::Compress(const hlx::bytes& _InputData, hlx::bytes& _OutputData, int32_t _uCompressionAcelleration)
		{
			if (_InputData.empty())
			{
				return false;
			}

			size_t uEstimatedSize = LZ4_COMPRESSBOUND(_InputData.size());
			_OutputData.resize(uEstimatedSize);

			int iLength = LZ4_compress_fast(
				reinterpret_cast<const char*>(_InputData.c_str()),
				reinterpret_cast<char*>(&_OutputData[0]),
				static_cast<int>(_InputData.size()),
				static_cast<int>(uEstimatedSize),
				_uCompressionAcelleration);

			_OutputData.resize(std::max(iLength, 0));

			return iLength > 0;
		}

		inline bool LZ4::Uncompress(const hlx::bytes& _InputData, hlx::bytes& _OutputData, size_t _uUncompressedSize)
		{
			if (_InputData.empty())
			{
				return false;
			}

			_uUncompressedSize = std::max(_uUncompressedSize, _InputData.size());

			_OutputData.resize(_uUncompressedSize);

			return LZ4_decompress_safe(
				reinterpret_cast<const char*>(_InputData.c_str()),
				reinterpret_cast<char*>(&_OutputData[0]),
				static_cast<int>(_InputData.size()),
				static_cast<int>(_uUncompressedSize)) > 0;
		}
	} // Resources
} // Helix

#endif // !LZ4_H
