//   Copyright 2020 Fabian Wahlster
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "LZMA.h"

//AES instruction set
#ifndef MY_CPU_X86_OR_AMD64
#define MY_CPU_X86_OR_AMD64
#endif

#include "LzmaEnc.h"
#include "LzmaDec.h"

#include "Aes.h"

#include "hlx\src\Bytes.h"

using namespace Helix::Resources;

static void * AllocForLzma(void *p, size_t size) { return malloc(size); }
static void FreeForLzma(void *p, void *address) { free(address); }
static ISzAlloc SzAllocForLzma = { &AllocForLzma, &FreeForLzma };

//---------------------------------------------------------------------------------------------------
LZMA::LZMA()
{
	AesGenTables();
}
//---------------------------------------------------------------------------------------------------
void LZMA::Encrypt(hlx::bytes& _Data, uint8_t _pKey[16], uint8_t _pIV[16])
{
	uint32_t* pIV = reinterpret_cast<uint32_t*>(_pIV);

	AesCbc_Init(pIV, _pKey);

	size_t uRndSize = _Data.size() % AES_BLOCK_SIZE;
	size_t uSize = _Data.size() + uRndSize;

	if (_Data.size() < uSize)
	{
		_Data.resize(uSize);
	}

	size_t uNumBlocks = uSize / AES_BLOCK_SIZE;
	g_AesCbc_Encode(pIV, &_Data[0], uNumBlocks);
}
//---------------------------------------------------------------------------------------------------
bool LZMA::Decrypt(hlx::bytes& _Data, uint8_t _pKey[16], uint8_t _pIV[16])
{
	uint32_t* pIV = reinterpret_cast<uint32_t*>(_pIV);

	AesCbc_Init(pIV, _pKey);

	size_t uSize = _Data.size();
	if (uSize == 0 || uSize % AES_BLOCK_SIZE != 0)
	{
		return false;
	}

	size_t uNumBlocks = uSize / AES_BLOCK_SIZE;
	g_AesCbc_Decode(pIV, &_Data[0], uNumBlocks);

	return true;
}
//---------------------------------------------------------------------------------------------------
bool LZMA::Compress(const hlx::bytes& _InputData, hlx::bytes& _OutputData, int32_t _uCompressionLevel, uint32_t _uDictionarySize)
{
	//return if there is nothing to compress
	if (_InputData.size() == 0)
		return false;

	size_t uPropSize = LZMA_PROPS_SIZE;

	//initialize properties
	CLzmaEncProps props;
	LzmaEncProps_Init(&props);

	//default compression level = 4
	props.level = (_uCompressionLevel < 0 || _uCompressionLevel > 9) ? 4 : _uCompressionLevel;
	//default dictionary size = 16mb
	props.dictSize = (_uDictionarySize < 0x1000 || _uDictionarySize > 0x40000000) ? 0x1000000 : _uDictionarySize;
	//write the endmark we already know the size of the data
	props.writeEndMark = 1;

	size_t uInputSize = _InputData.size();

	//see link below for output buffer size:
	//http://sourceforge.net/p/sevenzip/discussion/45798/thread/dd3b392c/
	size_t uOutputSize = uInputSize + (uInputSize / 40) + 4096;

	//props are saved infront of the output buffer so they can be read befor uncompressing
	size_t uResultsSize = LZMA_PROPS_SIZE + uOutputSize;

	//resize if the output buffer is too small
	if (_OutputData.size() < uResultsSize)
		_OutputData.resize(uResultsSize);
	
	SRes iRes = LzmaEncode(&_OutputData[LZMA_PROPS_SIZE], &uResultsSize, //define output buffer
							&_InputData[0], uInputSize, //input buffer
							&props, &_OutputData[0], &uPropSize, props.writeEndMark, //props and prop location in output buffer
							NULL, &SzAllocForLzma, &SzAllocForLzma); // define allocators

	if (iRes != SZ_OK)
		return false;

	//resize output buffer to actual result size using infos gained from the encoding process
	_OutputData.resize(uPropSize + uResultsSize);
	
	return true;
}

//---------------------------------------------------------------------------------------------------

bool LZMA::Uncompress(const hlx::bytes& _InputData, hlx::bytes& _OutputData, size_t _uUncompressedSize)
{
	//no data to compress
	if (_InputData.size() == 0)
		return false;

	if (_uUncompressedSize == 0)
	{
		if (_OutputData.size() > _InputData.size())
		{
			//if theres already allocated mem in the output buffer, use as uncompressed size
			_uUncompressedSize = _OutputData.size();
			//no need to resize, buffer is already allocated
		}
		else
		{
			//in case no uncompressed size was specified we just assume the compression ration was 0.5 (which is very hacky)
			_uUncompressedSize = _InputData.size() * 2;
			_OutputData.resize(_uUncompressedSize);
		}
	}
	else
	{
		//we assume the uncompressed size is correct, no further checks
		_OutputData.resize(_uUncompressedSize);
	}

	ELzmaStatus status;

	size_t uInputSize = _InputData.size() - LZMA_PROPS_SIZE;
	size_t uResultSize = _uUncompressedSize;

	SRes iRes = LzmaDecode( &_OutputData[0], &uResultSize, //define output buffer
							&_InputData[LZMA_PROPS_SIZE], &uInputSize, // input source
							&_InputData[0], LZMA_PROPS_SIZE, //properties 
							LZMA_FINISH_ANY, &status, //mode
							&SzAllocForLzma); // allocator
	if (iRes != SZ_OK)
		return false;

	//resize to actuall uncompressed size (should be equal)
	_OutputData.resize(uResultSize);

	//optional: check status flags

	return true;
}

