//   Copyright 2020 Fabian Wahlster, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "RIFF.h"
#include <algorithm>

using namespace Helix::Resources;
//==================================================================================================================================
// RIFF CHUNK
//----------------------------------------------------------------------------------------------------------------------------------
RIFFChunk::RIFFChunk()
{
}
//----------------------------------------------------------------------------------------------------------------------------------
RIFFChunk::RIFFChunk(FourCC _ID, uint32_t _uDataSize): m_Type(_ID), m_uDataSize(_uDataSize)
{
}
//----------------------------------------------------------------------------------------------------------------------------------
RIFFChunk::~RIFFChunk()
{
}
//----------------------------------------------------------------------------------------------------------------------------------
bool RIFFChunk::Read(hlx::fbytestream& _Stream)
{
	// check for corruption of RIFF file
	if (_Stream.available() < m_uDataSize)
	{
		return false;
	}

	// load file content to local 
	m_Data = hlx::bytes(_Stream.get<hlx::bytes>(m_uDataSize));
	return true;
}
//----------------------------------------------------------------------------------------------------------------------------------
const RIFFChunk::FourCC& Helix::Resources::RIFFChunk::GetType() const
{
	return m_Type;
}
//----------------------------------------------------------------------------------------------------------------------------------
hlx::bytes RIFFChunk::GetData() const
{
	return hlx::bytes(m_Data);
}

//==================================================================================================================================
// RIFF FILE
//----------------------------------------------------------------------------------------------------------------------------------
RIFFFile::RIFFFile()
{
}
//----------------------------------------------------------------------------------------------------------------------------------
const RIFFFile::FourCC& RIFFFile::GetFileType() const
{
	return m_FileType;
}
//----------------------------------------------------------------------------------------------------------------------------------
// RIFF root chunks have a different header (4B fourcc, 4B data size, 4B file type)
bool RIFFFile::Read(hlx::fbytestream & _Stream)
{
	if (_Stream.available() < 3 * sizeof(uint32_t))
	{
		return false;
	}

	_Stream >> m_Type;
	_Stream >> m_uDataSize;
	_Stream >> m_FileType;

	// add offset for file type field
	m_uDataSize -= 4u;

	// check for corruption of RIFF file
	if (_Stream.available() < m_uDataSize)
	{
		return false;
	}

	// find child chunks in data
	bool bSuccess = true;
	while (_Stream.available() > 2 * sizeof(uint32_t) && bSuccess)
	{
		FourCC ChildType = _Stream.get<RIFFChunk::FourCC>(); // 4B of type in four char code
		uint32_t uChildDataSize = _Stream.get<uint32_t>(); // 4B of payload
		RIFFChunk Child(ChildType, uChildDataSize);
		if (bSuccess = Child.Read(_Stream))
		{
			m_Children.push_back(Child);
		}
	}
	return bSuccess;
}
//----------------------------------------------------------------------------------------------------------------------------------
hlx::bytes RIFFFile::Find(FourCC _Type)
{
	for (RIFFChunk Chunk : m_Children)
	{
		if (Chunk.GetType() == _Type)
		{
			return Chunk.GetData();
		}
	}
	return hlx::bytes();
}