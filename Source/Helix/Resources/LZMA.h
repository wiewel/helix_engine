//   Copyright 2020 Fabian Wahlster
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef LZMA_H
#define LZMA_H

#include "hlx\src\Logger.h"
#include "hlx\src\Bytes.h"

// forward declarations

namespace Helix
{
	namespace Resources
	{
		struct AES128Key
		{
			uint8_t m_pKey[16];
			uint8_t m_pIV[16];
		};

		class LZMA
		{
		public:
			HDEBUGNAME("LZMA");
			LZMA();

			//AES encryption
			static void Encrypt(hlx::bytes& _Data, uint8_t _pKey[16], uint8_t _pIV[16]);
			static bool Decrypt(hlx::bytes& _Data, uint8_t _pKey[16], uint8_t _pIV[16]);

			//LZMA compression
			static bool Compress(const hlx::bytes& _InputData, hlx::bytes& _OutputData, int32_t _uCompressionLevel = 4, uint32_t _uDictionarySize = 0x1000000);
			static bool Uncompress(const hlx::bytes& _InputData, hlx::bytes& _OutputData, size_t _uUncompressedSize = 0);
		}; // LZMA
	} // Resources
} // Helix

#endif