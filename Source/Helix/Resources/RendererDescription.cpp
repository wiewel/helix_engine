//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "RendererDescription.h"
using namespace Helix::Resources;

//---------------------------------------------------------------------------------------------------

RendererDesc::RendererDesc(const hlx::TextToken& _InputToken)
{
	auto ProcRange = _InputToken.getAllTokens("RenderProcedure");

	for (std::multimap<std::string, hlx::TextToken>::const_iterator pit = ProcRange.first; pit != ProcRange.second; ++pit)
	{
		const hlx::TextToken& ProcToken = pit->second;

		RenderProcedureDesc Proc = {};

		Proc.sProcedureName = ProcToken.getValue("Name");
		Proc.bEnabled = ProcToken.get<bool>("Enabled");

		auto PassRange = ProcToken.getAllTokens("RenderPass");

		for (std::multimap<std::string, hlx::TextToken>::const_iterator it = PassRange.first; it != PassRange.second; ++it)
		{
			Proc.Passes.push_back(RenderPassDesc(it->second));
		}

		Procedures.push_back(Proc);
	}
}
//---------------------------------------------------------------------------------------------------

hlx::TextToken RendererDesc::Serialize() const
{
	hlx::TextToken Token("Renderer");

	// serialize procedures
	for (const RenderProcedureDesc& ProcDesc : Procedures)
	{
		hlx::TextToken ProcToken("RenderProcedure");

		// procedure info
		ProcToken.set("Name", ProcDesc.sProcedureName);
		ProcToken.set("Enabled", ProcDesc.bEnabled);

		// serialize passes
		for (const RenderPassDesc& PassDesc : ProcDesc.Passes)
		{
			ProcToken.addToken("RenderPass", PassDesc.Serialize());
		}
		
		Token.addToken("RenderProcedure", ProcToken);
	}

	return Token;
}
//---------------------------------------------------------------------------------------------------
