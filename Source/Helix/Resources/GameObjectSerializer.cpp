//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "GameObjectSerializer.h"

using namespace Helix::Math;
using namespace Helix::Physics;
using namespace Helix::Datastructures;
using namespace Helix::Resources;
//---------------------------------------------------------------------------------------------------

hlx::TextToken GameObjectSerializer::Serialize(const GameObjectDesc& _Desc)
{
	hlx::TextToken GOToken("Object");
	GOToken.set("Type", _Desc.kType);
	SetTranfsorm("Transform", GOToken, _Desc.Transform);

	if (_Desc.vRenderingScale != float3(HFLOAT3_ONE))
	{
		GOToken.setVector("RenderingScale", to(_Desc.vRenderingScale));
	}
	GOToken.set("Flags", static_cast<uint32_t>(_Desc.kFlags.GetFlag()));

	GOToken.set("Mesh", _Desc.sMeshFileName);
	GOToken.addContainer<std::string>("SubName", _Desc.sMeshClusterSubNames.begin(), _Desc.sMeshClusterSubNames.end());
	GOToken.addContainer<std::string>("Material", _Desc.sMaterialNames.begin(), _Desc.sMaterialNames.end());
	GOToken.addContainer<std::string>("Tag", _Desc.sTags.begin(), _Desc.sTags.end());

	GOToken.set("Name", _Desc.sObjectName);

	GOToken.set("CCD", _Desc.bContinuousCollisionDetection);
	GOToken.set("Mass", _Desc.fMass);

	// TODO: child objects

	return GOToken;
}
//---------------------------------------------------------------------------------------------------

GameObjectDesc GameObjectSerializer::Deserialize(const hlx::TextToken& _GOToken)
{
	GameObjectDesc Desc;

	Desc.kType =  _GOToken.get<GameObjectType>("Type", GameObjectType_Unknown);
	Desc.Transform = GetTransform("Transform", _GOToken);
	Desc.vRenderingScale = to(_GOToken.getVector<float, 3>("RenderingScale", HFLOAT3_ONE));
	Desc.kFlags = static_cast<GameObjectFlag>(_GOToken.get<uint32_t>("Flags", GameObjectFlag_None));

	Desc.sMeshFileName = _GOToken.getValue("Mesh");
	Desc.sMeshClusterSubNames = _GOToken.getAllValues("SubName");
	Desc.sMaterialNames = _GOToken.getAllValues("Material");
	Desc.sTags = _GOToken.getAllValues("Tag");
	Desc.sObjectName = _GOToken.getValue("Name");

	Desc.bContinuousCollisionDetection = _GOToken.get<bool>("CCD");
	Desc.fMass = _GOToken.get<float>("Mass", 1.f);

	return Desc;
}
//---------------------------------------------------------------------------------------------------
