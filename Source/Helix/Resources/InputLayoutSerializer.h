//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef INPUTLAYOUTSERIALIZER_H
#define INPUTLAYOUTSERIALIZER_H

#include "hlx\src\StandardDefines.h"
#include <vector>
#include "Display\ViewDefines.h"
#include "hlx\src\ByteStream.h"

namespace Helix
{
	namespace Resources
	{
		class InputLayoutSerializer
		{
		public:
			InputLayoutSerializer();
			~InputLayoutSerializer();

			void Clear();

			template <typename BYTES>
			bool Read(hlx::basestream<BYTES>& _Stream);

			void Write(hlx::bytestream& _Stream);

			void AddInputElement(const Display::InputElementDesc& _InputElement);

			const std::vector<Display::InputElementDesc>& GetInputElementDescription() const;
		private:
			uint32_t m_uElementCount = 0;

			std::vector<Display::InputElementDesc> m_InputElements;
		};

		inline const std::vector<Display::InputElementDesc>& InputLayoutSerializer::GetInputElementDescription() const
		{
			return m_InputElements;
		}

		template<typename BYTES>
		inline bool InputLayoutSerializer::Read(hlx::basestream<BYTES>& _Stream)
		{
			_Stream >> m_uElementCount;

			m_InputElements.resize(m_uElementCount);
			for (InputElementDesc& InputElement : m_InputElements)
			{
				_Stream >> InputElement.sSemanticName;
				_Stream >> InputElement.sVariableName;
				_Stream >> InputElement.uSemanticIndex;
				_Stream >> InputElement.uRegister;
				_Stream >> InputElement.kSystemValueType;
				_Stream >> InputElement.kShaderComponentType;
				_Stream >> InputElement.uMask;
				_Stream >> InputElement.uReadWriteMask;
				_Stream >> InputElement.uStream;
				_Stream >> InputElement.kMinPrecision;
			}

			return true;
		}
	} // Compiler
} // Helix

#endif //INPUTLAYOUTSERIALIZER