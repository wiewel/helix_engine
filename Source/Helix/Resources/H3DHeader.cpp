//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "H3DHeader.h"

using namespace Helix::Resources;
using namespace Helix::Display;
//---------------------------------------------------------------------------------------------------

bool H3DHeader::Read(hlx::bytestream& _Stream)
{
	if (_Stream.good() == false ||_Stream.get<uint32_t>() != H3DMagic)
	{
		HERROR("Invalid H3DHeader identifier (expected 0x%X)", H3DMagic);
		return false;
	}

	uint32_t uTemp = 0;
	_Stream >> uTemp;

	if (uTemp >= H3DVersion_Unknown)
	{
		HERROR("Unknown H3D version %d", kVersion);
		return false;
	}

	kVersion = static_cast<H3DVersion>(uTemp);

	_Stream >> uVertexDataOffset;
	_Stream >> uVertexDataSize;
	_Stream >> uIndexDataOffset;
	_Stream >> uIndexDataSize;
	_Stream >> uPhysXDataOffset;
	_Stream >> uPhysXDataSize;
	_Stream >> uClusterCount;

	_Stream >> uTemp; // kFlags
	if (uTemp >= (1 << H3DFlag_NumOf))
	{
		HERROR("Unknown H3DFlag %d", uTemp);
		return false;
	}

	kFlags = static_cast<H3DFlag>(uTemp);

	_Stream >> sName;

	return _Stream.good();
}
//---------------------------------------------------------------------------------------------------

void H3DHeader::Write(hlx::bytestream& _Stream) const
{
	_Stream << H3DMagic;

	_Stream << kVersion;
	_Stream << uVertexDataOffset;
	_Stream << uVertexDataSize;
	_Stream << uIndexDataOffset;
	_Stream << uIndexDataSize;
	_Stream << uPhysXDataOffset;
	_Stream << uPhysXDataSize;
	_Stream << uClusterCount;
	_Stream << kFlags;
	
	_Stream << sName;
}

//---------------------------------------------------------------------------------------------------

bool H3DHeader::SubMesh::Read(hlx::bytestream& _Stream)
{
	_Stream >> sSubName;

	//Vertex Info
	_Stream >>  uVertexByteOffset; // offset from first byte in the buffer to the first vertex to be drawn in byte
	_Stream >>  uVertexStrideSize;
	_Stream >>  uVertexCount; // VertexCount * StrideSize = SubMeshSize

	uint32_t uTemp = 0u;
	_Stream >> uTemp;

	if (uTemp >= (1 << VertexLayout_NumOf))
	{
		HERROR("Unknown VertexLayout for Cluster %s", CSTR(sSubName));
		return false;
	}

	kVertexDeclaration = static_cast<VertexLayout>(uTemp);

	_Stream >> uTemp;
	
	if (uTemp >= PrimitiveTopology_NumOf)
	{
		HERROR("Unknown PrimitiveTopology for Cluster %s", CSTR(sSubName));
		return false;
	}

	kPrimitiveTopology = static_cast<PrimitiveTopology>(uTemp);
	
	// Index Info
	_Stream >>  uIndexByteOffset;// offset from first byte in the buffer to the first index to be drawn in byte
	_Stream >>  uIndexCount; // IndexCount * PixelFormatSize = Total index size
	_Stream >>  uIndexElementSize ; // 2 or 4 byte

	_Stream >> uTemp; // kIndexBufferFormat

	if (uTemp != PixelFormat_R32_UInt && uTemp != PixelFormat_R16_UInt)
	{
		HERROR("Unknown IndexFormat for Cluster %s", CSTR(sSubName));
		return false;
	}

	kIndexBufferFormat = static_cast<PixelFormat>(uTemp);

	// PhysxMesh
	_Stream >> uConvexPhysxMeshOffset;
	_Stream >> uConvexPhysxMeshSize;

	return true;
}
//---------------------------------------------------------------------------------------------------

void H3DHeader::SubMesh::Write(hlx::bytestream & _Stream) const
{
	_Stream << sSubName;

	//Vertex Info
	_Stream << uVertexByteOffset; // offset from first byte in the buffer to the first vertex to be drawn in byte
	_Stream << uVertexStrideSize;
	_Stream << uVertexCount; // VertexCount * StrideSize = SubMeshSize
	_Stream << kVertexDeclaration;
	_Stream << kPrimitiveTopology;

	// Index Info
	_Stream << uIndexByteOffset;// offset from first byte in the buffer to the first index to be drawn in byte
	_Stream << uIndexCount; // IndexCount * PixelFormatSize = Total index size
	_Stream << uIndexElementSize; // 2 or 4 byte
	_Stream << kIndexBufferFormat; // PixelFormat_R16_UInt or PixelFormat_R32_UInt

	// PhysX Mesh
	_Stream << uConvexPhysxMeshOffset;
	_Stream << uConvexPhysxMeshSize;
}
//---------------------------------------------------------------------------------------------------