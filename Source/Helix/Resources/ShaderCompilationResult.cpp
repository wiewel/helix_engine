//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "ShaderCompilationResult.h"
#include "Resources\LZMA.h"

#include "Util\CRC32.h"
#include "hlx\src\Bytes.h"

using namespace Helix::Display;
using namespace Helix::Resources;
//---------------------------------------------------------------------------------------------------

void ShaderCompilationResult::AddFunction(const ShaderFunction& _Function)
{
	m_Functions.push_back(_Function);
}
//---------------------------------------------------------------------------------------------------
void ShaderCompilationResult::AddPermutation(const ShaderFunction& _Function, const HLXSLPermutation& _NewPermutation)
{
	m_CompiledPermutations.insert({ _Function.GetHash(), _NewPermutation });
}
//---------------------------------------------------------------------------------------------------
void ShaderCompilationResult::SetPermutationMask(const uint32_t& _uMask, const uint32_t& _uShaderTypeIndex)
{
	HASSERTD(_uShaderTypeIndex < m_uPermutationsMasks.size(), "Invalid shader type index");
	m_uPermutationsMasks[_uShaderTypeIndex] = _uMask;
}
//---------------------------------------------------------------------------------------------------
uint32_t ShaderCompilationResult::AddCode(hlx::bytes&& Code, _Out_ uint32_t& _uCompressedSizeOut, uint32_t uOrigCRC)
{
	uint32_t uCRC = uOrigCRC == 0u ? CRC32(Code) : uOrigCRC;

	TParallelUnoderedByteMap::iterator it = m_CodeBuffer.find(uCRC);
	if (it == m_CodeBuffer.end())
	{
		hlx::bytes CompressedCode;
		bool bSuccess = false;

		switch (m_kCompressionMethod)
		{
		case CodeCompression_LZMA:
			bSuccess = LZMA::Compress(Code, CompressedCode, m_iLZMACompressionLevel, m_uLZMADictionarySize);
			break;
		//case CodeCompression_D3D:
			// do nothing because the compiler has to compress with D3D functions
		case CodeCompression_Unknown:
		case CodeCompression_None:
		default:
			CompressedCode.swap(Code);
			bSuccess = true;
			break;
		}

		if (bSuccess)
		{
			_uCompressedSizeOut = static_cast<uint32_t>(CompressedCode.size());
			m_CodeBuffer.insert({ uCRC , std::move(CompressedCode) });
		}
	}
	else
	{
		_uCompressedSizeOut = static_cast<uint32_t>(it->second.size());
	}

	return uCRC;
}
//---------------------------------------------------------------------------------------------------
uint32_t ShaderCompilationResult::AddInterface(hlx::bytes&& Interface)
{
	uint32_t uCRC = CRC32(Interface);

	if (m_InterfaceBuffer.count(uCRC) == 0)
	{
		m_InterfaceBuffer.insert({ uCRC , std::move(Interface) });
	}

	return uCRC;
}
//---------------------------------------------------------------------------------------------------
uint32_t ShaderCompilationResult::AddLayout(hlx::bytes&& Layout)
{
	uint32_t uCRC = CRC32(Layout);

	if (m_InputOutputLayoutBuffer.count(uCRC) == 0)
	{
		m_InputOutputLayoutBuffer.insert({ uCRC , std::move(Layout) });
	}

	return uCRC;
}
//---------------------------------------------------------------------------------------------------
void ShaderCompilationResult::Reset()
{
	for (uint32_t i = 0; i < Display::ShaderType_NumOf + 1; ++i)
	{
		m_uPermutationsMasks[i] = 0;
	}

	m_sShaderName.resize(0);
	m_Functions.resize(0);
	m_CompiledPermutations.clear();
	m_CodeBuffer.clear();
	m_InterfaceBuffer.clear();
	m_InputOutputLayoutBuffer.clear();
}
//---------------------------------------------------------------------------------------------------
void ShaderCompilationResult::PrintStats(const uint32_t& _uTotalPermutations)
{
	HLOG("Compiled %u permutations of %u using %u code buffers with %u different interfaces and %u IO-Layouts",
		m_CompiledPermutations.size(), _uTotalPermutations, m_CodeBuffer.size(), m_InterfaceBuffer.size(), m_InputOutputLayoutBuffer.size());
}
//---------------------------------------------------------------------------------------------------
void ShaderCompilationResult::PrunePermutationsWithRange(const size_t& _uFunction, const uint32_t& _uRangeIndex)
{
	std::pair<TPermutationMap::const_iterator, TPermutationMap::const_iterator> Range = m_CompiledPermutations.equal_range(_uFunction);
	for (TPermutationMap::const_iterator it = Range.first; it != Range.second;)
	{
		// has valid range
		if (_uRangeIndex < it->second.Hash.Ranges.size())
		{
			// is not a null permutation
			if (it->second.Hash.Ranges.at(_uRangeIndex) != 0u)
			{
				HLOG("Pruning Permutation P%u IO%u %llu",
					it->second.Hash.uClassPermutation,
					it->second.Hash.uIOPermutation,
					it->second.Hash.GetRangeHash());

				it = m_CompiledPermutations.unsafe_erase(it);
				continue;
			}
		}

		++it;
	}
}
//---------------------------------------------------------------------------------------------------