//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher, David Labode
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "H3DFile.h"
#include "hlx\src\Logger.h"
#include "Util\CRC32.h"

using namespace Helix;
using namespace Helix::Resources;
//---------------------------------------------------------------------------------------------------

H3DFile::H3DFile()
{
}
//---------------------------------------------------------------------------------------------------

H3DFile::~H3DFile()
{
}
//---------------------------------------------------------------------------------------------------
bool H3DFile::LoadHeader(hlx::bytestream & _Stream)
{
	if (m_Header.Read(_Stream) == false)
	{
		return false;
	}

	if (_Stream.get_offset() != m_Header.uVertexDataOffset)
	{
		HERROR("Invalid VertexDataOffset %d for %s", m_Header.uVertexDataOffset, CSTR(m_Header.sName));
		return false;
	}

	if (_Stream.available() < m_Header.uVertexDataSize)
	{
		HERROR("Stream does not hold enough vertex data");
		return false;
	}

	_Stream.set_offset(m_Header.uIndexDataOffset);

	if (_Stream.available() < m_Header.uIndexDataSize)
	{
		HERROR("Stream does not hold enough index data");
		return false;
	}
	
	_Stream.set_offset(m_Header.uPhysXDataOffset);

	if (_Stream.available() < m_Header.uPhysXDataSize)
	{
		HERROR("Stream does not hold enough physx data");
		return false;
	}

	// ##### H3D Format Layout #####
	// Header
	// VertexData
	// IndexData (optional)
	// PhysXData (optional)
	// MeshCluster

	// skip stream to after the end of the data section (where the cluster info is stored)
	if (m_Header.uPhysXDataSize > 0u && m_Header.uPhysXDataOffset > 0u)
	{
		_Stream.set_offset(m_Header.uPhysXDataOffset + m_Header.uPhysXDataSize);
	}
	else if (m_Header.uIndexDataSize > 0u && m_Header.uIndexDataOffset > 0u)
	{
		_Stream.set_offset(m_Header.uIndexDataOffset + m_Header.uIndexDataSize);
	}
	else
	{
		_Stream.set_offset(m_Header.uVertexDataOffset + m_Header.uVertexDataSize);
	}

	m_MeshCluster.resize(m_Header.uClusterCount);

	for (H3DHeader::SubMesh& Mesh : m_MeshCluster)
	{
		if (Mesh.Read(_Stream) == false)
		{
			return false;
		}
	}

	return true;
}
//---------------------------------------------------------------------------------------------------
Display::TMeshCluster H3DFile::GetMeshCluster() const
{
	Display::TMeshCluster MeshClusterOutput(m_Header.uClusterCount);

	// copy mesh cluster data
	uint32_t uOutMesh = 0;
	for (const H3DHeader::SubMesh& InputMesh : m_MeshCluster)
	{
		Display::SubMesh& OutputMesh = MeshClusterOutput[uOutMesh++];
		OutputMesh.m_uVertexCount = InputMesh.uVertexCount;
		OutputMesh.m_uIndexCount = InputMesh.uIndexCount;
		OutputMesh.m_sSubName = InputMesh.sSubName;
		OutputMesh.m_kPrimitiveTopology = InputMesh.kPrimitiveTopology;

		OutputMesh.m_uVertexByteOffset = InputMesh.uVertexByteOffset;
		OutputMesh.m_uVertexStrideSize = InputMesh.uVertexStrideSize;
		OutputMesh.m_kVertexDeclaration = InputMesh.kVertexDeclaration;

		OutputMesh.m_uIndexByteOffset = InputMesh.uIndexByteOffset;
		OutputMesh.m_uIndexElementSize = InputMesh.uIndexElementSize;
		OutputMesh.m_kIndexBufferFormat = InputMesh.kIndexBufferFormat;
	}

	return MeshClusterOutput;
}
//---------------------------------------------------------------------------------------------------
bool H3DFile::LoadDirectlyFromStream(hlx::bytestream& _Stream, Display::TMeshCluster& _MeshClusterOutput, Display::BufferSubresourceData& _VertexData, Display::BufferSubresourceData& _IndexData)
{
	_MeshClusterOutput = GetMeshCluster();

	_VertexData.pData = _Stream.get_data(m_Header.uVertexDataOffset);
	_VertexData.uRowPitch = m_Header.uVertexDataSize;

	if (m_Header.uIndexDataSize > 0)
	{
		_IndexData.pData = _Stream.get_data(m_Header.uIndexDataOffset);
		_IndexData.uRowPitch = m_Header.uIndexDataSize;
	}

	return true;
}
//---------------------------------------------------------------------------------------------------

bool H3DFile::Load(hlx::bytestream& _Stream, Display::TMeshCluster& _MeshClusterOutput, Display::BufferSubresourceData& _VertexData, Display::BufferSubresourceData& _IndexData, const Display::TVertexDeclaration& _kTargetDeclaration, const Display::PixelFormat& _kTargetIndexFormat)
{
	_MeshClusterOutput = GetMeshCluster();

	m_VertexData.resize(0);
	hlx::bytestream VertexOut(m_VertexData);
	uint32_t uVertexByteOffset = 0;

	for (Display::SubMesh& Mesh : _MeshClusterOutput)
	{
		if (Mesh.m_kVertexDeclaration ^ _kTargetDeclaration)
		{
			HERROR("Mesh %s does not support target vertex layout", CSTR(Mesh.m_sSubName));
			return false;
		}

		// skip to vertex data of input mesh
		_Stream.set_offset(m_Header.uVertexDataOffset + Mesh.m_uVertexByteOffset);

		Mesh.m_uVertexByteOffset = uVertexByteOffset;
		Mesh.m_uVertexStrideSize = Display::GetVertexDeclarationSize(_kTargetDeclaration);
		uVertexByteOffset += Mesh.m_uVertexCount * Mesh.m_uVertexStrideSize;

		if (Mesh.m_kVertexDeclaration == _kTargetDeclaration)
		{
			// copy data at once
			VertexOut << _Stream.get<hlx::bytes>(Mesh.m_uVertexCount * Mesh.m_uVertexStrideSize);
		}
		else
		{
			for (uint32_t uVertex = 0; uVertex < Mesh.m_uVertexCount; ++uVertex)
			{
				// copy vertex data from input to output stream
				for (uint32_t i = 0; i < Display::VertexLayout_NumOf; ++i)
				{
					Display::VertexLayout Component = static_cast<Display::VertexLayout>(1 << i);

					if (Mesh.m_kVertexDeclaration & Component)
					{
						hlx::bytes&& Temp = _Stream.get<hlx::bytes>(Display::VertexComponent[i].uSize);

						if (_kTargetDeclaration & Component)
						{
							VertexOut << Temp;
						}
					}
				}
			}
		}

		Mesh.m_kVertexDeclaration = _kTargetDeclaration;
	}

	if (m_VertexData.size() == 0)
	{
		HERROR("Failed to convert vertex data");
		return false;
	}

	// fill out BufferSubresourceData
	_VertexData.pData = &m_VertexData[0];
	_VertexData.uRowPitch = uVertexByteOffset;

	m_IndexData.resize(0);
	hlx::bytestream IndexOut(m_IndexData);
	uint32_t uIndexByteOffset = 0;

	for (Display::SubMesh& Mesh : _MeshClusterOutput)
	{
		if (Mesh.m_uIndexCount > 0xffff && _kTargetIndexFormat == Display::PixelFormat_R16_UInt)
		{
			HERROR("Mesh %s does not support target index format", CSTR(Mesh.m_sSubName));
			return false;
		}

		// skip to index data of input mesh
		_Stream.set_offset(m_Header.uIndexDataOffset + Mesh.m_uIndexByteOffset);

		Mesh.m_uIndexByteOffset = uIndexByteOffset;
		uIndexByteOffset += Mesh.m_uIndexCount * (_kTargetIndexFormat == Display::PixelFormat_R16_UInt ? 2 : 4);

		if (_kTargetIndexFormat == Mesh.m_kIndexBufferFormat)
		{
			// copy data at once
			IndexOut << _Stream.get<hlx::bytes>(Mesh.m_uIndexElementSize * Mesh.m_uIndexCount);
		}
		else
		{
			// convert 16<->32 bit indices
			for (uint32_t uIndex = 0; uIndex < Mesh.m_uIndexCount; ++uIndex)
			{
				if (Mesh.m_kIndexBufferFormat == Display::PixelFormat_R16_UInt && _kTargetIndexFormat == Display::PixelFormat_R32_UInt)
				{
					IndexOut.put<uint32_t>(_Stream.get<uint16_t>());
				}
				else if (Mesh.m_kIndexBufferFormat == Display::PixelFormat_R32_UInt && _kTargetIndexFormat == Display::PixelFormat_R16_UInt)
				{
					IndexOut.put<uint16_t>(_Stream.get<uint32_t>());
				}
			}
		}

		Mesh.m_uIndexElementSize = _kTargetIndexFormat == Display::PixelFormat_R16_UInt ? 2 : 4;
		Mesh.m_kIndexBufferFormat = _kTargetIndexFormat;
	}

	if (m_IndexData.size() == 0)
	{
		HERROR("Faiiled to convert index data");
		return false;
	}

	// fill out BufferSubresourceData
	_IndexData.pData = &m_IndexData[0];
	_IndexData.uRowPitch = uIndexByteOffset;

	return true;
}
//---------------------------------------------------------------------------------------------------

bool H3DFile::Load(hlx::bytestream& _Stream, ConvexMeshCluster& _ConvexClusterOutput)
{
	_Stream.set_offset(m_Header.uPhysXDataOffset);

	if (_Stream.available() < m_Header.uPhysXDataSize)
	{
		HERROR("Stream does not hold enough physx data");
		return false;
	}

	for (const H3DHeader::SubMesh& InputMesh : m_MeshCluster)
	{
		if (InputMesh.uConvexPhysxMeshSize == 0u)
		{
			continue;
		}

		_Stream.set_offset(m_Header.uPhysXDataOffset + InputMesh.uConvexPhysxMeshOffset);

		ConvexMeshCluster::ConvexMesh CMesh;
		CMesh.Data = _Stream.get<hlx::bytes>(InputMesh.uConvexPhysxMeshSize);
		CMesh.uCRC32 = CRC32(CMesh.Data);

		_ConvexClusterOutput.Meshes.push_back(CMesh);
	}

	return true;
}
//---------------------------------------------------------------------------------------------------
bool H3DFile::Save(hlx::bytestream& _Stream)
{
	if (m_VertexData.size() != m_Header.uVertexDataSize)
	{
		HERROR("Invalid vertex data size");
		return false;
	}

	if (m_IndexData.size() != m_Header.uIndexDataSize)
	{
		HERROR("Invalid index data size");
		return false;
	}

	if (m_PhysxData.size() != m_Header.uPhysXDataSize)
	{
		HERROR("Invalid physx data size");
		return false;
	}

	m_Header.Write(_Stream);

	_Stream << m_VertexData;
	_Stream << m_IndexData;
	_Stream << m_PhysxData;

	for (const H3DHeader::SubMesh& Mesh : m_MeshCluster)
	{
		Mesh.Write(_Stream);
	}

	return true;
}
//---------------------------------------------------------------------------------------------------