//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef H3DHEADER_H
#define H3DHEADER_H

#include "Display\ViewDefines.h"
#include "hlx\src\Logger.h"
#include "hlx\src\ByteStream.h"

// Forward declarations:

namespace Helix
{
	namespace Resources
	{
		enum H3DVersion : uint32_t
		{
			H3DVersion_v1 = 0,
			H3DVersion_Unknown
		};

		Flag32E(H3DFlag)
		{
			H3DFlag_None = 0,
			H3DFlag_PhysxMesh = 1 << 0,
			H3DFlag_NumOf
		};

		using TH3DFlags = Flag<H3DFlag>;

		struct H3DHeader
		{
			struct FaceGroup
			{
				uint32_t uStartIndex = 0; // start indice or vertex (depends on submesh)
				uint32_t uCount = 0; // number of indices / vertices to draw with this material

				std::string sMaterial;

				inline uint32_t GetSizeOnDisk(H3DVersion _kVersion) const
				{
					uint32_t uSize = 0;

					switch (_kVersion)
					{
					case H3DVersion_v1:
					default:
						uSize = static_cast<uint32_t>(sMaterial.size()) + sizeof(char) + sizeof(uint32_t) * 2;
					}

					return uSize;
				}
			};

			struct SubMesh
			{
				bool Read(hlx::bytestream& _Stream);
				void Write(hlx::bytestream& _Stream) const;

				std::string sSubName;

				//Vertex Info
				uint32_t uVertexByteOffset = 0u; // offset from first byte in the buffer to the first vertex to be drawn in byte
				uint32_t uVertexStrideSize = 0u;
				uint32_t uVertexCount = 0u; // VertexCount * StrideSize = SubMeshSize
				Display::TVertexDeclaration kVertexDeclaration = Display::VertexLayout_Pos_XYZ; // verify with shader layout (permutation)
				Display::PrimitiveTopology kPrimitiveTopology = Display::PrimitiveTopology_Undefined;

				// Index Info
				uint32_t uIndexByteOffset = 0u;// offset from first byte in the buffer to the first index to be drawn in byte
				uint32_t uIndexCount = 0u; // IndexCount * PixelFormatSize = Total index size
				uint32_t uIndexElementSize = 0u; // 2 or 4 byte
				Display::PixelFormat kIndexBufferFormat = Display::PixelFormat_R32_UInt; // U16 or U32

				uint32_t uConvexPhysxMeshOffset = 0u;
				uint32_t uConvexPhysxMeshSize = 0u;

				const uint32_t uEndOfStruct = 0xFFFFFFFF;

				inline uint32_t GetSizeOnDisk(H3DVersion _kVersion) const
				{
					uint32_t uSize = 0;

					switch (_kVersion)
					{
					case H3DVersion_v1:
					default:
						uSize = static_cast<uint32_t>(sSubName.size()) + sizeof(char) + 
							(offsetof(SubMesh, uEndOfStruct) - offsetof(SubMesh, uVertexByteOffset));
					}

					return uSize;
				}
			};

			bool Read(hlx::bytestream& _Stream);
			void Write(hlx::bytestream& _Stream) const;

			static constexpr uint32_t H3DMagic = 0x666999;
			std::string sName;

			H3DVersion kVersion = H3DVersion_v1;
			uint32_t uVertexDataOffset = 0u;
			uint32_t uVertexDataSize = 0u; // total size of vertex data
			uint32_t uIndexDataOffset = 0u;
			uint32_t uIndexDataSize = 0u; // total size of index data
			uint32_t uPhysXDataOffset = 0u;
			uint32_t uPhysXDataSize = 0u; // total size of physx data
			uint32_t uClusterCount = 0u; // number of submeshes
			TH3DFlags kFlags;

			uint32_t uEndOfStruct = 0xFFFFFFFF;

			inline uint32_t GetSizeOnDisk(H3DVersion _kVersion) const
			{
				uint32_t uSize = 0;

				switch (_kVersion)
				{
				case H3DVersion_v1:
				default:
					uSize = static_cast<uint32_t>(sName.size()) + sizeof(char) + sizeof(uint32_t) // H3DMagic
						+ (offsetof(H3DHeader, uEndOfStruct) - offsetof(H3DHeader, kVersion));
				}
				
				return uSize;
			}
		};
	} // Resources
} // Helix

#endif // H3DHEADER_H