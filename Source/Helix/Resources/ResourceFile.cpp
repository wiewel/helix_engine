//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "ResourceFile.h"
#include "LZMA.h"
#include "Util\CRC32.h"
#include <sstream>

using namespace Helix::Resources;

ResourceFile::ResourceFile(hlx::string _packagePath, hlx::string _packageDirectory, bool _bCheckCRC)
{
	m_sPackageDir = _packageDirectory;
	m_sPackagePath = _packagePath;
	m_bCheckCRC = _bCheckCRC;
}
//---------------------------------------------------------------------------------------------------
ResourceFile::~ResourceFile()
{
	Close();

	m_PackageFileStreams.clear();
	m_Resources.clear();
}
//---------------------------------------------------------------------------------------------------
void ResourceFile::Close()
{
	if (m_FileStream.is_open())
	{
		m_FileStream.close();
	}

	for(uint32_t i = 0; i < m_PackageFileStreams.size(); ++i)
	{
		if (m_PackageFileStreams[i] != nullptr && m_PackageFileStreams[i]->is_open())
		{
			m_PackageFileStreams[i]->close();		
		}
	}
}
//---------------------------------------------------------------------------------------------------
const ResourceEntry* ResourceFile::GetEntry(uint32_t _uFileID)
{
	size_t uSize = m_Resources.size();

	if (_uFileID >= uSize)
	{
		HERROR("Invalid FileID %d, number of entries %d", _uFileID, uSize);
		return nullptr;
	}

	return &m_Resources[_uFileID];
}
//---------------------------------------------------------------------------------------------------
const ResourceEntry* ResourceFile::GetEntry(const std::string& _sFilePath, char _Separator)
{
	if (m_Resources.size() == 0)
	{
		return nullptr;
	}

	std::unordered_map<std::string, const ResourceEntry*>::iterator itr = m_ResourceMap.find(_sFilePath);

	if (itr != m_ResourceMap.end())
	{
		return itr->second;
	} 
	else 
	{
		uint32_t fileID = 1; //1 because the root entry does not have a next entry
		std::istringstream ss(_sFilePath);
		std::string curName;
		const ResourceEntry *entry = nullptr;

		while (std::getline(ss, curName, _Separator))
		{
			while (fileID != HR_INVALID)
			{
				entry = &m_Resources[fileID];
				if (curName.compare(entry->name) == 0)
				{
					if (entry->entryType == ResourceEntryFlag_Directory && ss.eof() == false)
					{
						fileID = entry->child;//only descend if theres still some path left
						break;//descend to next level if the last matching entry was a directory
					}

					if (ss.eof())
						break; // if theres no more path left, exit the inner loop (top level loop will exit if eof)	
				}

				fileID = m_Resources[fileID].next;
			}
		}

		if (fileID == HR_INVALID)
		{
			m_ResourceMap[_sFilePath] = nullptr;
			return nullptr;
		}
		else 
		{
			m_ResourceMap[_sFilePath] = entry;
			return entry;
		}		
	}	
}
//---------------------------------------------------------------------------------------------------
bool ResourceFile::Read()
{
	m_FileStream.open(m_sPackagePath, std::ios::in | std::ios::binary);

	if(m_FileStream.is_open() == false)
	{
		HERROR("Unable to open %s", CSTR(m_sPackagePath));
		return false;
	}

	if (m_IndexFileHeader.Read(m_FileStream) == false)
	{
		return false;
	}

	//Allocate space
	m_Resources.resize(m_IndexFileHeader.entryCount);
	m_PackageFileStreams.resize(m_IndexFileHeader.packageCount,nullptr);

	uint32_t i;
	ResourceEntry entry;

	//Read entries and save local files
	for(i = 0; i < m_IndexFileHeader.entryCount && m_FileStream.good(); ++i)
	{
		if (entry.Read(m_FileStream) == false || entry.fileID >= m_IndexFileHeader.entryCount)
		{
			return false;
		}

		m_Resources[entry.fileID] = entry;

		if(entry.entryType & ResourceEntryFlag_LocalHasPriority)
		{
			m_LocalFileIDs.push_back(entry.fileID);
		}

		entry.Reset();
	}

	//end of file has been reached (or some other reason .good() returned false), not all entries could be read
	if(i < m_IndexFileHeader.entryCount)
	{
		HERROR("Read %d entries of %d from index file %s",i, m_IndexFileHeader.entryCount, CSTR(m_IndexFileHeader.packageIdentifier));
		return false;
	} 

	HLOG("Successfully read %s.rif Version %d", CSTR(m_IndexFileHeader.packageIdentifier), m_IndexFileHeader.fileVersion);

	return true;
}
//---------------------------------------------------------------------------------------------------
std::string ResourceFile::GetPath(uint32_t fileID) const
{
	std::string path;

	if (fileID >= m_Resources.size() || fileID == 0 || fileID == HR_INVALID)
	{
		return path;
	}

	const ResourceEntry *entry = &m_Resources[fileID];
	path = entry->name;

	//construct fullpath
	while(entry->parent != 0 && entry->parent != HR_INVALID)
	{
		entry = &m_Resources[entry->parent];
		path = entry->name + "\\" + path;
	}

	return path;
}
//---------------------------------------------------------------------------------------------------
//only use local means to prefer extracted files, if they are not extracted, they will be extracted.
//this should be set to true when debugging a game without rebuilding the package
bool ResourceFile::GetResource(uint32_t _uFileID, hlx::bytes& _OutputData, bool _bOnlyUseLocalFiles)
{
	if (_uFileID >= m_Resources.size())
	{
		HERROR("Invalid FileID %d, number of entries %d", _uFileID, m_Resources.size());
		return false;
	}

	ResourceEntry *pEntry = &m_Resources[_uFileID];
	//entry is a directory
	if (pEntry->entryType == ResourceEntryFlag_Directory)
	{
		return false;
	}

	if(_bOnlyUseLocalFiles || pEntry->entryType & ResourceEntryFlag_LocalHasPriority)
	{
		//Load the data from the local package directory
		hlx::string path = m_sPackageDir + S("\\") + hlx::to_string(m_IndexFileHeader.packageIdentifier) + S("\\") + hlx::to_string(GetPath(_uFileID));
		hlx::fbytestream local_file(path, std::ios::in | std::ios::binary);

		if(local_file.is_open() == false)
		{
			HWARNING("Unable to load %s",path.c_str());
			//extract and unpack file if necessary
			return ExtractFile(*pEntry, &_OutputData);
		}
		else
		{
			//read the whole file, local files could be modified and bigger than entries suggest
			hlx::bytes Buffer = local_file.get<hlx::bytes>(local_file.size());

			local_file.close();
			
			return UnpackAndDecrypt(Buffer, _OutputData, *pEntry);
		}		
	}
	else
	{
		//Load the data from the resouce/package file
		return GetDataFromResourceFile(*pEntry, _OutputData);
	}

	return true;
}
//---------------------------------------------------------------------------------------------------
//Reads the raw data from the index / package file and unpacks / validates it
inline bool ResourceFile::GetDataFromResourceFile(const ResourceEntry& _Entry, hlx::bytes& _OutputData)
{	
	hlx::bytes Buffer;

	//Read data from preload
	if(_Entry.preloadSize > 0)
	{
		//read the resource from the index file
		if(_Entry.preloadSize + _Entry.preloadOffset <= m_FileStream.size())
		{
			m_FileStream.seekg(_Entry.preloadOffset,std::ios::beg);
			Buffer = m_FileStream.get<hlx::bytes>(_Entry.preloadSize);
		}else{
			HERROR("Resource %s offset %d is outside the file", CSTR(_Entry.name),_Entry.preloadOffset);
			return false;
		}
	}
	else
	{
		//read the file from a external package file
		if(_Entry.packageIndex >= m_PackageFileStreams.size())
		{
			HERROR("Package %d is not availiable",_Entry.packageIndex);
			return false;
		}

		//check if the package is opened, open it otherwise
		std::shared_ptr<hlx::fbytestream> package = m_PackageFileStreams[_Entry.packageIndex];
		//if the stream does not exist jet, create a new one
		if(package == nullptr)		{

			package = std::make_shared<hlx::fbytestream>();
			m_PackageFileStreams[_Entry.packageIndex] = package;
		}

		if(package->is_open() == false)
		{
			hlx::string packageFile = m_sPackageDir + S("\\") + hlx::to_string(m_IndexFileHeader.packageIdentifier) + S("_") + hlx::to_string(std::to_string(_Entry.packageIndex)) + S(".rpf");
			package->open(packageFile, std::ios::in | std::ios::binary);

			if(package->is_open() == false)
			{
				HERROR("Unable to load %s",packageFile.c_str());
				return false;
			}

			//Check if the Package file is valid
			ResourcePackageFileHeader packageHeader;

			if (packageHeader.Read(*package) == false)
			{
				return false;
			}			

			if(packageHeader.packageIdentifier.compare(m_IndexFileHeader.packageIdentifier) != 0)
			{
				HERROR("PackageIdentifier %s does not match %s",
					CSTR(packageHeader.packageIdentifier),
					CSTR(m_IndexFileHeader.packageIdentifier));
				return false;
			}

			if(packageHeader.fileVersion != m_IndexFileHeader.fileVersion)
			{
				HERROR("FileVersion %d does not match %d",packageHeader.fileVersion,m_IndexFileHeader.fileVersion);
				return false;
			}
		}

		//read the data from the package file
		if(_Entry.size + _Entry.offset <= package->size())
		{
			package->seekg(_Entry.offset, std::ios::beg);
			Buffer = package->get<hlx::bytes>(_Entry.size);
		}
		else
		{
			HERROR("Resource %s offset %d is outside the file", CSTR(_Entry.name),_Entry.offset);
			return false;
		}
	}
	
	return UnpackAndDecrypt(Buffer, _OutputData, _Entry);
}
//---------------------------------------------------------------------------------------------------
//call this function on the first startup of the game after installation
bool ResourceFile::ExtractLocalFiles()
{
	HLOG("Extracting %d local files",m_LocalFileIDs.size());

	uint32_t i = 0;
	for(; i < m_LocalFileIDs.size(); ++i)
	{
		if (ExtractFile(m_Resources[m_LocalFileIDs[i]]) == false)
		{
			return false;
		}
	}

	HLOG("Extracted %d of %d local files",i,m_LocalFileIDs.size());

	return false;
}

//---------------------------------------------------------------------------------------------------
bool ResourceFile::ExtractAllFiles()
{
	HLOG("Extracting %d resource files",m_Resources.size());

	uint32_t i = 1;
	for(; i < m_Resources.size(); ++i)
	{
		if(ExtractFile(m_Resources[i]) == false) return false;
	}

	HLOG("Extracted %d of %d resource files",i,m_Resources.size());

	return false;
}
//---------------------------------------------------------------------------------------------------
//extract a specific file
bool ResourceFile::ExtractFile(const ResourceEntry& _Entry, hlx::bytes* _pOutputData)
{
	//create extraction folder
	hlx::string extractionFolder = m_sPackageDir + S("\\") + hlx::to_string(m_IndexFileHeader.packageIdentifier) + S("\\");

	if(CreateDirectory(extractionFolder.c_str(),NULL) == 0 && GetLastError() != ERROR_ALREADY_EXISTS)
	{
		HERROR("Unable to create directory %s",extractionFolder.c_str());
		return false;
	}

	hlx::string path = STR(GetPath(_Entry.parent));

	HLOGD("Extracting %s...", CSTR(_Entry.name));

	//Create the parent directory
	if(_Entry.entryType == ResourceEntryFlag_Directory)
	{
		if(path.empty())
		{
			path = extractionFolder + hlx::to_string(_Entry.name) + S("\\");
		}
		else
		{
			path = extractionFolder + path + S("\\") + hlx::to_string(_Entry.name) + S("\\");
		}		
	}
	else
	{
		path = extractionFolder + path + S("\\");
	}
	
	if(CreateDirectory(path.c_str(),NULL) == 0 && GetLastError() != ERROR_ALREADY_EXISTS)
	{
		HWARNING("Unable to create directory %s", path.c_str());
		return false;
	}

	if (_Entry.entryType == ResourceEntryFlag_Directory)
	{
		return true;
	}

	hlx::bytes Buffer;
	if (GetDataFromResourceFile(_Entry, Buffer) == false)
	{
		return false;
	}

	if (_pOutputData != nullptr)
	{
		*_pOutputData = std::move(Buffer);
	}

	path += hlx::to_string(_Entry.name);

	hlx::fbytestream local_file(path, std::ios::out | std::ios::binary);
	if(local_file.is_open() == false)
	{
		HWARNING("Unable to open %s for writing",path.c_str());
		return false;
	}

	local_file.put<hlx::bytes>(Buffer);
	local_file.flush();
	local_file.close();

	return true;
}

//---------------------------------------------------------------------------------------------------
bool ResourceFile::UnpackAndDecrypt(hlx::bytes& _InputData, hlx::bytes& _OutputData, const ResourceEntry& _Entry)
{
	//Decrypt with AES128 
	if (_Entry.entryType & ResourceEntryFlag_Encrypted)
	{
		LZMA Aes;
		if (Aes.Decrypt(_InputData, m_DecryptionKey.m_pKey, m_DecryptionKey.m_pIV) == false)
		{
			HERROR("Failed to decrypt data");
			return false;
		}

		//remove added empty aes block (max 16 byte)
		if (_InputData.size() > _Entry.m_uDecryptedSize)
		{
			_InputData.resize(_Entry.m_uDecryptedSize);
		}

		_OutputData = _InputData;
	}

	//Uncompress with lzma
	if (_Entry.entryType & ResourceEntryFlag_Compressed)
	{
		if (LZMA::Uncompress(_InputData, _OutputData, _Entry.m_uUncompressedSize) == false)
		{
			HERROR("Failed to uncompress shader %s", CSTR(_Entry.name));
			return false;
		}
	}

	if (m_bCheckCRC)
	{
		uint32_t uChecksum = CRC32(_OutputData);
		if (uChecksum != _Entry.checksum)
		{
			HWARNING("%s checksum %X does not match %X", CSTR(_Entry.name), uChecksum, _Entry.checksum);
			return false;
		}
	}

	return true;
}
