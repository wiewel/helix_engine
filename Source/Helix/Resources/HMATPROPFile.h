//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef HMATPROPFILE_H
#define HMATPROPFILE_H
#include "Math\MathTypes.h"
#include "hlx\src\TextToken.h"
#include "hlx\src\StandardDefines.h"
#include "Util\BaseHash.h"

#include <vector>

namespace Helix
{
	namespace Resources
	{
		struct HTextureMapProp
		{
			bool bEnabled = TRUE;

			/// Additional Settings
			Math::float4 vScale = HFLOAT4_ONE;
			Math::float4 vOffset = HFLOAT4_ZERO;
		};

		struct HMatProps
		{
			std::string sHMatName;
			std::string sSubMeshName;

			float fMetallic = 0.0f; // currently either 0.0f (dielectric) or 1.0f (metallic)
			float fRoughness = 0.0f;
			float fHorizonFade = 0.0f;

			Math::float3 vColorAlbedo = HFLOAT3_ONE;
			Math::float3 vColorEmissive = HFLOAT3_ZERO;
			Math::float2 vTextureTiling = HFLOAT2_ONE;

			HTextureMapProp AlbedoMap;
			HTextureMapProp NormalMap;
			HTextureMapProp MetallicMap;
			HTextureMapProp RoughnessMap;
			HTextureMapProp EmissiveMap;
			HTextureMapProp HeightMap;

			bool bUseDefaultPasses = TRUE;
			std::vector<std::string> RenderPasses;

			inline uint64_t Hash() const
			{
				struct MatPropHash : public BaseHash
				{
					inline const uint64_t& operator()(const HTextureMapProp& _fu)
					{
						BaseHash::operator()(_fu.bEnabled);
						BaseHash::operator()(_fu.vOffset);
						return BaseHash::operator()(_fu.vScale);
					}
				};

				BaseHash Hasher;
				MatPropHash MapHasher;

				Hasher(sHMatName);
				Hasher(sSubMeshName);

				Hasher(fHorizonFade);
				Hasher(fMetallic);
				Hasher(fRoughness);
				Hasher(vColorAlbedo);
				Hasher(vColorEmissive);
				Hasher(vTextureTiling);
				Hasher(bUseDefaultPasses);

				if (bUseDefaultPasses == false)
				{
					for (const std::string sPass : RenderPasses)
					{
						Hasher(sPass);
					}
				}

				MapHasher(AlbedoMap);
				MapHasher(NormalMap);
				MapHasher(MetallicMap);
				MapHasher(RoughnessMap);
				MapHasher(EmissiveMap);
				MapHasher(HeightMap);

				return Hasher.Combine(MapHasher);
			}
		};

		class HMATPROPFile
		{
		public:
			HMATPROPFile(const HMatProps& _Props);
			HMATPROPFile(const hlx::string& _sFileName = {});
			HMATPROPFile(const HMATPROPFile& _PropFile);

			~HMATPROPFile();

			void Read(const hlx::string& _sFileName);
			void Read(hlx::bytestream& _Stream);
			void Read(const hlx::TextToken& _Properties);

			static HMatProps Convert(const hlx::TextToken& _Properties);

			void Write(hlx::bytestream& _Stream);
			hlx::TextToken Write();

			static hlx::TextToken Convert(const HMatProps& _Props);

			const HMatProps& GetProperties() const;
			HMatProps& GetProperties();

		private:
			HMatProps m_Properties;
		};

		inline const HMatProps& HMATPROPFile::GetProperties() const	{return m_Properties;}
		inline HMatProps& HMATPROPFile::GetProperties(){ return m_Properties; }
	} // Resources
} // Helix

#endif // !HMATPROPFILE_H
