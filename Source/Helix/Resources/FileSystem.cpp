//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "FileSystem.h"

#include "hlx\src\StringHelpers.h"
#include "hlx\src\Logger.h"

using namespace Helix::Resources;

//---------------------------------------------------------------------------------------------------
FileSystem::FileSystem()
{
}
//---------------------------------------------------------------------------------------------------
FileSystem::~FileSystem()
{
}
//---------------------------------------------------------------------------------------------------
bool FileSystem::Initialize(
	const hlx::string& _sBasePath,
	const hlx::string& _sProjectName,
	bool _bCreateDirs)
{
	m_sBasePath = _sBasePath;
	m_sProjectName = _sProjectName;

	if (m_sBasePath.empty())
	{
		TCHAR pBuffer[2048] = { 0 };
		HRESULT hRes = GetCurrentDirectory(2048, pBuffer);

		HR2(hRes);

		if (FAILED(hRes) == false)
		{
			m_sBasePath = hlx::string(pBuffer);
		}
	}

	while (hlx::ends_with(m_sBasePath, S("\\")))
	{
		m_sBasePath.pop_back();
	}

	HLOG("Initializing FileSystem '%s'...", m_sBasePath.c_str());

	bool bAllExists = true;
	hlx::string sCurPath;

	for (uint32_t i = 0u; i < HelixDirectories_NumOf; ++i)
	{
		const HelixDir& Dir = HelixDirectoryNames[i];

		sCurPath = m_sBasePath;
		
		if (i > 0u && Dir.bProjectDependant && m_sProjectName.empty() == false)
		{
			sCurPath += S("\\") + m_sProjectName;
		}
		
		sCurPath += hlx::string(Dir.pPath);

		if (PathExists(sCurPath) == false)
		{
			HWARNING("HelixDirectory '%s' not found!", Dir.pPath);
			if (_bCreateDirs)
			{
				CreateDirectory(sCurPath.c_str(), NULL);
			}
			else
			{
				bAllExists = false;
			}
		}
		else if(Dir.bIndexed) // don't add files from the base path
		{
			AddFilesFromDirectory(i, sCurPath, Dir.pExtension, Dir.bRecursiveIndexed);
		}

		// set directory anyway
		m_Directories[i] = sCurPath;
	}

	m_bInitialized = bAllExists;

	return bAllExists;
}
//---------------------------------------------------------------------------------------------------
bool FileSystem::AddMiscFilesFromDirectory(const hlx::string& _sAbsoluteDirectoryPath, const hlx::string& _sExtension, bool _bRecursive)
{
	if (PathExists(_sAbsoluteDirectoryPath) == false)
	{
		HWARNING("Directory '%s' not found!", _sAbsoluteDirectoryPath.c_str());
		return false;
	}

	uint32_t uFiles = 0;
	hlx::string sFileName;

	for (const hlx::string& sFilePath : GetFilesFromDirectory(_sAbsoluteDirectoryPath, _sExtension, _bRecursive))
	{
		sFileName = GetShortName(sFilePath);

		auto itr_pair = m_MiscFiles.insert({ sFileName , sFilePath });
		if (itr_pair.second == false)
		{
			HWARNING("Duplicate file '%s' (non unique naming)!", sFileName.c_str());
		}
		else
		{
			++uFiles;
		}
	}

	HLOG("Indexed '%s' with %u Files", _sAbsoluteDirectoryPath.c_str(), uFiles);

	return uFiles > 0;
}
//---------------------------------------------------------------------------------------------------
bool FileSystem::AddFilesFromDirectory(const uint32_t _uKnownDirIndex, const hlx::string& _sAbsoluteDirectoryPath, const hlx::string& _sExtension, bool _bRecursive)
{
	HASSERT(_uKnownDirIndex < HelixDirectories_NumOf, "Known directory index out of bounds!");

	if (PathExists(_sAbsoluteDirectoryPath) == false)
	{
		HWARNING("Directory '%s' not found!", _sAbsoluteDirectoryPath.c_str());
		return false;
	}

	uint32_t uFiles = 0;
	hlx::string sFileName;

	for (const hlx::string& sFilePath : GetFilesFromDirectory(_sAbsoluteDirectoryPath, _sExtension, _bRecursive))
	{
		sFileName = GetShortName(sFilePath);

		auto itr_pair = m_Files[_uKnownDirIndex].insert({ sFileName , sFilePath });
		if (itr_pair.second == false)
		{
			HWARNING("Duplicate file '%s' (non unique naming)!", sFileName.c_str());
		}
		else
		{
			++uFiles;
		}
	}

	HLOG("Indexed '%s' with %u Files", _sAbsoluteDirectoryPath.c_str(), uFiles);

	return uFiles > 0;
}
//---------------------------------------------------------------------------------------------------
bool FileSystem::AddFileToIndex(const HelixDirectories _kDirectory, const hlx::string& _sFilePath, bool _bSuppressDuplicateWarning)
{
	hlx::string sFileName(GetShortName(_sFilePath));

	auto itr_pair = m_Files[_kDirectory].insert({ sFileName , _sFilePath });
	if (itr_pair.second == false && _bSuppressDuplicateWarning == false)
	{
		HWARNING("Duplicate file '%s' (non unique naming)!", sFileName.c_str());
		return false;
	}
	return true;
}
//---------------------------------------------------------------------------------------------------
bool FileSystem::GetFullPath(HelixDirectories _kDirectory, const hlx::string& _sFileName, _Out_ hlx::string& _sFilePath)
{
	HASSERT(_kDirectory < HelixDirectories_NumOf, "Directory index out of bounds!");

	if (PathExists(_sFileName))
	{
		_sFilePath = _sFileName;
		return true;
	}

	std::unordered_map<hlx::string, hlx::string>::iterator itr = m_Files[_kDirectory].find(GetShortName(_sFileName));

	if (itr != m_Files[_kDirectory].end())
	{
		_sFilePath = itr->second;
		return true;
	}

	return false;
}
//---------------------------------------------------------------------------------------------------
bool FileSystem::GetFullPath(const hlx::string& _sFileName, _Out_ hlx::string& _sFilePath)
{
	if (PathExists(_sFileName))
	{
		_sFilePath = _sFileName;
		return true;
	}

	std::unordered_map<hlx::string, hlx::string>::iterator itr = m_MiscFiles.find(GetShortName(_sFileName));

	if (itr != m_MiscFiles.end())
	{
		_sFilePath = itr->second;
		return true;
	}

	return false;
}
//---------------------------------------------------------------------------------------------------
bool FileSystem::OpenFileStream(const hlx::string& _sFileName, const int _iOpenMode, hlx::fbytestream& _FileStream)
{
	bool bOpen = false;

	if (PathExists(_sFileName))
	{
		_FileStream.swap(hlx::fbytestream(_sFileName, _iOpenMode));
	}
	else
	{
		// search in common files
		std::unordered_map<hlx::string, hlx::string>::iterator itr = m_MiscFiles.find(GetShortName(_sFileName));

		if (itr != m_MiscFiles.end())
		{
			_FileStream.swap(hlx::fbytestream(itr->second, _iOpenMode));
		}
	}

	bOpen = _FileStream.is_open();

	if (bOpen)
	{
		HLOG("Opening file '%s'", _sFileName.c_str());
	}
	else
	{
		HWARNING("Failed to open file '%s'", _sFileName.c_str());
	}

	return bOpen;
}
//---------------------------------------------------------------------------------------------------
bool FileSystem::OpenFileStream(HelixDirectories _kDirectory, const hlx::string& _sFileName, const int _iOpenMode, hlx::fbytestream& _FileStream)
{
	HASSERT(_kDirectory < HelixDirectories_NumOf, "Directory index out of bounds!");

	bool bOpen = false;

	hlx::string sOutFileName = _sFileName;

	if (PathExists(_sFileName))
	{
		_FileStream.swap(hlx::fbytestream(_sFileName, _iOpenMode));
	}
	else
	{
		// search in common files
		std::unordered_map<hlx::string, hlx::string>::iterator itr = m_Files[_kDirectory].find(GetShortName(_sFileName));

		if (itr != m_Files[_kDirectory].end())
		{
			sOutFileName = itr->second;
			_FileStream.swap(hlx::fbytestream(itr->second, _iOpenMode));
		}
	}

	bOpen = _FileStream.is_open();

	if (bOpen)
	{
		HLOG("Opening file '%s'", sOutFileName.c_str());
	}
	else
	{
		HWARNING("Failed to open file '%s'", sOutFileName.c_str());
	}

	return bOpen;
}
//---------------------------------------------------------------------------------------------------
std::vector<hlx::string> FileSystem::GetFilesFromDirectory(const hlx::string& _sDirectoryName, const hlx::string& _sExtensions, bool _bRecursive)
{
	{	
		const std::vector<hlx::string>&& Extensions = hlx::split(_sExtensions, S('|'));

		// parse extensions
		if (Extensions.size() > 1)
		{
			std::vector<hlx::string> AllFiles;

			for (const hlx::string& sExtension : Extensions)
			{
				// use rvalue ref for perfect forwarding
				const std::vector<hlx::string>&& Files = GetFilesFromDirectory(_sDirectoryName, sExtension, _bRecursive);
				AllFiles.insert(AllFiles.end(), Files.begin(), Files.end());
			}

			return AllFiles;
		}
	}	

	hlx::string DirName = _sDirectoryName;

	bool bHasSeparator = false;

	if (hlx::ends_with(_sDirectoryName, S("\\")))
	{
		DirName += S('*');
		bHasSeparator = true;
	}
	else
	{
		DirName += S("\\*");
		bHasSeparator = false;
	}

	// add file extension to search for
	DirName += _sExtensions;

	WIN32_FIND_DATA ffd;
	HANDLE hFind = INVALID_HANDLE_VALUE;

	std::vector<hlx::string> FileNames;

	hlx::string FileName;
	hlx::string FilePath;

	hFind = FindFirstFile(DirName.c_str(), &ffd);

	while (hFind != INVALID_HANDLE_VALUE)
	{
		FileName = hlx::string(ffd.cFileName);;
		FilePath = _sDirectoryName + (bHasSeparator ? FileName : (S("\\") + FileName));

		if (ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
		{
			//File is a directory
			if (_bRecursive && hlx::starts_with(FileName, S(".")) == false && FileName.compare(S("..")) != 0)
			{				
				const std::vector<hlx::string>&& SubDirFiles = GetFilesFromDirectory(FilePath, _sExtensions, _bRecursive);
				FileNames.insert(FileNames.end(), SubDirFiles.begin(), SubDirFiles.end());
			}
		}
		else
		{
			FileNames.push_back(FilePath);
		}

		if (FindNextFile(hFind, &ffd) == 0)
		{
			break;
		}
	}

	FindClose(hFind);

	return FileNames;
}
//---------------------------------------------------------------------------------------------------
bool FileSystem::PathExists(const hlx::string& _sFilePath)
{
	DWORD dwAttrib = GetFileAttributes(_sFilePath.c_str());

	return (dwAttrib != INVALID_FILE_ATTRIBUTES /*&& !(dwAttrib & FILE_ATTRIBUTE_DIRECTORY)*/);
}
//---------------------------------------------------------------------------------------------------
bool FileSystem::PathExists(const hlx::string& _sFilePath, uint32_t& _uAttributes)
{
	_uAttributes = GetFileAttributes(_sFilePath.c_str());
	return (_uAttributes != INVALID_FILE_ATTRIBUTES);
}
//---------------------------------------------------------------------------------------------------
bool FileSystem::CreateDirectoryRecursive(const hlx::string& _sDirectory)
{
	std::vector<hlx::string> Dirs = hlx::split(_sDirectory, S('\\'));

	hlx::string sDir;
	for (std::vector<hlx::string>::iterator itr = Dirs.begin(); itr != Dirs.end(); ++itr)
	{
		sDir += *itr + S('\\');
		if (CreateDirectory(sDir.c_str(), NULL) != 0 && GetLastError() != ERROR_ALREADY_EXISTS)
		{
			return false;
		}
	}

	return true;
}
//---------------------------------------------------------------------------------------------------
hlx::string FileSystem::GetFileNameWithoutExtension(const hlx::string& _sFilePath)
{
	hlx::string&& sFileName = GetFileName(_sFilePath);

	return sFileName.substr(0, sFileName.rfind(S('.')));
}
//---------------------------------------------------------------------------------------------------
hlx::string FileSystem::GetFileName(const hlx::string& _sFilePath)
{
	hlx::string sFileName = hlx::get_right_of(_sFilePath, S("\\"));

	if (hlx::contains(sFileName, S("/")))
	{
		sFileName = hlx::get_right_of(_sFilePath, S("/"));
	}

	if (sFileName.empty())
	{
		return _sFilePath;
	}

	return sFileName;
}
//---------------------------------------------------------------------------------------------------
hlx::string FileSystem::GetDirectoryFromPath(const hlx::string& _sFilePath)
{
	hlx::string sDirectory = hlx::get_left_of(_sFilePath, S("\\"), true, true);

	if (hlx::contains(sDirectory, S("/")))
	{
		sDirectory = hlx::get_left_of(_sFilePath, S("/"), true, true);
	}

	// filename only has been passed, assume relative dir
	if (sDirectory == _sFilePath)
	{
		sDirectory.clear(); // "\\";
	}

	return sDirectory;
}
//---------------------------------------------------------------------------------------------------
hlx::string FileSystem::GetShortName(const hlx::string& _sFileNameOrPath)
{
	return hlx::to_lower(FileSystem::GetFileNameWithoutExtension(_sFileNameOrPath));
}
#ifdef UNICODE
//---------------------------------------------------------------------------------------------------
std::string FileSystem::GetFileNameWithoutExtension(const std::string& _sFilePath)
{
	return hlx::to_sstring(GetFileNameWithoutExtension(hlx::to_string(_sFilePath)));
}
//---------------------------------------------------------------------------------------------------
std::string FileSystem::GetShortName(const std::string& _sFileNameOrPath)
{
	return hlx::to_lower(FileSystem::GetFileNameWithoutExtension(_sFileNameOrPath));
}
#endif
//---------------------------------------------------------------------------------------------------
