//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "InputLayoutSerializer.h"
#include "Display\DX11\ViewDefinesDX11.h"

using namespace Helix::Display;
using namespace Helix::Resources;

//---------------------------------------------------------------------------------------------------
InputLayoutSerializer::InputLayoutSerializer()
{
}
//---------------------------------------------------------------------------------------------------
InputLayoutSerializer::~InputLayoutSerializer()
{
}
//---------------------------------------------------------------------------------------------------
void InputLayoutSerializer::Clear()
{
	m_InputElements.resize(0);
	m_uElementCount = 0;
}
//---------------------------------------------------------------------------------------------------
void InputLayoutSerializer::Write(hlx::bytestream& _Stream)
{
	m_uElementCount = static_cast<uint32_t>(m_InputElements.size());
	_Stream << m_uElementCount;

	for (const InputElementDesc& InputElement : m_InputElements)
	{
		_Stream << InputElement.sSemanticName;
		_Stream << InputElement.sVariableName;
		_Stream << InputElement.uSemanticIndex;
		_Stream << InputElement.uRegister;
		_Stream << InputElement.kSystemValueType;
		_Stream << InputElement.kShaderComponentType;
		_Stream << InputElement.uMask;
		_Stream << InputElement.uReadWriteMask;
		_Stream << InputElement.uStream;
		_Stream << InputElement.kMinPrecision;
	}
}
//---------------------------------------------------------------------------------------------------
void InputLayoutSerializer::AddInputElement(const InputElementDesc& _InputElement)
{
	m_InputElements.push_back(_InputElement);
}
//---------------------------------------------------------------------------------------------------
