//   Copyright 2020 Fabian Wahlster, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef RIFFFILE_H
#define RIFFFILE_H

#include "hlx\src\StandardDefines.h"
#include "hlx\src\FileStream.h"
#include "hlx\src\Bytes.h"
#include <vector>
#include "Util\Functional.h"
#include "hlx\src\Logger.h"

namespace Helix
{
	namespace Resources
	{
		//==================================================================================================================================
		// A riff file consists of arbitrary numbers of chunks and these can be recursive.
		class RIFFChunk
		{
		public:
			enum class FourCC : uint32_t
			{
				NONE = 0u,
				RIFF = 'FFIR',
				data = 'atad',
				fmt = ' tmf',
				WAVE = 'EVAW',
				XWMA = 'AMWX',
				dpds = 'sdpd',
				LIST = 'TSIL',
			};

		protected:
			RIFFChunk();

		public:
			RIFFChunk(FourCC _ID, uint32_t _uDataSize);
			~RIFFChunk();

			virtual bool Read(hlx::fbytestream& _Stream);

			const FourCC& GetType() const;
			hlx::bytes GetData() const;
		
		protected:
			FourCC m_Type = FourCC::NONE;
			uint32_t m_uDataSize = 0u;
			hlx::bytes m_Data;
		};
		//==================================================================================================================================
		class RIFFFile : public RIFFChunk
		{
		public:
			RIFFFile();

			virtual bool Read(hlx::fbytestream& _Stream) final;
			const FourCC& GetFileType() const;
			hlx::bytes Find(FourCC _Type);

		protected:
			std::vector<RIFFChunk> m_Children;
			FourCC m_FileType = FourCC::NONE;
		};
	}
}

#endif // !RIFFFILE_H