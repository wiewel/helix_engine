//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef RENDERPASSDESCRIPTION_H
#define RENDERPASSDESCRIPTION_H

#include "hlx\src\TextToken.h"
#include "Display\ViewDefines.h"

namespace Helix
{
	namespace Resources
	{
		struct InputMapping
		{
			std::string sName;
			std::string sMappedName;
			std::string sMappedCopyName; // RenderTarget only
		};

		struct RenderTargetTextureDesc
		{
			std::string sName; // Shader output name used in ShaderRenderTargetDX11
			uint32_t uIndex; // 0..7 rentertarget slot

			Display::TextureResizeProperties ResizeProps;
			Display::RenderTargetBlendDesc BlendDesc;

			Math::float4 vClearColor;

			bool bBackBuffer = false;
			bool bShaderResource = true;
			bool bRWTexture = false;
			Display::PixelFormat kFormat = Display::PixelFormat_B8G8R8A8_UNorm;
			uint32_t uWidth = 0;
			uint32_t uHeight = 0;
		};

		struct DepthStencilTextureDesc
		{
			std::string sMappedName;

			Display::TextureResizeProperties ResizeProps;

			uint32_t uWidth = 0u;
			uint32_t uHeight = 0u;
			uint32_t uStencilRef = 0u;
			bool bShaderResource = true;
			Display::PixelFormat kFormat = Display::PixelFormat_R32_Float;

			float fClearDepth = 1.f;
			uint8_t uClearStencil = 0u;
		};

		struct SamplerDescEx 
		{
			std::string sName;
			Display::SamplerDesc Description = {};
		};

		// blend desc without rendertargets
		struct BlendDescReduced
		{
			bool bAlphaToCoverageEnable = false;
			bool bIndependentBlendEnable = false;

			// not in dx blend desc
			std::array<float, 4> fBlendFactor = { 0.0f,0.0f,0.0f,0.0f };
			uint32_t uSampleMask = 0xffffffff;
		};

		class RenderPassDesc
		{
		public:
			RenderPassDesc() {};
			RenderPassDesc(const hlx::TextToken& _InputToken);

			hlx::TextToken Serialize() const;

			static Display::DepthStencilDesc ParseDepthStencilDesc(const hlx::TextToken& _InputToken);
			static hlx::TextToken SerializeDepthStencilDesc(const Display::DepthStencilDesc& _Desc);
			
			static BlendDescReduced ParseBlendDesc(const hlx::TextToken& _InputToken);
			static hlx::TextToken SerializeBlendDesc(const BlendDescReduced& _Desc);

			static Display::RasterizerDesc ParseRasterizerDesc(const hlx::TextToken& _InputToken);
			static hlx::TextToken SerializeRasterizerDesc(const Display::RasterizerDesc& _Desc);

			static Display::SamplerDesc ParseSamplerDesc(const hlx::TextToken& _InputToken);
			static hlx::TextToken SerializeSamplerDesc(const Display::SamplerDesc& _Desc);

			static DepthStencilTextureDesc ParseDepthStencilTextureDesc(const hlx::TextToken& _InputToken);
			static hlx::TextToken SerializeDepthStencilTextureDesc(const DepthStencilTextureDesc& _Desc);

			static RenderTargetTextureDesc ParseRenderTargetTextureDesc(const hlx::TextToken& _InputToken);
			static hlx::TextToken SerializeRenderTargetTextureDesc(const RenderTargetTextureDesc& _Desc);

			std::string sShaderName;
			std::string sPassInstanceName;
			std::string sPassType;
			bool bEnabled;
			bool bClearRenderTargets;
			bool bClearDepthStencil; 

			std::vector<RenderTargetTextureDesc> RenderTargets;
			std::vector<InputMapping> InputMappings;
			
			std::vector<SamplerDescEx> Samplers;

			Display::RasterizerDesc RasterizerDescription;
			BlendDescReduced BlendDescription;
			Display::DepthStencilDesc DepthStencilDescription;
			DepthStencilTextureDesc DepthStencilTextureDescription;

			hlx::TextToken CustomVariables;
		};



	} // Resources
} // Helix

#endif // !RENDERPASSDESCRIPTION_H