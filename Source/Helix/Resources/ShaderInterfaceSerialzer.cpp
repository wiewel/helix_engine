//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "ShaderInterfaceSerialzer.h"

using namespace Helix::Resources;
using namespace Helix::Display;

// Stream helpers:
//---------------------------------------------------------------------------------------------------
// write
inline void write_bindinfo(hlx::bytestream& _Stream, const BindInfoDesc& _Desc)
{
	_Stream << _Desc.sName.GetString();
	_Stream << _Desc.uSlot;
	_Stream << _Desc.uSize;
	_Stream << _Desc.uCount;
	//return _Stream;
}

//---------------------------------------------------------------------------------------------------

ShaderInterfaceSerializer::ShaderInterfaceSerializer()
{
}
//---------------------------------------------------------------------------------------------------

ShaderInterfaceSerializer::~ShaderInterfaceSerializer()
{
}

//---------------------------------------------------------------------------------------------------

void ShaderInterfaceSerializer::Write(hlx::bytestream& _Stream)
{
	m_uSamplerCount = static_cast<uint32_t>(m_ShaderInterface.Samplers.size());
	m_uTextureCount = static_cast<uint32_t>(m_ShaderInterface.Textures.size());
	m_uBufferCount = static_cast<uint32_t>(m_ShaderInterface.Buffers.size());

	_Stream << m_uSamplerCount;
	_Stream << m_uTextureCount;
	_Stream << m_uBufferCount;

	for (const ShaderSamplerDesc& Sampler : m_ShaderInterface.Samplers)
	{
		write_bindinfo(_Stream, Sampler.BindInfo);
	}

	for (const ShaderTextureDesc& Texture : m_ShaderInterface.Textures)
	{
		write_bindinfo(_Stream, Texture.BindInfo);

		_Stream << Texture.kDimension;
		_Stream << Texture.uNumOfSamples;
		_Stream << Texture.kReturnType;
		_Stream << Texture.bRWTexture;
	}

	for (const ShaderBufferDesc& Buffer : m_ShaderInterface.Buffers)
	{
		write_bindinfo(_Stream, Buffer.BindInfo);
		
		_Stream << Buffer.kType;
		//HLOG("============================");
		//HLOG("%s Slot %u Size %u (%u):", CBuffer.BindInfo.sName.c_str(), CBuffer.BindInfo.uSlot, CBuffer.BindInfo.uSize, CBuffer.BindInfo.uSize % 16);

		// write variable count
		_Stream << static_cast<uint32_t>(Buffer.Variables.size());

		for (const ShaderVariableDesc& Variable : Buffer.Variables)
		{
			WriteShaderMemberVariables(_Stream, Variable);
		}
	}
}
//---------------------------------------------------------------------------------------------------

void ShaderInterfaceSerializer::AddBuffer(const ShaderBufferDesc& _CBuffer)
{
	m_ShaderInterface.Buffers.push_back(_CBuffer);
}
//---------------------------------------------------------------------------------------------------

void ShaderInterfaceSerializer::AddTexture(const ShaderTextureDesc& _Texture)
{
	m_ShaderInterface.Textures.push_back(_Texture);
}
//---------------------------------------------------------------------------------------------------

void ShaderInterfaceSerializer::AddSampler(const ShaderSamplerDesc& _Sampler)
{
	m_ShaderInterface.Samplers.push_back(_Sampler);
}
//---------------------------------------------------------------------------------------------------
void ShaderInterfaceSerializer::WriteShaderVariable(hlx::bytestream & _Stream, const ShaderVariableDesc & _Variable)
{
	_Stream << _Variable.sName;
	_Stream << _Variable.uStartOffset;
	_Stream << _Variable.uSize;
	_Stream << _Variable.uFlags;

	//HLOG("%s Offset %u Size %u", _Variable.sName.c_str(), _Variable.uStructureOffset, _Variable.uSize);

	_Stream << _Variable.uRows;
	_Stream << _Variable.uColumns;
	_Stream << _Variable.uElements;
	_Stream << _Variable.uMembers;
	_Stream << _Variable.uStructureOffset;

	_Stream << _Variable.kClass;
	_Stream << _Variable.kType;

	HASSERTD(_Variable.uMembers == _Variable.Children.size(), "Invalid number of member variables!");
	_Stream << _Variable.uMembers;	
}
//---------------------------------------------------------------------------------------------------
void ShaderInterfaceSerializer::WriteShaderMemberVariables(hlx::bytestream& _Stream, const ShaderVariableDesc & _Variable)
{
	WriteShaderVariable(_Stream, _Variable);

	for (const ShaderVariableDesc& Member : _Variable.Children)
	{
		WriteShaderMemberVariables(_Stream, Member);
	}
}
//---------------------------------------------------------------------------------------------------
