//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "HLXSLFile.h"
#include "ShaderCompilationResult.h"
#include <map>

using namespace Helix::Resources;

//---------------------------------------------------------------------------------------------------
HLXSLFile::HLXSLFile()
{
}
//---------------------------------------------------------------------------------------------------
HLXSLFile::~HLXSLFile()
{
}

//---------------------------------------------------------------------------------------------------
inline uint32_t GetIndex(std::map<uint32_t, uint32_t>& _Map, std::vector<std::pair<uint32_t, uint32_t>>& _Order, uint32_t& _uCurIndex, const uint32_t& _uHash)
{
	if (_uHash == 0)
	{
		return HUNDEFINED;
	}

	std::map<uint32_t, uint32_t>::iterator it = _Map.find(_uHash);
	if (it == _Map.end())
	{
		_Map.insert({ _uHash,_uCurIndex });
		_Order.push_back({ _uHash,_uCurIndex });

		uint32_t uRet = _uCurIndex++;
		return uRet;
	}
	else
	{
		return it->second;
	}
}
//---------------------------------------------------------------------------------------------------
bool HLXSLFile::Read(hlx::bytestream& _Stream)
{
	//---------------------------------------------------------------------------------------------------
	// read hlxsl
	//---------------------------------------------------------------------------------------------------

	uint32_t uMagic = 0;
	_Stream >> uMagic;

	if (uMagic != HLXSLMagic)
	{
		HERROR("Unknown HLXSL identifier %u", uMagic);
		return false;
	}

	// read header:	
	_Stream >> m_kVersion;
	if (m_kVersion >= HLXSLVersion_Unknown)
	{
		HERROR("Unknown HLXSL version %u", m_kVersion);
		return false;
	}

	_Stream >> m_kCodeCompressionMethod;
	if (m_kCodeCompressionMethod >= CodeCompression_Unknown)
	{
		HERROR("Unknown CodeCompressionMethod %u", m_kCodeCompressionMethod);
		return false;
	}

	_Stream >> m_uFunctionCount;
	_Stream >> m_uPermutationCount;
	_Stream >> m_uPermutationMaskCount; // might depend on version
	_Stream >> m_uCodeCount;
	_Stream >> m_uInterfaceCount;
	_Stream >> m_uLayoutCount;
	_Stream >> m_sShaderName;

	if (m_uPermutationMaskCount != m_uPermutationsMasks.size())
	{
		HERROR("Invalid PermutationMaskCount %u", m_uPermutationMaskCount);
		return false;
	}

	// write all shader masks
	for (uint32_t i = 0; i < m_uPermutationMaskCount; ++i)
	{
		_Stream >> m_uPermutationsMasks[i];
	}

	// write shader functions
	m_Functions.resize(m_uFunctionCount);
	for (Display::ShaderFunction& Function : m_Functions)
	{
		_Stream >> Function.kTargetVersion;
		_Stream >> Function.sFunctionName;
	}

	std::vector<uint32_t> CodeSizes(m_uCodeCount);
	std::vector<uint32_t> InterfaceSizes(m_uInterfaceCount);
	std::vector<uint32_t> LayoutSizes(m_uLayoutCount);

	//---------------------------------------------------------------------------------------------------
	// read sizes & validate file size

	// read code sizes
	for (uint32_t& uSize : CodeSizes)
	{
		_Stream >> uSize;
	}
	
	// read interface sizes
	for (uint32_t& uSize : InterfaceSizes)
	{
		_Stream >> uSize;
	}

	// read layout sizes
	for (uint32_t& uSize : LayoutSizes)
	{
		_Stream >> uSize;
	}

	//---------------------------------------------------------------------------------------------------
	// read permutations

	m_PermutationIndices.resize(m_uPermutationCount);
	for (HLXSLPermutationIndex& Permutation : m_PermutationIndices)
	{
		_Stream >> Permutation.Hash.uBasePermutation;
		uint32_t uNumOfRanges = 0u;
		_Stream >> uNumOfRanges;

		Permutation.Hash.Ranges.resize(uNumOfRanges);
		for (uint8_t& uRange : Permutation.Hash.Ranges)
		{
			_Stream >> uRange;
		}

		_Stream >> Permutation.uFunctionIndex;

		_Stream >> Permutation.uCodeHash;
		_Stream >> Permutation.uUncompressedCodeSize;
		_Stream >> Permutation.uCodeIndex;

		_Stream >> Permutation.uInterfaceHash;
		_Stream >> Permutation.uInterfaceIndex;

		_Stream >> Permutation.uInputLayoutHash;
		_Stream >> Permutation.uInputLayoutIndex;

		_Stream >> Permutation.uOutputLayoutHash;
		_Stream >> Permutation.uOutputLayoutIndex;
	}

	HLOGD("CodeOffset %u", _Stream.get_offset());

	//---------------------------------------------------------------------------------------------------
	// read code, interface and layout buffers

	m_CodeBuffer.resize(0);
	for (uint32_t i = 0; i < m_uCodeCount; ++i)
	{
		m_CodeBuffer.push_back(std::move(_Stream.get<hlx::bytes>(CodeSizes[i])));
	}

	HLOGD("InterfaceOffset %u", _Stream.get_offset());

	m_InterfaceBuffer.resize(0);
	for (uint32_t i = 0; i < m_uInterfaceCount; ++i)
	{
		m_InterfaceBuffer.push_back(std::move(_Stream.get<hlx::bytes>(InterfaceSizes[i])));
	}

	HLOGD("LayoutOffset %u", _Stream.get_offset());

	m_LayoutBuffer.resize(0);
	for (uint32_t i = 0; i < m_uLayoutCount; ++i)
	{
		m_LayoutBuffer.push_back(std::move(_Stream.get<hlx::bytes>(LayoutSizes[i])));
	}

	return true;
}
//---------------------------------------------------------------------------------------------------

bool HLXSLFile::Write(hlx::bytestream& _Stream, const ShaderCompilationResult& _CompiledShaders, const HLXSLVersion _kVersion)
{
	//---------------------------------------------------------------------------------------------------
	// assemble and reoder all needed information
	//---------------------------------------------------------------------------------------------------

	// maps hash -> index (occurence in stream)
	uint32_t uCodeIndex = 0;
	std::map<uint32_t, uint32_t> CodeIndexMap;
	std::vector<std::pair<uint32_t, uint32_t>> CodeOrder;

	uint32_t uInterfaceIndex = 0;
	std::map<uint32_t, uint32_t> InderfaceIndexMap;
	std::vector<std::pair<uint32_t, uint32_t>> InterfaceOrder;

	uint32_t uLayoutIndex = 0;
	std::map<uint32_t, uint32_t> LayoutIndexMap;
	std::vector<std::pair<uint32_t, uint32_t>> LayoutOrder;

	std::vector<HLXSLPermutationIndex> OutputIndices;

	uint32_t uFunctionIndex = 0;
	// write down permutations for each sahder function
	for (const Display::ShaderFunction& Function : _CompiledShaders.m_Functions)
	{
		std::pair<TPermutationMap::const_iterator, TPermutationMap::const_iterator> Range = _CompiledShaders.m_CompiledPermutations.equal_range(Function.GetHash());

		for (TPermutationMap::const_iterator it = Range.first; it != Range.second; ++it)
		{
			const HLXSLPermutation& Perm = it->second;
			HLXSLPermutationIndex Index = {};

			Index.Hash = Perm.Hash;
			Index.uFunctionIndex = uFunctionIndex;

			// transform hash to index
			Index.uCodeHash = Perm.uCodeHash;
			Index.uUncompressedCodeSize = Perm.uUncompressedCodeSize;
			Index.uCodeIndex = GetIndex(CodeIndexMap, CodeOrder, uCodeIndex, Perm.uCodeHash);

			Index.uInterfaceHash = Perm.uInterfaceHash;
			Index.uInterfaceIndex = GetIndex(InderfaceIndexMap, InterfaceOrder, uInterfaceIndex, Perm.uInterfaceHash);

			Index.uInputLayoutHash = Perm.uInputLayoutHash;
			Index.uInputLayoutIndex = GetIndex(LayoutIndexMap, LayoutOrder, uLayoutIndex, Perm.uInputLayoutHash);

			Index.uOutputLayoutHash = Perm.uOutputLayoutHash;
			Index.uOutputLayoutIndex = GetIndex(LayoutIndexMap, LayoutOrder, uLayoutIndex, Perm.uOutputLayoutHash);

			OutputIndices.push_back(Index);
		}

		++uFunctionIndex;
	}

	//---------------------------------------------------------------------------------------------------
	// write hlxsl
	//---------------------------------------------------------------------------------------------------

	// write header:
	m_kVersion = _kVersion;
	m_kCodeCompressionMethod = _CompiledShaders.GetCompressionMethod();
	m_uFunctionCount = static_cast<uint32_t>(_CompiledShaders.m_Functions.size());
	m_uPermutationCount = static_cast<uint32_t>(OutputIndices.size());
	m_uPermutationMaskCount = static_cast<uint32_t>(_CompiledShaders.m_uPermutationsMasks.size());
	m_uCodeCount = static_cast<uint32_t>(_CompiledShaders.m_CodeBuffer.size());
	m_uInterfaceCount = static_cast<uint32_t>(_CompiledShaders.m_InterfaceBuffer.size());
	m_uLayoutCount = static_cast<uint32_t>(_CompiledShaders.m_InputOutputLayoutBuffer.size());
	m_sShaderName = _CompiledShaders.m_sShaderName;

	_Stream << HLXSLMagic;
	_Stream << m_kVersion;
	_Stream << m_kCodeCompressionMethod;
	_Stream << m_uFunctionCount;
	_Stream << m_uPermutationCount;
	_Stream << m_uPermutationMaskCount; // might depend on version
	_Stream << m_uCodeCount;
	_Stream << m_uInterfaceCount;
	_Stream << m_uLayoutCount;
	_Stream << m_sShaderName;

	// write all shader masks
	for (uint32_t i = 0; i < m_uPermutationMaskCount; ++i)
	{
		m_uPermutationsMasks[i] = _CompiledShaders.m_uPermutationsMasks[i];
		_Stream << m_uPermutationsMasks[i];
	}

	// write shader functions
	for (const Display::ShaderFunction& Function : _CompiledShaders.m_Functions)
	{
		_Stream << Function.kTargetVersion;
		_Stream << Function.sFunctionName;
	}

	//---------------------------------------------------------------------------------------------------
	// write sizees

	// write code sizes
	for (const std::pair<uint32_t, uint32_t>& Index : CodeOrder)
	{
		_Stream << static_cast<uint32_t>(_CompiledShaders.m_CodeBuffer.at(Index.first).size());
	}

	// write interface sizes
	for (const std::pair<uint32_t, uint32_t>& Index : InterfaceOrder)
	{
		_Stream << static_cast<uint32_t>(_CompiledShaders.m_InterfaceBuffer.at(Index.first).size());
	}

	// write layout sizes
	for (const std::pair<uint32_t, uint32_t>& Index : LayoutOrder)
	{
		_Stream << static_cast<uint32_t>(_CompiledShaders.m_InputOutputLayoutBuffer.at(Index.first).size());
	}

	//---------------------------------------------------------------------------------------------------
	// write permutations

	for (const HLXSLPermutationIndex& Permutation : OutputIndices)
	{
		_Stream << Permutation.Hash.uBasePermutation;
		_Stream << static_cast<uint32_t>(Permutation.Hash.Ranges.size());

		for (const uint8_t& uRange : Permutation.Hash.Ranges)
		{
			_Stream << uRange;
		}

		_Stream << Permutation.uFunctionIndex;

		_Stream << Permutation.uCodeHash;
		_Stream << Permutation.uUncompressedCodeSize;
		_Stream << Permutation.uCodeIndex;

		_Stream << Permutation.uInterfaceHash;
		_Stream << Permutation.uInterfaceIndex;

		_Stream << Permutation.uInputLayoutHash;
		_Stream << Permutation.uInputLayoutIndex;

		_Stream << Permutation.uOutputLayoutHash;
		_Stream << Permutation.uOutputLayoutIndex;
	}

	HLOGD("CodeOffset %u", _Stream.get_offset());
	
	//---------------------------------------------------------------------------------------------------
	// write buffers

	// write code buffer
	for (const std::pair<uint32_t, uint32_t>& Index : CodeOrder)
	{
		_Stream << _CompiledShaders.m_CodeBuffer.at(Index.first);
	}

	HLOGD("InterfaceOffset %u", _Stream.get_offset());

	// write interface buffer
	for (const std::pair<uint32_t, uint32_t>& Index : InterfaceOrder)
	{
		_Stream << _CompiledShaders.m_InterfaceBuffer.at(Index.first);
	}

	HLOGD("LayoutOffset %u", _Stream.get_offset());
	// write layout buffer
	for (const std::pair<uint32_t, uint32_t>& Index : LayoutOrder)
	{
		_Stream << _CompiledShaders.m_InputOutputLayoutBuffer.at(Index.first);
	}
	
	return true;
}
//---------------------------------------------------------------------------------------------------
const PermutationBufferRef HLXSLFile::GetPermuation(const uint32_t& _uPermutationIndex) const
{
	if (_uPermutationIndex >= m_PermutationIndices.size())
	{
		HERROR("Invalid permutation index %u", _uPermutationIndex);
		return PermutationBufferRef(nullptr, nullptr, nullptr, nullptr, nullptr, nullptr);
	}
	
	const HLXSLPermutationIndex& Index = m_PermutationIndices[_uPermutationIndex];

	const hlx::bytes* pCode = Index.uCodeIndex < m_CodeBuffer.size() ? &m_CodeBuffer[Index.uCodeIndex] : nullptr;
	const hlx::bytes* pInterface = Index.uInterfaceIndex < m_InterfaceBuffer.size() ? &m_InterfaceBuffer[Index.uInterfaceIndex] : nullptr;
	const hlx::bytes* pInputLayout = Index.uInputLayoutIndex < m_LayoutBuffer.size() ? &m_LayoutBuffer[Index.uInputLayoutIndex] : nullptr;
	const hlx::bytes* pOutputLayout = Index.uOutputLayoutIndex < m_LayoutBuffer.size() ? &m_LayoutBuffer[Index.uOutputLayoutIndex] : nullptr;
	const Display::ShaderFunction* pFunction = Index.uFunctionIndex < m_Functions.size() ? &m_Functions[Index.uFunctionIndex] : nullptr;

	return PermutationBufferRef(&Index, pFunction, pCode, pInterface, pInputLayout, pOutputLayout);
}
//---------------------------------------------------------------------------------------------------
