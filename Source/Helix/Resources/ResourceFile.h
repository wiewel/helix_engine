//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef RESOURCEFILE_H
#define RESOURCEFILE_H

#include "ResourceFileFormat.h"
#include <vector>
#include <unordered_map>
#include <memory>
#include "hlx\src\Logger.h"
#include "LZMA.h"

namespace Helix
{
	namespace Resources
	{
		class ResourceFile
		{
		public:
			ResourceFile(hlx::string _packagePath, hlx::string _packageDirectory, bool _checkCRC = false);
			~ResourceFile();

			bool Read();

			//load a resource from file and return a buffer for parsing
			bool GetResource(uint32_t _uFileID, hlx::bytes& _OutputData, bool _bOnlyUseLocalFiles = false);

			bool ExtractLocalFiles();
			bool ExtractAllFiles();

			std::string GetPath(uint32_t _uFileID) const;
			const ResourceEntry* GetEntry(uint32_t _uFileID);
			const ResourceEntry* GetEntry(const std::string& _sFilePath, char _Separator = '\\');

			void SetDecryptionKey(AES128Key _Key);
			//call this function after loading all resources has been done
			//this will close all opened file streams but leaves package info intact
			void Close();

			HDEBUGNAME("ResourceFile");
		private:
			bool GetDataFromResourceFile(const ResourceEntry& _Entry, hlx::bytes& _OutputData);
			//if out != nullptr and extraction was successfull out will be filled (append) the extracted data
			bool ExtractFile(const ResourceEntry& _Entry, hlx::bytes* _pOutputData = nullptr);

			bool UnpackAndDecrypt(hlx::bytes& _InputData, hlx::bytes& _OutputData, const ResourceEntry& _Entry);
		private:
			hlx::fbytestream m_FileStream;
			//path to the directory that contains all package files
			hlx::string m_sPackageDir;
			//path to the Index file
			hlx::string m_sPackagePath;

			bool m_bCheckCRC;

			AES128Key m_DecryptionKey;

			ResourceIndexFileHeader m_IndexFileHeader;

			//each element can be accessed via its fileID
			std::vector<ResourceEntry> m_Resources;

			//fileIDs of all entries marked as local_has_priority 
			std::vector<uint32_t> m_LocalFileIDs;

			//streams of all package files associated to this index file
			std::vector<std::shared_ptr<hlx::fbytestream> > m_PackageFileStreams;

			//map for faster resource entry lookup using paths
			std::unordered_map<std::string, const ResourceEntry*> m_ResourceMap;
		};

		inline void ResourceFile::SetDecryptionKey(AES128Key _Key) { m_DecryptionKey = _Key; }
	} // Resources
} // Helix

#endif //RESOURCEFILE_H