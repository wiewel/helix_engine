//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "HMATPROPFile.h"
#include "CommonTypeTextSerializer.h"
#include "FileSystem.h"

using namespace Helix::Resources;
//---------------------------------------------------------------------------------------------------
HMATPROPFile::HMATPROPFile(const HMatProps& _Props):
	m_Properties(_Props)
{
}
//---------------------------------------------------------------------------------------------------

HMATPROPFile::HMATPROPFile(const hlx::string& _sFileName)
{
	if (_sFileName.empty() == false)
	{
		Read(_sFileName);
	}
}
//---------------------------------------------------------------------------------------------------

HMATPROPFile::HMATPROPFile(const HMATPROPFile& _PropFile) :
	m_Properties(_PropFile.m_Properties)
{
}
//---------------------------------------------------------------------------------------------------

HMATPROPFile::~HMATPROPFile()
{
}
//---------------------------------------------------------------------------------------------------

inline HTextureMapProp ReadMapProps(const hlx::TextToken& _Token)
{
	HTextureMapProp Map;

	Map.bEnabled = _Token.get<bool>("Enabled", true);
	Map.vScale = to(_Token.getVector<float, 4>("Scale"));
	Map.vOffset = to(_Token.getVector<float, 4>("Offset"));

	return Map;
}
//---------------------------------------------------------------------------------------------------

inline hlx::TextToken WriteMapProps(const HTextureMapProp& Props, const std::string& _sMapName)
{
	hlx::TextToken Map(_sMapName);

	Map.set("Enabled", Props.bEnabled);
	Map.setVector<float, 4>("Scale", to(Props.vScale));
	Map.setVector<float, 4>("Offset", to(Props.vOffset));

	return Map;
}
//---------------------------------------------------------------------------------------------------

void HMATPROPFile::Read(const hlx::string& _sFileName)
{
	hlx::fbytestream FStream;
	if (FileSystem::Instance()->OpenFileStream(HelixDirectories_MaterialProperties, _sFileName, std::ios::in, FStream))
	{
		hlx::bytes Data = FStream.get<hlx::bytes>(FStream.size());
		FStream.close();

		hlx::bytestream BStream(Data);
		Read(BStream);
	}
}
//---------------------------------------------------------------------------------------------------

void HMATPROPFile::Read(hlx::bytestream& _Stream)
{
	hlx::TextToken MatToken(_Stream, "MaterialProperties");
	Read(MatToken);
}
//---------------------------------------------------------------------------------------------------

void HMATPROPFile::Read(const hlx::TextToken& _Properties)
{
	m_Properties = Convert(_Properties);
}
//---------------------------------------------------------------------------------------------------

HMatProps HMATPROPFile::Convert(const hlx::TextToken & _Properties)
{
	HMatProps Props;

	Props.sHMatName = _Properties.getValue("HMatName");
	Props.sSubMeshName = _Properties.getValue("SubMesh");

	Props.fMetallic = _Properties.get<float>("Metallic", 0.f);
	Props.fRoughness = _Properties.get<float>("Roughness", 1.f);

	Props.fHorizonFade = _Properties.get<float>("HorizonFade", 0.0f);

	Props.vColorAlbedo = to(_Properties.getVector<float, 3>("Albedo Color", HFLOAT3_ONE));
	Props.vColorEmissive = to(_Properties.getVector<float, 3>("Emissive Color", HFLOAT3_ZERO));

	Props.vTextureTiling = to(_Properties.getVector<float, 2>("Texture Tiling", { 1.f, 1.f }));
	Props.bUseDefaultPasses = _Properties.get<bool>("Use Default Passes", true);
	Props.RenderPasses = _Properties.getAllValues("RenderPasses");

	hlx::TextToken tmp;

	if (_Properties.getTokenConst("AlbedoMap", tmp))
	{
		Props.AlbedoMap = ReadMapProps(tmp);
	}

	if (_Properties.getTokenConst("NormalMap", tmp))
	{
		Props.NormalMap = ReadMapProps(tmp);
	}

	if (_Properties.getTokenConst("MetallicMap", tmp))
	{
		Props.MetallicMap = ReadMapProps(tmp);
	}

	if (_Properties.getTokenConst("RoughnessMap", tmp))
	{
		Props.RoughnessMap = ReadMapProps(tmp);
	}

	if (_Properties.getTokenConst("EmissiveMap", tmp))
	{
		Props.EmissiveMap = ReadMapProps(tmp);
	}

	if (_Properties.getTokenConst("HeightMap", tmp))
	{
		Props.HeightMap = ReadMapProps(tmp);
	}

	if (_Properties.getTokenConst("EmissiveMap", tmp))
	{
		Props.EmissiveMap = ReadMapProps(tmp);
	}

	return Props;
}
//---------------------------------------------------------------------------------------------------

void HMATPROPFile::Write(hlx::bytestream& _Stream)
{
	Write().serialize(_Stream);
}
//---------------------------------------------------------------------------------------------------

hlx::TextToken HMATPROPFile::Write()
{
	return Convert(m_Properties);
}
//---------------------------------------------------------------------------------------------------

hlx::TextToken HMATPROPFile::Convert(const HMatProps& _Props)
{
	hlx::TextToken MatToken("MaterialProperties");

	MatToken.set("HMatName", _Props.sHMatName);
	MatToken.set("SubMesh", _Props.sSubMeshName);

	MatToken.set("Metallic", _Props.fMetallic);
	MatToken.set("Roughness", _Props.fRoughness);
	MatToken.set("HorizonFade", _Props.fHorizonFade);

	MatToken.setVector<float, 3>("Albedo Color", to(_Props.vColorAlbedo));
	MatToken.setVector<float, 3>("Emissive Color", to(_Props.vColorEmissive));
	MatToken.setVector<float, 2>("Texture Tiling", to(_Props.vTextureTiling));

	MatToken.set("Use Default Passes", _Props.bUseDefaultPasses);
	MatToken.addContainer<std::string>("RenderPasses", _Props.RenderPasses.cbegin(), _Props.RenderPasses.cend());

	MatToken.addToken("AlbedoMap", WriteMapProps(_Props.AlbedoMap, "AlbedoMap"));
	MatToken.addToken("NormalMap", WriteMapProps(_Props.NormalMap, "NormalMap"));
	MatToken.addToken("MetallicMap", WriteMapProps(_Props.MetallicMap, "MetallicMap"));
	MatToken.addToken("RoughnessMap", WriteMapProps(_Props.RoughnessMap, "RoughnessMap"));
	MatToken.addToken("EmissiveMap", WriteMapProps(_Props.EmissiveMap, "EmissiveMap"));
	MatToken.addToken("HeightMap", WriteMapProps(_Props.HeightMap, "HeightMap"));
	MatToken.addToken("EmissiveMap", WriteMapProps(_Props.EmissiveMap, "EmissiveMap"));

	return MatToken;
}
//---------------------------------------------------------------------------------------------------
