//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher, David Labode
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef PHYSXLOGGER_H
#define PHYSXLOGGER_H

#include "foundation\PxErrorCallback.h"
#include "foundation\PxAssert.h"
#include "hlx\src\Logger.h"

namespace Helix
{
	namespace Physics
	{
		class PhysXLogger : public physx::PxErrorCallback
		{
		public:
			PhysXLogger(const hlx::MessageType _kMaxErrorLevel = hlx::kMessageType_Fatal) : m_kMaxErrorLevel(_kMaxErrorLevel) {};
			~PhysXLogger() {};

			void reportError(physx::PxErrorCode::Enum code, const char* message, const char* file, int line) final;
			
			void SetMaxErrorLevel(const hlx::MessageType _kMaxErrorLevel);

		private:
			hlx::MessageType m_kMaxErrorLevel;
		};

		inline void PhysXLogger::reportError(physx::PxErrorCode::Enum code, const char* message, const char* file, int line)
		{
			const wchar_t* errorCode = nullptr;

			using namespace hlx;
			MessageType kErrorLevel = kMessageType_Info;

			switch (code)
			{
			case physx::PxErrorCode::eNO_ERROR:
				errorCode = L"no error";
				kErrorLevel = kMessageType_Info;
				break;
			case physx::PxErrorCode::eINVALID_PARAMETER:
				errorCode = L"invalid parameter";
				kErrorLevel = kMessageType_Fatal;
				break;
			case physx::PxErrorCode::eINVALID_OPERATION:
				errorCode = L"invalid operation";
				kErrorLevel = kMessageType_Fatal;
				break;
			case physx::PxErrorCode::eOUT_OF_MEMORY:
				errorCode = L"out of memory";
				kErrorLevel = kMessageType_Fatal;
				break;
			case physx::PxErrorCode::eDEBUG_INFO:
				errorCode = L"info";
				kErrorLevel = kMessageType_Info;
				break;
			case physx::PxErrorCode::eDEBUG_WARNING:
				errorCode = L"warning";
				kErrorLevel = kMessageType_Warning;
				break;
			case physx::PxErrorCode::ePERF_WARNING:
				errorCode = L"performance warning";
				kErrorLevel = kMessageType_Warning;
				break;
			case physx::PxErrorCode::eABORT:
				errorCode = L"abort";
				kErrorLevel = kMessageType_Fatal;
				break;
			case physx::PxErrorCode::eINTERNAL_ERROR:
				errorCode = L"internal error";
				kErrorLevel = kMessageType_Fatal;
				break;
			case physx::PxErrorCode::eMASK_ALL:
			default:
				errorCode = L"unknown error";
				kErrorLevel = kMessageType_Error;
				break;
			}

			kErrorLevel = std::min(m_kMaxErrorLevel, kErrorLevel);

			Logger::Instance()->Log(kErrorLevel, nullptr, hlx::to_wstring(file).c_str(), line, L"PhysX %s: %s", errorCode, WCSTR(message));
		}
		//---------------------------------------------------------------------------------------------------

		inline void PhysXLogger::SetMaxErrorLevel(const hlx::MessageType _kMaxErrorLevel)
		{
			m_kMaxErrorLevel = _kMaxErrorLevel;
		}
		//---------------------------------------------------------------------------------------------------

		class AssertHandler : public physx::PxAssertHandler
		{
		public:
			inline void operator()(const char* exp, const char* file, int line, bool& ignore) final
			{
				if (m_bIgnoreAssert)
				{
					HERROR("PhysX supressing assert %s %s (%d)", CSTR(exp), CSTR(file), line);
				}
				else
				{
					HFATAL("PhysX assert %s %s (%d)", CSTR(exp), CSTR(file), line);
				}

				ignore = m_bIgnoreAssert;
			};

			void SetIgnoreAssert(bool _bIgnore);
			bool GetIgnoreAssert() const;
		private:
			bool m_bIgnoreAssert = false;
		};

		inline void AssertHandler::SetIgnoreAssert(bool _bIgnore){	m_bIgnoreAssert = _bIgnore;}
		inline bool AssertHandler::GetIgnoreAssert() const{	return m_bIgnoreAssert;	}
	} // Physics
} // Helix

#endif // !PHYSXLOGGER_H
