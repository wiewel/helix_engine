//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef SCENELOCK_H
#define SCENELOCK_H

#include "PxScene.h"
#include "PxActor.h"
#include "hlx\src\Logger.h"
#include "Scene\PhysicsScene.h"

namespace Helix
{
	namespace Physics
	{
		class SceneReadLock
		{
		public:
			inline SceneReadLock()
				: m_pScene(Scene::PhysicsScene::Instance()->GetScene())
			{
				HASSERTD(m_pScene != nullptr, "Invalid scene!");
				m_pScene->lockRead();
			}

			inline SceneReadLock(physx::PxScene* _pScene)
				: m_pScene(_pScene)
			{
				HASSERTD(m_pScene != nullptr, "Invalid scene!");
				m_pScene->lockRead();
			}

			inline SceneReadLock(physx::PxActor* _pActor)
				: m_pScene(_pActor != nullptr ? _pActor->getScene() : nullptr)
			{
				HASSERTD(m_pScene != nullptr, "Invalid scene!");
				m_pScene->lockRead();
			}

			inline SceneReadLock(Scene::PhysicsScene* _pScene)
				: m_pScene(_pScene != nullptr ? _pScene->GetScene() : nullptr)
			{
				HASSERTD(m_pScene != nullptr, "Invalid scene!");
				m_pScene->lockRead();
			}

			inline ~SceneReadLock()
			{
				m_pScene->unlockRead();
			}

		private:
			physx::PxScene* m_pScene = nullptr;
		};

		class SceneWriteLock
		{
		public:
			inline SceneWriteLock()
			{
				if (m_pScene == nullptr)
				{
					m_pScene = Scene::PhysicsScene::Instance();
					m_pPxScene = m_pScene->GetScene();
				}

				HASSERTD(m_pScene != nullptr && m_pPxScene != nullptr, "Invalid scene!");
				m_pScene->LockWrite();
				m_pPxScene->lockWrite();
			}

			inline ~SceneWriteLock()
			{
				m_pPxScene->unlockWrite();
				m_pScene->UnlockWrite();
			}

		public:
			static Scene::PhysicsScene* m_pScene;
			static physx::PxScene* m_pPxScene;
		};
	}
} // Helix

#endif // !SCENELOCK_H
