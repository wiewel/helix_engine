//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher, David Labode
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Force fields for rigidbody kinematics
//--------------------------------------------------------------------------------------------------------------------
// Summary:
// Various kinds of force fields should be defined here
// the first one shall be gravity
//--------------------------------------------------------------------------------------------------------------------
// - Gravity
// - Directional
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef GRAVITYFIELD_H
#define GRAVITYFIELD_H
#include "DataStructures\GameObjectPool.h"

namespace Helix
{
	namespace Scene
	{
		struct ParticleYoke;
	}

	namespace Physics
	{
		// Forward declarations
		class GravityField;
		// Yoke
		struct GravityFieldYoke
		{
			GravityFieldYoke(GravityField* _pField, float _fDeltaTime);
			float fMinSqDistance;
			float fMaxSqDistance;
			float fTotalTime;
			float fDeltaTime;
			AMP::array<AMP::float4>& GravityPoints;
			AMP::float3 RandomVectors[256];
		};
		// Data
		class GravityField
		{
			friend struct GravityFieldYoke;
		private:
			float m_fMinSqDistance = 0.0f;
			float m_fMaxSqDistance = 0.0f;
			float m_fTotalTime = 0.0f;
			AMP::AMPVector<AMP::float4> m_GravityParticles;
			uint32_t m_uGravityPointCount = 0;
			uint32_t m_uMaxGravityPoints;

		public:
			GravityField(uint32_t _uNumGravityParticles, const concurrency::accelerator_view& _AMPAccelerator, const concurrency::access_type _kAccessType = concurrency::access_type_auto, float _fNearRadius = 0.4472136f, float _InfluenceRadius = 6.0f);
			void Upload();
			void ApplyForce(Datastructures::RigidbodyYoke& _RbYoke, Scene::ParticleYoke& _PaYoke, const float _fDeltaTime, uint32_t& _uNumActiveParticles);
			bool PushGravityPoint(const AMP::float3& _Position, const float _Strength);
			bool RemoveGravityPoint(const AMP::float3& _Position);
			void Clear();
			uint32_t GetMaxParticleCount() const;
			void SetMaxDistance(const float _fMaxDist);
			float GetMaxDistance() const;

		private:
			void ProcessRigidbodies(Datastructures::RigidbodyYoke& _RbYoke, GravityFieldYoke& _GfYoke);
			void ProcessParticles(Scene::ParticleYoke& _PaYoke, GravityFieldYoke& _GfYoke, uint32_t& _uNumActiveParticles);

		};

		inline uint32_t GravityField::GetMaxParticleCount() const { return m_uMaxGravityPoints; }
		inline void GravityField::SetMaxDistance(const float _fMaxDist)
		{
			m_fMaxSqDistance = _fMaxDist*_fMaxDist;
		}
		inline float GravityField::GetMaxDistance() const { return std::sqrtf(m_fMaxSqDistance); }


	}
}

#endif // !GRAVITYFIELD_H
