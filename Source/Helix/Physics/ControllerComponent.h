//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef CONTROLLERCOMPONENT_H
#define CONTROLLERCOMPONENT_H

#include "DataStructures\GameObjectComponent.h"
#include "characterkinematic\PxController.h"
#include "Async\SpinLock.h"

namespace Helix
{
	namespace Physics
	{
		enum ControllerType
		{
			ControllerType_None = 0,
			ControllerType_Capsule = 1,
			ControllerType_Box = 2,
		};

		//===================================================================================================
		struct ControllerDesc
		{
			Math::float3 vPosition = Math::float3(0,0,0); 
			Math::float3 vUpDirection = Math::float3(0,1,0);
			float fSlopeLimit = 0.707f;
			float fInvisibleWallHeight = 0.0f;
			float fMaxJumpHeight = 0.0f;
			float fContactOffset = 0.1f;
			float fStepOffset = 0.5f;
			SurfaceMaterial* pSurfaceMaterial = nullptr;
		};
		struct CapsuleControllerDesc : ControllerDesc
		{
			float fRadius = 0.5f;
			float fHeight = 0.5f; // real height is fHeight + fRadius
		};
		struct BoxControllerDesc : ControllerDesc
		{
			float fHalfHeight = 1.0f;
			float fHalfForwardExtent = 0.5f;
			float fHalfSideExtent = 0.5f;
		};

		//===================================================================================================
		// physx constructs a hidden actor with a query shape to detemine if the actor is penetrating. 
		// in order to allow other shapes beeing attached to the gameobject, we need to ignore collisions between
		// them and the hidden CC shapes explicitly
		class ControllerQueryFilterCallback : public physx::PxQueryFilterCallback
		{
		public:
			//---------------------------------------------------------------------------------------------------
			void SetParent(Datastructures::GameObject* _pParent)
			{
				m_pParent = _pParent;
			}
			//---------------------------------------------------------------------------------------------------
			physx::PxQueryHitType::Enum preFilter(
				const physx::PxFilterData &_FilterData,
				const physx::PxShape *_pShape,
				const physx::PxRigidActor *_pActor,
				physx::PxHitFlags &_QueryFlags) final
			{
				// ignore wrongfully initialized callbacks
				if (m_pParent == nullptr)
				{
					return physx::PxQueryHitType::eNONE;
				}
				// ignore self overlaps
				if (m_pParent == _pActor->userData)
				{
					return physx::PxQueryHitType::eNONE;
				}

				return physx::PxQueryHitType::eBLOCK;
			}
			//---------------------------------------------------------------------------------------------------
			physx::PxQueryHitType::Enum postFilter(const physx::PxFilterData& _FilterData,	const physx::PxQueryHit& _Hit) final
			{
				return physx::PxQueryHitType::eTOUCH;
			}

		private:

			Datastructures::GameObject* m_pParent = nullptr;
		};
		//===================================================================================================
		class ControllerComponent : public Datastructures::IGameObjectComponent
		{
		private:
			ControllerComponent(Datastructures::GameObject* _pParent);
			ControllerComponent(Datastructures::GameObject* _pParent, CapsuleControllerDesc& _Desc);
			ControllerComponent(Datastructures::GameObject* _pParent, BoxControllerDesc& _Desc);

			COMPTYPENAME(ControllerComponent, Datastructures::GameObject, 'Ctrl');
			COMPDESCONSTR(ControllerComponent, Datastructures::GameObject, IGameObjectComponent);
			COMPDEFCONSTR(ControllerComponent, Datastructures::GameObject, IGameObjectComponent);
		public:

			void OnUpdate(double _fDeltaTime, double _fTotalTime) final;

			void CreateBody(CapsuleControllerDesc& _ControllerDesc);
			void CreateBody(BoxControllerDesc& _ControllerDesc);
			void DestroyBody();

			void Strafe(const float _fDistance);
			void Elevate(const float _fDistance);
			void Walk(const float _fDistance);

			void Teleport(const Math::float3 _vPosition);

			void Jump(const float _fVelocity);

			void Turn(const float _fDegrees);
			void Nod(const float _fDegrees);
			void LookAt(const Math::float3& _vPosition);

			Math::float3 GetRight() const;
			Math::float3 GetUp() const;
			Math::float3 GetLook() const;

			void SetClipping(bool _bIsGhost);
			bool GetClipping() const;

			void SetGravityEnabled(bool _bEnabled);
			bool SetGravityEnabled() const;

			float GetMoveDistance() const;

		private:

			void CreateCharacterController(physx::PxControllerDesc& _Desc);

			bool m_bClipping = true;
			bool m_bGravityEnabled = true;

			float m_fForce = 0.0f;
			float m_fVelocity = 0.0f;
			Math::float3 m_vDisplacement = Math::float3(0,0,0);
			bool m_bIsJumping = false;

			physx::PxController* m_pController = nullptr;
			ControllerQueryFilterCallback m_QueryFilterCallback;

			Async::SpinLock m_ControllerLock;
			Async::SpinLock m_MovementLock;
		};
		//---------------------------------------------------------------------------------------------------
		inline Math::float3 ControllerComponent::GetRight() const
		{
			return GetParent()->GetOrientation() * Math::float3(1, 0, 0);
		}
		//---------------------------------------------------------------------------------------------------
		inline Math::float3 ControllerComponent::GetUp() const
		{
			return GetParent()->GetOrientation() * Math::float3(0, 1, 0);
		}
		//---------------------------------------------------------------------------------------------------
		inline Math::float3 ControllerComponent::GetLook() const
		{
			return GetParent()->GetOrientation() * Math::float3(0, 0, 1);
		}
		//---------------------------------------------------------------------------------------------------
		inline void ControllerComponent::SetClipping(bool _bIsClipping)
		{
			m_bClipping = _bIsClipping;
		}
		//---------------------------------------------------------------------------------------------------
		inline bool ControllerComponent::GetClipping() const
		{
			return m_bClipping;
		}
		//---------------------------------------------------------------------------------------------------
		inline void ControllerComponent::SetGravityEnabled(bool _bEnabled)
		{
			m_bGravityEnabled = _bEnabled;
		}
		//---------------------------------------------------------------------------------------------------
		inline bool ControllerComponent::SetGravityEnabled() const
		{
			return m_bGravityEnabled;
		}
		//---------------------------------------------------------------------------------------------------
		inline float ControllerComponent::GetMoveDistance() const
		{
			return m_vDisplacement.magnitude();
		}
	}
}

#endif // !CONTROLLERCOMPONENT_H
