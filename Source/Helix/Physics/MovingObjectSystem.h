//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef MOVINGOBJECTSYSTEM_H
#define MOVINGOBJECTSYSTEM_H

#include "PhysicsSystem.h"
#include "MovingObject.h"
#include "XMMath.h"
#include "XMIntegrators.h"

#include <vector>
#include <atomic>
#include <unordered_map>

using namespace DirectX;

namespace helix
{
	namespace physics
	{

		typedef std::vector<datastructures::MovingObject*> TMovingObjects;
		typedef std::unordered_multimap<uint32_t, XMFLOAT3> TMovingObjectForceMap;

		//All time values are in Seconds
		class MovingObjectSystem : public PhysicsSystem<TMovingObjects, TMovingObjectForceMap>
		{
		public:
			HDEBUGNAME("MovingObjectSystem");

			//don't allow copying
			MovingObjectSystem(const MovingObjectSystem& rbs) = delete;

			MovingObjectSystem();
			MovingObjectSystem(async::IAsyncObserver* _Observer);

			~MovingObjectSystem();

			//from PhysicsSystem
			virtual void ClearData();
			virtual void ClearExternalData();

			void SetDefaultForce(const XMFLOAT3& _DefaultForce);

			void SetIntegrator(math::XMIntegrator _Integrator);

		private:
			//from PhysicsSystem
			virtual void Simulate(float _fDeltaTime);

		private:
			math::XMIntegrator m_Integrator = math::XMIntegrator_Heun;
			XMFLOAT3 m_DefaultForce = XMFLOAT3_ZERO;

		};

	}; // namespace physics

}

#endif // MOVINGOBJECTSYSTEM_H