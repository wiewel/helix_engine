//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "SimulationEventCallback.h"
#include "DataStructures\ShapeComponent.h"

using namespace Helix::Physics;

//-----------------------------------------------------------------------------------------------
void Helix::Physics::SimulationEventCallback::onTrigger(physx::PxTriggerPair* _pPairs, physx::PxU32 _uCount)
{
	auto begin = stdext::checked_array_iterator<physx::PxTriggerPair*>(_pPairs, _uCount, 0);
	auto end = stdext::checked_array_iterator<physx::PxTriggerPair*>(_pPairs, _uCount, _uCount);

	concurrency::parallel_for_each(begin, end, [&](physx::PxTriggerPair& _pPair)
	{
		// only proceed if the trigger objects requested a callback
		const TriggerEvent* pEvent = nullptr;
		if (m_TriggerReceivers.Find(_pPair.triggerShape, pEvent))
		{
			TriggerInfo Info;
			Info.pTrigger = static_cast<Datastructures::GameObject*>(_pPair.triggerShape->userData);
			Info.pActivator = static_cast<Datastructures::GameObject*>(_pPair.otherShape->userData);

			// some internal physics actors don't have a user data set -> just ignore them
			if (Info.pActivator == nullptr || Info.pTrigger == Info.pActivator)
			{
				return;
			}

			// only select flags relevant in trigger event handling
			Info.State = static_cast<SimulationEventState>(_pPair.status & SimulationEventState_All);

			// execute the callbacks that were registered in the trigger shape component
			pEvent->Invoke(Info);
		}
	});
}
//-----------------------------------------------------------------------------------------------
void Helix::Physics::SimulationEventCallback::onContact(const physx::PxContactPairHeader & _PairHeader, 
	const physx::PxContactPair * _pPairs, physx::PxU32 _uNbPairs)
{
	PX_UNUSED(_PairHeader);

	// TODO: parallel?
	for (uint32_t i = 0; i < _uNbPairs; ++i)
	{
		// check if the first object requested a callback and buildup collition information
		const ContactEvent* pEvent = nullptr;
		if (m_ContactReceivers.Find(_pPairs[i].shapes[0], pEvent))
		{
			ContactInfo Info;
			Info.pObject0 = static_cast<Datastructures::GameObject*>(_pPairs[i].shapes[0]->userData);
			Info.pObject1 = static_cast<Datastructures::GameObject*>(_pPairs[i].shapes[1]->userData);
			Info.State = static_cast<SimulationEventState>(SimulationEventState_All & static_cast<uint32_t>(_pPairs[i].events));

			// extract the contact points
			std::vector<physx::PxContactPairPoint> ContactPairPoints(_pPairs->contactCount);
			_pPairs->extractContacts(ContactPairPoints.data(), _pPairs->contactCount);
			for (physx::PxContactPairPoint Point : ContactPairPoints)
			{
				ContactPoint ContactPoint;
				ContactPoint.vPosition = Point.position;
				ContactPoint.vNormal = Point.normal;
				ContactPoint.vImpulse = Point.impulse;
				ContactPoint.fSeparation = Point.separation;
				Info.ContactPoints.push_back(ContactPoint);
			}

			// execute all callbacks attached to the shape
			pEvent->Invoke(Info);
		}

		// check if the second object requested a callback and create collision information
		pEvent = nullptr;
		if (m_ContactReceivers.Find(_pPairs[i].shapes[1], pEvent))
		{
			ContactInfo Info;
			// change order to ensure that the requesting object is the first in info
			Info.pObject0 = static_cast<Datastructures::GameObject*>(_pPairs[i].shapes[0]->userData);
			Info.pObject1 = static_cast<Datastructures::GameObject*>(_pPairs[i].shapes[1]->userData);
			Info.State = static_cast<SimulationEventState>(SimulationEventState_All & static_cast<uint32_t>(_pPairs[i].events));

			// extract the contact points
			std::vector<physx::PxContactPairPoint> ContactPairPoints(_pPairs->contactCount);
			_pPairs->extractContacts(ContactPairPoints.data(), _pPairs->contactCount);
			for (physx::PxContactPairPoint Point : ContactPairPoints)
			{
				ContactPoint ContactPoint;
				ContactPoint.vPosition = Point.position;
				ContactPoint.vNormal = Point.normal;
				ContactPoint.vImpulse = Point.impulse;
				ContactPoint.fSeparation = Point.separation;
				Info.ContactPoints.push_back(ContactPoint);
			}

			// execute all callbacks attached to the shape
			pEvent->Invoke(Info);
		}
	}
}
//-----------------------------------------------------------------------------------------------
void Helix::Physics::SimulationEventCallback::onWake(physx::PxActor ** _pActors, physx::PxU32 _uCount)
{
	// TODO
}
//-----------------------------------------------------------------------------------------------
void Helix::Physics::SimulationEventCallback::onSleep(physx::PxActor ** _pActors, physx::PxU32 _uCount)
{
	// TODO
}
//-----------------------------------------------------------------------------------------------
void Helix::Physics::SimulationEventCallback::onConstraintBreak(physx::PxConstraintInfo * _Constraints, 
	physx::PxU32 _uCount)
{
	// TODO
}
