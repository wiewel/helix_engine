//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef PHYSICSSYSTEM_H
#define PHYSICSSYSTEM_H

#include "Async\AsyncObject.h"

namespace Helix
{
	namespace Physics
	{

		//All time values are in Seconds
		template <class MAINDATA, class EXTERNALDATA>
		class PhysicsSystem : public Async::IAsyncObject
		{
		public:
		
			//don't allow copying
			PhysicsSystem(const PhysicsSystem& rbs) = delete;

			PhysicsSystem(Async::IAsyncObject* _pParent = nullptr, Async::IAsyncObserver* _pObserver = nullptr)
				: Async::IAsyncObject(_pParent, _pObserver, true, true)
			{
			}

			~PhysicsSystem()
			{
			}

			//extract results of the last simulation step
			inline void GetCurrentData(_Out_ MAINDATA& _OutputData)
			{
				std::lock_guard<std::mutex> lock(m_LockMutex);
				_OutputData = m_Data;
			}

			//set input data for the next simulation step
			inline void SetData(_In_ const MAINDATA& _InputData)
			{
				std::lock_guard<std::mutex> lock(m_LockMutex);
				m_Data = _InputData;
			}

			//add external data to the system for the next simulation step
			inline void AddExternalData(_In_ const EXTERNALDATA& _ExternalData)
			{
				std::lock_guard<std::mutex> lock(m_LockMutex);
				m_ExternalData = _ExternalData;
			}

			virtual void ClearExternalData() = 0;
			virtual void ClearData() = 0;

			inline void SetDeltaTime(float _fDeltaTime) { std::lock_guard<std::mutex> lock(m_LockMutex); m_fDeltaTime = _fDeltaTime; }
			inline float GetDeltaTime() const { return m_fDeltaTime; }

		protected:
			MAINDATA m_Data;
			EXTERNALDATA m_ExternalData;

			std::mutex m_LockMutex;

			//delta time (in seconds) for the current simulation step
			float m_fDeltaTime = 0.02f;
		};	

	}; // namespace Physics

}

#endif //PHYSICSSYSTEM_H