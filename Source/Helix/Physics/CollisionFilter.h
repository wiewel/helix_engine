//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef COLLISIONFILTER_H
#define COLLISIONFILTER_H

#include "PxFiltering.h"
#include "Util\Flag.h"

namespace Helix
{
	namespace Physics
	{
		enum CollisionFilterFlag : uint32_t
		{
			CollisionFilterFlag_None = 0,
			CollisionFilterFlag_Default			= (1 << 0),
			CollisionFilterFlag_Invisible		= (1 << 1),
			CollisionFilterFlag_MouseTool	= (1 << 2),
			//CollisionFilterFlag_Default = CollisionFilterFlag_Visible
		};

		using TCollisionFilterMask = Flag<CollisionFilterFlag>;

		struct CollisionFilter : public physx::PxFilterData
		{
			using physx::PxFilterData::PxFilterData;

			explicit inline CollisionFilter() : physx::PxFilterData()
			{}

			explicit inline CollisionFilter(CollisionFilterFlag _FilterGroup) :
				physx::PxFilterData(static_cast<uint32_t>(_FilterGroup), 0u, 0u, 0u)
			{}

			explicit inline CollisionFilter(const physx::PxFilterData& _FilterData) :
				physx::PxFilterData(_FilterData.word0, _FilterData.word1, _FilterData.word2, _FilterData.word3)
			{}

			/// This constructor should be used to setup filtering rules for rigid bodies
			/// http://docs.nvidia.com/gameworks/content/gameworkslibrary/physx/guide/Manual/RigidBodyCollision.html
			/// FilterGroup = own ID
			/// FilterMask = ID masks to filter collision callbacks
			explicit inline CollisionFilter(CollisionFilterFlag _FilterGroup, TCollisionFilterMask _FilterMask) :
				physx::PxFilterData(static_cast<uint32_t>(_FilterGroup),
									static_cast<uint32_t>(_FilterMask.GetFlag()),
									0u, 0u)
			{}
			explicit inline CollisionFilter(TCollisionFilterMask _FilterGroups, TCollisionFilterMask _FilterMask = {}) :
				physx::PxFilterData(
					static_cast<uint32_t>(_FilterGroups.GetFlag()),
					static_cast<uint32_t>(_FilterMask.GetFlag()),
					0u, 0u)
			{}

			inline CollisionFilter(const CollisionFilter& _Other)
			{
				word0 = _Other.word0;
				word1 = _Other.word1;
				word2 = _Other.word2;
				word3 = _Other.word3;
			}

			inline CollisionFilter& operator=(const CollisionFilter& _Other)
			{
				word0 = _Other.word0;
				word1 = _Other.word1;
				word2 = _Other.word2;
				word3 = _Other.word3;
				return *this;
			}
			
			inline CollisionFilter& operator |= (const CollisionFilter& _Other)
			{
				word0 |= _Other.word0;
				word1 |= _Other.word1;
				word2 |= _Other.word2;
				word3 |= _Other.word3;
				return *this;
			}

			inline CollisionFilter operator | (const CollisionFilter& _Other)
			{
				CollisionFilter filter;
				filter.word0 = word0 | _Other.word0;
				filter.word1 = word1 | _Other.word1;
				filter.word2 = word2 | _Other.word2;
				filter.word3 = word3 | _Other.word3;
				return filter;
			}
		};
	} // Physics
} // Helix

#endif // !COLLISIONFILTER_H
