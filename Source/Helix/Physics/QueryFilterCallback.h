//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher, David Labode
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef QUERYFILTERCALLBACK_H
#define QUERYFILTERCALLBACK_H

#include "PxQueryFiltering.h"
#include "DataStructures\GameObject.h"

#include <unordered_set>

namespace Helix
{
	namespace Physics
	{
		//=======================================================================================================
		class QueryFilterCallback : public physx::PxQueryFilterCallback
		{
		public:
			//---------------------------------------------------------------------------------------------------
			physx::PxQueryHitType::Enum preFilter(
				const physx::PxFilterData& filterData,
				const physx::PxShape* shape,
				const physx::PxRigidActor* actor,
				physx::PxHitFlags& queryFlags) override
			{
				return physx::PxQueryHitType::eTOUCH;
			}
			//---------------------------------------------------------------------------------------------------
			inline physx::PxQueryHitType::Enum postFilter(
				const physx::PxFilterData& filterData, 
				const physx::PxQueryHit& hit) override
			{
				return physx::PxQueryHitType::eTOUCH;
			}
		};

		//=======================================================================================================
		template <class Container>
		struct QueryFilterAggregator : public QueryFilterCallback
		{
			//---------------------------------------------------------------------------------------------------
			QueryFilterAggregator(Container& _Container) :
				m_GameObjects(_Container) {}
			//---------------------------------------------------------------------------------------------------
			// Why is this query filter collecting all gameobjects in broad phase?
			inline physx::PxQueryHitType::Enum preFilter(
				const physx::PxFilterData& filterData,
				const physx::PxShape* shape,
				const physx::PxRigidActor* actor,
				physx::PxHitFlags& queryFlags) final
			{
				m_GameObjects.push_back(static_cast<Datastructures::GameObject*>(actor->userData));
				return physx::PxQueryHitType::eTOUCH;
			}
			//---------------------------------------------------------------------------------------------------
			inline physx::PxQueryHitType::Enum postFilter(const physx::PxFilterData& filterData, const physx::PxQueryHit& hit) final
			{
				return physx::PxQueryHitType::eTOUCH;
			}
		private:
			Container& m_GameObjects;
		};

		//=======================================================================================================
		class QueryFilterIgnore : public QueryFilterCallback
		{
		public:
			//---------------------------------------------------------------------------------------------------
			QueryFilterIgnore() {}
			//---------------------------------------------------------------------------------------------------
			QueryFilterIgnore(const std::vector<Datastructures::GameObject*> _GameObjectsToIgnore)
			{
				for (Datastructures::GameObject* pGO : _GameObjectsToIgnore)
				{
					m_GameObjectsToIgnore.insert(pGO);
				}
			}
			//---------------------------------------------------------------------------------------------------
			void AddIgnore(const Datastructures::GameObject* _pGameObjectToIgnore)
			{
				m_GameObjectsToIgnore.insert(_pGameObjectToIgnore);
			}
			//---------------------------------------------------------------------------------------------------
			void RemoveIgnore(const Datastructures::GameObject* _pGameObject)
			{
				m_GameObjectsToIgnore.erase(_pGameObject);
			}
			//---------------------------------------------------------------------------------------------------
			void ClearIgnores()
			{
				m_GameObjectsToIgnore.clear();
			}
			//---------------------------------------------------------------------------------------------------
			physx::PxQueryHitType::Enum preFilter(
				const physx::PxFilterData &_FilterData,
				const physx::PxShape *_pShape,
				const physx::PxRigidActor *_pActor,
				physx::PxHitFlags &_QueryFlags) final
			{
				// ignore previously specified game objects
				if (m_GameObjectsToIgnore.find(static_cast<Datastructures::GameObject*>(_pActor->userData)) != m_GameObjectsToIgnore.end())
				{
					return physx::PxQueryHitType::eNONE;
				}

				return physx::PxQueryHitType::eBLOCK;
			}
			//---------------------------------------------------------------------------------------------------
			physx::PxQueryHitType::Enum postFilter(const physx::PxFilterData& _FilterData, const physx::PxQueryHit& _Hit) final
			{
				return physx::PxQueryHitType::eTOUCH;
			}

		private:
			std::unordered_set<const Datastructures::GameObject*> m_GameObjectsToIgnore;
		};
	} // Physics
} // Helix

#endif // !QUERYFILTERCALLBACK_H
