//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher, David Labode
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef PHYSXBYTESTREAM_H
#define PHYSXBYTESTREAM_H

#include "hlx\src\ByteStream.h"
#include "foundation\PxIO.h"

namespace Helix
{
	namespace Physics
	{
		class PhysXInputStream : public hlx::const_bytestream, public physx::PxInputData
		{
		public:
			PhysXInputStream() : hlx::const_bytestream() {}
			PhysXInputStream(hlx::const_bytestream::BaseType& b) : hlx::const_bytestream(b) {};
			PhysXInputStream(hlx::const_bytestream::BaseType&& b) : hlx::const_bytestream(std::forward<hlx::const_bytestream::BaseType>(b)) {};

			~PhysXInputStream() {}

			inline physx::PxU32 read(void* dest, physx::PxU32 count) final
			{
				count = std::min(count, (physx::PxU32)available());
				if (count > 0u)
				{
					memcpy(dest, get_data(m_uOffset), count);
					m_uOffset += count;
				}
				return count;
			}

			inline physx::PxU32 getLength() const final
			{
				return static_cast<physx::PxU32>(m_Bytes.size());
			}

			inline void seek(physx::PxU32 offset) final
			{
				set_offset(std::min(offset, static_cast<physx::PxU32>(m_Bytes.size())));
			}

			inline physx::PxU32 tell() const final
			{
				return static_cast<physx::PxU32>(m_uOffset);
			}
		};
	}
} // Helix

#endif // PHYSXBYTESTREAM_H