//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef SIMULATIONEVENTCALLBACK_H
#define SIMULATIONEVENTCALLBACK_H

#include "Async\Event.h"
#include "hlx\src\Singleton.h"
#include "Async\SpinLock.h"
#include "Math\MathTypes.h"
#include "Scene\PhysicsScene.h"

#include <PxSimulationEventCallback.h>
#include <PxShape.h>
#include <unordered_map>

namespace Helix
{
	// forward declare
	namespace Datastructures
	{
		class ShapeComponent;
	}

	namespace Physics
	{
		class SimulationEventCallback;
		
		//===============================================================================================
		// convenience mapping to PxPairFlag with only the commonly used flags
		enum SimulationEventState : uint32_t
		{
			SimulationEventState_Unknown = 0,
			SimulationEventState_Enter = (1 << 2),
			SimulationEventState_Stay = (1 << 3),
			SimulationEventState_Leave = (1 << 4),
			SimulationEventState_CCD = (1 << 5),
			SimulationEventState_All = SimulationEventState_Enter | SimulationEventState_Stay | SimulationEventState_Leave | SimulationEventState_CCD,
		};

		//===============================================================================================
		// information about a trigger beeing activated
		struct TriggerInfo
		{
			// the shape that requested the callback
			Datastructures::GameObject* pTrigger;
			// the shape that activated the trigger
			Datastructures::GameObject* pActivator;
			// enter stay leave
			SimulationEventState State;
		};

		//===============================================================================================
		struct ContactPoint
		{
			Math::float3 vPosition;
			// points from shape 1 to shape 0
			Math::float3 vNormal;
			// divide by deltaT to get force
			Math::float3 vImpulse;
			// negative in case of penetration
			float fSeparation;
		};

		//===============================================================================================
		// information about a collision
		struct ContactInfo
		{
			// this is always the shape that requested the callback
			Datastructures::GameObject* pObject0;
			// the other shape
			Datastructures::GameObject* pObject1;
			// enter persists leave
			SimulationEventState State;
			// the contact points of the collision
			std::vector<ContactPoint> ContactPoints;
		};

		// Event callback signature definitions
		typedef typename Async::Event<void, TriggerInfo&> TriggerEvent;
		typedef typename Async::Event<void, ContactInfo&> ContactEvent;

		//===============================================================================================
		// all shapes that want to receive physx callbacks must register first
		template<typename T>
		struct SimulationEventReceivers
		{
			friend class SimulationEventCallback;
			typedef std::unordered_map<const physx::PxShape*, const T*> ShapeMap;
		public:
			//-----------------------------------------------------------------------------------------------
			inline void RegisterShape(const physx::PxShape* _pShape, const T* _pEvent)
			{
				Async::ScopedSpinLock Lock(m_Lock);
				m_Events[_pShape] = _pEvent;
			}
			//-----------------------------------------------------------------------------------------------
			inline void RemoveShape(const physx::PxShape* _pShape)
			{
				Async::ScopedSpinLock Lock(m_Lock);
				m_Events.erase(_pShape);
			}
			//-----------------------------------------------------------------------------------------------
			inline void RemoveAll()
			{
				Async::ScopedSpinLock Lock(m_Lock);
				m_Events.clear();
			}
		private:
			//-----------------------------------------------------------------------------------------------
			// only invoke the event if the shape was registered before
			template<class... Args>
			inline const bool Invoke(const physx::PxShape* _pShape, Args... _Args)
			{
				Async::ScopedSpinLock Lock(m_Lock);
				ShapeMap::iterator It = m_Events.find(_pShape);
				if (It != m_Events.end())
				{
					It->second->Invoke(_Args...);
					return true;
				}
				return false;
			}
			//-----------------------------------------------------------------------------------------------
			inline bool Contains(const physx::PxShape* _pShape)
			{
				Async::ScopedSpinLock Lock(m_Lock);
				ShapeMap::iterator It = m_Events.find(_pShape);
				return It != m_Events.end();
			}
			//-----------------------------------------------------------------------------------------------
			inline bool Find(const physx::PxShape* _pShape, const T*& _Event)
			{
				Async::ScopedSpinLock Lock(m_Lock);
				ShapeMap::iterator It = m_Events.find(_pShape);
				if (It != m_Events.end() && It->second != nullptr)
				{
					_Event = It->second;
					return true;
				}
				_Event = nullptr;
				return false;
			}

		private:
			Async::SpinLock m_Lock;
			ShapeMap m_Events;
		};

		//===============================================================================================
		// Callbacks for all physx events on actors
		class SimulationEventCallback : physx::PxSimulationEventCallback, hlx::Singleton<SimulationEventCallback>
		{
			friend class Datastructures::ShapeComponent;
			friend class Scene::PhysicsScene;

		public:
			// The shape component can register its events using the receiver
			SimulationEventReceivers<TriggerEvent>& GetOnTrigger();
			SimulationEventReceivers<ContactEvent>& GetOnContact();

		private:
			// These will be called by physx
			void onTrigger(physx::PxTriggerPair* _pPairs, physx::PxU32 _uCount) final;
			void onContact(const physx::PxContactPairHeader& _PairHeader, const physx::PxContactPair* _pPairs, physx::PxU32 _uNbPairs) final;
			void onWake(physx::PxActor** _pActors, physx::PxU32 _uCount) final;
			void onSleep(physx::PxActor** _pActors, physx::PxU32 _uCount) final;
			void onConstraintBreak(physx::PxConstraintInfo* _Constraints, physx::PxU32 _uCount) final;

		private:
			inline void Clear()
			{
				m_TriggerReceivers.RemoveAll();
				m_ContactReceivers.RemoveAll();
			}

			SimulationEventReceivers<TriggerEvent> m_TriggerReceivers;
			SimulationEventReceivers<ContactEvent> m_ContactReceivers;
		};
		//-----------------------------------------------------------------------------------------------
		inline SimulationEventReceivers<TriggerEvent>& SimulationEventCallback::GetOnTrigger() { return m_TriggerReceivers; }
		//-----------------------------------------------------------------------------------------------
		inline SimulationEventReceivers<ContactEvent>& SimulationEventCallback::GetOnContact() { return m_ContactReceivers; }
	}
}

#endif // SIMULATIONEVENTCALLBACK_H