//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher, David Labode
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef VOLUMECACHEITERATOR_H
#define	VOLUMECACHEITERATOR_H

#include "PxVolumeCache.h"

namespace Helix
{
	namespace Physics
	{
		template <class Container>
		struct VolumeCacheIterator : physx::PxVolumeCache::Iterator
		{
			VolumeCacheIterator(Container& _Container) :
				m_GameObjects(_Container) {}

		private:
			inline void processShapes(physx::PxU32 count, const physx::PxActorShape* actorShapePairs) final
			{
				m_GameObjects.reserve(m_GameObjects.size() + count);

				for (uint32_t i = 0; i < count; ++i)
				{
					m_GameObjects.push_back(static_cast<Container::value_type>(actorShapePairs[i].actor->userData));
				}
			}

		private:
			Container& m_GameObjects;
		};
	} // Physics
} // Helix

#endif // !VOLUMECACHEITERATOR_H
