//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef RIGIDBODYSYSTEM_H
#define RIGIDBODYSYSTEM_H

#include "PhysicsSystem.h"
#include "Datastructures\TplGameObject.h"
#include "Math\XMMath.h"
#include "Math\XMIntegrators.h"

#include <vector>
#include <atomic>
#include <unordered_map>

using namespace DirectX;

namespace Helix
{
	namespace Physics
	{
		struct ForcePoint
		{
			uint32_t m_uVertexID;
			XMFLOAT3 m_vForce;
		};

		typedef std::unordered_multimap<uint32_t, ForcePoint> TRigidBodyForceMap;

		//All time values are in Seconds
		class RigidBodySystem : public PhysicsSystem<Datastructures::TRigidBodies, TRigidBodyForceMap>
		{
		public:
			HDEBUGNAME("RigidBodySystem");

			//don't allow copying
			RigidBodySystem(const RigidBodySystem& rbs) = delete;

			RigidBodySystem(Async::IAsyncObject* _pParent, Async::IAsyncObserver* _Observer);

			~RigidBodySystem();

			//from PhysicsSystem
			virtual void ClearData();
			virtual void ClearExternalData();

			//this is actually the default acceleration => f = m*a will be done internal
			void SetDefaultForce(const XMFLOAT3& _DefaultForce);

			void SetIntegrator(Math::XMIntegrator _Integrator);

		private:
			//from PhysicsSystem
			void Execute();
		private:
			Math::XMIntegrator m_Integrator;
			XMFLOAT3 m_DefaultForce;
		};
	}; // namespace Physics

}

#endif //RIGIDBODYSYSTEM_H