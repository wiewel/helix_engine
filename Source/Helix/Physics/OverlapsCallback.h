//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher, David Labode
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef OVERLAPSCALLBACK_H
#define OVERLAPSCALLBACK_H

#include <PxQueryReport.h>

namespace Helix
{
	namespace Physics
	{
		template <class Container, int Size = 1024>
		struct OverlapCallback : public physx::PxOverlapCallback
		{
			OverlapCallback(Container& _Container) : m_Container(_Container), physx::PxOverlapCallback(m_pBuffer, Size) {}

			inline physx::PxAgain processTouches(const physx::PxOverlapHit* buffer, physx::PxU32 nbHits) final
			{
				for (uint32_t i = 0; i < nbHits; ++i)
				{
					m_Container.push_back(static_cast<Container::value_type>(buffer[i].actor->userData));
				}

				return true;
			}

			physx::PxOverlapHit m_pBuffer[Size];
			Container& m_Container;
		};
	} // Physics
} // Helix

#endif // !OVERLAPSCALLBACK_H
