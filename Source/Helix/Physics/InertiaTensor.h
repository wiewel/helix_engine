//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef INERTIATENSOR_H
#define INERTIATENSOR_H

#include "Math\XMMath.h"
#include "hlx\src\Logger.h"
#include <vector>
#include "Math\MathTypes.h"

namespace Helix
{
	namespace Physics
	{
		using namespace DirectX;
		class InertiaTensor
		{
		public:
			//static Math::float3x3 ComputeMeshInertiaTensor(std::vector<Math::float3>& _Vertices, float _fTotalMass);

			static XMFLOAT3X3 ComputeMeshInertiaTensor(std::vector<XMFLOAT3>& _Vertices, float _fTotalMass);
			static XMFLOAT3X3 ComputeMeshInertiaTensor(std::vector<XMFLOAT3>& _Vertices, std::vector<float>& _Masses);
			static XMFLOAT3X3 ComputeCuboidInertiaTensor(const XMFLOAT3& _vDim, float _fTotalMass);
			static Helix::Math::float3x3 ComputeCuboidInertiaTensor(const Helix::Math::float3& _vDim, float _fTotalMass);
			static XMFLOAT3X3 ComputeSolidSphereInertiaTensor(float _fRadius, float _fTotalMass);
			static XMFLOAT3X3 ComputeHollowSphereInertiaTensor(float _fRadius, float _fTotalMass);
		};
	}
}

#endif