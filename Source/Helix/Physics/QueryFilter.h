//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher, David Labode
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef QUERYFILTER_H
#define QUERYFILTER_H

#include "PxQueryFiltering.h"
#include "Util\Flag.h"
#include "CollisionFilter.h"

namespace Helix
{
	namespace Physics
	{
		enum QueryFilterFlag : uint16_t
		{
			//!< Traverse static shapes
			QueryFilterFlag_Static = (1 << 0),	

			//!< Traverse dynamic shapes
			QueryFilterFlag_Dynamic = (1 << 1),	

			//!< Run the pre-intersection-test filter (see #PxQueryFilterCallback::preFilter())
			QueryFilterFlag_PreFilter = (1 << 2),	

			//!< Run the post-intersection-test filter (see #PxQueryFilterCallback::postFilter())
			QueryFilterFlag_PostFilter = (1 << 3),	

			//!< Abort traversal as soon as any hit is found and return it via callback.block.
			//!< Helps query performance. Both eTOUCH and eBLOCK hitTypes are considered hits with this flag.
			QueryFilterFlag_AnyHit = (1 << 4),	

			//!< All hits are reported as touching. Overrides eBLOCK returned from user filters with eTOUCH.
			QueryFilterFlag_NoBlock = (1 << 5)
		};

		using TQueryFilterFlags = Flag<QueryFilterFlag>;
		struct QueryFilter : physx::PxQueryFilterData
		{
			/** \brief default constructor */
			explicit inline QueryFilter() {}

			/** \brief constructor to set both filter data and filter flags */
			explicit inline QueryFilter(const physx::PxFilterData& fd, physx::PxQueryFlags f) : physx::PxQueryFilterData(fd, f) {}

			/** \brief constructor to set filter flags only */
			explicit inline QueryFilter(physx::PxQueryFlags f) : physx::PxQueryFilterData(f) {}

			explicit inline QueryFilter(QueryFilterFlag f) : physx::PxQueryFilterData(physx::PxQueryFlags(f)) {}

			explicit inline QueryFilter(QueryFilterFlag f, CollisionFilter _Filter) :
				physx::PxQueryFilterData(physx::PxFilterData(_Filter), physx::PxQueryFlags(f))
			{}

			explicit inline QueryFilter(CollisionFilter _Filter) :
				physx::PxQueryFilterData()
			{
				data = physx::PxFilterData(_Filter);
			}

			inline QueryFilter(const TQueryFilterFlags& _Flag) : 
				physx::PxQueryFilterData(physx::PxQueryFlags(_Flag.GetFlag())) {}

			inline QueryFilter(const QueryFilter& _Other)
			{
				data = _Other.data;
				flags = _Other.flags;
				clientId = _Other.clientId;
			}

			inline QueryFilter& operator=(const QueryFilter& _Other)
			{
				data = _Other.data;
				flags = _Other.flags;
				clientId = _Other.clientId;
				return *this;
			}

			inline QueryFilter& operator |=(const physx::PxQueryFlags& f)
			{
				flags |= f;
				return *this;
			}

			inline QueryFilter& operator |=(const QueryFilterFlag& f)
			{
				flags |= physx::PxQueryFlags(f);
				return *this;
			}

			inline QueryFilter& operator |=(const TQueryFilterFlags& f)
			{
				flags |= physx::PxQueryFlags(f.GetFlag());
				return *this;
			}

			inline QueryFilter& operator |= (const CollisionFilter& _Other)
			{
				data.word0 |= _Other.word0;
				data.word1 |= _Other.word1;
				data.word2 |= _Other.word2;
				data.word3 |= _Other.word3;
				return *this;
			}
			inline QueryFilter operator | (const CollisionFilter& _Other)
			{
				QueryFilter filter;
				filter.data.word0 = data.word0 | _Other.word0;
				filter.data.word1 = data.word1 | _Other.word1;
				filter.data.word2 = data.word2 | _Other.word2;
				filter.data.word3 = data.word3 | _Other.word3;
				return filter;
			}

			inline QueryFilter& operator |= (const QueryFilter& _Other)
			{
				data.word0 |= _Other.data.word0;
				data.word1 |= _Other.data.word1;
				data.word2 |= _Other.data.word2;
				data.word3 |= _Other.data.word3;

				return *this;
			}

			inline QueryFilter operator | (const QueryFilter& _Other)
			{
				QueryFilter filter;

				filter.data.word0 = data.word0 | _Other.data.word0;
				filter.data.word1 = data.word1 | _Other.data.word1;
				filter.data.word2 = data.word2 | _Other.data.word2;
				filter.data.word3 = data.word3 | _Other.data.word3;

				return filter;
			}

			template <class T>
			inline QueryFilter& operator |= (const Flag<T>& _Other)
			{
				data.word0 |= static_cast<uint32_t>(_Other.GetFlag());
				return *this;
			}

			template <class T>
			inline QueryFilter operator | (const Flag<T>& _Other)
			{
				QueryFilter filter;
				filter.data.word0 |= static_cast<uint32_t>(_Other.GetFlag());
				return filter;
			}

			template <class EnumFlag>
			inline QueryFilter& operator |= (const EnumFlag& _Other)
			{
				data.word0 |= static_cast<uint32_t>(_Other);
				return *this;
			}

			template <class EnumFlag>
			inline QueryFilter operator | (const EnumFlag& _Other)
			{
				QueryFilter filter;
				filter.data.word0 |= static_cast<uint32_t>(_Other);
				return filter;
			}
		};
	} // Physics
} // Helix

#endif // !QUERYFILTER_H
