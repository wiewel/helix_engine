//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher, David Labode
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef GEOMETRYSHAPEFACTORY_H
#define GEOMETRYSHAPEFACTORY_H

#include "GeometryDescriptions.h"
#include "GeometryTypes.h"
#include "hlx\src\Singleton.h"
#include "Async\SpinLock.h"
#include "Resources\H3DFile.h"
#include <concurrent_unordered_map.h>
#include <unordered_map>

namespace Helix
{
	namespace Physics
	{
		// crc checksum of data -> convex mesh
		using TConvexMeshPool = std::unordered_map<uint32_t, ConvexMesh*>;
		using TSurfaceMaterialPool = concurrency::concurrent_unordered_map<uint64_t, SurfaceMaterial*>;


		class GeometryShapeFactory : public hlx::Singleton<GeometryShapeFactory>
		{
		public:
			GeometryShapeFactory();
			~GeometryShapeFactory();

			Shape* CreateShape(const GeometryDesc& _Desc);

			bool AddConvexMesh(const Resources::ConvexMeshCluster& _ConvexMeshCluster);

			void ReleaseConvexMesh(const uint32_t& _uChecksum);

		private:
			SurfaceMaterial* CreateMaterial(const SurfaceMaterialDesc& _Desc);

		private:
			// name hash -> mesh ptr
			TConvexMeshPool m_ConvexMeshPool;

			// desc hash -> material ptr
			TSurfaceMaterialPool m_SurfaceMaterialPool;

			Async::SpinLock m_ConvexMeshLock;
		};
	}
}

#endif // !GEOMETRYSHAPEFACTORY_H

