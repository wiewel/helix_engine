//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher, David Labode
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "PhysicsFactory.h"
#include "PxPhysics.h"
#include "Math\MathTypes.h"
#include "Util\Functional.h"

using namespace Helix::Physics;
using namespace physx;
//----------------------------------------------------------------------------------------------------------------------------

PhysicsFactory::PhysicsFactory()
{
	m_ToleranceScale.length = 1.f; // 1 Unit = 1 meter
	m_ToleranceScale.speed = 9.81f; // default speed of an object falling under gravity for 1s
	m_ToleranceScale.mass = 1000.f; // The approximate mass of a length * length * length block.
}
//----------------------------------------------------------------------------------------------------------------------------

PhysicsFactory::~PhysicsFactory()
{
	//safe_release(m_pCooking);
	HSAFE_FUNC_RELEASE(m_pCooking, release);

	HSAFE_FUNC_RELEASE(m_pCPUDispatcher, release);

	if (m_pCUDAContextManager != nullptr)
	{
		if(m_pCUDAContextManager->contextIsValid())
		{
			m_pCUDAContextManager->releaseContext();
		}
		m_pCUDAContextManager->release();
	}

	if (mpPhysics != nullptr)
	{
		PxCloseExtensions();
		HSAFE_FUNC_RELEASE(mpPhysics, release);
	}

	HSAFE_FUNC_RELEASE(m_pProfileZoneManager, release);

	HSAFE_FUNC_RELEASE(m_pFoundation, release);
}
//----------------------------------------------------------------------------------------------------------------------------

bool PhysicsFactory::Initialize(bool _bCreatePhysics, bool _bRecordMemoryAllocations, bool _bCreatCookingSDK)
{
	m_PhysXLogger.SetMaxErrorLevel(hlx::kMessageType_Error);

#ifdef HNUCLEO
	m_AssertHandler.SetIgnoreAssert(true);
#endif

	physx::PxSetAssertHandler(m_AssertHandler);
	// create foundation
	if (m_pFoundation == nullptr)
	{
		m_pFoundation = PxCreateFoundation(PX_PHYSICS_VERSION, m_DefaultAllocatorCallback, m_PhysXLogger);
	}

	if (m_pFoundation == nullptr)
	{
		HFATAL("Failed to create PhysX::Foundation!");
		return false;
	}

	// create profile manager
	if (m_pProfileZoneManager == nullptr)
	{
		m_pProfileZoneManager = &PxProfileZoneManager::createProfileZoneManager(m_pFoundation);

		if (m_pProfileZoneManager == nullptr)
		{
			HFATAL("Failed to create PhysX::ProfileZoneManager!");
			return false;
		}
	}

	// create physics
	if (mpPhysics == nullptr && _bCreatePhysics)
	{
		mpPhysics = PxCreatePhysics(PX_PHYSICS_VERSION, *m_pFoundation, m_ToleranceScale, _bRecordMemoryAllocations, m_pProfileZoneManager);

		if (mpPhysics != nullptr)
		{
			if (PxInitExtensions(*mpPhysics) == false)
			{
				HFATAL("Failed to init PhysX::Extensions!");
				return false;
			}
		}
	}

	if (mpPhysics == nullptr && _bCreatePhysics)
	{
		HFATAL("Failed to create PhysX::Physics!");
		return false;
	}

	if (m_pCPUDispatcher == nullptr)	
	{	
		m_pCPUDispatcher = PxDefaultCpuDispatcherCreate(std::max(std::thread::hardware_concurrency(), 4u));
		
		if (m_pCPUDispatcher == nullptr)
		{
			HFATAL("Failed to create PhysX::CPUDispatcher!");
			return false;
		}
	}

#if PX_SUPPORT_GPU_PHYSX
	if (m_pCUDAContextManager == nullptr)
	{
		PxCudaContextManagerDesc cudaContextManagerDesc = {};
		m_pCUDAContextManager = PxCreateCudaContextManager(*m_pFoundation, cudaContextManagerDesc, m_pProfileZoneManager);
	}

	if (m_pCUDAContextManager != nullptr)
	{
		if (m_pCUDAContextManager->contextIsValid() == false)
		{
			HFATAL("Failed to create PhysX::CUDAContext!");
			m_pCUDAContextManager->release();
			m_pCUDAContextManager = nullptr;
			return false;
		}

		m_pCUDAContextManager->getGpuDispatcher();
	}
	else
	{
		HFATAL("Failed to create PhysX::CUDAContextManager!");
		return false;
	}
#endif

	if (m_pCooking == nullptr && _bCreatCookingSDK)
	{
		physx::PxCookingParams Params(m_ToleranceScale);
		//Params.skinWidth
		m_pCooking = PxCreateCooking(PX_PHYSICS_VERSION, *m_pFoundation, Params);
		if (m_pCooking == nullptr)
		{
			HFATAL("Failed to initialize CookingSDK!");
			return false;
		}
	}

	HLOG("PhysX %u.%u.%u successfully initialized", PX_PHYSICS_VERSION_MAJOR, PX_PHYSICS_VERSION_MINOR, PX_PHYSICS_VERSION_BUGFIX);

	return true;
}
//----------------------------------------------------------------------------------------------------------------------------
