//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "RigidBodySystem.h"
#include "Util\Logger.h"
#include "Util\HelixDefines.h"
#include "Util\Flag.h"
#include "Util\Profiler.h"

using namespace Helix::Math;
using namespace Helix::Async;
using namespace Helix::Physics;
using namespace Helix::Datastructures;

//---------------------------------------------------------------------------------------------------
RigidBodySystem::RigidBodySystem(IAsyncObject* _pParent, IAsyncObserver* _pObserver) : PhysicsSystem<TRigidBodies, TRigidBodyForceMap>(_pParent, _pObserver)
{
	m_Integrator = XMIntegrator_Heun;
	m_DefaultForce = XMFLOAT3_ZERO;
}
//---------------------------------------------------------------------------------------------------
RigidBodySystem::~RigidBodySystem()
{

}
//---------------------------------------------------------------------------------------------------
//run simulation step here
void RigidBodySystem::Execute()
{
	//HLOGD(TEXT("Simulate %f"), m_fDeltaTime);
	//add external forces here

	HPROFILE(RigidBodySimulation)

	XMFLOAT3 XI[8];
	XMVECTOR F = XMLoadFloat3(&m_DefaultForce);

	//TODO: evenly distribute the force over all vertices by dividing it by the total number of vertices in the Physics BV
	XMVECTOR fd = XMVectorScale(F, 1.f / 8.f);

	XMVECTOR xi;
	XMVECTOR fi;
	XMVECTOR q; // torque
	
	float fMotionBias = std::powf(0.7f, m_fDeltaTime);

	for (Datastructures::RigidBodyIterator itr = m_Data.begin; itr != m_Data.end; ++itr)
	{
		Datastructures::TplGameObject& GO = *itr;
		Datastructures::RigidBodyData* const pRB = GO.GetRigidBodyData();
		HASSERTD(pRB != nullptr, "GameObject is not of type RigidBody!");
		if (itr->CheckFlag(TplGameObjectFlag_Static))
		{
			pRB->m_vLinearVelocity = XMFLOAT3_ZERO;
			pRB->m_vAngularMomentum = XMFLOAT3_ZERO;
			pRB->m_vAngularVelocity = XMFLOAT3_ZERO;
			continue;
		}
		XMVECTOR XWorld = XMLoadFloat3(&GO.m_vPosition);

		q = XMVectorZero();
		F = XMLoadFloat3(&m_DefaultForce);

		GO.GetOBB().GetCorners(XI);
		for (uint32_t i = 0; i < BoxIndices_NumOfEdges; ++i)
		{
			xi = XMLoadFloat3(&XI[i]);
			xi = XMVectorSubtract(xi, XWorld);

			//q += xi x fd
			q = XMVectorAdd(q, XMVector3Cross(xi, fd));
		}
		
		std::pair<TRigidBodyForceMap::iterator, TRigidBodyForceMap::iterator> p = m_ExternalData.equal_range(GO.GetID());
		//iterate over all forces for a specific rigidbody
		for (; p.first != p.second; ++p.first)
		{
			const ForcePoint& fp = p.first->second;
			fi = XMLoadFloat3(&fp.m_vForce);
			xi = XMLoadFloat3(&XI[fp.m_uVertexID]);
			xi = XMVectorSubtract(xi, XWorld);

			//q += xi x fi
			q = XMVectorAdd(q, XMVector3Cross(xi, fi));

			//accumulate total force for rigidbody
			F = XMVectorAdd(F, fi);
		}
		XMVECTOR vAppliedForce = XMVectorSubtract(F, XMLoadFloat3(&m_DefaultForce));
		// fCurrentMotion actually holds the current impulse
		float fCurrentMotion = XMVectorGetX(XMVectorAdd(XMVector3Dot(q, q), XMVector3Dot(vAppliedForce, vAppliedForce))) * m_fDeltaTime;
		if (!itr->TryWake(fCurrentMotion))
		{
			// RB is sleeping
			continue;
		}

		XMVECTOR x = XMLoadFloat3(&GO.m_vPosition);
		XMVECTOR v = XMLoadFloat3(&pRB->m_vLinearVelocity);
		XMVECTOR a = XMVectorScale(F, pRB->m_fInvMass);
		XMVECTOR v_prev = v;
		XMVECTOR r = XMLoadFloat4(&GO.m_vOrientation);
		XMVECTOR L = XMLoadFloat3(&pRB->m_vAngularMomentum);
		XMVECTOR w = XMLoadFloat3(&pRB->m_vAngularVelocity);
		XMVECTOR w_prev = w;

		XMVectorSetW(w, 0.f); // (w,0)

		float fDamping = pRB->m_fDamping;

		switch (m_Integrator)
		{
		case XMIntegrator_Euler:
			//v += h*(F/M)
			//x += h*v
			XMVectorEuler(x, v, a, fDamping, m_fDeltaTime);
			break;
		case XMIntegrator_Heun:
			XMVectorHeun(x, v, a, fDamping, m_fDeltaTime);
			break;
		case XMIntegrator_RK2:
			XMVectorRungeKutta2(x, v, a, fDamping, m_fDeltaTime);
			break;
		case XMIntegrator_RK3:
			XMVectorRungeKutta3(x, v, a, fDamping, m_fDeltaTime);
			break;
		case XMIntegrator_RK4:
		default:
			XMVectorRungeKutta4(x, v, a, fDamping, m_fDeltaTime);
		}

		XMStoreFloat3(&GO.m_vPosition, x);
		XMStoreFloat3(&pRB->m_vLinearVelocity, v);
		XMStoreFloat3(&pRB->m_vDeltaLinearVelocity, XMVectorSubtract(v, v_prev));

		//r' = (w,0) * r
		w = XMQuaternionMultiply(w, r);

		//r += h/2 * r'
		r = Math::XMVectorEuler(r, w, m_fDeltaTime * 0.5f);
		r = XMQuaternionNormalize(r);
		XMStoreFloat4(&GO.m_vOrientation, r);

		// L += h*q
		L = Math::XMVectorEuler(L, q, m_fDeltaTime * fDamping);
		XMStoreFloat3(&pRB->m_vAngularMomentum, L);

		XMMATRIX I = XMLoadFloat3x3(&pRB->m_mInvInertiaTensor);
		XMMATRIX R = XMMatrixRotationQuaternion(r);
		XMStoreFloat3x3(&GO.m_mRotation, R);

		XMMATRIX Rt = XMMatrixTranspose(R);
		// R^T * I * R
		I = XMMatrixMultiply(Rt, I);
		I = XMMatrixMultiply(I, R);

		w = XMVector3Transform(L, I);
		XMStoreFloat3(&pRB->m_vAngularVelocity, w);
		XMStoreFloat3(&pRB->m_vDeltaAngularVelocity, XMVectorSubtract(w, w_prev));
		// sleep 
		if (!itr->CheckFlag(TplGameObjectFlag_NoSleep))
		{
			fCurrentMotion = fabsf(XMVectorGetX(XMVectorAdd(XMVector3Dot(v, v), XMVector3Dot(w, w))));
			itr->TrySleep(fMotionBias * pRB->m_fMotion + (1.0f - fMotionBias) * fCurrentMotion);
		}

		GO.UpdateBoundingVolumes();
	}

	m_ExternalData.clear();

	HPROFILE_END(RigidBodySimulation);
}
//---------------------------------------------------------------------------------------------------
void RigidBodySystem::ClearData()
{
	/*std::lock_guard<std::mutex> lock(m_LockMutex);
	m_Data.resize(0);*/
}
//---------------------------------------------------------------------------------------------------
void RigidBodySystem::ClearExternalData()
{
	//std::lock_guard<std::mutex> lock(m_LockMutex);
	m_ExternalData.clear();
}
//---------------------------------------------------------------------------------------------------
void RigidBodySystem::SetDefaultForce(const XMFLOAT3& _DefaultForce)
{
	//std::lock_guard<std::mutex> lock(m_LockMutex);
	m_DefaultForce = _DefaultForce;
}
//---------------------------------------------------------------------------------------------------
void RigidBodySystem::SetIntegrator(XMIntegrator _Integrator)
{
	//std::lock_guard<std::mutex> lock(m_LockMutex);
	m_Integrator = _Integrator;
}
//---------------------------------------------------------------------------------------------------
