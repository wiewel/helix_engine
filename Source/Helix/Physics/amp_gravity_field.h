//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher, David Labode
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// AMP methods used in GravityField class
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef AMP_GRAVITY_FIELD_H
#define AMP_GRAVITY_FIELD_H
#include "AMP\AMPDefines.h"
#include "AMP\AMPTypes.h"

namespace Concurrency
{
	void gravity_step(const float3& _p, float3& _v, const float _m, array<float4>& _GravParticles, const float _h, const float _fMinSqDistance, const float _fMaxSqDistance) restrict(amp);
	float3 gravity_force(const float3& _p, const float _m, float4(&_GravParticles)[32], const float _fMinSqDistance, const float _fMaxSqDistance) restrict(amp);
	float3 spawn_point(const float4& _SpawnSphere, const float _fTotalTime) restrict(amp);

}

void Concurrency::gravity_step(const float3& _p, float3& _v, const float _m, array<float4>& _GravParticles, const float _h, const float _fMinSqDistance, const float _fMaxSqDistance) restrict(amp)
{
	float4 GravityParticles[32];
	// Load gravity particles from global memory
	for (uint i = 0; i < 32; ++i)
	{
		GravityParticles[i] = _GravParticles[i];
	}

	float3 k1 = gravity_force(_p, _m, GravityParticles, _fMinSqDistance, _fMaxSqDistance);

	float3 k2 = gravity_force(_p + (_h / 2.0f) * _v, _m, GravityParticles, _fMinSqDistance, _fMaxSqDistance);

	float3 k3 = gravity_force(_p + _h * _v, _m, GravityParticles, _fMinSqDistance, _fMaxSqDistance);

	_v += (_h / 6.0f) * (k1 + 4.0f * k2 + k3);
}
//--------------------------------------------------------------------------------------------------------------------
Concurrency::float3 Concurrency::gravity_force(const float3& _p, const float _m, float4(&_GravParticles)[32], const float _fMinSqDistance, const float _fMaxSqDistance) restrict(amp)
{
	float3 vForce = HMFLOAT3_ZERO;
	for (uint32_t i = 0u; i < 32u; ++i)
	{
		float3 vDirection = _GravParticles[i].xyz - _p;
		float fDistanceSq = vDirection.LengthSquare();
		if (fDistanceSq > 0)
		{
			// normalization of a null vector is undefined
			vDirection.Normalize();
			if (fDistanceSq < _fMinSqDistance)
			{
				fDistanceSq = _fMinSqDistance;
			}
			if (fDistanceSq <= _fMaxSqDistance)
			{
				vForce += vDirection * (66.7f * _m * _GravParticles[i].w / fDistanceSq);
			}
		}
	}
	return vForce;
}
//--------------------------------------------------------------------------------------------------------------------
Concurrency::float3 Concurrency::spawn_point(const float4& _SpawnSphere, const float _fTotalTime) restrict(amp)
{
	// TODO
	return HMFLOAT3_ZERO;
}
#endif // !AMP_GRAVITY_FIELD_H
