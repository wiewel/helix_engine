//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher, David Labode
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef GEOMETRYTYPES_H
#define GEOMETRYTYPES_H

#include "foundation\PxBounds3.h"
#include "geometry\PxBoxGeometry.h"
#include "geometry\PxSphereGeometry.h"
#include "geometry\PxCapsuleGeometry.h"
#include "geometry\PxConvexMeshGeometry.h"
#include "geometry\PxTriangleMeshGeometry.h"
#include "geometry\PxHeightFieldGeometry.h"
#include "geometry\PxPlaneGeometry.h"
#include "geometry\PxGeometryQuery.h"
#include "PxMaterial.h"
#include "PxShape.h"

namespace Helix
{
	namespace Physics
	{
		// primitives
		using AABB = physx::PxBounds3;
		using SurfaceMaterial = physx::PxMaterial;

		// Geometry types
		using GeoBase = physx::PxGeometry;
		using GeoBox = physx::PxBoxGeometry;
		using GeoSphere = physx::PxSphereGeometry;
		using GeoCapsule = physx::PxCapsuleGeometry;
		using GeoPlane = physx::PxPlaneGeometry;
		using GeoConvexMesh = physx::PxConvexMeshGeometry;

		// mesh types
		using ConvexMesh = physx::PxConvexMesh;

		// shapes
		using Shape = physx::PxShape;
	};
};

#endif // !GEOMETRYTYPES_H
