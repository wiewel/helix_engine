//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "ControllerComponent.h"
#include "DataStructures\GameObject.h"
#include "DataStructures\ShapeComponent.h"

using namespace Helix;
using namespace Helix::Physics;
using namespace Helix::Math;
using namespace physx;
//===================================================================================================
//---------------------------------------------------------------------------------------------------
ControllerComponent::ControllerComponent(Datastructures::GameObject * _pParent) :
	IGameObjectComponent(_pParent,Datastructures::ComponentCallback_OnUpdate)
{
	_pParent->SetKinematic(true);
}
//---------------------------------------------------------------------------------------------------
Helix::Physics::ControllerComponent::ControllerComponent(Datastructures::GameObject * _pParent, CapsuleControllerDesc & _Desc)
	: ControllerComponent(_pParent)
{
	CreateBody(_Desc);
}
//---------------------------------------------------------------------------------------------------
Helix::Physics::ControllerComponent::ControllerComponent(Datastructures::GameObject * _pParent, BoxControllerDesc & _Desc)
	: ControllerComponent(_pParent)
{
	CreateBody(_Desc);
}
//---------------------------------------------------------------------------------------------------
ControllerComponent::~ControllerComponent()
{
	Async::ScopedSpinLock Lock(m_ControllerLock);
}
//---------------------------------------------------------------------------------------------------
void ControllerComponent::OnUpdate(double _fDeltaTime, double _fTotalTime)
{
	Async::ScopedSpinLock ControllerLock(m_ControllerLock);
	Async::ScopedSpinLock MovementLock(m_MovementLock);

	// determine if the controller movement should be constrained by a physics character controller
	if (m_bClipping && m_pController != nullptr /*&& Scene::PhysicsScene::Instance()->IsPaused() == false*/)
	{
		m_bGravityEnabled = true;
		PxControllerCollisionFlags Flags;

		// apply gravity
		m_fVelocity += m_fForce * static_cast<float>(_fDeltaTime);
		m_vDisplacement.y += m_fVelocity * static_cast<float>(_fDeltaTime);
		// move the hidden actor (physics scene must be write locked)
		{
			Physics::SceneWriteLock PhysicsLock;
			Flags = m_pController->move(m_vDisplacement, 0.01f, static_cast<float>(_fDeltaTime), PxControllerFilters(nullptr, &m_QueryFilterCallback));
		}
		// the actor's feet are on the ground
		if (Flags.isSet(PxControllerCollisionFlag::eCOLLISION_DOWN))
		{
			// we don't to apply gravity as long as the actor is not in the air
			m_fVelocity = 0.0f;
			m_fForce = 0.0f;
			if (m_bIsJumping)
			{
				m_bIsJumping = false;
			}
		}
		else
		{
			// the actor is in the air, so we need to apply gravity
			m_fForce = m_bGravityEnabled ? Scene::PhysicsScene::Instance()->GetGravity().y : 0.0f;
		}

		// the controller is moved while beeing constrained by the hidden physics actor
		GetParent()->SetPosition(m_pController->getPosition());
	}
	else
	{
		m_bGravityEnabled = false;
		// the controller is moved without beeing constrained by the hidden physics actor
		Teleport(GetParent()->GetPosition() + m_vDisplacement);
	}

	// clear the displacement
	m_vDisplacement = float3(0, 0, 0);
}
//---------------------------------------------------------------------------------------------------
void ControllerComponent::CreateBody(CapsuleControllerDesc & _ControllerDesc)
{
	Async::ScopedSpinLock Lock(m_ControllerLock);

	PxCapsuleControllerDesc Desc;
	Desc.radius = _ControllerDesc.fRadius;
	Desc.height = _ControllerDesc.fHeight;
	Desc.position.x = static_cast<PxExtended>(_ControllerDesc.vPosition.x);
	Desc.position.y = static_cast<PxExtended>(_ControllerDesc.vPosition.y);
	Desc.position.z = static_cast<PxExtended>(_ControllerDesc.vPosition.z);
	Desc.upDirection = _ControllerDesc.vUpDirection;
	Desc.slopeLimit = _ControllerDesc.fSlopeLimit;
	Desc.invisibleWallHeight = _ControllerDesc.fInvisibleWallHeight;
	Desc.maxJumpHeight = _ControllerDesc.fMaxJumpHeight;
	Desc.contactOffset = _ControllerDesc.fContactOffset;
	Desc.stepOffset = _ControllerDesc.fStepOffset;
	Desc.material = Scene::PhysicsScene::Instance()->GetPhysics()->createMaterial(0.2f, 0.2f, 0.0125f);

	CreateCharacterController(Desc);
}
//---------------------------------------------------------------------------------------------------
void ControllerComponent::CreateBody(BoxControllerDesc & _ControllerDesc)
{
	Async::ScopedSpinLock Lock(m_ControllerLock);

	PxBoxControllerDesc Desc;
	Desc.halfHeight = _ControllerDesc.fHalfHeight;
	Desc.halfForwardExtent = _ControllerDesc.fHalfForwardExtent;
	Desc.halfSideExtent = _ControllerDesc.fHalfSideExtent;
	Desc.position.x = static_cast<PxExtended>(_ControllerDesc.vPosition.x);
	Desc.position.y = static_cast<PxExtended>(_ControllerDesc.vPosition.y);
	Desc.position.z = static_cast<PxExtended>(_ControllerDesc.vPosition.z);
	Desc.upDirection = _ControllerDesc.vUpDirection;
	Desc.slopeLimit = _ControllerDesc.fSlopeLimit;
	Desc.invisibleWallHeight = _ControllerDesc.fInvisibleWallHeight;
	Desc.maxJumpHeight = _ControllerDesc.fMaxJumpHeight;
	Desc.contactOffset = _ControllerDesc.fContactOffset;
	Desc.stepOffset = _ControllerDesc.fStepOffset;
	Desc.material = Scene::PhysicsScene::Instance()->GetPhysics()->createMaterial(0.2f, 0.2f, 0.0125f);

	CreateCharacterController(Desc);
}
//---------------------------------------------------------------------------------------------------
void Helix::Physics::ControllerComponent::DestroyBody()
{
	if (m_pController != nullptr)
	{
		m_pController->release();
		m_pController = nullptr;
	}
}
//---------------------------------------------------------------------------------------------------
void ControllerComponent::CreateCharacterController(PxControllerDesc & _Desc)
{
	PxControllerManager* pManager = Scene::PhysicsScene::Instance()->GetControllerManager();
	if (pManager != nullptr)
	{
		HSAFE_FUNC_RELEASE(m_pController, release);
		m_pController = pManager->createController(_Desc);
		
		PxRigidActor* pActor = m_pController->getActor();
		// set the actor user data to the parent game object in order to avoid checking for nullpointers in all user data
		Datastructures::GameObject* pParent = GetParent();
		pActor->userData = pParent;
		std::string sName = "Character Controller of " + pParent->GetName();
		pActor->setName(sName.c_str());
		m_QueryFilterCallback.SetParent(pParent);

		// ensure a valid shape user data 
		const uint32_t uNumShapes = pActor->getNbShapes();
		std::vector<PxShape*> Shapes(uNumShapes);
		pActor->getShapes(Shapes.data(), uNumShapes);
		for (auto Shape : Shapes)
		{
			// if user data is nullptr, the shape will be ignored in simulation events
			Shape->userData = pParent;
		}
	}
}
//---------------------------------------------------------------------------------------------------
// move forward/ backward
void ControllerComponent::Walk(const float _fDistance)
{
	Async::ScopedSpinLock Lock(m_MovementLock);
	
	if (m_bGravityEnabled)
	{
		// character is not flying -> altitude should not influence walking
		float3 vEquatorLook = GetRight().cross(float3(0,1,0));
		m_vDisplacement += _fDistance * vEquatorLook;
	}
	else
	{
		m_vDisplacement += _fDistance * GetLook();
	}
}
//---------------------------------------------------------------------------------------------------
void ControllerComponent::Teleport(const float3 _vPosition)
{
	// move the hidden actor (physics scene must be write locked)
	if(m_pController != nullptr)
	{
		Physics::SceneWriteLock PhysicsLock;
		// thanks NVIDIA
		physx::PxExtendedVec3 ExtPosition;
		ExtPosition.x = _vPosition.x;
		ExtPosition.y = _vPosition.y;
		ExtPosition.z = _vPosition.z;
		m_pController->setPosition(ExtPosition);
		m_vDisplacement = Math::float3(0,0,0);
	}
	GetParent()->SetPosition(_vPosition);
}
//---------------------------------------------------------------------------------------------------
// move up/ down
void ControllerComponent::Elevate(const float _fDistance)
{
	Async::ScopedSpinLock Lock(m_MovementLock);
	m_vDisplacement.y += _fDistance;
}
//---------------------------------------------------------------------------------------------------
// move left/ right
void ControllerComponent::Strafe(const float _fDistance)
{
	Async::ScopedSpinLock Lock(m_MovementLock);
	m_vDisplacement += _fDistance * GetRight();
}
//---------------------------------------------------------------------------------------------------
void ControllerComponent::Jump(const float _fVelocity)
{
	if (m_bIsJumping == false)
	{
		Async::ScopedSpinLock Lock(m_MovementLock);
		m_fVelocity += _fVelocity;
		m_bIsJumping = true;
	}
}
//---------------------------------------------------------------------------------------------------
// Turn the gameobject around world up
void ControllerComponent::Turn(const float _fDegrees)
{
	quaternion qTurn(DegToRad(_fDegrees), float3(0, 1, 0));
	GetParent()->Rotate(qTurn, false);
}
//---------------------------------------------------------------------------------------------------
// Lower and heighten sight around local right
void ControllerComponent::Nod(const float _fDegrees)
{
	float fRadians = DegToRad(_fDegrees);
	constexpr float fMargin = 3.14159f / 8.0f;
	if (fRadians < 0.0f)
	{
		float fAngleToZenith = -(acosf(GetLook().dot(float3(0, 1, 0))) - fMargin);
		fRadians = max(fRadians, fAngleToZenith);
	}
	else
	{
		float fAngleToZenith = acosf(GetLook().dot(float3(0, -1, 0))) - fMargin;
		fRadians = min(fRadians, fAngleToZenith);
	}
	quaternion qTurn(fRadians, float3(1,0,0));
	GetParent()->Rotate(qTurn);
}
//---------------------------------------------------------------------------------------------------
void ControllerComponent::LookAt(const float3 & _vPosition)
{
	float3 vV = _vPosition - GetParent()->GetPosition();
	float3 vU = GetLook();

	float fAngle = atan2f(vV.x, vV.z) - atan2f(vU.x, vU.z);
	float3 vAxis = Math::float3(0, 1, 0);
	quaternion Rotation(fAngle, vAxis);
	if (Rotation.isSane())
	{
		GetParent()->Rotate(Rotation, false);
	}
}
