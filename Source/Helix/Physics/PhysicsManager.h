//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef PHYSICSMANAGER_H
#define PHYSICSMANAGER_H

#include "Debug.h"
#include "AsyncObserver.h"
#include "AsyncObject.h"
#include "Timer.h"
#include "Singleton.h"
#include "UniqueID.h"

#include <atomic>
#include <memory>

#include "RigidBodySystem.h"

#include "GameObjectContainer.h"
#include "TplGameObjectContainer.h"

namespace helix
{
	namespace physics
	{
		class PhysicsManager : public async::IAsyncObserver, public async::IAsyncObject, public TSingleton<PhysicsManager>
		{
		public:
			HDEBUGNAME("PhysicsManager");

			PhysicsManager();
			~PhysicsManager();

			//set desired update interval (deltatime), this is a recommdation for the computation time spent on simulations
			void SetUpdateInterval(double _fUpdateInterval);
			//returns the actual computation time for the last simulation interval
			double GetDeltaTime();
			//if this is set to true the simulations will use the exact UpdateInterval set with the SetUpdateInterval, independant from the computation time.
			void SetFixedUpdateInterval(bool _bFixedUpdateInterval);
			//unlocks free computation interval, simulations will run as fast as possible
			void SetFreeUpdateInterval(bool _bFreeUpdateInterval);

			void SetDefaultForce(const XMFLOAT3& _DefaultForce);

			void AddRigidBodyForces(TRigidBodyForceMap& _Forces);

			void GetVisibleObjects(_Out_ std::vector<datastructures::GameObject>& _VisibleObjects, uint64_t& _uVersionID);

		private:
			virtual void Execute();

			virtual void StopSubsystems();

			virtual void InitializeSubsystems();
		private:

			UniqueID<uint32_t> m_UniqueID;
			//synchronisation
			uint32_t m_uTotalNotifyCount = 1; // rigidbody system
			uint64_t m_uNumOfSimulations = 0;
			uint64_t m_uNumOfResumes = 0;

			Timer m_Timer;
			double m_fUpdateInterval = 0.02;
			double m_fDeltaTime;
			bool m_bFreeUpdateInterval = false;
			bool m_bFixedUpdateInterval = false;

			datastructures::GameObjectContainer m_GameObjects;

			std::mutex m_LockMutex;

			std::mutex m_WaitMutex;
			std::condition_variable m_CV;

			datastructures::TplGameObjectContainer* const m_pTplGameObjects;

			std::vector<collision::CollisionInfo> m_Collisions;

			RigidBodySystem m_RigidBodySystem;
		};
	}; // namespace physics

}; // namespace helix

#endif