//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher, David Labode
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "GeometryShapeFactory.h"
#include "PxPhysics.h"
#include "Util\Functional.h"
#include "PhysXByteStream.h"
#include "PhysicsFactory.h"
//#include "Util\HashedString.h"
#include "Math\MathFunctions.h"
#include "Util\BaseHash.h"

using namespace Helix::Resources;
using namespace Helix::Physics;
//---------------------------------------------------------------------------------------------------------------------

GeometryShapeFactory::GeometryShapeFactory()
{
}
//---------------------------------------------------------------------------------------------------------------------

GeometryShapeFactory::~GeometryShapeFactory()
{
	for (const auto& kv : m_ConvexMeshPool)
	{
		kv.second->release();
	}

	for (const auto& kv : m_SurfaceMaterialPool)
	{
		kv.second->release();
	}
}
//---------------------------------------------------------------------------------------------------------------------

physx::PxShape* GeometryShapeFactory::CreateShape(const GeometryDesc& _Desc)
{
	physx::PxPhysics* pPhysics = PhysicsFactory::Instance()->GetPhysics();

	Shape* pShape = nullptr;
	float fMaxScale = max(_Desc.vScale.x, _Desc.vScale.y, _Desc.vScale.z);
	if (fMaxScale == 0.f)
	{
		HERRORD("Invalid shape scale!");
		fMaxScale = 1.f;
	}

	// create material
	SurfaceMaterial* pMaterial = CreateMaterial(_Desc.Material);
	if (pMaterial == nullptr)
	{
		return nullptr;
	}

	physx::PxShapeFlags kFlags = physx::PxShapeFlag::eSCENE_QUERY_SHAPE;

	switch (_Desc.kUsage)
	{
	case ShapeUsageType_Query:
		kFlags = physx::PxShapeFlag::eSCENE_QUERY_SHAPE;
		break;
	case ShapeUsageType_Simulation:
		kFlags = physx::PxShapeFlag::eSIMULATION_SHAPE;
		break;
	case ShapeUsageType_Trigger:
		kFlags = physx::PxShapeFlag::eTRIGGER_SHAPE;
		break;
	default:
		break;
	}

	switch (_Desc.kType)
	{
	case GeometryType_Sphere:

#ifdef _DEBUG
		if (_Desc.Sphere.fRadius == 0.f)
		{
			HERROR("Invalid sphere radius!");
		}
#endif

		pShape = pPhysics->createShape(physx::PxSphereGeometry((_Desc.Sphere.fRadius != 0.f ? _Desc.Sphere.fRadius : 1.f) * fMaxScale), *pMaterial, true, kFlags);
		break;
	case GeometryType_Plane:
		pShape = pPhysics->createShape(physx::PxPlaneGeometry(), *pMaterial, true, kFlags);
		break;
	case GeometryType_Capsule:

#ifdef _DEBUG
		if (_Desc.Capsule.fRadius == 0.f || _Desc.Capsule.fHalfHeight == 0.f)
		{
			HERROR("Invalid capsule radius/height!");
		}
#endif

		pShape = pPhysics->createShape(GeoCapsule(
			(_Desc.Capsule.fRadius != 0.f ? _Desc.Capsule.fRadius : 1.f) * fMaxScale,
			(_Desc.Capsule.fHalfHeight != 0.f ? _Desc.Capsule.fHalfHeight : 1.f) * fMaxScale),
			*pMaterial, true, kFlags);
		break;
	case GeometryType_Box:
		{
			// ensure valid box
			BoxDesc Box;

			Box.vHalfExtents.x = _Desc.Box.vHalfExtents.x != 0.f ? _Desc.Box.vHalfExtents.x : 1.f;
			Box.vHalfExtents.y = _Desc.Box.vHalfExtents.y != 0.f ? _Desc.Box.vHalfExtents.y : 1.f;
			Box.vHalfExtents.z = _Desc.Box.vHalfExtents.z != 0.f ? _Desc.Box.vHalfExtents.z : 1.f;

#ifdef _DEBUG
			if (Box.vHalfExtents != _Desc.Box.vHalfExtents)
			{
				HERROR("Invalid Box extends!");
			}
#endif

			Box.vHalfExtents = Box.vHalfExtents * _Desc.vScale;

			// physx: x axis is up => swizzle x & y
			//Box.vHalfExtents = Box.vHalfExtents.yzx;

			pShape = pPhysics->createShape(GeoBox(Box.vHalfExtents), *pMaterial, true, kFlags);
		}

		break;
	case GeometryType_ConvexMesh:
	{
		Async::ScopedSpinLock lock(m_ConvexMeshLock);

		TConvexMeshPool::iterator it = m_ConvexMeshPool.find(_Desc.ConvexMesh.uCRC32);
		if (it != m_ConvexMeshPool.end())
		{
			pShape = pPhysics->createShape(
				GeoConvexMesh(it->second, physx::PxMeshScale(_Desc.vScale, Math::quaternion(physx::PxIdentity))),
				*pMaterial, true, kFlags);
		}
		else
		{
			HERROR("ConvexMesh %u not found", _Desc.ConvexMesh.uCRC32);
		}		
	}
		break;
	case GeometryType_TriangleMesh:
	case GeometryType_HeightField:
	default:
		HERROR("ShapeType not supported!");
		break;
	}

	if (pShape == nullptr)
	{
		return nullptr;
	}

	if (_Desc.LocalTransform.isValid())
	{
		pShape->setLocalPose(_Desc.LocalTransform);
	}

	switch (_Desc.kUsage)
	{
	case ShapeUsageType_Query:
		pShape->setQueryFilterData(_Desc.Filter);
		break;
	case ShapeUsageType_Simulation:
		pShape->setSimulationFilterData(_Desc.Filter);
		break;
	default:
		break;
	}
	
	return pShape;
}
//---------------------------------------------------------------------------------------------------------------------

bool GeometryShapeFactory::AddConvexMesh(const ConvexMeshCluster& _ConvexMeshCluster)
{
	physx::PxPhysics* pPhysics = PhysicsFactory::Instance()->GetPhysics();

	uint32_t uIndex = 0u;
	for (const ConvexMeshCluster::ConvexMesh& CMesh : _ConvexMeshCluster.Meshes)
	{
		if (CMesh.uCRC32 == 0u || CMesh.Data.empty())
		{
			HERROR("Invalid convex mesh data at index %u", uIndex);
			continue;
		}

		uIndex++;
		if (m_ConvexMeshPool.count(CMesh.uCRC32) == 0)
		{
			PhysXInputStream input(CMesh.Data);
			ConvexMesh* pConvexMesh = pPhysics->createConvexMesh(input);

			if (pConvexMesh == nullptr)
			{
				HERROR("Faild to create ConvexMesh %X", CMesh.uCRC32);
				return false;
			}

			Async::ScopedSpinLock lock(m_ConvexMeshLock);
			m_ConvexMeshPool.insert({ CMesh.uCRC32, pConvexMesh });
		}	
	}

	return true;
}
//---------------------------------------------------------------------------------------------------------------------
void GeometryShapeFactory::ReleaseConvexMesh(const uint32_t& _uChecksum)
{
	Async::ScopedSpinLock lock(m_ConvexMeshLock);

	TConvexMeshPool::iterator it = m_ConvexMeshPool.find(_uChecksum);
	if (it != m_ConvexMeshPool.end())
	{
		it->second->release();
		m_ConvexMeshPool.erase(it);
	}
}
//---------------------------------------------------------------------------------------------------------------------
SurfaceMaterial* GeometryShapeFactory::CreateMaterial(const SurfaceMaterialDesc& _Desc)
{
	uint64_t uHash = Helix::BaseTypeHash(_Desc);

	TSurfaceMaterialPool::iterator it = m_SurfaceMaterialPool.find(uHash);
	if (it != m_SurfaceMaterialPool.end())
	{
		return it->second;
	}

	physx::PxPhysics* pPhysics = PhysicsFactory::Instance()->GetPhysics();

	SurfaceMaterial* pMaterial = pPhysics->createMaterial(_Desc.fStaticFriction, _Desc.fDynamicFriction, _Desc.fRestitution);
	if (pMaterial == nullptr)
	{
		HERROR("Failed to create PxMaterial");
		return nullptr;
	}

	pMaterial->setFrictionCombineMode(static_cast<physx::PxCombineMode::Enum>(_Desc.kFrictionCombineMode));
	pMaterial->setRestitutionCombineMode(static_cast<physx::PxCombineMode::Enum>(_Desc.kRestitutionCombineMode));
	pMaterial->setFlags(static_cast<physx::PxMaterialFlag::Enum>(_Desc.kFrictionFlags));

	m_SurfaceMaterialPool.insert({ uHash, pMaterial });

	return pMaterial;
}
//---------------------------------------------------------------------------------------------------------------------
