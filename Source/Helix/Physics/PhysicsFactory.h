//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher, David Labode
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef PHYSICSINITIALIZER_H
#define	PHYSICSINITIALIZER_H

#include "PhysXLogger.h"
#include "common\PxTolerancesScale.h"
#include "foundation\PxFoundation.h"
#include "cooking\PxCooking.h"
#include "PxPhysicsAPI.h"
#include "PxScene.h"
// forward declaration

#include "hlx\src\Singleton.h"

namespace Helix
{
	namespace Physics
	{
		class PhysicsFactory : public hlx::Singleton<PhysicsFactory>
		{
		public:
			PhysicsFactory();
			~PhysicsFactory();

			bool Initialize(bool _bCreatePhysics = true, bool _bRecordMemoryAllocations = false, bool _bCreatCookingSDK = true);
			
			physx::PxPhysics* GetPhysics() const;
			physx::PxFoundation* GetFoundation() const;
			const physx::PxTolerancesScale& GetToleranceScale() const;
			physx::PxDefaultCpuDispatcher* GetCPUDispatcher() const;
			physx::PxGpuDispatcher* GetGPUDispatcher() const;
			physx::PxCooking* GetCooking() const;

			PhysXLogger& GetLogger();
			const PhysXLogger& GetLogger() const;

			AssertHandler& GetAssertHandler();
			const AssertHandler& GetAssertHandler() const;

		private:
			PhysXLogger m_PhysXLogger;
			AssertHandler m_AssertHandler;
			physx::PxDefaultAllocator m_DefaultAllocatorCallback;

			physx::PxFoundation* m_pFoundation = nullptr;
			physx::PxPhysics* mpPhysics = nullptr;
			physx::PxProfileZoneManager* m_pProfileZoneManager = nullptr;
			physx::PxDefaultCpuDispatcher* m_pCPUDispatcher = nullptr;
			physx::PxCooking* m_pCooking = nullptr;

			physx::PxCudaContextManager* m_pCUDAContextManager = nullptr;
			physx::PxTolerancesScale m_ToleranceScale = {};			
		};

		inline physx::PxPhysics* PhysicsFactory::GetPhysics() const { return mpPhysics; }
		inline physx::PxFoundation* PhysicsFactory::GetFoundation() const { return m_pFoundation; }
		inline const physx::PxTolerancesScale& PhysicsFactory::GetToleranceScale() const{return m_ToleranceScale;}
		inline physx::PxDefaultCpuDispatcher* PhysicsFactory::GetCPUDispatcher() const	{return m_pCPUDispatcher;}

		inline physx::PxGpuDispatcher* PhysicsFactory::GetGPUDispatcher() const	{return m_pCUDAContextManager->getGpuDispatcher();}
		inline physx::PxCooking* PhysicsFactory::GetCooking() const	{return m_pCooking;}
		inline PhysXLogger& PhysicsFactory::GetLogger()	{return m_PhysXLogger;}
		inline const PhysXLogger& PhysicsFactory::GetLogger() const	{return m_PhysXLogger;}

		inline AssertHandler&  PhysicsFactory::GetAssertHandler() { return m_AssertHandler; }
		inline const AssertHandler&  PhysicsFactory::GetAssertHandler() const { return m_AssertHandler; }
	} // Physics
} // Helix


#endif // !PHYSICSINITIALIZER_H
