//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher, David Labode
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef GEOMETRYDESCRIPTIONS_H
#define GEOMETRYDESCRIPTIONS_H

#include "Math\MathTypes.h"
#include "hlx\src\Logger.h"
#include <string>
#include "CollisionFilter.h"

namespace Helix
{
	namespace Physics
	{
		enum CombineMode : uint32_t
		{
			CombineMode_Average = 0,		//!< Average: (a + b)/2
			CombineMode_Min = 1,		//!< Minimum: minimum(a,b)
			CombineMode_Mulitply = 2,		//!< Multiply: a*b
			CombineMode_Max = 3,		//!< Maximum: maximum(a,b)
		};

		enum FrictionFlag : uint32_t
		{
			FrictionFlag_None = 0,
			FrictionFlag_DisableFriction = 1 << 0,
			FrictionFlag_DisableStrongFriction = 1 << 1
		};

		struct SurfaceMaterialDesc
		{
			float fStaticFriction = 0.2f;
			float fDynamicFriction = 0.2f;
			float fRestitution = 0.0125f;

			CombineMode kRestitutionCombineMode = CombineMode_Average;
			CombineMode kFrictionCombineMode = CombineMode_Average;
			FrictionFlag kFrictionFlags = FrictionFlag_None;
		};

		enum GeometryType : uint32_t
		{
			GeometryType_Sphere = 0,
			GeometryType_Plane,
			GeometryType_Capsule,
			GeometryType_Box,
			GeometryType_ConvexMesh,
			GeometryType_TriangleMesh,
			GeometryType_HeightField,
			GeometryType_Unknown
		};

		enum ShapeUsageType : uint32_t
		{
			ShapeUsageType_Query = 0,
			ShapeUsageType_Simulation,
			ShapeUsageType_Trigger,
			ShapeUsageType_Unknown
		};

		struct BoxDesc
		{
			BoxDesc() : vHalfExtents(0.5f, 0.5f, 0.5f) {}
			BoxDesc(const Math::float3& _vHalfExtents) : vHalfExtents(_vHalfExtents) {}
			BoxDesc(const float& _fX, const float& _fY, const float& _fZ) : vHalfExtents(_fX, _fY, _fZ) {}

			Math::float3 vHalfExtents = { 0.5f, 0.5f, 0.5f };
		};

		struct SphereDesc
		{
			SphereDesc(const float& _fRadius = 1.f) : fRadius(_fRadius) {};
			float fRadius = 1.f;
		};

		struct CapsuleDesc
		{
			CapsuleDesc(const float& _fRadius = 0.5f, const float& _fHalfHight = 1.f) : fRadius(_fRadius), fHalfHeight(_fHalfHight) {}
			// default capsule hight = 2
			float fRadius = 0.5f;
			float fHalfHeight = 1.f;
		};

		//struct PlaneDesc
		//{
		//	Plane PlaneEquation;
		//};

		struct ConvexMeshDesc
		{
			ConvexMeshDesc(const uint32_t& _uCRC32 = 0u) : uCRC32(_uCRC32) {}
			uint32_t uCRC32 = 0u;
		};

		struct GeometryDesc
		{
			GeometryType kType = GeometryType_Box;
			ShapeUsageType kUsage = ShapeUsageType_Query;

			CollisionFilter Filter = CollisionFilter(CollisionFilterFlag_Default);

			Math::float3 vScale = { 1.f, 1.f, 1.f };
			bool bScaleWithParent = true;
			Math::transform LocalTransform = HTRANSFORM_IDENTITY;

			SurfaceMaterialDesc Material;

			BoxDesc Box;
			SphereDesc Sphere;
			CapsuleDesc Capsule;
			ConvexMeshDesc ConvexMesh;
		};
	} // Physics
} // Helix

#endif // GEOMETRYDESCRIPTIONS_H