//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher, David Labode
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "GravityField.h"
#include "amp_gravity_field.h"
#include "Scene\ParticleSystem.h"
#include <algorithm>
using namespace Helix::Physics;
using namespace Helix::AMP;
using namespace Helix::Datastructures;
using namespace Helix::Scene;

//--------------------------------------------------------------------------------------------------------------------
Helix::Physics::GravityFieldYoke::GravityFieldYoke(GravityField * _pField, float _fDeltaTime)
	: GravityPoints(_pField->m_GravityParticles.m_GPUVector)
	, fTotalTime(_pField->m_fTotalTime)
	, fMinSqDistance(_pField->m_fMinSqDistance)
	, fMaxSqDistance(_pField->m_fMaxSqDistance)
	, fDeltaTime(_fDeltaTime)
{
	for (uint32_t i = 0; i < 256; ++i)
	{
		float3 Rand = float3(rand(), rand(), rand()).Normalized();
		RandomVectors[i] = Rand;
	}
}
//--------------------------------------------------------------------------------------------------------------------
GravityField::GravityField(uint32_t _uNumGravityParticles, const concurrency::accelerator_view& _AMPAccelerator, const concurrency::access_type _kAccessType, float _fNearRadius, float _fInfluenceRadius)
	: m_GravityParticles(_uNumGravityParticles, _AMPAccelerator, _kAccessType)
	, m_uMaxGravityPoints(_uNumGravityParticles)
	, m_fTotalTime(0.0f)
	, m_fMinSqDistance(_fNearRadius * _fNearRadius)
	, m_fMaxSqDistance(_fInfluenceRadius * _fInfluenceRadius)
{
}
//--------------------------------------------------------------------------------------------------------------------
void GravityField::Upload()
{
	m_GravityParticles.Upload();
}
//--------------------------------------------------------------------------------------------------------------------
void GravityField::ApplyForce(RigidbodyYoke& _RbYoke, ParticleYoke& _PaYoke, const float _fDeltaTime, uint32_t & _uNumActiveParticles)
{
	m_fTotalTime += _fDeltaTime;
	GravityFieldYoke GfYoke(this, _fDeltaTime);
	ProcessRigidbodies(_RbYoke, GfYoke);

	if (_PaYoke.bPaused == false)
	{
		ProcessParticles(_PaYoke, GfYoke, _uNumActiveParticles);
	}
	else
	{
		_uNumActiveParticles = 0;
	}
}
//--------------------------------------------------------------------------------------------------------------------
bool GravityField::PushGravityPoint(const float3& _Position, const float _Strength)
{
	if (m_uGravityPointCount+1 < m_GravityParticles.m_GPUVector.extent.size())
	{
		for (uint32_t i = 0; i < m_GravityParticles.m_GPUVector.extent.size(); ++i)
		{
			if (m_GravityParticles.Get(i) == float4(0.f,0.f,0.f,0.f))
			{
				m_GravityParticles.Set(i, float4(_Position, _Strength));
				++m_uGravityPointCount;
				return true;				
			}
		}
	}
	return false;
}
//--------------------------------------------------------------------------------------------------------------------
bool GravityField::RemoveGravityPoint(const float3& _Position)
{
	if (m_uGravityPointCount > 0)
	{
		for (uint32_t i = 0; i < m_GravityParticles.m_GPUVector.extent.size(); ++i)
		{
			if (m_GravityParticles.Get(i).xyz == _Position)
			{
				m_GravityParticles.Set(i, HMFLOAT4_ZERO);
				--m_uGravityPointCount;
				return true;
			}
		}
	}
	return false;
}
//--------------------------------------------------------------------------------------------------------------------
void GravityField::Clear()
{
	m_GravityParticles.Clear();
}
//--------------------------------------------------------------------------------------------------------------------
void GravityField::ProcessRigidbodies(RigidbodyYoke& _RbYoke, GravityFieldYoke& _GfYoke)
{
	try
	{
		extent<1> Extent(_RbYoke.uActiveGameObjectsCount);

		parallel_for_each(Extent, [_RbYoke, _GfYoke](index<1> Idx) restrict(amp)
		{
			if (CheckFlag(_RbYoke.uFlags[Idx], GameObjectFlag::NoPhysics) == false)
			{
				float3 p = _RbYoke.vPosition[Idx];
				float3 v = _RbYoke.vLinearVelocity[Idx];
				float m = 1.0f / _RbYoke.fInvMass[Idx];

				gravity_step(p, v, m, _GfYoke.GravityPoints, _GfYoke.fDeltaTime, _GfYoke.fMinSqDistance, _GfYoke.fMaxSqDistance);

				_RbYoke.vLinearVelocity[Idx] = v;
			}
		});
	}
	catch (concurrency::runtime_exception e)
	{
		HFATALD2("%s", e.what());
	}
}
//--------------------------------------------------------------------------------------------------------------------
void GravityField::ProcessParticles(ParticleYoke& _PaYoke, GravityFieldYoke& _GfYoke, uint32_t& _uNumActiveParticles)
{
	try
	{
		_uNumActiveParticles = static_cast<uint32_t>(m_fTotalTime / _PaYoke.fSpawnInterval);
		extent<1> Extent = _PaYoke.Positions.extent;
		// clamp number of particles
		if (_uNumActiveParticles > Extent.size())
		{
			_uNumActiveParticles = Extent.size();
		}

		parallel_for_each(Extent, [_PaYoke, _GfYoke](index<1> Idx) restrict(amp)
		{
			float4 PartVel = _PaYoke.Velocities[Idx];
			float4 PartPos = _PaYoke.Positions[Idx];

			PartVel.w += _GfYoke.fDeltaTime; // velocity.w is the lifetime

			// respawn dead particles
			if (PartVel.w >= _PaYoke.fMaxLifeTime)
			{
				PartPos.xyz = _GfYoke.RandomVectors[Idx[0] % 256];
				PartPos.xyz *= _PaYoke.SpawnSphere.w;
				PartPos.xyz += _PaYoke.SpawnSphere.xyz;

				// reset velocity and life time
				PartVel.xyz = _PaYoke.InitVelocity;
				PartVel.w = 0;
			}

			// in the beginning some particles were not yet started
			if (PartVel.w >= 0.0f)
			{
				float3 p = PartPos.xyz;
				float3 v = PartVel.xyz;

				gravity_step(p, v, 1.0f, _GfYoke.GravityPoints, _GfYoke.fDeltaTime, _GfYoke.fMinSqDistance, _GfYoke.fMaxSqDistance);

				PartVel.xyz = v;
				PartPos.xyz += v * _GfYoke.fDeltaTime;
			}

			_PaYoke.Velocities[Idx] = PartVel;
			_PaYoke.Positions[Idx] = PartPos;
		});
	}
	catch (concurrency::runtime_exception e)
	{
		HFATALD2("%s", e.what());
	}
}