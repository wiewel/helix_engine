//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher, David Labode
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef RAYCASTCALLBACK_H
#define	RAYCASTCALLBACK_H

#include <PxQueryReport.h>

namespace Helix
{
	namespace Physics
	{
		template <class Container, int Size = 1024>
		struct RaycastCallback : public physx::PxRaycastCallback
		{
			RaycastCallback(Container& _Container) : m_Container(_Container), physx::PxRaycastCallback(m_pBuffer, Size) {}

			inline physx::PxAgain processTouches(const physx::PxRaycastHit* buffer, physx::PxU32 nbHits) final
			{
				for (uint32_t i = 0; i < nbHits; ++i)
				{
					m_Container.push_back(static_cast<Container::value_type>(buffer[i].actor->userData));
				}

				return true;
			}

			physx::PxRaycastHit m_pBuffer[Size];
			Container& m_Container;
		};
	} // Physics
} // Helix

#endif // !RAYCASTCALLBACK_H
