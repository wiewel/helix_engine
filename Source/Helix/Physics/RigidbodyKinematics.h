//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher, David Labode
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Fast 4-way parallel radix sorting on GPUs (Linh Ha, Jens Kr�ger, Cl�udio T. Silva)
//--------------------------------------------------------------------------------------------------------------------
// Summary:
// (Wikipedia) Radix sort is a non-comparative integer sorting algorithm that sorts data with integer keys by grouping 
// keys by the individual digits which share the same significant position and value. The radix is the base of the 
// significant part which is compared and sorted every step. In our case the radix is 4, which means that each step 
// there are 2 significant bits.
//--------------------------------------------------------------------------------------------------------------------
// Notation:
//  - for consistency with C++ AMP the CUDA term 'blocks' from the paper is translated to 'tiles'
//--------------------------------------------------------------------------------------------------------------------
// Prefix Sum:
//	s_0 = d_0
//	s_n = s_(n-1) + d_n
//
//	data:		0 1 0 0 1 0 1 1
//	prefix sum:	0 1 1 1 2 2 3 4
//--------------------------------------------------------------------------------------------------------------------
// http://vgc.poly.edu/~csilva/papers/cgf.pdf (Fast 4-way parallel radix sorting on GPUs)
// based on:
// http://http.developer.nvidia.com/GPUGems3/gpugems3_ch39.html (GPU Gems 3: Chapter 39)
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef RIGIDBODYKINEMATICS_H
#define RIGIDBODYKINEMATICS_H
#include "AMP\AMPDefines.h"
#include "AMP\AMPMath.h"
#include "DataStructures\GameObjectPool.h"
#include "Util\Profiler.h"

namespace Helix
{
	namespace Physics
	{
		struct KinematicsConf
		{
			// Sleep (from Millington 07 Game Engine Development)
			float fBaseSleepBias = 0.7f; // controlls weighting of previous frames -> smaller = less weight
			float fSleepEpsilon = 0.1f; // controlls how fast objects fall asleep
			AMP::float3 vDefaultAcceleration = HMFLOAT3_ZERO/*AMP::float3(0, -9.81f, 0)*/; // m/s^2 or N/kg
		};
		template<uint32_t uTileSize = 256>
		void RigidbodyKinematics(Datastructures::RigidbodyYoke& _GameObjs, float _fDeltaTime, KinematicsConf _Conf = KinematicsConf());
	}
}

//---------------------------------------------------------------------------------------------------
template<uint32_t uTileSize>
void Helix::Physics::RigidbodyKinematics(Datastructures::RigidbodyYoke & _GameObjs, float _fDeltaTime, KinematicsConf _Conf)
{
	using namespace AMP;
	using namespace Datastructures;

	// make sleep bias dependend on frame time
	const float fMotionBias = std::powf(_Conf.fBaseSleepBias, _fDeltaTime);
	try
	{
		extent<1> Extent(_GameObjs.uActiveGameObjectsCount);
		parallel_for_each(Extent, [=](index<1> Idx) restrict(amp)
		{
			if (CheckFlag(_GameObjs.uFlags[Idx], GameObjectFlag::Static) == false)
			{
				float fInvMass = _GameObjs.fInvMass[Idx];
				float3 F = _GameObjs.vForce[Idx];
				_GameObjs.vForce[Idx] = HMFLOAT3_ZERO;

				// TODO: sleep
				float3 x = _GameObjs.vPosition[Idx];
				float3 v = _GameObjs.vLinearVelocity[Idx];
				float3 a = _Conf.vDefaultAcceleration + F * fInvMass;
				float3 v_prev = v;

				float fDamping = _GameObjs.fDamping[Idx];

				heun(x, v, a, fDamping, _fDeltaTime);

				_GameObjs.vPosition[Idx] = x;
				_GameObjs.vLinearVelocity[Idx] = v;
				
				// TODO: update motion
				// TODO: update bounding volumes
			}
		});
		parallel_for_each(Extent, [=](index<1> Idx) restrict(amp)
		{
			if (CheckFlag(_GameObjs.uFlags[Idx], GameObjectFlag::Static) == false)
			{
				float3 q = _GameObjs.vTorque[Idx];
				_GameObjs.vTorque[Idx] = HMFLOAT3_ZERO;

				quaternion r = _GameObjs.vOrientation[Idx];
				float3 L = _GameObjs.vAngularMomentum[Idx];
				float3 w = _GameObjs.vAngularVelocity[Idx];
				float3 w_prev = w;
				float fDamping = _GameObjs.fDamping[Idx];

				//r' = quat(w) * r
				quaternion r1 = quaternion(w) * r;

				//r += h/2 * r'
				r = euler(r, r1, _fDeltaTime * 0.5f);
				r.Normalize();
				_GameObjs.vOrientation[Idx] = r;

				// L += h*q
				L = euler(L, q, _fDeltaTime * fDamping);
				_GameObjs.vAngularMomentum[Idx] = L;

				float3x3 I = _GameObjs.mInvInertiaTensor[Idx];
				float3x3 R = r.GetMatrix();
				_GameObjs.mRotation[Idx] = R;

				// R^T * I * R
				I = R.Transposed() * I * R;

				w = I * L;
				_GameObjs.vAngularVelocity[Idx] = w;
			}
		});
	}
	catch (concurrency::runtime_exception e)
	{
		HFATALD2("%s", e.what());
	}
}

#endif