//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "HIDMapping.h"
#include "hlx\src\TextToken.h"
#include "hlx\src\Logger.h"
using namespace Helix::Input;
using namespace hlx;

//----------------------------------------------------------------------------------------------------------------------------
HIDMapping::HIDMapping()
{
}
//----------------------------------------------------------------------------------------------------------------------------
HIDMapping::~HIDMapping()
{
}
//----------------------------------------------------------------------------------------------------------------------------
bool HIDMapping::Initialize(std::string _sDeviceName, hlx::string _sConfigPath)
{
	TextToken Config(_sConfigPath, _sDeviceName);

	// Get the type of hid
	std::string sType = Config.get<std::string>("Type");
	if (sType != "Gamepad")
	{
		HWARNING("HID of type %s is currently unsupported", CSTR(_sDeviceName));
		return false;
		//m_Type = Gamepad;
	}

	// Get device info
	m_uVendorID = Config.get<uint32_t>("Vendor ID");
	m_uProductID = Config.get<uint32_t>("Product ID");

	// Button to Button Mapping
	{
		std::pair<std::multimap<std::string, TextToken>::iterator, std::multimap<std::string, TextToken>::iterator> IteratorPair = 
			Config.getAllTokens("Button to Button");
		for (std::multimap<std::string, TextToken>::iterator it = IteratorPair.first; it != IteratorPair.second; ++it)
		{
			uint32_t RIButton = it->second.get<uint32_t>("RI Button");
			uint32_t HIButton = it->second.get<uint32_t>("HI Button");
			m_ButtonToButtonMappings.emplace(RIButton, HIButton);
		}
	}
	// Axis to Button Mapping
	{
		std::pair<std::multimap<std::string, TextToken>::iterator, std::multimap<std::string, TextToken>::iterator> IteratorPair =
			Config.getAllTokens("Axis to Button");
		for (std::multimap<std::string, TextToken>::iterator it = IteratorPair.first; it != IteratorPair.second; ++it)
		{
			AxisToButton Mapping;
			Mapping.uRIAxis = it->second.get<uint32_t>("RI Axis");
			Mapping.uThreshold = it->second.get<uint32_t>("Threshold");
			Mapping.uHIButtonLow = it->second.get<uint32_t>("HI Button Low");
			Mapping.uHIButtonHigh = it->second.get<uint32_t>("HI Button High");
			m_AxisToButtonMappings.emplace(Mapping.uRIAxis, Mapping);
		}
	}
	// Button to Axis Mapping
	{
		std::pair<std::multimap<std::string, TextToken>::iterator, std::multimap<std::string, TextToken>::iterator> IteratorPair =
			Config.getAllTokens("Button to Raw");
		for (std::multimap<std::string, TextToken>::iterator it = IteratorPair.first; it != IteratorPair.second; ++it)
		{
			ButtonToAxis Mapping;
			Mapping.uRIButtonLow = it->second.get<uint32_t>("RI Button Low");
			Mapping.uRIButtonHigh = it->second.get<uint32_t>("RI Button High");
			Mapping.fValueMin = it->second.get<float>("Value Min");
			Mapping.fValueMax = it->second.get<float>("Value Max");
			Mapping.uHIAxis = it->second.get<uint32_t>("HI Axis");
			m_ButtonToAxisMappings.emplace(Mapping.uRIButtonLow, Mapping);
			m_ButtonToAxisMappings.emplace(Mapping.uRIButtonHigh, Mapping);
		}
	}
	// Axis to virtual Axis mapping
	{
		std::pair<std::multimap<std::string, TextToken>::iterator, std::multimap<std::string, TextToken>::iterator> IteratorPair =
			Config.getAllTokens("Axis to Axis");
		for (std::multimap<std::string, TextToken>::iterator it = IteratorPair.first; it != IteratorPair.second; ++it)
		{
			AxisToAxis Mapping;
			Mapping.uRIAxis = it->second.get<uint32_t>("RI Axis");
			Mapping.uRIMin = it->second.get<uint32_t>("RI Min");
			Mapping.uRIMax = it->second.get<uint32_t>("RI Max");
			Mapping.uRIDeadzoneFrom = it->second.get<uint32_t>("RI Deadzone From");
			Mapping.uRIDeadzoneTo = it->second.get<uint32_t>("RI Deadzone To");
			Mapping.uHIAxis = it->second.get<uint32_t>("HI Axis");
			Mapping.fHIMin = it->second.get<float>("HI Min");
			Mapping.fHIMax = it->second.get<float>("HI Max");
			m_AxisToAxisMappings.emplace(Mapping.uRIAxis, Mapping);
		}
	}
	// Axis to Digipad
	{
		std::pair<std::multimap<std::string, TextToken>::iterator, std::multimap<std::string, TextToken>::iterator> IteratorPair =
			Config.getAllTokens("Axis to Digipad");
		for (std::multimap<std::string, TextToken>::iterator it = IteratorPair.first; it != IteratorPair.second; ++it)
		{
			AxisToDigipad Mapping;
			Mapping.uRIAxis = it->second.get<uint32_t>("RI Axis");
			TextToken ValueMapToken;
			it->second.getToken("Value Map", ValueMapToken);
			std::vector<uint32_t> ValueMap = ValueMapToken.getAll<uint32_t>();
			for (int i = 0; i < 8; ++i)
			{
				Mapping.ValueMap[i] = ValueMap[i];
			}
			m_AxisToDigipadMappings.emplace(Mapping.uRIAxis, Mapping);
		}
	}
	return true;
}
//----------------------------------------------------------------------------------------------------------------------------
bool HIDMapping::GetButtonFromButton(Button _uRIButton, Button & _uHIButton) const
{
	if (m_ButtonToButtonMappings.empty() == false)
	{
		std::map<Button,Button>::const_iterator Mapping = m_ButtonToButtonMappings.find(_uRIButton);
		if (Mapping != m_ButtonToButtonMappings.end())
		{
			_uHIButton = Mapping->second;
			return true;
		}
	}
	return false; // no mapping found
}
//----------------------------------------------------------------------------------------------------------------------------
bool HIDMapping::GetButtonFromAxis(Axis _RIAxis, uint32_t _RIValue, Button& _HIButtonDown, std::vector<Button>& _HIButtonsUp) const
{
	if (m_AxisToButtonMappings.empty() == false)
	{
		std::map<Axis, AxisToButton>::const_iterator Mapping = m_AxisToButtonMappings.find(_RIAxis);
		if (Mapping != m_AxisToButtonMappings.end())
		{
			_HIButtonDown = Mapping->second.Map(_RIValue);
			_HIButtonsUp.push_back(_HIButtonDown == Mapping->second.uHIButtonLow ? Mapping->second.uHIButtonHigh : Mapping->second.uHIButtonLow);
			return true;
		}
	}
	if (m_AxisToDigipadMappings.empty() == false)
	{
		std::map<Axis, AxisToDigipad>::const_iterator Mapping = m_AxisToDigipadMappings.find(_RIAxis);
		if (Mapping != m_AxisToDigipadMappings.end())
		{
			for (int i = 0; i < 8; ++i)
			{
				if (i != _RIValue)
				{
					_HIButtonsUp.push_back(Mapping->second.Map(i));
				}
			}
			_HIButtonDown = Mapping->second.Map(_RIValue);
		}
	}
	return false; // no mapping found
}
//----------------------------------------------------------------------------------------------------------------------------
bool HIDMapping::GetAxisFromButton(Button _uRIButton, Axis & _uHIAxis, float & _fValue) const
{
	if (m_ButtonToAxisMappings.empty() == false)
	{
		std::map<Button, ButtonToAxis>::const_iterator Mapping = m_ButtonToAxisMappings.find(_uRIButton);
		if (Mapping != m_ButtonToAxisMappings.end())
		{
			_uHIAxis = Mapping->second.uHIAxis;
			_fValue = Mapping->second.Map(_uRIButton);
			return true;
		}
	}
	return false; // no mapping found
}
//----------------------------------------------------------------------------------------------------------------------------
bool HIDMapping::GetAxisFromAxis(Axis _uRIAxis, uint32_t _fRIValue, Axis & _uHIAxis, float & _fHIValue) const
{
	// find axis to virtual axis mappings
	if (m_AxisToAxisMappings.empty() == false)
	{
		std::map<Axis, AxisToAxis>::const_iterator Mapping = m_AxisToAxisMappings.find(_uRIAxis);
		if (Mapping != m_AxisToAxisMappings.end())
		{
			_uHIAxis = Mapping->second.uHIAxis;
			_fHIValue = Mapping->second.Map(_fRIValue);
			return true;
		}
	}
	return false; // no mapping found
}
//---------------------------------------------------------------------------------------------------
