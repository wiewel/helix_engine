//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include <string>
#include "InputConstants.h"
using namespace Helix::Input;

//----------------------------------------------------------------------------------------------------------------------------
InputConstants::InputConstants()
{
	m_ButtonNames = std::unordered_map<std::string, Button>{
		{ "Keyboard_Btn_BACK", static_cast<Button>(Keyboard_Btn::BACK) },
		{ "Keyboard_Btn_RETURN", static_cast<Button>(Keyboard_Btn::RETURN) },
		{ "Keyboard_Btn_SPACE", static_cast<Button>(Keyboard_Btn::SPACE) },
		{ "Keyboard_Btn_TAB", static_cast<Button>(Keyboard_Btn::TAB) },
		{ "Keyboard_Btn_SHIFT", static_cast<Button>(Keyboard_Btn::SHIFT) },
		{ "Keyboard_Btn_LSHIFT", static_cast<Button>(Keyboard_Btn::LSHIFT) },
		{ "Keyboard_Btn_RSHIFT", static_cast<Button>(Keyboard_Btn::RSHIFT) },
		{ "Keyboard_Btn_CAPSLOCK", static_cast<Button>(Keyboard_Btn::CAPSLOCK) },
		{ "Keyboard_Btn_CONTROL", static_cast<Button>(Keyboard_Btn::CONTROL) },
		{ "Keyboard_Btn_LCONTROL", static_cast<Button>(Keyboard_Btn::LCONTROL) },
		{ "Keyboard_Btn_RCONTROL", static_cast<Button>(Keyboard_Btn::RCONTROL) },
		{ "Keyboard_Btn_LSUPER", static_cast<Button>(Keyboard_Btn::LSUPER) },
		{ "Keyboard_Btn_RSUPER", static_cast<Button>(Keyboard_Btn::RSUPER) },
		{ "Keyboard_Btn_ESC", static_cast<Button>(Keyboard_Btn::ESC) },
		{ "Keyboard_Btn_PAUSE", static_cast<Button>(Keyboard_Btn::PAUSE) },
		{ "Keyboard_Btn_PAGEUP", static_cast<Button>(Keyboard_Btn::PAGEUP) },
		{ "Keyboard_Btn_PAGEDOWN", static_cast<Button>(Keyboard_Btn::PAGEDOWN) },
		{ "Keyboard_Btn_END", static_cast<Button>(Keyboard_Btn::END) },
		{ "Keyboard_Btn_HOME", static_cast<Button>(Keyboard_Btn::HOME) },
		{ "Keyboard_Btn_SELECT", static_cast<Button>(Keyboard_Btn::SELECT) },
		{ "Keyboard_Btn_PRINT", static_cast<Button>(Keyboard_Btn::PRINT) },
		{ "Keyboard_Btn_PRINTSCREEN", static_cast<Button>(Keyboard_Btn::PRINTSCREEN) },
		{ "Keyboard_Btn_INSERT", static_cast<Button>(Keyboard_Btn::INSERT) },
		{ "Keyboard_Btn_DEL", static_cast<Button>(Keyboard_Btn::DEL) },
		{ "Keyboard_Btn_LEFT", static_cast<Button>(Keyboard_Btn::LEFT) },
		{ "Keyboard_Btn_UP", static_cast<Button>(Keyboard_Btn::UP) },
		{ "Keyboard_Btn_RIGHT", static_cast<Button>(Keyboard_Btn::RIGHT) },
		{ "Keyboard_Btn_DOWN", static_cast<Button>(Keyboard_Btn::DOWN) },
		{ "Keyboard_Btn_NUM0", static_cast<Button>(Keyboard_Btn::NUM0) },
		{ "Keyboard_Btn_NUM1", static_cast<Button>(Keyboard_Btn::NUM1) },
		{ "Keyboard_Btn_NUM2", static_cast<Button>(Keyboard_Btn::NUM2) },
		{ "Keyboard_Btn_NUM3", static_cast<Button>(Keyboard_Btn::NUM3) },
		{ "Keyboard_Btn_NUM4", static_cast<Button>(Keyboard_Btn::NUM4) },
		{ "Keyboard_Btn_NUM5", static_cast<Button>(Keyboard_Btn::NUM5) },
		{ "Keyboard_Btn_NUM6", static_cast<Button>(Keyboard_Btn::NUM6) },
		{ "Keyboard_Btn_NUM7", static_cast<Button>(Keyboard_Btn::NUM7) },
		{ "Keyboard_Btn_NUM8", static_cast<Button>(Keyboard_Btn::NUM8) },
		{ "Keyboard_Btn_NUM9", static_cast<Button>(Keyboard_Btn::NUM9) },
		{ "Keyboard_Btn_A", static_cast<Button>(Keyboard_Btn::A) },
		{ "Keyboard_Btn_B", static_cast<Button>(Keyboard_Btn::B) },
		{ "Keyboard_Btn_C", static_cast<Button>(Keyboard_Btn::C) },
		{ "Keyboard_Btn_D", static_cast<Button>(Keyboard_Btn::D) },
		{ "Keyboard_Btn_E", static_cast<Button>(Keyboard_Btn::E) },
		{ "Keyboard_Btn_F", static_cast<Button>(Keyboard_Btn::F) },
		{ "Keyboard_Btn_G", static_cast<Button>(Keyboard_Btn::G) },
		{ "Keyboard_Btn_H", static_cast<Button>(Keyboard_Btn::H) },
		{ "Keyboard_Btn_I", static_cast<Button>(Keyboard_Btn::I) },
		{ "Keyboard_Btn_J", static_cast<Button>(Keyboard_Btn::J) },
		{ "Keyboard_Btn_K", static_cast<Button>(Keyboard_Btn::K) },
		{ "Keyboard_Btn_L", static_cast<Button>(Keyboard_Btn::L) },
		{ "Keyboard_Btn_M", static_cast<Button>(Keyboard_Btn::M) },
		{ "Keyboard_Btn_N", static_cast<Button>(Keyboard_Btn::N) },
		{ "Keyboard_Btn_O", static_cast<Button>(Keyboard_Btn::O) },
		{ "Keyboard_Btn_P", static_cast<Button>(Keyboard_Btn::P) },
		{ "Keyboard_Btn_Q", static_cast<Button>(Keyboard_Btn::Q) },
		{ "Keyboard_Btn_R", static_cast<Button>(Keyboard_Btn::R) },
		{ "Keyboard_Btn_S", static_cast<Button>(Keyboard_Btn::S) },
		{ "Keyboard_Btn_T", static_cast<Button>(Keyboard_Btn::T) },
		{ "Keyboard_Btn_U", static_cast<Button>(Keyboard_Btn::U) },
		{ "Keyboard_Btn_V", static_cast<Button>(Keyboard_Btn::V) },
		{ "Keyboard_Btn_W", static_cast<Button>(Keyboard_Btn::W) },
		{ "Keyboard_Btn_X", static_cast<Button>(Keyboard_Btn::X) },
		{ "Keyboard_Btn_Y", static_cast<Button>(Keyboard_Btn::Y) },
		{ "Keyboard_Btn_Z", static_cast<Button>(Keyboard_Btn::Z) },
		{ "Keyboard_Btn_NUMPAD0", static_cast<Button>(Keyboard_Btn::NUMPAD0) },
		{ "Keyboard_Btn_NUMPAD1", static_cast<Button>(Keyboard_Btn::NUMPAD1) },
		{ "Keyboard_Btn_NUMPAD2", static_cast<Button>(Keyboard_Btn::NUMPAD2) },
		{ "Keyboard_Btn_NUMPAD3", static_cast<Button>(Keyboard_Btn::NUMPAD3) },
		{ "Keyboard_Btn_NUMPAD4", static_cast<Button>(Keyboard_Btn::NUMPAD4) },
		{ "Keyboard_Btn_NUMPAD5", static_cast<Button>(Keyboard_Btn::NUMPAD5) },
		{ "Keyboard_Btn_NUMPAD6", static_cast<Button>(Keyboard_Btn::NUMPAD6) },
		{ "Keyboard_Btn_NUMPAD7", static_cast<Button>(Keyboard_Btn::NUMPAD7) },
		{ "Keyboard_Btn_NUMPAD8", static_cast<Button>(Keyboard_Btn::NUMPAD8) },
		{ "Keyboard_Btn_NUMPAD9", static_cast<Button>(Keyboard_Btn::NUMPAD9) },
		{ "Keyboard_Btn_MULTIPLY", static_cast<Button>(Keyboard_Btn::MULTIPLY) },
		{ "Keyboard_Btn_ADD", static_cast<Button>(Keyboard_Btn::ADD) },
		{ "Keyboard_Btn_SEPARATOR", static_cast<Button>(Keyboard_Btn::SEPARATOR) },
		{ "Keyboard_Btn_SUBTRACT", static_cast<Button>(Keyboard_Btn::SUBTRACT) },
		{ "Keyboard_Btn_DECIMAL", static_cast<Button>(Keyboard_Btn::DECIMAL) },
		{ "Keyboard_Btn_DIVIDE", static_cast<Button>(Keyboard_Btn::DIVIDE) },
		{ "Keyboard_Btn_F1", static_cast<Button>(Keyboard_Btn::F1) },
		{ "Keyboard_Btn_F2", static_cast<Button>(Keyboard_Btn::F2) },
		{ "Keyboard_Btn_F3", static_cast<Button>(Keyboard_Btn::F3) },
		{ "Keyboard_Btn_F4", static_cast<Button>(Keyboard_Btn::F4) },
		{ "Keyboard_Btn_F5", static_cast<Button>(Keyboard_Btn::F5) },
		{ "Keyboard_Btn_F6", static_cast<Button>(Keyboard_Btn::F6) },
		{ "Keyboard_Btn_F7", static_cast<Button>(Keyboard_Btn::F7) },
		{ "Keyboard_Btn_F8", static_cast<Button>(Keyboard_Btn::F8) },
		{ "Keyboard_Btn_F9", static_cast<Button>(Keyboard_Btn::F9) },
		{ "Keyboard_Btn_F10", static_cast<Button>(Keyboard_Btn::F10) },
		{ "Keyboard_Btn_F11", static_cast<Button>(Keyboard_Btn::F11) },
		{ "Keyboard_Btn_F12", static_cast<Button>(Keyboard_Btn::F12) },
		{ "Keyboard_Btn_F13", static_cast<Button>(Keyboard_Btn::F13) },
		{ "Keyboard_Btn_F14", static_cast<Button>(Keyboard_Btn::F14) },
		{ "Keyboard_Btn_F15", static_cast<Button>(Keyboard_Btn::F15) },
		{ "Keyboard_Btn_F16", static_cast<Button>(Keyboard_Btn::F16) },
		{ "Keyboard_Btn_F17", static_cast<Button>(Keyboard_Btn::F17) },
		{ "Keyboard_Btn_F18", static_cast<Button>(Keyboard_Btn::F18) },
		{ "Keyboard_Btn_F19", static_cast<Button>(Keyboard_Btn::F19) },
		{ "Keyboard_Btn_F20", static_cast<Button>(Keyboard_Btn::F20) },
		{ "Keyboard_Btn_F21", static_cast<Button>(Keyboard_Btn::F21) },
		{ "Keyboard_Btn_F22", static_cast<Button>(Keyboard_Btn::F22) },
		{ "Keyboard_Btn_F23", static_cast<Button>(Keyboard_Btn::F23) },
		{ "Keyboard_Btn_F24", static_cast<Button>(Keyboard_Btn::F24) },
		{ "Keyboard_Btn_OEM1", static_cast<Button>(Keyboard_Btn::OEM1) },
		{ "Keyboard_Btn_PLUS", static_cast<Button>(Keyboard_Btn::PLUS) },
		{ "Keyboard_Btn_COMMA", static_cast<Button>(Keyboard_Btn::COMMA) },
		{ "Keyboard_Btn_MINUS", static_cast<Button>(Keyboard_Btn::MINUS) },
		{ "Keyboard_Btn_PERIOD", static_cast<Button>(Keyboard_Btn::PERIOD) },
		{ "Keyboard_Btn_OEM2", static_cast<Button>(Keyboard_Btn::OEM2) },
		{ "Keyboard_Btn_OEM3", static_cast<Button>(Keyboard_Btn::OEM3) },
		{ "Keyboard_Btn_OEM4", static_cast<Button>(Keyboard_Btn::OEM4) },
		{ "Keyboard_Btn_OEM5", static_cast<Button>(Keyboard_Btn::OEM5) },
		{ "Keyboard_Btn_OEM6", static_cast<Button>(Keyboard_Btn::OEM6) },
		{ "Keyboard_Btn_OEM7", static_cast<Button>(Keyboard_Btn::OEM7) },
		{ "Keyboard_Btn_OEM8", static_cast<Button>(Keyboard_Btn::OEM8) },
		{ "Mouse_Btn_PRIMARY", static_cast<Button>(Mouse_Btn::PRIMARY) },
		{ "Mouse_Btn_SECONDARY", static_cast<Button>(Mouse_Btn::SECONDARY) },
		{ "Mouse_Btn_MIDDLE", static_cast<Button>(Mouse_Btn::MIDDLE) },
		{ "Mouse_Btn_X1", static_cast<Button>(Mouse_Btn::X1) },
		{ "Mouse_Btn_X2", static_cast<Button>(Mouse_Btn::X2) }
	};
	m_AxisNames = std::unordered_map<std::string, Axis>{
		{ "Mouse_Axis_X", static_cast<Axis>(Mouse_Axis::X) },
		{ "Mouse_Axis_Y", static_cast<Axis>(Mouse_Axis::Y) },
		{ "Mouse_Axis_WHEEL", static_cast<Axis>(Mouse_Axis::WHEEL) }
	};

	m_uHIDButtonOffset = static_cast<uint64_t>(Mouse_Axis::Count);
	for (uint32_t i = 0; i < MAX_BUTTONS; ++i)
	{
		m_ButtonNames.emplace("Gamepad_Btn_" + std::to_string(i + 1), i + m_uHIDButtonOffset);
	}

	m_uHIDAxisOffset = m_uHIDButtonOffset + MAX_BUTTONS;
	for (uint32_t i = 0; i < MAX_AXES; ++i)
	{
		m_AxisNames.emplace("Gamepad_Axis_" + std::to_string(i + 1), i + m_uHIDAxisOffset);
	}

	// XBox One
	m_XBONE.m_Buttons = {
		{1, m_uHIDButtonOffset +	0},
		{2, m_uHIDButtonOffset +	3},
		{3, m_uHIDButtonOffset +	1},
		{4, m_uHIDButtonOffset +	2},
		{5, m_uHIDButtonOffset +	4},
		{6, m_uHIDButtonOffset +	5},
		{7, m_uHIDButtonOffset +	14},
		{8, m_uHIDButtonOffset +	15},
		{9, m_uHIDButtonOffset +	8},
		{10, m_uHIDButtonOffset +	9}
	};
	GamepadMaps::GamepadAxisInfo Info;
	Info.Axis = m_uHIDAxisOffset + 0;
	Info.bIsTwoDirectional = true;
	Info.m_uPrecision = 1;
	m_XBONE.m_Axis.emplace(0, Info);

	Info.Axis = m_uHIDAxisOffset + 1;
	Info.bIsTwoDirectional = true;
	Info.m_uPrecision = 1;
	m_XBONE.m_Axis.emplace(1, Info);

	Info.Axis = m_uHIDAxisOffset + 2;
	Info.bIsTwoDirectional = true;
	Info.m_uPrecision = 1;
	m_XBONE.m_Axis.emplace(2, Info);

	Info.Axis = m_uHIDAxisOffset + 3;
	Info.bIsTwoDirectional = true;
	Info.m_uPrecision = 1;
	m_XBONE.m_Axis.emplace(3, Info);

	Info.Axis = m_uHIDAxisOffset + 4;
	Info.bIsTwoDirectional = false;
	Info.m_uPrecision = 256;
	m_XBONE.m_Axis.emplace(4, Info);

	Info.Axis = m_uHIDAxisOffset + 5;
	Info.bIsTwoDirectional = false;
	Info.m_uPrecision = 256;
	m_XBONE.m_Axis.emplace(5, Info);

	// XBOX 360 Controller
	m_XBox360.m_Buttons = {
		{ 1, m_uHIDButtonOffset + 0 },
		{ 2, m_uHIDButtonOffset + 3 },
		{ 3, m_uHIDButtonOffset + 1 },
		{ 4, m_uHIDButtonOffset + 2 },
		{ 5, m_uHIDButtonOffset + 4 },
		{ 6, m_uHIDButtonOffset + 5 },
		{ 7, m_uHIDButtonOffset + 14 },
		{ 8, m_uHIDButtonOffset + 15 },
		{ 9, m_uHIDButtonOffset + 8 },
		{ 10, m_uHIDButtonOffset + 9 }
	};
	Info.Axis = m_uHIDAxisOffset + 0;
	Info.bIsTwoDirectional = true;
	Info.m_uPrecision = 1;
	m_XBox360.m_Axis.emplace(0, Info);

	Info.Axis = m_uHIDAxisOffset + 1;
	Info.bIsTwoDirectional = true;
	Info.m_uPrecision = 1;
	m_XBox360.m_Axis.emplace(1, Info);

	Info.Axis = m_uHIDAxisOffset + 2;
	Info.bIsTwoDirectional = true;
	Info.m_uPrecision = 1;
	m_XBox360.m_Axis.emplace(2, Info);

	Info.Axis = m_uHIDAxisOffset + 3;
	Info.bIsTwoDirectional = true;
	Info.m_uPrecision = 1;
	m_XBox360.m_Axis.emplace(3, Info);
}
