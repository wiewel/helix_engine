//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "InputContext.h"

#include "hlx\src\TextToken.h"
#include "hlx\src\StandardDefines.h"
#include "Resources\FileSystem.h"
#include "hlx\src\Bytes.h"
#include "hlx\src\Logger.h"

using namespace Helix::Input;
using namespace hlx;

//----------------------------------------------------------------------------------------------------------------------------
Helix::Input::InputContext::InputContext()
{
}
//----------------------------------------------------------------------------------------------------------------------------
Helix::Input::InputContext::~InputContext()
{
}
//----------------------------------------------------------------------------------------------------------------------------
bool InputContext::Reload(const std::string _sContextName, const hlx::string _sConfigFile)
{
	// get configuration file
	hlx::fbytestream ConfigPathStream;
	if (Resources::FileSystem::Instance()->OpenFileStream(Resources::HelixDirectories_EngineConfigs, _sConfigFile, std::ios::in, ConfigPathStream) == false)
	{
		HWARNING("Cannot find input context %s in %s", CSTR(_sContextName), _sConfigFile.c_str());
		return false;
	}
	hlx::bytes&& ConfigFile = ConfigPathStream.get<hlx::bytes>(ConfigPathStream.size());
	ConfigPathStream.close();
	hlx::bytestream ConfigContent(ConfigFile);
	TextToken Config(ConfigContent, _sContextName);
	
	// load values from the configuration file
	//m_uPriority = Config.get<uint32_t>("Priority");
	// DownActions
	TextToken DownActions;
	if (Config.getToken("DownActions", DownActions))
	{
		std::vector<std::pair<std::string, std::string>> DownActionMap = DownActions.getAllPairs<std::string>();
		HFOREACH_CONST(pair, end, DownActionMap)
			Button Btn;
			if (InputConstants::Instance()->GetButton(pair->first, Btn))
			{
				m_DownActionMap.emplace(Btn, pair->second);
			}
		HFOREACH_CONST_END
	}
	// UpActions
	TextToken UpActions;
	if (Config.getToken("UpActions", UpActions))
	{
		std::vector<std::pair<std::string, std::string>> UpActionMap = UpActions.getAllPairs<std::string>();
		HFOREACH_CONST(pair, end, UpActionMap)
			Button Btn;
			if (InputConstants::Instance()->GetButton(pair->first, Btn))
			{
				m_UpActionMap.emplace(Btn, pair->second);
			}
		HFOREACH_CONST_END
	}
	// States
	TextToken States;
	if (Config.getToken("States", States))
	{
		std::vector<std::pair<std::string, std::string>> StateMap = States.getAllPairs<std::string>();
		HFOREACH_CONST(pair, end, StateMap)
			Button Btn;
			if (InputConstants::Instance()->GetButton(pair->first, Btn))
			{
				m_StateMap.emplace(Btn, pair->second);
			}
		HFOREACH_CONST_END
	}
	// Ranges
	TextToken Ranges;
	if (Config.getToken("Ranges", Ranges))
	{
		std::vector<TextToken> RangeTokenMap = Ranges.getAllTokens();
		HFOREACH_CONST(token, end, RangeTokenMap)
			std::string sRangeName = token->getValue("Name");

			Axis Axs;
			if (InputConstants::Instance()->GetAxis(token->getKey(), Axs))
			{
				m_MouseMap.emplace(Axs, sRangeName);
			}

		HFOREACH_CONST_END
		
		std::vector<std::pair<std::string, std::string>> RangePairMap = Ranges.getAllPairs<std::string>();
		HFOREACH_CONST(pair, end, RangePairMap)
			Axis Axs;
			if (InputConstants::Instance()->GetAxis(pair->first, Axs))
			{
				m_MouseMap.emplace(Axs, pair->second);
			}
		HFOREACH_CONST_END
	}
	return true;
}
bool Helix::Input::InputContext::IsActive()
{
	return m_bIsActive;
}
void Helix::Input::InputContext::Activate()
{
	m_bIsActive = true;
}
void Helix::Input::InputContext::Deactivate()
{
	m_bIsActive = false;
}
//----------------------------------------------------------------------------------------------------------------------------
bool InputContext::MapButtonToAction(Button _Button, bool _bIsDownAction, InputAction & _Action)
{
	if (_bIsDownAction)
	{
		if (m_DownActionMap.empty())
		{
			return false;
		}

		std::unordered_map<Button, InputAction>::const_iterator it = m_DownActionMap.find(_Button);
		if (it == m_DownActionMap.end())
		{
			// No mapping exists
			return false;
		}

		_Action = it->second;
		return true;
	}
	else
	{
		if (m_UpActionMap.empty())
		{
			return false;
		}
		std::unordered_map<Button, InputAction>::const_iterator it = m_UpActionMap.find(_Button);
		if (it == m_UpActionMap.end())
		{
			// No mapping exists
			return false;
		}

		_Action = it->second;
		return true;
	}
}
//----------------------------------------------------------------------------------------------------------------------------
bool InputContext::MapButtonToState(Button _Button, InputState & _State)
{
	if (m_StateMap.empty())
	{
		return false;
	}

	std::unordered_map<Button, InputState>::const_iterator it = m_StateMap.find(_Button);
	if (it == m_StateMap.end())
	{
		// No mapping exists
		return false;
	}

	_State = it->second;
	return true;
}
//----------------------------------------------------------------------------------------------------------------------------
bool InputContext::MapAxisToRange(Axis _Axis, InputRange & _Range)
{
	if (_Axis == Mouse_Axis::X)
	{
		_Range = "HELIX_Mouse_X";
		return true;
	}
	if (_Axis == Mouse_Axis::Y)
	{
		_Range = "HELIX_Mouse_Y";
		return true;
	}

	if (m_MouseMap.empty())
	{
		return false;
	}

	std::unordered_map<Axis, InputRange>::const_iterator it = m_MouseMap.find(_Axis);
	if (it == m_MouseMap.end())
	{
		// No mapping exists
		return false;
	}

	_Range = it->second;
	return true;
}
//---------------------------------------------------------------------------------------------------
