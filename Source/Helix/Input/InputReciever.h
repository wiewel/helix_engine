//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef MAPPEDINPUT_H
#define MAPPEDINPUT_H

#include <set>
#include <string>
#include <unordered_map>
#include <concurrent_unordered_map.h>

#include "InputConstants.h"
#include "InputContext.h"
#include "hlx\src\String.h"

#include "Async\SpinLock.h"

namespace Helix
{
	namespace Input
	{
		class InputReciever
		{
		public:
			InputReciever();
			~InputReciever();

			void Initialize();
			void Uninitialize();

			void AddContext(const std::string& _sContextName, const hlx::string& _sConfigPath);
			void RemoveContext(const std::string& _sContextName);

			void Sync();
			void MapInput(HardwareInput& _Input);
			
			bool Action(InputAction _Action); 
			bool Action(InputAction _Action, uint32_t _uPlayerNumber);

			bool State(InputState _State);
			bool State(InputState _State, uint32_t _uPlayerNumber);

			bool Range(InputRange _Range, int32_t& _iValue);
			bool Range(InputRange _Range, int32_t& _iValue, uint32_t _uPlayerNumber);
			bool Mouse(int32_t& _iMouseX, int32_t& _iMouseY);
		
		private:
			void AddAction(InputAction _Action);
			void AddAction(InputAction _Action, uint32_t _uPlayerNumber);

			void AddState(InputState _State);
			void AddState(InputState _State, uint32_t _uPlayerNumber);
			void RemoveState(InputState _State);
			void RemoveState(InputState _State, uint32_t _uPlayerNumber);

			void AddRange(InputRange _Range, int32_t _iValue);
			void AddRange(InputRange _Range, int32_t _iValue, uint32_t _uPlayerNumber);
			void SetRange(InputRange _Range, int32_t _iValue);
			void SetRange(InputRange _Range, int32_t _iValue, uint32_t _uPlayerNumber);

		private:
			uint32_t m_uID;
			//const std::string m_sConfigPath;
			std::mutex m_Mutex;
			bool bIsLocked = false;
			std::set<InputAction> m_Actions;
			std::set<InputAction> m_ActionsReciever;
			std::set<InputState> m_States;
			std::set<InputState> m_StatesReciever;
			std::unordered_map<InputRange, int32_t> m_Ranges;
			std::unordered_map<InputRange, int32_t> m_RangesReciever;
			std::unordered_map<std::string, InputContext> m_InputContexts;
		};
	}
} 
#endif