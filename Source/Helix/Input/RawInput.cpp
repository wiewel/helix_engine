//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "hlx\src\StandardDefines.h"
#include "RawInput.h"
#include "hlx\src\Logger.h"
using namespace Helix::Input;

//=======================================
// INITAIALIZATION
//=======================================
//----------------------------------------------------------------------------------------------------------------------------
RawInput::RawInput() : IAsyncObject(nullptr, nullptr, true, false)
{
	//StartInNewThread(false);
}
//----------------------------------------------------------------------------------------------------------------------------
RawInput::~RawInput()
{
	DestroyWindow(m_hWnd);
	HFOREACH_CONST(it, end, m_Devices)
		it->second.Destroy();
	HFOREACH_CONST_END
}
//----------------------------------------------------------------------------------------------------------------------------
void RawInput::Initialize(HWND _hWND)
{
	// Create a message only window which is used as RIDEV_INPUTSINK
	Input::InputMapper::Instance()->Initialize("InputContexts.cfg");
	m_hWnd = _hWND;
	// Register all devices from which reading might be performed
	// these do not need to be connected
	//----------------------------------------------------------
	// usUsagePage typically is 0x01 for generic desktop controlls
	// When usUsagePage is 0x01, usUsage values are:
	// 0: undefined
	// 1: pointer
	// 2: mouse
	// 3: reserved
	// 4: joystick
	// 5: gamepad
	// 6: keyboard
	// 7: keypad
	// 8: multi-axis controller
	// 9: tablet-pc controlls
	// from http://www.toymaker.info/Games/html/raw_input.html
	RAWINPUTDEVICE Rid[3];

	// keyboard
	Rid[0].usUsagePage = 0x01;
	Rid[0].usUsage = 0x06; // keyboard
	Rid[0].dwFlags = 0 /*| RIDEV_DEVNOTIFY | RIDEV_INPUTSINK*/; // suppress legacy messages with RIDEV_NOLEGACY
	Rid[0].hwndTarget = m_hWnd;

	// mouse
	Rid[1].usUsagePage = 0x01;
	Rid[1].usUsage = 0x02; // mouse
	Rid[1].dwFlags = 0 /*| RIDEV_DEVNOTIFY | RIDEV_INPUTSINK*/; // suppress legacy messages with RIDEV_NOLEGACY
	Rid[1].hwndTarget = m_hWnd;

	// gamepad
	Rid[2].usUsagePage = 0x01;
	Rid[2].usUsage = 0x05; // gamepad
	Rid[2].dwFlags = RIDEV_DEVNOTIFY | RIDEV_INPUTSINK;
	Rid[2].hwndTarget = m_hWnd;

	if (RegisterRawInputDevices(Rid, 3, sizeof(Rid[0])) == FALSE) 
	{
		HERRORD("Unable to register input devices. %X", GetLastError());
	}
}
//----------------------------------------------------------------------------------------------------------------------------
void Helix::Input::RawInput::Execute()
{
	uint32_t uInactive = m_uActive;

	// move to the next staging buffer
	{
		Async::ScopedSpinLock Lock(m_InputLock);
		m_uActive = (m_uActive + 1u) % 2;
		m_PressedButtons[m_uActive] = m_PressedButtons[uInactive];
	}

	// add all button up events to the output
	for (auto it = m_PressedButtons[uInactive].begin(); it != m_PressedButtons[uInactive].end(); ++it)
	{
		if (m_HardwareInput[uInactive].m_ButtonInput.find(*it) == m_HardwareInput[uInactive].m_ButtonInput.end())
		{
			HardwareInput::ButtonInfo Info;
			Info.bDown = false;
			Info.bUp = true;
			m_HardwareInput[uInactive].m_ButtonInput[*it] = Info;
		}
	}
	
	// hand over the output to the input mapper
	InputMapper::Instance()->MapRawInput(m_HardwareInput[uInactive]);

	// clear the old buffer
	m_HardwareInput[uInactive].m_AxisInput[Mouse_Axis::X].iValue = 0;
	m_HardwareInput[uInactive].m_AxisInput[Mouse_Axis::Y].iValue = 0;
	m_HardwareInput[uInactive].m_ButtonInput.clear();

	// prevent too frequent updates
	std::this_thread::sleep_for(std::chrono::milliseconds(10));
}
//=======================================
// Read
//=======================================
//----------------------------------------------------------------------------------------------------------------------------
bool RawInput::Read(LPARAM _lParam, WPARAM _wParam)
{
	// Raw Input read
	UINT dwSize;
	GetRawInputData(reinterpret_cast<HRAWINPUT>(_lParam), RID_INPUT, NULL, &dwSize, sizeof(RAWINPUTHEADER));
	if (dwSize == 0)
	{
		return false;
	}
	if (m_RawBuffer.size() < dwSize)
	{
		m_RawBuffer.resize(dwSize);
	}

	if (GetRawInputData(reinterpret_cast<HRAWINPUT>(_lParam), RID_INPUT, &m_RawBuffer[0], &dwSize, sizeof(RAWINPUTHEADER)) != dwSize)
	{
		HWARNINGD("GetRawInputData does not return correct size !\n");
		return false;
	}

	RAWINPUT* raw = reinterpret_cast<RAWINPUT*>(&m_RawBuffer[0]);

	// Check which device and

	// keyboard (also mouse buttons)
	if (raw->header.dwType == RIM_TYPEKEYBOARD)
	{
		ReadKeyboardInput(raw);
	}
	// mouse
	else if (raw->header.dwType == RIM_TYPEMOUSE)
	{
		ReadMouseInput(raw);
	}
	else if (raw->header.dwType == RIM_TYPEHID)
	{
		ReadHIDInput(raw);
	}
	return true;
}
//----------------------------------------------------------------------------------------------------------------------------
void RawInput::OnDeviceChange(LPARAM _lParam, WPARAM _wParam)
{
	if (_wParam & GIDC_ARRIVAL)
	{
		Device Dev;
		if (InitializeHID(reinterpret_cast<HRAWINPUT>(_lParam), Dev))
		{
			m_Devices.emplace((HANDLE)_lParam, Dev);
		}
	}
	else if (_wParam & GIDC_REMOVAL)
	{
		HRAWINPUT key = reinterpret_cast<HRAWINPUT>(_lParam);
		m_Devices[key].Destroy();
		m_Devices.erase(key);
	}
}

#pragma region Keyboard
//----------------------------------------------------------------------------------------------------------------------------
void RawInput::ReadKeyboardInput(RAWINPUT * _pRaw)
{

	Button Btn = _pRaw->data.keyboard.VKey;
	HardwareInput::ButtonInfo KeyboardBtnInfo;
	KeyboardBtnInfo.bDown = _pRaw->data.keyboard.Flags == RI_KEY_MAKE;
	KeyboardBtnInfo.bUp = _pRaw->data.keyboard.Flags & RI_KEY_BREAK;

	Async::ScopedSpinLock Lock(m_InputLock);
	auto itr = m_PressedButtons[m_uActive].find(Btn);
	if (itr != m_PressedButtons[m_uActive].end())
	{
		KeyboardBtnInfo.bDown = false;
	}
	m_HardwareInput[m_uActive].m_ButtonInput[Btn] = KeyboardBtnInfo;

	if (KeyboardBtnInfo.bDown)
	{
		m_PressedButtons[m_uActive].emplace(Btn);
	}
	else if(KeyboardBtnInfo.bUp)
	{
		m_PressedButtons[m_uActive].erase(Btn);
	}
}
#pragma endregion

#pragma region Mouse
//----------------------------------------------------------------------------------------------------------------------------
void RawInput::ReadMouseInput(RAWINPUT * _pRaw)
{
	Async::ScopedSpinLock Lock(m_InputLock);

	//SetCursorPos(100, 100); // capture the cursor in the window isn't this hacky?
	InputMapper* InputMapper = InputMapper::Instance();
	if (_pRaw->data.mouse.usButtonFlags & RI_MOUSE_WHEEL)
	{
		// mouse wheel is always relative
		HardwareInput::AxisInfo Wheel;
		Wheel.iValue = _pRaw->data.mouse.usButtonData;
		m_HardwareInput[m_uActive].m_AxisInput[Mouse_Axis::WHEEL] = Wheel;
	}
	
	HardwareInput::AxisInfo XAxis;
	HardwareInput::AxisInfo YAxis;

	XAxis.iValue = static_cast<int32_t>(_pRaw->data.mouse.lLastX);
	YAxis.iValue = static_cast<int32_t>(_pRaw->data.mouse.lLastY);
	m_HardwareInput[m_uActive].m_AxisInput[Mouse_Axis::X] = XAxis;
	m_HardwareInput[m_uActive].m_AxisInput[Mouse_Axis::Y] = YAxis;

	HardwareInput::ButtonInfo MouseBtn;
	// Left mouse button
	if (_pRaw->data.mouse.usButtonFlags & RI_MOUSE_LEFT_BUTTON_DOWN)
	{
		MouseBtn.bDown = true;
		MouseBtn.bUp = false;
		m_PressedButtons[m_uActive].insert(Mouse_Btn::PRIMARY);
		m_HardwareInput[m_uActive].m_ButtonInput[Mouse_Btn::PRIMARY] = MouseBtn;
	}
	if (_pRaw->data.mouse.usButtonFlags & RI_MOUSE_LEFT_BUTTON_UP)
	{
		MouseBtn.bDown = false;
		MouseBtn.bUp = true;
		m_PressedButtons[m_uActive].erase(Mouse_Btn::PRIMARY);
		m_HardwareInput[m_uActive].m_ButtonInput[Mouse_Btn::PRIMARY] = MouseBtn;
	}
	// Right mouse button
	if (_pRaw->data.mouse.usButtonFlags & RI_MOUSE_RIGHT_BUTTON_DOWN)
	{
		MouseBtn.bDown = true;
		MouseBtn.bUp = false;
		m_PressedButtons[m_uActive].insert(Mouse_Btn::SECONDARY);
		m_HardwareInput[m_uActive].m_ButtonInput[Mouse_Btn::SECONDARY] = MouseBtn;
	}
	if (_pRaw->data.mouse.usButtonFlags & RI_MOUSE_RIGHT_BUTTON_UP)
	{
		MouseBtn.bDown = false;
		MouseBtn.bUp = true;
		m_PressedButtons[m_uActive].erase(Mouse_Btn::SECONDARY);
		m_HardwareInput[m_uActive].m_ButtonInput[Mouse_Btn::SECONDARY] = MouseBtn;
	}
	// Middle mouse button
	if (_pRaw->data.mouse.usButtonFlags & RI_MOUSE_MIDDLE_BUTTON_DOWN)
	{
		MouseBtn.bDown = true;
		MouseBtn.bUp = false;
		m_PressedButtons[m_uActive].insert(Mouse_Btn::MIDDLE);
		m_HardwareInput[m_uActive].m_ButtonInput[Mouse_Btn::MIDDLE] = MouseBtn;
	}
	if (_pRaw->data.mouse.usButtonFlags & RI_MOUSE_MIDDLE_BUTTON_UP)
	{
		MouseBtn.bDown = false;
		MouseBtn.bUp = true;
		m_PressedButtons[m_uActive].erase(Mouse_Btn::MIDDLE);
		m_HardwareInput[m_uActive].m_ButtonInput[Mouse_Btn::MIDDLE] = MouseBtn;
	}
	// X1 mouse button
	if (_pRaw->data.mouse.usButtonFlags & RI_MOUSE_BUTTON_4_DOWN)
	{
		MouseBtn.bDown = true;
		MouseBtn.bUp = false;
		m_PressedButtons[m_uActive].insert(Mouse_Btn::X1);
		m_HardwareInput[m_uActive].m_ButtonInput[Mouse_Btn::X1] = MouseBtn;
	}
	if (_pRaw->data.mouse.usButtonFlags & RI_MOUSE_BUTTON_4_UP)
	{
		MouseBtn.bDown = false;
		MouseBtn.bUp = true;
		m_PressedButtons[m_uActive].erase(Mouse_Btn::X1);

		m_HardwareInput[m_uActive].m_ButtonInput[Mouse_Btn::X1] = MouseBtn;
	}
	// X2 mouse button
	if (_pRaw->data.mouse.usButtonFlags & RI_MOUSE_BUTTON_5_DOWN)
	{
		MouseBtn.bDown = true;
		MouseBtn.bUp = false;
		m_PressedButtons[m_uActive].insert(Mouse_Btn::X2);
		m_HardwareInput[m_uActive].m_ButtonInput[Mouse_Btn::X2] = MouseBtn;
	}
	if (_pRaw->data.mouse.usButtonFlags & RI_MOUSE_BUTTON_5_UP)
	{
		MouseBtn.bDown = false;
		MouseBtn.bUp = true;
		m_PressedButtons[m_uActive].erase(Mouse_Btn::X2);
		m_HardwareInput[m_uActive].m_ButtonInput[Mouse_Btn::X2] = MouseBtn;
	}
}
#pragma endregion

#pragma region Generic HID
//----------------------------------------------------------------------------------------------------------------------------
bool RawInput::InitializeHID(HANDLE _hDevice, Device& _Device)
{
	Device Dev;
	UINT uBufferSize = 0;
	USHORT uCapsLength = 0;
	Dev.Handle = _hDevice;

	// Get preparsed data
	GetRawInputDeviceInfo(_hDevice, RIDI_PREPARSEDDATA, nullptr, &uBufferSize);
	Dev.pPreparsedData = reinterpret_cast<PHIDP_PREPARSED_DATA>(new BYTE[uBufferSize]);
	if (Dev.pPreparsedData == nullptr)
	{
		Dev.Destroy();
		return false;
	}
	if (GetRawInputDeviceInfo(_hDevice, RIDI_PREPARSEDDATA, Dev.pPreparsedData, &uBufferSize) < 0)
	{
		HWARNINGD("GetRawInputDeviceInfo does not return the correct size!");
		Dev.Destroy();
		return false;
	}

	// Get capabilities
	if (HidP_GetCaps(Dev.pPreparsedData, &Dev.Caps) != HIDP_STATUS_SUCCESS)
	{
		Dev.Destroy();
		return false;
	}

	// Get button capabilities
	uCapsLength = Dev.Caps.NumberInputButtonCaps;
	Dev.pButtonCaps = reinterpret_cast<PHIDP_BUTTON_CAPS>( new HIDP_BUTTON_CAPS[uCapsLength]);
	if (Dev.pButtonCaps == nullptr)
	{
		Dev.Destroy();
		return false;
	}
	if (HidP_GetButtonCaps(HidP_Input, Dev.pButtonCaps, &uCapsLength, Dev.pPreparsedData) != HIDP_STATUS_SUCCESS)
	{
		Dev.Destroy();
		return false;
	}
	Dev.uNumberOfButtons = Dev.pButtonCaps->Range.UsageMax - Dev.pButtonCaps->Range.UsageMin + 1;
	//Dev.PressedButtons.resize(Dev.uNumberOfButtons,false);

	// Getvalue capabilities
	uCapsLength = Dev.Caps.NumberInputValueCaps;
	Dev.pValueCaps = reinterpret_cast<PHIDP_VALUE_CAPS>( new  HIDP_VALUE_CAPS[uCapsLength]);
	if (Dev.pValueCaps == nullptr)
	{
		Dev.Destroy();
		return false;
	}
	if (HidP_GetValueCaps(HidP_Input, Dev.pValueCaps, &uCapsLength, Dev.pPreparsedData) != HIDP_STATUS_SUCCESS)
	{
		Dev.Destroy();
		return false;
	}

	Dev.Info.cbSize = sizeof(RID_DEVICE_INFO);
	uBufferSize = Dev.Info.cbSize;
	GetRawInputDeviceInfo(_hDevice, RIDI_DEVICEINFO, &Dev.Info, &uBufferSize);

	_Device = Dev;
	//HFATALD("Input device registered. Vendor: %u Product: %u", Dev.Info.hid.dwVendorId, Dev.Info.hid.dwProductId);
	return true;
}
//----------------------------------------------------------------------------------------------------------------------------
void RawInput::ReadHIDInput(RAWINPUT * _pRaw)
{
	if (m_Devices.empty())
	{
		HLOGD("Recieved input from an unknown device");
		return;
	}
	std::map<HANDLE, Device>::iterator DevIt = m_Devices.find(_pRaw->header.hDevice);
	if (DevIt == m_Devices.end())
	{
		HLOGD("Recieved input from an unknown device");
		return;
	}
	InputConstants::GamepadMaps m_GamepadMap;
	if (DevIt->second.Info.hid.dwVendorId == 1118u && DevIt->second.Info.hid.dwProductId == 721u)
	{
		// XBONE controller
		m_GamepadMap = InputConstants::Instance()->m_XBONE;
	}
	else if (DevIt->second.Info.hid.dwVendorId == 1118u && DevIt->second.Info.hid.dwProductId == 673u)
	{
		// XBONE controller
		m_GamepadMap = InputConstants::Instance()->m_XBox360;
	}
	// Read button input
	ULONG uUsageLength = DevIt->second.uNumberOfButtons;
	ZeroMemory(m_ButtonUsageBuffer, sizeof(USAGE[MAX_BUTTONS]));
	NTSTATUS Status = HidP_GetUsages(
		HidP_Input, 
		DevIt->second.pButtonCaps->UsagePage, 
		0, 
		m_ButtonUsageBuffer, 
		&uUsageLength, 
		DevIt->second.pPreparsedData,
		reinterpret_cast<PCHAR>(_pRaw->data.hid.bRawData), 
		_pRaw->data.hid.dwSizeHid);
	if (Status != HIDP_STATUS_SUCCESS)
	{
		return;
	}

	std::set<Button> PressedButtons;
	for (uint32_t i = 0u; i < uUsageLength; ++i)
	{
		std::unordered_map<uint32_t, Button>::iterator it = m_GamepadMap.m_Buttons.find(m_ButtonUsageBuffer[i]);
		if (it != m_GamepadMap.m_Buttons.end())
		{
			Async::ScopedSpinLock Lock(m_InputLock);

			Button Btn = it->second;
			HardwareInput::ButtonInfo BtnInfo;
			PressedButtons.insert(Btn);
			auto itr = m_PressedButtons[m_uActive].find(Btn);
			if (itr != m_PressedButtons[m_uActive].end())
			{
				BtnInfo.bDown = false;
				BtnInfo.bUp = false;
			}
			else
			{
				BtnInfo.bDown = true;
				BtnInfo.bUp = false;
				m_PressedButtons[m_uActive].emplace(Btn);
				DevIt->second.PreviouslyPressedBtns.push_back(Btn);
			}
			m_HardwareInput[m_uActive].m_ButtonInput[Btn] = BtnInfo;
		}
	}
	// CHeck if button is no longer pressed -> up action
	//if(DevIt->second.PreviouslyPressedBtns.empty() )
	for (auto it = DevIt->second.PreviouslyPressedBtns.begin(); it != DevIt->second.PreviouslyPressedBtns.end(); ) 
	{
		Button Btn = *it;
		if (PressedButtons.find(Btn) == PressedButtons.end())
		{
			Async::ScopedSpinLock Lock(m_InputLock);
			HardwareInput::ButtonInfo BtnInfo;
			BtnInfo.bDown = false;
			BtnInfo.bUp = true;
			m_HardwareInput[m_uActive].m_ButtonInput[Btn] = BtnInfo;
			m_PressedButtons[m_uActive].erase(Btn);
			it = DevIt->second.PreviouslyPressedBtns.erase(it);
		}
		else
		{
			++it;
		}
	}

	// Read value input
	ULONG uValue;
	//std::vector<int32_t> Vals;
	for (int i = 0; i < DevIt->second.Caps.NumberInputValueCaps; ++i)
	{
		Status = HidP_GetUsageValue(
			HidP_Input,
			DevIt->second.pValueCaps[i].UsagePage,
			0,
			DevIt->second.pValueCaps[i].Range.UsageMin,
			&uValue, 
			DevIt->second.pPreparsedData,
			reinterpret_cast<PCHAR>(_pRaw->data.hid.bRawData),
			_pRaw->data.hid.dwSizeHid);
		if (Status != HIDP_STATUS_SUCCESS)
		{
			continue;
		}
		//Vals.push_back(static_cast<int32_t>(uValue));
		std::unordered_map<uint32_t, InputConstants::GamepadMaps::GamepadAxisInfo>::iterator it = m_GamepadMap.m_Axis.find(i);
		if (it != m_GamepadMap.m_Axis.end())
		{
			Async::ScopedSpinLock Lock(m_InputLock);
			Axis Ax = it->second.Axis;
			HardwareInput::AxisInfo AxInfo;
			AxInfo.iValue = static_cast<int32_t>(uValue) * it->second.m_uPrecision;
			if (it->second.bIsTwoDirectional)
			{
				AxInfo.iValue -= 32768;
			}
			m_HardwareInput[m_uActive].m_AxisInput[Ax] = AxInfo;
		}
	}
}
#pragma endregion

//=======================================
// WINDOW MESSAGES
//=======================================
#pragma region Message Only Window Procedure
//----------------------------------------------------------------------------------------------------------------------------
LRESULT RawInput::ProcessInputMsg(HWND _hWnd, uint32_t _uMessage, WPARAM _wParam, LPARAM _lParam)
{
	switch (_uMessage)
	{
		case WM_INPUT:
		{
			Instance()->Read(_lParam, _wParam);
			//return ERROR_SUCCESS;
		} break;
		case WM_INPUT_DEVICE_CHANGE:
		{
			Instance()->OnDeviceChange(_lParam, _wParam);
			//return ERROR_SUCCESS;
		} break;
	}

	return false;//DefWindowProc(_hWnd, _uMessage, _wParam, _lParam);
}
#pragma endregion