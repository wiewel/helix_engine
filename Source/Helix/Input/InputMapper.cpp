//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "InputMapper.h"
#include "InputReciever.h"

using namespace Helix::Input;

//----------------------------------------------------------------------------------------------------------------------------
InputMapper::InputMapper()
{
}
//----------------------------------------------------------------------------------------------------------------------------
InputMapper::~InputMapper()
{
	m_InputRecievers.clear();
}
//----------------------------------------------------------------------------------------------------------------------------
void InputMapper::Initialize(std::string _sConfigPath)
{
	m_sConfigPath = _sConfigPath;
}
//----------------------------------------------------------------------------------------------------------------------------
uint32_t InputMapper::RegisterInputReciever(InputReciever* _pInputReciever)
{
	Async::ScopedSpinLock Lock(m_InputReceiversLock);
	uint32_t ID = ++m_uRecieverID;
	m_InputRecievers.insert({ ID, _pInputReciever });
	return ID;
}
////----------------------------------------------------------------------------------------------------------------------------
void InputMapper::UnregisterInputReciever(uint32_t _uID)
{
	Async::ScopedSpinLock Lock(m_InputReceiversLock);
	auto RemoveIt = m_InputRecievers.find(_uID);
	if (RemoveIt != m_InputRecievers.end())
	{
		m_InputRecievers.erase(RemoveIt);
	}
}

//----------------------------------------------------------------------------------------------------------------------------
void InputMapper::MapRawInput(HardwareInput& _Input)
{
	Async::ScopedSpinLock Lock(m_InputReceiversLock);
	HFOREACH_CONST(it, end, m_InputRecievers)
		if (it->second != nullptr)
		{
			it->second->MapInput(_Input);
		}
	HFOREACH_CONST_END
}