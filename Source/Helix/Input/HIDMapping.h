//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

/////////////////////////////////////////////////////////////////////////////////////////////////
// HID Mapping
//-----------------------------------------------------------------------------------------------
// Summary:
// Maps raw input hid buttons and values to helix input scheme.
//-----------------------------------------------------------------------------------------------
// Future Work:
// - Editor
/////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef HIDMAPPING_H
#define HIDMAPPING_H
#include <map>
#include <vector>
#include <array>
#include "hlx\src\StandardDefines.h"
#include "hlx\src\String.h"

namespace Helix
{
	namespace Input
	{
		class HIDMapping
		{
		private:
			typedef uint32_t Button;
			typedef uint32_t Axis;
			struct AxisToButton
			{
				Axis uRIAxis;
				uint32_t uThreshold;
				Button uHIButtonLow;
				Button uHIButtonHigh;
				Button Map(uint32_t _uValue) const
				{ 
					return _uValue < uThreshold ? uHIButtonLow : uHIButtonHigh;
				}
			};
			struct ButtonToAxis
			{
				Button uRIButtonLow;
				Button uRIButtonHigh;
				float fValueMin;
				float fValueMax;
				Axis uHIAxis;
				float Map(Button _Button) const
				{
					return _Button == uRIButtonLow ? fValueMin : fValueMax;
				}
			};
			struct AxisToAxis
			{
				Axis uRIAxis;
				uint32_t uRIMin;
				uint32_t uRIMax;
				uint32_t uRIDeadzoneFrom;
				uint32_t uRIDeadzoneTo;
				float fHIMin;
				float fHIMax;
				Axis uHIAxis;
				float Map(uint32_t _uValue) const
				{
					if (_uValue >= uRIDeadzoneFrom && _uValue <= uRIDeadzoneTo)
					{
						return 0.5f * (fHIMax - fHIMin) + fHIMin;
					}
					float fNormalized = (_uValue - uRIMin) / static_cast<float>((uRIMax - uRIMin));
					return (fNormalized) * (fHIMax - fHIMin) + fHIMin;
				}
			};
			struct AxisToDigipad
			{
				Axis uRIAxis;
				std::array<uint32_t, 8> ValueMap;
				Button Map(uint32_t _uValue) const
				{
					return ValueMap[_uValue];
				}
			};
		public:
			HIDMapping();
			~HIDMapping();
			bool Initialize(std::string _sDeviceName, hlx::string _sConfigPath);

			bool GetButtonFromButton(_In_ Button _uRIButton, _Out_ Button& _uHIButton) const;
			bool GetButtonFromAxis(_In_ Axis _RIAxis, _In_ uint32_t _RIValue, _Out_ Button& _HIButtonDown, _Out_ std::vector<Button>& _HIButtonsUp) const;
			bool GetAxisFromButton(_In_ Button _uRIButton, _Out_ Axis& _uHIAxis, _Out_ float& _fValue) const;
			bool GetAxisFromAxis(_In_ Axis _uRIAxis, _In_ uint32_t _fRIValue, _Out_ Axis& _uHIAxis, _Out_ float& _fHIValue) const;
			uint32_t GetVendorID() const;
			uint32_t GetProductID() const;

		private:
			uint32_t m_uVendorID;
			uint32_t m_uProductID;
			std::map<Button, Button> m_ButtonToButtonMappings;
			std::map<Axis, AxisToButton> m_AxisToButtonMappings;
			std::map<Axis, AxisToDigipad> m_AxisToDigipadMappings;
			std::map<Button, ButtonToAxis> m_ButtonToAxisMappings;
			std::map<Axis, AxisToAxis> m_AxisToAxisMappings;
		};
		inline uint32_t HIDMapping::GetVendorID() const { return m_uVendorID; }
		inline uint32_t HIDMapping::GetProductID() const
		{
			return m_uProductID;
		}
	}
}

#endif