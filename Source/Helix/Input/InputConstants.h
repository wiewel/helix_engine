//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

/////////////////////////////////////////////////////////////////////////////////////////////////
// InputConstants
//-----------------------------------------------------------------------------------------------
// Summary:
// This File defines all the HELiX raw input keywords and their value.
/////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef INPUTCONSTANTS_H
#define INPUTCONSTANTS_H
#include <unordered_map>
#include <map>
//#include "HIDMapping.h"
#include "hlx\src\StandardDefines.h"
#include "hlx\src\Singleton.h"

#define MAX_BUTTONS	32
#define MAX_AXES 32

// Constants for identifying raw inputs from hardware/OS layers
namespace Helix
{
	namespace Input
	{
		typedef uint32_t Button;
		typedef uint32_t Axis;
		enum class HIDType
		{
			Keyboard,
			Mouse,
			Gamepad,
		};
		typedef std::string InputAction;
		typedef std::string InputState;
		typedef std::string InputRange;
		
		struct HardwareInput
		{
			struct ButtonInfo
			{
				bool bDown;
				bool bUp;
			};
			struct AxisInfo
			{
				int32_t iValue;
			};
			std::unordered_map<Button, ButtonInfo> m_ButtonInput;
			std::unordered_map<Axis, AxisInfo> m_AxisInput;
		};
		namespace Keyboard_Btn
		{
			// keyboard buttons supported by helix. compatible with windows virtual key codes
			enum Keyboard_Btn : uint64_t
			{
				Undefined = 0,
				BACK = VK_BACK,
				RETURN = VK_RETURN,
				SPACE = VK_SPACE,
				TAB = VK_TAB,
				SHIFT = VK_SHIFT,
				LSHIFT = VK_LSHIFT,
				RSHIFT = VK_RSHIFT,
				CAPSLOCK = VK_CAPITAL,
				CONTROL = VK_CONTROL,
				LCONTROL = VK_LCONTROL,
				RCONTROL = VK_RCONTROL,
				LSUPER = VK_LWIN,
				RSUPER = VK_RWIN,
				ESC = VK_ESCAPE,
				PAUSE = VK_PAUSE,
				PAGEUP = VK_PRIOR,
				PAGEDOWN = VK_NEXT,
				END = VK_END,
				HOME = VK_HOME,
				SELECT = VK_SELECT,
				PRINT = VK_PRINT,
				PRINTSCREEN = VK_SNAPSHOT,
				INSERT = VK_INSERT,
				DEL = VK_DELETE,
				LEFT = VK_LEFT,
				UP = VK_UP,
				RIGHT = VK_RIGHT,
				DOWN = VK_DOWN,
				NUM0 = 0x30,
				NUM1,
				NUM2,
				NUM3,
				NUM4,
				NUM5,
				NUM6,
				NUM7,
				NUM8,
				NUM9,
				A = 0x41,
				B,
				C,
				D,
				E,
				F,
				G,
				H,
				I,
				J,
				K,
				L,
				M,
				N,
				O,
				P,
				Q,
				R,
				S,
				T,
				U,
				V,
				W,
				X,
				Y,
				Z,
				NUMPAD0 = VK_NUMPAD0,
				NUMPAD1 = VK_NUMPAD1,
				NUMPAD2 = VK_NUMPAD2,
				NUMPAD3 = VK_NUMPAD3,
				NUMPAD4 = VK_NUMPAD4,
				NUMPAD5 = VK_NUMPAD5,
				NUMPAD6 = VK_NUMPAD6,
				NUMPAD7 = VK_NUMPAD7,
				NUMPAD8 = VK_NUMPAD8,
				NUMPAD9 = VK_NUMPAD9,
				MULTIPLY = VK_MULTIPLY,
				ADD = VK_ADD,
				SEPARATOR = VK_SEPARATOR,
				SUBTRACT = VK_SUBTRACT,
				DECIMAL = VK_DECIMAL,
				DIVIDE = VK_DIVIDE,
				F1 = VK_F1,
				F2 = VK_F2,
				F3 = VK_F3,
				F4 = VK_F4,
				F5 = VK_F5,
				F6 = VK_F6,
				F7 = VK_F7,
				F8 = VK_F8,
				F9 = VK_F9,
				F10 = VK_F10,
				F11 = VK_F11,
				F12 = VK_F12,
				F13 = VK_F13,
				F14 = VK_F14,
				F15 = VK_F15,
				F16 = VK_F16,
				F17 = VK_F17,
				F18 = VK_F18,
				F19 = VK_F19,
				F20 = VK_F20,
				F21 = VK_F21,
				F22 = VK_F22,
				F23 = VK_F23,
				F24 = VK_F24,
				OEM1 = VK_OEM_1,
				PLUS = VK_OEM_PLUS,
				COMMA = VK_OEM_COMMA,
				MINUS = VK_OEM_MINUS,
				PERIOD = VK_OEM_PERIOD,
				OEM2 = VK_OEM_2,
				OEM3 = VK_OEM_3,
				OEM4 = VK_OEM_4,
				OEM5 = VK_OEM_5,
				OEM6 = VK_OEM_6,
				OEM7 = VK_OEM_7,
				OEM8 = VK_OEM_8,
				Count = 256
			};
		}	
		namespace Mouse_Btn
		{
			enum Mouse_Btn : uint64_t
			{
				PRIMARY = static_cast<uint64_t>(Keyboard_Btn::Count),
				SECONDARY,
				MIDDLE,
				X1,
				X2,
				Count
			};
		}
		namespace Mouse_Axis
		{
			enum Mouse_Axis : uint64_t
			{
				X = static_cast<uint64_t>(Mouse_Btn::Count),
				Y,
				WHEEL,
				Count
			};
		}


		class InputConstants : public hlx::Singleton<InputConstants>
		{
			friend class RawInput;
		private:
			struct GamepadMaps
			{
				struct GamepadAxisInfo
				{
					Axis Axis = 0;
					uint32_t m_uPrecision = 1; // reference is 65536
					bool bIsTwoDirectional = true;
				};
				std::unordered_map<uint32_t, Button> m_Buttons;
				std::unordered_map<uint32_t, GamepadAxisInfo> m_Axis;
			};
		public:
			InputConstants();
			bool GetButton(const std::string _ButtonName, Button& _Button) const;
			bool GetAxis(const std::string _AxisName, Axis& _Axis) const;
		private:
			std::unordered_map<std::string, Button> m_ButtonNames;
			std::unordered_map<std::string, Axis> m_AxisNames;
			GamepadMaps m_XBONE;
			GamepadMaps m_XBox360;

			//GamepadMaps m_DS4;
			uint32_t m_uHIDButtonOffset;
			uint32_t m_uHIDAxisOffset;
		};
		inline bool InputConstants::GetButton(const std::string _ButtonName, Button& _Button) const 
		{
			auto it = m_ButtonNames.find(_ButtonName);
			if (it != m_ButtonNames.end())
			{
				_Button = it->second;
				return true;
			}
			return false;
		}
		inline bool InputConstants::GetAxis(const std::string _AxisName, Axis& _Axis) const 
		{ 
			auto it = m_AxisNames.find(_AxisName);
			if (it != m_AxisNames.end())
			{
				_Axis = it->second;
				return true;
			}
			return false;
		}
	}
}
#endif