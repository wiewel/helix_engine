//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "InputReciever.h"
#include "InputContext.h"
#include "InputMapper.h"

using namespace Helix::Input;

//---------------------------------------------------------------------------------------------------
Helix::Input::InputReciever::InputReciever()
{
	
}
//---------------------------------------------------------------------------------------------------
Helix::Input::InputReciever::~InputReciever()
{
}
//---------------------------------------------------------------------------------------------------
void Helix::Input::InputReciever::Initialize()
{
	m_uID = InputMapper::Instance()->RegisterInputReciever(this);
}
//---------------------------------------------------------------------------------------------------
void Helix::Input::InputReciever::Uninitialize()
{
	InputMapper::Instance()->UnregisterInputReciever(m_uID);
}
//---------------------------------------------------------------------------------------------------
void InputReciever::AddContext(const std::string& _sContextName, const hlx::string& _sConfigPath)
{
	std::lock_guard<std::mutex> Lock(m_Mutex);
	auto it = m_InputContexts.find(_sContextName);
	if (it == m_InputContexts.end())
	{
		m_InputContexts[_sContextName] = InputContext();
		m_InputContexts[_sContextName].Reload(_sContextName, _sConfigPath);
	}
	else
	{
		it->second.Activate();
	}
}
//---------------------------------------------------------------------------------------------------
void InputReciever::RemoveContext(const std::string& _sContextName)
{
	std::lock_guard<std::mutex> Lock(m_Mutex);
	auto it = m_InputContexts.find(_sContextName);
	if (it != m_InputContexts.end())
	{
		it->second.Deactivate();
	}
}
//---------------------------------------------------------------------------------------------------
void InputReciever::Sync()
{
	std::lock_guard<std::mutex> Lock(m_Mutex);
	m_Actions.swap(m_ActionsReciever);
	m_ActionsReciever.clear();
	m_States.swap(m_StatesReciever);
	m_StatesReciever.clear();
	m_Ranges.swap(m_RangesReciever);
	m_RangesReciever.clear();
}
//---------------------------------------------------------------------------------------------------
void InputReciever::MapInput(HardwareInput & _Input)
{
	std::lock_guard<std::mutex> Lock(m_Mutex);
	HFOREACH_CONST(it, end, m_InputContexts)
		InputContext* pContext = &it->second;
		if (pContext->IsActive())
		{
			// map buttons
			HFOREACH_CONST(BtnInfo, end, _Input.m_ButtonInput)
				InputAction Action;
				if (pContext->MapButtonToAction(BtnInfo->first, BtnInfo->second.bDown, Action))
				{
					AddAction(Action);
				}
				InputState State;
				if (pContext->MapButtonToState(BtnInfo->first, State))
				{
					AddState(State);
				}
			HFOREACH_CONST_END
			// Map axes
			HFOREACH_CONST(AxisInfo, end, _Input.m_AxisInput)
				InputRange Range;
				if (pContext->MapAxisToRange(AxisInfo->first, Range))
				{
					AddRange(Range, AxisInfo->second.iValue);
				}
			HFOREACH_CONST_END
		}
	HFOREACH_CONST_END
}
//---------------------------------------------------------------------------------------------------
// Check and consume action
bool InputReciever::Action(InputAction _Action)
{
	return m_Actions.erase(_Action) != 0;
}
//---------------------------------------------------------------------------------------------------
// Check and consume player specific action
bool InputReciever::Action(InputAction _Action, uint32_t _uPlayerNumber) 
{ 
	return Action(std::to_string(_uPlayerNumber) + ":" + _Action); 
}
//---------------------------------------------------------------------------------------------------
// Check state
bool InputReciever::State(InputState _State) 
{ 
	return m_States.find(_State) != m_States.end(); 
}
//---------------------------------------------------------------------------------------------------
// Check player specific state
bool InputReciever::State(InputState _State, uint32_t _uPlayerNumber) 
{ 
	return State(std::to_string(_uPlayerNumber) + ":" + _State); 
}
//---------------------------------------------------------------------------------------------------
// Check, read (into _fValue) and consume range
bool InputReciever::Range(InputRange _Range, int32_t & _iValue)
{
	std::unordered_map<InputRange, int32_t>::iterator it = m_Ranges.find(_Range);
	if (it == m_Ranges.end())
	{
		return false;
	}
	_iValue = it->second;
	//m_Ranges.erase(it);
	return true;
}
//---------------------------------------------------------------------------------------------------
// Check, read (into _fValue) and consume player specific range
bool InputReciever::Range(InputRange _Range, int32_t & _iValue, uint32_t _uPlayerNumber) 
{ 
	return Range(std::to_string(_uPlayerNumber) + ":" + _Range, _iValue); 
}
//---------------------------------------------------------------------------------------------------
bool InputReciever::Mouse(int32_t & _iMouseX, int32_t & _iMouseY)
{
	bool Success = false;
	_iMouseX = 0;
	_iMouseY = 0;
	std::unordered_map<InputRange, int32_t>::iterator itr = m_Ranges.find("HELIX_Mouse_X");
	if (itr != m_Ranges.end())
	{
		Success = true;
		_iMouseX = itr->second;
		itr->second = 0;
	}

	itr = m_Ranges.find("HELIX_Mouse_Y");
	if (itr != m_Ranges.end())
	{
		Success = true;
		_iMouseY = itr->second;
		itr->second = 0;
	}
	return Success;
}
//---------------------------------------------------------------------------------------------------
// Adds a general action only
void InputReciever::AddAction(InputAction _Action) 
{ 
	m_ActionsReciever.emplace(_Action); 
}
//---------------------------------------------------------------------------------------------------
// Adds a general and a player specific action
void InputReciever::AddAction(InputAction _Action, uint32_t _uPlayerNumber) 
{ 
	AddAction(_Action); AddAction(std::to_string(_uPlayerNumber) + ":" + _Action); 
}
//---------------------------------------------------------------------------------------------------
// Adds a general state only
void InputReciever::AddState(InputState _State) 
{ 
	m_StatesReciever.insert(_State); 
}
//---------------------------------------------------------------------------------------------------
// Adds a general and a player specific state
void InputReciever::AddState(InputState _State, uint32_t _uPlayerNumber) 
{ 
	AddState(_State); AddState(std::to_string(_uPlayerNumber) + ":" + _State); 
}
//---------------------------------------------------------------------------------------------------
// Removes a general state from the list (on button up)
void InputReciever::RemoveState(InputState _State) 
{ 
	m_StatesReciever.erase(_State); 
}
//---------------------------------------------------------------------------------------------------
// Removes a general and a player specific state from the list (on button up)
void InputReciever::RemoveState(InputState _State, uint32_t _uPlayerNumber) 
{ 
	RemoveState(_State); RemoveState(std::to_string(_uPlayerNumber) + ":" + _State); 
}
//---------------------------------------------------------------------------------------------------
// Adds a general range
void InputReciever::AddRange(InputRange _Range, int32_t _iValue) 
{ 
	m_RangesReciever[_Range] += _iValue; 
}
//---------------------------------------------------------------------------------------------------
// Adds a general and a player specific range
void InputReciever::AddRange(InputRange _Range, int32_t _iValue, uint32_t _uPlayerNumber) 
{ 
	AddRange(_Range, _iValue); AddRange(std::to_string(_uPlayerNumber) + ":" + _Range, _iValue); 
}
//---------------------------------------------------------------------------------------------------
// Sets a geneneral range to a value
void InputReciever::SetRange(InputRange _Range, int32_t _iValue) 
{ 
	m_RangesReciever[_Range] = _iValue; 
}
//---------------------------------------------------------------------------------------------------
// Sets a general and a player specific range to a value
void InputReciever::SetRange(InputRange _Range, int32_t _iValue, uint32_t _uPlayerNumber) 
{
	SetRange(_Range, _iValue); SetRange(std::to_string(_uPlayerNumber) + ":" + _Range, _iValue); 
}
//---------------------------------------------------------------------------------------------------