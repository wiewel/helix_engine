//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

/////////////////////////////////////////////////////////////////////////////////////////////////
// InputMapper
//-----------------------------------------------------------------------------------------------
// Summary:
//
// Maps Raw input tp game specific actions, states and ranges
//-----------------------------------------------------------------------------------------------
// MappedInput is the struct which gets passed to every OnInput callback.
//-----------------------------------------------------------------------------------------------
// Future Work:
// - Add more Accessors to Mapped Input
// - Load all mappings from HIDMappings.ini -> texttoken issue
/////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef INPUTMAPPER_H
#define INPUTMAPPER_H

#include <string>
#include <map>
#include "hlx\src\Singleton.h"
#include "hlx\src\Logger.h"
#include <atomic>
#include "Async\SpinLock.h"
#include "InputConstants.h"
namespace Helix
{
	namespace Input
	{
		class InputReciever;
		class InputMapper : public hlx::Singleton<InputMapper>
		{
			friend class RawInput;
			HDEBUGNAME("InputMapper");
		public:
			// Init and shutdown
			InputMapper();
			~InputMapper();
			void Initialize(std::string _sConfigPath);

			uint32_t RegisterInputReciever(InputReciever* _pInputReciever);
			void UnregisterInputReciever(uint32_t _uID);

		private:
			// Recieve Input
			void MapRawInput(HardwareInput& _Input);
		private:
			std::uint32_t m_uRecieverID = 0u;
			std::string m_sConfigPath;
			Async::SpinLock m_InputReceiversLock;
			std::unordered_map<uint32_t, InputReciever*> m_InputRecievers;
		};
	}
}
#endif