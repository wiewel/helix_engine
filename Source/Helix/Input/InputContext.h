//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

/////////////////////////////////////////////////////////////////////////////////////////////////
// InputContext
//-----------------------------------------------------------------------------------------------
// Summary:
//
// Each context describes a scenario in which the game handles user input. It maps raw input 
// like e.g. button presses to abstract input consisting of actions, states and ranges.
//-----------------------------------------------------------------------------------------------
// Furure Work:
// Add convenience naming for various gamepads
/////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef INPUTCONTEXT_H
#define INPUTCONTEXT_H

#include <string>
#include <unordered_map>

#include "InputConstants.h"
#include "hlx\src\String.h"

namespace Helix
{
	namespace Input
	{
		class InputContext
		{
		public:
			InputContext();
			~InputContext();

			bool Reload(const std::string _sContextName, const hlx::string _sConfigFile);
			bool IsActive();
			void Activate();
			void Deactivate();

			bool MapButtonToAction(_In_ Button _Button, _In_ bool _bIsDownAction, _Out_ InputAction& _Action);
			bool MapButtonToState(_In_ Button _Button, _Out_ InputState& _State);
			bool MapAxisToRange(_In_ Axis _Axis, _Out_ InputRange& _Range);
		
		private:
			bool m_bIsActive = true;

			std::unordered_map<Button, InputAction> m_DownActionMap;
			std::unordered_map<Button, InputAction> m_UpActionMap;
			std::unordered_map<Button, InputState> m_StateMap;
			std::unordered_map<Axis, InputRange> m_MouseMap;
		};
	}
}


#endif