//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

/////////////////////////////////////////////////////////////////////////////////////////////////
// RawInput
//-----------------------------------------------------------------------------------------------
// Summary:
//
// Recieves raw input data from the OS, interprets it and the passes it to the input mapper.
// We use neither XINPUT nor DirectInput but the Raw Input API.
//
// - Recieves raw input data from all supported HIDs
//-----------------------------------------------------------------------------------------------
// Look at InputConstants for all HELiX raw input codes
//-----------------------------------------------------------------------------------------------
// Future Work:
// - DS4 support
// - XBox360, XBoxOne controller support
// - Rumble support
/////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef RAWINPUT_H
#define RAWINPUT_H

#include "InputMapper.h"
#include "InputConstants.h"
#include "hlx\src\Singleton.h"
#include "Util\Flag.h"
#include "hlx\src\Bytes.h"
#include "Async\AsyncObject.h"
#include <vector>
#include <set>
#include "Async\SpinLock.h"

extern "C"
{
	#include <hidsdi.h>
	#include <hidclass.h>
}


namespace Helix
{
	namespace Input
	{
		class RawInput : public Async::IAsyncObject, public hlx::Singleton<RawInput>
		{
			HDEBUGNAME("RawInput");
		private:
			struct Device
			{
				void Destroy()
				{
					HSAFE_DELETE(pValueCaps);
					HSAFE_DELETE(pButtonCaps);
					HidD_FreePreparsedData(pPreparsedData);
				}
				HANDLE Handle;
				HIDP_CAPS Caps;
				RID_DEVICE_INFO Info;
				PHIDP_PREPARSED_DATA pPreparsedData;
				PHIDP_BUTTON_CAPS pButtonCaps;
				PHIDP_VALUE_CAPS pValueCaps;
				uint32_t uNumberOfButtons;
				uint32_t uNumberOfValues;
				std::vector<Button> PreviouslyPressedBtns;
			};
		public:
			RawInput();
			void Initialize(HWND _hWND); // register all input devices
			~RawInput();
			// Called by OS on every input
			static LRESULT ProcessInputMsg(HWND _hWnd, uint32_t _uMessage, WPARAM _wParam, LPARAM _lParam);


		private:
			void Execute();
		private:
			// OS interface
			bool Read(LPARAM _lParam, WPARAM _wParam); // perform standard read in case WM_INPUT
			void OnDeviceChange(LPARAM _lParam, WPARAM _wParam);

			// Helpers
			void ReadKeyboardInput(RAWINPUT* _pRaw);
			void ReadMouseInput(RAWINPUT* _pRaw);
			//void RecognizeHID(RAWINPUT* _pRaw);
			bool InitializeHID(HANDLE _hDevice, Device& _Device);
			void ReadHIDInput(RAWINPUT* _pRaw);

		private:
			Async::SpinLock m_InputLock;

			// staging buffers
			HardwareInput m_HardwareInput[2];
			std::set<Button> m_PressedButtons[2];
			uint32_t m_uActive = 0u;
			
			hlx::bytes m_RawBuffer;
			HWND m_hWnd;
			USAGE m_ButtonUsageBuffer[MAX_BUTTONS];

			int32_t m_iAbsMouseX = 0;
			int32_t m_iAbsMouseY = 0;
			float m_fRWAMouseX = 0.0f;
			float m_fRWAMouseY = 0.0f;
			const float m_fMouseAlpha = 0.01f;
			std::map<HANDLE, Device> m_Devices;
		};
	}
}
#endif