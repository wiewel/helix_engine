//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher, David Labode
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Narrow Phase
//--------------------------------------------------------------------------------------------------------------------------
// Summary:
// AMP narrow phase collision detection
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef NARROWPHASE_H
#define NARROWPHASE_H

#include "Util\HelixDefines.h"
#include "AMP\AMPDefines.h"
#include "AMP\AMPTypes.h"
namespace Helix
{
	// forward declarations
	namespace Datastructures
	{
		struct RigidbodyYoke;
	}
	namespace Collision
	{
		// forward declarations
		struct CollisionYoke;
		struct NarrowPhaseYoke;

		class NarrowPhase
		{
			friend struct NarrowPhaseYoke;
		private:
			struct SATInfo
			{
				AMP::float3 Direction;
				float Depth;
			};
			AMP::array<SATInfo, 2> m_SATField;
		
		public:
			NarrowPhase(uint32_t _uNumCollisionPairs)
				: m_SATField(_uNumCollisionPairs, 16)
			{}
			void Sieve(Datastructures::RigidbodyYoke& _RbYoke, CollisionYoke& _ColYoke);
		};

		struct NarrowPhaseYoke
		{
			AMP::array<NarrowPhase::SATInfo, 2>& SATField;
			NarrowPhaseYoke(NarrowPhase* _pNarrowPhase)
				: SATField(_pNarrowPhase->m_SATField)
			{}
		};
	}
}

#endif