//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher, David Labode
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

/////////////////////////////////////////////////////////////////////////////////////////////////
// Linear Bounding Volume Hierarchy
//-----------------------------------------------------------------------------------------------
// Summary:
// Datastructure for spatial partitioning
/////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef LINEARBVH_H
#define LINEARBVH_H
#include "DataStructures\GameObjectPool.h"
#include "AMP\AMPDefines.h"
#include "AMP\AMPAlgorithms.h"
#include "AABB.h"
#include "DataStructures\GameObject.h"
namespace Helix
{
	namespace Collision
	{
		struct BroadPhaseYoke;
		struct CollisionYoke;

		class LinearBVH
		{
			friend struct BroadPhaseYoke;

		private:
			__declspec(align(16)) struct InternalNode
			{
				InternalNode() HAMP
					: uLeftIndex(0)
					, uRightIndex(0)
					, ChildType(BothInternal)
					, iParentIndex(-1)
				{}
				uint32_t uLeftIndex;
				uint32_t uRightIndex;
				enum Flag : uint32_t
				{
					BothInternal = 0,		// 0
					LeftLeaf = 1 << 0,	// 1
					RightLeaf = 1 << 1,	// 2
					BothLeaf = 3
				} ChildType;
				int32_t iParentIndex;
			};
			__declspec(align(16)) struct InternalNodeProp
			{
				InternalNodeProp() HAMP
					: BoundingBox()
					, Padding1(1)
					, Padding2(2)
				{}
				AABB BoundingBox;
				uint32_t Padding1;
				uint32_t Padding2;
			};
			__declspec(align(16)) struct LeafNode
			{
				LeafNode() HAMP
					: BoundingBox()
					, iParentIndex(-1)
					, uFlag(Datastructures::GameObjectFlag::None)
				{}
				AABB BoundingBox; // 2 x float3
				int32_t iParentIndex;
				Datastructures::GameObjectFlag uFlag;
			};

			AMP::array<InternalNode> m_InternalNodes;
			AMP::array<InternalNodeProp> m_InternalNodeProps;
			AMP::array<LeafNode> m_LeafNodes;
			AMP::array<uint32_t> m_VisitCount;
			AMP::array<uint32_t> m_CollisionCount;
			AMP::array<uint32_t, 2> m_CollisionField;
			AMP::array<uint32_t> m_CollisionIndex;
			const uint32_t m_uMaxCollisions;

			concurrency::accelerator_view m_AcceleratorView;
		public:
			LinearBVH(uint32_t _uNumGameObjects, const concurrency::accelerator_view& _AcceleratorView, uint32_t _uMaxCollisions = 32)
				: m_InternalNodes(_uNumGameObjects - 1, _AcceleratorView)
				, m_InternalNodeProps(_uNumGameObjects - 1, _AcceleratorView)
				, m_LeafNodes(_uNumGameObjects, _AcceleratorView)
				, m_VisitCount(_uNumGameObjects - 1, _AcceleratorView)
				, m_CollisionCount(_uNumGameObjects, _AcceleratorView)
				, m_CollisionField(Concurrency::extent<2>(_uNumGameObjects, _uMaxCollisions), _AcceleratorView)
				, m_CollisionIndex(_uNumGameObjects, _AcceleratorView)
				, m_uMaxCollisions(_uMaxCollisions)
				, m_AcceleratorView(_AcceleratorView)
			{
			}

			void Sieve(Datastructures::RigidbodyYoke& _RbYoke, CollisionYoke& _ColYoke);
			inline const concurrency::accelerator_view& GetAcceleratorView() const { return m_AcceleratorView; }

		private:
			void BuildLBVH(Datastructures::RigidbodyYoke& _RbYoke, CollisionYoke& _ColYoke, BroadPhaseYoke& _BpYoke);
			template<uint32_t uMaxCollisions = 32, uint32_t uTileSize = 256>
			void TraverseLBVH(Datastructures::RigidbodyYoke& _RbYoke, CollisionYoke& _ColYoke, BroadPhaseYoke& _BpYoke);
		};

		struct BroadPhaseYoke
		{
			BroadPhaseYoke(uint32_t uNumActiveGameObjects, LinearBVH* _BroadSelector)
				: LeafExtent(uNumActiveGameObjects)
				, InternalExtent(uNumActiveGameObjects - 1)
				, InternalNodes(_BroadSelector->m_InternalNodes)
				, InternalNodeProps(_BroadSelector->m_InternalNodeProps)
				, LeafNodes(_BroadSelector->m_LeafNodes)
				, VisitCount(_BroadSelector->m_VisitCount)
				, CollisionCount(_BroadSelector->m_CollisionCount)
				, CollisionField(_BroadSelector->m_CollisionField)
				, CollisionIndex(_BroadSelector->m_CollisionIndex)
				, uMaxCollisions(_BroadSelector->m_uMaxCollisions)
			{}
			AMP::extent<1> LeafExtent;
			AMP::extent<1> InternalExtent;
			AMP::array<LinearBVH::InternalNode>& InternalNodes;
			AMP::array<LinearBVH::InternalNodeProp>& InternalNodeProps;
			AMP::array<LinearBVH::LeafNode>& LeafNodes;
			AMP::array<uint32_t>& VisitCount;
			AMP::array<uint32_t>& CollisionCount;
			AMP::array<uint32_t, 2>& CollisionField;
			AMP::array<uint32_t>& CollisionIndex;
			const uint32_t uMaxCollisions;
		};
	}
}

#endif