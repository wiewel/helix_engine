//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher, David Labode
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Fast 4-way parallel radix sorting on GPUs (Linh Ha, Jens Kr�ger, Cl�udio T. Silva)
//--------------------------------------------------------------------------------------------------------------------
// Summary:
// (Wikipedia) Radix sort is a non-comparative integer sorting algorithm that sorts data with integer keys by grouping 
// keys by the individual digits which share the same significant position and value. The radix is the base of the 
// significant part which is compared and sorted every step. In our case the radix is 4, which means that each step 
// there are 2 significant bits.
//--------------------------------------------------------------------------------------------------------------------
// Notation:
//  - for consistency with C++ AMP the CUDA term 'blocks' from the paper is translated to 'tiles'
//--------------------------------------------------------------------------------------------------------------------
// Prefix Sum:
//	s_0 = d_0
//	s_n = s_(n-1) + d_n
//
//	data:		0 1 0 0 1 0 1 1
//	prefix sum:	0 1 1 1 2 2 3 4
//--------------------------------------------------------------------------------------------------------------------
// http://vgc.poly.edu/~csilva/papers/cgf.pdf (Fast 4-way parallel radix sorting on GPUs)
// based on:
// http://http.developer.nvidia.com/GPUGems3/gpugems3_ch39.html (GPU Gems 3: Chapter 39)
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef RADIXSORT_H
#define RADIXSORT_H

//#include "AMPDatastructures.h"
#include "AMP\AMPDefines.h"
#include "AMP/AMPAlgorithms.h"

namespace Concurrency
{
	//--------------------------------------------------------------------------------------------------------------------
	template<uint32_t uTileSize>
	inline void four_way_parallel_scan(uint32_t _Data_SHR[4][uTileSize], tiled_index<uTileSize> _Tidx) restrict(amp)
	{
		uint32_t uOffset = 1;

		tile_static uint32_t ScanData_SHR[4][uTileSize];
		uint32_t uLidx = _Tidx.local[0];
		uint32_t uBankOffset = conflict_free_offset(uLidx);

		ScanData_SHR[0][(uLidx + uBankOffset) % uTileSize] = _Data_SHR[0][uLidx];
		ScanData_SHR[1][(uLidx + uBankOffset) % uTileSize] = _Data_SHR[1][uLidx];
		ScanData_SHR[2][(uLidx + uBankOffset) % uTileSize] = _Data_SHR[2][uLidx];
		ScanData_SHR[3][(uLidx + uBankOffset) % uTileSize] = _Data_SHR[3][uLidx];

		// Up Sweep
		for (uint32_t d = uTileSize / 2; d > 0; d /= 2)
		{
			_Tidx.barrier.wait();
			//-------------------
			if (uLidx < d)
			{
				uint32_t uFromIndex = uOffset * (2 * uLidx + 1) - 1;
				uint32_t uToIndex = uOffset * (2 * uLidx + 2) - 1;
				// stride access to reduce bank conflicts
				uFromIndex += conflict_free_offset(uFromIndex);
				uToIndex += conflict_free_offset(uToIndex);

				ScanData_SHR[0][uToIndex] += ScanData_SHR[0][uFromIndex];
				ScanData_SHR[1][uToIndex] += ScanData_SHR[1][uFromIndex];
				ScanData_SHR[2][uToIndex] += ScanData_SHR[2][uFromIndex];
				ScanData_SHR[3][uToIndex] += ScanData_SHR[3][uFromIndex];
			}
			uOffset *= 2;
		}

		_Tidx.barrier.wait();
		//------------------- Up sweep complete

		// Clear last element
		if (uLidx == 0)
		{
			uint32_t uIdx = uTileSize - 1;
			uIdx += conflict_free_offset(uIdx);
			ScanData_SHR[0][uIdx] = 0;
			ScanData_SHR[1][uIdx] = 0;
			ScanData_SHR[2][uIdx] = 0;
			ScanData_SHR[3][uIdx] = 0;
		}

		// Down Sweep
		for (uint32_t d = 1; d < uTileSize; d *= 2)
		{
			uOffset /= 2;
			_Tidx.barrier.wait();
			//-------------------
			if (uLidx < d)
			{
				uint32_t uA = uOffset * (2 * uLidx + 1) - 1;
				uint32_t uB = uOffset * (2 * uLidx + 2) - 1;
				uA += conflict_free_offset(uA);
				uB += conflict_free_offset(uB);

				for (uint32_t b = 0; b < 4; ++b)
				{
					uint32_t uT = ScanData_SHR[b][uA];
					ScanData_SHR[b][uA] = ScanData_SHR[b][uB];
					ScanData_SHR[b][uB] += uT;
				}
			}

		}

		_Tidx.barrier.wait();
		//------------------- up sweep complete


		_Data_SHR[0][uLidx] = ScanData_SHR[0][uLidx + uBankOffset];
		_Data_SHR[1][uLidx] = ScanData_SHR[1][uLidx + uBankOffset];
		_Data_SHR[2][uLidx] = ScanData_SHR[2][uLidx + uBankOffset];
		_Data_SHR[3][uLidx] = ScanData_SHR[3][uLidx + uBankOffset];

		_Tidx.barrier.wait();
		//------------------- copy complete
	}
	//--------------------------------------------------------------------------------------------------------------------
	template<uint32_t Dim1, uint32_t Dim2>
	inline void parallel_scan(uint32_t _Data_SHR[Dim1][Dim2], tiled_index<Dim1, Dim2> _Tidx) restrict(amp)
	{
		int32_t iOffset = 1;

		tile_static uint32_t ScanData_SHR[Dim1][Dim2];
		index<2> Lidx = _Tidx.local;
		int32_t iBankOffset = conflict_free_offset(Lidx[1]);

		ScanData_SHR[Lidx[0]][(Lidx[1] + iBankOffset) % Dim2] = _Data_SHR[Lidx[0]][Lidx[1]];

		// Up Sweep
		for (int32_t d = Dim2 / 2; d > 0; d /= 2)
		{
			_Tidx.barrier.wait();
			//-------------------
			if (Lidx[1] < d)
			{
				int32_t iFromIndex = iOffset * (2 * Lidx[1] + 1) - 1;
				int32_t iToIndex = iOffset * (2 * Lidx[1] + 2) - 1;
				// stride access to reduce bank conflicts
				iFromIndex += conflict_free_offset(iFromIndex);
				iToIndex += conflict_free_offset(iToIndex);

				ScanData_SHR[Lidx[0]][iToIndex] += ScanData_SHR[Lidx[0]][iFromIndex];
			}
			iOffset *= 2;
		}

		_Tidx.barrier.wait();
		//------------------- Up sweep complete

		// Clear last element
		if (Lidx[1] == 0)
		{
			uint32_t uIdx = Dim2 - 1;
			uIdx += conflict_free_offset(uIdx);
			ScanData_SHR[Lidx[0]][uIdx] = 0;
		}

		// Down Sweep
		for (int32_t d = 1; d < Dim2; d *= 2)
		{
			iOffset /= 2;
			_Tidx.barrier.wait();
			//-------------------
			if (Lidx[1] < d)
			{
				int32_t A = iOffset * (2 * Lidx[1] + 1) - 1;
				int32_t B = iOffset * (2 * Lidx[1] + 2) - 1;
				A += conflict_free_offset(A);
				B += conflict_free_offset(B);

				uint32_t uT = ScanData_SHR[Lidx[0]][A];
				ScanData_SHR[Lidx[0]][A] = ScanData_SHR[Lidx[0]][B];
				ScanData_SHR[Lidx[0]][B] += uT;
			}
		}

		_Tidx.barrier.wait();
		//------------------- up sweep complete

		_Data_SHR[Lidx[0]][Lidx[1]] = ScanData_SHR[Lidx[0]][Lidx[1] + iBankOffset];

		_Tidx.barrier.wait();
		//------------------- copy complete
	}
	//--------------------------------------------------------------------------------------------------------------------
	template<uint32_t uTileSize>
	inline void global_scan(extent<2> _Extent, Helix::AMP::array<uint32_t, 2>& _TileSum_GLOB)
	{
		uint32_t uNumTiles = num_tiles<2>(_TileSum_GLOB.extent, uTileSize * 4);
		array<uint32_t, 2> IntermediateSum(extent<2>(4, uNumTiles), _TileSum_GLOB.accelerator_view);
		tiled_extent<4, uTileSize> Extent = _TileSum_GLOB.extent.tile<4, uTileSize>().pad();

		// Scan over the per tile sum
		parallel_for_each(Extent, [&_TileSum_GLOB, &IntermediateSum](tiled_index<4, uTileSize> Tidx) restrict(amp)
		{
			//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BEGIN AMP KERNEL >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
			tile_static uint32_t Sums_SHR[4][uTileSize];
			Sums_SHR[Tidx.local[0]][Tidx.local[1]] = guarded_read<uint32_t, 2>(_TileSum_GLOB, index<2>(Tidx.global), 0);
			Tidx.barrier.wait();
			//------------------ load complete

			uint32_t uLastElement = Sums_SHR[Tidx.local[0]][uTileSize - 1];
			ParallelPrefixSumScan<4, uTileSize>(Sums_SHR, Tidx);
			Tidx.barrier.wait();
			//------------------ scan complete

			guarded_write<uint32_t, 2>(_TileSum_GLOB, index<2>(Tidx.global), Sums_SHR[Tidx.local[0]][Tidx.local[1]]);
			if (Tidx.local[1] == 0)
			{
				IntermediateSum[Tidx.local[0]][Tidx.tile[0]] = Sums_SHR[Tidx.local[0]][uTileSize - 1] + uLastElement;
			}
			//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< END AMP KERNEL <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
		});

		// last recursion layer -> exit early IntermediateSum will be zero
		if (uNumTiles > 1)
		{
			// Scan deeper
			global_scan<uTileSize>(IntermediateSum.extent, IntermediateSum);
		}
		else
		{
			// Scan over the per tile sum
			extent<1> Extent(4);
			parallel_for_each(Extent, [&IntermediateSum](index<1> Idx) restrict(amp)
			{
				//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BEGIN AMP KERNEL >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
				uint32_t First = IntermediateSum[0][0];
				uint32_t Second = IntermediateSum[1][0];
				uint32_t Third = IntermediateSum[2][0];

				if (Idx[0] == 0)
				{
					IntermediateSum[0][0] = 0;
					IntermediateSum[1][0] = First;
					IntermediateSum[2][0] = First + Second;
					IntermediateSum[3][0] = First + Second + Third;
				}
				//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< END AMP KERNEL <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
			});
		}

		// Add results from deeper scans
		parallel_for_each(Extent, [&_TileSum_GLOB, &IntermediateSum](tiled_index<4, uTileSize> Tidx) restrict(amp)
		{
			uint32_t uCurrentSum = guarded_read<uint32_t, 2>(_TileSum_GLOB, index<2>(Tidx.global), 0);
			//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BEGIN AMP KERNEL >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
			guarded_write<uint32_t, 2>(_TileSum_GLOB, index<2>(Tidx.global), uCurrentSum + IntermediateSum[Tidx.global[0]][Tidx.tile[0]]);
			//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< END AMP KERNEL <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
		});
	}
	//---------------------------------------------------------------------------------------------------------------------
	// This function selects _uAmount bits from the input number beginning at bit index _uStartingFrom
	// Example:
	// 
	// ExtractBits(_uInBits = 0011 0000, _uStartingFrom = 4, _uAmount = 2) 
	// (0011 0000 >> 4) & 0000 0011
	// = 0000 0011
	inline uint32_t extract_bits(uint32_t _uInBits, const uint32_t _uStartingFrom, const uint32_t _uAmount) restrict(amp)
	{
		_uInBits >>= _uStartingFrom;
		uint32_t uMask = ~(0xFFFFFFFF << _uAmount);
		return _uInBits & uMask;
	}
}

namespace Concurrency
{
	// Helper to select significant bits
	//uint32_t extract_bits(uint32_t _uInBits, const uint32_t _uStartingFrom, const uint32_t _uAmount) restrict(amp);

	//template<uint32_t uTileSize = 256>
	//void parallel_radix_sort(SorterYoke& _SortYoke, CollisionYoke& _ColYoke);

	namespace Implementation
	{
		//diagnostic::marker_series g_MarkerSeries("hAMPelmann");
		//  Parallel order checking (Algorithm 3) 
		//template<uint32_t uTileSize>
		//bool is_in_order(SorterYoke& _SortYoke, CollisionYoke& _ColYoke);

		//// 4-way-prefix-sum (Algorithm 2)
		//template<uint32_t uTileSize>
		//void tile_sort(uint32_t _uBitOffset, SorterYoke& _SortYoke, CollisionYoke& _ColYoke);


		// Scan the obtaines tile sums
		//template<uint32_t uTileSize>
		//void global_scan(SorterYoke& _SortYoke);

		//template<uint32_t uTileSize>
		//void tile_mapping(uint32_t _uBitOffset, SorterYoke& _SortYoke, CollisionYoke& _ColYoke);
	}
}
////--------------------------------------------------------------------------------------------------------------------
//template<uint32_t uTileSize>
//void Concurrency::parallel_radix_sort(SorterYoke& _SortYoke, CollisionYoke& _ColYoke)
//{
//	using namespace Implementation;
//
//	const uint32_t uNumTiles = num_tiles(_Data.extent, uTileSize);
//
//	// for all 2 bits in uint32_t -> radix 4
//	for (uint32_t uBitOffset = 0; uBitOffset < 32; uBitOffset += 2)
//	{
//		// Step 1: Perform order checking (is it already sorted?)
//		if (is_in_order<uTileSize>(_Data))
//		{
//			break;
//		}
//
//		array<uint32_t, 2> TilePrefixSum(extent<2>(4, uNumTiles), _Data.accelerator_view); // this will contain the frequency of digits per tile
//		array<uint32_t> LocalPrefixSum(_Data.extent.size(), _Data.accelerator_view); // this will hold the local prefix sum of each data element as computed in TileSort
//																					 // Step 2: Sort data per tile and obtain per tile frequency of input chunks
//																					 // Tiled sort
//																					 // [0 2 2 1 | 3 0 1 2] -> [0 1 2 2 | 0 1 2 3] 
//																					 // Frequency:
//																					 //  0 1 2 3   0 1 2 3
//																					 // [0 1 2 0] [1 1 1 1]
//		array<uint32_t> LocalOffset_GLOB(_Data.extent.size(), _Data.accelerator_view);
//		tile_sort<uTileSize>(uBitOffset, LocalPrefixSum, TilePrefixSum, _Data, LocalOffset_GLOB);
//
//		// Step 3: scan over tile sum to obtain global tile prefix sum
//		global_scan<uTileSize>(TilePrefixSum);
//		//// Step 4: Map elements to right global position
//		tile_mapping<uTileSize>(uBitOffset, LocalPrefixSum, LocalOffset_GLOB, TilePrefixSum, _Data, _Index);
//	} // end bit selection
//}
////--------------------------------------------------------------------------------------------------------------------
//// Step 1: Paralllel order checking (Algorithm 3) 
//template<uint32_t uTileSize>
//inline bool Helix::AMP::Implementation::is_in_order(SorterYoke& _SortYoke, CollisionYoke& _ColYoke)
//{
//	tiled_extent<uTileSize> Extent = _InputData_GLOB.extent.tile<uTileSize>().pad();
//	uint32_t uNumTiles = num_tiles(_InputData_GLOB.extent, uTileSize);
//	array<uint32_t> InOrder_GLOB(uNumTiles, _InputData_GLOB.accelerator_view);
//
//	parallel_for_each(Extent, [=, &_InputData_GLOB, &InOrder_GLOB](tiled_index<uTileSize> Tidx) restrict(amp)
//	{
//		//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BEGIN AMP KERNEL >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//		// Load global data into shared memory
//		tile_static uint32_t InputData_SHR[uTileSize + 1]; // last element := first of next tile
//		index<1> Idx(Tidx.global[0]);
//		InputData_SHR[Tidx.local[0]] = guarded_read(_InputData_GLOB, Idx, UINT_MAX);
//		if (Tidx.local[0] == uTileSize - 1) // last element in tile
//		{
//			// Write first data from next tile into last shared data or pad if out of bounds
//			InputData_SHR[Tidx.local[0] + 1] = Extent.contains(Idx + 1) ? _InputData_GLOB[Idx + 1] : UINT_MAX;
//		}
//		Tidx.barrier.wait();
//		//------------------ load complete
//
//		// Compare neighbors
//		tile_static uint32_t Comparison_SHR[uTileSize];
//		Comparison_SHR[Tidx.local[0]] = InputData_SHR[Tidx.local[0]] > InputData_SHR[Tidx.local[0] + 1];
//		Tidx.barrier.wait();
//		//------------------ compare complete
//
//		parallel_aggregate<uTileSize>(Comparison_SHR, Tidx);// sum up all comparisons
//		Tidx.barrier.wait();
//		//------------------ reduce complete
//
//		// write to global memory
//		if (Tidx.local[0] == 0)
//		{
//			guarded_write(InOrder_GLOB, index<1>(Tidx.tile[0]), Comparison_SHR[0]);
//		}
//		//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< END AMP KERNEL <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//	});
//	return parallel_aggregate<uTileSize>(InOrder_GLOB) == 0;
//}
//
////--------------------------------------------------------------------------------------------------------------------
//// Step 2: 4-way-prefix-sum (Algorithm 2)
//template<uint32_t uTileSize>
//inline void Concurrency::Implementation::tile_sort(uint32_t _uBitOffset, SorterYoke& _SortYoke, CollisionYoke& _ColYoke)
//{
//	//span Span(g_MarkerSeries, "Radix Sort: per tile sorting");
//	tiled_extent<uTileSize> Extent = _Data_GLOB.extent.tile<uTileSize>().pad();
//
//	parallel_for_each(Extent, [=, &_LocalPrefixSum_GLOB, &_TilePrefixSum_GLOB, &_Data_GLOB, &_LocalOffset_GLOB](tiled_index<uTileSize> Tidx) restrict(amp)
//	{
//		//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BEGIN AMP KERNEL >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//		// Allocate shared memory
//		tile_static uint32_t Data_SHR[uTileSize];
//		tile_static uint32_t Mask_SHR[4][uTileSize];
//
//		uint32_t uLidx = Tidx.local[0];
//		// Load data from global memory
//		Data_SHR[Tidx.local[0]] = guarded_read(_Data_GLOB, index<1>(Tidx.global[0]), UINT_MAX);
//		Tidx.barrier.wait();
//		//------------------ load complete
//
//		// Select significant bits
//		uint32_t uExtractedBits = ExtractBits(Data_SHR[Tidx.local[0]], _uBitOffset, 2);
//		// iterate over the four possible values of a 2 bit radix
//		for (uint32_t uDigit = 0; uDigit < 4; ++uDigit)
//		{
//			// count bit combinations
//			Mask_SHR[uDigit][Tidx.local[0]] = uDigit == uExtractedBits; // Count <- Mask(Input, b)
//		}
//
//		Tidx.barrier.wait();
//		//------------------ extract complete
//
//		// Copy to prefix sum
//		tile_static uint32_t PrefixSum_SHR[4][uTileSize];
//		PrefixSum_SHR[0][Tidx.local[0]] = Mask_SHR[0][Tidx.local[0]];
//		PrefixSum_SHR[1][Tidx.local[0]] = Mask_SHR[1][Tidx.local[0]];
//		PrefixSum_SHR[2][Tidx.local[0]] = Mask_SHR[2][Tidx.local[0]];
//		PrefixSum_SHR[3][Tidx.local[0]] = Mask_SHR[3][Tidx.local[0]];
//
//		Tidx.barrier.wait();
//		//------------------ copy complete
//
//		FourWayParallelPrefixSumScan<uTileSize>(PrefixSum_SHR, Tidx); // Count <- LocalPrefix[4]
//		// At this point Mask_SHR contains a digit dependend local offset for each digit not reflecting the true local offset
//
//		uint32_t uTotalOccurences[4];
//		uTotalOccurences[0] = Mask_SHR[0][uTileSize - 1] + PrefixSum_SHR[0][uTileSize - 1];
//		uTotalOccurences[1] = Mask_SHR[1][uTileSize - 1] + PrefixSum_SHR[1][uTileSize - 1];
//		uTotalOccurences[2] = Mask_SHR[2][uTileSize - 1] + PrefixSum_SHR[2][uTileSize - 1];
//		uTotalOccurences[3] = Mask_SHR[3][uTileSize - 1] + PrefixSum_SHR[3][uTileSize - 1];
//
//		if (Tidx.local[0] == 0)
//		{
//			_TilePrefixSum_GLOB[0][Tidx.tile[0]] = uTotalOccurences[0];
//			_TilePrefixSum_GLOB[1][Tidx.tile[0]] = uTotalOccurences[1];
//			_TilePrefixSum_GLOB[2][Tidx.tile[0]] = uTotalOccurences[2];
//			_TilePrefixSum_GLOB[3][Tidx.tile[0]] = uTotalOccurences[3];
//		}
//
//		Tidx.barrier.wait();
//		//------------------ write complete
//
//		// merge results
//		uint32_t uPreviousElems[4];
//		uPreviousElems[0] = PrefixSum_SHR[0][uLidx];
//		uPreviousElems[1] = PrefixSum_SHR[1][uLidx] + uTotalOccurences[0];
//		uPreviousElems[2] = PrefixSum_SHR[2][uLidx] + uTotalOccurences[0] + uTotalOccurences[1];
//		uPreviousElems[3] = PrefixSum_SHR[3][uLidx] + uTotalOccurences[0] + uTotalOccurences[1] + uTotalOccurences[2];
//
//		tile_static uint32_t Offset_SHR[uTileSize];
//		Offset_SHR[uLidx] = 0;
//		Offset_SHR[uLidx] += (Mask_SHR[0][uLidx]) ? uPreviousElems[0] : 0;
//		Offset_SHR[uLidx] += (Mask_SHR[1][uLidx]) ? uPreviousElems[1] : 0;
//		Offset_SHR[uLidx] += (Mask_SHR[2][uLidx]) ? uPreviousElems[2] : 0;
//		Offset_SHR[uLidx] += (Mask_SHR[3][uLidx]) ? uPreviousElems[3] : 0;
//
//		Tidx.barrier.wait();
//		//------------------ merge complete
//		index<1> ShuffleIndex(Tidx.tile_origin[0] + Offset_SHR[uLidx]);
//		guarded_write<uint32_t>(_LocalOffset_GLOB, index<1>(Tidx.global[0]), ShuffleIndex[0]);
//		guarded_write(_Data_GLOB, ShuffleIndex, Data_SHR[uLidx]);
//		_LocalPrefixSum_GLOB[ShuffleIndex] = PrefixSum_SHR[uExtractedBits][Tidx.local[0]];
//
//		//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< END AMP KERNEL <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//	});
//}
 
////---------------------------------------------------------------------------------------------------------------------
//// This function selects _uAmount bits from the input number beginning at bit index _uStartingFrom
//// Example:
//// 
//// ExtractBits(_uInBits = 0011 0000, _uStartingFrom = 4, _uAmount = 2) 
//// (0011 0000 >> 4) & 0000 0011
//// = 0000 0011
//inline uint32_t Concurrency::extract_bits(uint32_t _uInBits, const uint32_t _uStartingFrom, const uint32_t _uAmount) restrict(amp)
//{
//	_uInBits >>= _uStartingFrom;
//	uint32_t uMask = ~(0xFFFFFFFF << _uAmount);
//	return _uInBits & uMask;
//}
//--------------------------------------------------------------------------------------------------------------------
//
//template<uint32_t uTileSize>
//inline void Helix::AMP::Implementation::tile_mapping(uint32_t _uBitOffset, SorterYoke& _SortYoke, CollisionYoke& _ColYoke)
//{
//	//span Span(g_MarkerSeries, "Radix Sort: global mapping");
//	tiled_extent<uTileSize> Extent = _Data_GLOB.extent.tile<uTileSize>().pad();
//	array<uint32_t> TmpData_GLOB(_Data_GLOB.extent.size(), _Data_GLOB.accelerator_view);
//	copy(_Data_GLOB, TmpData_GLOB);
//	array<uint32_t> IndexOffset_GLOB(_Index_GLOB.extent, _Index_GLOB.accelerator_view);
//	parallel_for_each(Extent, [=, &_LocalPrefixSum_GLOB, &_TilePrefixSum_GLOB, &_Data_GLOB, &TmpData_GLOB, &IndexOffset_GLOB](tiled_index<uTileSize> Tidx) restrict(amp)
//	{
//		//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< BEGIN AMP KERNEL <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//		uint32_t uDigit = guarded_read<uint32_t>(TmpData_GLOB, index<1>(Tidx.global[0]), UINT_MAX);
//		uint32_t uBits = ExtractBits(uDigit, _uBitOffset, 2);
//		uint32_t uTileOffset = _TilePrefixSum_GLOB[uBits][Tidx.tile[0]];
//		uint32_t uPosition = 0;
//		if (guarded_read<uint32_t>(_LocalPrefixSum_GLOB, index<1>(Tidx.global[0]), 0, uPosition))
//		{
//			uPosition += uTileOffset;
//			IndexOffset_GLOB[Tidx.global[0]] = uPosition;
//			guarded_write(_Data_GLOB, index<1>(uPosition), uDigit);
//		}
//		//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< END AMP KERNEL <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//	});
//	array<uint32_t> TmpIndex_GLOB(_Index_GLOB.extent, _Index_GLOB.accelerator_view);
//	copy(_Index_GLOB, TmpIndex_GLOB);
//	parallel_for_each(_Index_GLOB.extent, [=, &_Index_GLOB, &IndexOffset_GLOB, &TmpIndex_GLOB, &_LocalOffset_GLOB](index<1> Idx) restrict(amp)
//	{
//		//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< BEGIN AMP KERNEL <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//		_Index_GLOB[Idx] = IndexOffset_GLOB[_LocalOffset_GLOB[TmpIndex_GLOB[Idx]]];
//		//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< END AMP KERNEL <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//	});
//}

#endif // RADIXSORT_H