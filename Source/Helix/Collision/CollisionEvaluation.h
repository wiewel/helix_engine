//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef COLLISIONEVALUATION_H
#define COLLISIONEVALUATION_H

#include "MovingObject.h"
#include "RigidBody.h"

#include "CollisionContact.h"

namespace helix
{
	namespace collision
	{
		class CollisionEvaluation
		{
		public:
			static void ComputeCollision(datastructures::GameObject* _pGO1, datastructures::GameObject* _pGO2, const collision::CollisionInfo& _CollisionInfo);

			static void ComputeCollision(datastructures::MovingObject& _MO1, datastructures::MovingObject& _MO2, const collision::CollisionInfo& _CollisionInfo);
			static void ComputeCollision(datastructures::RigidBody& _RB1, datastructures::RigidBody& _RB2, const collision::CollisionInfo& _CollisionInfo);
		};
	} // collision
} // helix

#endif // COLLISIONEVALUATION_H
