//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher, David Labode
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "CollisionSystem.h"

#include "CPUNarrowSelector.h"
#include "ResolutionPhase.h"
#include "DataStructures\GameObjectPool.h"
#include "DataStructures\GameObject.h"

#define TILE_SIZE 256
using namespace Helix::Collision;
using namespace Helix::Datastructures;
using namespace Helix::AMP;

//-----------------------------------------------------------------------------------------------
void CollisionSystem::Resize(uint32_t _uNumGameObjects)
{
	m_MortonCodes = AMP::array<uint32_t>(_uNumGameObjects, m_AMPAccelerator);
	m_OrientedBoundingBoxes = AMP::array<OBB>(_uNumGameObjects, m_AMPAccelerator);
}
//-----------------------------------------------------------------------------------------------
void CollisionSystem::ProcessCollisions(RigidbodyYoke& _RbYoke)
{
	HASSERT(m_MortonCodes.extent.size() == _RbYoke.Extent.size(), "Collision System must be initialized with same size as Game Object Pool");
	try
	{
		if (_RbYoke.uActiveGameObjectsCount == 0)
		{
			return;
		}
		CollisionYoke ColYoke(this);

		m_RadixSorter.Sort(_RbYoke, ColYoke);

		// Translate and rotate the OBB
		extent<1> Extent(_RbYoke.uActiveGameObjectsCount);
		parallel_for_each(Extent, [_RbYoke, ColYoke](index<1> Idx) HGPU
		{
			const float3& vPos = _RbYoke.vPosition[Idx];
			const float3& vScale = _RbYoke.vScale[Idx];
			//bool TEST = vScale.x < 0.06f && vScale.x > 0.04f;
			const quaternion& vOrientation = _RbYoke.vOrientation[Idx];
			ColYoke.OrientedBoundingBoxes[Idx] = _RbYoke.OBB[Idx].Transformed(vPos, vScale, vOrientation);
		});
		
		m_LinearBVH.Sieve(_RbYoke, ColYoke);

		// BEGIN HACK
		CPUNarrowSelector NarrowPhase;
		NarrowPhase.Sieve(_RbYoke, ColYoke, m_CollisionInfo);
		m_CollisionMap.clear();
		for (CollisionInformation Info : m_CollisionInfo)
		{
			m_CollisionMap.insert({ Info.m_IndexPair.x, Info.m_IndexPair.y });
			m_CollisionMap.insert({ Info.m_IndexPair.y, Info.m_IndexPair.x });
		}
		
		//ResolutionPhase Resolution;
		//Resolution.Resolve(ColYoke, _RbYoke);
	} 
	catch (concurrency::runtime_exception e)
	{
		HFATALD2("Collision System failed: %s", e.what());
	}
}
//-----------------------------------------------------------------------------------------------
std::vector<CollisionInformation>& CollisionSystem::GetCollisionInformation()
{
	return m_CollisionInfo; 
}
//-----------------------------------------------------------------------------------------------
// test if game object did collide
bool CollisionSystem::DidCollide(GameObject & _GameObject) const
{
	auto it = m_CollisionMap.find(_GameObject.Index);
	if (it != m_CollisionMap.end())
	{
		return true;
	}
	else
	{
		return false;
	}
}
//-----------------------------------------------------------------------------------------------
bool CollisionSystem::DidCollideWith(GameObject & _GameObject, GameObject & _OtherGO) const
{
	auto range = m_CollisionMap.equal_range(_GameObject.Index);
	uint32_t uOtherIndex = _OtherGO.Index;
	for (auto it = range.first; it != range.second; ++it)
	{
		if (it->second == uOtherIndex)
		{
			return true;
		}
	}
	return false;
}
