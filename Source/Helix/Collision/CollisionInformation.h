//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher, David Labode
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

/////////////////////////////////////////////////////////////////////////////////////////////////
// Game Object Sorter
//-----------------------------------------------------------------------------------------------
// Summary:
// Sorts Game Objects for usage in the broad phase collision
/////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef COLLISIONINFORMATION_H
#define COLLISIONINFORMATION_H

#include "AMP\AMPTypes.h"

namespace Helix
{
	namespace Collision
	{
		struct CollisionInformation
		{
			AMP::uint2 m_IndexPair;
			float m_fPenetrationDepth;
			AMP::float3 m_vNormal;
		};
	}
}

#endif