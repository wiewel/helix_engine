//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher, David Labode
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "RadixSorter.h"
#include "amp_radix_sort.h"
#include "DataStructures\GameObjectPool.h"
#include "DataStructures\GameObject.h"
#include "CollisionSystem.h"
#include <math.h>

using namespace Helix::Collision;
using namespace Helix::AMP;
using namespace Helix::Datastructures;
//---------------------------------------------------------------------------------------------------------------------
void RadixSorter::Sort(RigidbodyYoke& _RbYoke, CollisionYoke& _ColYoke)
{
	// determine how many game objecst must be sorted
	// if go were deleted we need to sort them to the end
	// if go were added we need to sort them too
	uint32_t uGameObjectsToSort = std::max(_RbYoke.uActiveGameObjectsCount, m_uPrevActiveGameObjects);
	m_uPrevActiveGameObjects = _RbYoke.uActiveGameObjectsCount;
	
	SorterYoke SortYoke(uGameObjectsToSort, this);
	ComputeMortonCodes(SortYoke, _ColYoke, _RbYoke);
	ParallelRadixSort<256>(SortYoke, _ColYoke);

	ReorderIndices(SortYoke, _RbYoke);
	Reorder(_RbYoke.vPosition, SortYoke.ReorderIndices, SortYoke.ActiveGOExtent);
	Reorder(_RbYoke.uFlags, SortYoke.ReorderIndices, SortYoke.ActiveGOExtent);
	Reorder(_RbYoke.vScale, SortYoke.ReorderIndices, SortYoke.ActiveGOExtent);
	Reorder(_RbYoke.mRotation, SortYoke.ReorderIndices, SortYoke.ActiveGOExtent);
	Reorder(_RbYoke.vOrientation, SortYoke.ReorderIndices, SortYoke.ActiveGOExtent);
	Reorder(_RbYoke.fDamping, SortYoke.ReorderIndices, SortYoke.ActiveGOExtent);
	Reorder(_RbYoke.fInvMass, SortYoke.ReorderIndices, SortYoke.ActiveGOExtent);
	Reorder(_RbYoke.fMotion, SortYoke.ReorderIndices, SortYoke.ActiveGOExtent);
	Reorder(_RbYoke.fElasticity, SortYoke.ReorderIndices, SortYoke.ActiveGOExtent);
	Reorder(_RbYoke.vForce, SortYoke.ReorderIndices, SortYoke.ActiveGOExtent);
	Reorder(_RbYoke.vTorque, SortYoke.ReorderIndices, SortYoke.ActiveGOExtent);
	Reorder(_RbYoke.vLinearVelocity, SortYoke.ReorderIndices, SortYoke.ActiveGOExtent);
	Reorder(_RbYoke.vAngularVelocity, SortYoke.ReorderIndices, SortYoke.ActiveGOExtent);
	Reorder(_RbYoke.vAngularMomentum, SortYoke.ReorderIndices, SortYoke.ActiveGOExtent);
	Reorder(_RbYoke.mInvInertiaTensor, SortYoke.ReorderIndices, SortYoke.ActiveGOExtent);
	Reorder(_RbYoke.OBB, SortYoke.ReorderIndices, SortYoke.ActiveGOExtent);
}
//---------------------------------------------------------------------------------------------------------------------
template<uint32_t uTileSize>
void RadixSorter::ParallelRadixSort(SorterYoke& _SortYoke, CollisionYoke& _ColYoke) HCPU
{
	// for all 2 bits in uint32_t -> radix 4
	for (uint32_t uBitOffset = 0; uBitOffset < 32; uBitOffset += 2)
	{
		// Step 1: Perform order checking (is it already sorted?)
		if (OrderChecking<uTileSize>(_SortYoke, _ColYoke))
		{
			break;
		}
		// Step 2: sort the data per tile
		TileSorting<uTileSize>(uBitOffset, _SortYoke, _ColYoke);
		// Step 3: sort the data globaly
		GlobalSorting<uTileSize>(uBitOffset, _SortYoke, _ColYoke);
	}
}
//---------------------------------------------------------------------------------------------------------------------
template<uint32_t uTileSize>
bool RadixSorter::OrderChecking(SorterYoke& _SortYoke, CollisionYoke& _ColYoke) HCPU
{
	tiled_extent<uTileSize> TiledActiveGOExtent = _SortYoke.ActiveGOExtent.tile<uTileSize>().pad();
	fill<uint32_t>(_SortYoke.OrderChecks, 0);
	parallel_for_each(TiledActiveGOExtent, [_SortYoke, _ColYoke] (tiled_index<uTileSize> Tidx) restrict(amp)
	{
		// Load global data into shared memory
		tile_static uint32_t MortonCodes_SHR[uTileSize + 1]; // last element := first of next tile
		index<1> Idx(Tidx.global[0]);
		MortonCodes_SHR[Tidx.local[0]] = guarded_read(_ColYoke.MortonCodes, Idx, UINT_MAX);
		if (Tidx.local[0] == uTileSize - 1) // last element in tile
		{
			// Write first data from next tile into last shared data or pad if out of bounds
			MortonCodes_SHR[Tidx.local[0] + 1] = guarded_read(_ColYoke.MortonCodes, Idx + 1, UINT_MAX);
		}
		Tidx.barrier.wait();
		//------------------ load complete

		// Compare neighbors
		tile_static uint32_t Comparison_SHR[uTileSize];
		Comparison_SHR[Tidx.local[0]] = MortonCodes_SHR[Tidx.local[0]] > MortonCodes_SHR[Tidx.local[0] + 1];
		Tidx.barrier.wait();
		//------------------ compare complete

		parallel_aggregate<uTileSize>(Comparison_SHR, Tidx);// sum up all comparisons
		Tidx.barrier.wait();
		//------------------ reduce complete

		// write to global memory
		if (Tidx.local[0] == 0)
		{
			guarded_write(_SortYoke.OrderChecks, index<1>(Tidx.tile[0]), Comparison_SHR[0]);
		}
	});
	return parallel_aggregate<uTileSize>(_SortYoke.OrderChecks) == 0;
}
//---------------------------------------------------------------------------------------------------------------------
template<uint32_t uTileSize>
void RadixSorter::TileSorting(const uint32_t _uBitOffset, SorterYoke& _SortYoke, CollisionYoke& _ColYoke) HCPU
{
	//span Span(g_MarkerSeries, "Radix Sort: per tile sorting");
	tiled_extent<uTileSize> ActiveGOExtent = _SortYoke.ActiveGOExtent.tile<uTileSize>().pad();
	
	fill<uint32_t, 2>(_SortYoke.TilePrefixSum, 0);

	parallel_for_each(ActiveGOExtent, [_uBitOffset, _SortYoke, _ColYoke](tiled_index<uTileSize> Tidx) restrict(amp)
	{
		// Allocate shared memory
		tile_static uint32_t MortonCodes_SHR[uTileSize];
		tile_static uint32_t Mask_SHR[4][uTileSize];

		uint32_t uLidx = Tidx.local[0];
		// Load data from global memory
		MortonCodes_SHR[Tidx.local[0]] = guarded_read(_ColYoke.MortonCodes, Tidx.global, UINT32_MAX);
		Tidx.barrier.wait();
		//------------------ load complete

		// Select significant bits
		uint32_t uExtractedBits = extract_bits(MortonCodes_SHR[Tidx.local[0]], _uBitOffset, 2);
		// iterate over the four possible values of a 2 bit radix
		for (uint32_t uDigit = 0; uDigit < 4; ++uDigit)
		{
			// count bit combinations
			Mask_SHR[uDigit][Tidx.local[0]] = uDigit == uExtractedBits; // Count <- Mask(Input, b)
		}

		Tidx.barrier.wait();
		//------------------ extract complete

		// Copy to prefix sum
		tile_static uint32_t PrefixSum_SHR[4][uTileSize];
		PrefixSum_SHR[0][Tidx.local[0]] = Mask_SHR[0][Tidx.local[0]];
		PrefixSum_SHR[1][Tidx.local[0]] = Mask_SHR[1][Tidx.local[0]];
		PrefixSum_SHR[2][Tidx.local[0]] = Mask_SHR[2][Tidx.local[0]];
		PrefixSum_SHR[3][Tidx.local[0]] = Mask_SHR[3][Tidx.local[0]];

		Tidx.barrier.wait();
		//------------------ copy complete

		four_way_parallel_scan<uTileSize>(PrefixSum_SHR, Tidx); // Count <- LocalPrefix[4]
		 // At this point Mask_SHR contains a digit dependend local offset for each digit not reflecting the true local offset

		uint32_t uTotalOccurences[4];
		uTotalOccurences[0] = Mask_SHR[0][uTileSize - 1] + PrefixSum_SHR[0][uTileSize - 1];
		uTotalOccurences[1] = Mask_SHR[1][uTileSize - 1] + PrefixSum_SHR[1][uTileSize - 1];
		uTotalOccurences[2] = Mask_SHR[2][uTileSize - 1] + PrefixSum_SHR[2][uTileSize - 1];
		uTotalOccurences[3] = Mask_SHR[3][uTileSize - 1] + PrefixSum_SHR[3][uTileSize - 1];

		if (Tidx.local[0] == 0)
		{
			_SortYoke.TilePrefixSum[0][Tidx.tile[0]] = uTotalOccurences[0];
			_SortYoke.TilePrefixSum[1][Tidx.tile[0]] = uTotalOccurences[1];
			_SortYoke.TilePrefixSum[2][Tidx.tile[0]] = uTotalOccurences[2];
			_SortYoke.TilePrefixSum[3][Tidx.tile[0]] = uTotalOccurences[3];
		}

		Tidx.barrier.wait();
		//------------------ write complete

		// merge results
		uint32_t uPreviousElems[4];
		uPreviousElems[0] = PrefixSum_SHR[0][uLidx];
		uPreviousElems[1] = PrefixSum_SHR[1][uLidx] + uTotalOccurences[0];
		uPreviousElems[2] = PrefixSum_SHR[2][uLidx] + uTotalOccurences[0] + uTotalOccurences[1];
		uPreviousElems[3] = PrefixSum_SHR[3][uLidx] + uTotalOccurences[0] + uTotalOccurences[1] + uTotalOccurences[2];

		tile_static uint32_t Offset_SHR[uTileSize];
		Offset_SHR[uLidx] = 0;
		Offset_SHR[uLidx] += (Mask_SHR[0][uLidx]) ? uPreviousElems[0] : 0;
		Offset_SHR[uLidx] += (Mask_SHR[1][uLidx]) ? uPreviousElems[1] : 0;
		Offset_SHR[uLidx] += (Mask_SHR[2][uLidx]) ? uPreviousElems[2] : 0;
		Offset_SHR[uLidx] += (Mask_SHR[3][uLidx]) ? uPreviousElems[3] : 0;

		Tidx.barrier.wait();
		//------------------ merge complete
		index<1> ShuffleIndex(Tidx.tile_origin[0] + Offset_SHR[uLidx]);
		guarded_write<uint32_t>(_SortYoke.LocalOffset, index<1>(Tidx.global[0]), ShuffleIndex[0]);
		guarded_write(_ColYoke.MortonCodes, ShuffleIndex, MortonCodes_SHR[uLidx]);
		_SortYoke.LocalPrefixSum[ShuffleIndex] = PrefixSum_SHR[uExtractedBits][Tidx.local[0]];
	});
}
//---------------------------------------------------------------------------------------------------------------------
template<uint32_t uTileSize>
void RadixSorter::GlobalSorting(const uint32_t _uBitOffset, SorterYoke& _SortYoke, CollisionYoke& _ColYoke) HCPU
{
	global_scan<256>(_SortYoke.TilesExtent, _SortYoke.TilePrefixSum);

	copy(_ColYoke.MortonCodes, _SortYoke.Tmp);
	parallel_for_each(_SortYoke.ActiveGOExtent, [_uBitOffset, _SortYoke, _ColYoke](index<1> Idx) restrict(amp)
	{
		uint32_t uDigit = _SortYoke.Tmp[Idx];
		uint32_t uBits = extract_bits(uDigit, _uBitOffset, 2);
		uint32_t uPosition = _SortYoke.LocalPrefixSum[Idx] + _SortYoke.TilePrefixSum[uBits][tile_from_index(Idx, uTileSize)];
		_SortYoke.IndexOffset[Idx] = uPosition;
		_ColYoke.MortonCodes[uPosition] = uDigit;
	});
	copy(_SortYoke.ReorderIndices, _SortYoke.Tmp);
	parallel_for_each(_SortYoke.ActiveGOExtent, [_SortYoke, _ColYoke](index<1> Idx) restrict(amp)
	{
		_SortYoke.ReorderIndices[Idx] = _SortYoke.IndexOffset[_SortYoke.LocalOffset[_SortYoke.Tmp[Idx]]];
	});
}
//---------------------------------------------------------------------------------------------------------------------
// Get morton codes from game object's position
void RadixSorter::ComputeMortonCodes(SorterYoke& _SortYoke, CollisionYoke& _ColYoke, RigidbodyYoke& _RbYoke)
{
	parallel_for_each(_SortYoke.ActiveGOExtent, [_SortYoke, _ColYoke, _RbYoke](index<1> Idx) restrict(amp)
	{
		// if game object is initialized compute morton code for it
		if (CheckFlag(_RbYoke.uFlags[Idx], GameObjectFlag::Destroyed) == false)
		{
			_ColYoke.MortonCodes[Idx] = MortonCode(_RbYoke.vPosition[Idx]) + Idx[0];
		}
		else
		{
			_ColYoke.MortonCodes[Idx] = UINT32_MAX;
		}
		// initialize the indices
		_SortYoke.ReorderIndices[Idx] = Idx[0];
	});
}
//---------------------------------------------------------------------------------------------------------------------
void RadixSorter::ReorderIndices(SorterYoke& _SortYoke, RigidbodyYoke& _RbYoke)
{
	copy(_RbYoke.uIndex, _SortYoke.Tmp);
	parallel_for_each(_SortYoke.ActiveGOExtent, [_SortYoke, _RbYoke](index<1> Idx) restrict(amp)
	{
		_RbYoke.uIndex[Idx] = _SortYoke.ReorderIndices[_SortYoke.Tmp[Idx]];
	});
}
//---------------------------------------------------------------------------------------------------------------------
template<typename T>
void RadixSorter::Reorder(array<T>& _Data, array<uint32_t>& _Indices, concurrency::extent<1> _Extent)
{
	array<T> TmpData(_Data.extent, m_AcceleratorView);
	copy(_Data, TmpData);
	parallel_for_each(_Extent, [&_Data, &_Indices, &TmpData](index<1> Idx) restrict(amp)
	{
		_Data[_Indices[Idx]] = TmpData[Idx];
	});
}
