//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher, David Labode
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "NarrowPhase.h"
#include "DataStructures\GameObjectPool.h"
#include "CollisionSystem.h"

using namespace Helix::Collision;
using namespace Helix::Datastructures;
using namespace Helix::AMP;
//--------------------------------------------------------------------------------------------------------------------------
void NarrowPhase::Sieve(RigidbodyYoke& _RbYoke, CollisionYoke& _ColYoke)
{
	NarrowPhaseYoke NpYoke(this);
	const uint32_t uNumCollisions = _ColYoke.uNumCollisions;
	tiled_extent<1, 16> Extent = extent<2>(_ColYoke.uNumCollisions, 16).tile<1, 16>();
	parallel_for_each(Extent, [_RbYoke, _ColYoke, NpYoke](tiled_index<1,16> Tidx) HGPU
	{
		 
	});
}
