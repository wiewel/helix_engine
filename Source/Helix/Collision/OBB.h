//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef OBB_H
#define OBB_H

//#include "AABB.h"
#include "Math\MathTypes.h"
#include <DirectXCollision.h>
#include "CollisionInformation.h"
namespace Helix
{
	namespace Collision
	{
		class OBB
		{
		friend class AABB;

		public:
			Math::float3 m_vCenter;
			Math::float3 m_vHalfExtents;
			Math::quaternion m_vOrientation;
		public:
			// Constructors
			OBB()
				: m_vCenter(0, 0, 0), m_vHalfExtents(0.0f, 0.0f, 0.0f), m_vOrientation(0, 0, 0, 1.f) {}
			OBB(const Math::float3& _Center, const Math::float3& _Extents, const Math::quaternion& _Orientation)
				: m_vCenter(_Center), m_vHalfExtents(0.5f * _Extents), m_vOrientation(_Orientation) {}
			OBB(const OBB& _Box)
				: m_vCenter(_Box.m_vCenter), m_vHalfExtents(_Box.m_vHalfExtents), m_vOrientation(_Box.m_vOrientation) {}
			OBB(const DirectX::BoundingOrientedBox& _Box)
				: m_vCenter(_Box.Center), m_vHalfExtents(_Box.Extents), m_vOrientation(Math::quaternion( _Box.Orientation.x,_Box.Orientation.y,_Box.Orientation.z,_Box.Orientation.w))
			{}

			void SeparatingAxisTest(OBB _Other);

			// Operators
			OBB& operator=(const OBB& _Box);
			
			void GetCorners(Math::float3(&_Corners)[8]) const ;

			OBB Transformed(const Math::float3& _vPosition, const Math::float3& _vScale, const Math::quaternion& _qOrientation) const ;
			// Intersection
			bool Intersects(const OBB& _Other, CollisionInformation& _Info) const;
		};
	}
}
//---------------------------------------------------------------------------------------------------------------------
inline void Helix::Collision::OBB::SeparatingAxisTest(OBB _Other)
{

}
//---------------------------------------------------------------------------------------------------------------------
// Operators
inline Helix::Collision::OBB& Helix::Collision::OBB::operator=(const OBB& _Box) 
{
	m_vCenter = _Box.m_vCenter;
	m_vHalfExtents = _Box.m_vHalfExtents;
	m_vOrientation = _Box.m_vOrientation;
	return *this;
}
//---------------------------------------------------------------------------------------------------------------------
inline void Helix::Collision::OBB::GetCorners(Math::float3(&_Corners)[8]) const {
	_Corners[0] = m_vHalfExtents;
	_Corners[1] = m_vHalfExtents;
	_Corners[2] = m_vHalfExtents;
	_Corners[3] = m_vHalfExtents;
	_Corners[4] = m_vHalfExtents;
	_Corners[5] = m_vHalfExtents;
	_Corners[6] = m_vHalfExtents;
	_Corners[7] = m_vHalfExtents;

	_Corners[0].xy *= -1.f;
	_Corners[1].y *= -1.f;
	//_Corners[2] *= 1
	_Corners[3].x *= -1.f;
	_Corners[4] *= -1.f;
	_Corners[5].yz *= -1.f;
	_Corners[6].z *= -1.f;
	_Corners[7].xz *= -1.f;

	_Corners[0] = m_vOrientation * _Corners[0];
	_Corners[1] = m_vOrientation * _Corners[1];
	_Corners[2] = m_vOrientation * _Corners[2];
	_Corners[3] = m_vOrientation * _Corners[3];
	_Corners[4] = m_vOrientation * _Corners[4];
	_Corners[5] = m_vOrientation * _Corners[5];
	_Corners[6] = m_vOrientation * _Corners[6];
	_Corners[7] = m_vOrientation * _Corners[7];

	_Corners[0] += m_vCenter;
	_Corners[1] += m_vCenter;
	_Corners[2] += m_vCenter;
	_Corners[3] += m_vCenter;
	_Corners[4] += m_vCenter;
	_Corners[5] += m_vCenter;
	_Corners[6] += m_vCenter;
	_Corners[7] += m_vCenter;

	//_Corners[0] = Orientation *(Extents * float3(-1.0f, -1.0f, 1.0f)) + Center;
	//_Corners[1] = Orientation *(Extents * float3(1.0f, -1.0f, 1.0f)) + Center;
	//_Corners[2] = Orientation *(Extents * float3(1.0f, 1.0f, 1.0f)) + Center;
	//_Corners[3] = Orientation * (Extents * float3(-1.0f, 1.0f, 1.0f)) + Center;
	//_Corners[4] = Orientation * (Extents * float3(-1.0f, -1.0f, -1.0f)) + Center;
	//_Corners[5] = Orientation * (Extents * float3(1.0f, -1.0f, -1.0f )) + Center;
	//_Corners[6] = Orientation * (Extents * float3(1.0f, 1.0f, -1.0f)) + Center;
	//_Corners[7] = Orientation * (Extents * float3(-1.0f, 1.0f, -1.0f)) + Center;
}
//---------------------------------------------------------------------------------------------------------------------
inline Helix::Collision::OBB Helix::Collision::OBB::Transformed(const Math::float3& _vPosition, const Math::float3& _vScale, const Math::quaternion& _qOrientation) const
{
	Math::float3 vCenter = _vPosition + m_vCenter;
	Math::float3 vExtents = (2.0f * m_vHalfExtents) * _vScale;
	Math::quaternion qOrientation = m_vOrientation * _qOrientation;
	return OBB(vCenter, vExtents, qOrientation);
}

// Intersection
// TODO
inline bool Helix::Collision::OBB::Intersects(const OBB & _Other, CollisionInformation & _Info) const 
{
	using namespace DirectX;
	_Info.m_vNormal = XMFLOAT3_ZERO;
	_Info.m_fPenetrationDepth = 0.0f;
	// Build the 3x3 rotation matrix that defines the orientation of B relative to A.
	XMVECTOR A_quat = XMLoadFloat4(&Math::Convert(m_vOrientation));
	XMVECTOR B_quat = XMLoadFloat4(&Math::Convert(_Other.m_vOrientation));

	HASSERTD(DirectX::Internal::XMQuaternionIsUnit(A_quat), "Quaternion does not have unit length!");
	HASSERTD(DirectX::Internal::XMQuaternionIsUnit(B_quat), "Quaternion does not have unit length!");

	XMVECTOR Q = XMQuaternionMultiply(A_quat, XMQuaternionConjugate(B_quat));
	XMMATRIX R = XMMatrixRotationQuaternion(Q);

	// Compute the translation of B relative to A.
	XMVECTOR A_cent = XMLoadFloat3(&Math::Convert(m_vCenter));
	XMVECTOR B_cent = XMLoadFloat3(&Math::Convert(_Other.m_vCenter));
	XMVECTOR t = XMVector3InverseRotate(B_cent - A_cent, A_quat);
	XMFLOAT3 vTf = Math::XMFloat3Get(t);

	const static float fTolerance = 0.0f;

	// Test each of the 15 possible seperating axii.
	// =============================================================================
	// h(A) = extents of A.
	// h(B) = extents of B.
	// -----------------------------------------------------------------------------
	// a(u) = axes of A = (1,0,0), (0,1,0), (0,0,1)
	// b(u) = axes of B relative to A = (r00,r10,r20), (r01,r11,r21), (r02,r12,r22)
	// -----------------------------------------------------------------------------
	// For each possible separating axis l:
	//   d(A) = sum (for i = u,v,w) h(A)(i) * abs( a(i) dot l )
	//   d(B) = sum (for i = u,v,w) h(B)(i) * abs( b(i) dot l )
	//   if abs( t dot l ) > d(A) + d(B) then disjoint


	// A.x
	// ------------------------------------------
	// l = a(u) = (1, 0, 0)
	// t dot l = t.x
	// d(A) = h(A).x
	// d(B) = h(B) dot abs(r00, r01, r02)
	XMVECTOR R0X = XMVector3Normalize(R.r[0]);
	XMVECTOR AR0X = XMVectorAbs(R0X);
	XMFLOAT3 vExtentsAf = Convert(m_vHalfExtents);
	XMVECTOR h_B = XMLoadFloat3(&Math::Convert(_Other.m_vHalfExtents)); // h(B) = extents of B.

	float fOverlap = vExtentsAf.x + XMVectorGetX(XMVector3Dot(h_B, AR0X)) - fabsf(vTf.x);
	if (fOverlap < fTolerance)
	{
		return false;
	}
	// MTV variables
	float fMinimalOverlap = fOverlap;
	XMVECTOR vMinimalTestAxis = XMVectorSet(1.0f, 0.0f, 0.0f, 0.0f); // = vL

																	 // A.y
																	 // ------------------------------------------
																	 // l = a(v) = (0, 1, 0)
																	 // t dot l = t.y
																	 // d(A) = h(A).y
																	 // d(B) = h(B) dot abs(r10, r11, r12)
	XMVECTOR R1X = XMVector3Normalize(R.r[1]);
	XMVECTOR AR1X = XMVectorAbs(R1X);
	float d_a = vExtentsAf.y;
	float d_b = XMVectorGetY(XMVector3Dot(h_B, AR1X));
	float tdl = fabsf(vTf.y);
	float o = d_a + d_b - tdl;
	fOverlap = vExtentsAf.y + XMVectorGetY(XMVector3Dot(h_B, AR1X)) - fabsf(vTf.y);
	if (fOverlap < fTolerance)
	{
		return false;
	}
	else if (fOverlap < fMinimalOverlap)
	{
		fMinimalOverlap = fOverlap;
		vMinimalTestAxis = XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f); // = vL
	}

	// A.z
	// ------------------------------------------
	// l = a(w) = (0, 0, 1)
	// t dot l = t.z
	// d(A) = h(A).z
	// d(B) = h(B) dot abs(r20, r21, r22)
	XMVECTOR R2X = XMVector3Normalize(R.r[2]);
	XMVECTOR AR2X = XMVectorAbs(R2X);

	fOverlap = vExtentsAf.z + XMVectorGetZ(XMVector3Dot(h_B, AR2X)) - fabsf(vTf.z);
	if (fOverlap  < fTolerance)
	{
		return false;
	}
	else if (fOverlap < fMinimalOverlap)
	{
		fMinimalOverlap = fOverlap;
		vMinimalTestAxis = XMVectorSet(0.0f, 0.0f, 1.0f, 0.0f); // = vL
	}


	// now collums are considered
	R = XMMatrixTranspose(R);

	// B.x
	// ------------------------------------------
	// l = b(u) = (r00, r10, r20)
	// d(A) = h(A) dot abs(r00, r10, r20)
	// d(B) = h(B).x
	XMVECTOR RX0 = XMVector3Normalize(R.r[0]);
	XMVECTOR ARX0 = XMVectorAbs(RX0);
	XMFLOAT3 vExtentsBf = Convert(_Other.m_vHalfExtents);
	XMVECTOR h_A = XMLoadFloat3(&Math::Convert(m_vHalfExtents)); // h(A) = extents of A.

	fOverlap = XMVectorGetX(XMVectorSubtract(XMVector3Dot(h_A, ARX0), XMVectorAbs(XMVector3Dot(t, RX0)))) + vExtentsBf.x;
	if (fOverlap < fTolerance)
	{
		return false;
	}
	else if (fOverlap < fMinimalOverlap)
	{
		fMinimalOverlap = fOverlap;
		vMinimalTestAxis = RX0; // = vL
	}

	// B.y
	// ------------------------------------------
	// l = b(v) = (r01, r11, r21)
	// d(A) = h(A) dot abs(r01, r11, r21)
	// d(B) = h(B).y
	XMVECTOR RX1 = XMVector3Normalize(R.r[1]);
	XMVECTOR ARX1 = XMVectorAbs(RX1);

	fOverlap = XMVectorGetX(XMVectorSubtract(XMVector3Dot(h_A, ARX1), XMVectorAbs(XMVector3Dot(t, RX1)))) + vExtentsBf.y;
	if (fOverlap < fTolerance)
	{
		return false;
	}
	else if (fOverlap < fMinimalOverlap)
	{
		fMinimalOverlap = fOverlap;
		vMinimalTestAxis = RX1; // = vL
	}

	// B.z
	// ------------------------------------------
	// l = b(w) = (r02, r12, r22)
	// d(A) = h(A) dot abs(r02, r12, r22)
	// d(B) = h(B).z
	XMVECTOR RX2 = XMVector3Normalize(R.r[2]);
	XMVECTOR ARX2 = XMVectorAbs(RX2);

	fOverlap = XMVectorGetX(XMVectorSubtract(XMVector3Dot(h_A, ARX2), XMVectorAbs(XMVector3Dot(t, RX2)))) + vExtentsBf.z;
	if (fOverlap < fTolerance)
	{
		return false;
	}
	else if (fOverlap < fMinimalOverlap)
	{
		fMinimalOverlap = fOverlap;
		vMinimalTestAxis = RX2; // = vL
	}

	// A.x  x  B.x
	// ------------------------------------------
	// l = a(u) x b(u) = (0, -r20, r10)
	// d(A) = h(A) dot abs(0, r20, r10)
	// d(B) = h(B) dot abs(0, r02, r01)
	// normalize test axis, to get correct MTVs, d should only depend on t and projection (not length of test axis)
	XMVECTOR vL = XMVector3Normalize(XMVectorPermute<XM_PERMUTE_0W, XM_PERMUTE_1Z, XM_PERMUTE_0Y, XM_PERMUTE_0X>(RX0, -RX0));
	XMVECTOR vD;
	XMVECTOR vD_A;
	XMVECTOR vD_B;
	if (XMVector3NotEqual(XMVectorZero(), vL))
	{
		vD = XMVector3Dot(t, vL);
		vD_A = XMVector3Dot(h_A, XMVector3Normalize(XMVectorSwizzle<XM_SWIZZLE_W, XM_SWIZZLE_Z, XM_SWIZZLE_Y, XM_SWIZZLE_X>(ARX0)));
		vD_B = XMVector3Dot(h_B, XMVector3Normalize(XMVectorSwizzle<XM_SWIZZLE_W, XM_SWIZZLE_Z, XM_SWIZZLE_Y, XM_SWIZZLE_X>(AR0X)));
		fOverlap = XMVectorGetX(XMVectorSubtract(XMVectorAdd(vD_A, vD_B), XMVectorAbs(vD)));
		if (fOverlap < fTolerance)
		{
			return false;
		}
		else if (fOverlap < fMinimalOverlap)
		{
			fMinimalOverlap = fOverlap;
			vMinimalTestAxis = vL; // = vL
		}
	}


	// A.x  x  B.y
	// ------------------------------------------
	// l = a(u) x b(v) = (0, -r21, r11)
	// d(A) = h(A) dot abs(0, r21, r11)
	// d(B) = h(B) dot abs(r02, 0, r00)
	vL = XMVector3Normalize(XMVectorPermute<XM_PERMUTE_0W, XM_PERMUTE_1Z, XM_PERMUTE_0Y, XM_PERMUTE_0X>(RX1, -RX1));
	if (XMVector3NotEqual(XMVectorZero(), vL))
	{
		vD = XMVector3Dot(t, vL);
		vD_A = XMVector3Dot(h_A, XMVector3Normalize(XMVectorSwizzle<XM_SWIZZLE_W, XM_SWIZZLE_Z, XM_SWIZZLE_Y, XM_SWIZZLE_X>(ARX1)));
		vD_B = XMVector3Dot(h_B, XMVector3Normalize(XMVectorSwizzle<XM_SWIZZLE_Z, XM_SWIZZLE_W, XM_SWIZZLE_X, XM_SWIZZLE_Y>(AR0X)));
		fOverlap = XMVectorGetX(XMVectorSubtract(XMVectorAdd(vD_A, vD_B), XMVectorAbs(vD)));
		if (fOverlap < fTolerance)
		{
			return false;
		}
		else if (fOverlap < fMinimalOverlap)
		{
			fMinimalOverlap = fOverlap;
			vMinimalTestAxis = vL; // = vL
		}
	}


	// A.x  x  B.z
	// ------------------------------------------
	// l = a(u) x b(w) = (0, -r22, r12)
	// d(A) = h(A) dot abs(0, r22, r12)
	// d(B) = h(B) dot abs(r01, r00, 0)
	vL = XMVector3Normalize(XMVectorPermute<XM_PERMUTE_0W, XM_PERMUTE_1Z, XM_PERMUTE_0Y, XM_PERMUTE_0X>(RX2, -RX2));
	if (XMVector3NotEqual(XMVectorZero(), vL))
	{
		vD = XMVector3Dot(t, vL);
		vD_A = XMVector3Dot(h_A, XMVector3Normalize(XMVectorSwizzle<XM_SWIZZLE_W, XM_SWIZZLE_Z, XM_SWIZZLE_Y, XM_SWIZZLE_X>(ARX2)));
		vD_B = XMVector3Dot(h_B, XMVector3Normalize(XMVectorSwizzle<XM_SWIZZLE_Y, XM_SWIZZLE_X, XM_SWIZZLE_W, XM_SWIZZLE_Z>(AR0X)));
		fOverlap = XMVectorGetX(XMVectorSubtract(XMVectorAdd(vD_A, vD_B), XMVectorAbs(vD)));
		if (fOverlap < fTolerance)
		{
			return false;
		}
		else if (fOverlap < fMinimalOverlap)
		{
			fMinimalOverlap = fOverlap;
			vMinimalTestAxis = vL; // = vL
		}
	}


	// A.y  x  B.x
	// ------------------------------------------
	// l = a(v) x b(u) = (r20, 0, -r00)
	// d(A) = h(A) dot abs(r20, 0, r00)
	// d(B) = h(B) dot abs(0, r12, r11)
	vL = XMVector3Normalize(XMVectorPermute<XM_PERMUTE_0Z, XM_PERMUTE_0W, XM_PERMUTE_1X, XM_PERMUTE_0Y>(RX0, -RX0));
	if (XMVector3NotEqual(XMVectorZero(), vL))
	{
		vD = XMVector3Dot(t, vL);
		vD_A = XMVector3Dot(h_A, XMVector3Normalize(XMVectorSwizzle<XM_SWIZZLE_Z, XM_SWIZZLE_W, XM_SWIZZLE_X, XM_SWIZZLE_Y>(ARX0)));
		vD_B = XMVector3Dot(h_B, XMVector3Normalize(XMVectorSwizzle<XM_SWIZZLE_W, XM_SWIZZLE_Z, XM_SWIZZLE_Y, XM_SWIZZLE_X>(AR1X)));
		fOverlap = XMVectorGetX(XMVectorSubtract(XMVectorAdd(vD_A, vD_B), XMVectorAbs(vD)));
		if (fOverlap < fTolerance)
		{
			return false;
		}
		else if (fOverlap < fMinimalOverlap)
		{
			fMinimalOverlap = fOverlap;
			vMinimalTestAxis = vL; // = vL
		}
	}


	// A.y  x  B.y
	// ------------------------------------------
	// l = a(v) x b(v) = (r21, 0, -r01)
	// d(A) = h(A) dot abs(r21, 0, r01)
	// d(B) = h(B) dot abs(r12, 0, r10)
	vL = XMVector3Normalize(XMVectorPermute<XM_PERMUTE_0Z, XM_PERMUTE_0W, XM_PERMUTE_1X, XM_PERMUTE_0Y>(RX1, -RX1));
	if (XMVector3NotEqual(XMVectorZero(), vL))
	{
		vD = XMVector3Dot(t, vL);
		vD_A = XMVector3Dot(h_A, XMVector3Normalize(XMVectorSwizzle<XM_SWIZZLE_Z, XM_SWIZZLE_W, XM_SWIZZLE_X, XM_SWIZZLE_Y>(ARX1)));
		vD_B = XMVector3Dot(h_B, XMVector3Normalize(XMVectorSwizzle<XM_SWIZZLE_Z, XM_SWIZZLE_W, XM_SWIZZLE_X, XM_SWIZZLE_Y>(AR1X)));
		fOverlap = XMVectorGetX(XMVectorSubtract(XMVectorAdd(vD_A, vD_B), XMVectorAbs(vD)));
		if (fOverlap < fTolerance)
		{
			return false;
		}
		else if (fOverlap < fMinimalOverlap)
		{
			fMinimalOverlap = fOverlap;
			vMinimalTestAxis = vL; // = vL
		}
	}


	// A.y  x  B.z
	// ------------------------------------------
	// l = a(v) x b(w) = (r22, 0, -r02)
	// d(A) = h(A) dot abs(r22, 0, r02)
	// d(B) = h(B) dot abs(r11, r10, 0)
	vL = XMVector3Normalize(XMVectorPermute<XM_PERMUTE_0Z, XM_PERMUTE_0W, XM_PERMUTE_1X, XM_PERMUTE_0Y>(RX2, -RX2));
	if (XMVector3NotEqual(XMVectorZero(), vL))
	{
		vD = XMVector3Dot(t, vL);
		vD_A = XMVector3Dot(h_A, XMVector3Normalize(XMVectorSwizzle<XM_SWIZZLE_Z, XM_SWIZZLE_W, XM_SWIZZLE_X, XM_SWIZZLE_Y>(ARX2)));
		vD_B = XMVector3Dot(h_B, XMVector3Normalize(XMVectorSwizzle<XM_SWIZZLE_Y, XM_SWIZZLE_X, XM_SWIZZLE_W, XM_SWIZZLE_Z>(AR1X)));
		fOverlap = XMVectorGetX(XMVectorSubtract(XMVectorAdd(vD_A, vD_B), XMVectorAbs(vD)));
		if (fOverlap < fTolerance)
		{
			return false;
		}
		else if (fOverlap < fMinimalOverlap)
		{
			fMinimalOverlap = fOverlap;
			vMinimalTestAxis = vL; // = vL
		}
	}


	// A.z  x  B.x
	// ------------------------------------------
	// l = a(w) x b(u) = (-r10, r00, 0)
	// d(A) = h(A) dot abs(r10, r00, 0)
	// d(B) = h(B) dot abs(0, r22, r21)
	vL = XMVector3Normalize(XMVectorPermute<XM_PERMUTE_1Y, XM_PERMUTE_0X, XM_PERMUTE_0W, XM_PERMUTE_0Z>(RX0, -RX0));
	if (XMVector3NotEqual(XMVectorZero(), vL))
	{
		vD = XMVector3Dot(t, vL);
		vD_A = XMVector3Dot(h_A, XMVector3Normalize(XMVectorSwizzle<XM_SWIZZLE_Y, XM_SWIZZLE_X, XM_SWIZZLE_W, XM_SWIZZLE_Z>(ARX0)));
		vD_B = XMVector3Dot(h_B, XMVector3Normalize(XMVectorSwizzle<XM_SWIZZLE_W, XM_SWIZZLE_Z, XM_SWIZZLE_Y, XM_SWIZZLE_X>(AR2X)));
		fOverlap = XMVectorGetX(XMVectorSubtract(XMVectorAdd(vD_A, vD_B), XMVectorAbs(vD)));
		if (fOverlap < fTolerance)
		{
			return false;
		}
		else if (fOverlap < fMinimalOverlap)
		{
			fMinimalOverlap = fOverlap;
			vMinimalTestAxis = vL; // = vL
		}

	}

	// A.z  x  B.y
	// ------------------------------------------
	// l = a(w) x b(v) = (-r11, r01, 0)
	// d(A) = h(A) dot abs(r11, r01, 0)
	// d(B) = h(B) dot abs(r22, 0, r20)
	vL = XMVector3Normalize(XMVectorPermute<XM_PERMUTE_1Y, XM_PERMUTE_0X, XM_PERMUTE_0W, XM_PERMUTE_0Z>(RX1, -RX1));
	if (XMVector3NotEqual(XMVectorZero(), vL))
	{
		vD = XMVector3Dot(t, vL);
		vD_A = XMVector3Dot(h_A, XMVector3Normalize(XMVectorSwizzle<XM_SWIZZLE_Y, XM_SWIZZLE_X, XM_SWIZZLE_W, XM_SWIZZLE_Z>(ARX1)));
		vD_B = XMVector3Dot(h_B, XMVector3Normalize(XMVectorSwizzle<XM_SWIZZLE_Z, XM_SWIZZLE_W, XM_SWIZZLE_X, XM_SWIZZLE_Y>(AR2X)));
		fOverlap = XMVectorGetX(XMVectorSubtract(XMVectorAdd(vD_A, vD_B), XMVectorAbs(vD)));
		if (fOverlap < fTolerance)
		{
			return false;
		}
		else if (fOverlap < fMinimalOverlap)
		{
			fMinimalOverlap = fOverlap;
			vMinimalTestAxis = vL; // = vL
		}

	}

	// A.z  x  B.z
	// ------------------------------------------
	// l = a(w) x b(w) = (-r12, r02, 0)
	// d(A) = h(A) dot abs(r12, r02, 0)
	// d(B) = h(B) dot abs(r21, r20, 0)
	vL = XMVector3Normalize(XMVectorPermute<XM_PERMUTE_1Y, XM_PERMUTE_0X, XM_PERMUTE_0W, XM_PERMUTE_0Z>(RX2, -RX2));
	if (XMVector3NotEqual(XMVectorZero(), vL))
	{
		vD = XMVector3Dot(t, vL);
		vD_A = XMVector3Dot(h_A, XMVector3Normalize(XMVectorSwizzle<XM_SWIZZLE_Y, XM_SWIZZLE_X, XM_SWIZZLE_W, XM_SWIZZLE_Z>(ARX2)));
		vD_B = XMVector3Dot(h_B, XMVector3Normalize(XMVectorSwizzle<XM_SWIZZLE_Y, XM_SWIZZLE_X, XM_SWIZZLE_W, XM_SWIZZLE_Z>(AR2X)));
		fOverlap = XMVectorGetX(XMVectorSubtract(XMVectorAdd(vD_A, vD_B), XMVectorAbs(vD)));
		if (fOverlap < fTolerance)
		{
			return false;
		}
		else if (fOverlap < fMinimalOverlap)
		{
			fMinimalOverlap = fOverlap;
			vMinimalTestAxis = vL; // = vL
		}

	}

	// No seperating axis found, boxes must intersect.
	XMVECTOR vNormal = XMVector3Normalize(Math::XMVector3Rotate(vMinimalTestAxis, A_quat));

	if (XMVector3Greater(XMVector3Dot(vNormal, XMVectorSubtract(B_cent, A_cent)), XMVectorZero()))
	{
		vNormal = XMVectorNegate(vNormal);
	}

	XMStoreFloat3(&_Info.m_vNormal, vNormal);
	_Info.m_fPenetrationDepth = fabsf(fMinimalOverlap);

	return true;
}
#endif //OBB_H