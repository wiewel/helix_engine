//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef AABB_H
#define AABB_H

#include "Math\MathTypes.h"
#include "Math\MathFunctions.h"
#include "OBB.h"

namespace Helix
{
	namespace Collision
	{
		class AABB
		{
		friend class OBB;
		public:
			Math::float3 m_vCenter;
			Math::float3 m_vHalfExtents;
		public:
			// Constructors
			AABB() 
				: m_vCenter(0, 0, 0), m_vHalfExtents(0.5f, 0.5f, 0.5f) {}
			AABB(const Math::float3& _m_vCenter, const Math::float3& _vExtents)
				: m_vCenter(_m_vCenter), m_vHalfExtents(_vExtents * 0.5f) {}
			AABB(const AABB& _Box) 
				: m_vCenter(_Box.m_vCenter), m_vHalfExtents(_Box.m_vHalfExtents) {}
			AABB(const OBB& _OBB) : m_vCenter(_OBB.m_vCenter)
			{
				Math::float3 Corners[8];
				_OBB.GetCorners(Corners);

				Math::float3 vMin(Corners[0]);
				Math::float3 vMax(Corners[0]);

				for (uint32_t i = 1; i < 8; ++i)
				{
					vMin = Math::Min(vMin, Corners[i]);
					vMax = Math::Max(vMax, Corners[i]);
				}

				m_vHalfExtents = (vMax - vMin) * 0.5f;
			}
			AABB(const AABB& _Box1, const AABB& _Box2) 
			{
				Math::float3& Center1 = _Box1.GetCenter();
				Math::float3& Center2 = _Box2.GetCenter();
				Math::float3 vDistance = Math::Abs(Center2 - Center1);
				m_vHalfExtents = (_Box1.m_vHalfExtents + vDistance + _Box2.m_vHalfExtents) * 0.5f;
				m_vCenter = (Center1 + Center2) * 0.5f;
			}
			// Operators
			AABB& operator=(const AABB& _Box) 
			{
				m_vCenter = _Box.m_vCenter;
				m_vHalfExtents = _Box.m_vHalfExtents;
				return *this;
			}
			// Getters & Setters
			inline Math::float3 GetCenter() const 
			{
				return m_vCenter;
			}
			inline void SetCenter(const Math::float3& _Pos) 
			{
				m_vCenter = _Pos;
			}
			//Intersection
			bool Intersects(const AABB& _OtherAABB) const;
		};
	}
}

inline bool Helix::Collision::AABB::Intersects(const AABB& _OtherAABB) const 
{
	Math::float3 MinA = m_vCenter - m_vHalfExtents;
	Math::float3 MaxA = m_vCenter + m_vHalfExtents;
	Math::float3 MinB = _OtherAABB.m_vCenter - _OtherAABB.m_vHalfExtents;
	Math::float3 MaxB = _OtherAABB.m_vCenter + _OtherAABB.m_vHalfExtents;
	
	//return false == ((MaxA < MinB).AnyTrue() || (MaxB < MinA).AnyTrue());

	return false == (MaxA.x < MinB.x || MaxA.y < MinB.y || MaxA.z < MinB.z ||
					MaxB.x < MinA.x || MaxB.y < MinA.y || MaxB.z < MinA.z);
}
#endif //AABB_H