//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef COLLISIONRESOLUTION_H
#define COLLISIONRESOLUTION_H

#include <vector>

#include "DataStructures\TplGameObject.h"
#include "Math\XMMath.h"
#include "XMCollision.h"



namespace Helix
{
	namespace Collision
	{
		using namespace DirectX;
		
		struct CollisionObject
		{
			Helix::Datastructures::TplGameObject* pGameObject; // The game object corresponding to the collision object
			const XMVECTOR vCenter; // X, center of mass
			const XMVECTOR vRelativeCollisionPoint; //R, distance vector to colision point
			XMVECTOR vLinearVelocity; // V
			const XMVECTOR vAngularVelocity; // W
			const XMVECTOR vCollisionPointVelocity; // = V + (W x R), velocity at collision point
			const XMVECTOR vDeltaCollisionPointVelocity; // Velocity change from last timeframe at colision point (gravity etc.)
			const XMMATRIX mInverseInertiaTensor; // Moment of inertia, depending on weight and size, determining angular velocity
			const XMMATRIX mInertiaTensor;
			const float fElasticity; // eo [0,1] 0: fully plastic, 1: fully elastic
			const float fInvMass; // 1/M
			const float fFriction; // coefficient of friction
			//float fImpulse; // object specific impulse needed for resting contact
			//float fDividend;
			XMVECTOR vAngularMomentum = XMVECTOR_ZERO;

			CollisionObject(Helix::Datastructures::TplGameObject* _pGO, CollisionInfo& _CollisionInfo) :
				pGameObject(_pGO),
				vCenter(XMLoadFloat3(&_pGO->m_vPosition)),
				vRelativeCollisionPoint(XMVectorSubtract(XMLoadFloat3(&_CollisionInfo.m_vPoint), vCenter)),
				vLinearVelocity(XMLoadFloat3(&_pGO->GetRigidBodyData()->m_vLinearVelocity)),
				vAngularVelocity(XMLoadFloat3(&_pGO->GetRigidBodyData()->m_vAngularVelocity)),
				vCollisionPointVelocity(XMVectorAdd(vLinearVelocity, XMVector3Cross(vAngularVelocity, vRelativeCollisionPoint))),
				vDeltaCollisionPointVelocity(XMLoadFloat3(&_pGO->GetRigidBodyData()->m_vDeltaLinearVelocity)),
				mInverseInertiaTensor(XMLoadFloat3x3(&_pGO->GetRigidBodyData()->m_mInvInertiaTensor)),
				mInertiaTensor(XMLoadFloat3x3(&_pGO->GetRigidBodyData()->m_InertiaTensor)),
				fElasticity(_pGO->GetRigidBodyData()->m_fElasticity),
				fInvMass(_pGO->GetRigidBodyData()->m_fInvMass),
				fFriction(_pGO->GetRigidBodyData()->m_fFriction) { }
			~CollisionObject() {}
			void ApplyImpulse(XMVECTOR vVelocityChange)
			{
				if (pGameObject->CheckFlag(Helix::Datastructures::TplGameObjectFlag::TplGameObjectFlag_Static))
				{
					pGameObject->GetRigidBodyData()->m_vLinearVelocity = XMFLOAT3_ZERO;
					pGameObject->GetRigidBodyData()->m_vAngularMomentum = XMFLOAT3_ZERO;

					return;
				}
				vAngularMomentum = XMLoadFloat3(&pGameObject->GetRigidBodyData()->m_vAngularMomentum);
				vAngularMomentum = XMVectorAdd(vAngularMomentum, XMVector3Cross(vRelativeCollisionPoint, vVelocityChange));

				vVelocityChange = XMVectorScale(vVelocityChange, fInvMass);
				vLinearVelocity = XMVectorAdd(vLinearVelocity, vVelocityChange);

				XMStoreFloat3(&pGameObject->GetRigidBodyData()->m_vAngularMomentum, vAngularMomentum);
				XMStoreFloat3(&pGameObject->GetRigidBodyData()->m_vLinearVelocity, vLinearVelocity);
			}
		};

		class CollisionResolution
		{
			HDEBUGNAME("CollisionResolution");
		public:
			static void ResolveCollision(CollisionInfo& _CollisionInfo);
		private:
			static void ResolveInterpenetration(CollisionInfo& _CollisionInfo);
			static void FindOBBCollisionFace(_Out_ std::vector<XMFLOAT3>& _vFace, _In_ const BoundingOrientedBox& _Box, _In_ const XMFLOAT3& _CollisionNormal);
			static void FindCollisionPoints(CollisionInfo& _CollisionInfo);
			static void ClipFaces(_In_ std::vector<XMFLOAT3>& _vClippingFace, _In_ std::vector<XMFLOAT3>& _vSubjectFace, _Out_ std::vector<XMFLOAT3>& _vClippedFace);
			static void CollisionResponse(CollisionInfo& _CollisionInfo);
		};
	}
}
#endif