//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher, David Labode
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "LinearBVH.h"
#include "CollisionSystem.h"
#include "broadselector_amp.h"
#include "AMP\AMPDefines.h"
using namespace Helix::Collision;
using namespace Helix::Datastructures;
using namespace Helix::AMP;

//----------------------------------------------------------------------------------------------------------------------------------------
void LinearBVH::Sieve(RigidbodyYoke& _RbYoke, CollisionYoke& _ColYoke)
{
	BroadPhaseYoke BpYoke(_RbYoke.uActiveGameObjectsCount, this);
	BuildLBVH(_RbYoke, _ColYoke, BpYoke);
	TraverseLBVH<32, 256>(_RbYoke, _ColYoke, BpYoke);
}
//----------------------------------------------------------------------------------------------------------------------------------------
void LinearBVH::BuildLBVH(RigidbodyYoke& _RbYoke, CollisionYoke& _ColYoke, BroadPhaseYoke& _BpYoke)
{
	fill<uint32_t>(_BpYoke.VisitCount, 0);

	// iterate over the internal nodes and compute their range and child indices
	parallel_for_each(_BpYoke.InternalExtent, [_RbYoke, _ColYoke, _BpYoke](index<1> Idx) HGPU
	{
		int32_t iFirst = 0;
		int32_t iLast = 0;

		// find intervall belonging to the current internal node
		determine_range(_ColYoke.MortonCodes, _RbYoke.uActiveGameObjectsCount, Idx, iFirst, iLast);

		// find split position in the intervall
		int32_t iSplit = find_split(_ColYoke.MortonCodes, iFirst, iLast);

		InternalNode Node;
		Node.ChildType = InternalNode::Flag::BothInternal;
		Node.iParentIndex = -1; // will be set later
		Node.uLeftIndex = iSplit;
		Node.uRightIndex = iSplit + 1;
		if (iSplit == iFirst) // left is leaf
		{
			Node.ChildType = InternalNode::Flag::LeftLeaf;
		}
		if ((iSplit + 1) == iLast) // right is leaf
		{
			Node.ChildType = (InternalNode::Flag) (Node.ChildType | InternalNode::Flag::RightLeaf);
		}
		_BpYoke.InternalNodes[Idx] = Node;
	});

	// Set the parent index
	parallel_for_each(_BpYoke.InternalExtent, [_RbYoke, _ColYoke, _BpYoke](index<1> Idx) HGPU
	{
		InternalNode Node = _BpYoke.InternalNodes[Idx];
		if ((Node.ChildType & InternalNode::Flag::LeftLeaf) == 0) // left is internal
		{
			_BpYoke.InternalNodes[Node.uLeftIndex].iParentIndex = Idx[0];
		}
		else
		{
			_BpYoke.LeafNodes[Node.uLeftIndex].iParentIndex = Idx[0];
		}
		if ((Node.ChildType & InternalNode::Flag::RightLeaf) == 0) // right is internal
		{
			_BpYoke.InternalNodes[Node.uRightIndex].iParentIndex = Idx[0];
		}
		else
		{
			_BpYoke.LeafNodes[Node.uRightIndex].iParentIndex = Idx[0];
		}
	});

	// generate AABBs from OBBs for the leaves
	parallel_for_each(_BpYoke.LeafExtent, [_RbYoke, _ColYoke, _BpYoke](index<1> Idx) HGPU
	{
		LeafNode Node = _BpYoke.LeafNodes[Idx];
		Node.BoundingBox = AABB(_ColYoke.OrientedBoundingBoxes[Idx]);
		Node.uFlag = _RbYoke.uFlags[Idx];
		_BpYoke.LeafNodes[Idx] = Node;
	});

	// internal bounding volume calculation
	parallel_for_each(_BpYoke.LeafExtent, [_RbYoke, _ColYoke, _BpYoke](index<1> Idx) HGPU
	{
		LeafNode Leaf = guarded_read(_BpYoke.LeafNodes, Idx);
		int32_t uDestIndex = Leaf.iParentIndex;
		// bottom up reduction by walking from leaf node to root
		while (uDestIndex >= 0 && atomic_fetch_inc(&_BpYoke.VisitCount[uDestIndex])) // parent index at root is -1
		{
			// load
			InternalNode Node = _BpYoke.InternalNodes[uDestIndex];
			InternalNodeProp LeftProp = _BpYoke.InternalNodeProps[Node.uLeftIndex];
			InternalNodeProp RightProp = _BpYoke.InternalNodeProps[Node.uRightIndex];
			LeafNode LeftLeaf = _BpYoke.LeafNodes[Node.uLeftIndex];
			LeafNode RightLeaf = _BpYoke.LeafNodes[Node.uRightIndex];
			const AABB& LeftAABB = (Node.ChildType & InternalNode::Flag::LeftLeaf) ? LeftLeaf.BoundingBox : LeftProp.BoundingBox;
			const AABB& RightAABB = (Node.ChildType & InternalNode::Flag::RightLeaf) ? RightLeaf.BoundingBox : RightProp.BoundingBox;

			// Compute properties of node's bounding volume
			InternalNodeProp NodeProp = {};
			NodeProp.BoundingBox = AABB(LeftAABB, RightAABB);

			// save
			guarded_write(_BpYoke.InternalNodeProps, index<1>(uDestIndex), NodeProp);

			// Get next parent index, root has parent index -1 -> stop
			uDestIndex = Node.iParentIndex;
		}
	});
}
//----------------------------------------------------------------------------------------------------------------------------------------
template<uint32_t uMaxCollisions = 32, uint32_t uTileSize = 256>
void LinearBVH::TraverseLBVH(RigidbodyYoke& _RbYoke, CollisionYoke& _ColYoke, BroadPhaseYoke& _BpYoke)
{
	fill<uint32_t>(_BpYoke.CollisionCount, 0);
	parallel_for_each(_BpYoke.LeafExtent, [_RbYoke, _ColYoke, _BpYoke](index<1> Idx) HGPU
	{
		LeafNode CollisionNode = _BpYoke.LeafNodes[Idx];
		AABB& BoundingBox = CollisionNode.BoundingBox;
		//_BpYoke.CollisionCount[Idx[0]] = 0;
		uint32_t uNumCollisions = 0;
		uint32_t uCollisionField[uMaxCollisions];
		stack<uint32_t, 64> Stack;
		Stack.push(0u); // push root
		
		// Traverse radix tree recursively and find all candidates for collision
		while (Stack.is_empty() == false)
		{
			// TODO: profile references!
			uint32_t uIndex = Stack.pop();
			InternalNode Node = _BpYoke.InternalNodes[uIndex];
			InternalNodeProp LeftInternalProp = _BpYoke.InternalNodeProps[Node.uLeftIndex];
			InternalNodeProp RightInternalProp = _BpYoke.InternalNodeProps[Node.uRightIndex];
			LeafNode LeftLeaf = _BpYoke.LeafNodes[Node.uLeftIndex];
			LeafNode RightLeaf = _BpYoke.LeafNodes[Node.uRightIndex];
			bool bLeftLeaf = Node.ChildType & InternalNode::Flag::LeftLeaf;
			bool bRightLeaf = Node.ChildType & InternalNode::Flag::RightLeaf;

			AABB& LeftAABB = (bLeftLeaf) ? LeftLeaf.BoundingBox : LeftInternalProp.BoundingBox;
			AABB& RightAABB = (bRightLeaf) ? RightLeaf.BoundingBox : RightInternalProp.BoundingBox;

			// Intersection tests
			bool bIntersectLeft = BoundingBox.Intersects(LeftAABB);
			bool bIntersectRight = BoundingBox.Intersects(RightAABB);

			bool bAcceptCollisionLeft = Node.uLeftIndex > Idx[0] && bIntersectLeft && bLeftLeaf;
			bool bAcceptCollisionRight = Node.uRightIndex > Idx[0] && bIntersectRight && bRightLeaf;

			// Store collision info if child is leaf
			if (bAcceptCollisionLeft)
			{
				uCollisionField[uNumCollisions++] = Node.uLeftIndex;
			}
			if (bAcceptCollisionRight)
			{
				uCollisionField[uNumCollisions++] = Node.uRightIndex;
			}

			// Traverse and build up stack
			if (bLeftLeaf == false && bIntersectLeft)
			{
				Stack.push(Node.uLeftIndex);
			}
			if (bRightLeaf == false && bIntersectRight)
			{
				Stack.push(Node.uRightIndex);
			}
		}

		_BpYoke.CollisionCount[Idx] = uNumCollisions;
		for (int i = 0; i < uNumCollisions; ++i)
		{
			guarded_write(_BpYoke.CollisionField, index<2>(Idx[0], i), uCollisionField[i]);
		}
	});

	// get total number of collisions and a index for each collision
	_ColYoke.uNumCollisions = 0;
	copy(_BpYoke.CollisionCount, _BpYoke.CollisionIndex);
	parallel_scan<uTileSize>(_BpYoke.CollisionIndex, _ColYoke.uNumCollisions, concurrency::add<uint32_t>());

	if (_ColYoke.uNumCollisions > 0)
	{
		_ColYoke.CollisionPairs = array<uint2>(_ColYoke.uNumCollisions, m_AcceleratorView);

		// write the collision pair at the index obtained by scanning the collision count
		parallel_for_each(_BpYoke.LeafExtent, [_RbYoke, _ColYoke, _BpYoke](index<1> Idx) restrict(amp)
		{
			uint32_t uNumCollisions = _BpYoke.CollisionCount[Idx];
			for (uint32_t i = 0; i < uNumCollisions; ++i)
			{
				uint32_t uCollisionIdx = _BpYoke.CollisionIndex[Idx] + i;
				uint32_t uOtherGO = _BpYoke.CollisionField[Idx[0]][i];
				_ColYoke.CollisionPairs[uCollisionIdx] = uint2(Idx[0], uOtherGO);
			}
		});
	}
}