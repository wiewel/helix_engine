//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher, David Labode
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "ResolutionPhase.h"
#include "AMP\AMPDefines.h"
using namespace Helix::Collision;
using namespace Helix::Datastructures;
using namespace Helix::AMP;
//--------------------------------------------------------------------------------------------------------------------------
ResolutionPhase::ResolutionPhase()
{
}
//--------------------------------------------------------------------------------------------------------------------------
void ResolutionPhase::Resolve(CollisionYoke & _ColYoke, RigidbodyYoke & _RbYoke)
{
	if (_ColYoke.uNumCollisions > 0)
	{
		extent<1> Extent(_ColYoke.uNumCollisions);
		parallel_for_each(Extent, [_ColYoke, _RbYoke](index<1> Idx) restrict(amp)
		{
			uint2 Pair = _ColYoke.CollisionPairs[Idx];
			_RbYoke.vLinearVelocity[Pair.x] = HMFLOAT3_ZERO;
			_RbYoke.vLinearVelocity[Pair.y] = HMFLOAT3_ZERO;
			_RbYoke.vAngularVelocity[Pair.x] = HMFLOAT3_ZERO;
			_RbYoke.vAngularVelocity[Pair.y] = HMFLOAT3_ZERO;
		});
	}
}
