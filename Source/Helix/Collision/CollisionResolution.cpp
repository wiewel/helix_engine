//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "CollisionResolution.h"
#include "Util\HelixDefines.h"

using namespace Helix::Collision;
using namespace Helix::Datastructures;
using namespace Helix::Math;

//---------------------------------------------------------------------------------------------------------------------------------------------------------------------
///<summary>
///<para>Move the colliding objects apart.</para>
///<para>If they are rigidbodies, a impulse will also be applied on them.</para>
///</summary>
///<param name="_ObjA">The colliding object</param>
///<param name="_ObjB">The collidee</param>
///<param name="_CollisionInfo">The information obtained from the intersection test.</param>
void CollisionResolution::ResolveCollision(CollisionInfo& _CollisionInfo)
{
	// apply the impulse to the game objects
	if ((_CollisionInfo.m_pGO1->GetType() == ObjectType::ObjectType_RigidBody) && (_CollisionInfo.m_pGO2->GetType() == ObjectType::ObjectType_RigidBody))
	{
		// move objects apart
		ResolveInterpenetration(_CollisionInfo);
		// compute collision points and find their middle
		FindCollisionPoints(_CollisionInfo);
		// apply impulse and friction
		CollisionResponse(_CollisionInfo);
	}
	else
	{
		// TODO: dispatch event only for any other collision
		HERRORD2("Collision response for non-rigidbody gameobjects is not yet supported.");
	}
}

#pragma region Interpenetration
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CollisionResolution::ResolveInterpenetration(CollisionInfo& _CollisionInfo)
{
	// resolve interpenetration by displacing 
	XMVECTOR vDisplacement = XMVectorScale(XMLoadFloat3(&_CollisionInfo.m_vNormal), _CollisionInfo.m_fPenetrationDepth);

	if (_CollisionInfo.m_pGO1->CheckFlag(TplGameObjectFlag_Static))
	{
		XMVECTOR vPos = XMVectorSubtract(XMLoadFloat3(&_CollisionInfo.m_pGO2->m_vPosition), vDisplacement);
		XMStoreFloat3(&_CollisionInfo.m_pGO2->m_vPosition, vPos);
	}
	else if (_CollisionInfo.m_pGO2->CheckFlag(TplGameObjectFlag_Static))
	{
		XMVECTOR vPos = XMVectorAdd(XMLoadFloat3(&_CollisionInfo.m_pGO1->m_vPosition), vDisplacement);
		XMStoreFloat3(&_CollisionInfo.m_pGO1->m_vPosition, vPos);
	}
	else
	{
		XMVECTOR vPos1 = XMLoadFloat3(&_CollisionInfo.m_pGO1->m_vPosition);
		XMVECTOR vPos2 = XMLoadFloat3(&_CollisionInfo.m_pGO2->m_vPosition);

		vPos1 = XMVectorAdd(vPos1, XMVectorScale(vDisplacement, 0.5f));
		vPos2 = XMVectorSubtract(vPos2, XMVectorScale(vDisplacement, 0.5f));

		XMStoreFloat3(&_CollisionInfo.m_pGO1->m_vPosition, vPos1);
		XMStoreFloat3(&_CollisionInfo.m_pGO2->m_vPosition, vPos2);
	}

	_CollisionInfo.m_pGO1->UpdateBoundingVolumes();
	_CollisionInfo.m_pGO2->UpdateBoundingVolumes();

}

#pragma endregion

#pragma region Collision Points
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Call this function AFTER separating OBBs to get collision points
void CollisionResolution::FindCollisionPoints(CollisionInfo& _CollisionInfo)
{
	// http://www.gamedev.net/topic/599771-obb-obb-collision-data/
	// http://de.wikipedia.org/wiki/Algorithmus_von_Sutherland-Hodgman
	//
	//	  # is the collision point.
	//	  +--------+	
	//	  |\	   |\	+-------+
	//	A |	+--------+ / \	   / \
	//	  |	|	   | |/	  \   /	  \
	//	  + |	   + #-------+	   + B
	//	   \|		\|\	  /	  \	  /
	//		+--------+ \ /	   \ /
	//					+-------+
	//   1.	select the face in A and B of which the normal is nearest to the MTV
	//			   +	
	//			   |\	+
	//			   | + / \	
	//			A  | |/	  \    	
	//			   + #	   +  B
	//				\|\   /
	//				 + \ /
	//					+
	//
	//	 2.	Clip vertices from B by performing the Sutherland-Hodgman Algorithm
	//		on the two faces. In case of a Vertex-(Face|Edge|Vertex)-Collision
	//		the result is a single point, in case of a Edge-(Face|Edge)-Collision
	//		two and in case of a Face-Face Collision there are four collision
	//		points.

	// Collision Planes:
	//	3------2
	//	|	   |
	//	|	   |
	//	0------1
	std::vector<XMFLOAT3> vFaceA; // contains all the relevant corner points from A
	std::vector<XMFLOAT3> vFaceB; // contains all the relevant corner points from B

	// Select faces nearest to the collision
	FindOBBCollisionFace(vFaceA, _CollisionInfo.m_pGO1->GetOBB(), XMFloat3Get(XMVectorNegate(XMLoadFloat3(&_CollisionInfo.m_vNormal))));
	FindOBBCollisionFace(vFaceB, _CollisionInfo.m_pGO2->GetOBB(), _CollisionInfo.m_vNormal);

	// clip vertices that lie in both faces
	std::vector<XMFLOAT3> vCollisionPoints;
	ClipFaces(vFaceA, vFaceB, vCollisionPoints);
	
	if (vCollisionPoints.empty())
	{
		// nothing was found, switch order
		ClipFaces(vFaceB, vFaceA, vCollisionPoints);
	}

	if(vCollisionPoints.empty())
	{
		// still nothing was found, fall back to the vertex closest to the collision
		float fMin = FLT_MAX;

		XMVECTOR vPoint = XMVECTOR_ZERO;
		XMVECTOR vBestPoint = XMVECTOR_ZERO;
		XMVECTOR vClippingPlane = XMPlaneFromPoints(XMLoadFloat3(&vFaceA[0]), XMLoadFloat3(&vFaceA[1]), XMLoadFloat3(&vFaceA[2]));

		size_t FaceBSize = vFaceB.size();
		for (size_t i = 0; i < FaceBSize; ++i)
		{
			vPoint = XMLoadFloat3(&vFaceB[i]);

			XMVECTOR vDistance = XMPlaneDotCoord(vClippingPlane, vPoint);
			float fDistance = fabsf(XMVectorGetX(vDistance));
			if (fDistance < fMin)
			{
				fMin = fDistance;
				vBestPoint = vPoint;
			}
		}
		vCollisionPoints.push_back(XMFloat3Get(vBestPoint));

		HERRORD2("No collision point found. Minimal distance: %f", fMin);
	}
	_CollisionInfo.m_vOverlapPoints = std::move(vCollisionPoints);

	HASSERTD(_CollisionInfo.m_vOverlapPoints.size() > 0, "Invalid collision data");

	XMVECTOR vPointOfIntersection = XMVECTOR_ZERO;
	HFOREACH_CONST(Point, End, _CollisionInfo.m_vOverlapPoints)
		vPointOfIntersection = XMVectorAdd(vPointOfIntersection, XMLoadFloat3(&(*Point)));
	HFOREACH_CONST_END

	_CollisionInfo.m_vPoint = XMFloat3Get(XMVectorScale(vPointOfIntersection, 1.0f / _CollisionInfo.m_vOverlapPoints.size()));
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CollisionResolution::FindOBBCollisionFace(_Out_ std::vector<XMFLOAT3>& _vFace, _In_ const BoundingOrientedBox& _Box, _In_ const XMFLOAT3& _CollisionNormal)
{
	// Face vertices:
	//	3------2
	//	|	   |
	//	|	   |
	//	0------1

	// loab OBB corners
	XMFLOAT3 vBoxCorners[8];
	_Box.GetCorners(vBoxCorners);
	XMVECTOR vCorners[8];
	for (int i = 0; i < _Box.CORNER_COUNT; ++i)
	{
		vCorners[i] = XMLoadFloat3(&vBoxCorners[i]);
	}

	XMVECTOR vCollisionNormal = XMLoadFloat3(&_CollisionNormal);

	XMVECTOR vFaceNormal;
	XMVECTOR vBoxFace[4];
	float fDotMax = 0.0f;
	float fNormDot = 0.0f;

	// generate normals for each box face, select the face with the normal
	// which is closest to paralel to the collision normal

	//	  3--------2	
	//	  |\	   |\
	//	  |	7--------6
	//	  |	|	   | |
	//	  0-|------1 |
	//	   \|		\|
	//		4--------5 

	// Side faces
	//--------------------------------------
	for (int i = 0; i < 4; ++i)
	{
		vFaceNormal = vCorners[(1 + i) % 4] - vCorners[0 + i];

		fNormDot = XMVectorGetX(XMVector3Dot(vFaceNormal, vCollisionNormal));
		if (fNormDot > fDotMax)
		{
			fDotMax = fNormDot;
			vBoxFace[0] = vCorners[(5 + i) % 8];
			vBoxFace[1] = vCorners[1 + i];
			vBoxFace[2] = vCorners[2 + i];
			vBoxFace[3] = vCorners[(6 + i) % 8];
		}
	}

	// Front face
	//--------------------------------------
	vFaceNormal = vCorners[4] - vCorners[0];
	fNormDot = XMVectorGetX(XMVector3Dot(vFaceNormal, vCollisionNormal));
	if (fNormDot > fDotMax)
	{
		fDotMax = fNormDot;
		vBoxFace[0] = vCorners[4];
		vBoxFace[1] = vCorners[5];
		vBoxFace[2] = vCorners[6];
		vBoxFace[3] = vCorners[7];
	}

	// Back face
	//--------------------------------------
	vFaceNormal = vCorners[0] - vCorners[4];
	fNormDot = XMVectorGetX(XMVector3Dot(vFaceNormal, vCollisionNormal));
	if (fNormDot > fDotMax)
	{
		fDotMax = fNormDot;
		vBoxFace[0] = vCorners[1];
		vBoxFace[1] = vCorners[0];
		vBoxFace[2] = vCorners[3];
		vBoxFace[3] = vCorners[2];
	}

	for (int i = 0; i < 4; ++i)
	{
		_vFace.push_back(XMFloat3Get(vBoxFace[i]));
	}
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CollisionResolution::ClipFaces(_In_ std::vector<XMFLOAT3>& _vClippingFace, _In_ std::vector<XMFLOAT3>& _vSubjectFace, _Out_ std::vector<XMFLOAT3>& _vClippedFace)
{
	const float fTolerance = 0.05f; // the tolerance of the inside tests
	const XMVECTOR vTolerance = XMVectorSet(fTolerance, fTolerance, fTolerance, fTolerance);

	//--------------------------
	// Load all needed vertices
	std::vector<XMVECTOR> vClippingPoints;
	for (int i = 0; i < 4; ++i)
	{
		vClippingPoints.push_back(XMLoadFloat3(&_vClippingFace[i]));
	}

	//---------------------------
	// construct a clipping plane
	XMVECTOR vClippingPlane = XMPlaneFromPoints(vClippingPoints[0], vClippingPoints[1], vClippingPoints[2]);

	//----------------------------------------------------------------------------
	// select vertices that lie near clipping plane. (not necessarily inside face)
	std::vector<XMVECTOR> vSubjectPoints;
	for (int i = 0; i < _vSubjectFace.size(); ++i)
	{
		const XMVECTOR vPoint = XMLoadFloat3(&_vSubjectFace[i]);

		// |DistanceToPlane| < Tolerance
		XMVECTOR vDistance = XMPlaneDotCoord(vClippingPlane, vPoint);
		if (XMVector3Less(XMVectorAbs(vDistance), vTolerance))
		{
			vSubjectPoints.push_back(vPoint);
		}
	}

	//-------------------
	// Sutherland-Hodgman
	//----------------------------------------------------------------------------
	//	   For each line in Plane A test if there is an intersection with plane B
	//	   Plane A is the Subject Plane and plane B the clipping plane
	//----------------------------------------------------------------------------	

	// initialize loop variables

	size_t iEnd = 0;
	const size_t ClippingPointsSize = 4;
	std::vector<XMVECTOR> OutputList(vSubjectPoints);

	// iterate over all edges of the clipping plane
	for (size_t iEdge = 0; iEdge < ClippingPointsSize; ++iEdge)
	{

		std::vector<XMVECTOR> InputList(OutputList);
		OutputList.clear();
		size_t InputSize = InputList.size();

		// create clipping plane for each edge by a normal pointing outwards
		vClippingPlane = XMPlaneFromPointNormal(
			// the first point of the edge
			vClippingPoints[(iEdge + 1) % ClippingPointsSize],
			// the normal pointing outwards
			XMVector3Normalize(XMVectorSubtract(vClippingPoints[(iEdge + 1) % ClippingPointsSize], vClippingPoints[iEdge]))
			);

		// iterate over all edges of the subject plane
		for (size_t iStart = 0; iStart < InputSize; ++iStart)
		{
			// Each subject edge consists of vertices START and END
			// 1.) START = 0, END = 1
			// 2.) START = 1, END = 2
			// ...
			iEnd = (iStart + 1) % InputSize;

			// if END inside of the clip region
			XMVECTOR vDistance = XMPlaneDotCoord(vClippingPlane, InputList[iEnd]);
			if (XMVector3Less(vDistance, vTolerance))
			{
				// END is inside of the clip region, so add it to the output list
				OutputList.push_back(InputList[iEnd]);

				// if START outside of the clip region
				vDistance = XMPlaneDotCoord(vClippingPlane, InputList[iStart]);
				if (XMVector3Greater(vDistance, vTolerance))
				{
					// also add intersction point between the subject edge and the clip plane to the output list
					OutputList.push_back(XMPlaneIntersectLine(vClippingPlane, InputList[iStart], InputList[iEnd]));
				}
			}
			// if END outside and START inside of the clip region
			else if (XMVector3Less(vDistance = XMPlaneDotCoord(vClippingPlane, InputList[iStart]), vTolerance))
			{
				// only add intersction point between the subject edge and the clip plane to the output list
				OutputList.push_back(XMPlaneIntersectLine(vClippingPlane, InputList[iStart], InputList[iEnd]));
			}
		} // end: iterate over all edges of the subject plane
	} // end: iterate over all edges of the clipping plane
	HFOREACH_CONST(Point, End, OutputList)
		_vClippedFace.push_back(XMFloat3Get(*Point));
	HFOREACH_CONST_END
}

#pragma endregion

#pragma region Impulse
//------------------------------------------------------------------------------------------------------------------------------------------------------------------
void CollisionResolution::CollisionResponse(CollisionInfo & _CollisionInfo)
{
	HASSERTD((_CollisionInfo.m_pGO1->GetType() == ObjectType::ObjectType_RigidBody) && (_CollisionInfo.m_pGO2->GetType() == ObjectType::ObjectType_RigidBody), "Both gameobjects must be rigidbodies");

	// Load collision info
	//===========================================================================================
	const XMVECTOR vCollisionPoint = XMLoadFloat3(&_CollisionInfo.m_vPoint);
	const XMVECTOR vCollisionNormal = XMLoadFloat3(&_CollisionInfo.m_vNormal);
	
	CollisionObject A = CollisionObject(_CollisionInfo.m_pGO1, _CollisionInfo);
	CollisionObject B = CollisionObject(_CollisionInfo.m_pGO2, _CollisionInfo);
	const XMVECTOR vRelativeVelocity = XMVectorSubtract(A.vCollisionPointVelocity, B.vCollisionPointVelocity);
	//const XMVECTOR vDeltaRelativeVelocity = XMVectorAdd(A.vDeltaCollisionPointVelocity, B.vDeltaCollisionPointVelocity);
	const float fRestitusionCoeff = (A.fElasticity + B.fElasticity) * 0.5f; // 0 = inelastic; 1 = perfectly elastic
	const float fCombinedFriction = (A.fFriction + B.fFriction) * 0.5f;
	
	// Check if already separating
	//-------------------------------------------------------------------------------------------
	if (XMVector3Greater(XMVector3Dot(vRelativeVelocity, vCollisionNormal), XMVECTOR_ZERO))
	{
		return;
	}

	// Compute Impulse
	//===========================================================================================
	
	// Impulse equation
	//-------------------------------------------------------------------------------------------
	//	J:		Impulse
	//	V_rel:	relative velocity
	//	c:		coefficient of restitution
	//	N:		collision normal
	//	M_a:	Mass of A
	//	M_b:	Mass of B
	//	InvI_a:	Inverse inertia tensor of A
	//	InvI_b:	Inverse inertia tensor of B
	//	R_a:	Distance vector to collision point from center of mass of A
	//	R_b:	Distance vector to collision point from center of mass of B
	//
	//		 ( -(1 + c) * (V_rel � N) )  
	//	J =	�������������������������������������������������������������������������������������
	//		( 1/M_a + 1/M_b + [((InvI_a * (R_a x N)) x R_a) + ((InvI_b * (R_b x N)) x R_b)] � N )
	//
	//		 Dividend
	//	J = ����������
	//		 Divisor
	//-------------------------------------------------------------------------------------------

	// Dividend
	//-------------------------------------------------------------------------------------------
	//	-(1 + c) * (V_rel � N)
	//------------------------------------------------------------------------------------------- 
	float fCollisionVelocity = XMVectorGetX(XMVector3Dot(vRelativeVelocity, vCollisionNormal));
	// remove acceleration from last frame (gravity etc.)
	float fVelocityFromAcceleration = XMVectorGetX(XMVector3Dot(A.vDeltaCollisionPointVelocity, vCollisionNormal));
	fVelocityFromAcceleration -= XMVectorGetX(XMVector3Dot(B.vDeltaCollisionPointVelocity, vCollisionNormal));
	float fDividend = -fCollisionVelocity - fRestitusionCoeff * (fCollisionVelocity - fVelocityFromAcceleration);
	//A.fDividend = -(fCollisionVelocity - XMVectorGetX(XMVector3Dot(A.vDeltaCollisionPointVelocity, vCollisionNormal))) - fRestitusionCoeff * (fCollisionVelocity - XMVectorGetX(XMVector3Dot(A.vDeltaCollisionPointVelocity,vCollisionNormal)));
	//B.fDividend = -(fCollisionVelocity - XMVectorGetX(XMVector3Dot(B .vDeltaCollisionPointVelocity, vCollisionNormal))) - fRestitusionCoeff * (fCollisionVelocity - XMVectorGetX(XMVector3Dot(B.vDeltaCollisionPointVelocity, vCollisionNormal)));

	// Divisor
	//-------------------------------------------------------------------------------------------
	// 1:	( 1/M_a + 1/M_b + 
	// 2:						[((InvI_a * (R_a x N)) x R_a) 
	// 3:						+ ((InvI_b * (R_b x N)) x R_b)] 
	// 4:														� N )
	//-------------------------------------------------------------------------------------------
	XMVECTOR vDivisor;
	float fDivisor;
	// 1:
	fDivisor = A.fInvMass + B.fInvMass;
	// 2:
	XMVECTOR vTmpDivisor = XMVector3Cross(A.vRelativeCollisionPoint, vCollisionNormal);
	vTmpDivisor = XMVector3Transform(vTmpDivisor, A.mInverseInertiaTensor);
	vTmpDivisor = XMVector3Cross(vTmpDivisor, A.vRelativeCollisionPoint);
	vDivisor = XMVECTOR(vTmpDivisor);
	// 3:
	vTmpDivisor = XMVector3Cross(B.vRelativeCollisionPoint, vCollisionNormal);
	vTmpDivisor = XMVector3Transform(vTmpDivisor, B.mInverseInertiaTensor);
	vTmpDivisor = XMVector3Cross(vTmpDivisor, B.vRelativeCollisionPoint);
	vDivisor = XMVectorAdd(vDivisor, vTmpDivisor);
	// 4:
  	vDivisor = XMVector3Dot(vDivisor, vCollisionNormal);
	fDivisor += XMVectorGetX(vDivisor);

	// Impulse
	//-------------------------------------------------------------------------------------------
	//		 Dividend
	//	J = ����������
	//		 Divisor
	//-------------------------------------------------------------------------------------------
	float fImpulse = fDividend / fDivisor;
	//A.fImpulse =  A.fDividend / fDivisor;
	//B.fImpulse = B.fDividend / fDivisor;

	//if (fImpulse <= 0.0f)
	//{
	//	return;
	//}

	// Apply Impulse
	//===========================================================================================
	// = Impulse * Normal
	XMVECTOR vNormalImpulse = XMVectorScale(vCollisionNormal, fImpulse); 
	// = (N x V_rel) x N
	XMVECTOR vCollisionTangent = XMVector3Normalize(XMVector3Cross(XMVector3Cross(vCollisionNormal, vRelativeVelocity), vCollisionNormal));
	// = u * J * t
	float fPlanarImpulse = XMVectorGetX(XMVectorSqrtEst(XMVector3Dot(vCollisionTangent, vRelativeVelocity)));
	XMVECTOR vTangentImpulse = XMVectorScale(vCollisionTangent, fImpulse * fCombinedFriction);
	XMVECTOR vVelocityChange;
	if (fPlanarImpulse > fImpulse * fCombinedFriction)
	{
		vVelocityChange = XMVectorAdd(vNormalImpulse, vTangentImpulse);
	}
	else
	{
		vVelocityChange = vNormalImpulse;
	}
	if (A.pGameObject->TryWake(fImpulse))
	{
		A.ApplyImpulse(vVelocityChange);
	}

	// = Impulse * Normal
	vNormalImpulse = XMVectorScale(vCollisionNormal, -fImpulse);
	// = (N x V_rel) x N
	vCollisionTangent = XMVector3Normalize(XMVector3Cross(XMVector3Cross(vCollisionNormal, vRelativeVelocity), vCollisionNormal));
	// = u * J * t
	fPlanarImpulse = XMVectorGetX(XMVectorSqrtEst(XMVector3Dot(vCollisionTangent, vRelativeVelocity)));
	vTangentImpulse = XMVectorScale(vCollisionTangent, fImpulse * fCombinedFriction);
	vVelocityChange;
	if (fPlanarImpulse > fImpulse * fCombinedFriction)
	{
		vVelocityChange = XMVectorAdd(vNormalImpulse, vTangentImpulse);
	}
	else
	{
		vVelocityChange = vNormalImpulse;
	}
	if (B.pGameObject->TryWake(fImpulse))
	{
		B.ApplyImpulse(vVelocityChange);
	}
}
#pragma endregion

#pragma region Game Physics Lecture 
// ###########################################################################
// ##																		##
// ##	Position Correction: Transformation									##
// ##	(r = orientation quaternion; P = collision point; X = COM of RB)	##
// ##																		##
// ##	X_new = X + Normalize(-R_xp) * fPositionOffsetScale					##
// ##																		##
// ###########################################################################

// Penetration-Hack: Shift Objects away from each other, by offsetting their position in collision direction
// get normalized vector pointing from collision point to own center of mass...
// R_px = Normalize(-R_xp)
//XMVECTOR R_px = XMVector3Normalize(XMVectorNegate(R_xp));

// and scale the resulting vector to receive an infinitesimal small direction vector
// R_px = Normalize(-R_xp) * fPositionOffsetScale	
//static const float fPositionOffsetScale = 0.002f;
//R_px = XMVectorScale(R_px, fPositionOffsetScale);

// calculate the new position by offsetting it with the calculated direction vector
// X_new = X + Normalize(-R_xp) * fPositionOffsetScale

//const XMVECTOR X_new = XMVectorAdd(X, R_px);
//XMStoreFloat3(&_RigidBody.m_vPosition, X_new);

// Lecture Alternative:

//// Lecture-Position-Hack:
//// X_new = X + Jn/M
//const XMVECTOR X_new = XMVectorAdd(X, Jn);
//XMStoreFloat3(&m_vPosition, X_new);


//// #########################################################################
//// ##																		##
//// ##	Position Correction: Orientation									##
//// ##	(r = orientation quaternion; P = collision point; X = COM of RB)	##
//// ##																		##
//// ##	r_new = r + [I_inv * (P x Jn)] / [(I_inv * (X x Jn) x X) Dot N]		##
//// ##																		##
//// #########################################################################

//const XMVECTOR r = XMLoadFloat4(&m_vOrientation);
//const XMMATRIX I_inv = XMLoadFloat3x3(&m_mInvInertiaTensor);

//// ###################################
//// ##	Dividend: Rotation Change	##
//// ##	dr = I_inv * (P x Jn)		##
//// ###################################
//
//// vDividend = (P x Jn)
//XMVECTOR vDividend = XMVector3Cross(P, Jn);
//// vDividend = I_inv * (P x Jn)
//vDividend = XMVector3Transform(vDividend, I_inv);

//if (XMVectorGetX(XMVector3LengthEst(vDividend)) <= FLT_EPSILON)
//	return;

//// ###############################################
//// ##	Divisor: angular component of inertia	##
//// ##	k = (I_inv * (X x Jn) x X) Dot N		##
//// ###############################################

//// vDivisor = (X x Jn)
//XMVECTOR vDivisor = XMVector3Cross(X, Jn);
//// vDivisor = vDivisor x X
//vDivisor = XMVector3Cross(vDivisor, X);
//// vDivisor = I_inv * (X x Jn)
//vDivisor = XMVector3Transform(vDivisor, I_inv);
//// vDivisor = vDivisor Dot N
//vDivisor = XMVector3Dot(vDivisor, N);

//float fDivisor = XMVectorGetX(vDivisor);

//if (fDivisor <= FLT_EPSILON)
//	return;

//// ###################################################
//// ##	Orientation Update:							##
//// ##	r_new = r + vDividend ScaleWith 1/fDivisor	##
//// ###################################################

//XMVECTOR vTmp = XMVectorScale(vDividend, 1.f/fDivisor);
//XMVECTOR r_new = XMVectorAdd(r, vTmp);

//// normalize rotation quaternion
//r_new = XMVector4Normalize(r_new);
//// store rotation quaternion
//XMStoreFloat4(&m_vOrientation, r_new);
//// convert quaternion r to rotation matrix
//XMMATRIX R = XMMatrixRotationQuaternion(r_new);
//XMStoreFloat3x3(&m_mRotation, R);
#pragma endregion