//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher, David Labode
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "CPUNarrowSelector.h"
#include "DataStructures\GameObjectPool.h"
#include "CollisionSystem.h"
#include "DataStructures\GameObject.h"
#include <concurrent_vector.h>

using namespace Helix::Collision;
using namespace Helix::AMP;
using namespace Helix::Datastructures;
CPUNarrowSelector::CPUNarrowSelector()
{
}

CPUNarrowSelector::~CPUNarrowSelector()
{
}

void CPUNarrowSelector::Sieve(RigidbodyYoke & _RbYoke, CollisionYoke & _ColYoke, std::vector<CollisionInformation>& _Infos)
{
	_Infos.clear();
	if (_ColYoke.uNumCollisions == 0)
	{
		return;
	}

	std::vector<uint2> CollPairs(_ColYoke.uNumCollisions);
	std::vector<OBB> BoundingBoxes(_ColYoke.OrientedBoundingBoxes.extent.size());
	std::vector<GameObjectFlag> Flags(_RbYoke.Extent.size());
	concurrency::concurrent_vector<CollisionInformation> CollisionInfo;
	copy(_ColYoke.CollisionPairs, CollPairs.begin());
	copy(_ColYoke.OrientedBoundingBoxes, BoundingBoxes.begin());
	copy(_RbYoke.uFlags, Flags.begin());

	parallel_for_each( CollPairs.begin(), CollPairs.end(), [&Flags, &BoundingBoxes, &CollisionInfo] (uint2& Pair)
	{
		const GameObjectFlag& FlagA = Flags[Pair.x];
		const GameObjectFlag& FlagB = Flags[Pair.y];
		bool bBothStatic = CheckFlag(FlagA & FlagB, GameObjectFlag::Static);
		bool bNoCollision = CheckFlag(FlagA | FlagB, GameObjectFlag::NoCollision);
		bool bDestroyed = CheckFlag(FlagA | FlagB, GameObjectFlag::Destroyed);
		CollisionInformation Info;
		if (bBothStatic == false && bNoCollision == false && bDestroyed == false)
		{
			const OBB& BoxA = BoundingBoxes[Pair.x];
			const OBB& BoxB = BoundingBoxes[Pair.y];
			if (BoxA.Intersects(BoxB, Info))
			{
				AABB A = AABB(BoxA);
				AABB B = AABB(BoxB);
				Info.m_IndexPair = Pair;
				/*HLOG("Collision between \n\t[Index: %u | Flag: %u | AABB-Pos: (%f, %f, %f) AABB-HExt: (%f, %f, %f)] \n\t[Index: %u | Flag: %u | AABB-Pos: (%f, %f, %f) AABB-HExt: (%f, %f, %f)]",
					Info.m_IndexPair.x, FlagA, A.m_vCenter.x, A.m_vCenter.y, A.m_vCenter.z, A.m_vHalfExtents.x, A.m_vHalfExtents.y, A.m_vHalfExtents.z,
					Info.m_IndexPair.y, FlagB, B.m_vCenter.x, B.m_vCenter.y, B.m_vCenter.z, B.m_vHalfExtents.x, B.m_vHalfExtents.y, B.m_vHalfExtents.z);*/
				CollisionInfo.push_back(Info);
			}
		}
	});
	_Infos = std::vector<CollisionInformation>(CollisionInfo.begin(), CollisionInfo.end());
}

