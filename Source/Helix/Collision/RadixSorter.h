//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher, David Labode
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

/////////////////////////////////////////////////////////////////////////////////////////////////
// Game Object Sorter
//-----------------------------------------------------------------------------------------------
// Summary:
// Sorts Game Objects for usage in the broad phase collision
/////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef RADIXSORTER_H
#define RADIXSORTER_H

#include "AMP\AMPDefines.h"
#include "DataStructures\GameObjectPool.h"
#include "Util\Logger.h"
namespace Helix
{

	namespace Collision
	{
		struct SorterYoke;
		struct CollisionYoke;

		class RadixSorter
		{
			friend struct SorterYoke;
			HDEBUGNAME("RadixSorter")
		private:
			concurrency::accelerator_view m_AcceleratorView;
			concurrency::extent<1> m_MaxExtent;
			uint32_t m_uPrevActiveGameObjects;
			concurrency::array<uint32_t> m_ReorderIndices;		// num game objects
			// radix sort specific:
			concurrency::array<uint32_t> m_OrderChecks;// num game objects
			concurrency::array<uint32_t> m_IndexOffset;
			concurrency::array<uint32_t> m_LocalPrefixSum;		// num game objects
			concurrency::array<uint32_t> m_LocalOffset;			// num game objects
			concurrency::array<uint32_t, 2> m_TilePrefixSum;	// 4 * NumTiles
			concurrency::array<uint32_t, 2> m_SumTmp1;	// m_TilePrefixSum / TileSize recursively decreasing
			concurrency::array<uint32_t, 2> m_SumTmp2;	// m_TilePrefixSum / TileSize recursively decreasing
			concurrency::array<uint32_t> m_Tmp;					// num game objects

		public:
			RadixSorter(uint32_t _uNumGameObjects, const concurrency::accelerator_view& _AcceleratorView)
				: m_MaxExtent(_uNumGameObjects)
				, m_uPrevActiveGameObjects(_uNumGameObjects)
				, m_ReorderIndices(m_MaxExtent, _AcceleratorView)
				, m_OrderChecks(concurrency::num_tiles(m_MaxExtent, 256), _AcceleratorView)
				, m_IndexOffset(m_MaxExtent, _AcceleratorView)
				, m_LocalPrefixSum(m_MaxExtent, _AcceleratorView)
				, m_LocalOffset(m_MaxExtent, _AcceleratorView)
				, m_TilePrefixSum(concurrency::extent<2>(4, concurrency::num_tiles(m_MaxExtent, 256)), _AcceleratorView)
				, m_SumTmp1(concurrency::extent<2>(4, concurrency::num_tiles(m_TilePrefixSum.extent, 256)), _AcceleratorView)
				, m_SumTmp2(concurrency::extent<2>(4, concurrency::num_tiles(m_TilePrefixSum.extent, 256)), _AcceleratorView)
				, m_Tmp(m_MaxExtent, _AcceleratorView)
				, m_AcceleratorView(_AcceleratorView)
			{}

			void Sort(Datastructures::RigidbodyYoke& _RbYoke, CollisionYoke& _ColYoke);

		private:
			template<uint32_t uTileSize>
			void ParallelRadixSort(SorterYoke& _SortYoke, CollisionYoke& _ColYoke) HCPU;
			template<uint32_t uTileSize>
			bool OrderChecking(SorterYoke& _SortYoke, CollisionYoke& _ColYoke) HCPU;
			template<uint32_t uTileSize>
			void TileSorting(const uint32_t _uBitOffset, SorterYoke& _SortYoke, CollisionYoke& _ColYoke) HCPU;
			template<uint32_t uTileSize>
			void GlobalSorting(const uint32_t _uBitOffset, SorterYoke& _SortYoke, CollisionYoke& _ColYoke) HCPU;
			
			void ComputeMortonCodes(SorterYoke& _SortYoke, CollisionYoke& _ColYoke, Datastructures::RigidbodyYoke& _RbYoke);
			void ReorderIndices(SorterYoke& _SortYoke, Datastructures::RigidbodyYoke& _RbYoke);

			template<typename T>
			void Reorder(Concurrency::array<T>& _Data, Concurrency::array<uint32_t>& _Indices, concurrency::extent<1> _Extent);
		};

		struct SorterYoke
		{
			SorterYoke(uint32_t uNumActiveGameObjects, RadixSorter* _Sorter)
				: ActiveGOExtent(_Sorter->m_MaxExtent)
				, uNumTiles(concurrency::num_tiles(ActiveGOExtent, 256))
				, TilesExtent(4, uNumTiles)
				, ReorderIndices(_Sorter->m_ReorderIndices)
				, OrderChecks(_Sorter->m_OrderChecks)
				, IndexOffset(_Sorter->m_IndexOffset)
				, LocalPrefixSum(_Sorter->m_LocalPrefixSum)
				, LocalOffset(_Sorter->m_LocalOffset)
				, TilePrefixSum(_Sorter->m_TilePrefixSum)
				, SumTmp1(_Sorter->m_SumTmp1)
				, SumTmp2(_Sorter->m_SumTmp2)
				, Tmp(_Sorter->m_Tmp)
			{}
			concurrency::extent<1> ActiveGOExtent;
			uint32_t uNumTiles = 0;
			concurrency::extent<2> TilesExtent;
			concurrency::array<uint32_t>& ReorderIndices;
			concurrency::array<uint32_t>& OrderChecks;
			concurrency::array<uint32_t>& IndexOffset;
			concurrency::array<uint32_t>& LocalPrefixSum;
			concurrency::array<uint32_t>& LocalOffset;
			concurrency::array<uint32_t, 2>& TilePrefixSum;
			concurrency::array<uint32_t, 2>& SumTmp1;
			concurrency::array<uint32_t, 2>& SumTmp2;

			concurrency::array<uint32_t>& Tmp;
		};
	}
}

#endif // !GAMEOBJECTSORTER_H
