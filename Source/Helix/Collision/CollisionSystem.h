//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher, David Labode
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.


///////////////////////////////////////////////////////////////////////////////////////////////////
//// Collision System
////-----------------------------------------------------------------------------------------------
//// Summary:
////	Broad and narrow phase collision detection and resolution
////-----------------------------------------------------------------------------------------------
//// Yoke?!
////	A yoke in the context of the physics pipeline is a unified accessor to the used data. In the
////	metapher the data gets 'carried' through the pipeline by its yoke.
////	https://upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Glaspalast_M%C3%BCnchen_1891_076b.jpg/800px-Glaspalast_M%C3%BCnchen_1891_076b.jpg
///////////////////////////////////////////////////////////////////////////////////////////////////
//
//#ifndef COLLISIONSYSTEM_H
//#define COLLISIONSYSTEM_H
//
//#include "AMP\AMPDefines.h"
//#include "AMP\AMPTypes.h"
//#include "OBB.h"
//#include "AABB.h"
//#include "CollisionInformation.h"
//#include <unordered_map>
//#include "LinearBVH.h"
//#include "RadixSorter.h"
//
//namespace Helix
//{
//	namespace Datastructures
//	{
//		struct RigidbodyYoke;
//		class GameObject;
//	}
//	namespace Collision
//	{
//		class OBB;
//		struct CollisionYoke;
//		
//		class CollisionSystem
//		{
//			friend struct CollisionYoke;
//		private:
//			AMP::array<uint32_t> m_MortonCodes;
//			AMP::array<OBB> m_OrientedBoundingBoxes;
//			AMP::array<AMP::uint2> m_CollisionPairs;
//			uint32_t m_uSize;
//			// BEGIN HACK
//			std::vector<CollisionInformation> m_CollisionInfo;
//			std::unordered_multimap<uint32_t, uint32_t> m_CollisionMap; 
//			// END HACK
//			AMP::accelerator_view m_AMPAccelerator;
//			RadixSorter m_RadixSorter;
//			LinearBVH m_LinearBVH;
//
//		public:
//			CollisionSystem(uint32_t _uNumGameObjects, const AMP::accelerator_view& _AMPAccelerator)
//				: m_MortonCodes(_uNumGameObjects, _AMPAccelerator)
//				, m_OrientedBoundingBoxes(_uNumGameObjects, _AMPAccelerator)
//				, m_CollisionPairs((_uNumGameObjects + 1) / 2, _AMPAccelerator)
//				, m_CollisionMap()
//				, m_AMPAccelerator(_AMPAccelerator)
//				, m_RadixSorter(_uNumGameObjects, _AMPAccelerator)
//				, m_LinearBVH(_uNumGameObjects, _AMPAccelerator)
//			{
//				concurrency::fill<uint32_t>(m_MortonCodes, UINT32_MAX);
//			}
//			void Resize(uint32_t _uNumGameObjects);
//			void ProcessCollisions(Datastructures::RigidbodyYoke& _RbYoke);
//			std::vector<CollisionInformation>& GetCollisionInformation();
//			bool DidCollide(Datastructures::GameObject& _GameObject) const;
//			bool DidCollideWith(Datastructures::GameObject& _GameObject, Datastructures::GameObject& _OtherGO) const;
//		};
//
//		struct CollisionYoke
//		{
//			CollisionYoke(CollisionSystem* _CollisionSystem)
//				: MortonCodes(_CollisionSystem->m_MortonCodes),
//				OrientedBoundingBoxes(_CollisionSystem->m_OrientedBoundingBoxes),
//				CollisionPairs(_CollisionSystem->m_CollisionPairs),
//				Extent(_CollisionSystem->m_MortonCodes.extent),
//				uSize(_CollisionSystem->m_MortonCodes.extent[0])
//			{}
//
//			AMP::array<uint32_t>& MortonCodes;
//			AMP::array<OBB>& OrientedBoundingBoxes;
//			AMP::array<AMP::uint2>& CollisionPairs;
//			AMP::extent<1> Extent;
//			uint32_t uSize;
//			uint32_t uNumCollisions = 0;
//		};
//	}
//}
//
//#endif
//
