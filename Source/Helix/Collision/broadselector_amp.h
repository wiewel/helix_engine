//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher, David Labode
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

/////////////////////////////////////////////////////////////////////////////////////////////////
// Broad Selector 
//-----------------------------------------------------------------------------------------------
// Summary:
// Broad phase collision candidate selection
/////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef BROADSELECTOR_AMP_H
#define BROADSELECTOR_AMP_H

#include "AMP\AMPDefines.h"
#include "AMP\amp_stack.h"
#include "AMP\AMPAlgorithms.h"

namespace Concurrency
{
	void determine_range(array<uint32_t>& _MortonCodes_GLOB, uint32_t uNumObjects, index<1>& _Idx, int32_t & _iFirst, int32_t & _iLast) restrict(amp);
	int32_t find_split(array<uint32_t>& _MortonCodes_GLOB, int32_t _iFirst, int32_t _iLast) restrict(amp);
}
//--------------------------------------------------------------------------------------------------------------------
// find the range of the internal node at the given index
inline void Concurrency::determine_range(array<uint32_t>& _MortonCodes_GLOB,
	uint32_t uNumObjects,
	index<1>& _Idx,
	int32_t & _iFirst,
	int32_t & _iLast) restrict(amp)
{
	// read morton codes. because morton codes only use 30 bits they can be stored in signed integers for convenience
	uint32_t uW = guarded_read<uint32_t>(_MortonCodes_GLOB, _Idx - 1, 0);
	uint32_t uX = guarded_read<uint32_t>(_MortonCodes_GLOB, _Idx, 0);
	uint32_t uY = guarded_read<uint32_t>(_MortonCodes_GLOB, _Idx + 1, 0);

	// Determine direction of the range (+1 or -1)
	int32_t iDirection = 0;
	if (_Idx[0] == 0)
	{
		iDirection = 1;
	}
	else if (_Idx[0] == uNumObjects - 1)
	{
		iDirection = -1;
	}
	else
	{
		iDirection = sign(common_prefix_length(uX, uY) - common_prefix_length(uX, uW));
	}

	// compute upper bound for the length of the array
	uint32_t uNextMortonCode = 0;
	int32_t iMinPrefix = -1;
	if (guarded_read<uint32_t>(_MortonCodes_GLOB, _Idx - iDirection, 0, uNextMortonCode))
	{
		iMinPrefix = common_prefix_length(uX, uNextMortonCode);
	}
	uint32_t uMaxLength = 1;
	while (reinterpret<int32_t>(common_prefix_length(uX, guarded_read<uint32_t>(_MortonCodes_GLOB, _Idx + uMaxLength * iDirection, 0))) > iMinPrefix && in_bounds(_Idx[0] + uMaxLength * iDirection, 0u, uNumObjects - 1u))
	{
		uMaxLength *= 2;
	}

	// find the other end using binary search
	uint32_t uLength = 0;
	for (uint32_t t = uMaxLength / 2; t >= 1; t /= 2)
	{
		if (reinterpret<int32_t>(common_prefix_length(uX, guarded_read<uint32_t>(_MortonCodes_GLOB, _Idx + (uLength + t) * iDirection, ~uX))) > iMinPrefix && in_bounds(_Idx[0] + (uLength + t) * iDirection, 0u, uNumObjects - 1u))
		{
			uLength += t;
		}
	}

	// Output range bounds
	_iFirst = (iDirection > 0) ? _Idx[0] : _Idx[0] - uLength;
	_iLast = (iDirection > 0) ? _Idx[0] + uLength : _Idx[0];
}
//--------------------------------------------------------------------------------------------------------------------
// find split position for radix tree construction using binary search
// _iFirst and _iLast must be in _MortonCodes_GLOB's extent
int32_t Concurrency::find_split(array<uint32_t>& _MortonCodes_GLOB, int32_t _iFirst, int32_t _iLast) restrict(amp)
{
	uint32_t uFirstCode = _MortonCodes_GLOB[_iFirst];
	uint32_t uLastCode = _MortonCodes_GLOB[_iLast];

	if (uFirstCode == uLastCode)
	{
		return (_iFirst + _iLast) / 2;
	}

	// calculate number of highest bits that are the same for all objects 
	// (those between first and last must share the same property)
	int32_t iCommonPrefix = clz(uFirstCode ^ uLastCode);

	// binary search where the next bit differs
	// specifically the highest code which still shares more than iCommonPrefix bits with uFirstCode
	int32_t iSplit = _iFirst; // first guess
	int32_t iStep = _iLast - _iFirst;
	do
	{
		iStep = (iStep + 1) / 2; // exponential decrease
		int32_t iNewSplit = iSplit + iStep; // proposed position

		if (iNewSplit < _iLast)
		{
			uint32_t uSplitCode = _MortonCodes_GLOB[iNewSplit];
			int32_t iSplitPrefix = clz(uFirstCode ^ uSplitCode);
			// check proposal
			iSplit = (iSplitPrefix > iCommonPrefix) ? iNewSplit : iSplit;
		}
	} while (iStep > 1);

	return iSplit;
}
#endif