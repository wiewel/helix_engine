//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef XMCOLLISION_H
#define XMCOLLISION_H

#include <vector>
//This Header needs to be included BEFORE any DirectX header to overwrite Math functions!
#include "Math\XMMath.h"

#pragma warning (disable: 4838 4458)
#include <DirectXCollision.h>
#pragma warning (default: 4838 4458)

#include "Util\Logger.h"
#include "Util\HelixDefines.h"
#include <DirectXCollision.inl>

namespace Helix
{
	namespace Datastructures
	{
		class TplGameObject;
	};

	namespace Collision
	{
		using namespace DirectX;

		struct CollisionInfo
		{
			//XMFLOAT3 m_vDisplacement; // with normal and penetration depth this isn't really needed
			XMFLOAT3 m_vNormal;
			XMFLOAT3 m_vPoint;
			//XMFLOAT3 m_vRelativeVelocity;
			//float m_fImpulse;
			//float m_fFriction;

			std::vector<XMFLOAT3> m_vOverlapPoints;

			float m_fPenetrationDepth;

			bool m_bIsValid = false;

			Datastructures::TplGameObject* m_pGO1; // Object 1
			Datastructures::TplGameObject* m_pGO2; // Object 2
		};
		// Intersection		
		//----------------------------------------------------------------------------------------------------------------------------------
		// OBB
		inline bool Intersects(_In_ const BoundingOrientedBox& _BoxA, _In_ const BoundingOrientedBox& _BoxB);
		inline bool Intersects(_In_ const BoundingOrientedBox& _BoxA, _In_ const BoundingOrientedBox& _BoxB, CollisionInfo& _CollisionInfo);
		inline bool Intersects(_In_ const BoundingOrientedBox& _BoxA, _In_ const BoundingBox& _BoxB);
		inline bool Intersects(_In_ const BoundingOrientedBox& box, _In_ const BoundingFrustum& frustum);
		inline bool Intersects(_In_ const BoundingOrientedBox& box, _In_ const BoundingSphere& sphere);
		// AABB
		inline bool Intersects(const BoundingBox& _BoxA, const BoundingBox& _BoxB);
		inline bool Intersects(_In_ const BoundingBox& _BoxA, _In_ const BoundingOrientedBox& _BoxB);
		inline bool Intersects(_In_ const BoundingBox& box, _In_ const BoundingSphere& sphere);
		inline bool Intersects(_In_ const BoundingBox& box, _In_ const BoundingFrustum& frustum);
		// Sphere
		inline bool Intersects(_In_ const BoundingSphere& sphere, _In_ const BoundingBox& box);
		inline bool Intersects(_In_ const BoundingSphere& sphere, _In_ const BoundingOrientedBox& box);
		// Frustum
		inline bool Intersects(_In_ const BoundingFrustum& frustum, _In_ const BoundingBox& box);
		inline bool Intersects(_In_ const BoundingFrustum& frustum, _In_ const BoundingOrientedBox& box);
		
		// Containment		
		//----------------------------------------------------------------------------------------------------------------------------------
		// OBB
		inline bool Contains(const BoundingOrientedBox& _BoxA, const BoundingOrientedBox& _BoxB);
		inline bool Contains(const BoundingOrientedBox& _BoxA, const BoundingBox& _BoxB);
		// AABB
		inline bool Contains(const BoundingBox& _BoxA, const BoundingOrientedBox& _BoxB);

		// Overlap		
		//----------------------------------------------------------------------------------------------------------------------------------
		// OBB
		inline bool Overlaps(const BoundingOrientedBox& _BoxA, const BoundingOrientedBox& _BoxB);
		inline bool Overlaps(const BoundingOrientedBox& _BoxA, const BoundingOrientedBox& _BoxB, CollisionInfo& _Info);
		// AABB
		inline bool Overlaps(const BoundingBox& _BoxA, const BoundingOrientedBox _BoxB);

		// Conversion		
		//----------------------------------------------------------------------------------------------------------------------------------
		inline void ConvertOBBToAABB(_In_ const BoundingOrientedBox& _OBB, _Out_ BoundingBox& _OutAABB);
		

		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// DEFINITIONS:
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		//=============================================================================
		// Intersection
		//-----------------------------------------------------------------------------
		// Checks:
		//  - Overlap
		//	- Containment
		//=============================================================================
#pragma region OBB intersects ...

		//-----------------------------------------------------------------------------
		// Fast oriented box / oriented box intersection test using the separating axis 
		// theorem.
		//-----------------------------------------------------------------------------
		///<summary>
		///<para>OBB-OBB intersection test.</para>
		///<para>Fast oriented box / oriented box intersection test using the separating axis theorem.</para>
		///<returns>Returns true for overlap or containment</returns>
		///</summary>
		inline bool Intersects(_In_ const BoundingOrientedBox& _BoxA, _In_ const BoundingOrientedBox& _BoxB)
		{
			// The normal SAT overlap test is sufficient
			return Overlaps(_BoxA, _BoxB);
		}

		//-----------------------------------------------------------------------------
		// Slow oriented box / oriented box intersection test using the separating axis 
		// theorem. It also calculates the collision normal and the penetration depth.
		//-----------------------------------------------------------------------------
		///<summary>
		///<para>OBB-OBB intersection test.</para>
		///<para>Slow oriented box / oriented box intersection test using the separating axis theorem.</para>
		///<para> - Tests for overlap and containment</para>
		///<para> - Fills CollisionInfo with collision normal and penetration depth</para>
		///<para> - Correct penetration depth even in containment cases</para>
		///<para> - Slowest OBB - OBB test</para>
		///<returns>Returns true for overlap or containment</returns>
		///</summary>
		inline bool Intersects(_In_ const BoundingOrientedBox& _BoxA, _In_ const BoundingOrientedBox& _BoxB, CollisionInfo& _CollisionInfo)
		{
			// TODO: Fix: In case of containment penetration depth will be incorrect!
			return Overlaps(_BoxA, _BoxB, _CollisionInfo);
		}

		//-----------------------------------------------------------------------------
		// BoundingVolume intersection pass-through functions:
		//-----------------------------------------------------------------------------
		// OBB & AABB
		_Use_decl_annotations_
		///<summary>
		///<para>OBB-ABB intersection test.</para>
		///<returns>Returns true for overlap or containment</returns>
		///</summary>
		inline bool Intersects(_In_ const BoundingOrientedBox& _BoxA, _In_ const BoundingBox& _BoxB)
		{
			return _BoxB.Intersects(_BoxA);
		}

		// OBB & Frustum
		_Use_decl_annotations_
		///<summary>
		///<para>OBB-Frustum intersection test.</para>
		///<para>Intersection test pass through function.</para>
		///<returns>Returns true for overlap or containment</returns>
		///</summary>
		inline bool Intersects(_In_ const BoundingOrientedBox& box, _In_ const BoundingFrustum& frustum)
		{
			return frustum.Intersects(box);
		}


		// OBB & Sphere
		_Use_decl_annotations_
		///<summary>
		///<para>OBB-Sphere intersection test.</para>
		///<returns>Returns true for overlap or containment</returns>
		///</summary>
		inline bool Intersects(_In_ const BoundingOrientedBox& box, _In_ const BoundingSphere& sphere)
		{
			return sphere.Intersects(box);
		}
#pragma endregion

#pragma region AABB intersects ...
		
		//-----------------------------------------------------------------------------
		// Axis-aligned box vs. axis-aligned box test
		//-----------------------------------------------------------------------------
		_Use_decl_annotations_
		///<summary>
		///<para>AABB-AABB intersection test.</para>
		///<returns>Returns true for overlap or containment</returns>
		///</summary>
		inline bool Intersects(const BoundingBox& _BoxA, const BoundingBox& _BoxB)
		{
			return _BoxA.Intersects(_BoxB);

		}

		//-----------------------------------------------------------------------------
		// BoundingVolume intersection pass-through functions:
		//-----------------------------------------------------------------------------
		// AABB & OBB
		_Use_decl_annotations_
		///<summary>
		///<para>AABB-OBB intersection test.</para>
		///<para> - Converts box (AABB) to OBB</para>
		///<returns>Returns true for overlap or containment</returns>
		///</summary>
		inline bool Intersects(_In_ const BoundingBox& _BoxA, _In_ const BoundingOrientedBox& _BoxB)
		{
			// Make the axis aligned box oriented and do an OBB vs frustum test.
			BoundingOrientedBox obox(_BoxA.Center, _BoxA.Extents, XMFLOAT4(0.f, 0.f, 0.f, 1.f));
			return Intersects(obox, _BoxB);
		}

		// AABB & Sphere
		_Use_decl_annotations_
		///<summary>
		///<para>AABB-Sphere intersection test.</para>
		///<returns>Returns true for overlap or containment</returns>
		///</summary>
		inline bool Intersects(_In_ const BoundingBox& box, _In_ const BoundingSphere& sphere)
		{
			return box.Intersects(sphere);
		}

		// AABB & Frustum 
		// DXMath does not have a function for AABB & frustum, so just convert AABB to OBB, TODO: check if there is a better way.
		_Use_decl_annotations_
		///<summary>
		///<para>AABB-Frustum intersection test.</para>
		///<para> - Converts box (AABB) to OBB</para>
		///<returns>Returns true for overlap or containment</returns>
		///</summary>
		inline bool Intersects(_In_ const BoundingBox& box, _In_ const BoundingFrustum& frustum)
		{
			// Make the axis aligned box oriented and do an OBB vs frustum test.
			BoundingOrientedBox obox(box.Center, box.Extents, XMFLOAT4(0.f, 0.f, 0.f, 1.f));
			return frustum.Intersects(obox);
		}
#pragma endregion

#pragma region Sphere intersects ...
		// Sphere & AABB
		_Use_decl_annotations_
		///<summary>
		///<para>Sphere-AABB intersection test.</para>
		///<returns>Returns true for overlap or containment</returns>
		///</summary>
		inline bool Intersects(_In_ const BoundingSphere& sphere, _In_ const BoundingBox& box)
		{
			return box.Intersects(sphere);
		}

		// Sphere & OBB
		_Use_decl_annotations_
		///<summary>
		///<para>Sphere-OBB intersection test.</para>
		///<returns>Returns true for overlap or containment</returns>
		///</summary>
		inline bool Intersects(_In_ const BoundingSphere& sphere, _In_ const BoundingOrientedBox& box)
		{
			return box.Intersects(sphere);
		}
#pragma endregion

#pragma region Frustum intersects ...
		// Frustum & AABB
		_Use_decl_annotations_
		///<summary>
		///<para>Frustum-AABB intersection test.</para>
		///<para> - Converts box (AABB) to OBB</para>
		///<returns>Returns true for overlap or containment</returns>
		///</summary>
		inline bool Intersects(_In_ const BoundingFrustum& frustum, _In_ const BoundingBox& box)
		{
			// Make the axis aligned box oriented and do an OBB vs frustum test.
			BoundingOrientedBox obox(box.Center, box.Extents, XMFLOAT4(0.f, 0.f, 0.f, 1.f));
			return frustum.Intersects(obox);
		}

		//Frustum & OBB
		_Use_decl_annotations_
		///<summary>
		///<para>Frustum-OBB intersection test.</para>
		///<returns>Returns true for overlap or containment</returns>
		///</summary>
		inline bool Intersects(_In_ const BoundingFrustum& frustum, _In_ const BoundingOrientedBox& box)
		{
			return frustum.Intersects(box);
		}
#pragma endregion

		//=============================================================================
		// Containment
		//-----------------------------------------------------------------------------
		// Test only if one BV is contained by another. An intersection test is 
		// omitted.
		//=============================================================================
#pragma region OBB contains ...
		//-----------------------------------------------------------------------------
		// Oriented bounding box in oriented bounding box (box1 contains box2)
		//-----------------------------------------------------------------------------
		_Use_decl_annotations_
		///<summary>
		///<para>A (OBB) 'contains' B (OBB)</para>
		///<returns>Returns true only if box A contains box B. Does NOT test for intersection.</returns>
		///</summary>
		inline bool Contains(const BoundingOrientedBox& _BoxA, const BoundingOrientedBox& _BoxB)
		{
			// Load the boxes
			XMVECTOR aCenter = XMLoadFloat3(&_BoxA.Center);
			XMVECTOR aExtents = XMLoadFloat3(&_BoxA.Extents);
			XMVECTOR aOrientation = XMLoadFloat4(&_BoxA.Orientation);

			HASSERTD(DirectX::Internal::XMQuaternionIsUnit(aOrientation), "Quaternion does not have unit length!");

			XMVECTOR bCenter = XMLoadFloat3(&_BoxB.Center);
			XMVECTOR bExtents = XMLoadFloat3(&_BoxB.Extents);
			XMVECTOR bOrientation = XMLoadFloat4(&_BoxB.Orientation);

			HASSERTD(DirectX::Internal::XMQuaternionIsUnit(bOrientation), "Quaternion does not have unit length!");

			XMVECTOR offset = bCenter - aCenter;

			for (size_t i = 0; i < 8; ++i)
			{
				// Cb = rotate( bExtents * corneroffset[i], bOrientation ) + bcenter
				// Ca = invrotate( Cb - aCenter, aOrientation )

				XMVECTOR C = Math::XMVector3Rotate(bExtents * g_BoxOffset[i], bOrientation) + offset;
				C = XMVector3InverseRotate(C, aOrientation);

				if (XMVector3InBounds(C, aExtents) == false)
					return false;
			}

			return true;
		}

		

		//-----------------------------------------------------------------------------
		// Axis aligned bounding box in oriented bounding box (box1 contains box2)
		//-----------------------------------------------------------------------------
		_Use_decl_annotations_
		///<summary>
		///<para>A (OBB) 'contains' B (AABB)</para>
		///<para>Converts B (AABB) to a OBB.</para>
		///<returns>Returns true only if box A contains box B. Does NOT test for intersection.</returns>
		///</summary>
		inline bool Contains(const BoundingOrientedBox& _BoxA, const BoundingBox& _BoxB)
		{
			// Make the axis aligned box oriented and do an OBB vs OBB test.
			BoundingOrientedBox obox(_BoxB.Center, _BoxB.Extents, XMFLOAT4(0.f, 0.f, 0.f, 1.f));
			return Contains(_BoxA, obox);
		}

#pragma endregion

#pragma region AABB contains ...
		//-----------------------------------------------------------------------------
		// Oriented box in axis-aligned box test (box1 contains box2)
		//-----------------------------------------------------------------------------
		_Use_decl_annotations_
		///<summary>
		///<para>A (AABB) 'contains' B (OBB)</para>
		///<returns>Returns true only if box A contains box B. Does NOT test for intersection.</returns>
		///</summary>
		inline bool Contains(const BoundingBox& _BoxA, const BoundingOrientedBox& _BoxB)
		{
			XMVECTOR vCenter = XMLoadFloat3(&_BoxA.Center);
			XMVECTOR vExtents = XMLoadFloat3(&_BoxA.Extents);

			// Subtract off the AABB center to remove a subtract below
			XMVECTOR oCenter = XMLoadFloat3(&_BoxB.Center) - vCenter;

			XMVECTOR oExtents = XMLoadFloat3(&_BoxB.Extents);
			XMVECTOR oOrientation = XMLoadFloat4(&_BoxB.Orientation);

			HASSERTD(DirectX::Internal::XMQuaternionIsUnit(oOrientation), "Quaternion does not have unit length!");

			XMVECTOR Inside = XMVectorTrueInt();

			for (size_t i = 0; i < BoundingOrientedBox::CORNER_COUNT; ++i)
			{
				XMVECTOR C = Math::XMVector3Rotate(oExtents * g_BoxOffset[i], oOrientation) + oCenter;
				XMVECTOR d = XMVectorAbs(C);

				if (Math::XMVectorCompare(XMVectorLessOrEqual(d, vExtents), XMVectorZero()))
				{
					// at least one axis is outside
					return false;
				}
			}
			return true;
		}

#pragma endregion


		//=============================================================================
		// Overlap
		//-----------------------------------------------------------------------------
		// - Checks only for overlap!
		// - Does NOT NECESSARILY test for containment. (although in some cases it might)
		//=============================================================================
#pragma region OBB overlaps ...
		///<summary>
		///<para>Fast OBB-OBB Overlap test using the separating axis theorem.</para>
		///<returns>Returns true in case of overlaping or containing objects. This is an EXEPTION for OBB-OBB tests.</returns>
		///</summary>
		inline bool Overlaps(const BoundingOrientedBox& _BoxA, const BoundingOrientedBox& _BoxB)
		{
			// Build the 3x3 rotation matrix that defines the orientation of B relative to A.
			XMVECTOR A_quat = XMLoadFloat4(&_BoxA.Orientation);
			XMVECTOR B_quat = XMLoadFloat4(&_BoxB.Orientation);

			HASSERTD(DirectX::Internal::XMQuaternionIsUnit(A_quat), "Quaternion does not have unit length!");
			HASSERTD(DirectX::Internal::XMQuaternionIsUnit(B_quat), "Quaternion does not have unit length!");

			XMMATRIX R = XMMatrixRotationQuaternion(XMQuaternionMultiply(A_quat, XMQuaternionConjugate(B_quat)));

			// Compute the translation of B relative to A.
			XMVECTOR A_cent = XMLoadFloat3(&_BoxA.Center);
			XMVECTOR B_cent = XMLoadFloat3(&_BoxB.Center);
			XMVECTOR t = XMVector3InverseRotate(B_cent - A_cent, A_quat);
			XMFLOAT3 vTf = Math::XMFloat3Get(t);

			const static float fTolerance = 0.0f;
			// Test each of the 15 possible seperating axii.
			// =============================================================================
			// h(A) = extents of A.
			// h(B) = extents of B.
			// -----------------------------------------------------------------------------
			// a(u) = axes of A = (1,0,0), (0,1,0), (0,0,1)
			// b(u) = axes of B relative to A = (r00,r10,r20), (r01,r11,r21), (r02,r12,r22)
			// -----------------------------------------------------------------------------
			// For each possible separating axis l:
			//   d(A) = sum (for i = u,v,w) h(A)(i) * abs( a(i) dot l )
			//   d(B) = sum (for i = u,v,w) h(B)(i) * abs( b(i) dot l )
			//   if abs( t dot l ) > d(A) + d(B) then disjoint


			// A.x
			// ------------------------------------------
			// l = a(u) = (1, 0, 0)
			// t dot l = t.x
			// d(A) = h(A).x
			// d(B) = h(B) dot abs(r00, r01, r02)
			XMVECTOR R0X = R.r[0];
			XMVECTOR AR0X = XMVectorAbs(R0X);
			XMFLOAT3 vExtentsAf = _BoxA.Extents;
			XMVECTOR h_B = XMLoadFloat3(&_BoxB.Extents); // h(B) = extents of B.
			if ((vExtentsAf.x + XMVectorGetX(XMVector3Dot(h_B, AR0X)) - fabsf(vTf.x)) < fTolerance)
			{
				return false;
			}


			// A.y
			// ------------------------------------------
			// l = a(v) = (0, 1, 0)
			// t dot l = t.y
			// d(A) = h(A).y
			// d(B) = h(B) dot abs(r10, r11, r12)
			XMVECTOR R1X = R.r[1];
			XMVECTOR AR1X = XMVectorAbs(R1X);
			if ((vExtentsAf.y + XMVectorGetY(XMVector3Dot(h_B, AR1X)) - fabsf(vTf.y)) < fTolerance)
			{
				return false;
			}

			// A.z
			// ------------------------------------------
			// l = a(w) = (0, 0, 1)
			// t dot l = t.z
			// d(A) = h(A).z
			// d(B) = h(B) dot abs(r20, r21, r22)
			XMVECTOR R2X = R.r[2];
			XMVECTOR AR2X = XMVectorAbs(R2X);
			if ((vExtentsAf.z + XMVectorGetZ(XMVector3Dot(h_B, AR2X)) - fabsf(vTf.z)) < fTolerance)
			{
				return false;
			}

			// now consider columns
			R = XMMatrixTranspose(R);

			// B.x
			// ------------------------------------------
			// l = b(u) = (r00, r10, r20)
			// d(A) = h(A) dot abs(r00, r10, r20)
			// d(B) = h(B).x
			XMVECTOR RX0 = R.r[0];
			XMVECTOR ARX0 = XMVectorAbs(RX0);
			XMFLOAT3 vExtentsBf = _BoxB.Extents;
			XMVECTOR h_A = XMLoadFloat3(&_BoxA.Extents); // h(A) = extents of A.
			if ((XMVectorGetX(XMVectorSubtract(XMVector3Dot(h_A, ARX0), XMVectorAbs(XMVector3Dot(t, RX0)))) + vExtentsBf.x) < 0.f)
			{
				return false;
			}


			// B.y
			// ------------------------------------------
			// l = b(v) = (r01, r11, r21)
			// d(A) = h(A) dot abs(r01, r11, r21)
			// d(B) = h(B).y
			XMVECTOR RX1 = R.r[1];
			XMVECTOR ARX1 = XMVectorAbs(RX1);
			if ((XMVectorGetX(XMVectorSubtract(XMVector3Dot(h_A, ARX1), XMVectorAbs(XMVector3Dot(t, RX1)))) + vExtentsBf.y) < 0.f)
			{
				return false;
			}

			// B.z
			// ------------------------------------------
			// l = b(w) = (r02, r12, r22)
			// d(A) = h(A) dot abs(r02, r12, r22)
			// d(B) = h(B).z
			XMVECTOR RX2 = R.r[2];
			XMVECTOR ARX2 = XMVectorAbs(RX2);
			if ((XMVectorGetX(XMVectorSubtract(XMVector3Dot(h_A, ARX2), XMVectorAbs(XMVector3Dot(t, RX2)))) + vExtentsBf.y) < 0.f)
			{
				return false;
			}

			// A.x  x  B.x
			// ------------------------------------------
			// l = a(u) x b(u) = (0, -r20, r10)
			// d(A) = h(A) dot abs(0, r20, r10)
			// d(B) = h(B) dot abs(0, r02, r01)
			// normalize test axis, to get correct MTVs, d should only depend on t and projection (not length of test axis)
			XMVECTOR vL = XMVectorPermute<XM_PERMUTE_0W, XM_PERMUTE_1Z, XM_PERMUTE_0Y, XM_PERMUTE_0X>(RX0, -RX0);
			XMVECTOR vD;
			XMVECTOR vD_A;
			XMVECTOR vD_B;
			vD = XMVectorAbs(XMVector3Dot(t, vL));
			vD_A = XMVector3Dot(h_A, XMVectorSwizzle<XM_SWIZZLE_W, XM_SWIZZLE_Z, XM_SWIZZLE_Y, XM_SWIZZLE_X>(ARX0));
			vD_B = XMVector3Dot(h_B, XMVectorSwizzle<XM_SWIZZLE_W, XM_SWIZZLE_Z, XM_SWIZZLE_Y, XM_SWIZZLE_X>(AR0X));
			if (XMVectorGetX(XMVectorSubtract(XMVectorAdd(vD_A, vD_B), vD)) < 0.f)
			{
				return false;
			}


			// A.x  x  B.y
			// ------------------------------------------
			// l = a(u) x b(v) = (0, -r21, r11)
			// d(A) = h(A) dot abs(0, r21, r11)
			// d(B) = h(B) dot abs(r02, 0, r00)
			vL = XMVectorPermute<XM_PERMUTE_0W, XM_PERMUTE_1Z, XM_PERMUTE_0Y, XM_PERMUTE_0X>(RX1, -RX1);
			vD = XMVectorAbs(XMVector3Dot(t, vL));
			vD_A = XMVector3Dot(h_A, XMVectorSwizzle<XM_SWIZZLE_W, XM_SWIZZLE_Z, XM_SWIZZLE_Y, XM_SWIZZLE_X>(ARX1));
			vD_B = XMVector3Dot(h_B, XMVectorSwizzle<XM_SWIZZLE_Z, XM_SWIZZLE_W, XM_SWIZZLE_X, XM_SWIZZLE_Y>(AR0X));
			if (XMVectorGetX(XMVectorSubtract(XMVectorAdd(vD_A, vD_B), vD)) < 0.f)
			{
				return false;
			}

			// A.x  x  B.z
			// ------------------------------------------
			// l = a(u) x b(w) = (0, -r22, r12)
			// d(A) = h(A) dot abs(0, r22, r12)
			// d(B) = h(B) dot abs(r01, r00, 0)
			vL = XMVectorPermute<XM_PERMUTE_0W, XM_PERMUTE_1Z, XM_PERMUTE_0Y, XM_PERMUTE_0X>(RX2, -RX2);
			vD = XMVectorAbs(XMVector3Dot(t, vL));
			vD_A = XMVector3Dot(h_A, XMVectorSwizzle<XM_SWIZZLE_W, XM_SWIZZLE_Z, XM_SWIZZLE_Y, XM_SWIZZLE_X>(ARX2));
			vD_B = XMVector3Dot(h_B, XMVectorSwizzle<XM_SWIZZLE_Y, XM_SWIZZLE_X, XM_SWIZZLE_W, XM_SWIZZLE_Z>(AR0X));
			if (XMVectorGetX(XMVectorSubtract(XMVectorAdd(vD_A, vD_B), vD)) < 0.f)
			{
				return false;
			}

			// A.y  x  B.x
			// ------------------------------------------
			// l = a(v) x b(u) = (r20, 0, -r00)
			// d(A) = h(A) dot abs(r20, 0, r00)
			// d(B) = h(B) dot abs(0, r12, r11)
			vL = XMVectorPermute<XM_PERMUTE_0Z, XM_PERMUTE_0W, XM_PERMUTE_1X, XM_PERMUTE_0Y>(RX0, -RX0);
			vD = XMVectorAbs(XMVector3Dot(t, vL));
			vD_A = XMVector3Dot(h_A, XMVectorSwizzle<XM_SWIZZLE_Z, XM_SWIZZLE_W, XM_SWIZZLE_X, XM_SWIZZLE_Y>(ARX0));
			vD_B = XMVector3Dot(h_B, XMVectorSwizzle<XM_SWIZZLE_W, XM_SWIZZLE_Z, XM_SWIZZLE_Y, XM_SWIZZLE_X>(AR1X));
			if (XMVectorGetX(XMVectorSubtract(XMVectorAdd(vD_A, vD_B), vD)) < 0.f)
			{
				return false;
			}

			// A.y  x  B.y
			// ------------------------------------------
			// l = a(v) x b(v) = (r21, 0, -r01)
			// d(A) = h(A) dot abs(r21, 0, r01)
			// d(B) = h(B) dot abs(r12, 0, r10)
			vL = XMVectorPermute<XM_PERMUTE_0Z, XM_PERMUTE_0W, XM_PERMUTE_1X, XM_PERMUTE_0Y>(RX1, -RX1);
			vD = XMVectorAbs(XMVector3Dot(t, vL));
			vD_A = XMVector3Dot(h_A, XMVectorSwizzle<XM_SWIZZLE_Z, XM_SWIZZLE_W, XM_SWIZZLE_X, XM_SWIZZLE_Y>(ARX1));
			vD_B = XMVector3Dot(h_B, XMVectorSwizzle<XM_SWIZZLE_Z, XM_SWIZZLE_W, XM_SWIZZLE_X, XM_SWIZZLE_Y>(AR1X));
			if (XMVectorGetX(XMVectorSubtract(XMVectorAdd(vD_A, vD_B), vD)) < 0.f)
			{
				return false;
			}

			// A.y  x  B.z
			// ------------------------------------------
			// l = a(v) x b(w) = (r22, 0, -r02)
			// d(A) = h(A) dot abs(r22, 0, r02)
			// d(B) = h(B) dot abs(r11, r10, 0)
			vL = XMVectorPermute<XM_PERMUTE_0Z, XM_PERMUTE_0W, XM_PERMUTE_1X, XM_PERMUTE_0Y>(RX2, -RX2);
			vD = XMVectorAbs(XMVector3Dot(t, vL));
			vD_A = XMVector3Dot(h_A, XMVectorSwizzle<XM_SWIZZLE_Z, XM_SWIZZLE_W, XM_SWIZZLE_X, XM_SWIZZLE_Y>(ARX2));
			vD_B = XMVector3Dot(h_B, XMVectorSwizzle<XM_SWIZZLE_Y, XM_SWIZZLE_X, XM_SWIZZLE_W, XM_SWIZZLE_Z>(AR1X));
			if (XMVectorGetX(XMVectorSubtract(XMVectorAdd(vD_A, vD_B), vD)) < 0.f)
			{
				return false;
			}

			// A.z  x  B.x
			// ------------------------------------------
			// l = a(w) x b(u) = (-r10, r00, 0)
			// d(A) = h(A) dot abs(r10, r00, 0)
			// d(B) = h(B) dot abs(0, r22, r21)
			vL = XMVectorPermute<XM_PERMUTE_1Y, XM_PERMUTE_0X, XM_PERMUTE_0W, XM_PERMUTE_0Z>(RX0, -RX0);
			vD = XMVectorAbs(XMVector3Dot(t, vL));
			vD_A = XMVector3Dot(h_A, XMVectorSwizzle<XM_SWIZZLE_Y, XM_SWIZZLE_X, XM_SWIZZLE_W, XM_SWIZZLE_Z>(ARX0));
			vD_B = XMVector3Dot(h_B, XMVectorSwizzle<XM_SWIZZLE_W, XM_SWIZZLE_Z, XM_SWIZZLE_Y, XM_SWIZZLE_X>(AR2X));
			if (XMVectorGetX(XMVectorSubtract(XMVectorAdd(vD_A, vD_B), vD)) < 0.f)
			{
				return false;
			}

			// A.z  x  B.y
			// ------------------------------------------
			// l = a(w) x b(v) = (-r11, r01, 0)
			// d(A) = h(A) dot abs(r11, r01, 0)
			// d(B) = h(B) dot abs(r22, 0, r20)
			vL = XMVectorPermute<XM_PERMUTE_1Y, XM_PERMUTE_0X, XM_PERMUTE_0W, XM_PERMUTE_0Z>(RX1, -RX1);
			vD = XMVectorAbs(XMVector3Dot(t, vL));
			vD_A = XMVector3Dot(h_A, XMVectorSwizzle<XM_SWIZZLE_Y, XM_SWIZZLE_X, XM_SWIZZLE_W, XM_SWIZZLE_Z>(ARX1));
			vD_B = XMVector3Dot(h_B, XMVectorSwizzle<XM_SWIZZLE_Z, XM_SWIZZLE_W, XM_SWIZZLE_X, XM_SWIZZLE_Y>(AR2X));
			if (XMVectorGetX(XMVectorSubtract(XMVectorAdd(vD_A, vD_B), vD)) < 0.f)
			{
				return false;
			}

			// A.z  x  B.z
			// ------------------------------------------
			// l = a(w) x b(w) = (-r12, r02, 0)
			// d(A) = h(A) dot abs(r12, r02, 0)
			// d(B) = h(B) dot abs(r21, r20, 0)
			vL = XMVectorPermute<XM_PERMUTE_1Y, XM_PERMUTE_0X, XM_PERMUTE_0W, XM_PERMUTE_0Z>(RX2, -RX2);
			vD = XMVectorAbs(XMVector3Dot(t, vL));
			vD_A = XMVector3Dot(h_A, XMVectorSwizzle<XM_SWIZZLE_Y, XM_SWIZZLE_X, XM_SWIZZLE_W, XM_SWIZZLE_Z>(ARX2));
			vD_B = XMVector3Dot(h_B, XMVectorSwizzle<XM_SWIZZLE_Y, XM_SWIZZLE_X, XM_SWIZZLE_W, XM_SWIZZLE_Z>(AR2X));
			if (XMVectorGetX(XMVectorSubtract(XMVectorAdd(vD_A, vD_B), vD)) < 0.f)
			{
				return false;
			}

			// No seperating axis found, boxes must intersect.
			return true;
		}

		///<summary>
		///<para>OBB-OBB Overlap test using the separating axis theorem.</para>
		///<para> - Fills CollisionInfo with a collision normal and a penetration depth.</para>
		///<para> - The penetration depth will NOT be correct in containment cases.</para>
		///<returns>Returns true in case of overlaping or containing objects. This is an EXEPTION for OBB-OBB tests.</returns>
		///</summary>
		inline bool Overlaps(const BoundingOrientedBox& _BoxA, const BoundingOrientedBox& _BoxB, CollisionInfo& _Info)
		{
			_Info.m_vNormal = XMFLOAT3_ZERO;
			_Info.m_fPenetrationDepth = 0.0f;
			// Build the 3x3 rotation matrix that defines the orientation of B relative to A.
			XMVECTOR A_quat = XMLoadFloat4(&_BoxA.Orientation);
			XMVECTOR B_quat = XMLoadFloat4(&_BoxB.Orientation);
			
			HASSERTD(DirectX::Internal::XMQuaternionIsUnit(A_quat), "Quaternion does not have unit length!");
			HASSERTD(DirectX::Internal::XMQuaternionIsUnit(B_quat), "Quaternion does not have unit length!");

			XMVECTOR Q = XMQuaternionMultiply(A_quat, XMQuaternionConjugate(B_quat));
			XMMATRIX R = XMMatrixRotationQuaternion(Q);

			// Compute the translation of B relative to A.
			XMVECTOR A_cent = XMLoadFloat3(&_BoxA.Center);
			XMVECTOR B_cent = XMLoadFloat3(&_BoxB.Center);
			XMVECTOR t = XMVector3InverseRotate(B_cent - A_cent, A_quat);
			XMFLOAT3 vTf = Math::XMFloat3Get(t);

			const static float fTolerance = 0.0f;

			// Test each of the 15 possible seperating axii.
			// =============================================================================
			// h(A) = extents of A.
			// h(B) = extents of B.
			// -----------------------------------------------------------------------------
			// a(u) = axes of A = (1,0,0), (0,1,0), (0,0,1)
			// b(u) = axes of B relative to A = (r00,r10,r20), (r01,r11,r21), (r02,r12,r22)
			// -----------------------------------------------------------------------------
			// For each possible separating axis l:
			//   d(A) = sum (for i = u,v,w) h(A)(i) * abs( a(i) dot l )
			//   d(B) = sum (for i = u,v,w) h(B)(i) * abs( b(i) dot l )
			//   if abs( t dot l ) > d(A) + d(B) then disjoint


			// A.x
			// ------------------------------------------
			// l = a(u) = (1, 0, 0)
			// t dot l = t.x
			// d(A) = h(A).x
			// d(B) = h(B) dot abs(r00, r01, r02)
			XMVECTOR R0X = XMVector3Normalize(R.r[0]);
			XMVECTOR AR0X = XMVectorAbs(R0X);
			XMFLOAT3 vExtentsAf = _BoxA.Extents;
			XMVECTOR h_B = XMLoadFloat3(&_BoxB.Extents); // h(B) = extents of B.

			float fOverlap = vExtentsAf.x + XMVectorGetX(XMVector3Dot(h_B, AR0X)) - fabsf(vTf.x);
			if (fOverlap < fTolerance)
			{
				return false;
			}
			// MTV variables
			float fMinimalOverlap = fOverlap;
			XMVECTOR vMinimalTestAxis = XMVectorSet(1.0f, 0.0f, 0.0f, 0.0f); // = vL

			// A.y
			// ------------------------------------------
			// l = a(v) = (0, 1, 0)
			// t dot l = t.y
			// d(A) = h(A).y
			// d(B) = h(B) dot abs(r10, r11, r12)
			XMVECTOR R1X = XMVector3Normalize(R.r[1]);
			XMVECTOR AR1X = XMVectorAbs(R1X);
			float d_a = vExtentsAf.y;
			float d_b = XMVectorGetY(XMVector3Dot(h_B, AR1X));
			float tdl = fabsf(vTf.y);
			float o = d_a + d_b - tdl;
			fOverlap = vExtentsAf.y + XMVectorGetY(XMVector3Dot(h_B, AR1X)) - fabsf(vTf.y);
			if (fOverlap < fTolerance)
			{
				return false;
			}
			else if (fOverlap < fMinimalOverlap)
			{
				fMinimalOverlap = fOverlap;
				vMinimalTestAxis = XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f); // = vL
			}

			// A.z
			// ------------------------------------------
			// l = a(w) = (0, 0, 1)
			// t dot l = t.z
			// d(A) = h(A).z
			// d(B) = h(B) dot abs(r20, r21, r22)
			XMVECTOR R2X = XMVector3Normalize(R.r[2]);
			XMVECTOR AR2X = XMVectorAbs(R2X);

			fOverlap = vExtentsAf.z + XMVectorGetZ(XMVector3Dot(h_B, AR2X)) - fabsf(vTf.z);
			if (fOverlap  < fTolerance)
			{
				return false;
			}
			else if (fOverlap < fMinimalOverlap)
			{
				fMinimalOverlap = fOverlap;
				vMinimalTestAxis = XMVectorSet(0.0f, 0.0f, 1.0f, 0.0f); // = vL
			}


			// now collums are considered
			R = XMMatrixTranspose(R);

			// B.x
			// ------------------------------------------
			// l = b(u) = (r00, r10, r20)
			// d(A) = h(A) dot abs(r00, r10, r20)
			// d(B) = h(B).x
			XMVECTOR RX0 = XMVector3Normalize(R.r[0]);
			XMVECTOR ARX0 = XMVectorAbs(RX0);
			XMFLOAT3 vExtentsBf = _BoxB.Extents;
			XMVECTOR h_A = XMLoadFloat3(&_BoxA.Extents); // h(A) = extents of A.

			fOverlap = XMVectorGetX(XMVectorSubtract(XMVector3Dot(h_A, ARX0), XMVectorAbs(XMVector3Dot(t, RX0)))) + vExtentsBf.x;
			if (fOverlap < fTolerance)
			{
				return false;
			}
			else if (fOverlap < fMinimalOverlap)
			{
				fMinimalOverlap = fOverlap;
				vMinimalTestAxis = RX0; // = vL
			}

			// B.y
			// ------------------------------------------
			// l = b(v) = (r01, r11, r21)
			// d(A) = h(A) dot abs(r01, r11, r21)
			// d(B) = h(B).y
			XMVECTOR RX1 = XMVector3Normalize(R.r[1]);
			XMVECTOR ARX1 = XMVectorAbs(RX1);

			fOverlap = XMVectorGetX(XMVectorSubtract(XMVector3Dot(h_A, ARX1), XMVectorAbs(XMVector3Dot(t, RX1)))) + vExtentsBf.y;
			if (fOverlap < fTolerance)
			{
				return false;
			}
			else if (fOverlap < fMinimalOverlap)
			{
				fMinimalOverlap = fOverlap;
				vMinimalTestAxis = RX1; // = vL
			}

			// B.z
			// ------------------------------------------
			// l = b(w) = (r02, r12, r22)
			// d(A) = h(A) dot abs(r02, r12, r22)
			// d(B) = h(B).z
			XMVECTOR RX2 = XMVector3Normalize(R.r[2]);
			XMVECTOR ARX2 = XMVectorAbs(RX2);

			fOverlap = XMVectorGetX(XMVectorSubtract(XMVector3Dot(h_A, ARX2), XMVectorAbs(XMVector3Dot(t, RX2)))) + vExtentsBf.z;
			if (fOverlap < fTolerance)
			{
				return false;
			}
			else if (fOverlap < fMinimalOverlap)
			{
				fMinimalOverlap = fOverlap;
				vMinimalTestAxis = RX2; // = vL
			}

			// A.x  x  B.x
			// ------------------------------------------
			// l = a(u) x b(u) = (0, -r20, r10)
			// d(A) = h(A) dot abs(0, r20, r10)
			// d(B) = h(B) dot abs(0, r02, r01)
			// normalize test axis, to get correct MTVs, d should only depend on t and projection (not length of test axis)
			XMVECTOR vL = XMVector3Normalize(XMVectorPermute<XM_PERMUTE_0W, XM_PERMUTE_1Z, XM_PERMUTE_0Y, XM_PERMUTE_0X>(RX0, -RX0));
			XMVECTOR vD;
			XMVECTOR vD_A;
			XMVECTOR vD_B;
			if (XMVector3NotEqual(XMVectorZero(), vL))
			{
				vD = XMVector3Dot(t, vL);
				vD_A = XMVector3Dot(h_A, XMVector3Normalize(XMVectorSwizzle<XM_SWIZZLE_W, XM_SWIZZLE_Z, XM_SWIZZLE_Y, XM_SWIZZLE_X>(ARX0)));
				vD_B = XMVector3Dot(h_B, XMVector3Normalize(XMVectorSwizzle<XM_SWIZZLE_W, XM_SWIZZLE_Z, XM_SWIZZLE_Y, XM_SWIZZLE_X>(AR0X)));
				fOverlap = XMVectorGetX(XMVectorSubtract(XMVectorAdd(vD_A, vD_B), XMVectorAbs(vD)));
				if (fOverlap < fTolerance)
				{
					return false;
				}
				else if (fOverlap < fMinimalOverlap)
				{
					fMinimalOverlap = fOverlap;
					vMinimalTestAxis = vL; // = vL
				}
			}


			// A.x  x  B.y
			// ------------------------------------------
			// l = a(u) x b(v) = (0, -r21, r11)
			// d(A) = h(A) dot abs(0, r21, r11)
			// d(B) = h(B) dot abs(r02, 0, r00)
			vL = XMVector3Normalize(XMVectorPermute<XM_PERMUTE_0W, XM_PERMUTE_1Z, XM_PERMUTE_0Y, XM_PERMUTE_0X>(RX1, -RX1));
			if (XMVector3NotEqual(XMVectorZero(), vL))
			{
				vD = XMVector3Dot(t, vL);
				vD_A = XMVector3Dot(h_A, XMVector3Normalize(XMVectorSwizzle<XM_SWIZZLE_W, XM_SWIZZLE_Z, XM_SWIZZLE_Y, XM_SWIZZLE_X>(ARX1)));
				vD_B = XMVector3Dot(h_B, XMVector3Normalize(XMVectorSwizzle<XM_SWIZZLE_Z, XM_SWIZZLE_W, XM_SWIZZLE_X, XM_SWIZZLE_Y>(AR0X)));
				fOverlap = XMVectorGetX(XMVectorSubtract(XMVectorAdd(vD_A, vD_B), XMVectorAbs(vD)));
				if (fOverlap < fTolerance)
				{
					return false;
				}
				else if (fOverlap < fMinimalOverlap)
				{
					fMinimalOverlap = fOverlap;
					vMinimalTestAxis = vL; // = vL
				}
			}


			// A.x  x  B.z
			// ------------------------------------------
			// l = a(u) x b(w) = (0, -r22, r12)
			// d(A) = h(A) dot abs(0, r22, r12)
			// d(B) = h(B) dot abs(r01, r00, 0)
			vL = XMVector3Normalize(XMVectorPermute<XM_PERMUTE_0W, XM_PERMUTE_1Z, XM_PERMUTE_0Y, XM_PERMUTE_0X>(RX2, -RX2));
			if (XMVector3NotEqual(XMVectorZero(), vL))
			{
				vD = XMVector3Dot(t, vL);
				vD_A = XMVector3Dot(h_A, XMVector3Normalize(XMVectorSwizzle<XM_SWIZZLE_W, XM_SWIZZLE_Z, XM_SWIZZLE_Y, XM_SWIZZLE_X>(ARX2)));
				vD_B = XMVector3Dot(h_B, XMVector3Normalize(XMVectorSwizzle<XM_SWIZZLE_Y, XM_SWIZZLE_X, XM_SWIZZLE_W, XM_SWIZZLE_Z>(AR0X)));
				fOverlap = XMVectorGetX(XMVectorSubtract(XMVectorAdd(vD_A, vD_B), XMVectorAbs(vD)));
				if (fOverlap < fTolerance)
				{
					return false;
				}
				else if (fOverlap < fMinimalOverlap)
				{
					fMinimalOverlap = fOverlap;
					vMinimalTestAxis = vL; // = vL
				}
			}


			// A.y  x  B.x
			// ------------------------------------------
			// l = a(v) x b(u) = (r20, 0, -r00)
			// d(A) = h(A) dot abs(r20, 0, r00)
			// d(B) = h(B) dot abs(0, r12, r11)
			vL = XMVector3Normalize(XMVectorPermute<XM_PERMUTE_0Z, XM_PERMUTE_0W, XM_PERMUTE_1X, XM_PERMUTE_0Y>(RX0, -RX0));
			if (XMVector3NotEqual(XMVectorZero(), vL))
			{
				vD = XMVector3Dot(t, vL);
				vD_A = XMVector3Dot(h_A, XMVector3Normalize(XMVectorSwizzle<XM_SWIZZLE_Z, XM_SWIZZLE_W, XM_SWIZZLE_X, XM_SWIZZLE_Y>(ARX0)));
				vD_B = XMVector3Dot(h_B, XMVector3Normalize(XMVectorSwizzle<XM_SWIZZLE_W, XM_SWIZZLE_Z, XM_SWIZZLE_Y, XM_SWIZZLE_X>(AR1X)));
				fOverlap = XMVectorGetX(XMVectorSubtract(XMVectorAdd(vD_A, vD_B), XMVectorAbs(vD)));
				if (fOverlap < fTolerance)
				{
					return false;
				}
				else if (fOverlap < fMinimalOverlap)
				{
					fMinimalOverlap = fOverlap;
					vMinimalTestAxis = vL; // = vL
				}
			}


			// A.y  x  B.y
			// ------------------------------------------
			// l = a(v) x b(v) = (r21, 0, -r01)
			// d(A) = h(A) dot abs(r21, 0, r01)
			// d(B) = h(B) dot abs(r12, 0, r10)
			vL = XMVector3Normalize(XMVectorPermute<XM_PERMUTE_0Z, XM_PERMUTE_0W, XM_PERMUTE_1X, XM_PERMUTE_0Y>(RX1, -RX1));
			if (XMVector3NotEqual(XMVectorZero(), vL))
			{
				vD = XMVector3Dot(t, vL);
				vD_A = XMVector3Dot(h_A, XMVector3Normalize(XMVectorSwizzle<XM_SWIZZLE_Z, XM_SWIZZLE_W, XM_SWIZZLE_X, XM_SWIZZLE_Y>(ARX1)));
				vD_B = XMVector3Dot(h_B, XMVector3Normalize(XMVectorSwizzle<XM_SWIZZLE_Z, XM_SWIZZLE_W, XM_SWIZZLE_X, XM_SWIZZLE_Y>(AR1X)));
				fOverlap = XMVectorGetX(XMVectorSubtract(XMVectorAdd(vD_A, vD_B), XMVectorAbs(vD)));
				if (fOverlap < fTolerance)
				{
					return false;
				}
				else if (fOverlap < fMinimalOverlap)
				{
					fMinimalOverlap = fOverlap;
					vMinimalTestAxis = vL; // = vL
				}
			}


			// A.y  x  B.z
			// ------------------------------------------
			// l = a(v) x b(w) = (r22, 0, -r02)
			// d(A) = h(A) dot abs(r22, 0, r02)
			// d(B) = h(B) dot abs(r11, r10, 0)
			vL = XMVector3Normalize(XMVectorPermute<XM_PERMUTE_0Z, XM_PERMUTE_0W, XM_PERMUTE_1X, XM_PERMUTE_0Y>(RX2, -RX2));
			if (XMVector3NotEqual(XMVectorZero(), vL))
			{
				vD = XMVector3Dot(t, vL);
				vD_A = XMVector3Dot(h_A, XMVector3Normalize(XMVectorSwizzle<XM_SWIZZLE_Z, XM_SWIZZLE_W, XM_SWIZZLE_X, XM_SWIZZLE_Y>(ARX2)));
				vD_B = XMVector3Dot(h_B, XMVector3Normalize(XMVectorSwizzle<XM_SWIZZLE_Y, XM_SWIZZLE_X, XM_SWIZZLE_W, XM_SWIZZLE_Z>(AR1X)));
				fOverlap = XMVectorGetX(XMVectorSubtract(XMVectorAdd(vD_A, vD_B), XMVectorAbs(vD)));
				if (fOverlap < fTolerance)
				{
					return false;
				}
				else if (fOverlap < fMinimalOverlap)
				{
					fMinimalOverlap = fOverlap;
					vMinimalTestAxis = vL; // = vL
				}
			}


			// A.z  x  B.x
			// ------------------------------------------
			// l = a(w) x b(u) = (-r10, r00, 0)
			// d(A) = h(A) dot abs(r10, r00, 0)
			// d(B) = h(B) dot abs(0, r22, r21)
			vL = XMVector3Normalize(XMVectorPermute<XM_PERMUTE_1Y, XM_PERMUTE_0X, XM_PERMUTE_0W, XM_PERMUTE_0Z>(RX0, -RX0));
			if (XMVector3NotEqual(XMVectorZero(), vL))
			{
				vD = XMVector3Dot(t, vL);
				vD_A = XMVector3Dot(h_A, XMVector3Normalize(XMVectorSwizzle<XM_SWIZZLE_Y, XM_SWIZZLE_X, XM_SWIZZLE_W, XM_SWIZZLE_Z>(ARX0)));
				vD_B = XMVector3Dot(h_B, XMVector3Normalize(XMVectorSwizzle<XM_SWIZZLE_W, XM_SWIZZLE_Z, XM_SWIZZLE_Y, XM_SWIZZLE_X>(AR2X)));
				fOverlap = XMVectorGetX(XMVectorSubtract(XMVectorAdd(vD_A, vD_B), XMVectorAbs(vD)));
				if (fOverlap < fTolerance)
				{
					return false;
				}
				else if (fOverlap < fMinimalOverlap)
				{
					fMinimalOverlap = fOverlap;
					vMinimalTestAxis = vL; // = vL
				}

			}

			// A.z  x  B.y
			// ------------------------------------------
			// l = a(w) x b(v) = (-r11, r01, 0)
			// d(A) = h(A) dot abs(r11, r01, 0)
			// d(B) = h(B) dot abs(r22, 0, r20)
			vL = XMVector3Normalize(XMVectorPermute<XM_PERMUTE_1Y, XM_PERMUTE_0X, XM_PERMUTE_0W, XM_PERMUTE_0Z>(RX1, -RX1));
			if (XMVector3NotEqual(XMVectorZero(), vL))
			{
				vD = XMVector3Dot(t, vL);
				vD_A = XMVector3Dot(h_A, XMVector3Normalize(XMVectorSwizzle<XM_SWIZZLE_Y, XM_SWIZZLE_X, XM_SWIZZLE_W, XM_SWIZZLE_Z>(ARX1)));
				vD_B = XMVector3Dot(h_B, XMVector3Normalize(XMVectorSwizzle<XM_SWIZZLE_Z, XM_SWIZZLE_W, XM_SWIZZLE_X, XM_SWIZZLE_Y>(AR2X)));
				fOverlap = XMVectorGetX(XMVectorSubtract(XMVectorAdd(vD_A, vD_B), XMVectorAbs(vD)));
				if (fOverlap < fTolerance)
				{
					return false;
				}
				else if (fOverlap < fMinimalOverlap)
				{
					fMinimalOverlap = fOverlap;
					vMinimalTestAxis = vL; // = vL
				}

			}

			// A.z  x  B.z
			// ------------------------------------------
			// l = a(w) x b(w) = (-r12, r02, 0)
			// d(A) = h(A) dot abs(r12, r02, 0)
			// d(B) = h(B) dot abs(r21, r20, 0)
			vL = XMVector3Normalize(XMVectorPermute<XM_PERMUTE_1Y, XM_PERMUTE_0X, XM_PERMUTE_0W, XM_PERMUTE_0Z>(RX2, -RX2));
			if (XMVector3NotEqual(XMVectorZero(), vL))
			{
				vD = XMVector3Dot(t, vL);
				vD_A = XMVector3Dot(h_A, XMVector3Normalize(XMVectorSwizzle<XM_SWIZZLE_Y, XM_SWIZZLE_X, XM_SWIZZLE_W, XM_SWIZZLE_Z>(ARX2)));
				vD_B = XMVector3Dot(h_B, XMVector3Normalize(XMVectorSwizzle<XM_SWIZZLE_Y, XM_SWIZZLE_X, XM_SWIZZLE_W, XM_SWIZZLE_Z>(AR2X)));
				fOverlap = XMVectorGetX(XMVectorSubtract(XMVectorAdd(vD_A, vD_B), XMVectorAbs(vD)));
				if (fOverlap < fTolerance)
				{
					return false;
				}
				else if (fOverlap < fMinimalOverlap)
				{
					fMinimalOverlap = fOverlap;
					vMinimalTestAxis = vL; // = vL
				}

			}

			// No seperating axis found, boxes must intersect.
			XMVECTOR vNormal = XMVector3Normalize(Math::XMVector3Rotate(vMinimalTestAxis, A_quat));

			if (XMVector3Greater(XMVector3Dot(vNormal, XMVectorSubtract(B_cent, A_cent)), XMVectorZero()))
			{
				vNormal = XMVectorNegate(vNormal);
			}

			XMStoreFloat3(&_Info.m_vNormal, vNormal);
			_Info.m_fPenetrationDepth = fabsf(fMinimalOverlap);

			return true;
		}
#pragma endregion

#pragma region AABB overlaps ...
		///<summary>
		///<para>AABB-OBB Overlap test using the separating axis theorem.</para>
		///<para> - Converts the AABB to a OBB.</para>
		///<returns>Returns true in case of overlaping or containing objects. This is an EXEPTION because of the underlying OBB-OBB test.</returns>
		///</summary>
		inline bool Overlaps(const BoundingBox& _BoxA, const BoundingOrientedBox _BoxB)
		{
			BoundingOrientedBox obox(_BoxA.Center, _BoxA.Extents, XMFLOAT4(0.f, 0.f, 0.f, 1.f));
			return Overlaps(obox, _BoxB);
		}
#pragma endregion

		//=============================================================================
		// Conversion
		//=============================================================================
		
		//----------------------------------------------------------------------------------------------
		inline void ConvertOBBToAABB(_In_ const BoundingOrientedBox& _OBB, _Out_ BoundingBox& _OutAABB)
		{
			XMFLOAT3 pPoints[8];
			_OBB.GetCorners(pPoints);

			// Find the minimum and maximum x, y, and z
			XMVECTOR vMin, vMax, vPoint;

			vMin = vMax = XMLoadFloat3(pPoints);

			for (uint32_t i = 1; i < 8; ++i)
			{
				vPoint = XMLoadFloat3(&pPoints[i]);

				vMin = XMVectorMin(vMin, vPoint);
				vMax = XMVectorMax(vMax, vPoint);
			}

			// Store center and extents.
			//XMStoreFloat3(&_Out.Center, (vMin + vMax) * 0.5f);
			_OutAABB.Center = _OBB.Center;
			XMStoreFloat3(&_OutAABB.Extents, (vMax - vMin) * 0.5f);
		}

	}; // Collision
}; // Helix

#endif // XMCOLLISION_H