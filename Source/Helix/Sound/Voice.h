//   Copyright 2020 Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef VOICE_H
#define VOICE_H

#include "hlx\src\StandardDefines.h"
#include "hlx\src\String.h"
#include "AudioLibrary.h"

#include <xaudio2.h>
#include <memory>

namespace Helix
{
	namespace Sound
	{
		class Mixer;

		class Voice
		{
		public:
			Voice();
			~Voice();

			Voice(Voice&& _Other);
			Voice(const Voice&) = delete;

			bool Create(const hlx::string& _sFileName, const bool _bIsLooping = false, const XAUDIO2_VOICE_SENDS* _pVoiceSends = nullptr);
			bool Start();
			bool Stop();
			void Loop(const bool _bIsLooping = true);
			bool IsLooping() const;
			bool IsPlaying() const;

			void SetVolume(float _fVolume);
			const float GetVolume() const;

			const hlx::string& GetName() const;
			void SetOutputVoices(const XAUDIO2_VOICE_SENDS* _pVoiceSends);

			const std::shared_ptr<AudioLibrary::AudioData>& GetData();

		private:
			IXAudio2SourceVoice* m_pSourceVoice = nullptr;
			XAUDIO2_BUFFER m_AudioBuffer = { 0 };
			std::shared_ptr<AudioLibrary::AudioData> m_pAudioData = nullptr;
			AudioLibrary* m_pAudioLibrary = nullptr;
			Mixer* m_pMixer = nullptr;
			hlx::string m_sVoiceName;

			bool m_bIsPlaying = false;
		};

		inline const hlx::string& Voice::GetName() const{return m_sVoiceName;}
		inline bool Voice::IsPlaying() const{return m_bIsPlaying;}
	}
}

#endif // !VOICE_H