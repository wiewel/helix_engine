//   Copyright 2020 Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef LISTENER_H
#define LISTENER_H

#include "hlx\src\StandardDefines.h"
#include "Math\MathTypes.h"
#include "DataStructures\GameObjectComponent.h"

#include <x3daudio.h>
#include <xaudio2.h>

namespace Helix
{
	namespace Sound
	{
		class Listener : public Datastructures::IGameObjectComponent
		{
		public:
			Listener(Datastructures::GameObject* _pParent);

			COMPTYPENAME(Listener, Datastructures::GameObject, 'Lstn');
			COMPDESCONSTR(Listener, Datastructures::GameObject, IGameObjectComponent);

			void OnUpdate(double _fDeltaTime, double _fElapsedTime) final;
			const X3DAUDIO_LISTENER& GetX3DAudioListener() const;

		private:
			void SetPosition(Math::float3 _Position);
			X3DAUDIO_LISTENER m_Listener;
		};
	}
}

#endif // !LISTENER_H



