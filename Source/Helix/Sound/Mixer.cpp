//   Copyright 2020 Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "Mixer.h"
#include "hlx\src\Logger.h"
#include "Util\Functional.h"
#include "hlx\src\StandardDefines.h"
#include "AudioLibrary.h"

using namespace Helix::Sound;
//----------------------------------------------------------------------------------------------------------------------------------
Mixer::Mixer()
{
	uint32_t uXAudio2Processor = XAUDIO2_DEFAULT_PROCESSOR;

	// Create the XAudio2 Engine
	HRESULT Result = XAudio2Create(&m_pXAudio2, 0, uXAudio2Processor);
	if (FAILED(Result))
	{
		HERROR("Failed to initialize Sound");
	}

	// Create a Master voice to which all other voices will be applied
	Result = m_pXAudio2->CreateMasteringVoice(&m_pMasteringVoice);
	if (FAILED(Result))
	{
		HERROR("Failed to create mastering voice");
	}
	DWORD dwChannelMask = 0u;
	m_pMasteringVoice->GetChannelMask(&dwChannelMask);
	X3DAudioInitialize(dwChannelMask, X3DAUDIO_SPEED_OF_SOUND, m_X3DInstance);
	
	m_pMasteringVoice->GetVoiceDetails(&m_MasteringVoiceDetails);
}
//----------------------------------------------------------------------------------------------------------------------------------
Mixer::~Mixer()
{
	safe_release<IXAudio2MasteringVoice>(m_pMasteringVoice);
	//safe_release<IXAudio2>(m_pXAudio2);
}
//----------------------------------------------------------------------------------------------------------------------------------
void Mixer::SetVolume(float _fMasterVolume)
{
}
//----------------------------------------------------------------------------------------------------------------------------------
IXAudio2 * Helix::Sound::Mixer::GetXAudio2() const
{
	return m_pXAudio2;
}
//----------------------------------------------------------------------------------------------------------------------------------
void Mixer::SetListener(Listener * _pListener)
{
	if (m_pListener == nullptr)
	{
		m_pListener = _pListener;
	}
	else
	{
		HWARNING("Tried to assign assign multiple Listeners to the Mixer. There can only be one.");
	}
}
//----------------------------------------------------------------------------------------------------------------------------------
const Listener* Mixer::GetListener() const
{
	return m_pListener;
}
//----------------------------------------------------------------------------------------------------------------------------------
const SubmixVoice& Helix::Sound::Mixer::GetChannelVoice(const ChannelGroup& _Channel)
{
	auto VoiceIt = m_ChannelVoices.find(_Channel);
	if (VoiceIt == m_ChannelVoices.end())
	{
		m_ChannelVoices.try_emplace(_Channel, SubmixVoice());
		m_ChannelVoices[_Channel].Create(ProcessingStage_Mixer, m_MasteringVoiceDetails.InputChannels);
		return m_ChannelVoices[_Channel];
	}
	else
	{
		return VoiceIt->second;
	}
}
//----------------------------------------------------------------------------------------------------------------------------------
const X3DAUDIO_HANDLE& Mixer::GetX3DAudioHandle() const
{
	return m_X3DInstance;
}
//----------------------------------------------------------------------------------------------------------------------------------
const XAUDIO2_VOICE_DETAILS& Helix::Sound::Mixer::GetMasterVoiceDetails() const
{
	return m_MasteringVoiceDetails;
}

const IXAudio2MasteringVoice * Helix::Sound::Mixer::GetMasterVoice() const
{
	return m_pMasteringVoice;
}
