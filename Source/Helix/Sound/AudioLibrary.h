//   Copyright 2020 Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef AUDIOLIBRARY_H
#define AUDIOLIBRARY_H

#include <unordered_map>
#include <xaudio2.h>
#include <memory>

//#include <x3daudio.h>

#include "hlx\src\StandardDefines.h"
#include "hlx\src\Singleton.h"
#include "hlx\src\FileStream.h"
#include "Resources\RIFF.h"

namespace Helix
{
	namespace Sound
	{
		class AudioLibrary : public hlx::Singleton<AudioLibrary>
		{
		private:
			struct WaveFormat : WAVEFORMATEX
			{
				WaveFormat() {}
				WaveFormat(PCMWAVEFORMAT& _PcmWaveFormat)
				{
					wFormatTag = _PcmWaveFormat.wf.wFormatTag;
					nChannels = _PcmWaveFormat.wf.nChannels;
					nSamplesPerSec = _PcmWaveFormat.wf.nSamplesPerSec;
					nAvgBytesPerSec = _PcmWaveFormat.wf.nAvgBytesPerSec;
					nBlockAlign = _PcmWaveFormat.wf.nBlockAlign;
					wBitsPerSample = _PcmWaveFormat.wBitsPerSample;
					cbSize = 0;
				}
			};
		public:
			struct AudioData
			{
				WAVEFORMATEX m_WaveFormat;
				hlx::bytes m_AudioBuffer;
				XAUDIO2_BUFFER MakeBuffer()
				{
					XAUDIO2_BUFFER Buffer = { 0 };
					Buffer.AudioBytes = static_cast<uint32_t>(m_AudioBuffer.size());
					Buffer.pAudioData = reinterpret_cast<BYTE*>(&m_AudioBuffer[0]);
					return Buffer;
				}
			};
			AudioLibrary();
			~AudioLibrary();

			bool GetAudioData(hlx::string _sClipName, std::shared_ptr<AudioData>& _AudioData);

		private:
			std::unordered_map<hlx::string, std::weak_ptr<AudioData>> m_AudioData;
		};
	}
}
#endif // !AUDIOLIBRARY_H