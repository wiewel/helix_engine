//   Copyright 2020 Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef SUBMIXVOICE_H
#define SUBMIXVOICE_H

#include "hlx\src\StandardDefines.h"

#include <xaudio2.h>
#include <x3daudio.h>

namespace Helix
{
	namespace Sound
	{
		class Mixer;
		// a submix voice is used in xaudio 2 to resample multiple voices into one submix voice
		class SubmixVoice
		{
		public:
			SubmixVoice();
			~SubmixVoice();
			bool Create(uint32_t _uProcessingStage, uint32_t _uInputChannels = 1u, const XAUDIO2_VOICE_SENDS* _TargetVoices = nullptr);
			void Destroy();

			const XAUDIO2_VOICE_SENDS& GetVoiceSends() const;
			void SetVolume(float _fVolume);
			const XAUDIO2_VOICE_DETAILS& GetVoiceDetails() const;
			void Apply3DSound(const X3DAUDIO_DSP_SETTINGS& _DSPSettings);
			float GetVolume() const;

			bool IsInitialized() const;
			void SetOutputVoice(const XAUDIO2_VOICE_SENDS* _pOutputVoice);

		private:
			IXAudio2SubmixVoice* m_pSubmixVoice = nullptr;
			IXAudio2Voice* m_pTargetVoice = nullptr;
			XAUDIO2_SEND_DESCRIPTOR m_SendDescriptor = { 0 };
			XAUDIO2_VOICE_SENDS m_VoiceSends = { 0 };
			XAUDIO2_VOICE_DETAILS m_VoiceDetails = { 0 };
			const Mixer* m_pMixer = nullptr;
		};

		inline bool SubmixVoice::IsInitialized() const
		{
			return m_pSubmixVoice != nullptr;
		}
	}
}
#endif // !SUBMIXVOICE_H