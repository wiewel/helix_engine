//   Copyright 2020 Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef SPEAKER_H
#define SPEAKER_H

#include "hlx\src\StandardDefines.h"
#include "hlx\src\String.h"
#include "Voice.h"
#include "SubmixVoice.h"
#include "Math\MathTypes.h"
#include "Mixer.h"
#include "Datastructures\GameObjectComponent.h"
#include "Datastructures\SyncVariable.h"

#include <xaudio2.h>
#include <x3daudio.h>
#include <unordered_map>

namespace Helix
{
	namespace Sound
	{
		class AudioLibrary;

		class Speaker : public Datastructures::IGameObjectComponent
		{
		private:
			Speaker(Datastructures::GameObject* _pParent, Mixer::ChannelGroup _TargetChannel = Mixer::ChannelGroup::SFX);

			COMPTYPENAME(Speaker, Datastructures::GameObject, 'Spkr')
			COMPDEFCONSTR(Speaker, Datastructures::GameObject, Datastructures::IGameObjectComponent)

		public:
			virtual void OnUpdate(double _fDeltaTime, double _fTotalTime) final;

			bool AddVoice(const hlx::string& _sFileName, bool _bIsLooping = false);
			void Start(const hlx::string& _sVoiceName);
			void Stop(const hlx::string& _sVoiceName);
			//bool GetVoice(hlx::string _sVoiceName, Voice& _Voice);
			void SetVolume(const float _fVolume);
			void SetVolume(const hlx::string& _sVoiceName, const float _fVolume);
			const float GetVolume() const;
			void SetIs3D(bool _bIs3D);
			bool GetIs3D() const;
			void SetDistanceFalloff(float _fDecay);

			Datastructures:: SyncComponent* GetSyncComponent() final;

			void OnDeserialize(const hlx::TextToken& _Token) final;
			bool OnSerialize(_Out_ hlx::TextToken& _Token) const final;
						
			void SetTargetChannel(Mixer::ChannelGroup _kTargetChannel);
			void Initialize();

		private:
			// deserialization constructor
			Speaker(Datastructures::GameObject* _pParent, const Helix::Datastructures::ObjectType _kRequiredObjectType, Helix::Datastructures::TComponentCallbacks _kCallbacks);

		private:
			Mixer::ChannelGroup m_TargetChannel;
			std::unordered_map<hlx::string, Voice> m_Voices;
			SubmixVoice m_SubmixVoice;
			bool m_bIs3D = true;

			// X3DAudio
			X3DAUDIO_EMITTER m_Emitter = { 0 };
			X3DAUDIO_DSP_SETTINGS m_DSPSettings = { 0 };
			std::vector<float> m_X3DAudioMatrix;

			// Helix Sound
			Mixer* m_pMixer = nullptr;
			AudioLibrary* m_pAudioLibrary = nullptr;

#ifdef HNUCLEO

			Datastructures::SyncEnum m_ChannelEnum = { {
				{ (uint32_t) Mixer::ChannelGroup::SFX , "SFX" },
				{ (uint32_t) Mixer::ChannelGroup::Speech , "Speech" },
				{ (uint32_t) Mixer::ChannelGroup::Music , "Music" },
				{ (uint32_t) Mixer::ChannelGroup::Background , "Background" },
				{ (uint32_t) Mixer::ChannelGroup::UI , "UI" },
				},(uint32_t) m_TargetChannel };

			inline const Datastructures::SyncEnum& GetChannelGroup() const {return m_ChannelEnum;}
			inline void SetChannelGroup(const Datastructures::SyncEnum& _Enum)
			{
				m_ChannelEnum = _Enum;
				Mixer::ChannelGroup kChannel = static_cast<Mixer::ChannelGroup>(_Enum.Get());
				if (kChannel != m_TargetChannel)
				{
					SetTargetChannel(kChannel);
				}
			}

			std::vector<std::string> m_VoiceNames;
			std::string m_sSelectedVoice;

			inline std::string GetText(int _iIndex) const { return m_VoiceNames[_iIndex]; }
			inline void SelectItem(int _iIndex)
			{
				m_sSelectedVoice = m_VoiceNames[_iIndex];
			}

			Datastructures::SyncButton m_RemoveVoice = { "Remove Voice", [&]()
			{
				auto it = std::remove(m_VoiceNames.begin(), m_VoiceNames.end(),m_sSelectedVoice);
				if (it != m_VoiceNames.end()) { m_VoiceNames.erase(it); }
				m_Voices.erase(STR(m_sSelectedVoice));
			} };

			bool m_bPlayVoice = false;
			inline bool GetPlayVoice() const {	return m_bPlayVoice; }
			inline void SetPlayVoice(bool _bPlaying)
			{
				m_bPlayVoice = _bPlaying;
				if (m_bPlayVoice)
				{
					Start(STR(m_sSelectedVoice));
				}
				else
				{
					Stop(STR(m_sSelectedVoice));
				}
			}

			inline float GetVoiceVolume() const 
			{
				auto it = m_Voices.find(STR(m_sSelectedVoice));
				if (it != m_Voices.end())
				{
					return it->second.GetVolume();
				}

				return 1.f;
			}
			inline void SetVoiceVolume(const float& _fVolume)
			{
				auto it = m_Voices.find(STR(m_sSelectedVoice));
				if (it != m_Voices.end())
				{
					it->second.SetVolume(_fVolume);
				}
			}

			inline const bool GetVoiceLooping() const
			{
				auto it = m_Voices.find(STR(m_sSelectedVoice));
				if (it != m_Voices.end())
				{
					return it->second.IsLooping();
				}

				return false;
			}
			inline void SetVoiceLooping(bool _bLooping)
			{
				auto it = m_Voices.find(STR(m_sSelectedVoice));
				if (it != m_Voices.end())
				{
					it->second.Loop(_bLooping);
				}
			}

			inline const std::string& GetSelectedVoiceName() const { return m_sSelectedVoice; }
			inline void SetSelectedVoiceName(const std::string& _sName)
			{
				m_sSelectedVoice = _sName;
				if (std::find(m_VoiceNames.begin(), m_VoiceNames.end(), m_sSelectedVoice) == m_VoiceNames.end())
				{
					AddVoice(STR(m_sSelectedVoice));
					auto it = m_Voices.find(STR(m_sSelectedVoice));
					if (it != m_Voices.end())
					{
						m_VoiceNames.push_back(m_sSelectedVoice);
					}
				}
			}

			SYNOBJ(PlayVoice, Speaker, bool, GetPlayVoice, SetPlayVoice) m_SyncPlayVoice = { *this, "Play" };
			SYNOBJ(SubmixVolume, Speaker, float, GetVolume, SetVolume) m_SyncSubmixVolume = { *this, "SubMix Volume" };
			SYNOBJ(VoiceVolume, Speaker, float, GetVoiceVolume, SetVoiceVolume) m_SyncVoiceVolume = { *this, "Voice Volume" };
			SYNOBJ(VoiceName, Speaker, std::string, GetSelectedVoiceName, SetSelectedVoiceName)  m_SyncSelectedVoice = { *this, "Voice" };
			SYNOBJ(VoiceLooping, Speaker, bool, GetVoiceLooping, SetVoiceLooping)  m_SyncVoiceLooping = { *this, "Voice Looping" };
			SYNOBJ(ChannelGroup, Speaker, Datastructures::SyncEnum, GetChannelGroup, SetChannelGroup) m_SyncChannelGroup = { *this, "Channel Group" };

			Datastructures::SyncList<std::string> m_LevelList = { "Voices", &m_VoiceNames, [&](int i) {return GetText(i); }, [&](int i) {SelectItem(i); } };

			Datastructures::SyncComponent m_SpeakerGroup = { this, "Speaker", 
			{ &m_SyncSelectedVoice, &m_SyncChannelGroup, &m_SyncSubmixVolume, &m_SyncPlayVoice, &m_SyncVoiceVolume, &m_SyncVoiceLooping, &m_LevelList,&m_RemoveVoice } };

#endif // !HNUCLEO

		};

		inline const float Speaker::GetVolume() const {return m_SubmixVoice.GetVolume();}
		inline void Speaker::SetIs3D(bool _bIs3D) { m_bIs3D = _bIs3D; }
		inline bool Speaker::GetIs3D() const { return m_bIs3D; }
		inline void Speaker::SetDistanceFalloff(float _fDecay)
		{
			m_Emitter.CurveDistanceScaler = _fDecay;
		}
		inline Datastructures::SyncComponent* Speaker::GetSyncComponent()
		{
#ifdef HNUCLEO
			return &m_SpeakerGroup;
#else
			return nullptr;
#endif
		}
	}
}
#endif // !SPEAKER_H