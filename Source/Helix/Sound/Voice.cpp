//   Copyright 2020 Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "Voice.h"
#include "Mixer.h"
#pragma comment(lib,"xaudio2.lib")

using namespace Helix::Sound;
//----------------------------------------------------------------------------------------------------------------------------------
Voice::Voice()
{
	m_pMixer = Mixer::Instance();
	HASSERTD(m_pMixer != nullptr, "Could not find the Sound Mixer");
	m_pAudioLibrary = AudioLibrary::Instance();
	HASSERTD(m_pAudioLibrary != nullptr, "Could not find the Audio Library");
}
//----------------------------------------------------------------------------------------------------------------------------------
Voice::~Voice()
{
	if(m_pSourceVoice != nullptr)
	{
		m_pSourceVoice->Stop();
		m_pSourceVoice->DestroyVoice();
		m_pSourceVoice = nullptr;
	}
}
//----------------------------------------------------------------------------------------------------------------------------------
Voice::Voice(Voice&& _Other) :
	m_pSourceVoice(_Other.m_pSourceVoice),
	m_pAudioData(_Other.m_pAudioData),
	m_pAudioLibrary(_Other.m_pAudioLibrary),
	m_pMixer(_Other.m_pMixer),
	m_AudioBuffer(std::move(_Other.m_AudioBuffer)),
	m_sVoiceName(std::move(_Other.m_sVoiceName))
{
	_Other.m_pSourceVoice = nullptr;
	_Other.m_pAudioData = nullptr;
	_Other.m_pAudioLibrary = nullptr;
	_Other.m_pMixer = nullptr;
}
//----------------------------------------------------------------------------------------------------------------------------------
bool Voice::Create(const hlx::string& _sFileName, const bool _bIsLooping, const XAUDIO2_VOICE_SENDS* _pVoiceSends)
{
	HRESULT Result = S_OK;
	if (m_pAudioLibrary->GetAudioData(_sFileName, m_pAudioData) == false)
	{
		return false;
	}

	if (FAILED(Result = m_pMixer->GetXAudio2()->CreateSourceVoice(&m_pSourceVoice, &m_pAudioData->m_WaveFormat, 0, 2.0f, nullptr, _pVoiceSends)))
	{
		HWARNING("Failed to create voice %s", hlx::to_string(_sFileName).c_str());
		return false;
	}
	m_AudioBuffer = m_pAudioData->MakeBuffer();
	if (_bIsLooping)
	{
		m_AudioBuffer.LoopCount = XAUDIO2_LOOP_INFINITE;
	}
	if (FAILED(Result = m_pSourceVoice->SubmitSourceBuffer(&m_AudioBuffer)))
	{
		HWARNING("Failed to submit audio buffer to voice %s:", hlx::to_string(_sFileName).c_str());
		return false;
	}

	return true;
}
//----------------------------------------------------------------------------------------------------------------------------------
bool Voice::Start()
{
	HRESULT Result = S_OK;
	
	Stop();

	if (FAILED(Result = m_pSourceVoice->FlushSourceBuffers())) 
	{
		HWARNING("Failed to flush buffers of voice %s", hlx::to_string(m_sVoiceName).c_str());
		return false;
	}

	if (FAILED(Result = m_pSourceVoice->SubmitSourceBuffer(&m_AudioBuffer)))
	{
		HWARNING("Failed to submit audio buffer to voice %s:", hlx::to_string(m_sVoiceName).c_str());
		return false;
	}

	if (FAILED(m_pSourceVoice->Start()))
	{
		HWARNING("Failed to start voice %s", hlx::to_string(m_sVoiceName).c_str());
		return false;
	}

	m_bIsPlaying = true;
	return true;
}
//----------------------------------------------------------------------------------------------------------------------------------
bool Voice::Stop()
{
	if (FAILED(m_pSourceVoice->Stop()))
	{
		HWARNING("Failed to stop voice %s", hlx::to_string(m_sVoiceName).c_str());
		return false;
	}

	m_bIsPlaying = false;
	return true;
}
//----------------------------------------------------------------------------------------------------------------------------------
void Voice::Loop(const bool _bIsLooping)
{
	HRESULT Result = S_OK;
	if (_bIsLooping)
	{
		m_AudioBuffer.LoopCount = XAUDIO2_LOOP_INFINITE;
	}
	else
	{
		m_AudioBuffer.LoopCount = 0;
	}
	if (FAILED(Result = m_pSourceVoice->SubmitSourceBuffer(&m_AudioBuffer)))
	{
		HWARNING("Failed to submit audio buffer to voice");
	}
}
//----------------------------------------------------------------------------------------------------------------------------------
void Voice::SetVolume(float _fVolume)
{
	if (m_pSourceVoice != nullptr)
	{
		m_pSourceVoice->SetVolume(_fVolume);
	}
}
//----------------------------------------------------------------------------------------------------------------------------------

const float Voice::GetVolume() const
{
	float fVolume = 1.f;
	if (m_pSourceVoice != nullptr)
	{
		m_pSourceVoice->GetVolume(&fVolume);	
	}
	return fVolume;
}
//----------------------------------------------------------------------------------------------------------------------------------

bool Voice::IsLooping() const
{
	return m_AudioBuffer.LoopCount == XAUDIO2_LOOP_INFINITE;
}
//----------------------------------------------------------------------------------------------------------------------------------
void Voice::SetOutputVoices(const XAUDIO2_VOICE_SENDS* _pVoiceSends)
{
	m_pSourceVoice->SetOutputVoices(_pVoiceSends);
}
//----------------------------------------------------------------------------------------------------------------------------------

const std::shared_ptr<AudioLibrary::AudioData>& Voice::GetData()
{
	return m_pAudioData;
}
//----------------------------------------------------------------------------------------------------------------------------------