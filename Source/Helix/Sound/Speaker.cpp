//   Copyright 2020 Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "Speaker.h"
#include "AudioLibrary.h"
#pragma comment(lib,"xaudio2.lib")

using namespace Helix::Sound;
using namespace Helix::Datastructures;
//----------------------------------------------------------------------------------------------------------------------------------
Speaker::Speaker(GameObject* _pParent, Mixer::ChannelGroup _TargetChannel)
	: IGameObjectComponent(_pParent, ComponentCallback(ComponentCallback_OnSerialize | ComponentCallback_OnUpdate))
{
	m_pMixer = Mixer::Instance();
	HASSERTD(m_pMixer != nullptr, "Could not find the Sound Mixer");

	Initialize();
	SetTargetChannel(_TargetChannel);
}
//----------------------------------------------------------------------------------------------------------------------------------
Speaker::Speaker(GameObject* _pParent,
	const ObjectType _kRequiredObjectType,
	TComponentCallbacks _kCallbacks) :
	IGameObjectComponent(_pParent, ComponentCallback(ComponentCallback_OnSerialize | ComponentCallback_OnUpdate))
{
	m_pMixer = Mixer::Instance();
	HASSERTD(m_pMixer != nullptr, "Could not find the Sound Mixer");

	Initialize();
}
//----------------------------------------------------------------------------------------------------------------------------------
Speaker::~Speaker()
{
}
//----------------------------------------------------------------------------------------------------------------------------------
bool Speaker::AddVoice(const hlx::string& _sClipName, bool _bIsLooping)
{
	m_Voices.try_emplace(_sClipName, Voice());
	if (m_Voices[_sClipName].Create(_sClipName, _bIsLooping, &m_SubmixVoice.GetVoiceSends()) == false)
	{
		m_Voices.erase(_sClipName);
		return false;
	}

	return true;
}
//----------------------------------------------------------------------------------------------------------------------------------
void Speaker::Start(const hlx::string& _sVoiceName)
{
	auto VoiceIt = m_Voices.find(_sVoiceName);
	if (VoiceIt == m_Voices.end())
	{
		HWARNING("Failed to start voice %s: Not found.", CSTR(_sVoiceName));
		return;
	}
	VoiceIt->second.Start();
}
//----------------------------------------------------------------------------------------------------------------------------------
void Speaker::Stop(const hlx::string& _sVoiceName)
{
	auto VoiceIt = m_Voices.find(_sVoiceName);
	if (VoiceIt == m_Voices.end())
	{
		HWARNING("Failed to stop voice %s: Not found.", CSTR(_sVoiceName));
		return;
	}
	VoiceIt->second.Stop();
}
//----------------------------------------------------------------------------------------------------------------------------------
void Speaker::SetVolume(const float _fVolume)
{
	m_SubmixVoice.SetVolume(_fVolume);
}
//----------------------------------------------------------------------------------------------------------------------------------
void Speaker::SetVolume(const hlx::string& _sVoiceName, const float _fVolume)
{
	auto VoiceIt = m_Voices.find(_sVoiceName);
	if (VoiceIt != m_Voices.end())
	{
		VoiceIt->second.SetVolume(_fVolume);
	}
}
//----------------------------------------------------------------------------------------------------------------------------------
void Speaker::OnUpdate(double _fDeltaTime, double _fTotalTime)
{
	m_Emitter.Position = Math::Convert(GetParent()->GetPosition());
	m_Emitter.Velocity = Math::Convert(GetParent()->GetLinearVelocity());

	const Listener* pListener = m_pMixer != nullptr ? m_pMixer->GetListener() : nullptr;
	if (pListener && m_bIs3D)
	{
		X3DAudioCalculate(
			m_pMixer->GetX3DAudioHandle(),
			&pListener->GetX3DAudioListener(),
			&m_Emitter,
			X3DAUDIO_CALCULATE_MATRIX | X3DAUDIO_CALCULATE_LPF_DIRECT, // | X3DAUDIO_CALCULATE_DOPPLER | X3DAUDIO_CALCULATE_REVERB,
			&m_DSPSettings);
		m_SubmixVoice.Apply3DSound(m_DSPSettings);
	}
}
//----------------------------------------------------------------------------------------------------------------------------------
void Speaker::OnDeserialize(const hlx::TextToken& _Token)
{
	m_pMixer = Mixer::Instance();

	m_TargetChannel = _Token.get<Mixer::ChannelGroup>("ChannelGroup", Mixer::ChannelGroup::SFX);
	SetTargetChannel(m_TargetChannel);

	float fSubMixVolume = _Token.get<float>("SubMixVolume", 1.f);
	SetVolume(fSubMixVolume);

	auto pair = _Token.getAllTokens("Voice");

	for (auto it = pair.first; it != pair.second; ++it)
	{
		const hlx::TextToken& VoiceToken = it->second;

		std::string sName = VoiceToken.getValue("Name");
		if (sName.empty())
			continue;

		float fVoiceVolume = VoiceToken.get<float>("Volume", 1.f);		
		bool bLooping = VoiceToken.get<bool>("Looping");
		bool bPlaying = VoiceToken.get<bool>("Playing");

		if (AddVoice(STR(sName), bLooping))
		{
			auto VoiceIt = m_Voices.find(STR(sName));
			if (VoiceIt != m_Voices.end())
			{
				VoiceIt->second.SetVolume(fVoiceVolume);
				if (bPlaying)
				{
					VoiceIt->second.Start();
				}
#ifdef HNUCLEO
				m_VoiceNames.push_back(sName);

				if (m_sSelectedVoice.empty())
				{
					m_sSelectedVoice = sName;
				}
#endif
			}
		}
	}
}
//----------------------------------------------------------------------------------------------------------------------------------
bool Speaker::OnSerialize(hlx::TextToken& _Token) const
{
	_Token.set("ChannelGroup", m_TargetChannel);
	_Token.set("SubMixVolume", GetVolume());

	for (const std::unordered_map<hlx::string, Voice>::value_type& KV: m_Voices)
	{
		hlx::TextToken VoiceToken("Voice");

		VoiceToken.set("Name", hlx::to_sstring(KV.first));
		VoiceToken.set("Volume", KV.second.GetVolume());
		VoiceToken.set("Looping", KV.second.IsLooping());
		VoiceToken.set("Playing", KV.second.IsPlaying());

		_Token.addToken("Voice", VoiceToken);
	}

	return true;
}
//----------------------------------------------------------------------------------------------------------------------------------
void Speaker::SetTargetChannel(Mixer::ChannelGroup _kTargetChannel)
{
	m_TargetChannel = _kTargetChannel;

	uint32_t uDeviceChannels = m_pMixer->GetMasterVoiceDetails().InputChannels;
	const SubmixVoice& TargetVoice = m_pMixer->GetChannelVoice(m_TargetChannel);
	const XAUDIO2_VOICE_SENDS* pVoiceSends = &TargetVoice.GetVoiceSends();
	m_SubmixVoice.SetOutputVoice(pVoiceSends);
	
	m_DSPSettings.SrcChannelCount = 1u;
	m_DSPSettings.DstChannelCount = TargetVoice.GetVoiceDetails().InputChannels;
	m_X3DAudioMatrix.resize(m_DSPSettings.SrcChannelCount * m_DSPSettings.DstChannelCount);
	m_DSPSettings.pMatrixCoefficients = m_X3DAudioMatrix.data();
}
//----------------------------------------------------------------------------------------------------------------------------------
// Initialize the speaker to output to the default audio device directly. To attach it to a channel in the mixer
// use SetTargetChannel afterwards. 
void Speaker::Initialize()
{
	if (m_SubmixVoice.IsInitialized())
		return;

	// All subvoices in this speaker are connected to a single submix voice.
	// This submix voice resamples to mono and receives the 3DX effect.
	m_SubmixVoice.Create(Mixer::ProcessingStage_Speaker, 1u);

	m_DSPSettings.SrcChannelCount = 1u;
	m_DSPSettings.DstChannelCount = m_pMixer->GetMasterVoiceDetails().InputChannels;
	m_X3DAudioMatrix.resize(m_DSPSettings.SrcChannelCount * m_DSPSettings.DstChannelCount);
	m_DSPSettings.pMatrixCoefficients = m_X3DAudioMatrix.data();

	m_Emitter.ChannelCount = 1u;
	m_Emitter.CurveDistanceScaler = 1.0f;
	m_Emitter.OrientFront = Math::Convert(Math::float3(0, 0, 1));
	m_Emitter.OrientTop = Math::Convert(Math::float3(0, 1, 0));
}
//----------------------------------------------------------------------------------------------------------------------------------
