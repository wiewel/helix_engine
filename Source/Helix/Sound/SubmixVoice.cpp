//   Copyright 2020 Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "SubmixVoice.h"
#include "Mixer.h"
#include "hlx\src\Logger.h"
#include "Util\Functional.h"

using namespace Helix::Sound;
//----------------------------------------------------------------------------------------------------------------------------------
SubmixVoice::SubmixVoice()
{
	m_pMixer = Mixer::Instance();
	HASSERTD(m_pMixer, "Failed to create submix voice: No mixer found");
}
//----------------------------------------------------------------------------------------------------------------------------------
SubmixVoice::~SubmixVoice()
{
	Destroy();
}
//----------------------------------------------------------------------------------------------------------------------------------
// the ProcessingStage defines in which order voices are processed. the higher the later
bool SubmixVoice::Create(uint32_t _uProcessingStage, uint32_t _uInputChannels, const XAUDIO2_VOICE_SENDS* _pTargetVoices)
{
	// create the voice
	HRESULT Result = S_OK;
	if (FAILED(Result = m_pMixer->GetXAudio2()->CreateSubmixVoice(&m_pSubmixVoice, _uInputChannels, 44100u, 0u, _uProcessingStage, _pTargetVoices)))
	{
		HWARNING("Failed to create submix voice");
		return false;
	}
	// get the target voice from the voice sends structure
	if (_pTargetVoices != nullptr)
	{
		m_pTargetVoice = _pTargetVoices->pSends->pOutputVoice;
	}
	// populate the descriptors to allow source voices beeing attached to the submix voice
	m_SendDescriptor.pOutputVoice = m_pSubmixVoice;
	m_SendDescriptor.Flags = 0u;
	m_VoiceSends.pSends = &m_SendDescriptor;
	m_VoiceSends.SendCount = 1;
	// fetch the parameters of the voice. inputchannels, sample rate etc.
	m_pSubmixVoice->GetVoiceDetails(&m_VoiceDetails);
	return true;
}
//----------------------------------------------------------------------------------------------------------------------------------
void SubmixVoice::Destroy()
{
	if (m_pSubmixVoice != nullptr)
	{
		m_pSubmixVoice->DestroyVoice();
		m_pSubmixVoice = nullptr;
	}
}
//----------------------------------------------------------------------------------------------------------------------------------
const XAUDIO2_VOICE_SENDS& SubmixVoice::GetVoiceSends() const
{
	return m_VoiceSends;
}
//----------------------------------------------------------------------------------------------------------------------------------
void SubmixVoice::SetVolume(float _fVolume)
{
	if (m_pSubmixVoice != nullptr)
	{
		m_pSubmixVoice->SetVolume(_fVolume);
	}
}
//----------------------------------------------------------------------------------------------------------------------------------
const XAUDIO2_VOICE_DETAILS& SubmixVoice::GetVoiceDetails() const
{
	return m_VoiceDetails;
}
//----------------------------------------------------------------------------------------------------------------------------------
void SubmixVoice::Apply3DSound(const X3DAUDIO_DSP_SETTINGS& _DSPSettings)
{
	if (m_pSubmixVoice != nullptr)
	{
		m_pSubmixVoice->SetOutputMatrix(m_pTargetVoice, _DSPSettings.SrcChannelCount, _DSPSettings.DstChannelCount, _DSPSettings.pMatrixCoefficients);
		XAUDIO2_FILTER_PARAMETERS FilterParameters = { LowPassFilter, 2.0f * sinf(X3DAUDIO_PI / 6.0f * _DSPSettings.LPFDirectCoefficient), 1.0f };
		m_pSubmixVoice->SetFilterParameters(&FilterParameters);
	}
}
//----------------------------------------------------------------------------------------------------------------------------------
float SubmixVoice::GetVolume() const
{
	float fVolume = 1.f;
	if (m_pSubmixVoice != nullptr)
	{
		m_pSubmixVoice->GetVolume(&fVolume);
	}
	return fVolume;
}
//----------------------------------------------------------------------------------------------------------------------------------
void SubmixVoice::SetOutputVoice(const XAUDIO2_VOICE_SENDS* _pOutputVoice)
{
	if (m_pSubmixVoice != nullptr)
	{
		m_pSubmixVoice->SetOutputVoices(_pOutputVoice);
	}
}
//----------------------------------------------------------------------------------------------------------------------------------
