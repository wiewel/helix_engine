//   Copyright 2020 Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "AudioLibrary.h"
#include <vector>
#include "Resources\FileSystem.h"
#include "hlx\src\Logger.h"
#pragma comment(lib,"xaudio2.lib")

using namespace Helix::Sound;

//----------------------------------------------------------------------------------------------------------------------------------
AudioLibrary::AudioLibrary()
{
}
//----------------------------------------------------------------------------------------------------------------------------------
AudioLibrary::~AudioLibrary()
{
}
//----------------------------------------------------------------------------------------------------------------------------------
bool AudioLibrary::GetAudioData(hlx::string _sClipName, std::shared_ptr<AudioData>& _AudioData)
{
	auto DataIt = m_AudioData.find(_sClipName);

	// cache miss -> load from file
	if (DataIt == m_AudioData.end() || DataIt->second.expired())
	{
		// load audio file
		hlx::fbytestream FileStream;
		if (Resources::FileSystem::Instance()->OpenFileStream(Resources::HelixDirectories_Sounds, hlx::to_string(_sClipName), std::ios::in | std::ios::binary, FileStream) == false)
		{
			return false;
		}

		// Create and read the WAV file
		Resources::RIFFFile File;
		File.Read(FileStream);
		FileStream.close();

		// Add the buffer to the output structure
		_AudioData = std::shared_ptr<AudioData>(new AudioData());
		PCMWAVEFORMAT PcmWaveFormat = File.Find(Resources::RIFFFile::FourCC::fmt).as<PCMWAVEFORMAT>();
		_AudioData->m_WaveFormat = WaveFormat(PcmWaveFormat);
		_AudioData->m_AudioBuffer = File.Find(Resources::RIFFFile::FourCC::data);

		// Add the data to the cache. The buffer is deleted after all users of the audio data released their shared pointers.
		std::weak_ptr<AudioData> pAudioData = std::weak_ptr<AudioData>(_AudioData);
		m_AudioData[_sClipName] = pAudioData;

		return true;
	}
	
	// the buffer is in cache
	if (_AudioData = DataIt->second.lock())
	{
		return true;
	}

	// something went wrong while locking
	return false;
}
