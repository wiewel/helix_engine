//   Copyright 2020 Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef MIXER_H
#define MIXER_H

#include "hlx\src\StandardDefines.h"
#include "hlx\src\Singleton.h"
#include "Listener.h"
#include "SubmixVoice.h"

#include <x3daudio.h>
#include <xaudio2.h>
#include <memory>
#include <unordered_map>

namespace Helix
{
	namespace Sound
	{
		class Mixer : public hlx::Singleton<Mixer>
		{
		public:
			enum DeviceChannels : uint32_t
			{
				DeviceChannels_None = 0u,
				DeviceChannels_Mono = 1u,
				DeviceChannels_Stereo = 2u,
				DeviceChannels_5_1 = 5u,
				DeviceChannels_7_1 = 7u,
				DeviceChannels_9_1 = 9u,
				DeviceChannels_Max = 9u,
			};
			enum ProcessingStage : uint32_t
			{
				ProcessingStage_None,
				ProcessingStage_Source,
				ProcessingStage_Speaker,
				ProcessingStage_Mixer,
			};
			enum class ChannelGroup : uint32_t
			{
				// typically at speaker
				SFX,
				Speech,
				// typically at listener
				Music,
				Background,
				UI,
			};
		public:
			Mixer();
			~Mixer();

			void SetVolume(float _fMasterVolume);
			IXAudio2* GetXAudio2() const;
			void SetListener(Listener* _pListener);
			const Listener* GetListener() const;
			const SubmixVoice& GetChannelVoice(const ChannelGroup& _Channel);
			const X3DAUDIO_HANDLE& GetX3DAudioHandle() const; 
			const XAUDIO2_VOICE_DETAILS& GetMasterVoiceDetails() const;
			const IXAudio2MasteringVoice* GetMasterVoice() const;

		private:
			std::unordered_map<ChannelGroup, SubmixVoice> m_ChannelVoices;
			Listener* m_pListener = nullptr;
			IXAudio2* m_pXAudio2 = nullptr;
			IXAudio2MasteringVoice* m_pMasteringVoice = nullptr;
			X3DAUDIO_HANDLE m_X3DInstance;
			XAUDIO2_VOICE_DETAILS m_MasteringVoiceDetails = {0};
		};
	}
}

#endif // !MIXER_H