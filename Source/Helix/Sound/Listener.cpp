//   Copyright 2020 Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "Listener.h"
#include "Mixer.h"

using namespace Helix;
using namespace Sound;

//----------------------------------------------------------------------------------------------------------------------------------
Listener::Listener(Datastructures::GameObject* _pParent) :
	Datastructures::IGameObjectComponent(_pParent, Datastructures::ComponentCallback_OnUpdate)
{
	Mixer::Instance()->SetListener(this);
	m_Listener.pCone = nullptr;
	m_Listener.OrientFront = Math::Convert(Math::float3(0, 0, 1));
	m_Listener.OrientTop = Math::Convert(Math::float3(0, 1, 0));
	m_Listener.Position = Math::Convert(Math::float3(0, 0, -1));
	m_Listener.Velocity = Math::Convert(Math::float3(0, 0, 0));
}
//----------------------------------------------------------------------------------------------------------------------------------
Listener::~Listener()
{
}
//----------------------------------------------------------------------------------------------------------------------------------
void Helix::Sound::Listener::SetPosition(Math::float3 _Position)
{
	m_Listener.Position = Math::Convert(_Position);
}
//----------------------------------------------------------------------------------------------------------------------------------
void Listener::OnUpdate(double _fDeltaTime, double _fElapsedTime)
{
	SetPosition(GetParent()->GetPosition());
	m_Listener.OrientFront = Math::Convert(GetParent()->GetOrientation() * Math::float3(0, 0, 1));
}
//----------------------------------------------------------------------------------------------------------------------------------
const X3DAUDIO_LISTENER& Listener::GetX3DAudioListener() const
{
	return m_Listener;
}
