//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "Level.h"
#include "hlx\src\Logger.h"
#include "hlx\src\StandardDefines.h"
#include "Resources\FileSystem.h"
#include "LightComponent.h"
#include "LightContext.h"
#include "Resources\GameObjectSerializer.h"
#include "Resources\HMATFile.h"
#include "Resources\HMATPROPFile.h"

#include "ScriptScene.h"
#include "ComponentScene.h"

using namespace Helix;
using namespace Helix::Resources;
using namespace Helix::Scene;
using namespace Helix::Datastructures;
using namespace Helix::Display;

//---------------------------------------------------------------------------------------------------------------------
Level::Level() 
{
}
//---------------------------------------------------------------------------------------------------------------------
Level::~Level()
{
	Unload();
}
//---------------------------------------------------------------------------------------------------------------------
bool Level::Load(const hlx::string& _sLevelName, bool _bAddToScene, bool _bLoadStaticAsKinematic)
{
	if (_sLevelName.empty() == false)
	{
		m_sName = _sLevelName;
	}

	hlx::string sFile = m_sName;

	if (hlx::ends_with(sFile, S(".hell")) == false)
	{
		sFile += S(".hell");
	}

	hlx::TextToken LevelToken;

	hlx::fbytestream HellFileStream;
	if (FileSystem::Instance()->OpenFileStream(Resources::HelixDirectories_Levels, sFile, std::ios::in | std::ios::binary, HellFileStream))
	{
		m_sFilePath = HellFileStream.GetFileName();

		std::unique_ptr<hlx::bytestream> Stream = HellFileStream.make_bytestream();
		HellFileStream.close();
		
		LevelToken.Initialize(*Stream, "Level", false);
		if(LevelToken.IsEmpty())
		{
			HERROR("Failed to load level %s", _sLevelName.c_str());
			return false;
		}
	}
	else
	{
		return false;
	}

	// General Settings
	LightContext::Instance()->SetAmbientColor(to(LevelToken.getVector<float, 3>("AmbientColor")));
	LightContext::Instance()->SetEnvironmentMap(LevelToken.getValue("EnvironmentMap"));
	LightContext::Instance()->SetSkybox(LevelToken.getValue("SkyBox"));

	GameObjectPool* pPool = GameObjectPool::Instance();

	auto ObjectsPair = LevelToken.getAllTokens("Object");

	size_t uObjectCount = std::distance(ObjectsPair.first, ObjectsPair.second);

	// allocate all needed objects at once
	std::vector<GameObject*> Objects = pPool->Create(static_cast<uint32_t>(uObjectCount)).get();

	if (Objects.size() != uObjectCount)
	{
		HERROR("Unable to allocate all GameObjects");

		for (GameObject* pGO : Objects)
		{
			pGO->Destroy();
		}

		return false;
	}

	size_t uCurObj = 0u;

	for (auto it = ObjectsPair.first; it != ObjectsPair.second; ++it)
	{
		const hlx::TextToken& GOToken = it->second;
		GameObjectDesc Desc = GameObjectSerializer::Deserialize(GOToken);

		if (Desc.kType != GameObjectType_RenderObject)
		{			
			GameObject* pGO = Objects[uCurObj++];

			if (_bLoadStaticAsKinematic && Desc.kType == GameObjectType_RigidStatic)
			{
				Desc.kType = GameObjectType_RigidDynamic;
				Desc.kFlags |= GameObjectFlag_Kinematic;
			}

			pGO->Initialize(Desc);
			m_GameObjects.push_back(pGO);

			if (_bAddToScene)
			{
				pGO->AddToScene();
			}

			// load components
			auto ComponentPair = GOToken.getAllTokens("Component");
			for (auto cit = ComponentPair.first; cit != ComponentPair.second; ++cit)
			{
				if (pGO->DeserializeComponent(cit->second))
				{
					if (m_bHasLight == false && cit->second.getValue("TypeName") == "LightComponent")
					{
						m_bHasLight = true;
					}
				}
			}

#ifdef HNUCLEO
			for (IComponent* pComponent : pGO->GetComponents())
			{
				pGO->AddSyncComponent(pComponent->GetSyncComponent());
			}
#endif

			if (_bAddToScene && Desc.kType == GameObjectType_RigidDynamic && Desc.kFlags ^ GameObjectFlag_Kinematic)
			{
				pGO->WakeUp();
			}
		}
		else // renderobjects
		{
			RenderObjectDX11* pRO = new RenderObjectDX11;
			pRO->Initialize(Desc);
			m_RenderObjects.push_back(pRO);
		}
	}


	HLOG("Level \"%s\" loaded with %u GameObjects %u RenderObjects!",
		m_sName.c_str(), static_cast<uint32_t>(m_GameObjects.size()), static_cast<uint32_t>(m_RenderObjects.size()));
	
	HNCL(m_LevelDock.Bind());

	ComponentScene::Instance()->LevelLoaded();
	ScriptScene::Instance()->LevelLoaded();

	return true;
}
//---------------------------------------------------------------------------------------------------------------------

bool Level::Save(const hlx::string& _sLevelFilePath)
{
	m_sFilePath = _sLevelFilePath;

	int iCur = 0;
	int iMax = static_cast<int>(m_GameObjects.size()) + static_cast<int>(m_RenderObjects.size());

	if (iMax == 0)
		return false;

	hlx::string sHMatDir = FileSystem::Instance()->GetKnownDirectoryPath(HelixDirectories_Materials);

	using TMatMap = std::unordered_map<uint64_t, Display::MaterialDX11>;
	using TPropMap = std::unordered_map<uint64_t, Resources::HMatProps>;

	TMatMap MaterialsToExport;
	TPropMap PropertiesToExport;

	uint64_t kDefaultPasses = MaterialPoolDX11::Instance()->GetDefaultRenderPasses();

	m_ProgressFunc(S("Exporing GameObjects"), iCur, 0, iMax);

	// export gameobject descriptions into texttoken
	{
		hlx::TextToken LevelToken("Level");

		LightContext* pLightContext = LightContext::Instance();

		// set general settings
		if (pLightContext->GetEnvironmentMap())
		{
			LevelToken.set("EnvironmentMap", pLightContext->GetEnvironmentMap().GetDescription().m_sFilePathOrName);
		}
		if (pLightContext->GetSkybox())
		{
			LevelToken.set("SkyBox", pLightContext->GetSkybox().GetDescription().m_sFilePathOrName);
		}
		LevelToken.setVector("AmbientColor", to(pLightContext->GetAmbientColor()));

		auto serialize = [&](const auto* pObj) -> void
		{
			m_ProgressFunc(STR("Exporing " + pObj->GetName()), iCur++, 0, iMax);

			// get active (unique) materials, to avoid overwriting/exporting the same material several times
			for (const MaterialDX11& Material : pObj->GetMaterials())
			{
				if (Material && Material.IsDefaultReference() == false)
				{
					HMatProps Props;
					const MaterialProperties DXMatProps = Material.GetProperties();

					Props.sHMatName = FileSystem::GetShortName(Material.GetPathOrName());
					Props.sSubMeshName = DXMatProps.sAssociatedSubMesh;

					Props.vColorEmissive = DXMatProps.vEmissiveColor;
					Props.EmissiveMap.vOffset = Math::float4(DXMatProps.vEmissiveOffset, 0.0f);
					Props.EmissiveMap.vScale = Math::float4(DXMatProps.vEmissiveScale, 0.0f);
					Props.vColorAlbedo = DXMatProps.vAlbedo;
					Props.bUseDefaultPasses = DXMatProps.kRenderPasses == kDefaultPasses;
					Props.AlbedoMap.bEnabled = DXMatProps.bUseAlbedoMap;
					Props.NormalMap.bEnabled = DXMatProps.bUseNormalMap;
					Props.HeightMap.bEnabled = DXMatProps.bUseHeightMap;
					Props.RoughnessMap.bEnabled = DXMatProps.bUseRoughnessMap;
					Props.MetallicMap.bEnabled = DXMatProps.bUseMetallicMap;
					Props.EmissiveMap.bEnabled = false; // not implemented
					Props.RoughnessMap.vScale = { DXMatProps.fRoughnessScale, DXMatProps.fRoughnessScale, DXMatProps.fRoughnessScale, DXMatProps.fRoughnessScale };
					Props.RoughnessMap.vOffset = { DXMatProps.fRoughnessOffset,DXMatProps.fRoughnessOffset, DXMatProps.fRoughnessOffset, DXMatProps.fRoughnessOffset };
					Props.fRoughness = DXMatProps.fRoughness;
					Props.fMetallic = DXMatProps.fMetallic;
					Props.fHorizonFade = DXMatProps.fHorizonFade;
					Props.vTextureTiling = DXMatProps.vTextureTiling;

					uint64_t uExportHash = Props.Hash();
					Material.SetExportHash(uExportHash);

					if (MaterialsToExport.count(Material.GetKey()) == 0)
					{
						++iMax;
						MaterialsToExport.insert({ Material.GetKey(), Material });
					}

					if (PropertiesToExport.count(uExportHash) == 0)
					{
						++iMax;
						PropertiesToExport.insert({ uExportHash, Props });
					}
				}
			}

			hlx::TextToken GOToken = GameObjectSerializer::Serialize(pObj->CreateDescription());

			// serialize components
			for (IComponent* pComponent : pObj->GetComponents())
			{
				if (pComponent->GetActiveCallbacks() & ComponentCallback_OnSerialize)
				{
					hlx::TextToken WrapperToken("Component");
					WrapperToken.set("TypeID", pComponent->GetTypeID());
					WrapperToken.set("TypeName", pComponent->GetTypeName());
					WrapperToken.set("RequiredObjectType", pComponent->GetRequiredObjectType());
					WrapperToken.set("Callbacks", pComponent->GetActiveCallbacks().GetFlag());

					hlx::TextToken ComponentToken(pComponent->GetTypeName());
					if (pComponent->OnSerialize(ComponentToken))
					{
						WrapperToken.addToken(pComponent->GetTypeName(), ComponentToken);
						GOToken.addToken("Component", WrapperToken);
					}
				}
			}

			LevelToken.addToken("GameObject", GOToken);
		};

		// serialize GOs
		for (const GameObject* pObj : m_GameObjects)
		{
			serialize(pObj);
		}

		// serialize ROs
		for (const RenderObjectDX11* pObj : m_RenderObjects)
		{
			serialize(pObj);
		}

		// serialize tokens to file
		if (LevelToken.serialize(_sLevelFilePath) == false)
		{
			HERROR("Failed to serialize level token %s", _sLevelFilePath.c_str());
		}
	}

	// Export used materials
	for (const TMatMap::value_type& KV : MaterialsToExport)
	{
		const MaterialDX11& DMat = KV.second;
		m_ProgressFunc(STR("Exporing " + DMat.GetPathOrName()), iCur++, 0, iMax);

		// convert display materials to resource Materials
		const MaterialProperties& Props = DMat.GetProperties();

		HMat RMat;
		RMat.sName = FileSystem::GetShortName(DMat.GetPathOrName());
		RMat.sMapAlbedo = DMat.GetAlbedoMap() ? DMat.GetAlbedoMap().GetDescription().m_sFilePathOrName : "";
		RMat.sMapNormal = DMat.GetNormalMap() ? DMat.GetNormalMap().GetDescription().m_sFilePathOrName : "";
		RMat.sMapHeight = DMat.GetHeightMap() ? DMat.GetHeightMap().GetDescription().m_sFilePathOrName : "";
		RMat.sMapRoughness = DMat.GetRoughnessMap() ? DMat.GetRoughnessMap().GetDescription().m_sFilePathOrName : "";
		RMat.sMapMetallic = DMat.GetMetallicMap() ? DMat.GetMetallicMap().GetDescription().m_sFilePathOrName : "";
		RMat.sMapEmissive = DMat.GetEmissiveMap() ? DMat.GetEmissiveMap().GetDescription().m_sFilePathOrName : "";

		HMATFile HMat(RMat);

		hlx::bytes OutBuffer;
		hlx::bytestream OutStream(OutBuffer);
		HMat.Write(OutStream);

		hlx::string sOutPath = sHMatDir + STR(RMat.sName + ".hmat");

		hlx::fbytestream FileStream(sOutPath, std::ios::binary | std::ios::out);
		if (FileStream.is_open() == false)
		{
			HERROR("Failed to open %s for writing!", sOutPath.c_str());
			continue;
		}

		FileStream.put(OutBuffer);
		FileStream.close();
	}

	// Export associated material properties
	for (const TPropMap::value_type& kv : PropertiesToExport)
	{
		HMATPROPFile HMatProp(kv.second);

		hlx::bytes OutBuffer;
		hlx::bytestream OutStream(OutBuffer);
		HMatProp.Write(OutStream);

		hlx::string sOutPath = sHMatDir + STR(kv.second.sHMatName + "_" + std::to_string(kv.first) + ".hmatprop");

		hlx::fbytestream FileStream(sOutPath, std::ios::binary | std::ios::out);
		if (FileStream.is_open() == false)
		{
			HERROR("Failed to open %s for writing!", sOutPath.c_str());
			continue;
		}

		FileStream.put(OutBuffer);
		FileStream.close();
	}

	HLOG("Exported %u Materials and %u Properties",
		static_cast<uint32_t>(MaterialsToExport.size()), static_cast<uint32_t>(PropertiesToExport.size()));

	m_ProgressFunc(S("Exporing done!"), iMax, 0, iMax);
	return false;
}
//---------------------------------------------------------------------------------------------------------------------
void Level::Unload()
{
	HNCL(m_LevelDock.Unbind());

	m_bHasLight = false;

	for (GameObject* pGO : m_GameObjects)
	{
		// this also destroys attached components
		pGO->Destroy();
	}

	for (RenderObjectDX11* pRO : m_RenderObjects)
	{
		delete pRO;	
	}

	m_GameObjects.clear();
	m_RenderObjects.clear();
}
//---------------------------------------------------------------------------------------------------------------------
GameObject* Level::SpawnBox(const Math::transform& _Transform, bool _bKinematic, bool _bBindToNucleo)
{
	MaterialProperties MatProp;
	MatProp.kRenderPasses = Display::MaterialPoolDX11::Instance()->GetDefaultRenderPasses();
	MatProp.vAlbedo = { 1.022f, 0.782f, 0.344f };

	MatProp.fRoughness = 0.5f;
	MatProp.fMetallic = 1.0f;
	MatProp.fHorizonFade = 0.0f;

	GameObject* pGO = GameObjectPool::Instance()->Create().get();

	if (pGO == nullptr)
		return nullptr;

	++m_uObjectCount;

	pGO->SetName("NewObject" + std::to_string(m_uObjectCount));
	pGO->SetMesh(DefaultInit);
	MatProp.sAssociatedSubMesh = pGO->GetMesh().GetSubMeshes().front().m_sSubName;
	pGO->CreatePhysicsActorDynamic(_Transform, 10.f, _bKinematic);

	Physics::GeometryDesc Shape;
	Shape.kType = Physics::GeometryType_Box;
	Shape.kUsage = Physics::ShapeUsageType_Query;
	Shape.Box.vHalfExtents = { 0.5f, 0.5f,0.5f };

	pGO->AddShape(Shape);

	Shape.kUsage = Physics::ShapeUsageType_Simulation;
	pGO->AddShape(Shape);

#ifdef HNUCLEO
	for (IComponent* pComponent : pGO->GetComponents())
	{
		pGO->AddSyncComponent(pComponent->GetSyncComponent());
	}
#endif

	pGO->AddToScene();

	MaterialDesc MatDesc(MatProp, "NewObjectMat" + std::to_string(m_uObjectCount), false, false);
	MaterialDX11 Mat(MatDesc);

	pGO->SetMaterial(Mat);

	if (_bBindToNucleo)
	{
		pGO->BindToNucleo();
	}

	m_GameObjects.push_back(pGO);

	return pGO;
}
//---------------------------------------------------------------------------------------------------------------------

void Level::RemoveGameObject(Datastructures::GameObject* _pObject)
{
	// todo: lock access
	// reverse search be cause it is more likely that the object was recently added
	auto it = std::find(m_GameObjects.rbegin(), m_GameObjects.rend(), _pObject);

	if (it != m_GameObjects.rend())
	{
		m_GameObjects.erase(it.base() - 1);
	}
}
//---------------------------------------------------------------------------------------------------------------------
