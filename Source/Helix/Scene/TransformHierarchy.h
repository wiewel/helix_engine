//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef TRANSFORMHIERARCHY_H
#define TRANSFORMHIERARCHY_H

#include "hlx\src\Singleton.h"
#include "Math\MathTypes.h"
#include "Async\SpinLock.h"
#include "hlx\src\Logger.h"

#include <unordered_map>

namespace Helix
{
	namespace Datastructures
	{
		class TransformHierarchyComponent;
	}
	namespace Scene
	{
		//===================================================================================================
		struct TransformNode
		{
			friend class TransformHierarchy;

		private:
			// every node with no parent is considered a root node
			Datastructures::TransformHierarchyComponent* pParent = nullptr;
			// the matrix combines all parent transforms up to this node
			Math::float4x4 TransformMatrix;
			// transformation matrix needs to be recalculated for all nodes that are dirty
			bool bIsDirty = false;
			mutable Async::SpinLock TransformMatrixLock;
		
		public:
			Math::transform GetTransform() const;
		};
		//---------------------------------------------------------------------------------------------------
		inline Math::transform TransformNode::GetTransform() const
		{
			HASSERTD(bIsDirty == false, "The obtained transform is invalid! Access to the transform nodes should only happen in component update");
			Async::ScopedSpinLock Lock(TransformMatrixLock);
			return Math::transform(TransformMatrix);
		}

		//===================================================================================================
		class TransformHierarchy : public hlx::Singleton<TransformHierarchy>
		{
			friend struct TransformNode;
			using HierarchyMap = std::unordered_map<Datastructures::TransformHierarchyComponent*, TransformNode>;

		public:
			TransformHierarchy();
			~TransformHierarchy();

			void RegisterComponent(Datastructures::TransformHierarchyComponent* _pComponent);
			void RemoveComponent(Datastructures::TransformHierarchyComponent* _pComponent);

			void PreComponentUpdate();

		private:
			void TraverseUp(TransformNode* _CurrentNode);

			HierarchyMap m_Hierarchy;
		};
	}
}

#endif // !TRANSFORMHIERARCHY_H
