//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef LIGHTCOMPONENT_H
#define LIGHTCOMPONENT_H

#include "Datastructures\GameObjectComponent.h"
#include "Math\MathFunctions.h"
#include "LightDefines.h"
#include "Datastructures\SyncVariable.h"
#include "Util\UniqueID.h"
#include "CameraComponent.h"

namespace Helix
{
	namespace Scene
	{
		class CameraComponent;

		class LightComponent : public Datastructures::IGameObjectComponent
		{
			static UniqueID<uint32_t> kLightID;

			COMPTYPENAME(LightComponent, Datastructures::GameObject, 'Lit');
			COMPDEFCONSTR(LightComponent, Datastructures::GameObject, Datastructures::IGameObjectComponent)

		private:
			// nucleo constructor
			LightComponent(Datastructures::GameObject* _pParent, LightType _kType);

			// deserialization constructor
			LightComponent(
				Datastructures::GameObject* _pParent,
				const Datastructures::ObjectType _kRequiredObjectType,
				Datastructures::TComponentCallbacks _kCallbacks);

		public:
			void OnDeserialize(const hlx::TextToken& _Token) final;
			// user needs to return true if data was serialized into the token
			bool OnSerialize(_Out_ hlx::TextToken& _Token) const final;

			const bool& GetEnabled() const;
			void SetEnabled(bool _bEnabled);

			const bool& GetShadowCaster() const;
			void SetShadowCaster(bool _bShadowCaster);

			Math::float3 GetColor() const;
			void SetColor(const Math::float3& _vColor);

			const float& GetRange() const;
			// Attention: this is quite expensive to call because it triggers a shape rebuild!
			void SetRange(const float& _fRange);

			// in degrees
			const float GetAngle() const;
			void SetAngle(const float& _fAngleInDegree);

			const float& GetIntensity() const;
			void SetIntensity(const float& _fIntensity);

			const float& GetDecayStart() const;
			void SetDecayStart(const float& _fDecayStart);

			LightType GetType() const;
			void SetType(LightType _kType);

			Math::float3 GetDirection() const;

			const ShadowCasterProperties& GetShadowProperties() const;
			void SetShadowProperties(const ShadowCasterProperties& _Props);

			__declspec(property(get = GetType, put = SetType)) LightType Type;
			__declspec(property(get = GetAngle, put = SetAngle)) float Angle;
			__declspec(property(get = GetDirection)) Math::float3 Direction;
			__declspec(property(get = GetEnabled, put = SetEnabled)) bool Enabled;
			__declspec(property(get = GetShadowCaster, put = SetShadowCaster)) bool ShadowCaster;
			__declspec(property(get = GetColor, put = SetColor)) Math::float3 Color;
			__declspec(property(get = GetRange, put = SetRange)) float Range;
			__declspec(property(get = GetIntensity, put = SetIntensity)) float Intensity;
			__declspec(property(get = GetDecayStart, put = SetDecayStart)) float DecayStart;
			__declspec(property(get = GetShadowProperties, put = SetShadowProperties)) ShadowCasterProperties ShadowProperties;

			Datastructures::SyncComponent* GetSyncComponent() final;

			LightDesc CreateDescription() const;

			bool IsShadowCamera(const CameraComponent* _pCamera) const;

		private:
			void UpdateShadowCamera();

		private:

			CameraComponent* m_pShadowCamera = nullptr;

			LightType m_kType = LightType_Point;
			bool m_bEnabled = true;
			bool m_bCastShadows = false;
			Math::float3 m_vColor = HFLOAT3_ONE;
			float m_fAngle = Math::Deg2Rad(30.f); // in radians
			float m_fRange = 10.f;
			float m_fIntensity = 0.5f;
			float m_fDecayStart = 0.1f; // Radius
			ShadowCasterProperties m_ShadowProps;

#ifdef HNUCLEO
			SYNOBJ(Enabled, LightComponent, bool, GetEnabled, SetEnabled) m_SyncEnabled = { *this, "Enabled" };
			SYNOBJ(CastShadows, LightComponent, bool, GetShadowCaster, SetShadowCaster) m_SyncCastShadows = { *this, "Cast Shadows" };

			Datastructures::SyncVariable<Math::float3> m_SyncColor = { m_vColor, "Color" };
			Datastructures::SyncVariable<float> m_SyncIntensity = { m_fIntensity, "Intensity" };
			Datastructures::SyncVariable<float> m_SyncDecayStart = { m_fDecayStart, "Decay Start" };

			SYNOBJ(Angle, LightComponent, float, GetAngle, SetAngle) m_SyncAngle = { *this, "Angle" };
			SYNOBJ(Range, LightComponent, float, GetRange, SetRange) m_SyncRange = { *this, "Range" };

			Datastructures::SyncEnum m_LightEnum = { {
				{ LightType_Directional , "Directional"},
				{ LightType_Point , "Point"},
				{ LightType_Spot , "Spot"},
				},m_kType };

			inline const Datastructures::SyncEnum& GetLightType() const { return m_LightEnum; }
			inline void SetLightType(const Datastructures::SyncEnum& _Enum)
			{
				m_LightEnum = _Enum;
				m_kType = static_cast<LightType>(_Enum.Get());
			}

			SYNOBJ(LightEnum, LightComponent, Datastructures::SyncEnum, GetLightType, SetLightType) m_SyncLightType = {*this, "Type"};

			Datastructures::SyncComponent m_LightGroup = { this, "Light",
			{ &m_SyncLightType, &m_SyncEnabled, &m_SyncCastShadows, &m_SyncColor, &m_SyncIntensity, &m_SyncDecayStart, &m_SyncAngle, &m_SyncRange } };
#endif
		};

		inline Datastructures::SyncComponent* LightComponent::GetSyncComponent()
		{
#ifdef HNUCLEO
			return &m_LightGroup;
#else
			return nullptr;
#endif
		}

		inline bool LightComponent::IsShadowCamera(const CameraComponent* _pCamera) const
		{
			if(m_pShadowCamera != nullptr && _pCamera == m_pShadowCamera)
			{
				return true;
			}
			return false;
		}

		inline const bool& LightComponent::GetEnabled() const { return m_bEnabled; }
		inline void LightComponent::SetEnabled(bool _bEnabled)
		{
			m_bEnabled = _bEnabled;
			UpdateShadowCamera();
		}

		inline const bool& LightComponent::GetShadowCaster() const { return m_bCastShadows; }
		inline void LightComponent::SetShadowCaster(bool _bShadowCaster)
		{
			m_bCastShadows = _bShadowCaster;
			UpdateShadowCamera();
		}

		inline const float LightComponent::GetAngle() const{return Math::Rad2Deg(m_fAngle);}

		inline void LightComponent::SetAngle(const float& _fAngleInDegree)
		{
			m_fAngle = Math::Deg2Rad(_fAngleInDegree);
			UpdateShadowCamera();
		}

		inline const float& LightComponent::GetRange() const { return m_fRange; }
		inline void LightComponent::SetRange(const float& _fRange)
		{
			m_fRange = _fRange;
			UpdateShadowCamera();
		}

		inline const float& LightComponent::GetIntensity() const { return m_fIntensity; }
		inline void LightComponent::SetIntensity(const float& _fIntensity) { m_fIntensity = _fIntensity; }

		inline const float& LightComponent::GetDecayStart() const{return m_fDecayStart;}
		inline void LightComponent::SetDecayStart(const float& _fDecayStart){m_fDecayStart = _fDecayStart;}

		inline LightType LightComponent::GetType() const{return m_kType;}
		inline void LightComponent::SetType(LightType _kType)
		{
			m_kType = _kType;
			UpdateShadowCamera();
		}

		inline Math::float3 LightComponent::GetColor() const { return m_vColor * m_fIntensity; }
		inline void LightComponent::SetColor(const Math::float3 & _vColor) { m_vColor = _vColor; }

		inline Math::float3 LightComponent::GetDirection() const { return GetParent()->GetTransform().q.rotate({0.f, 0.f, 1.f})/*.getNormalized()*/; }

		inline const ShadowCasterProperties& LightComponent::GetShadowProperties() const { return m_ShadowProps; }
		inline void LightComponent::SetShadowProperties(const ShadowCasterProperties& _Props) { m_ShadowProps = _Props; }
		//---------------------------------------------------------------------------------------------------
	} // scene

} // Helix

#endif // LIGHTCOMPONENT_H
