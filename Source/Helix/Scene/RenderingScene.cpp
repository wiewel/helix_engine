//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "RenderingScene.h"
#include "Util\AdvancedProfiler.h"
#include "CameraComponent.h"
#include "PhysicsScene.h"
#include "ScriptScene.h"
#include "CameraManager.h"
#include "Display\DXGIManager.h"
#include "Util\Functional.h"
#include "Physics\SceneLock.h"
#include <bitset>

using namespace Helix::Scene;

//---------------------------------------------------------------------------------------------------

RenderingScene::RenderingScene() :
	Scene()
{
}
//---------------------------------------------------------------------------------------------------
RenderingScene::~RenderingScene()
{
}

//---------------------------------------------------------------------------------------------------

void RenderingScene::Update(double _fDeltaTime, double _fTotalTime)
{
	//---------------------------------------------------------------------------------------------------
	// CAMERAS & rendering
	//---------------------------------------------------------------------------------------------------
	PhysicsScene* pPhysics = PhysicsScene::Instance();
	
	// allow users scripts to safely destory gameobjects
	ScriptScene::Instance()->Cleanup();	

	std::lock_guard<std::recursive_mutex> Guard(m_RenderingMutex);

	HGUIPROFILE(CameraUpdate)

	m_Cameras.resize(0);

	CameraManager::Instance()->GetActiveCameras(m_Cameras);
	for (CameraComponent* pCamera : m_Cameras)
	{
		pCamera->ClearVisibleObjects();
		pCamera->UpdateIntermediateProperties();
		pCamera->UpdateRenderingProperties();
	}

	m_uGatheredObjects = 0u;
	for(CameraComponent* pCamera : m_Cameras)
	{
		uint64_t uCameraID = pCamera->GetCameraID();

		m_VisibleObjectsBuffer.resize(0);
		pCamera->Gather(m_VisibleObjectsBuffer);
		m_uGatheredObjects += static_cast<uint32_t>(m_VisibleObjectsBuffer.size());

		// Get Most and least significant bits => range of passes to check
		DWORD uLSB = 0u;
		_BitScanForward64(&uLSB, pCamera->GetFlag());
		DWORD uMSB = 0u;
		_BitScanReverse64(&uMSB, pCamera->GetFlag());

		Math::float3 vCamPos = pCamera->GetPosition();

		// sort objects into per pass vectors
		conditional_parallel_for_each(m_VisibleObjectsBuffer.begin(), m_VisibleObjectsBuffer.end(), 1000u,
			[&](const Datastructures::GameObject* pObject)
		{
			// this is not the same as the gather flag for the query shape!
			if (pObject->CheckFlags(Datastructures::GameObjectFlag_Invisible) == false)
			{
				for (uint32_t uPassIndex = uLSB; uPassIndex <= uMSB; ++uPassIndex)
				{
					uint64_t uPassID = 1ull << uPassIndex;
					// check if the camera uses this pass
					if (pCamera->CheckFlag(uPassID) && pObject->CheckRenderPass(uPassID) &&
						pObject->CheckAndSetGatherFlags(uCameraID, uPassID) == false) // check wheter the object has been gathered for this pass and camera
					{
						// store distance to camera now to avoid gameobjects chaning position while sorting them, invalidating the iterators
						pCamera->GetVisibleObjects(uPassID).push_back({ pObject, pObject->GetPosition().distanceSquared(vCamPos) });
					}
				}
			}					
		});
			
		// sort Objects by Material props & distance to camera
		for (uint32_t uPassIndex = uLSB; uPassIndex <= uMSB; ++uPassIndex)
		{
			uint64_t uPassID = 1ull << uPassIndex;

			TVisibleObjectDistances& Objects = pCamera->GetVisibleObjects(uPassID);

			// sort objects by depth and material
			conditional_parallel_sort(Objects.begin(), Objects.end(), 1000u,
				[&](const GOCamDist& GO1,
					const GOCamDist& GO2) -> bool
			{
				
				std::bitset<64> MatMask1(GO1.pObject->GetMaterialMask());
				std::bitset<64> MatMask2(GO2.pObject->GetMaterialMask());

				size_t uMask1 = MatMask1.count();
				size_t uMask2 = MatMask2.count();

				if (uMask1 == uMask2)
				{
					return GO1.fDistToCamera < GO2.fDistToCamera;
				}

				return uMask1 < uMask2;
			});

			// clear gather flags
			conditional_parallel_for_each(Objects.begin(), Objects.end(), 1000u, [](const GOCamDist& GO)
			{
				GO.pObject->ResetGatherFlags();
			});
		}
	}

	HGUIPROFILE_END(CameraUpdate, RenderingScene)
	
	{
		HGUIPROFILE(Rendering)

		Display::RenderDeviceDX11::Instance()->Lock();

		ScriptScene::Instance()->PreRendering(); // this needs to be protected in case the backbuffer is resized by the gui

		//Render scene
		m_Renderer.Apply();

		Display::RenderDeviceDX11::Instance()->Unlock();


		HGUIPROFILE_END(Rendering, RenderingScene);

		HGUIPROFILE(Present)

		// Present
		Display::DXGIManager* pDXGIManager = Display::DXGIManager::Instance();
		DXGI_RATIONAL RefreshRate = pDXGIManager->GetMainDisplayMode().RefreshRate;
		float fRefreshRate = static_cast<float>(RefreshRate.Numerator) / static_cast<float>(RefreshRate.Denominator);

		//	First param: UINT SyncInterval specifies how to sync presentation with vertical blank
		//		=> for bit-block transfer (bitblt) model (DXGI_SWAP_EFFECT_DISCARD or DXGI_SWAP_EFFECT_SEQUENTIAL) values are:
		//		=> 0: presentation occurs immediately, no sync AND 1,2,3,4: synchronize presentation after nth vertical blank
		//		=> for flip model (DXGI_SWAP_EFFECT_FLIP_SEQUENTIAL) values are:
		//		=> 0: discard this frame if newer is queued AND  n>0: synchronize presentation for at least n vertical blanks

		if (m_bVSync || (m_bAdaptiveVSync && GetExecutionTime() <= fRefreshRate))
		{
			pDXGIManager->GetSwapChain()->Present(1, 0);
		}
		else
		{
			pDXGIManager->GetSwapChain()->Present(0, DXGI_PRESENT_DO_NOT_WAIT);
		}
		HGUIPROFILE_END(Present, RenderingScene);
	}
}
//---------------------------------------------------------------------------------------------------

void RenderingScene::End()
{
	Stop();
}
//---------------------------------------------------------------------------------------------------