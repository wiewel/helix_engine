//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef ROTATIONCOMPONENT_H
#define ROTATIONCOMPONENT_H

#include "DataStructures\Component.h"
#include "DataStructures\SyncVariable.h"

namespace Helix
{
	namespace Scene
	{
		class RotationComponent : public Datastructures::IComponent
		{
			COMPTYPENAME_EX(RotationComponent, Datastructures::IBaseObject, 'Rot ', Datastructures::ComponentCallback_OnUpdate);
			COMPDEFCONSTR(RotationComponent, Datastructures::IBaseObject, Datastructures::IComponent)

		private:
			RotationComponent(
				Datastructures::IBaseObject* _pParent,
				Math::float3 _vAxis = { 0.f, 1.f, 0.f },
				float _fSpeed = 1.f,
				bool _bLocal = true,
				bool _bPaused = false);

			// deserialization constructor
			RotationComponent(Datastructures::IBaseObject* _pParent, const Datastructures::ObjectType _kObjectType, Datastructures::TComponentCallbacks _kCallbacks);

			void OnUpdate(double _fDeltaTime, double _fTotalTime) final;

			void OnDeserialize(const hlx::TextToken& _Token) final;
			// user needs to return true if data was serialized into the token
			bool OnSerialize(_Out_ hlx::TextToken& _Token) const final;

		public:
			// allows the user to bind components to the object SyncDock
			Datastructures::SyncComponent* GetSyncComponent() final;
			
			const Math::float3& GetAxis() const;
			void SetAxis(const Math::float3& _vAxis);

			float GetSpeed() const;
			void SetSpeed(const float _fSpeed);

			bool GetPaused() const;
			void SetPaused(const bool _bPaused);

			bool GetLocal() const;
			void SetLocal(const bool _bLocal);

		private:
			Math::float3 m_vAxis = { 0.f, 1.f, 0.f };
			float m_fSpeed = 1.f;
			bool m_bPaused = false;
			bool m_bLocal = true;

#ifdef HNUCLEO
			Datastructures::SyncVariable<Math::float3> m_SyncAxis = { m_vAxis, "Axis" };
			Datastructures::SyncVariable<float> m_SyncSpeed = { m_fSpeed, "Speed" };
			Datastructures::SyncVariable<bool> m_SyncLocal = { m_bLocal, "Local" };
			Datastructures::SyncVariable<bool> m_SyncPaused = { m_bPaused, "Pause" };

			Datastructures::SyncComponent m_SyncComponent = { this, "Rotation Tool", {&m_SyncAxis,&m_SyncSpeed,&m_SyncLocal,&m_SyncPaused } };
#endif

		};
		inline const Math::float3& RotationComponent::GetAxis() const{return m_vAxis;}
		inline void RotationComponent::SetAxis(const Math::float3& _vAxis){m_vAxis = _vAxis;}

		inline float RotationComponent::GetSpeed() const{return m_fSpeed;}
		inline void RotationComponent::SetSpeed(const float _fSpeed){m_fSpeed = _fSpeed;}

		inline bool RotationComponent::GetPaused() const {return m_bPaused;}
		inline void RotationComponent::SetPaused(const bool _bPaused){m_bPaused = _bPaused;	}

		inline bool RotationComponent::GetLocal() const	{return m_bLocal;}
		inline void RotationComponent::SetLocal(const bool _bLocal) { m_bLocal = _bLocal; }

	}
} // Helix

#endif // !ROTATIONCOMPONENT_H
