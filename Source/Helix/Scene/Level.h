//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef LEVEL_H
#define LEVEL_H

#include <vector>
#include "Math\MathTypes.h"
#include "hlx\src\StandardDefines.h"
#include "PhysicsScene.h"
#include "DataStructures\GameObject.h"
#include "LightComponent.h"
#include "Util\NucleoProgressFunction.h"
#include "NucleoSelectionInterface.h"

namespace Helix
{
	namespace Scene
	{
		class Level
		{
		public:
			HDEBUGNAME("Level");

			Level();
			~Level();

			bool Load(const hlx::string& _sLevelName, bool _bAddToScene, bool _bLoadStaticAsKinematic = false);

			// requires absolute file path
			bool Save(const hlx::string& _sLevelFilePath);

			// destroys gameobjects & components
			void Unload();

			// creates an new gameobject for modification
			Datastructures::GameObject* SpawnBox(const Math::transform& _Transform, bool _bKinematic = true,  bool _bBindToNucleo = false);

			const hlx::string& GetName() const;
			const hlx::string& GetFilePath() const;

			void SetProgressFunction(const TProgressFunctor& _Func);

			void RemoveGameObject(Datastructures::GameObject* _pObject);

			bool HasLight() const;

		private:
			hlx::string m_sName;
			hlx::string m_sFilePath;

			uint32_t m_uObjectCount = 0u;

			bool m_bHasLight = false;

			TProgressFunctor m_ProgressFunc;

			std::vector<Display::RenderObjectDX11*> m_RenderObjects;
			std::vector<Datastructures::GameObject*> m_GameObjects;

#ifdef HNUCLEO
			inline std::string GetText(int _iIndex) const { return m_GameObjects[_iIndex]->GetName(); }
			inline void SelectItem(int _iIndex) const { NucleoSelectionInterface::Instance()->SetActiveSelection(m_GameObjects[_iIndex]); m_GameObjects[_iIndex]->BindToNucleo(); }
			Datastructures::SyncList<Datastructures::GameObject*> m_LevelList = { "Objects", &m_GameObjects, [&](int i) {return GetText(i); },[&](int i) {SelectItem(i); } };

			Datastructures::SyncGroup m_LevelGroup = { "Selection",{ &m_LevelList}/*,  Datastructures::WidgetLayout_Vertical */};
			Datastructures::SyncDock m_LevelDock = { "Level", {&m_LevelGroup }, Datastructures::DockAnchor_Right };
#endif
		};

		inline void Level::SetProgressFunction(const TProgressFunctor& _Func) { m_ProgressFunc = _Func; }
		
		// Getter/Setter
		inline const hlx::string& Level::GetName() const { return m_sName; }
		inline const hlx::string& Level::GetFilePath() const{return m_sFilePath;}

		inline bool Helix::Scene::Level::HasLight() const{return m_bHasLight;}
	} // Scene

} // Helix

#endif