//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "RenderingSceneGuard.h"
#include "RenderingScene.h"

using namespace Helix::Scene;
//---------------------------------------------------------------------------------------------------
RenderingScene* RenderingSceneGuard::m_pScene = nullptr;

RenderingSceneGuard::RenderingSceneGuard()
{
	if (m_pScene == nullptr)
	{
		m_pScene = RenderingScene::Instance();
	}

	m_pScene->Lock();
}
//---------------------------------------------------------------------------------------------------

RenderingSceneGuard::~RenderingSceneGuard()
{
	HASSERTD(m_pScene != nullptr, "Invalid RenderingScene!");
	m_pScene->Unlock();
}
//---------------------------------------------------------------------------------------------------

void RenderingSceneGuard::Lock()
{
	if (m_pScene == nullptr)
	{
		m_pScene = RenderingScene::Instance();
	}

	m_pScene->Lock();
}
//---------------------------------------------------------------------------------------------------

void RenderingSceneGuard::Unlock()
{
	HASSERTD(m_pScene != nullptr, "Invalid RenderingScene!");
	m_pScene->Unlock();
}
//---------------------------------------------------------------------------------------------------
