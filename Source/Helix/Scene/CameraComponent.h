//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef CAMERACOMPONENT_H
#define CAMERACOMPONENT_H

#include "hlx\src\StandardDefines.h"
#include "CameraManager.h"
#include "DataStructures\Component.h"
#include "CameraDefines.h"
#include "CullingFrustum.h"
#include "Util\Flag.h"
#include "hlx\src\String.h"

namespace Helix
{
	namespace Display
	{
		class RenderPassDX11;
		class DebugRenderDX11;
		class DeferredRendererDX11;
	}
	namespace Physics
	{
		struct CollisionFilter;
	}
	namespace Scene
	{
		struct RaycastHitInfo;
		class VolumeCache;
		class FullscreenEffects;

		//     Near    Far
		//    0----1  4----5
		//    |    |  |    |
		//    |    |  |    |
		//    3----2  7----6
		enum FrustumCorner
		{
			kFrustumCorner_NearUpperLeft = 0,
			kFrustumCorner_NearUpperRight,
			kFrustumCorner_NearLowerRight,
			kFrustumCorner_NearLowerLeft,
			kFrustumCorner_FarUpperLeft,
			kFrustumCorner_FarUpperRight,
			kFrustumCorner_FarLowerRight,
			kFrustumCorner_FarLowerLeft,
			kFrustumCorner_NumOf
		};

		using FrustumCorners = std::array<Math::float3, kFrustumCorner_NumOf>;

		struct CameraProperties
		{
			Math::float3 m_vOffset = { 0,0,0 };

			// View & Projection matrices
			Math::float4x4 m_mView;
			Math::float4x4 m_mProj;
			Math::float4x4 m_mViewProj;

			Math::quaternion m_qOrientation;
			Math::float3 m_vPosition;
			Math::float3 m_vUp;
			Math::float3 m_vRight;
			Math::float3 m_vLook;

			// Projection properties
			float m_fFovY;			// Y field of view
			float m_fAspectRatio;	// Width / Height
			float m_fNearDistance;
			float m_fFarDistance;
			float m_fNearWindowHeight;
			float m_fFarWindowHeight;

			FrustumCorners m_FrustumCorners;
		};

		class CameraComponent : public Datastructures::IComponent, public Flag<uint64_t>
		{
			friend class Display::DebugRenderDX11;
			friend class Display::RenderPassDX11;
			friend class RenderingScene;
			friend class FullscreenEffects;

		private:
			CameraComponent(Datastructures::IBaseObject* _pParent,
				std::string _sName, 
				const float _fFovYDegrees,
				const float _fAspectRatio,
				const float _fNearDistance,
				const float _fFarDistance);

			COMPTYPENAME(CameraComponent, Datastructures::IBaseObject, 'Cam ');
			COMPDEFCONSTR(CameraComponent, Datastructures::IBaseObject, IComponent);
			COMPDESCONSTR(CameraComponent, Datastructures::IBaseObject, IComponent);

			enum CullingGeometry
			{
				kCullingGeometry_Frustum = 0,
				kCullingGeometry_Sphere,
				kCullingGeometry_Unknown
			};

		private:
			// View matrix must be recalculated after transforming the camera 
			void UpdateIntermediateProperties();

		public:
			// Change the camera projection
			void SetLens(const float _fFovYRad, const float _fAspectRatio, const float _fNearDistance, const float _fFarDistance);
			void SetAspectRatio(const float _fAspectRatio);
			void SetFOV(const float _fFovYDeg);

			const bool IsActive() const;
			void SetActive(const bool _bActive);
			const std::string& GetName() const;
			void SetName(std::string _sName);

			void SetDistances(const float _fNearDistance, const float _fFarDistance);

			// View & Proj Matrices
			const Math::float4x4& GetView() const;
			const Math::float4x4& GetProj() const;
			const Math::float4x4& GetViewProj() const;

			// Camera Basis Vectors in world space
			const Math::float3& GetUp() const;
			const Math::float3& GetRight() const;
			const Math::float3& GetLook() const;
			const Math::float3& GetPosition() const;
			const Math::quaternion& GetOrientation() const;
			const Math::transform GetTransform() const;

			// Projection properties
			float GetFovY() const;
			float GetFovX() const;
			float GetNearDistance() const;
			float GetFarDistance() const;
			float GetAspectRatio() const;
			float GetNearWindowWidth() const;
			float GetNearWindowHeight() const;
			float GetFarWindowWidth() const;
			float GetFarWindowHeight() const;

			void UpdateRenderingProperties();
			const CameraProperties& GetRenderingProperties() const;

			// Query
			Math::float3 ScreenPosToWorldRay(const Math::float2& _vScreenPos) const;
			Math::float2 WorldToScreenPos(const Math::float3& _vWorldPos) const;
			Math::float3 GetProjectedPointOnNearPlane(const Math::float3& _vOther) const;
			static void ConvertToInfiniteProj(Math::float4x4& _vProj, const float _fNearDistance);
			// raycast in view direction from camera pos
			void Raycast(_Out_ TVisibleObjects& _ObjectsInCameraFrustum);
			void Raycast(const Math::float3& _vOrigin, const Math::float3& _vUnitDir, const float _fDistance, _Out_ TVisibleObjects& _ObjectsInCameraFrustum);
			// Raycast first hit
			Datastructures::GameObject* Raycast( const Math::float3& _vOrigin, const Math::float3& _vUnitDir, const float _fDistance, RaycastHitInfo* _pHitInfo = nullptr, Physics::TCollisionFilterMask _CollisionFilters = { Physics::CollisionFilterFlag_Default }) const;
			const FrustumCorners& GetFrustumCorners() const;
			const CullingFrustum GetCullingFrustum() const;
			void Gather(_Out_ TVisibleObjects& _ObjectsInCameraFrustum);
			void AddRenderObject(Display::RenderObjectDX11* _pRenderObject);
			void RemoveRenderObject(Display::RenderObjectDX11* _pRenderObject);

			void UseCullingSphere(const Physics::SphereDesc& _Desc);
			void UseCullingFrustum();

			const uint64_t& GetCameraID() const;

			void SetOffset(const Math::float3& _vOffset);
			Math::float3 GetOffset() const;

		protected:
			// Culling
			bool InitializeVolumeCache(uint32_t _uStaticObjects = 10000u, uint32_t _uDynamicObjects = 10000u) const;
			TVisibleObjectDistances& GetVisibleObjects(uint64_t _kPassID);
			void ClearVisibleObjects();
			std::vector<Display::RenderObjectDX11*>& GetRenderObjects();
			// Frustum used for culling intersection tests

		private:
			bool m_bActive = true;
			std::string m_sName = "New Camera";
			uint64_t m_uCameraID;

			CameraProperties m_IntermediateProperties;
			CameraProperties m_RenderingProperties;

			Physics::SphereDesc m_CullingSphere;
			CullingGeometry m_kCullingGeometry = kCullingGeometry_Frustum;

			mutable VolumeCache* m_pVolumeCache = nullptr;
			concurrency::concurrent_vector<TVisibleObjectDistances> m_VisibleObjects;
			std::vector<Display::RenderObjectDX11*> m_RenderObjects;
		};

		// Active
		//---------------------------------------------------------------------------------------------------
		inline const bool CameraComponent::IsActive() const { return m_bActive; }
		//---------------------------------------------------------------------------------------------------
		inline void CameraComponent::SetActive(const bool _bActive) { m_bActive = _bActive; }
		//---------------------------------------------------------------------------------------------------
		inline const std::string & CameraComponent::GetName() const { return m_sName; }
		//---------------------------------------------------------------------------------------------------
		inline void CameraComponent::SetName(std::string _sName) 
		{ 
			CameraManager::Instance()->RenameCamera(this, _sName);
			m_sName = _sName; 
		}

		//---------------------------------------------------------------------------------------------------
		inline void CameraComponent::SetDistances(const float _fNearDistance, const float _fFarDistance) { SetLens(m_IntermediateProperties.m_fFovY, m_IntermediateProperties.m_fAspectRatio, _fNearDistance, _fFarDistance); }
		//---------------------------------------------------------------------------------------------------

		// Matrices
		//---------------------------------------------------------------------------------------------------
		inline const Math::float4x4& CameraComponent::GetView() const { return m_IntermediateProperties.m_mView; }
		//---------------------------------------------------------------------------------------------------
		inline const Math::float4x4& CameraComponent::GetProj() const { return m_IntermediateProperties.m_mProj; }
		//---------------------------------------------------------------------------------------------------
		inline const Math::float4x4& CameraComponent::GetViewProj() const { return m_IntermediateProperties.m_mViewProj; }
		//---------------------------------------------------------------------------------------------------
		inline const Math::float3 & CameraComponent::GetUp() const { return m_IntermediateProperties.m_vUp; }
		//---------------------------------------------------------------------------------------------------
		inline const Math::float3 & CameraComponent::GetRight() const { return m_IntermediateProperties.m_vRight; }
		//---------------------------------------------------------------------------------------------------
		inline const Math::float3 & CameraComponent::GetLook() const { return m_IntermediateProperties.m_vLook; }
		//---------------------------------------------------------------------------------------------------
		inline const Math::float3 & CameraComponent::GetPosition() const { return m_IntermediateProperties.m_vPosition; }
		//---------------------------------------------------------------------------------------------------
		inline const Math::quaternion & CameraComponent::GetOrientation() const { return m_IntermediateProperties.m_qOrientation; }
		//---------------------------------------------------------------------------------------------------
		inline const Math::transform CameraComponent::GetTransform() const { return GetParent()->GetTransform(); }

		// Projection properties.
		//---------------------------------------------------------------------------------------------------
		inline float CameraComponent::GetFovY()const { return m_IntermediateProperties.m_fFovY; }
		//---------------------------------------------------------------------------------------------------
		inline float CameraComponent::GetFovX() const
		{
			float fHalfWidth = 0.5f * GetNearWindowWidth();
			return 2.0f * atan(fHalfWidth / m_IntermediateProperties.m_fNearDistance);
		}
		//---------------------------------------------------------------------------------------------------
		inline float CameraComponent::GetNearDistance() const { return m_IntermediateProperties.m_fNearDistance; }
		//---------------------------------------------------------------------------------------------------
		inline float CameraComponent::GetFarDistance() const { return m_IntermediateProperties.m_fFarDistance; }
		//---------------------------------------------------------------------------------------------------
		inline float CameraComponent::GetAspectRatio()const { return m_IntermediateProperties.m_fAspectRatio; }
		//---------------------------------------------------------------------------------------------------
		inline float CameraComponent::GetNearWindowHeight() const { return m_IntermediateProperties.m_fNearWindowHeight; }
		//---------------------------------------------------------------------------------------------------
		inline float CameraComponent::GetFarWindowHeight() const { return m_IntermediateProperties.m_fFarWindowHeight; }
		//---------------------------------------------------------------------------------------------------
		inline void CameraComponent::UpdateRenderingProperties(){m_RenderingProperties = m_IntermediateProperties;}
		//---------------------------------------------------------------------------------------------------
		inline const CameraProperties& CameraComponent::GetRenderingProperties() const	{return m_RenderingProperties;}
		//---------------------------------------------------------------------------------------------------
		inline float CameraComponent::GetNearWindowWidth() const { return m_IntermediateProperties.m_fAspectRatio * m_IntermediateProperties.m_fNearWindowHeight; }
		//---------------------------------------------------------------------------------------------------
		inline float CameraComponent::GetFarWindowWidth() const { return m_IntermediateProperties.m_fAspectRatio * m_IntermediateProperties.m_fFarWindowHeight; }
		//---------------------------------------------------------------------------------------------------
		inline void CameraComponent::ConvertToInfiniteProj(Math::float4x4& _vProj, const float _fNearDistance)
		{
			_vProj[2][2] = 1.0f - FLT_EPSILON;
			_vProj[3][2] = (FLT_EPSILON - 1.0f) * _fNearDistance;
		}
		// Culling
		//---------------------------------------------------------------------------------------------------
		inline const FrustumCorners& CameraComponent::GetFrustumCorners() const { return m_IntermediateProperties.m_FrustumCorners; }
		//---------------------------------------------------------------------------------------------------
		inline TVisibleObjectDistances& CameraComponent::GetVisibleObjects(uint64_t _kPassID)
		{
			uint32_t uIndex = Math::Log2(_kPassID);
			if (m_VisibleObjects.size() <= uIndex)
			{
				m_VisibleObjects.resize(uIndex + 1);
			}

			return m_VisibleObjects[uIndex];
		}
		//---------------------------------------------------------------------------------------------------
		inline void CameraComponent::ClearVisibleObjects()
		{
			for (TVisibleObjectDistances& Objects : m_VisibleObjects)
			{
				Objects.resize(0);
			}
		}
		//---------------------------------------------------------------------------------------------------
		inline void CameraComponent::AddRenderObject(Display::RenderObjectDX11* _pRenderObject) 
		{ 
			m_RenderObjects.push_back(_pRenderObject); 
		}
		//---------------------------------------------------------------------------------------------------
		inline void CameraComponent::RemoveRenderObject(Display::RenderObjectDX11* _pRenderObject)
		{
			auto RemoveIt = std::find(m_RenderObjects.begin(), m_RenderObjects.end(), _pRenderObject);
			if (RemoveIt != m_RenderObjects.end())
			{
				m_RenderObjects.erase(RemoveIt);
			}
		}
		//---------------------------------------------------------------------------------------------------
		inline void CameraComponent::UseCullingSphere(const Physics::SphereDesc& _Desc) { m_kCullingGeometry = kCullingGeometry_Sphere; m_CullingSphere = _Desc; }
		inline void CameraComponent::UseCullingFrustum() { m_kCullingGeometry = kCullingGeometry_Frustum; }
		inline const uint64_t& CameraComponent::GetCameraID() const	{return m_uCameraID;}
		inline void CameraComponent::SetOffset(const Math::float3 & _vOffset){ m_IntermediateProperties.m_vOffset = _vOffset;	}
		inline Math::float3 CameraComponent::GetOffset() const	{return m_IntermediateProperties.m_vOffset;	}
		//---------------------------------------------------------------------------------------------------
		inline std::vector<Display::RenderObjectDX11*>& CameraComponent::GetRenderObjects() { return m_RenderObjects; }
		//---------------------------------------------------------------------------------------------------

	}
}
#endif // !CAMERACOMPONENT_H