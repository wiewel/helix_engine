//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef PHYSICSSCENE_H
#define PHYSICSSCENE_H

#include "Scene.h"
#include "Physics\PhysicsFactory.h"
#include "Math\MathTypes.h"
#include "PxVolumeCache.h"
#include "CullingFrustum.h"

namespace Helix
{
	namespace Datastructures
	{
		class GameObject;
		class ShapeComponent;
	}

	namespace Physics
	{
		class SceneReadLock;
		class SceneWriteLock;
		class ControllerComponent;
		class QueryFilterCallback;
	}

	namespace Scene
	{
		class JointComponent;
		class PhysicsScene : public Scene, public hlx::Singleton<PhysicsScene>
		{
			friend class Datastructures::GameObject;
			friend class VolumeCache;
			friend class JointComponent;
			friend class Physics::SceneWriteLock;
			friend class Physics::SceneReadLock;
			friend class Physics::ControllerComponent;

		public:
			PhysicsScene();
			~PhysicsScene();

			bool Initialize();
			//bool Begin();
			void End();

			bool ConnectToPVD(const char* _sIP = "127.0.0.1", int32_t _uPort = 5425, uint32_t _uTimeOut = 100u);
			void DisconnectPVD();

			void SetGravity(Math::float3& _vGravityForce);
			Math::float3 GetGravity() const;

			bool Raycast(Math::float3 _vOrigin, Math::float3 _vUnitDir, float _fDistance, _Out_ Datastructures::GameObject*& _pHit, Datastructures::GameObject* _pCaster) const;
			bool Sweep(Physics::GeoBase& _Geometry, Math::transform& _InitialPose, Math::float3& _vSweepDir, float _fDistance, Physics::QueryFilterCallback& _QueryFilter, _Out_ Datastructures::GameObject*& _pHit) const;

			bool GetSimulatePhysics() const;
			void SetSimulatePhysics(const bool _bSimulate);

		private:
			void Update(double _fDeltaTime, double _fTotalTime) final;

			physx::PxScene* GetScene() const;
			physx::PxPhysics* GetPhysics() const;
			physx::PxControllerManager* GetControllerManager() const;

			void LockWrite();
			void UnlockWrite();

		private:
			Physics::PhysicsFactory* m_pFactory = nullptr;
			physx::PxScene* m_pScene = nullptr;
			physx::PxControllerManager* m_pControllerManager = nullptr;
			physx::PxVisualDebuggerConnection* m_pPVD = nullptr;

			physx::PxPhysics* m_pPhysics = nullptr;
			bool m_bSimulate = true;

			std::mutex m_WriteLock;
		};

		inline physx::PxScene* PhysicsScene::GetScene() const { return m_pScene; }
		inline physx::PxPhysics* PhysicsScene::GetPhysics() const { return m_pPhysics; }
		inline physx::PxControllerManager* PhysicsScene::GetControllerManager() const { return m_pControllerManager; }
		
		inline void PhysicsScene::LockWrite(){m_WriteLock.lock();}
		inline void PhysicsScene::UnlockWrite()	{m_WriteLock.unlock();}
		inline bool PhysicsScene::GetSimulatePhysics() const { return m_bSimulate;}
		inline void PhysicsScene::SetSimulatePhysics(const bool _bSimulate) { m_bSimulate = _bSimulate; }


	} // Scene
} // Helix


#endif // !PHYSICSSCENE_H
