//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "CameraManager.h"
#include "CameraComponent.h"
#include "hlx\src\Logger.h"

using namespace Helix::Async;
using namespace Helix::Scene;

//---------------------------------------------------------------------------------------------------

CameraManager::CameraManager()
{
}
//---------------------------------------------------------------------------------------------------

CameraManager::~CameraManager()
{
}
//---------------------------------------------------------------------------------------------------
CameraComponent* CameraManager::GetCameraByName(const std::string& _sName)
{
	ScopedSpinLock lock(m_CreateLock);

	auto FoundIt = m_Cameras.find(_sName);
	if (FoundIt != m_Cameras.end())
	{
		return FoundIt->second;
	}

	HERROR("Camera %s not found!", CSTR(_sName));
	return nullptr;
}
//---------------------------------------------------------------------------------------------------
uint64_t CameraManager::RegisterCamera(CameraComponent* _pCameraComponent)
{
	ScopedSpinLock lock(m_CreateLock);
	const std::string& sName = _pCameraComponent->GetName();

	auto it = m_Cameras.find(sName);

	if (it != m_Cameras.end())
	{
		HFATAL("Camera with name %s already exists!", sName);
		return 0ull;
	}

	m_Cameras.insert({ sName, _pCameraComponent });

	return 1ull << m_CameraIDs.GetAssociatedID(sName);
}
//---------------------------------------------------------------------------------------------------
void CameraManager::RemoveCamera(CameraComponent* _pCameraComponent)
{
	ScopedSpinLock lock(m_CreateLock);

	auto RemoveIt = m_Cameras.find(_pCameraComponent->GetName());
	if (RemoveIt != m_Cameras.end())
	{
		m_Cameras.erase(RemoveIt);
	}
}
//---------------------------------------------------------------------------------------------------
void Helix::Scene::CameraManager::RenameCamera(CameraComponent * _pCameraComponent, std::string _sNewName)
{
	ScopedSpinLock lock(m_CreateLock);

	auto RenameIt = m_Cameras.find(_pCameraComponent->GetName());
	if (RenameIt != m_Cameras.end())
	{
		m_Cameras.erase(RenameIt);
		m_Cameras.insert({ _sNewName, _pCameraComponent });
	}
}
//---------------------------------------------------------------------------------------------------
