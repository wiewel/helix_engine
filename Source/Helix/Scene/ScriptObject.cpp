//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "ScriptObject.h"
#include "Scene\ScriptScene.h"
#include "DataStructures\GameObject.h"

using namespace Helix::Scene;
using namespace Helix::Datastructures;
//---------------------------------------------------------------------------------------------------

IScriptObject::IScriptObject() :
	m_pGameObjectPool(GameObjectPool::Instance())
{
	ScriptScene::Instance()->RegisterScript(this);
}
//---------------------------------------------------------------------------------------------------
// scripts need to be deleted before the scene!
IScriptObject::~IScriptObject()
{
	if (m_bStaticScript == false)
	{
		ScriptScene::Instance()->UnregisterScript(this);
	}
}
//---------------------------------------------------------------------------------------------------

std::future<GameObject*> IScriptObject::CreateGameObject()
{
	return m_pGameObjectPool->Create();
}
//---------------------------------------------------------------------------------------------------

std::future<std::vector<GameObject*>> IScriptObject::CreateGameObject(uint32_t _uCount)
{
	return m_pGameObjectPool->Create(_uCount);
}
//---------------------------------------------------------------------------------------------------
