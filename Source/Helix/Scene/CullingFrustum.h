//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef CULLINGFRUSTUM_H
#define CULLINGFRUSTUM_H

#include "Math\MathTypes.h"
#include "Physics\GeometryTypes.h"
//#include "Collision\XMCollision.h"

namespace Helix
{
	namespace Scene
	{
		class CullingFrustum
		{
		public:
			CullingFrustum(const Physics::GeoBox& _AABB, const Math::transform& _Transform);
			~CullingFrustum();

			const Physics::GeoBox& GetAABB() const;
			const Math::transform& GetTransform() const;

		private:
			Physics::GeoBox m_AABB;
			Math::transform m_Transform;
		};

		inline CullingFrustum::CullingFrustum(const Physics::GeoBox& _AABB, const Math::transform& _Transform) :
			m_AABB(_AABB),
			m_Transform(_Transform)
		{
		}
		//---------------------------------------------------------------------------------
		inline CullingFrustum::~CullingFrustum()
		{
		}
		//---------------------------------------------------------------------------------
		inline const Physics::GeoBox& CullingFrustum::GetAABB() const
		{
			return m_AABB;
		}
		//---------------------------------------------------------------------------------
		inline const Math::transform& CullingFrustum::GetTransform() const
		{
			return m_Transform;
		}
	} // Scene
} // Helix

#endif // !FRUSTUM_H
