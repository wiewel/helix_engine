//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher, David Labode
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef NUCLEOSELECTIONINTERFACE_H
#define NUCLEOSELECTIONINTERFACE_H

#include "hlx\src\Singleton.h"
#include "Async\SpinLock.h"

namespace Helix
{
	// forward Decls
	namespace Nucleo
	{
		class MouseTool;
	}

	namespace Datastructures
	{
		class GameObject;
	}

	namespace Scene
	{
		class NucleoSelectionInterface : public hlx::Singleton<NucleoSelectionInterface>
		{
			friend class Nucleo::MouseTool;
			friend class Level;
		public:
			NucleoSelectionInterface();
			~NucleoSelectionInterface();			

			Datastructures::GameObject* GetActiveSelection() const;
		private:

			void SetActiveSelection(Datastructures::GameObject* _pObject);

		private:
			Datastructures::GameObject* m_pActiveSelection = nullptr;
			mutable Async::SpinLock m_Lock;
		};
	} // Scene
} // Helix

#endif // !NUCLEOSELECTIONINTERFACE_H
