//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher, David Labode
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "NucleoSelectionInterface.h"
#include "DataStructures\GameObject.h"

using namespace Helix::Async;
using namespace Helix::Scene;
using namespace Helix::Datastructures;

NucleoSelectionInterface::NucleoSelectionInterface()
{
}
//---------------------------------------------------------------------------------------------------

NucleoSelectionInterface::~NucleoSelectionInterface()
{
	m_pActiveSelection = nullptr;
}
//---------------------------------------------------------------------------------------------------

GameObject* NucleoSelectionInterface::GetActiveSelection() const
{
	ScopedSpinLock lock(m_Lock);
	return m_pActiveSelection;
}
//---------------------------------------------------------------------------------------------------

void NucleoSelectionInterface::SetActiveSelection(GameObject* _pObject)
{
	ScopedSpinLock lock(m_Lock);
	m_pActiveSelection = _pObject;
}
//---------------------------------------------------------------------------------------------------
