//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher, David Labode
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef LIGHTCONTEXT_H
#define LIGHTCONTEXT_H

#include "hlx\src\Singleton.h"
#include "Display\DX11\TextureVariantsDX11.h"
#include "LightDefines.h"
#include <unordered_set>
#include "Async\SpinLock.h"
#include "DataStructures\SyncVariable.h"

namespace Helix
{
	namespace Scene
	{
		// forward decls
		class LightComponent;

		using TLights = std::unordered_set<const LightComponent*>;

		class LightContext : public hlx::Singleton<LightContext>
		{
			friend class LightComponent;
		public:
			LightContext();
			~LightContext();

			void SetAmbientColor(const Math::float3& _vAmbientColor);
			void SetShadowTransform(const Math::float4x4& _ShadowTransform);

			const Math::float3& GetAmbientColor() const;
			const Math::float4x4& GetShadowTransform() const;
			bool GetMainDirLight(_Out_ LightDesc& _sMainLight) const;

			/// LightType_Unknown gathers all lights
			void GetShadowCastingLights(std::vector<const LightComponent*>& _ShadowCaster, LightType _kType = LightType_Unknown) const;

			void GetLights(
				std::vector<LightDesc>& _DirLights,
				std::vector<LightDesc>& _PointLights,
				std::vector<LightDesc>& _SpotLights, 
				bool _bEnabledOnly = true);

			void SetSkybox(const std::string& _sSkybox);
			const Display::TextureCubeDX11& GetSkybox() const;

			void SetEnvironmentMap(const std::string& _sEnvironmentMap);
			const Display::TextureCubeDX11& GetEnvironmentMap() const;

			void SortLights(std::vector<LightDesc>& _Lights, const Math::float3& _vCameraPos) const;
			void SortLights(std::vector<const LightComponent*>& _Lights, const Math::float3& _vCameraPos) const;

		private:
			void AddLight(const LightComponent* _pLight);
			void RemoveLight(const LightComponent* _pLight);

			void GatherLights(
				std::vector<LightDesc>& _DirLights,
				std::vector<LightDesc>& _PointLights,
				std::vector<LightDesc>& _SpotLights,
				bool _bEnabledOnly,
				bool _bShadowCasterOnly);

			void GatherShadowCastingLights(
				std::vector<const LightComponent*>& _Lights,
				LightType _kType,
				bool _bEnabledOnly,
				bool _bShadowCasterOnly) const;

		private:
			Math::float3 m_vAmbientColor;

			mutable Async::SpinLock m_Lock;

			TLights m_Lights;
			const LightComponent* m_pMainDirLight = nullptr;

			/// Used to draw the skybox
			Display::TextureCubeDX11 m_Skybox = nullptr;

			/// Used for lighting calculations, should be preconvolved cos power lobe
			Display::TextureCubeDX11 m_EnvironmentMap = nullptr;

			Math::float4x4 m_ShadowTransform;

			inline std::string GetSkyBoxPath() const { return m_Skybox ? m_Skybox.GetDescription().m_sFilePathOrName : ""; }
			inline std::string GetEnvironmentMapPath() const { return m_EnvironmentMap ? m_EnvironmentMap.GetDescription().m_sFilePathOrName : ""; }

#ifdef HNUCLEO
			SYNOBJ(SkyBox, LightContext, std::string, GetSkyBoxPath, SetSkybox) m_SyncSkyBox = { *this, "SkyBox" };
			SYNOBJ(EnvironmentMap, LightContext, std::string, GetEnvironmentMapPath, SetEnvironmentMap) m_SyncEnvMap = { *this, "EnvironmentMap" };

			Datastructures::SyncVariable<Math::float3> m_SyncAmbientColor = { m_vAmbientColor, "Ambient Color" };

			Datastructures::SyncGroup m_SyncLightGroup = { "Light Settings", {&m_SyncSkyBox, &m_SyncEnvMap, &m_SyncAmbientColor} };
			Datastructures::SyncDock m_SyncLightDock = { "Light Context",{ &m_SyncLightGroup} };
#endif
		};

		inline const Math::float4x4& LightContext::GetShadowTransform() const { return m_ShadowTransform; }

		inline const Math::float3& LightContext::GetAmbientColor() const { return m_vAmbientColor; }
		inline const Display::TextureCubeDX11& LightContext::GetSkybox() const { return m_Skybox; }
		inline const Display::TextureCubeDX11& LightContext::GetEnvironmentMap() const { return m_EnvironmentMap; }

		inline void LightContext::SetAmbientColor(const Math::float3& _vAmbientColor)	{m_vAmbientColor = _vAmbientColor;}
		inline void LightContext::SetShadowTransform(const Math::float4x4& _ShadowTransform){	m_ShadowTransform = _ShadowTransform;}
		//---------------------------------------------------------------------------------------------------

		inline void LightContext::SetSkybox(const std::string& _sSkybox)
		{
			if (_sSkybox.empty() == false)
			{
				if (GetSkyBoxPath() != _sSkybox || m_Skybox == nullptr)
				{
					m_Skybox = std::move(Display::TextureCubeDX11(_sSkybox, 8, false, false));
				}
			}
			else
			{
				m_Skybox = nullptr;
			}
		}
		//---------------------------------------------------------------------------------------------------
		inline void LightContext::SetEnvironmentMap(const std::string& _sEnvironmentMap)
		{
			if (_sEnvironmentMap.empty() == false)
			{
				if (GetEnvironmentMapPath() != _sEnvironmentMap || m_EnvironmentMap == nullptr)
				{
					m_EnvironmentMap = std::move(Display::TextureCubeDX11(_sEnvironmentMap, 8, false, false));
				}
			}
			else
			{
				m_EnvironmentMap = nullptr;
			}
		}

	} // Scene

} // Helix


#endif