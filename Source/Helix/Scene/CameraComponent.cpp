//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "CameraComponent.h"
#include "Math\MathTypes.h"
#include "Math\MathFunctions.h"
#include "VolumeCache.h"
#include "Physics\CollisionFilter.h"
#include "Scene\PhysicsScene.h"

using namespace Helix::Scene;
using namespace Helix::Math;
using namespace Helix::Datastructures;
using namespace Helix::Physics;

//===================================================================================================
//---------------------------------------------------------------------------------------------------
CameraComponent::CameraComponent(
	IBaseObject * _pParent,
	std::string _sName,
	const float _fFovYDegrees,
	const float _fAspectRatio,
	const float _fNearDistance,
	const float _fFarDistance)
	: IComponent(
		_pParent,
		ObjectType_BaseObject,
		ComponentCallback(ComponentCallback_OnUpdate))
{
	// Register the camera in the camera manager
	m_sName = _sName;
	m_uCameraID = CameraManager::Instance()->RegisterCamera(this);
	SetLens(DegToRad(_fFovYDegrees), _fAspectRatio, _fNearDistance, _fFarDistance);
}
//---------------------------------------------------------------------------------------------------
CameraComponent::~CameraComponent()
{
	CameraManager::Instance()->RemoveCamera(this);
	HSAFE_DELETE(m_pVolumeCache);
}

//===================================================================================================
// Update
//---------------------------------------------------------------------------------------------------
// Update view matrix after transforming the camera
void CameraComponent::UpdateIntermediateProperties()
{
	// get parent object transform
	m_IntermediateProperties.m_qOrientation = GetParent()->GetOrientation();
	m_IntermediateProperties.m_vPosition = GetParent()->GetPosition();
	m_IntermediateProperties.m_vPosition += m_IntermediateProperties.m_vOffset;

	// update basis vectors
	m_IntermediateProperties.m_vRight = m_IntermediateProperties.m_qOrientation * float3(1, 0, 0);
	m_IntermediateProperties.m_vUp = m_IntermediateProperties.m_qOrientation * float3(0, 1, 0);
	m_IntermediateProperties.m_vLook = m_IntermediateProperties.m_qOrientation * float3(0, 0, 1);

	// reconstruct view matrix from local coordinate system
	m_IntermediateProperties.m_mView = MatrixLookToLH(m_IntermediateProperties.m_vPosition, m_IntermediateProperties.m_vLook, m_IntermediateProperties.m_vUp).getTranspose();

	// Update view-projection matrix after updating the view matrix
	m_IntermediateProperties.m_mViewProj = m_IntermediateProperties.m_mProj * m_IntermediateProperties.m_mView;

	// compute frustum corners
	float2 vNearHalfExtents;
	vNearHalfExtents.x = (m_IntermediateProperties.m_fNearWindowHeight * m_IntermediateProperties.m_fAspectRatio) * 0.5f;
	vNearHalfExtents.y = m_IntermediateProperties.m_fNearWindowHeight * 0.5f;

	m_IntermediateProperties.m_FrustumCorners[kFrustumCorner_NearUpperLeft] = m_IntermediateProperties.m_vPosition - m_IntermediateProperties.m_vRight * vNearHalfExtents.x + m_IntermediateProperties.m_vUp * vNearHalfExtents.y;
	m_IntermediateProperties.m_FrustumCorners[kFrustumCorner_NearUpperRight] = m_IntermediateProperties.m_vPosition + m_IntermediateProperties.m_vRight * vNearHalfExtents.x + m_IntermediateProperties.m_vUp * vNearHalfExtents.y;
	m_IntermediateProperties.m_FrustumCorners[kFrustumCorner_NearLowerRight] = m_IntermediateProperties.m_vPosition + m_IntermediateProperties.m_vRight * vNearHalfExtents.x - m_IntermediateProperties.m_vUp * vNearHalfExtents.y;
	m_IntermediateProperties.m_FrustumCorners[kFrustumCorner_NearLowerLeft] = m_IntermediateProperties.m_vPosition - m_IntermediateProperties.m_vRight * vNearHalfExtents.x - m_IntermediateProperties.m_vUp * vNearHalfExtents.y;

	float2 vFarHalfExtents;
	vFarHalfExtents.x = (m_IntermediateProperties.m_fFarWindowHeight * m_IntermediateProperties.m_fAspectRatio) * 0.5f;
	vFarHalfExtents.y = m_IntermediateProperties.m_fFarWindowHeight * 0.5f;

	float3 vFarCenter = m_IntermediateProperties.m_vPosition + m_IntermediateProperties.m_vLook * (m_IntermediateProperties.m_fFarDistance - m_IntermediateProperties.m_fNearDistance);
	m_IntermediateProperties.m_FrustumCorners[kFrustumCorner_FarUpperLeft] = vFarCenter - m_IntermediateProperties.m_vRight * vFarHalfExtents.x + m_IntermediateProperties.m_vUp * vFarHalfExtents.y;
	m_IntermediateProperties.m_FrustumCorners[kFrustumCorner_FarUpperRight] = vFarCenter + m_IntermediateProperties.m_vRight * vFarHalfExtents.x + m_IntermediateProperties.m_vUp * vFarHalfExtents.y;
	m_IntermediateProperties.m_FrustumCorners[kFrustumCorner_FarLowerRight] = vFarCenter + m_IntermediateProperties.m_vRight * vFarHalfExtents.x - m_IntermediateProperties.m_vUp * vFarHalfExtents.y;
	m_IntermediateProperties.m_FrustumCorners[kFrustumCorner_FarLowerLeft] = vFarCenter - m_IntermediateProperties.m_vRight * vFarHalfExtents.x - m_IntermediateProperties.m_vUp * vFarHalfExtents.y;
}
//===================================================================================================
// Projection
//---------------------------------------------------------------------------------------------------
void CameraComponent::SetLens(
	const float _fFovYRad, 
	const float _fAspectRatio, 
	const float _fNearDistance, 
	const float _fFarDistance)
{
	// save projection properties
	m_IntermediateProperties.m_fFovY = _fFovYRad;
	m_IntermediateProperties.m_fAspectRatio = _fAspectRatio;
	m_IntermediateProperties.m_fNearDistance = _fNearDistance;
	m_IntermediateProperties.m_fFarDistance = _fFarDistance;

	// window size in the frustum
	m_IntermediateProperties.m_fNearWindowHeight = 2.0f * m_IntermediateProperties.m_fNearDistance * tanf(0.5f * m_IntermediateProperties.m_fFovY);
	m_IntermediateProperties.m_fFarWindowHeight = 2.0f * m_IntermediateProperties.m_fFarDistance * tanf(0.5f * m_IntermediateProperties.m_fFovY);

	// generate projection matrix
	m_IntermediateProperties.m_mProj = Math::MatrixPerspectiveFovLH(m_IntermediateProperties.m_fFovY, m_IntermediateProperties.m_fAspectRatio, m_IntermediateProperties.m_fNearDistance, m_IntermediateProperties.m_fFarDistance);

	UpdateIntermediateProperties();
}
//---------------------------------------------------------------------------------------------------
void CameraComponent::SetAspectRatio(const float _fAspectRatio)
{
	SetLens(m_IntermediateProperties.m_fFovY, _fAspectRatio, m_IntermediateProperties.m_fNearDistance, m_IntermediateProperties.m_fFarDistance);
}
//---------------------------------------------------------------------------------------------------
void CameraComponent::SetFOV(const float _fFovYDeg)
{
	SetLens(Math::DegToRad(_fFovYDeg), m_IntermediateProperties.m_fAspectRatio, m_IntermediateProperties.m_fNearDistance, m_IntermediateProperties.m_fFarDistance);
}
//===================================================================================================
// Query
//---------------------------------------------------------------------------------------------------
float3 CameraComponent::ScreenPosToWorldRay(const float2& _vScreenPos) const
{
	/// Normalized Device Coordinates [-1,1]
	float2 vRayNDC = (2.0f * _vScreenPos);
	vRayNDC.x = vRayNDC.x - 1.0f;
	vRayNDC.y = 1.0f - vRayNDC.y;

	/// Homogeneous Clip Space
	float4 vRayClip = float4(vRayNDC.x, vRayNDC.y, 1.0f, 1.0f);

	/// Eye Space (camera is origin and xyz axes are aligned)
	float4 vRayEye = m_IntermediateProperties.m_mProj.getInverse().transform(vRayClip);
	vRayEye = float4(vRayEye.x, vRayEye.y, 1.0f, 0.0f);

	/// World Space (normalized)
	float4 vRayWorld = m_IntermediateProperties.m_mView.getInverse().transform(vRayEye);
	vRayWorld.w = 0.0f;
	vRayWorld.normalize();
	return vRayWorld.getXYZ();
}
//---------------------------------------------------------------------------------------------------
float2 CameraComponent::WorldToScreenPos(const float3& _vWorldPos) const
{
	float4 vResult = m_IntermediateProperties.m_mViewProj.transform(float4(_vWorldPos, 1.0f));
	return vResult.xy / vResult.w;
}
//---------------------------------------------------------------------------------------------------
float3 CameraComponent::GetProjectedPointOnNearPlane(const float3& _vOther) const
{
	float3 vNearCenter = m_IntermediateProperties.m_vPosition + m_IntermediateProperties.m_fNearDistance * m_IntermediateProperties.m_vLook;

	float3 vTmp;
	vTmp.x = m_IntermediateProperties.m_vLook.dot(_vOther - vNearCenter);
	vTmp.y = m_IntermediateProperties.m_vLook.dot(m_IntermediateProperties.m_vLook); /// m_vLook == normal of near plane
	vTmp.z = vTmp.x / vTmp.y;

	return _vOther + vTmp.z * m_IntermediateProperties.m_vLook;
}
//---------------------------------------------------------------------------------------------------
void CameraComponent::Raycast(TVisibleObjects& _Objects)
{
	Raycast(m_IntermediateProperties.m_vPosition, m_IntermediateProperties.m_vLook, m_IntermediateProperties.m_fFarDistance, _Objects);
}
//---------------------------------------------------------------------------------------------------
void CameraComponent::Raycast(
	const float3& _vOrigin,
	const float3& _vUnitDir,
	const float _fDistance,
	TVisibleObjects& _Objects)
{
	if (m_pVolumeCache == nullptr)
	{
		HLOG("Initializing default VolumeCache for %s camera", CSTR(GetName()));
		InitializeVolumeCache();
	}

	if (m_pVolumeCache != nullptr)
	{
		m_pVolumeCache->Raycast(_vOrigin, _vUnitDir, _fDistance, _Objects);
	}
}
//---------------------------------------------------------------------------------------------------
GameObject* CameraComponent::Raycast(
	const float3& _vOrigin,
	const float3& _vUnitDir,
	const float _fDistance,
	RaycastHitInfo* _pHitInfo,
	TCollisionFilterMask _CollisionFilters) const
{
	if (m_pVolumeCache == nullptr)
	{
		HLOG("Initializing default VolumeCache for %s camera", CSTR(m_sName));
		InitializeVolumeCache();
	}

	GameObject* pHitObject = nullptr;
	if (m_pVolumeCache)
	{
		pHitObject = m_pVolumeCache->Raycast(_vOrigin, _vUnitDir, _fDistance, _pHitInfo, &QueryFilter(CollisionFilter(_CollisionFilters)));
	}
	return pHitObject;
}
//===================================================================================================
// Culling
//---------------------------------------------------------------------------------------------------
const CullingFrustum CameraComponent::GetCullingFrustum() const
{
	Physics::GeoBox aabb;

	aabb.halfExtents.x = (m_IntermediateProperties.m_fFarWindowHeight / m_IntermediateProperties.m_fAspectRatio) * 0.5f;
	aabb.halfExtents.y = (m_IntermediateProperties.m_fFarDistance - m_IntermediateProperties.m_fNearDistance) * 0.5f;
	aabb.halfExtents.z = m_IntermediateProperties.m_fFarWindowHeight * 0.5f;

	transform trans = GetParent()->GetTransform();
	trans.p += GetLook() * aabb.halfExtents.z; // TODO: check if .z is correct here.. 

	return CullingFrustum(aabb, trans);
}

//---------------------------------------------------------------------------------------------------
bool CameraComponent::InitializeVolumeCache(uint32_t _uStaticObjects, uint32_t _uDynamicObjects) const
{
	if (m_pVolumeCache != nullptr)
	{
		return false;
	}

	m_pVolumeCache = new VolumeCache(_uStaticObjects, _uDynamicObjects);

	return m_pVolumeCache != nullptr;
}
//---------------------------------------------------------------------------------------------------
void CameraComponent::Gather(_Out_ TVisibleObjects& _ObjectsInCameraFrustum)
{
	if (m_pVolumeCache == nullptr)
	{
		HLOG("Initializing default VolumeCache for %s camera", CSTR(m_sName));
		InitializeVolumeCache();
	}

	if (m_pVolumeCache != nullptr)
	{
		m_pVolumeCache->Update(Physics::GeoSphere(m_IntermediateProperties.m_fFarDistance), GetTransform());

		switch (m_kCullingGeometry)
		{
		case kCullingGeometry_Frustum:
			{
				CullingFrustum Frustum = GetCullingFrustum();
				m_pVolumeCache->Gather(Frustum.GetAABB(), Frustum.GetTransform(), _ObjectsInCameraFrustum);
			}
			break;
		case kCullingGeometry_Sphere:
			{
				m_pVolumeCache->Gather(GeoSphere(m_CullingSphere.fRadius), GetTransform(), _ObjectsInCameraFrustum);
			}
			break;
		default:
			break;
		}
	}
}
//---------------------------------------------------------------------------------------------------