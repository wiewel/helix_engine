//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "RotationComponent.h"
#include "DataStructures\BaseObject.h"
#include "Resources\CommonTypeTextSerializer.h"

using namespace Helix::Scene;
using namespace Helix::Datastructures;
using namespace Helix::Math;
//---------------------------------------------------------------------------------------------------
// default constructor
RotationComponent::RotationComponent(IBaseObject* _pParent, float3 _vAxis, float _fSpeed, bool _bLocal, bool _bPaused) :
	IComponent(_pParent, ObjectType_BaseObject, ComponentCallback_OnUpdate | ComponentCallback_OnSerialize),
	m_vAxis(_vAxis), 
	m_fSpeed(_fSpeed),
	m_bLocal(_bLocal),
	m_bPaused(_bPaused)
{
}
//---------------------------------------------------------------------------------------------------
RotationComponent::~RotationComponent()
{
}
//---------------------------------------------------------------------------------------------------
// deserialzation constructor
RotationComponent::RotationComponent(
	IBaseObject* _pParent,
	const ObjectType _kObjectType,
	TComponentCallbacks _kCallbacks) :
	IComponent(_pParent, ObjectType_BaseObject, _kCallbacks | ComponentCallback_OnUpdate | ComponentCallback_OnSerialize)
{
}
//---------------------------------------------------------------------------------------------------

void RotationComponent::OnUpdate(double _fDeltaTime, double _fTotalTime)
{
	if (m_bPaused)
		return;

	Math::quaternion vRot(static_cast<float>(m_fSpeed * _fDeltaTime), m_vAxis.getNormalized());

	GetParent()->Rotate(vRot, m_bLocal);
}
//---------------------------------------------------------------------------------------------------
void RotationComponent::OnDeserialize(const hlx::TextToken& _Token)
{
	m_bLocal = _Token.get<bool>("Local", true);
	m_bPaused = _Token.get<bool>("Paused", false);
	m_fSpeed = _Token.get<float>("Speed", 1.f);
	m_vAxis = Resources::to(_Token.getVector<float, 3>("Axis", { 0.f, 1.f, 0.f }));
}
//---------------------------------------------------------------------------------------------------
bool RotationComponent::OnSerialize(hlx::TextToken& _Token) const
{
	_Token.set("Local", m_bLocal);
	_Token.set("Paused", m_bPaused);
	_Token.set("Speed", m_fSpeed);
	_Token.setVector<float, 3>("Axis", Resources::to(m_vAxis));

	return true;
}
//---------------------------------------------------------------------------------------------------
SyncComponent* RotationComponent::GetSyncComponent()
{
#ifdef HNUCLEO
	return &m_SyncComponent;
#else
	return nullptr;
#endif // HNUCLEO
}
//---------------------------------------------------------------------------------------------------
