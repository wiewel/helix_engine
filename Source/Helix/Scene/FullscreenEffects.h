//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef FULLSCREENEFFECTS_H
#define FULLSCREENEFFECTS_H

#include "Display\DX11\RenderObjectDX11.h"

namespace Helix
{
	namespace Scene
	{
		class FullscreenEffects
		{
		public:
			FullscreenEffects() {}

			FullscreenEffects(const std::string& _sCameraName, uint64_t _kPasses, bool _bDebug = false);

			~FullscreenEffects();

			bool Initialize(const std::string& _sCameraName, uint64_t _kPasses, bool _bDebug = false);

			bool Uninitialize();

			void Enable(uint64_t _kPassID);
			//use HUNDEFINED64 to disable all passes in this fullscreen effect
			void Disable(uint64_t _kPassID = HUNDEFINED64);
		private:
			std::string m_sCameraName;
			Display::RenderObjectDX11 m_FullscreenObject;
		};

		inline void FullscreenEffects::Enable(uint64_t _kPassID)
		{
			if (m_FullscreenObject.GetMaterials().empty() == false)
			{
				m_FullscreenObject.GetMaterials().front().EnableRenderPass(_kPassID);
			}
		}

		//use HUNDEFINED64 to disable all passes in this fullscreen effect
		inline void FullscreenEffects::Disable(uint64_t _kPassID)
		{
			if (m_FullscreenObject.GetMaterials().empty() == false)
			{
				m_FullscreenObject.GetMaterials().front().DisableRenderPass(_kPassID);
			}
		}
	}
}
#endif // !FULLSCREENEFFECTS_H
