//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "JointComponent.h"
#include "Scene\PhysicsScene.h"

using namespace Helix::Scene;
using namespace Helix::Datastructures;
using namespace physx;
//===================================================================================================
//---------------------------------------------------------------------------------------------------
JointComponent::JointComponent(GameObject * _pParent, JointType _Type, PxRigidActor* _pOtherActor)
	: IGameObjectComponent(
		_pParent,
		ObjectType_GameObject,
		ComponentCallback_OnUpdate)
{
	m_Type = _Type;
	m_pParentActor = GetParent()->GetActor();
	m_LocalTransform.p = Math::float3(1, 0, 0);
	m_JointTransform.p = Math::float3(1, 0, 0);
	Attach(_Type, _pOtherActor);
}
//---------------------------------------------------------------------------------------------------
JointComponent::~JointComponent()
{
	safe_release(m_pJoint);
}
//---------------------------------------------------------------------------------------------------
inline void JointComponent::Attach(JointType _Type, physx::PxRigidActor* _pOtherActor)
{
	PxPhysics* pPhysics = PhysicsScene::Instance()->GetPhysics();
	HASSERT(pPhysics != nullptr, "Physics scene is not initialized yet.");

	m_pOtherActor = _pOtherActor;
	m_Type = _Type;

	switch (_Type)
	{
	case Helix::Scene::JointComponent::JointType_None:
		// Destroy the joint
		safe_release(m_pJoint);
		break;
	case Helix::Scene::JointComponent::JointType_Fixed:
		safe_release(m_pJoint);
		m_pJoint = PxFixedJointCreate((*pPhysics), m_pOtherActor, m_LocalTransform, m_pParentActor, m_JointTransform);
		break;
	case Helix::Scene::JointComponent::JointType_Distance:
		safe_release(m_pJoint);
		m_pJoint = PxDistanceJointCreate((*pPhysics), m_pParentActor, m_LocalTransform, m_pOtherActor, m_JointTransform);
		break;
	case Helix::Scene::JointComponent::JointType_Spherical:
		safe_release(m_pJoint);
		m_pJoint = PxSphericalJointCreate((*pPhysics), m_pParentActor, m_LocalTransform, m_pOtherActor, m_JointTransform);
		break;
	case Helix::Scene::JointComponent::JointType_Revolute:
		safe_release(m_pJoint);
		m_pJoint = PxRevoluteJointCreate((*pPhysics), m_pParentActor, m_LocalTransform, m_pOtherActor, m_JointTransform);
		break;
	case Helix::Scene::JointComponent::JointType_Prismatic:
		safe_release(m_pJoint);
		m_pJoint = PxPrismaticJointCreate((*pPhysics), m_pParentActor, m_LocalTransform, m_pOtherActor, m_JointTransform);
		break;
	case Helix::Scene::JointComponent::JointType_D6:
		safe_release(m_pJoint);
		m_pJoint = PxD6JointCreate((*pPhysics), m_pParentActor, m_LocalTransform, m_pOtherActor, m_JointTransform);
		break;
	default:
		break;
	}
}