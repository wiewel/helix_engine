//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "ScriptScene.h"
#include "Scene\ScriptObject.h"
#include "Util\AdvancedProfiler.h"
#include "Util\Functional.h"

using namespace Helix::Scene;
using namespace Helix::Async;

//---------------------------------------------------------------------------------------------------

ScriptScene::ScriptScene() : Scene()
{

}
//---------------------------------------------------------------------------------------------------

ScriptScene::~ScriptScene()
{
	for (IScriptObject* pScript : m_ScriptObjects)
	{
		pScript->m_bStaticScript = pScript->IsStaticScript();
	}
}

//---------------------------------------------------------------------------------------------------
bool ScriptScene::Begin(bool _bStartSupended)
{
	HLOG("BeginScene...");

	//HGUIPROFILE(BeginScripts)

	{
		std::lock_guard<std::mutex> Lock(m_ScriptLockMutex);

		// execute all main scripts
		Helix::conditional_parallel_for_each(m_ScriptObjects.begin(), m_ScriptObjects.end(),2u, [&](IScriptObject* pScript)
		{
			pScript->OnSceneBegin();
		});
	}

	//HGUIPROFILE_END(BeginScripts, ScriptScene)

	return StartInNewThread(_bStartSupended, false);
}
//---------------------------------------------------------------------------------------------------
void ScriptScene::End()
{
	Stop();

	//HPROFILE(EndScripts)

	{
		std::lock_guard<std::mutex> Lock(m_ScriptLockMutex);

		// execute all main scripts
		Helix::conditional_parallel_for_each(m_ScriptObjects.begin(), m_ScriptObjects.end(), 2u, [&](IScriptObject* pScript)
		{
			pScript->OnSceneEnd();
		});
	}

	//HPROFILE_END(EndScripts)
}
//---------------------------------------------------------------------------------------------------
void ScriptScene::RegisterScript(IScriptObject* _pScript)
{
	std::lock_guard<std::mutex> lock(m_ScriptLockMutex);
	m_ScriptObjects.push_back(_pScript);
}
//---------------------------------------------------------------------------------------------------
void ScriptScene::UnregisterScript(IScriptObject* _pScript)
{
	std::lock_guard<std::mutex> lock(m_ScriptLockMutex);

	std::vector<IScriptObject*>::iterator it = std::find(m_ScriptObjects.begin(), m_ScriptObjects.end(), _pScript);

	if (it != m_ScriptObjects.end())
	{
		m_ScriptObjects.erase(it);
	}
}
//---------------------------------------------------------------------------------------------------

void ScriptScene::LevelLoaded()
{
	//std::lock_guard<std::mutex> lock(m_ScriptLockMutex);

	Helix::conditional_parallel_for_each(m_ScriptObjects.begin(), m_ScriptObjects.end(), 2u, [&](IScriptObject* pScript)
	{
		pScript->OnLevelLoaded();
	});
}
//---------------------------------------------------------------------------------------------------

void ScriptScene::Cleanup()
{
	HGUIPROFILE(CleanUp)

	std::lock_guard<std::mutex> lock(m_ScriptLockMutex);

	for(IScriptObject* pScript : m_ScriptObjects)
	{
		pScript->OnDestroyObjects();
	}

	HGUIPROFILE_END(CleanUp, ScriptScene)
}
//---------------------------------------------------------------------------------------------------

void ScriptScene::PreRendering()
{
	HGUIPROFILE(PreRendering)

	std::lock_guard<std::mutex> lock(m_ScriptLockMutex);

	// execute serial
	for (IScriptObject* pScript : m_ScriptObjects)
	{
		pScript->OnPreRendering();
	}

	HGUIPROFILE_END(PreRendering, ScriptScene)
}
//---------------------------------------------------------------------------------------------------
void ScriptScene::Update(double _fDeltaTime, double _fTotalTime)
{
	HGUIPROFILE(Update)
	//---------------------------------------------------------------------------------------------------
	// update scripts
	//---------------------------------------------------------------------------------------------------

	{
		std::lock_guard<std::mutex> lock(m_ScriptLockMutex);

		// remove finished scripts
		for (std::vector<IScriptObject*>::iterator it = m_ScriptObjects.begin(); it != m_ScriptObjects.end();)
		{
			IScriptObject* pScript = *it;

			if (pScript->Terminated())
			{
				// remove terminated script
				it = m_ScriptObjects.erase(it);
			}
			else
			{
				++it;
			}
		}

		// execute all main scripts
		Helix::conditional_parallel_for_each(m_ScriptObjects.begin(), m_ScriptObjects.end(), 2u, [&](IScriptObject* pScript)
		{
			pScript->OnUpdate(_fDeltaTime, _fTotalTime);
		});
	}

	HGUIPROFILE_END(Update, ScriptScene)
}
//---------------------------------------------------------------------------------------------------
