//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "SceneGuard.h"

#include "RenderingScene.h"
#include "PhysicsScene.h"
#include "ScriptScene.h"
#include "ComponentScene.h"

using namespace Helix::Scene;
//---------------------------------------------------------------------------------------------------
SceneGuard::SceneGuard()
{
	Lock();
}
//---------------------------------------------------------------------------------------------------
SceneGuard::~SceneGuard()
{
	Unlock();
}
//---------------------------------------------------------------------------------------------------
void SceneGuard::Lock()
{
	PhysicsScene::Instance()->Pause(true);

	RenderingScene::Instance()->Pause(true);
	RenderingScene::Instance()->Lock(); // lock qt hosted rendering

	ScriptScene::Instance()->Pause(true);
	ComponentScene::Instance()->Pause(true);
}
//---------------------------------------------------------------------------------------------------
void SceneGuard::Unlock()
{
	PhysicsScene::Instance()->Continue();

	RenderingScene::Instance()->Unlock();
	RenderingScene::Instance()->Continue();

	ScriptScene::Instance()->Continue();
	ComponentScene::Instance()->Continue();
}
//---------------------------------------------------------------------------------------------------