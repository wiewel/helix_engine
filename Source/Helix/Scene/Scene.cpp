//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "Scene.h"

using namespace Helix::Scene;

Scene::Scene(const double& _fUpdateInterval, const double& _fIntialTimeStep) :
	//no observer, looped execution, no suspend after execution
	Async::IAsyncObject(nullptr, nullptr, true, false),
	m_fUpdateInterval(_fUpdateInterval),
	m_fDeltaTime(_fIntialTimeStep)
{
}
//---------------------------------------------------------------------------------------------------
Scene::~Scene()
{
}
//---------------------------------------------------------------------------------------------------

void Scene::Execute()
{
	m_Timer.Reset();

	// execute step
	Update(m_fDeltaTime, m_fTotalDeltaTime);

	//all tasks are done, measure time it took
	double fDeltaTime = m_Timer.TotalTimeD();
	m_fExecutionTime = fDeltaTime;

	if (m_fUpdateInterval > 0.0)
	{
		//calculate remaining time for UpdateInterval to be complete
		int64_t iTimeToSleepInMicroSec = static_cast<int64_t>((m_fUpdateInterval - fDeltaTime) * 1000000.0);

		//sleep for remaining time
		if (iTimeToSleepInMicroSec > 0)
		{
			auto end = std::chrono::system_clock::now() + std::chrono::microseconds(iTimeToSleepInMicroSec);
			std::unique_lock<std::mutex> wait(m_WaitMutex);
			m_CV.wait_until(wait, end);

			//get new total time
			double fResumeTime = m_Timer.TotalTimeD();

			++m_uNumOfResumes;

			//update the delta time
			fDeltaTime = fResumeTime;
		}
	}

	m_fTotalDeltaTime += fDeltaTime;
	m_fDeltaTime = fDeltaTime;
	++m_uNumOfSimulations;

	if (m_uStopAfterXSteps != 0)
	{
		if (m_uNumOfSimulations >= m_uStopAfterXSteps)
		{
			//StopInternal();
			PauseInternal();
		}
	}
}
//---------------------------------------------------------------------------------------------------