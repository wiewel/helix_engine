//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef SCRIPTSCENE_H
#define SCRIPTSCENE_H

#include "Scene.h"

#include "DataStructures\GameObjectPool.h"

namespace Helix
{
	// forward declarations
	namespace Scene
	{
		class ScriptScene : public Scene, public hlx::Singleton<ScriptScene>
		{
			friend class IScriptObject;
			friend class RenderingScene;
			friend class Level;
		public:
			ScriptScene();
			~ScriptScene();

			bool Begin(bool _bStartSupended = false);
			void End();

		private:
			void Update(double _fDeltaTime, double _fTotalTime);

			void RegisterScript(IScriptObject* _pScript);
			void UnregisterScript(IScriptObject* _pScript);

			void LevelLoaded();

			// invokes OnDestroyGameObjects on scripts
			void Cleanup();

			// invokes OnPreRendering serially
			void PreRendering();
		private:

			// Scripting:
			std::vector<IScriptObject*> m_ScriptObjects;
			std::mutex m_ScriptLockMutex;
		};

	}
} // Helix

#endif // !SCRIPTSCENE_H
