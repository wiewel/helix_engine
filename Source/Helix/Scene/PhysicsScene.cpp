//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher, David Labode
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "PhysicsScene.h"
#include "Scene\ScriptObject.h"
#include "hlx\src\Profiler.h"
#include "Physics\GeometryTypes.h"
#include "extensions\PxDefaultSimulationFilterShader.h"
#include "Physics\SceneLock.h"
#include "Physics\SimulationEventCallback.h"
#include "Physics\SimulationFilterShader.h"
#include "Physics\ControllerComponent.h"
#include "Physics\QueryFilterCallback.h"

using namespace Helix::Math;
using namespace Helix::Scene;
using namespace Helix::Async;
using namespace Helix;

PhysicsScene::PhysicsScene() : 
	Scene(), m_pFactory(Physics::PhysicsFactory::Instance())
{
	if (m_pFactory->Initialize())
	{
		m_pPhysics = m_pFactory->GetPhysics();
	}
}
//---------------------------------------------------------------------------------------------------
PhysicsScene::~PhysicsScene()
{
	DisconnectPVD();

	if (m_pScene != nullptr) 
	{
		HWARNING("Physics scene might not have been terminated properly using Scene::End");
	}

	HSAFE_FUNC_RELEASE(m_pControllerManager, release);
	HSAFE_FUNC_RELEASE(m_pScene, release);
}

//---------------------------------------------------------------------------------------------------
bool PhysicsScene::Initialize()
{
	HSAFE_FUNC_RELEASE(m_pScene, release);

	using namespace physx;

	PxSceneDesc Desc(m_pFactory->GetToleranceScale());
	Desc.gravity = { 0.f, -9.81f, 0.f };
	
	Desc.simulationEventCallback = Physics::SimulationEventCallback::Instance();
	Desc.contactModifyCallback = nullptr;
	Desc.ccdContactModifyCallback = nullptr;

	Desc.filterShaderData = nullptr;
	Desc.filterShaderDataSize = 0;
	Desc.filterShader = &Physics::SimulationFilterShader;
	Desc.filterCallback = nullptr;

	Desc.broadPhaseType = PxBroadPhaseType::eSAP;
	Desc.broadPhaseCallback = nullptr;	
	Desc.limits.setToDefault();
	Desc.frictionType = PxFrictionType::ePATCH;
	Desc.bounceThresholdVelocity = 0.2f * std::abs(Desc.gravity.y);

	Desc.cpuDispatcher = m_pFactory->GetCPUDispatcher();
	// TODO: CUDA dispatcher

	Desc.staticStructure = PxPruningStructure::eSTATIC_AABB_TREE; // eDYNAMIC_AABB_TREE is default
	Desc.dynamicStructure = PxPruningStructure::eDYNAMIC_AABB_TREE;
	Desc.dynamicTreeRebuildRateHint = 100;

	if (m_pScene == nullptr)
	{
		m_pScene = m_pPhysics->createScene(Desc);
		SetGravity(float3( 0.f, -9.81f, 0.f ));
		if (m_pScene == nullptr)
		{
			HFATAL("Failed to create PhysX::Scene!");
			return false;
		}
	}
	
	HDBG(ConnectToPVD());

	m_pControllerManager = PxCreateControllerManager(*m_pScene, true);

	return true;
}

//---------------------------------------------------------------------------------------------------

bool PhysicsScene::ConnectToPVD(const char* _sIP, int32_t _iPort, uint32_t _uTimeOut)
{
	// locks the scene it self
	if (m_pPVD != nullptr)
	{
		DisconnectPVD();
		return false;
	}

	Physics::SceneWriteLock lock;

	HLOG("Connecting to PVD at %s:%d (%u ms timeout)", CSTR(_sIP), _iPort, _uTimeOut);

	physx::debugger::comm::PvdConnectionManager* pConnectionManager = m_pPhysics->getPvdConnectionManager();

	if (pConnectionManager == nullptr)
	{
		HERROR("Failed to get PVD connection manager");
		return false;
	}

	// consoles and remote PCs need a higher timeout.
	physx::PxVisualDebuggerConnectionFlags connectionFlags = physx::PxVisualDebuggerExt::getAllConnectionFlags();

	// and now try to connect
	m_pPVD = physx::PxVisualDebuggerExt::createConnection(pConnectionManager, _sIP, _iPort, _uTimeOut, connectionFlags);

	if (m_pPVD == nullptr || m_pPVD->isConnected() == false)
	{
		HERROR("Failed to connect to PVD");
		return false;
	}

	return true;
}
//---------------------------------------------------------------------------------------------------

void PhysicsScene::DisconnectPVD()
{
	if (m_pPVD != nullptr)
	{
		HLOG("Disconnecting PVD");
		Physics::SceneWriteLock lock;
		m_pPVD->disconnect();
		m_pPVD->release();
		m_pPVD = nullptr;
	}
}
//---------------------------------------------------------------------------------------------------
// first hit raycast
bool PhysicsScene::Raycast(Math::float3 _vOrigin, Math::float3 _vUnitDir, float _fDistance, _Out_ Datastructures::GameObject*& _pHit, Datastructures::GameObject* _pCaster) const
{
	_pHit = nullptr;
	if (m_pScene == nullptr)
	{
		HERRORD("Raycast before scene initialization");
		return false;
	}
	
	physx::PxRaycastBuffer HitBuffer;
	Physics::ControllerQueryFilterCallback QueryFilterCallback;
	physx::PxQueryFilterData FilterData;

	Physics::SceneReadLock lock(m_pScene);
	FilterData.flags = physx::PxQueryFlag::ePREFILTER | physx::PxQueryFlag::eDYNAMIC | physx::PxQueryFlag::eSTATIC;
	QueryFilterCallback.SetParent(_pCaster);

	bool bHit = m_pScene->raycast(_vOrigin, _vUnitDir, _fDistance, HitBuffer, physx::PxHitFlag::eDEFAULT | physx::PxHitFlag::eMESH_MULTIPLE, FilterData, &QueryFilterCallback);
	if (bHit)
	{
		_pHit = static_cast<Datastructures::GameObject*>(HitBuffer.block.actor->userData);
	}
	return bHit;
}
//---------------------------------------------------------------------------------------------------
bool PhysicsScene::Sweep(
	Physics::GeoBase& _Geometry, 
	Math::transform& _InitialPose, 
	Math::float3& _vSweepDir, 
	float _fDistance, 
	Physics::QueryFilterCallback& _QueryFilter, 
	Datastructures::GameObject*& _pHit) const
{
	_pHit = nullptr;

	if (m_pScene == nullptr)
	{
		HERRORD("Sweep before scene initialization");
		return false;
	}

	physx::PxSweepBuffer HitBuffer;
	Physics::SceneReadLock Lock(m_pScene);
	physx::PxQueryFilterData FilterData;

	FilterData.flags = physx::PxQueryFlag::ePREFILTER | physx::PxQueryFlag::eDYNAMIC | physx::PxQueryFlag::eSTATIC;

	bool bHit = m_pScene->sweep(_Geometry, _InitialPose, _vSweepDir, _fDistance, HitBuffer, physx::PxHitFlag::eDEFAULT | physx::PxHitFlag::eMESH_MULTIPLE, FilterData, &_QueryFilter);
	if (bHit)
	{
		_pHit = static_cast<Datastructures::GameObject*>(HitBuffer.block.actor->userData);
	}

	return bHit;
}
//---------------------------------------------------------------------------------------------------
void PhysicsScene::Update(double _fDeltaTime, double _fTotalTime)
{
	HPROFILE(PhysicsScene);

	{
		if(m_bSimulate)
		{
			std::lock_guard<std::mutex> lock(m_WriteLock);
			m_pScene->lockWrite();
			uint32_t uErrorCode = 0;
			if (m_pScene->fetchResults(true, &uErrorCode) == false) // fetch previous results
			{
				if (uErrorCode != 0)
				{
					HWARNING("Error %u occured on PhysX fetchResults.", uErrorCode);
				}
			}
			m_pScene->unlockWrite();
		}

		{
			std::lock_guard<std::mutex> lock(m_WriteLock);
			m_pScene->lockWrite();
			Datastructures::GameObjectPool::Instance()->Synchronize();
			m_pScene->unlockWrite();
		}

		if(m_bSimulate)
		{
			std::lock_guard<std::mutex> lock(m_WriteLock);
			m_pScene->lockWrite();
			m_pScene->simulate(static_cast<float>(_fDeltaTime));
			m_pScene->unlockWrite();
		}
	}
	HPROFILE_END(PhysicsScene)
}
//---------------------------------------------------------------------------------------------------
void PhysicsScene::End()
{
	if (m_pScene != nullptr)
	{
		Physics::SimulationEventCallback::Instance()->Clear();
	}

	if (m_pScene != nullptr && m_pScene->checkResults(true)/* m_bSimulate*/)
	{
		// need to fetch the last results before releasing the scene
		m_pScene->lockWrite();

		uint32_t uErrorCode = 0;
		if (m_pScene->fetchResults(true, &uErrorCode) == false) // fetch previous results
		{
			HASSERT(false, "PhysX: fetchResults failed");
		}
		if (uErrorCode != 0u)
		{
			HWARNING("Error %u occured on PhysX fetchResults.", uErrorCode);
		}

		m_pScene->unlockWrite();
	}
	DisconnectPVD();

	HSAFE_FUNC_RELEASE(m_pControllerManager, release);
	HSAFE_FUNC_RELEASE(m_pScene, release);
}
//---------------------------------------------------------------------------------------------------
void PhysicsScene::SetGravity(float3 & _vGravityForce)
{
	if (m_pScene)
	{
		Physics::SceneWriteLock Lock;
		m_pScene->setGravity(_vGravityForce);
	}
}
//---------------------------------------------------------------------------------------------------
float3 PhysicsScene::GetGravity() const
{
	if (m_pScene)
	{
		Physics::SceneReadLock Lock(m_pScene);
		return m_pScene->getGravity();
	}
	return float3(0, 0, 0);
}
//---------------------------------------------------------------------------------------------------
