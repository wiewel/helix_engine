//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef CAMERAMANAGER_H
#define CAMERAMANAGER_H

#include "hlx\src\Singleton.h"
#include "Async\SpinLock.h"
#include <unordered_map>
#include "Util\UniqueAssociationID.h"

namespace Helix
{
	namespace Scene
	{
		class CameraComponent;
		class CameraManager : public hlx::Singleton<CameraManager>
		{
			friend class CameraComponent;
		public:
			CameraManager();
			~CameraManager();

			template <class Vector>
			void GetCameras(_Out_ Vector& _OutCameras)
			{
				Async::ScopedSpinLock lock(m_CreateLock);

				for (auto it : m_Cameras)
				{
					_OutCameras.push_back(it.second);
				}
			}

			template <class Vector>
			void GetActiveCameras(Vector& _OutCameras, const uint64_t& _kPasses = 0u)
			{
				Async::ScopedSpinLock lock(m_CreateLock);

				for (auto it : m_Cameras)
				{
					if (it.second->IsActive() && it.second->CheckFlag(_kPasses))
					{
						_OutCameras.push_back(it.second);
					}
				}
			}

			CameraComponent* GetCameraByName(const std::string& _sName);
		
		private:
			// registers camera and returns unique power of two ID
			uint64_t RegisterCamera(CameraComponent* _pCameraComponent);
			void RemoveCamera(CameraComponent* _pCameraComponent);
			void RenameCamera(CameraComponent* _pCameraComponent, std::string _sNewName);

		private:
			Async::SpinLock m_CreateLock;
			// maybe remove constness from cameras (but then again the creator should be the only one changing the camera)
			std::unordered_map<std::string, CameraComponent*> m_Cameras;

			// mapps camera name to unique id, might overflow if there are more than 64 different cameras, but thats okay i guess
			UniqueAssociationID<std::string, uint64_t> m_CameraIDs;
		};
	} // Scene
} // Helix

#endif // !CAMERAMANAGER_H
