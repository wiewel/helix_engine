//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "VolumeCache.h"
#include "Util\Functional.h"
#include "Physics\VolumeCacheIterator.h"
#include "DataStructures\GameObject.h"
#include "PhysicsScene.h"
#include "Physics\OverlapsCallback.h"
#include "Physics\RaycastCallback.h"

using namespace Helix::Math;
using namespace Helix::Scene;
using namespace Helix::Physics;
//---------------------------------------------------------------------------------------------------

VolumeCache::VolumeCache(uint32_t _uStaticObjects, uint32_t _uDynamicObjects) :
	m_pScene(PhysicsScene::Instance()->GetScene())
{
	HASSERT(m_pScene != nullptr, "Scene is not initialized yet");
	
	m_pVolumeCache = m_pScene->createVolumeCache(_uStaticObjects, _uDynamicObjects),
	m_Volume.radius = 1.f;
	m_Transform = HTRANSFORM_IDENTITY;

	HASSERT(m_pVolumeCache != nullptr, "Failed to create PhysX::VolumeCache!");
}
//---------------------------------------------------------------------------------------------------

VolumeCache::~VolumeCache()
{
	safe_release(m_pVolumeCache);
}
//---------------------------------------------------------------------------------------------------
bool VolumeCache::Update(const GeoSphere& _Volume, const transform& _Transform)
{
	physx::PxVolumeCache::FillStatus kStatus = physx::PxVolumeCache::FILL_OK;
	
	if (_Volume.radius > 0.f && _Transform.isValid() &&
		(m_Volume.radius != _Volume.radius ||
		m_Transform.operator==(_Transform) == false))
	{
		m_Volume = _Volume;
		m_Transform = _Transform;

		SceneWriteLock lock;

		kStatus = m_pVolumeCache->fill(_Volume, _Transform);
	}

	switch (kStatus)
	{
	case physx::PxVolumeCache::FILL_OVER_MAX_COUNT:
		HWARNINGD("VolumeCache full");
	case physx::PxVolumeCache::FILL_OK:
		return true;
		//case physx::PxVolumeCache::FILL_UNSUPPORTED_GEOMETRY_TYPE:
	case physx::PxVolumeCache::FILL_OUT_OF_MEMORY:
		HERROR("VolumeCache out of Memory!");
	default:
		return false;
	}

	return kStatus == physx::PxVolumeCache::FILL_OK;
}
//---------------------------------------------------------------------------------------------------
void VolumeCache::Gather(
	const GeoBase& _Volume,
	const transform& _Transform,
	TVisibleObjects& _ObjectsInVolume,
	const QueryFilter* _pQueryFilter,
	QueryFilterCallback* _pCustomFilterCallback)
{
	SceneReadLock lock(m_pScene);
	bool bStatus;

	Physics::OverlapCallback<TVisibleObjects> Overlaps(_ObjectsInVolume);
	if (_pQueryFilter != nullptr && _pCustomFilterCallback != nullptr)
	{
		bStatus = m_pVolumeCache->overlap(_Volume, _Transform, Overlaps, *_pQueryFilter, _pCustomFilterCallback);
	}
	else
	{
		bStatus = m_pVolumeCache->overlap(_Volume, _Transform, Overlaps);
	}
}
//---------------------------------------------------------------------------------------------------
void VolumeCache::Raycast(
	const float3& _vOrigin,
	const float3& _vUnitDir,
	const float _fDistance,
	TVisibleObjects& _Objects,
	const QueryFilter* _pQueryFilter,
	QueryFilterCallback* _pCustomFilterCallback)
{
	Physics::RaycastCallback<TVisibleObjects> Buffer(_Objects);
	RaycastInternal(_vOrigin, _vUnitDir, _fDistance, Buffer, _pQueryFilter, _pCustomFilterCallback);
}
//---------------------------------------------------------------------------------------------------
VolumeCache::ObjectType* VolumeCache::Raycast(
	const float3& _vOrigin,
	const float3 & _vUnitDir,
	const float _fDistance,
	RaycastHitInfo* _pHitInfo,
	const QueryFilter* _pQueryFilter,
	QueryFilterCallback* _pCustomFilterCallback) const
{
	ObjectType* pObject = nullptr;
	physx::PxRaycastBuffer Buffer;

	RaycastInternal(_vOrigin, _vUnitDir, _fDistance, Buffer, _pQueryFilter, _pCustomFilterCallback);

	if(Buffer.hasBlock)
	{
		pObject = static_cast<ObjectType*>(Buffer.block.actor->userData);
		if(_pHitInfo != nullptr)
		{
			_pHitInfo->vPosition = Buffer.block.position;
			_pHitInfo->vNormal = Buffer.block.normal;
		}
	}
	else
	{
		pObject = nullptr;
	}
	return pObject;
}
//---------------------------------------------------------------------------------------------------
