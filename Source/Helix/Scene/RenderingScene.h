//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef RENDERINGSCENE_H
#define RENDERINGSCENE_H

#include "Scene.h"
#include "Display\RenderView.h"
#include "concurrent_vector.h"
#include "Display\DX11\RenderProcedureManagerDX11.h"
#include "DataStructures\GameObject.h"

namespace Helix
{
	namespace Scene
	{
		// forward declaration
		class CameraComponent;
		class VolumeCache;

		class RenderingScene : public Scene, public hlx::Singleton<RenderingScene>
		{
		public:
			RenderingScene();
			~RenderingScene();
		
			void End();

			void Update(double _fDeltaTime, double _fTotalTime);

			Display::RenderProcedureManagerDX11& GetRenderer();
			const Display::RenderProcedureManagerDX11& GetRenderer() const;

			void EnableVSync(bool _bEnable);
			void EnableAdaptiveVSync(bool _bEnable);

			void Lock();
			void Unlock();

			uint32_t GetGatheredObjectCount() const;
		private:
			uint32_t m_uGatheredObjects = 0u;
			bool m_bVSync = false;
			bool m_bAdaptiveVSync = false;

			concurrency::concurrent_vector<const Datastructures::GameObject*> m_VisibleObjectsBuffer;
			Display::RenderProcedureManagerDX11 m_Renderer;
			concurrency::concurrent_vector<CameraComponent*> m_Cameras;
			std::recursive_mutex m_RenderingMutex;
		};

		inline void RenderingScene::Lock() { m_RenderingMutex.lock(); }
		inline void RenderingScene::Unlock() { m_RenderingMutex.unlock(); }

		inline uint32_t RenderingScene::GetGatheredObjectCount() const	{return m_uGatheredObjects;	}

		inline Display::RenderProcedureManagerDX11& RenderingScene::GetRenderer(){ return m_Renderer; }
		inline const Display::RenderProcedureManagerDX11& RenderingScene::GetRenderer() const { return m_Renderer; }

		inline void RenderingScene::EnableVSync(bool _bEnable){	m_bVSync = _bEnable;}
		inline void RenderingScene::EnableAdaptiveVSync(bool _bEnable) { m_bAdaptiveVSync = _bEnable; }
	} // Scene
} // Helix

#endif // !RENDERINGSCENE_H
