//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "FullscreenEffects.h"
#include "Scene\CameraManager.h"
#include "Display\Geometry.h"
#include "Scene\CameraComponent.h"

using namespace Helix;
using namespace Helix::Scene;

//---------------------------------------------------------------------------------------------------

FullscreenEffects::FullscreenEffects(const std::string& _sCameraName, uint64_t _kPasses, bool _bDebug) : m_sCameraName(_sCameraName)
{
	Initialize(_sCameraName, _kPasses, _bDebug);
}

//---------------------------------------------------------------------------------------------------

FullscreenEffects::~FullscreenEffects()
{
	//Uninitialize();
}
//---------------------------------------------------------------------------------------------------

bool FullscreenEffects::Initialize(const std::string& _sCameraName, uint64_t _kPasses, bool _bDebug)
{
	if (m_sCameraName.empty() == false)
	{
		HFATAL("FullscreenEffect already initialized!");
		return false;
	}
	m_sCameraName = _sCameraName;

	CameraComponent* pCamera = CameraManager::Instance()->GetCameraByName(m_sCameraName);

	if (pCamera == nullptr)
	{
		HERROR("Camera with name %s not found", CSTR(m_sCameraName));
		m_sCameraName.clear();
		return false;
	}

	if (_bDebug)
	{
		m_FullscreenObject.SetMesh(Display::Geometry::Generate2DPlane(1.0f, "FullscreenPlane"));
	}
	else
	{
		m_FullscreenObject.SetPrimitiveTopology(Display::PrimitiveTopology_TriangleList);
		m_FullscreenObject.SetVertexCount(3);
	}

	Display::MaterialDX11 Mat;
	Mat.EnableRenderPass(_kPasses);
	m_FullscreenObject.SetMaterial(Mat);

	// remove object to make sure its only added once when calling initialize mutiple times
	pCamera->RemoveRenderObject(&m_FullscreenObject);
	pCamera->AddRenderObject(&m_FullscreenObject);

	return true;
}
//---------------------------------------------------------------------------------------------------
bool FullscreenEffects::Uninitialize()
{
	if (m_sCameraName.empty())
		return true;

	CameraComponent* pCamera = CameraManager::Instance()->GetCameraByName(m_sCameraName);

	if (pCamera == nullptr)
	{
		HERROR("Camera with name %s not found", CSTR(m_sCameraName));
		return false;
	}

	pCamera->RemoveRenderObject(&m_FullscreenObject);

	return true;
}
//---------------------------------------------------------------------------------------------------
