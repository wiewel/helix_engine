//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher, David Labode
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef LIGHTDEFINES_H
#define LIGHTDEFINES_H

#include "Math\MathTypes.h"
#include "Util\BaseHash.h"

namespace Helix
{
	namespace Scene
	{
		enum LightType : uint32_t
		{
			LightType_Directional = 0,
			LightType_Point = 1,
			LightType_Spot = 2,
			LightType_Unknown
		};

		struct ShadowCasterProperties
		{
			Math::float4x4 mShadowTransform; /// for point and spotlight: mLightView;
			Math::float2 vShadowMapStartTexCoord; /// x,y start position (left top)
			Math::float2 vShadowMapEndTexCoord;   /// z,w end position (bottom right)
			float fLightCameraNearDist;
			float fLightCameraFarDist;
		};

		struct LightDesc
		{
			bool bEnabled = true;
			bool bCastShadows = false;
			/// Shadow Caster Member (set internally)
			ShadowCasterProperties ShadowProps;
			/// common members (all light types)
			LightType kType = LightType_Unknown;
			Math::float3 vColor = HFLOAT3_ZERO;
			Math::float3 vPosition = HFLOAT3_ZERO; // even dir light has direction for culling)
			float fRange = 0.f;
			float fIntensity = 1.f;

			// point light & spot light (dir lights dont have falloff)
			float fDecayStart = 0.f; // 'radius of light sphere'

			// dir & spotlight
			Math::float3 vDirection = HFLOAT3_ZERO;
			float fSpotAngle = 0.f;

			inline friend bool operator==(const LightDesc& l, const LightDesc& r)
			{
				return (l.bEnabled == r.bEnabled &&
					l.bCastShadows == r.bCastShadows &&
					l.kType == r.kType &&
					l.vColor == r.vColor &&
					l.vPosition == r.vPosition &&
					l.fRange == r.fRange &&
					l.fIntensity == r.fIntensity &&
					l.fDecayStart == r.fDecayStart &&
					l.vDirection == r.vDirection &&
					l.fSpotAngle == r.fSpotAngle);
			}

			// hasher
			inline size_t operator()(const LightDesc& _Desc) const
			{
				BaseHash Hash;

				Hash(_Desc.bCastShadows);
				Hash(_Desc.kType);
				Hash(_Desc.vColor);
				Hash(_Desc.vPosition);
				Hash(_Desc.fRange);
				Hash(_Desc.fIntensity);
				Hash(_Desc.fDecayStart);
				Hash(_Desc.vDirection);
				Hash(_Desc.fSpotAngle);

				return Hash.GetHash();
			}
		};

		// TODO: AreaLight
	} // Scene
} // Helix

#endif // !LIGHTDEFINES_H
