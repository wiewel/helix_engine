//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "TransformHierarchy.h"

#include "DataStructures\TransformHierarchyComponent.h"

using namespace Helix;
using namespace Helix::Scene;

//===================================================================================================
// Transform Hierarchy
//---------------------------------------------------------------------------------------------------
TransformHierarchy::TransformHierarchy()
{
}
//---------------------------------------------------------------------------------------------------
TransformHierarchy::~TransformHierarchy()
{
}
//---------------------------------------------------------------------------------------------------
// also sets the node pointer of the component
void TransformHierarchy::RegisterComponent(Datastructures::TransformHierarchyComponent * _pComponent)
{
	_pComponent->m_pNode = &m_Hierarchy[_pComponent];
}
//---------------------------------------------------------------------------------------------------
void TransformHierarchy::RemoveComponent(Datastructures::TransformHierarchyComponent * _pComponent)
{
	_pComponent->m_pNode = nullptr;
	
	// remove from map -> every access with the old pointer can be detected as invalid
	m_Hierarchy.erase(_pComponent);
}
//---------------------------------------------------------------------------------------------------
void TransformHierarchy::PreComponentUpdate()
{
	// first update all the transform nodes with the values from the component
	concurrency::parallel_for_each(m_Hierarchy.begin(), m_Hierarchy.end(), [&](auto& _Pair)
	{
		_Pair.second.bIsDirty = true;
		_Pair.second.TransformMatrix = _Pair.first->m_LocalTransform;
		_Pair.second.pParent = _Pair.first->m_pParent;
	});

	// then traverse the tree upward for every node until root or the parent is not dirty
	for (auto& Pair : m_Hierarchy)
	{
		// for nodes that are already updated by its children, execution stops immediately
		TraverseUp(&Pair.second);
	}
}
//---------------------------------------------------------------------------------------------------
void TransformHierarchy::TraverseUp(TransformNode* _CurrentNode)
{
	// the transform of this node was either not updated or already recomputed
	if (_CurrentNode->bIsDirty == false)
	{
		return;
	}

	// If at root -> transform matrix is already filled with local transform 
	if (_CurrentNode->pParent == nullptr)
	{
		_CurrentNode->bIsDirty = false;
		return;
	}

	// If parent was removed the pointer might still seem valid but can't be found in the map
	HierarchyMap::iterator Parent = m_Hierarchy.find(_CurrentNode->pParent);
	if (Parent == m_Hierarchy.end())
	{
		_CurrentNode->pParent = nullptr;
		_CurrentNode->bIsDirty = false;
		return;
	}

	// To update the current transform, the parent must already be updated
	TraverseUp(&Parent->second);

	// The parent must now contain all upstream transforms and can be used to compute the current
	_CurrentNode->TransformMatrix = Parent->second.TransformMatrix * _CurrentNode->TransformMatrix;

	// the node now contains a valid transform
	_CurrentNode->bIsDirty = false;
}

