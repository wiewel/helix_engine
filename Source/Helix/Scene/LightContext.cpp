//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "LightContext.h"
#include "LightComponent.h"

using namespace Helix::Math;
using namespace Helix::Display;
using namespace Helix::Scene;

//---------------------------------------------------------------------------------------------------
LightContext::LightContext()
{
	HNCL(m_SyncLightDock.Bind());
}
//---------------------------------------------------------------------------------------------------
LightContext::~LightContext()
{
}

//--------------------------------------------------------------------------------------------------
// helper function
void LightContext::SortLights(std::vector<LightDesc>& _Lights, const float3& _vCameraPos) const
{
	std::sort(_Lights.begin(), _Lights.end(),
		[_vCameraPos](const LightDesc& L1, const LightDesc& L2)
	{
		float fDist1 = L1.vPosition.distanceSquared(_vCameraPos);
		float fDist2 = L2.vPosition.distanceSquared(_vCameraPos);
		if (fDist1 == fDist2)
		{
			// tie breaker
			return L1.fRange + L1.fIntensity + L1.fDecayStart < L2.fRange + L2.fIntensity + L2.fDecayStart;
		}

		return fDist1 < fDist2;
	});
}
void LightContext::SortLights(std::vector<const LightComponent*>& _Lights, const float3& _vCameraPos) const
{
	std::sort(_Lights.begin(), _Lights.end(),
		[_vCameraPos](const LightComponent*& L1, const LightComponent*& L2)
	{
		float fDist1 = L1->GetParent()->Position.distanceSquared(_vCameraPos);
		float fDist2 = L2->GetParent()->Position.distanceSquared(_vCameraPos);
		if (fDist1 == fDist2)
		{
			// tie breaker
			return L1->Range + L1->Intensity + L1->DecayStart < L2->Range + L2->Intensity + L2->DecayStart;
		}

		return fDist1 < fDist2;
	});
}

//---------------------------------------------------------------------------------------------------

void LightContext::GetShadowCastingLights(std::vector<const LightComponent*>& _ShadowCaster, LightType _kType) const
{
	GatherShadowCastingLights(_ShadowCaster, _kType, true, true);
}

//---------------------------------------------------------------------------------------------------

void LightContext::GetLights(
	std::vector<LightDesc>& _DirLights,
	std::vector<LightDesc>& _PointLights,
	std::vector<LightDesc>& _SpotLights,
	bool _bEnabledOnly)
{
	GatherLights(_DirLights, _PointLights, _SpotLights, _bEnabledOnly, false);
}

//--------------------------------------------------------------------------------------------------
void LightContext::GatherLights(
	std::vector<LightDesc>& _DirLights,
	std::vector<LightDesc>& _PointLights,
	std::vector<LightDesc>& _SpotLights,
	bool _bEnabledOnly,
	bool _bShadowCasterOnly)
{
	Async::ScopedSpinLock lock(m_Lock);

	for (const LightComponent* pLight : m_Lights)
	{
		const LightDesc Desc = pLight->CreateDescription();
		if(_bShadowCasterOnly == false || Desc.bCastShadows)
		{
			if (_bEnabledOnly == false || Desc.bEnabled)
			{
				switch (Desc.kType)
				{
				case LightType_Directional:
					m_pMainDirLight = pLight;
					_DirLights.push_back(Desc);
					break;
				case LightType_Point:
					_PointLights.push_back(Desc);
					break;
				case LightType_Spot:
					_SpotLights.push_back(Desc);
					break;
				default:
					break;
				}
			}
		}
	}
}

//--------------------------------------------------------------------------------------------------
void LightContext::GatherShadowCastingLights(
	std::vector<const LightComponent*>& _Lights,
	LightType _kType,
	bool _bEnabledOnly, 
	bool _bShadowCasterOnly) const
{
	Async::ScopedSpinLock lock(m_Lock);

	for (const LightComponent* pLight : m_Lights)
	{
		if (_bShadowCasterOnly == false || pLight->ShadowCaster)
		{
			if (_bEnabledOnly == false || pLight->Enabled)
			{
				if(_kType == LightType_Unknown || _kType == pLight->Type)
				{
					_Lights.push_back(pLight);
				}
			}
		}
	}
}

//--------------------------------------------------------------------------------------------------
void LightContext::AddLight(const LightComponent* _pLight)
{
	Async::ScopedSpinLock lock(m_Lock);

	if (m_Lights.count(_pLight) == 0u)
	{
		m_Lights.insert(_pLight);
		if (_pLight->Type == LightType_Directional)
		{
			m_pMainDirLight = _pLight;
		}
	}
}
//---------------------------------------------------------------------------------------------------
void LightContext::RemoveLight(const LightComponent* _pLight)
{
	Async::ScopedSpinLock lock(m_Lock);

	m_Lights.erase(_pLight);

	if (m_pMainDirLight == _pLight)
	{
		m_pMainDirLight = nullptr;
	}
}
//---------------------------------------------------------------------------------------------------

bool LightContext::GetMainDirLight(LightDesc& _sMainLight) const
{
	Async::ScopedSpinLock lock(m_Lock);

	if(m_pMainDirLight == nullptr || m_pMainDirLight->Type != LightType_Directional)
		return false;

	_sMainLight = m_pMainDirLight->CreateDescription();
	return true;
}

//---------------------------------------------------------------------------------------------------
