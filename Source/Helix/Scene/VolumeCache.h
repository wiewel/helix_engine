//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef VOLUMECACHE_H
#define VOLUMECACHE_H

#include "PhysXSDK\Include\PxVolumeCache.h"
#include "hlx\src\Logger.h"
#include "Math\MathTypes.h"
#include "Physics\GeometryTypes.h"
#include "Physics\QueryFilter.h"
#include "Physics\QueryFilterCallback.h"
#include "CameraDefines.h"
#include "Physics\SceneLock.h"

namespace Helix
{
	// forward decls
	namespace Datastructures
	{
		class GameObject;
	}

	namespace Scene
	{
		struct RaycastHitInfo
		{
			RaycastHitInfo()
			{
				vPosition = Math::float3(0.0f);
				vNormal = Math::float3(0.0f);
			}
			Math::float3 vPosition;
			Math::float3 vNormal;
		};

		class VolumeCache
		{
		public:
			HDEBUGNAME("VolumeCache");
			using ObjectType = Datastructures::GameObject;
			using ObjectTypeConst = const Datastructures::GameObject;

			VolumeCache(uint32_t _uStaticObjects, uint32_t _uDynamicObjects);
			~VolumeCache();

			bool Update(const Physics::GeoSphere& _Volume, const Math::transform& _Transform);

			void Gather(
				const Physics::GeoBase& _Volume,
				const Math::transform& _Transform,
				_Out_ TVisibleObjects& _ObjectsInVolume,
				const Physics::QueryFilter* _pQueryFilter = nullptr,
				Physics::QueryFilterCallback* _pCustomFilerCallback = nullptr);

			/// Returns all hits on ray
			void Raycast(const Math::float3& _vOrigin,
				const Math::float3& _vUnitDir,
				const float _fDistance,
				TVisibleObjects& _Objects,
				const Physics::QueryFilter* _pQueryFilter = nullptr,
				Physics::QueryFilterCallback* _pCustomFilerCallback = nullptr);

			/// Returns first hit on ray
			ObjectType* Raycast(const Math::float3& _vOrigin,
				const Math::float3& _vUnitDir,
				const float _fDistance,
				RaycastHitInfo* _pHitInfo = nullptr,
				const Physics::QueryFilter* _pQueryFilter = nullptr,
				Physics::QueryFilterCallback* _pCustomFilerCallback = nullptr) const;
		private:
			template <class BufferType>
			void RaycastInternal(const Math::float3& _vOrigin,
				const Math::float3& _vUnitDir,
				const float _fDistance,
				BufferType& _RaycastBuffer,
				const Physics::QueryFilter* _pQueryFilter = nullptr,
				Physics::QueryFilterCallback* _pCustomFilerCallback = nullptr) const;

		private:
			Math::transform m_Transform;
			Physics::GeoSphere m_Volume;
			physx::PxVolumeCache* m_pVolumeCache = nullptr;
			physx::PxScene* m_pScene = nullptr;
		};

		template <class BufferType>
		inline void VolumeCache::RaycastInternal(
			const Math::float3& _vOrigin,
			const Math::float3& _vUnitDir,
			const float _fDistance,
			BufferType& _RaycastBuffer,
			const Physics::QueryFilter* _pQueryFilter,
			Physics::QueryFilterCallback* _pCustomFilterCallback) const
		{
			Physics::SceneReadLock lock(m_pScene);

			//TODO: implement physx::PxHitFlag
			physx::PxHitFlags Flags = physx::PxHitFlag::eDEFAULT;
			if (_pQueryFilter != nullptr)
			{
				m_pVolumeCache->raycast(_vOrigin, _vUnitDir, _fDistance, _RaycastBuffer, Flags, *_pQueryFilter, _pCustomFilterCallback);
			}
			else
			{
				m_pVolumeCache->raycast(_vOrigin, _vUnitDir, _fDistance, _RaycastBuffer, Flags, physx::PxQueryFilterData(), _pCustomFilterCallback);
			}
		}

	} // Physics
} // Helix

#endif // !VOLUMECACHE_H
