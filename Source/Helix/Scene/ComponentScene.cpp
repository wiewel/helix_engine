//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "ComponentScene.h"
#include "DataStructures\Component.h"
#include "Util\Functional.h"
#include "Util\AdvancedProfiler.h"
#include "TransformHierarchy.h"

using namespace Helix::Scene;
using namespace Helix::Datastructures;
//---------------------------------------------------------------------------------------------------

ComponentScene::ComponentScene()
{
}
//---------------------------------------------------------------------------------------------------

ComponentScene::~ComponentScene()
{
	Destroy();
}
//---------------------------------------------------------------------------------------------------

void ComponentScene::LevelLoaded()
{
	Async::ScopedSpinLock Lock(m_UnregisterLock);

	Helix::conditional_parallel_for_each(m_Components.begin(), m_Components.end(), 2u,
		[](IComponent* _pComponent)
	{
		if (_pComponent->GetActiveCallbacks() & Datastructures::ComponentCallback_OnLevelLoaded)
		{
			_pComponent->OnLevelLoaded();
		}
	});
}
//---------------------------------------------------------------------------------------------------

void ComponentScene::Destroy()
{
	HSAFE_MAP_DELETE(m_Templates);
	m_Templates.clear();
}
//---------------------------------------------------------------------------------------------------

void ComponentScene::Update(double _fDeltaTime, double _fTotalTime)
{
	Async::ScopedSpinLock Lock(m_UnregisterLock);
	
	TransformHierarchy::Instance()->PreComponentUpdate();

	Helix::conditional_parallel_for_each(m_Components.begin(), m_Components.end(), 2u,
		[_fDeltaTime, _fTotalTime](IComponent* _pComponent)
	{

		if (_pComponent->GetActiveCallbacks() & Datastructures::ComponentCallback_OnUpdate)
		{
			HGUIPROFILE(Component)
				_pComponent->OnUpdate(_fDeltaTime, _fTotalTime);
			HGUIPROFILEDYNEX_END(Component, _pComponent->GetTypeName() + std::to_string(_pComponent->GetID()), _pComponent->GetTypeName(), "ProfileComponents");
		}

	});
}
//---------------------------------------------------------------------------------------------------

void ComponentScene::RegisterComponent(IComponent* _pComponent)
{
	Async::ScopedSpinLock Lock(m_UnregisterLock);

	m_Components.push_back(_pComponent);
}
//---------------------------------------------------------------------------------------------------

void ComponentScene::UnregisterComponent(IComponent* _pComponent)
{
	Async::ScopedSpinLock Lock(m_UnregisterLock);
	TComponents::iterator it = std::remove(m_Components.begin(), m_Components.end(), _pComponent);
	if (it != m_Components.end())
	{
		m_Components.erase(it);
	}
	else
	{
		HFATAL("Component not found!");
	}
}
//---------------------------------------------------------------------------------------------------

IComponent* ComponentScene::GetTemplate(int32_t _iTypeID)
{
	TComponentTemplates::iterator it = m_Templates.find(_iTypeID);
	if (it != m_Templates.end())
	{
		return it->second;
	}

	return nullptr;
}
//---------------------------------------------------------------------------------------------------
