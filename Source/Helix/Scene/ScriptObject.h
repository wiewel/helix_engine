//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef SCRIPTOBJECT_H
#define SCRIPTOBJECT_H

#include <future>
#include <vector>
#include <atomic>

namespace Helix
{
	// forward declartation
	namespace Datastructures
	{
		class GameObject;
		class GameObjectPool;
	};

	namespace Scene
	{
		class IScriptObject
		{
			friend class ScriptScene;
		public:
			IScriptObject();

			virtual ~IScriptObject();

			// stops and unregisters script
			void Stop();
			bool Terminated();

		protected:
			// Allocates a gameobject when the pool is not busy
			std::future<Datastructures::GameObject*> CreateGameObject();

			// Allocates a bunch of gameobjects when the pool is not busy
			std::future<std::vector<Datastructures::GameObject*>> CreateGameObject(uint32_t _uCount);

			virtual void OnSceneBegin() {};
			virtual void OnSceneEnd() {};
			virtual void OnUpdate(double _fDeltaTime, double _fTotalTime) {};
			// the use can safely destroy gameobjects in this function, without crashing the rendering / physics
			virtual void OnDestroyObjects() {};
			// called serially before sorting & rendering
			virtual void OnPreRendering() {};
			// called after level has been successfuly loaded
			virtual void OnLevelLoaded() {};
			
			// this function indicates whether the script should be unregisted in the destructor or not
			// since the lifetime of a static variable is unknown in relation to the lifetime of the script scene, we need to make sure
			// that static scripts never unregister on a invalid script scene singleton instance
			inline virtual bool IsStaticScript() const { return false; }
		private:
			std::atomic_bool m_bStop = false;

			Datastructures::GameObjectPool* const m_pGameObjectPool;

		private:
			// never ever change the value of this variable, it has to be consistent with IsStaticScript()!
			bool m_bStaticScript = false;
		};

		inline void IScriptObject::Stop(){m_bStop.store(true);}
		inline bool IScriptObject::Terminated()	{return m_bStop.load();}

	}// Async
} // Helix


#ifndef STATICSCRIPT
#define STATICSCRIPT(_type) \
public: \
	inline static _type& Instance() { return m_Creator; } \
private: \
	static _type m_Creator; \
protected: \
	inline bool IsStaticScript() const final { return true; } \
public: 
#endif

#ifndef STATICSCRIPT_CPP
#define STATICSCRIPT_CPP(_type, ...) _type _type::m_Creator{__VA_ARGS__};
#endif

#endif // SCRIPTOBJECT_H