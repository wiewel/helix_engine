//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef CAMERADEFINES_H
#define CAMERADEFINES_H

#include "Datastructures\GameObject.h"
#include <concurrent_vector.h>

namespace Helix
{
	namespace Scene
	{
		// Type definitions
		struct GOCamDist
		{
			GOCamDist(const Datastructures::GameObject* _pObject = nullptr, const float _fDistToCamera = 0.f) :
				pObject(_pObject), fDistToCamera(_fDistToCamera) {}

			const Datastructures::GameObject* pObject;
			float fDistToCamera;
		};

		using TVisibleObjects = concurrency::concurrent_vector<const Datastructures::GameObject*>;
		using TVisibleObjectDistances = concurrency::concurrent_vector<GOCamDist>;
	} // Scene
} // Helix

#endif // !CAMERADEFINES_H
