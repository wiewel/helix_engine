//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "LightComponent.h"
#include "Resources\CommonTypeTextSerializer.h"
#include "Input/InputConstants.h"
#include "Display/DX11/CommonRenderPasses.h"
#include "RenderingSceneGuard.h"
#include "LightContext.h"

using namespace Helix::Resources;
using namespace Helix::Scene;
using namespace Helix::Display;
using namespace Helix::Datastructures;
//---------------------------------------------------------------------------------------------------

Helix::UniqueID<uint32_t> LightComponent::kLightID;

//---------------------------------------------------------------------------------------------------

LightComponent::LightComponent(GameObject* _pParent, LightType _kType) :
	IGameObjectComponent(_pParent, ComponentCallback_OnSerialize), m_kType(_kType)
{
	LightContext::Instance()->AddLight(this);
	HNCL(m_LightEnum.uSelectedIndex = _kType);
}
//---------------------------------------------------------------------------------------------------
// deserialization constructor
LightComponent::LightComponent(GameObject* _pParent, const ObjectType _kRequiredObjectType, TComponentCallbacks _kCallbacks) :
	IGameObjectComponent(_pParent, ComponentCallback_OnSerialize)
{
	LightContext::Instance()->AddLight(this);
}
//---------------------------------------------------------------------------------------------------

LightComponent::~LightComponent()
{
	LightContext::Instance()->RemoveLight(this);
}
//---------------------------------------------------------------------------------------------------
LightDesc LightComponent::CreateDescription() const
{
	LightDesc Desc;

	Desc.bEnabled = m_bEnabled;
	Desc.bCastShadows = m_bCastShadows;
	Desc.kType = m_kType;
	Desc.fDecayStart = m_fDecayStart;
	Desc.fIntensity = m_fIntensity;
	Desc.fRange = m_fRange;
	Desc.fSpotAngle = m_fAngle;
	Desc.vColor = m_vColor * Desc.fIntensity;
	Desc.vDirection = GetDirection();
	Desc.vPosition = GetParent()->GetPosition();
	Desc.ShadowProps = m_ShadowProps;

	return Desc;
}
//---------------------------------------------------------------------------------------------------

void LightComponent::UpdateShadowCamera()
{
	if(m_bCastShadows && m_bEnabled)
	{
		if (m_pShadowCamera == nullptr)
		{
			RenderingSceneGuard lock;

			switch (m_kType)
			{
			case LightType_Point:
				m_pShadowCamera = GetParent()->AddComponent<CameraComponent>("PointLight" + std::to_string(kLightID.NextID()), 80.0f, 1.f, m_fRange * 0.01f, m_fRange);
				(*m_pShadowCamera) |= g_kParaboloidShadowPass;
				break;
			case LightType_Spot:
				m_pShadowCamera = GetParent()->AddComponent<CameraComponent>("SpotLight" + std::to_string(kLightID.NextID()), Math::RadToDeg(m_fAngle * 2.0f), 1.f, m_fRange * 0.01f, m_fRange);
				(*m_pShadowCamera) |= g_kProjectedShadowPass;
				break;
			default:
				HFATAL("Not implemented! Unsupported light type!");
			}
			m_pShadowCamera->UseCullingSphere(Physics::SphereDesc(m_fRange));
		}
		else
		{
			m_pShadowCamera->SetActive(true);
			m_pShadowCamera->SetFOV(Math::RadToDeg(m_fAngle * 2.0f));
			m_pShadowCamera->SetDistances(m_fRange * 0.01f, m_fRange);
			m_pShadowCamera->UseCullingSphere(Physics::SphereDesc(m_fRange));
			m_pShadowCamera->ResetFlag();
			switch (m_kType)
			{
			case LightType_Point:
				(*m_pShadowCamera) |= g_kParaboloidShadowPass;
				break;
			case LightType_Spot:
				(*m_pShadowCamera) |= g_kProjectedShadowPass;
				break;
			default:
				HFATAL("Not implemented! Unsupported light type!");
			}
		}
	}
	else if(m_pShadowCamera != nullptr)
	{
		//RenderingSceneGuard lock;
		//HSAFE_FUNC_RELEASE(m_pShadowCamera, Destroy);
		m_pShadowCamera->SetActive(false);
	}
}

//---------------------------------------------------------------------------------------------------
void LightComponent::OnDeserialize(const hlx::TextToken& _Token)
{
	m_kType = _Token.get<LightType>("Type", LightType_Point);
	m_bEnabled = _Token.get<bool>("Enabled", false);
	m_bCastShadows = _Token.get<bool>("CastShadows", false);
	m_vColor = to(_Token.getVector<float, 3>("Color", HFLOAT3_ONE));
	m_fAngle = _Token.get<float>("Angle", Math::Deg2Rad(30.f));
	m_fRange = _Token.get<float>("Range", 1000.f);
	m_fIntensity = _Token.get<float>("Intensity", 1.f);
	m_fDecayStart = _Token.get<float>("DecayStart", 1.f);

	HNCL(m_LightEnum.uSelectedIndex = m_kType);

	UpdateShadowCamera();
};
//---------------------------------------------------------------------------------------------------

bool LightComponent::OnSerialize(_Out_ hlx::TextToken& _Token) const
{ 
	_Token.set("Type", m_kType);
	_Token.set("Enabled", m_bEnabled);
	_Token.set("CastShadows", m_bCastShadows);
	_Token.setVector<float, 3>("Color", to(m_vColor));
	_Token.set("Angle", m_fAngle);
	_Token.set("Range", m_fRange);
	_Token.set("Intensity", m_fIntensity);
	_Token.set("DecayStart", m_fDecayStart);

	return true;
}
//---------------------------------------------------------------------------------------------------
