//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef JOINTCOMPONENT_H
#define JOINTCOMPONENT_H

#include "DataStructures\GameObject.h"
#include "Datastructures\GameObjectComponent.h"

#include "extensions\PxJoint.h"

namespace Helix
{
	namespace Scene
	{
		class JointComponent : public Datastructures::IGameObjectComponent
		{
		public:
			enum JointType
			{
				JointType_None = 0,
				JointType_Fixed,
				JointType_Distance,
				JointType_Spherical,
				JointType_Revolute,
				JointType_Prismatic,
				JointType_D6
			};

			JointComponent(Datastructures::GameObject* _pParent, JointType _Type, physx::PxRigidActor* _pOtherActor);

			COMPTYPENAME(JointComponent, Datastructures::GameObject, 'Jnt ');
			COMPDESCONSTR(JointComponent, Datastructures::GameObject, IGameObjectComponent);
			COMPDEFCONSTR(JointComponent, Datastructures::GameObject, IGameObjectComponent);

			void Attach(JointType _Type, physx::PxRigidActor* _pOtherActor);
			JointType GetJointType();

		private:
			JointType m_Type = JointType_None;
			physx::PxRigidActor* m_pParentActor = nullptr;
			physx::PxJoint* m_pJoint = nullptr;
			// transform of the gameobject relative to the joint
			Math::transform m_LocalTransform;
			// transform of the joint relative to the other actor
			Math::transform m_JointTransform;
			physx::PxRigidActor* m_pOtherActor = nullptr;
		};
		//---------------------------------------------------------------------------------------------------
		inline JointComponent::JointType JointComponent::GetJointType()
		{
			return m_Type;
		}
		//---------------------------------------------------------------------------------------------------
	}
}

#endif // !JOINTCOMPONENT_H
