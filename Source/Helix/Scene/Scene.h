//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef SCENE_H
#define SCENE_H

#include "hlx\src\StandardDefines.h"
#include "hlx\src\TimerWin32.h"

#include "Async\AsyncObject.h"

namespace Helix
{
	namespace Scene
	{

		class Scene : public Async::IAsyncObject
		{
		public:
			Scene(const double& _fUpdateInterval = 1.f / 60.0, const double& _fIntialTimeStep = 1.f / 60.0);
			virtual ~Scene();

			//set desired update interval (deltatime) in SECONDS, this is a recommdation for the computation time spent on simulations
			void LockUpdateInterval(double _fUpdateInterval);
			
			//unlocks free computation interval, simulations will run as fast as possible
			void UnlockUpdateInterval();

			double GetDeltaTime() const;
			double GetExecutionTime() const;
			double GetTotalTime() const;
			double GetUpdateInterval() const;
			uint64_t GetNumOfSimulations() const;
			uint64_t GetNumOfResumes() const;

			void StopAfterXSteps(uint32_t _uSteps);

		private:
			void Execute();

		protected:
			virtual void Update(double _fDeltaTime, double _fTotalTime) = 0;

		private:
			
			//locking and thread control
			std::condition_variable m_CV;
			std::mutex m_WaitMutex;

			//simulation time
			hlx::TimerWin32 m_Timer;

			double m_fUpdateInterval = 0.0; 
			double m_fDeltaTime = 0.0;
			double m_fExecutionTime = 0.0; // time without sleep
			double m_fTotalDeltaTime = 0.0;

			uint64_t m_uNumOfSimulations = 0;
			uint64_t m_uNumOfResumes = 0;

			uint32_t m_uStopAfterXSteps = 0;
		};

		inline void Scene::LockUpdateInterval(double _fUpdateInterval) { m_fUpdateInterval = _fUpdateInterval; }
		inline void Scene::UnlockUpdateInterval() {	m_fUpdateInterval = 0.0f; }
		inline double Scene::GetDeltaTime() const{ return m_fDeltaTime; }
		inline double Scene::GetExecutionTime() const{return m_fExecutionTime;}
		inline double Scene::GetTotalTime() const { return m_fTotalDeltaTime; }
		inline double Scene::GetUpdateInterval() const{return m_fUpdateInterval;}
		inline uint64_t Scene::GetNumOfSimulations() const { return m_uNumOfSimulations; }
		inline uint64_t Scene::GetNumOfResumes() const { return m_uNumOfResumes; }
		inline void Scene::StopAfterXSteps(uint32_t _uSteps) { m_uStopAfterXSteps = _uSteps; }
	};
}; // Helix

#endif // SCENE_H