//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "EngineConfig.h"
#include "Resources\FileSystem.h"
#include "hlx\src\TextToken.h"
#include "Resources\CommonTypeTextSerializer.h"
#include "hlx\src\Logger.h"

using namespace Helix::Resources;
using namespace Helix::Engine;
//---------------------------------------------------------------------------------------------------

EngineConfig::EngineConfig(bool _bLoad)
{
	if (_bLoad)
	{
		Load();
	}
}
//---------------------------------------------------------------------------------------------------

EngineConfig::~EngineConfig()
{
}
//---------------------------------------------------------------------------------------------------

bool EngineConfig::Load()
{
	hlx::fbytestream FStream;
	if (FileSystem::Instance()->OpenFileStream(HelixDirectories_EngineConfigs, S("EngineConfig"), std::ios::in, FStream) == false)
	{
		return false;
	}

	hlx::bytes Data = FStream.get<hlx::bytes>(FStream.size());
	FStream.close();

	hlx::bytestream BStream(Data);
	hlx::TextToken config(BStream, "EngineConfig");

	m_vResolution = to(config.getVector<uint32_t, 2>("Resolution", { 1920u, 1080u }));
	m_fRefreshRate = config.get<float>("RefreshRate", 60.f);
	m_bVSync = config.get<bool>("VSync", true);
	m_bAdaptiveVSync = config.get<bool>("AdaptiveVSync", true);
	m_bQtHostedRendering = config.get<bool>("QtHostedRendering", true);
	m_bDebugRendering = config.get<bool>("DebugRendering", false);
	m_bUseDoF = config.get<bool>("UseDoF", true);
	m_bFullScreen = config.get<bool>("Fullscreen", false);
	m_bUseHDR = config.get<bool>("UseHDR", true);
	
	return true;
}
//---------------------------------------------------------------------------------------------------

bool EngineConfig::Save() const
{
	hlx::string sFilePath = FileSystem::Instance()->GetKnownDirectoryPath(HelixDirectories_EngineConfigs) + S("EngineConfig.cfg");

	hlx::TextToken config("EngineConfig");

	config.setVector<uint32_t, 2>("Resolution", to(m_vResolution));
	config.set<float>("RefreshRate", m_fRefreshRate);
	config.set<bool>("VSync", m_bVSync);
	config.set<bool>("AdaptiveVSync", m_bAdaptiveVSync);
	config.set<bool>("QtHostedRendering", m_bQtHostedRendering);
	config.set<bool>("DebugRendering", m_bDebugRendering);
	config.set<bool>("UseDoF", m_bUseDoF);
	config.set<bool>("Fullscreen", m_bFullScreen);
	config.set<bool>("UseHDR", m_bUseHDR);

	bool bSuccess = config.serialize(sFilePath);

	if (bSuccess == false)
	{
		HERROR("Failed to save EngineConfig to %s", sFilePath.c_str());
	}

	return bSuccess;
}
//---------------------------------------------------------------------------------------------------
