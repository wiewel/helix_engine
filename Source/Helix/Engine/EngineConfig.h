//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef ENGINECONFIG_H
#define ENGINECONFIG_H

#include "hlx\src\String.h"
#include "Math\MathTypes.h"

namespace Helix
{
	namespace Engine
	{
		class EngineConfig
		{
		public:
			EngineConfig(bool _bLoad = false);
			~EngineConfig();

			bool Load();
			bool Save() const;

		private:
			Math::uint2 m_vResolution = {1920u, 1080u};
			float m_fRefreshRate = 60.f;
			bool m_bVSync = true;
			bool m_bAdaptiveVSync = true;
			bool m_bQtHostedRendering = false;
			bool m_bDebugRendering = false;
			bool m_bUseDoF = true;
			bool m_bFullScreen = false;
			bool m_bUseHDR = true;

		public:
			inline const Math::uint2& GetResolution() const { return m_vResolution; }
			inline void SetResolution(const Math::uint2& _vResolution) { m_vResolution = _vResolution; }

			inline const float& GetRefreshRate() const { return m_fRefreshRate; }
			inline void SetRefreshReate(const float _fRefreshRate) { m_fRefreshRate = _fRefreshRate; }

			inline bool GetVSync() const { return m_bVSync; }
			inline void SetVSync(const bool _bVSync) { m_bVSync = _bVSync; }

			inline bool GetAdaptiveVSync() const { return m_bAdaptiveVSync; }
			inline void SetAdaptiveVSync(const bool _bAdaptiveVSync) { m_bAdaptiveVSync = _bAdaptiveVSync; }

			inline bool GetQtHostedRendering() const { return m_bQtHostedRendering; }
			inline void SetQtHostedRendering(const bool _bQtHostedRendering) { m_bQtHostedRendering = _bQtHostedRendering; }

			inline bool GetDebugRendering() const { return m_bDebugRendering; }
			inline void SetDebugRendering(const bool _bDebugRendering) { m_bDebugRendering = m_bDebugRendering; }

			inline bool GetUseDoF() const { return m_bUseDoF; }
			inline void SetUseDoF(const bool _bUseDoF) { m_bUseDoF = _bUseDoF; }

			inline bool GetFullScreen() const { return m_bFullScreen; }
			inline void SetFullScreen(const bool _bFullScreen) { m_bFullScreen = _bFullScreen; }

			inline bool GetUseHDR() const { return m_bUseHDR; }
			inline void SetUseHDR(const bool _bUseHDR) { m_bUseHDR = m_bUseHDR; }
		};
	}
} // Helix

#endif // !ENGINECONFIG_H
