//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef ENGINEINITIALIZER_H
#define ENGINEINITIALIZER_H

#include <string>
#include "hlx\src\StandardDefines.h"
#include "Scene\FullscreenEffects.h"
#include "EngineConfig.h"

namespace Helix
{
	// forward decls
	namespace Display
	{
		class RenderView;
	}

	namespace Scene
	{
		class ComponentScene;
		class CameraComponent;
	}

	namespace Engine
	{
		class EngineInitializer
		{
		public:
			EngineInitializer(Display::RenderView& m_RenderView);
			~EngineInitializer();

			bool Initialize(
				const hlx::string& _sProjectName,
				HWND _hWND,
				const hlx::string& _sBasePath = S(""),
				bool _bCreateEngineDirs = true,
				bool _bCreateMainCamera = true,
				bool _bUseDefaultRenderPasses = true);

			bool Start(bool _bStartSupended = false, bool _bNoRendering = false);

			virtual bool OnInitializeCustomComponents(Scene::ComponentScene* _pComponentScene) { return true; };

			const EngineConfig& GetConfig() const;
			EngineConfig& GetConfig();

		private:
			bool Uninitialize();

		private:
			bool m_bInitialized = false;
			HWND m_hWND = NULL;
			EngineConfig m_Config;
			Datastructures::GameObject* m_pMainCamera = nullptr;
			Scene::CameraComponent* m_pMainCameraComponent = nullptr;

			Scene::FullscreenEffects m_DefaultEffects;
			Display::RenderView& m_RenderView;
		};

		inline const EngineConfig& EngineInitializer::GetConfig() const	{return m_Config;}
		inline EngineConfig& EngineInitializer::GetConfig(){return m_Config;}
	} // Engine
} // Helix

#endif // !ENGINEINITIALIZER_H
