//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "EngineInitializer.h"
#include "Display\DXGIManager.h"

#include "hlx\src\Logger.h"
#include "hlx\src\Profiler.h"
#include "Resources\FileSystem.h"
#include "Scene\PhysicsScene.h"
#include "Scene\RenderingScene.h"
#include "Scene\ScriptScene.h"
#include "Scene\ComponentScene.h"
#include "Scene\CameraManager.h"
#include "DataStructures\GameObjectPool.h"
#include "Input\RawInput.h"
#include "Display\RenderView.h"

#include "Display\DX11\CommonRenderPasses.h"

// engine components:
#include "Sound\Speaker.h"
#include "Scene\LightComponent.h"
#include "Scene\CameraComponent.h"
#include "Scene\RotationComponent.h"
#include "DataStructures\ShapeComponent.h"

using namespace Helix;
using namespace Helix::Engine;
using namespace Helix::Display;
//---------------------------------------------------------------------------------------------------

EngineInitializer::EngineInitializer(Display::RenderView& _RenderView) :
	m_RenderView(_RenderView)
{
	HInitDebug();
}
//---------------------------------------------------------------------------------------------------

EngineInitializer::~EngineInitializer()
{
	Uninitialize();
}
//---------------------------------------------------------------------------------------------------

bool EngineInitializer::Initialize(
	const hlx::string& _sProjectName,
	HWND _hWND,
	const hlx::string& _sBasePath,
	bool _bCreateEngineDirs,
	bool _bCreateMainCamera,
	bool _bUseDefaultRenderPasses)
{
	if (m_bInitialized)
	{
		HFATAL("Engine is already initialized!");
		return m_bInitialized;
	}
	
	m_hWND = _hWND;
	bool bSuccess = false;

	// only log warnings and above
#ifdef _FINAL
	Logger::Instance()->SetLogLevel(Helix::HERROR_WARNING);
#endif

	hlx::Logger::Instance()->WriteToFile(TEXT("log.txt"));
	hlx::Logger::Instance()->SetShowMessageBoxOnFatal(true);
	hlx::Logger::Instance()->WriteToAdditionalConsole();

	if ((m_bInitialized = Resources::FileSystem::Instance()->Initialize(_sBasePath, _sProjectName, _bCreateEngineDirs)) == false)
	{
		return false;	
	}

	// load config
	m_Config.Load();
		
	Input::RawInput::Instance()->Initialize(_hWND);

	if ((m_bInitialized = Datastructures::GameObjectPool::Instance()->Initialize(1000u)) == false)
	{
		return false;
	};

	// initialize DXGI
	{
		Display::DXGIManager* pDXGIManager = Display::DXGIManager::Instance();
		const std::vector<DXGI_MODE_DESC>& Modes = pDXGIManager->GetDisplayModes();

		uint32_t uWidth = m_Config.GetResolution().x;
		uint32_t uHeight = m_Config.GetResolution().y;
		float fRefreshRate = m_Config.GetRefreshRate();
		float fRefreshRateDiff = 60.f;
		uint32_t uModeIndex = static_cast<uint32_t>(Modes.size()) / 2u;
		
		for (uint32_t i = 0u; i < static_cast<uint32_t>(Modes.size()); i++)
		{
			const DXGI_MODE_DESC& Mode = Modes.at(i);
			if (Mode.Height == uHeight && Mode.Width == uWidth)
			{
				float fRate = static_cast<float>(Mode.RefreshRate.Numerator) / static_cast<float>(Mode.RefreshRate.Denominator);
				float fDiff = fabsf(fRate - fRefreshRate); // find res with nearest refresh rate
				if (fDiff < fRefreshRateDiff)
				{
					fRefreshRateDiff = fDiff;
					uModeIndex = i;
				}
			}
		}

		pDXGIManager->SetMainDisplayModeByIndex(uModeIndex);	
	}

	m_RenderView.SetDebug(m_Config.GetDebugRendering());

	if ((m_bInitialized = m_RenderView.Initialize(m_hWND)) == false )
	{
		return false;
	}

	Scene::ComponentScene* pComponentScene = Scene::ComponentScene::Instance();
	Scene::PhysicsScene* pPhysicsScene = Scene::PhysicsScene::Instance();
	Scene::RenderingScene* pRenderingScene = Scene::RenderingScene::Instance();
	Scene::ScriptScene* pScriptScene = Scene::ScriptScene::Instance();

	if ((m_bInitialized = pPhysicsScene->Initialize()) == false)
	{
		HERROR("Failed to initialize physics scene");
		return false;
	};

	// register engine components
	pComponentScene->RegisterTemplate<Scene::RotationComponent>();
	pComponentScene->RegisterTemplate<Scene::LightComponent>();
	pComponentScene->RegisterTemplate<Datastructures::ShapeComponent>();
	pComponentScene->RegisterTemplate<Sound::Speaker>();

	// initialize user components
	if ((m_bInitialized = OnInitializeCustomComponents(pComponentScene)) == false)
	{
		HERROR("Failed to initialize custom user componets");
		return false;
	}

	pScriptScene->LockUpdateInterval(1.0 / 60.0);
	pPhysicsScene->LockUpdateInterval(1.0 / 60.0);
	pRenderingScene->LockUpdateInterval(1.0 / (m_Config.GetRefreshRate() + 2.0));
	pComponentScene->LockUpdateInterval(1.0 / 60.0);

	pRenderingScene->EnableVSync(m_Config.GetVSync());
	pRenderingScene->EnableAdaptiveVSync(m_Config.GetAdaptiveVSync());

	if (_bUseDefaultRenderPasses)
	{
		MaterialPoolDX11::SetDefaultRenderPasses(g_kShadowMapPass() | g_kParaboloidShadowPass() | g_kProjectedShadowPass() | g_kSolidPass());
	}

	if (_bCreateMainCamera)
	{
		m_pMainCamera = Datastructures::GameObjectPool::Instance()->Create().get();
		m_pMainCamera->CreatePhysicsActorDynamic(Math::transform(Math::float3(0, 0, -5)), 1.0f, true);
		if (m_pMainCamera != nullptr)
		{
			m_pMainCamera->AddToScene();
		}
		m_pMainCamera->SetName("MainCamera");
		m_pMainCameraComponent = m_pMainCamera->AddComponent<Scene::CameraComponent>("MainCamera", 80.f, 16.f / 9.f, 0.1f, 150.f);
		if (m_pMainCameraComponent != nullptr)
		{
			if (_bUseDefaultRenderPasses)
			{
				(*m_pMainCameraComponent) |= g_kDepthPrePass;
				(*m_pMainCameraComponent) |= g_kSolidPass;
				(*m_pMainCameraComponent) |= g_kDebugLinesPass;
				(*m_pMainCameraComponent) |= g_kGizmoPass;

				// default 
				uint64_t kFullscreenPasses =
					g_kHorizontalBlurPass |
					g_kVerticalBlurPass |
					g_kSSAOPass |
					g_kSkyboxPass |
					g_kSimpleDoFPass |
					g_kDeferredLightingPass |
					g_kDeferredMergePass |
					g_kScalePass |
					g_kLuminancePass |
					g_kLuminanceAdaptionPass |
					g_kBloomPass |
					g_kHDRCompositePass |
					g_kGlitchPass;

				(*m_pMainCameraComponent) |= kFullscreenPasses;

				m_DefaultEffects.Initialize("MainCamera", kFullscreenPasses, m_Config.GetDebugRendering());
			}
		}
	}

	return m_bInitialized;
}
//---------------------------------------------------------------------------------------------------

bool EngineInitializer::Start(bool _bStartSupended, bool _bNoRendering)
{
	if (m_bInitialized == false)
	{
		HFATAL("Engine has not been initialized!");
		return m_bInitialized;
	}

	HLOG("Starting HELiXEngine...");
	bool bComponent = Scene::ComponentScene::Instance()->StartInNewThread(_bStartSupended, false);
	bool bPhsyics = Scene::PhysicsScene::Instance()->StartInNewThread(_bStartSupended, false);
	bool bScript = Scene::ScriptScene::Instance()->Begin(_bStartSupended);
	bool bRendering = _bNoRendering ? true : Scene::RenderingScene::Instance()->StartInNewThread(_bStartSupended, false);
	bool bInput = Input::RawInput::Instance()->StartInNewThread(_bStartSupended, false);
	
	return bComponent && bScript && bPhsyics && bRendering && bInput;
}
//---------------------------------------------------------------------------------------------------

bool EngineInitializer::Uninitialize()
{
	HLOG("Stopping HELiXEngine...");
	m_Config.Save();

	if (Scene::RenderingScene::Instance()->IsRunning())
	{
		Scene::RenderingScene::Instance()->End();
	}

	if (Input::RawInput::Instance()->IsRunning())
	{
		Input::RawInput::Instance()->Stop();
	}

	if (Scene::ScriptScene::Instance()->IsRunning())
	{
		Scene::ScriptScene::Instance()->End();
	}

	if (Scene::ComponentScene::Instance()->IsRunning())
	{
		Scene::ComponentScene::Instance()->Stop();
	}
	Scene::ComponentScene::Instance()->Destroy();

	if (Scene::PhysicsScene::Instance()->IsRunning())
	{
		Scene::PhysicsScene::Instance()->Stop();
	}

	HPROFILE_PRINT()

	bool bSuccess = m_DefaultEffects.Uninitialize();

	if (m_bInitialized)
	{
		m_RenderView.Shutdown();
	}

	m_bInitialized = false;

	if (m_pMainCamera != nullptr)
	{
		//m_pMainCamera->RemoveComponent(m_pMainCameraComponent); // ? why
		m_pMainCamera->Destroy();
		m_pMainCamera = nullptr;
	}

	// destroy gameobjects & physx actors before destorying the physc scene
	Datastructures::GameObjectPool::Instance()->Destroy();

	Scene::PhysicsScene::Instance()->End();

	return bSuccess;
}
//---------------------------------------------------------------------------------------------------
