﻿//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "D3DWidget.hpp"
#include "Display\DXGIManager.h"
#include "Input\RawInput.h"
#include "Scene\RenderingScene.h"

#include <qdir.h>

using namespace Helix::Engine;
//---------------------------------------------------------------------------------------------------
D3DWidget::D3DWidget(bool _bTrapMouse, QWidget* _pParent) :
	QWidget(_pParent),
	m_Initializer(m_RenderView),
	m_bMouseTrapping(_bTrapMouse)
{
	setAttribute(Qt::WA_PaintOnScreen);
	setAttribute(Qt::WA_NativeWindow);
	setAttribute(Qt::WA_NoSystemBackground);
	setAttribute(Qt::WA_OpaquePaintEvent);

	m_pDeferredRenderer = new Helix::Display::DeferredRendererDX11();
}
//---------------------------------------------------------------------------------------------------
D3DWidget::~D3DWidget()
{
	HSAFE_DELETE(m_pDeferredRenderer);

	HSAFE_DELETE(m_pResizeTimer);

	if (m_pRenderTimer != nullptr)
	{
		Scene::RenderingScene::Instance()->GetRenderer().Reset();
	}

	HSAFE_DELETE(m_pRenderTimer);
}
//---------------------------------------------------------------------------------------------------
void D3DWidget::Initialize(const hlx::string _sProjectName)
{
	std::wstring sBasePath = QDir::toNativeSeparators(QDir::currentPath()).toStdWString() + S('\\');

	int iRenderFreq = 16; // 16ms
	bool bQtHostedRendering = true;

	if (m_Initializer.Initialize(_sProjectName, reinterpret_cast<HWND>(winId()), hlx::to_string(sBasePath)))
	{
		bQtHostedRendering = m_Initializer.GetConfig().GetQtHostedRendering();
		DXGI_MODE_DESC desc = Display::DXGIManager::Instance()->GetMainDisplayMode();
		setMinimumSize(desc.Width, desc.Height);
		iRenderFreq = static_cast<int>(static_cast<float>(desc.RefreshRate.Denominator * 1000) / static_cast<float>(desc.RefreshRate.Numerator));

		m_pDeferredRenderer->SetDebugMode(m_Initializer.GetConfig().GetDebugRendering());
		m_pDeferredRenderer->EnableDepthOfField(m_Initializer.GetConfig().GetUseDoF());
		m_pDeferredRenderer->EnableHDR(m_Initializer.GetConfig().GetUseHDR());

		bool bNoRendering = m_pDeferredRenderer->Initialize() == false || bQtHostedRendering;
		// if renderer init fails, renderscene wont be invoked
		m_Initializer.Start(false, bNoRendering);
	}

	if (m_bMouseTrapping)
	{
		setMouseTracking(true);
		setCursor(QCursor(Qt::BlankCursor));
	}

	// initialize resize timer
	m_pResizeTimer = new QTimer(this);
	connect(m_pResizeTimer, SIGNAL(timeout()), this, SLOT(ResizeTimeout()));
	m_pResizeTimer->setSingleShot(true);

	if (bQtHostedRendering)
	{
		m_pRenderTimer = new QTimer(this);
		connect(m_pRenderTimer, SIGNAL(timeout()), this, SLOT(RenderTimeout()));
		m_pRenderTimer->start(iRenderFreq);
	}
}
//---------------------------------------------------------------------------------------------------
void D3DWidget::ResizeTimeout()
{
	m_pResizeTimer->stop();
	m_RenderView.ResizeRenderer(width(), height());
}
//---------------------------------------------------------------------------------------------------
void D3DWidget::resizeEvent(QResizeEvent* _pEvent)
{
	m_pResizeTimer->stop();
	m_pResizeTimer->start(250);
}
//---------------------------------------------------------------------------------------------------

void D3DWidget::RenderTimeout()
{
	Scene::RenderingScene::Instance()->Update(0.f, 0.f);
}
//---------------------------------------------------------------------------------------------------
bool D3DWidget::nativeEvent(const QByteArray& eventType, void* _pMessage, long* _iResult)
{
	MSG* pMSG = reinterpret_cast<MSG*>(_pMessage);

	if (hasFocus())
	{
		return Input::RawInput::Instance()->ProcessInputMsg(pMSG->hwnd, pMSG->message, pMSG->wParam, pMSG->lParam);
	}

	return false;
}
//---------------------------------------------------------------------------------------------------
void D3DWidget::mouseMoveEvent(QMouseEvent* _pEvent)
{
	if(m_bMouseTrapping && hasFocus())
	{
		QPoint GlobalPos = mapToGlobal(QPoint(width() / 2, height() / 2));
		QCursor::setPos(GlobalPos);		
	}
}
//---------------------------------------------------------------------------------------------------
QPaintEngine* D3DWidget::paintEngine() const
{
	return nullptr;
}
//---------------------------------------------------------------------------------------------------
void D3DWidget::paintEvent(QPaintEvent * evt)
{
}
//---------------------------------------------------------------------------------------------------
bool D3DWidget::IsRendererFullScreen() const
{
	//return m_RenderView.GetViewSettings().bFullscreen;
	return m_Initializer.GetConfig().GetFullScreen();
}
//---------------------------------------------------------------------------------------------------