﻿//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef D3DWIDGET_HPP
#define D3DWIDGET_HPP

#include <QWidget>
#include "EngineInitializer.h"
#include "Display\DX11\RenderViewDX11.h"
#include "Display\DX11\DeferredRendererDX11.h"
#include <qtimer.h>

namespace Helix
{
	namespace Engine
	{
		class D3DWidget : public QWidget
		{
			Q_OBJECT

		public:
			D3DWidget(bool _bTrapMouse = false, QWidget * parent = Q_NULLPTR);
			~D3DWidget();

			void Initialize(const hlx::string _sProjectName);

			bool IsRendererFullScreen() const;
			Display::DeferredRendererDX11& GetRenderer();

		private:
			void resizeEvent(QResizeEvent* _pEvent) final;
			void mouseMoveEvent(QMouseEvent* _pEvent) override;
			QPaintEngine* paintEngine() const final;
			void paintEvent(QPaintEvent* evt) final;
			
		private slots:
			void ResizeTimeout();
			void RenderTimeout();
			bool nativeEvent(const QByteArray& eventType, void* _pMessage, long* _iResult) override;

		private:
			QTimer* m_pResizeTimer = nullptr;
			QTimer* m_pRenderTimer = nullptr;

			Display::DeferredRendererDX11* m_pDeferredRenderer = nullptr;
			Display::RenderViewDX11 m_RenderView;
			EngineInitializer m_Initializer;
			bool m_bMouseTrapping = false;
		};
		inline Display::DeferredRendererDX11& D3DWidget::GetRenderer() { return *m_pDeferredRenderer; }
	} // Nucleo
} // Helix

#endif // D3DWIDGET_HPP