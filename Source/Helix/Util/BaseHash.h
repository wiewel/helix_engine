//   Copyright 2020 Fabian Wahlster
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef BASEHASH_H
#define BASEHASH_H

#include <stdint.h>
#include <string>

namespace Helix
{
	inline uint32_t DefaultHashFunction32(const  uint8_t* _pData, size_t _uLength)
	{
		uint32_t result = 2166136261;
		uint32_t prime = 16777619;

		for (size_t i = 0; i < _uLength; ++i)
		{
			result = (result ^ (uint32_t)_pData[i]) * prime;
		}

		return result;
	}

	inline uint64_t DefaultHashFunction(const  uint8_t* _pData, size_t _uLength)
	{
		uint64_t result = 14695981039346656037ull;
		uint64_t prime = 1099511628211ull;

		for (size_t i = 0; i < _uLength; ++i)
		{
			result = (result ^ (uint64_t)_pData[i]) * prime;
		}

		return result;
	}

	template <class T>
	inline uint64_t BaseTypeHash(const T& _BaseType)
	{
		return DefaultHashFunction(reinterpret_cast<const uint8_t*>(&_BaseType), sizeof(T));
	}

	template <>
	inline uint64_t BaseTypeHash(const bool& _BaseType)
	{
		uint8_t boolVal = _BaseType ? 1u : 0u;
		return DefaultHashFunction(&boolVal, sizeof(uint8_t));
	}

	template <>
	inline uint64_t BaseTypeHash(const std::wstring& _BaseStr)
	{
		return DefaultHashFunction(reinterpret_cast<const uint8_t*>(_BaseStr.data()), _BaseStr.size() * sizeof(std::wstring::value_type));
	}

	template <>
	inline uint64_t BaseTypeHash(const std::string& _BaseStr)
	{
		return DefaultHashFunction(reinterpret_cast<const uint8_t*>(_BaseStr.data()), _BaseStr.size() * sizeof(std::string::value_type));
	}

	// helper for std hash
	template <class T>
	inline size_t std_hash(const T& _PrimitiveType)
	{
		std::hash<T> Hasher;
		return Hasher(_PrimitiveType);
	}

	struct BaseHash
	{
		template <class T>
		inline const uint64_t& operator()(const T& _BaseType)
		{
			uHash ^= BaseTypeHash(_BaseType) + 0x9e3779b9 + (uHash << 6) + (uHash >> 2);
			return uHash;
		}

		inline const uint64_t& Combine(const uint64_t& _uOtherHash)
		{
			uHash ^= _uOtherHash + 0x9e3779b9 + (uHash << 6) + (uHash >> 2);
			return uHash;
		}

		inline const uint64_t& Combine(const BaseHash& _OtherHash)
		{
			uHash ^= _OtherHash.GetHash() + 0x9e3779b9 + (uHash << 6) + (uHash >> 2);
			return uHash;
		}

		inline const uint64_t& GetHash() const { return uHash; }

	protected:
		uint64_t uHash = 0ull;
	};

	inline uint64_t CombineHashes(const uint64_t& _uHash1, const uint64_t& _uHash2)
	{
		return _uHash1 ^ (_uHash1 + 0x9e3779b9 + (_uHash2 << 6) + (_uHash2 >> 2));
	}

	inline uint64_t BaseTypeHashCombined_Impl(uint64_t& _uHash) { return _uHash; }

	template <typename T, typename... Rest>
	inline uint64_t BaseTypeHashCombined_Impl(uint64_t& _uHash, const T& _BaseType, Rest... _Rest)
	{
		uHash ^= BaseTypeHash(_BaseType) + 0x9e3779b9 + (_uHash << 6) + (_uHash >> 2);
		return BaseTypeHashCombined_Impl(uHash, _Rest...);
	}

	template <typename T, typename... Rest>
	inline uint64_t BaseTypeHashCombined(const T& _BaseType, Rest... _Rest)
	{
		uint64_t uHash = 0ull;
		return BaseTypeHashCombined_Impl(uHash, _BaseType, _Rest...);
	}
} // Helix

#endif // !BASEHASH_H
