//   Copyright 2020 Fabian Wahlster
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef FUNCTIONAL_H
#define FUNCTIONAL_H

#include <vector>
#include <tuple>

#ifdef min
#undef min
#endif

#include <algorithm>
#include <type_traits>
#include <ppl.h>

// meta function template library

namespace Helix
{
#pragma region All true
	inline bool all_true() { return true; }

	template<typename T, typename... Args>
	inline bool all_true(const T& arg, const Args&... args)
	{
		if (arg == false) return false;
		return all_true(args...);
	}
#pragma endregion

#pragma region minmax
	template<typename T>
	inline T max(const T& x1, const T& x2)
	{
		return x1 > x2 ? x1 : x2;
	}

	template<typename T, typename... Args>
	inline T max(const T& x1, const T& x2, const Args&... args)
	{
		return max(x1 > x2 ? x1 : x2, args...);
	}

	template<typename T>
	inline T min(const T& x1, const T& x2)
	{
		return x1 < x2 ? x1 : x2;
	}

	template<typename T, typename... Args>
	inline T min(const T& x1, const T& x2, const Args&... args)
	{
		return min(x1 < x2 ? x1 : x2, args...);
	}
#pragma endregion

#pragma region map
	template <typename Func, typename T>
	inline auto map(Func Fn, const std::vector<T>& Inputs)
	{
		typedef typename std::result_of<Func(T)>::type ResultType;
		std::vector<ResultType> Results(Inputs.size());
		size_t length = Results.size();

		for (size_t i = 0; i < length; i++)
		{
			Results.at(i) = Fn(Inputs.at(i));
		}

		return Results;
	}
#pragma endregion

#pragma region zip
	// To avoid the unspecified order, brace-enclosed initializer lists can be used, which guarantee strict left-to-right order of evaluation.
	struct pass
	{
		template<typename ...T> pass(T...) {}
	};

	template <typename ...Iterator>
	inline auto zip_aux(std::pair<Iterator, Iterator>... _Ranges)
	{
		static_assert(sizeof...(Iterator) >= 1, "zip only works with non-empty sets");

		std::vector<std::tuple<typename std::iterator_traits<Iterator>::value_type...>> Results;

		while (all_true(_Ranges.first != _Ranges.second...))
		{
			Results.emplace_back(*_Ranges.first...);
			pass{ ++_Ranges.first... };
		}

		return Results;
	}

	template <typename ...Ranges>
	inline auto zip(Ranges&&... _Ranges)
	{
		return zip_aux(std::make_pair(std::begin(_Ranges), std::end(_Ranges))...);
	}
#pragma endregion

#pragma region to_ref
	// this construct will always return a reference of type T, even from pointer types
	// example: to_ref(T* t).member or to_ref(T& t).member (-> is omitted when pointer type is unknown)

	template<class T>
	inline typename std::enable_if<std::is_pointer<T>::value, typename std::remove_pointer<T>::type& >::type to_ref(T& t) {	return *t;	}

	template<class T>
	inline std::enable_if_t<!std::is_pointer<T>::value, T&>	to_ref(T& t) { return t; }

#pragma endregion

#pragma region has_func
	// construct a template type that tests wheter a member function exists or not
	// usage: HGENERATE_HAS_FUNC(Release); bool exists = has_Release<Obj>::value;

#ifndef HGENERATE_HAS_FUNC
#define HGENERATE_HAS_FUNC(func) template<typename T> \
struct Has_##func \
{ \
private: \
	typedef std::true_type yes; \
	typedef std::false_type no; \
	template<typename U> static auto test(int) -> decltype(std::declval<U>().func() == 1, yes()); \
	template<typename> static no test(...); \
public: \
	static constexpr bool value = std::is_same<decltype(test<T>(0)), yes>::value; };
#endif

#pragma endregion

#pragma region exec

	template <bool Execute, typename Func, typename... Args>
	struct exec_if_t
	{
		inline auto operator()(Func fn, Args&&... args) const { return execute<Execute>(fn, std::forward<Args>(args)...); }
		template <bool bExec> auto execute(Func fn, Args&&... args) const;
		template <>	inline auto execute<true>(Func fn, Args&&... args) const { return fn(std::forward<Args>(args)...); }
		template <>	inline auto execute<false>(Func fn, Args&&... args) const { }
	};

	// returns true if function exists
#ifndef EXEC_IF_EXISTS
#define EXEC_IF_EXISTS(_funcname) \
	HGENERATE_HAS_FUNC(_funcname) \
	template <class T> \
	struct exec_functor_##_funcname \
	{ \
		static constexpr bool value = Has_##_funcname<T>::value; \
		inline bool operator()(T f) const { return execute<value>(f);} \
		template <bool bHasFunc> bool execute(T f) const; \
		template <>	inline bool execute<true>(T f) const{to_ref(f)._funcname(); return true;} \
		template <>	inline bool execute<false>(T f) const{return false;} \
	}; \
	template <class T> \
	inline bool exec_##_funcname(T f) {exec_functor_##_funcname<T> func; return func(f); }
#endif

	EXEC_IF_EXISTS(release)
	EXEC_IF_EXISTS(Release)

	template <class T>
	void safe_destroy(T*& _pObj)
	{
		if (_pObj != nullptr)
		{
			if (exec_Release(_pObj) == false &&
				exec_release(_pObj) == false)
			{
				delete _pObj;
			}
		}

		_pObj = nullptr;
	}

	template <class T>
	void safe_release(T*& _pObj)
	{
		if (_pObj != nullptr)
		{
			if (exec_Release(_pObj) == false)
			{
				exec_release(_pObj);
			}
		}

		_pObj = nullptr;
	}

#pragma endregion

#ifndef HGENERATE_HAS_VAR
#define HGENERATE_HAS_VAR(var) template<typename T, typename V> \
struct Has_##var \
{ \
private: \
	typedef std::true_type yes; \
	typedef std::false_type no; \
	template<typename U> static auto test(int) -> decltype(std::is_same<V, decltype(U::var)>::value); \
	template<typename> static no test(...); \
public: \
	static constexpr bool value = std::is_same<decltype(test<T>(0)), yes>::value; };
#endif

	template <class ConcurrentItr, class Func, class DiffType = std::iterator_traits<ConcurrentItr>::difference_type>
	inline void conditional_parallel_for_each(ConcurrentItr _Begin, ConcurrentItr _End,
		const DiffType& _ParallelCount, Func _Func)
	{
		if (std::distance(_End, _Begin) < _ParallelCount)
		{
			std::for_each(_Begin, _End, _Func);
		}
		else
		{
			concurrency::parallel_for_each(_Begin, _End, _Func);
		}
	}

	template <class ConcurrentItr, class Func, class DiffType = std::iterator_traits<ConcurrentItr>::difference_type>
	inline void conditional_parallel_sort(ConcurrentItr _Begin, ConcurrentItr _End,
		const DiffType& _ParallelCount, Func _Func)
	{
		if (std::distance(_End, _Begin) < _ParallelCount)
		{
			std::sort(_Begin, _End, _Func);
		}
		else
		{
			concurrency::parallel_sort(_Begin, _End, _Func);
		}
	}

} // Helix

#endif // FUNCTIONAL_H