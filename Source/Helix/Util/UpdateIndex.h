//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef UPDATEINDEX_H
#define UPDATEINDEX_H

#include <atomic>
#include "hlx\src\StandardDefines.h"

namespace Helix
{
	struct UpdateIndex
	{
		explicit inline operator bool() const
		{
			return m_uStartIndex != HUNDEFINED;
		}

		inline uint32_t GetStartIndex() const { return m_uStartIndex; }
		inline uint32_t GetEndIndex() const { return m_uEndIndex; }

		inline uint32_t GetCount() const { return (m_uEndIndex - m_uStartIndex) + 1u; }

		inline void Reset()
		{
			m_uStartIndex = (HUNDEFINED32);
			m_uEndIndex = 0u;
		}

		inline void Add(uint32_t _uIndex)
		{
			m_uStartIndex = std::min(m_uStartIndex, _uIndex);
			m_uEndIndex = std::max(m_uEndIndex, _uIndex);
		}

	private:
		uint32_t m_uStartIndex = HUNDEFINED;
		uint32_t m_uEndIndex = 0u;
	};
} // Helix
#endif // UPDATEINDEX_H