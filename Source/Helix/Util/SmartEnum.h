//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef SMARTENUM_H
#define SMARTENUM_H

// Smart enums allow matching strings to enums and vice versa
#ifndef SMARTENUM_VALUE
#define SMARTENUM_VALUE(typeName, value) value, 
#endif

#ifndef STRINGIZE
#define STRINGIZE(S) #S
#endif

#ifndef SMARTENUM_STRING
#define SMARTENUM_STRING(typeName, value) STRINGIZE(typeName##_##value), 
#endif

#ifndef SMARTENUM_DEFINE_ENUM
#define SMARTENUM_DEFINE_ENUM(typeName, values) enum class typeName : uint32_t { Undefined, values(SMARTENUM_VALUE) Count };
#endif

#ifndef SMARTENUM_DEFINE_NAMES
#define SMARTENUM_DEFINE_NAMES(typeName, values) const char* typeName##_##Array [] = { values(SMARTENUM_STRING) };
#endif

#ifndef SMARTENUM_DEFINE_GET_VALUE_FROM_STRING
#define SMARTENUM_DEFINE_GET_VALUE_FROM_STRING(typeName, name)	\
	typeName get##typeName##FromString(const char* str) 					\
	{				                      			\
		for (uint32_t i = 0; i < static_cast<uint32_t>(typeName::Count) - 1; ++i)       				\
		if (!strcmp(##typeName##_##Array[i], str))											\
			return static_cast<typeName>(i + 1);																		\
		return typeName::Count;																\
	}
#endif

#ifndef getStringFromEnumValue
#define getStringFromEnumValue(typeName, value) typeName##_##Array[##value]
#endif

#ifndef getEnumValueFromString
#define getEnumValueFromString(typeName, name)	get##typeName##FromString(##name)
#endif

#endif // !SMARTENUM_H
