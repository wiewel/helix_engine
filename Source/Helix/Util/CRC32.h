//   Copyright 2020 Fabian Wahlster
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef CRC32_H
#define CRC32_H

#include "hlx\src\Bytes.h"

namespace Helix
{
	//CRC32, length is optional and should be smaller than the data size if set
	inline uint32_t CRC32(const hlx::bytes& _InputData, const size_t& _uLength = 0)
	{
		size_t uSize = 0;
		size_t uCurSize = 0;
		uint64_t uCRC32 = 0;

		if (_uLength != 0 && _uLength < _InputData.size())
		{
			uSize = _uLength;
		}
		else
		{
			uSize = _InputData.size();
		}

		uCurSize = uSize;

		const uint64_t* pData = reinterpret_cast<const uint64_t*>(_InputData.c_str());

		// compute 64bit chunks
		while (uCurSize > sizeof(uint64_t))
		{
			uCRC32 = _mm_crc32_u64(uCRC32, *pData);
			uCurSize -= sizeof(uint64_t);
			++pData;
		}

		//compute rest that does not fit into 64bit chunk
		for (; uCurSize > 0; --uCurSize)
		{
			uCRC32 = _mm_crc32_u8(static_cast<uint32_t>(uCRC32), _InputData[uSize - uCurSize]);
		}

		return static_cast<uint32_t>(uCRC32);	
	}
} // Helix

#endif // !CRC32_H
