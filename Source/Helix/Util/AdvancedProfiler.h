//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef ADVANCEDPROFILER_H
#define ADVANCEDPROFILER_H

#include "DataStructures\SyncVariable.h"
#include "hlx\src\Singleton.h"
#include "Async\SpinLock.h"
#include <limits>
#include <unordered_map>
#include "hlx\src\TimerWin32.h"
#include "hlx\src\StandardDefines.h"

namespace Helix
{
	struct ProfileChart : public Datastructures::SyncChart
	{
		inline ProfileChart(const std::string& _sName) : 
			Datastructures::SyncChart(m_fVar, _sName) {}

		inline void Update(const float& _fVal) { m_fVar = _fVal; }

	private:
		float m_fVar = 0.f;
	};

	class AdvancedProfiler : public hlx::Singleton<AdvancedProfiler>
	{
	public:
		using TDocks = std::unordered_map<std::string, Datastructures::SyncDock*>;

		AdvancedProfiler();
		~AdvancedProfiler();

		ProfileChart* GetChart(
			const std::string& _sName,
			const std::string& _sGroup,
			const std::string& _sDockName = {});

		void SetEnabled(bool _bEnabled);
		const bool GetEnabled() const;

	private:
		bool m_bEnabled = false;
		Async::SpinLock m_Lock;
		TDocks m_Docks;
	};

	inline void AdvancedProfiler::SetEnabled(bool _bEnabled){ Async::ScopedSpinLock Lock(m_Lock); m_bEnabled = _bEnabled;}
	inline const bool AdvancedProfiler::GetEnabled() const{	return m_bEnabled;}

	inline ProfileChart* AdvancedProfiler::GetChart(
		const std::string& _sName,
		const std::string& _sGroup,
		const std::string& _sDockName)
	{
		Async::ScopedSpinLock Lock(m_Lock);

		if (m_bEnabled == false)
			return nullptr;

		ProfileChart* pChart = nullptr;

		TDocks::iterator it = m_Docks.find(_sDockName);

		if (it == m_Docks.end())
		{
			pChart = new ProfileChart(_sName);
			Datastructures::SyncGroup* pGroup = new Datastructures::SyncGroup(_sGroup, { pChart }, Datastructures::WidgetLayout_Grid);
			Datastructures::SyncDock* pDock = new Datastructures::SyncDock(
				_sDockName,
				{ pGroup },
				Datastructures::DockAnchor_Any, // initial area
				false, // ontoptab
				true, // floating
				Datastructures::DockAnchor_Any, // allowed areas
				Datastructures::WidgetLayout_Grid); // group layout

			m_Docks.insert({ _sDockName, pDock });
			pDock->Bind();
		}
		else
		{
			std::lock_guard<std::recursive_mutex> lock(it->second->GetLockObject());
			for (Datastructures::SyncGroup* pGroup : it->second->GetGroups())
			{
				if (pGroup->sName == _sGroup)
				{
					std::lock_guard<std::recursive_mutex> lock(pGroup->GetLockObject());

					for (const Datastructures::SyncVariableBase* pVar : pGroup->GetVariables())
					{
						if (pVar->sName == _sName)
						{
							return (ProfileChart*)(pVar);
						}
					}

					// group found but variable does not exist yet
					pChart = new ProfileChart(_sName);
					pGroup->AddVariable(pChart);
					return pChart;
				}
			}

			// group was not found
			pChart = new ProfileChart(_sName);
			Datastructures::SyncGroup* pGroup = new Datastructures::SyncGroup(_sGroup, { pChart }, Datastructures::WidgetLayout_Grid);
			it->second->AddGroup(pGroup);
		}

		return pChart;
	} 
}// Helix

#if !defined(_FINAL) && defined(HNUCLEO)
#define HGUIPROFILE(_VarName) const int64_t iStartTime##_VarName = hlx::TimerWin32::GetCurrentCount();

#define HGUIPROFILE_END(_VarName, _Group) {static Helix::ProfileChart* pProfileChart##_VarName = nullptr; \
	if( pProfileChart##_VarName == nullptr) pProfileChart##_VarName = Helix::AdvancedProfiler::Instance()->GetChart(#_VarName, #_Group);\
	if( pProfileChart##_VarName != nullptr) pProfileChart##_VarName->Update( static_cast<float>((hlx::TimerWin32::GetCurrentCount() - iStartTime##_VarName)*1000u) / hlx::TimerWin32::GetTicksPerSecondF());}

#define HGUIPROFILEEX_END(_VarName, _Group, _Dock) {static Helix::ProfileChart* pProfileChart##_VarName = nullptr; \
	if( pProfileChart##_VarName == nullptr) pProfileChart##_VarName = Helix::AdvancedProfiler::Instance()->GetChart(#_VarName, #_Group, #_Dock);\
	if( pProfileChart##_VarName != nullptr) pProfileChart##_VarName->Update( static_cast<float>((hlx::TimerWin32::GetCurrentCount() - iStartTime##_VarName)*1000u) / hlx::TimerWin32::GetTicksPerSecondF());}

#define HGUIPROFILEDYN_END(_VarName, _Name, _Group)  {Helix::ProfileChart* pProfileChart##_VarName = Helix::AdvancedProfiler::Instance()->GetChart(_Name, _Group); \
	if(pProfileChart##_VarName != nullptr) pProfileChart##_VarName->Update( static_cast<float>((hlx::TimerWin32::GetCurrentCount() - iStartTime##_VarName)*1000u) / hlx::TimerWin32::GetTicksPerSecondF());}

#define HGUIPROFILEDYNEX_END(_VarName, _Name, _Group, _Dock)  {Helix::ProfileChart* pProfileChart##_VarName = Helix::AdvancedProfiler::Instance()->GetChart(_Name,_Group, _Dock); \
	if(pProfileChart##_VarName != nullptr) pProfileChart##_VarName->Update( static_cast<float>((hlx::TimerWin32::GetCurrentCount() - iStartTime##_VarName)*1000u) / hlx::TimerWin32::GetTicksPerSecondF());}
#else
#define HGUIPROFILE(_Name)
#define HGUIPROFILE_END(_Name,_Group)
#define HGUIPROFILEDYN_END(_VarName, _Name, _Group)
#define HGUIPROFILEEX_END(_Name,_Group, _Dock)
#define HGUIPROFILEDYNEX_END(_VarName, _Name, _Group, _Dock)
#endif

#endif // !ADVANCEDPROFILER_H
