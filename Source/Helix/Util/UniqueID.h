//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef UNIQUEID_H
#define UNIQUEID_H

//usage:
//global application context:
//uint32_t gid = UniqueID<uint32_t>::NextGlobalID();

//local class context:
//UniqueID<uint64_t> uID;
//uint64_t id = uID.NextID();

#include <atomic>
namespace Helix
{
	template <class T>
	class UniqueID
	{
	private:
		std::atomic<T> ID = T(0);
		static std::atomic<T> GlobalID;
	public:
		inline T NextID()
		{
			return ID++;
		}
		inline T GetID()
		{
			return ID.load();
		}
		static inline T NextGlobalID()
		{
			return GlobalID++;
		}
		static inline T GetGlobalID()
		{
			return GlobalID.load();
		}
	};

template<class T> T UniqueID<T>::GlobalID(0);

}

#endif // UNIQUEID_H