//   Copyright 2020 Fabian Wahlster
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef UNIQUEASSOCIATIONID_H
#define UNIQUEASSOCIATIONID_H

#include "UniqueID.h"
#include <concurrent_unordered_map.h>
#include <type_traits>

namespace Helix
{
	template <class T, class IDType = size_t, class HashFunc = std::hash<T>>
	class UniqueAssociationID
	{
		typedef typename std::result_of<HashFunc(T)>::type HashType;

		struct DefaultReduction
		{
			inline const HashType& operator()(const HashType& _ID) const { return _ID; }
		};

		using TAssociationContainer = concurrency::concurrent_unordered_map<HashType, IDType, DefaultReduction>;
	public:

		inline IDType GetAssociatedID(const T& _AssociatedType)
		{
			HashType Hash = m_HashFunc(_AssociatedType);

			TAssociationContainer::iterator it = m_Association.find(Hash);
			if (it != m_Association.end())
			{
				return it->second;
			}

			IDType ID = m_UniqueID.NextID();

			m_Association.insert({ Hash, ID });

			return ID;
		}

		inline IDType operator()(const T& _AssociatedType) { return GetAssociatedID(_AssociatedType); }

		inline size_t GetAssociationCount() const { return m_Association.size(); }

	private:
		UniqueID<IDType> m_UniqueID;
		HashFunc m_HashFunc;
		TAssociationContainer m_Association;
	};
} // Helix

#endif // !UNIQUEASSOCIATIONID_H