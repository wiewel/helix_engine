//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "AdvancedProfiler.h"

using namespace Helix;
//---------------------------------------------------------------------------------------------------

AdvancedProfiler::AdvancedProfiler()
{
}
//---------------------------------------------------------------------------------------------------

AdvancedProfiler::~AdvancedProfiler()
{
	for (TDocks::value_type& KV : m_Docks)
	{
		// delete heap allocated groups
		for (const Datastructures::SyncGroup* pGroup : KV.second->GetGroups())
		{
			for (const Datastructures::SyncVariableBase* pVar : pGroup->GetVariables())
			{
				HSAFE_DELETE(pVar);
			}

			HSAFE_DELETE(pGroup);
		}

		HSAFE_DELETE(KV.second);
	}
}
//---------------------------------------------------------------------------------------------------
