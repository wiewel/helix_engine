//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef FLAG_H
#define FLAG_H

#include <cstdint>
#include <type_traits>
#include "hlx\src\ByteStream.h"

namespace Helix
{
	//use this macro to define your flags, it forces the compiler to use a 32bit int for enum storage:
#define Flag32E(Name) enum Name : uint32_t
#define Flag64E(Name) enum Name : uint64_t

	template <typename T>
	class Flag
	{
	public:
		constexpr Flag() noexcept : m_Flag(T(0)) {}

		constexpr Flag(const Flag& _Flag) noexcept : m_Flag(_Flag.m_Flag) { }

		constexpr Flag(const uint64_t& _Flag) noexcept : m_Flag(static_cast<T>(_Flag)) {}
		constexpr Flag(const uint32_t& _Flag) noexcept : m_Flag(static_cast<T>(_Flag)) {}

		inline operator uint64_t() { return static_cast<uint64_t>(m_Flag); }

		inline const T& GetFlag() const
		{
			return m_Flag;
		}

		inline bool CheckFlag(const T& _Flag) const
		{
			return (static_cast<uint64_t>(m_Flag) & static_cast<uint64_t>(_Flag)) == static_cast<uint64_t>(_Flag);
		}

		inline bool CheckFlag(const Flag& _Flag) const
		{
			return CheckFlag(_Flag.GetFlag());
		}

		// sets/adds flags from _Flag
		inline void SetFlag(const T& _Flag)
		{
			m_Flag = static_cast<T>(static_cast<uint64_t>(m_Flag) | static_cast<uint64_t>(_Flag));
		}
		
		inline void SetFlag(const Flag& _Flag)
		{
			m_Flag |= _Flag.GetFlag();
		}

		// zero/ clear all flags
		inline void ResetFlag()
		{
			m_Flag = T(0);
		}

		// zero all flags equal to flags set in _Flag
		inline void ClearFlag(const T& _Flag)
		{
			//static_cast<uint64_t&>(m_Flag) &= ~static_cast<uint64_t>(_Flag);
			//0001001 mFlag
			//1001000 _Flag

			//1000001 XOR
			//0001001 mFlag
			//0000001 AND
			m_Flag = static_cast<T>(static_cast<uint64_t>(m_Flag) & (static_cast<uint64_t>(m_Flag) ^ static_cast<uint64_t>(_Flag)));
		}

		// Assignment
		inline Flag& operator= (const Flag& rhs)
		{
			m_Flag = rhs.GetFlag();
			return *this;
		}

		inline Flag& operator= (const T& rhs)
		{
			m_Flag = rhs;
			return *this;
		}

		//Equality operator, checks if all flags are identical
		inline bool operator== (const Flag& rhs) const
		{
			return m_Flag == rhs.GetFlag();
		}

		inline bool operator== (const T& rhs) const
		{
			return m_Flag == rhs;
		}

		//Inquality operator, checks if all flags are distinct
		inline bool operator!= (const Flag& rhs) const
		{
			return m_Flag != rhs.GetFlag();
		}

		inline bool operator!= (const T& rhs) const
		{
			return m_Flag != rhs;
		}

		// Bitwise OR
		inline Flag operator| (const Flag& rhs) const
		{
			Flag Ret(m_Flag);
			Ret.SetFlag(rhs.GetFlag());
			return Ret;
		}

		inline Flag operator| (const T& rhs) const
		{
			Flag Ret(m_Flag);
			Ret.SetFlag(rhs);
			return Ret;
		}

		// Bitwise OR mutable
		inline Flag& operator|= (const Flag& rhs)
		{
			SetFlag(rhs.GetFlag());
			return *this;
		}
		
		inline Flag& operator|= (const T& rhs)
		{
			SetFlag(rhs);
			return *this;
		}

		inline Flag& operator&= (const T& rhs)
		{
			SetFlag(rhs);
			return *this;
		}

		//check if flag is set
		inline bool operator& (const Flag& rhs) const
		{
			return CheckFlag(rhs.GetFlag());
		}

		inline bool operator& (const T& rhs) const
		{
			return CheckFlag(rhs);
		}

		//check if flag is not set
		inline bool operator^ (const Flag& rhs) const
		{
			return CheckFlag(rhs.GetFlag()) == false;
		}

		inline bool operator^ (const T& rhs) const
		{
			return CheckFlag(rhs) == false;
		}

	protected:
		T m_Flag;
	};

}; // Helix

#endif // FLAG_H