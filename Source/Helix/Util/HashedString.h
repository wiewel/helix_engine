//   Copyright 2020 Fabian Wahlster
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef HASHEDSTRING_H
#define HASHEDSTRING_H

#include <string>
#include "BaseHash.h"

namespace Helix
{
	static const std::hash<std::string> SH;
	static const std::hash<std::wstring> SWH;

	template <typename Str>
	size_t StrHash(Str);

	template <>
	inline size_t StrHash(const std::string&& str) { return SH(str); }

	template <>
	inline size_t StrHash(std::string&& str) { return SH(str); }

	template <>
	inline size_t StrHash(const std::string& str) { return SH(str); }

	template <>
	inline size_t StrHash(std::string& str) { return SH(str); }

	template <>
	inline size_t StrHash(std::string str) { return SH(str); }

	template <>
	inline size_t StrHash(const std::wstring&& str) { return SWH(str); }

	template <>
	inline size_t StrHash(std::wstring&& str) { return SWH(str); }

	template <>
	inline size_t StrHash(const std::wstring& str) { return SWH(str); }

	template <>
	inline size_t StrHash(std::wstring& str) { return SWH(str); }

	template <>
	inline size_t StrHash(std::wstring str) { return SWH(str); }

	template <>
	inline size_t StrHash(const char* pStr) { return std::_Hash_seq((const unsigned char*)pStr, strlen(pStr)); }

	template<int size>
	inline size_t StrHash(const char(&_First)[size]) { return std::_Hash_seq((const unsigned char*)_First, size - 1); }

	class HashedString
	{
	public:
		inline HashedString(const std::string& _sString = {}) :
			m_uHash(SH(_sString)) {}

		template<int size = 0>
		inline HashedString(const char(&pStr)[size]) :
			m_uHash(StrHash(pStr)) {}

		inline HashedString(uint64_t _uHash) : m_uHash(_uHash) {}
		inline HashedString(const HashedString& _Str) : m_uHash(_Str.m_uHash) {}

		//inline HashedString& operator=(const uint64_t& _uHash) { m_uHash = _uHash; return *this; }
		inline HashedString& operator=(const HashedString& _Val) { m_uHash = _Val.m_uHash; return *this; }
		inline HashedString& operator=(const std::string& _sString) { m_uHash = SH(_sString); return *this; }
				
		inline operator uint64_t() const { return m_uHash; }
		inline bool operator==(std::nullptr_t) const { return m_uHash == 0; }
		inline bool operator!=(std::nullptr_t) const { return m_uHash != 0;}
		inline explicit operator bool() const { return m_uHash != 0; }
		inline bool operator==(const HashedString& _String) const { return m_uHash == _String.m_uHash; }
		inline bool operator!=(const HashedString& _String) const { return m_uHash != _String.m_uHash; }
		inline bool operator<(const HashedString& _String) const { return m_uHash < _String.m_uHash; }

		//inline friend HashedString operator+(const HashedString& l, const HashedString& r)	{return CombineHashes(l.m_uHash, r.m_uHash);}

		inline const uint64_t& GetHash() const { return m_uHash; }

	protected:
		uint64_t m_uHash = 0;
	};

	class HashedStringValue : public HashedString
	{
	public:
		//inline HashedStringValue(const uint64_t& _uHash = 0u) : HashedString(_uHash) {}		
		inline HashedStringValue(const HashedStringValue& _Val) : HashedString(_Val.m_uHash), m_sString(_Val.m_sString) {}		
		inline HashedStringValue(const std::string& _sString = {}) : HashedString(_sString), m_sString(_sString) {}

		template<int size = 0>
		inline HashedStringValue(const char(&pStr)[size]) : HashedString(StrHash(pStr)), m_sString(pStr) {}

		inline HashedStringValue& operator=(const HashedStringValue& _Val)
		{
			m_sString = _Val.m_sString;
			m_uHash = _Val.m_uHash;
			return *this;
		}

		inline HashedStringValue& operator=(const std::string& _sVal)
		{
			m_sString = _sVal;
			m_uHash = SH(_sVal);
			return *this;
		}

		inline HashedStringValue& operator=(std::nullptr_t) { m_sString.clear(); m_uHash = 0ull; return *this; }

		inline const std::string& GetString() const { return m_sString; }

		inline operator std::string() const { return m_sString; }

	private:
		std::string m_sString;
	};

}// Helix

#endif // HASHEDSTRING_H