//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef SEQUENCE_H
#define SEQUENCE_H

#include <vector>
#include "Math\MathTypes.h"
#include "ITrack.h"

#ifdef GetCurrentTime
#undef GetCurrentTime
#endif

namespace Helix
{
	namespace Animation
	{
		class Sequence
		{
		public:
			Sequence();
			~Sequence();

			// starts all tracks of the sequence from where they are left
			void Play();
			// pauses all tracks
			void Pause();
			// rewinds all tracks
			void Reset();
			// jump to time
			void Jump(double _fTime);
			// resume currently must be called from the script. in the future this should only be done by the logicscene
			void Resume(double _fTrackTime);
			
			// END OF SEQUENCE
			// get end of sequence
			const double& GetEndTime() const;
			// set end of sequence. now it is not needed to set the end time anymore. the longest track will determine the end time.
			void SetEndTime(const double& _fEndTime);
			// end of sequence property
			__declspec(property(get = GetEndTime, put = SetEndTime)) double EndTime;

			// LOOP
			// get if sequence is looping
			const bool& GetLoop() const;
			// set if sequence is looping
			void SetLoop(const bool& _bLoop);
			// property if sequence is looping
			__declspec(property(get = GetLoop, put = SetLoop)) bool IsLooping;
			
			// Playtime
			const double& GetPlaytime() const;
			__declspec(property(get = GetPlaytime, put = Jump)) double Playtime;
			
			// SEQUENCE STATUS
			const bool IsPlaying() const;

			// add a track to the sequence. all tracks will then be progressed by this sequences resume.
			void AddTrack(std::shared_ptr<Animation::ITrack> _Track);
			
			size_t GetNumberOfTracks() const;
			std::vector<std::shared_ptr<Animation::ITrack>>::const_iterator TracksBegin() const;
			std::vector<std::shared_ptr<Animation::ITrack>>::const_iterator TracksEnd() const;

		protected:
			// changed by Play/ Pause and Resume if end of sequence is reached
			bool m_bIsPlaying = false;
			// changed increnentally by resume, or set by Jump
			double m_fPlaytime = 0.0f;
			// after this time the sequence will stop
			double m_fEndTime = 0.0f;
			// if end of sequence is reached jump to start again
			bool m_bIsLooping = false;
			// hold tracks that are 'read' by the sequence
			std::vector<std::shared_ptr<Animation::ITrack>> m_Tracks;
		};
		//---------------------------------------------------------------------------------------------------
		inline std::vector<std::shared_ptr<Animation::ITrack>>::const_iterator Sequence::TracksBegin() const { return m_Tracks.cbegin(); }
		//---------------------------------------------------------------------------------------------------
		inline std::vector<std::shared_ptr<Animation::ITrack>>::const_iterator Sequence::TracksEnd() const{ return m_Tracks.cend(); }
		inline const double& Sequence::GetPlaytime() const { return m_fPlaytime; }
		inline const bool Sequence::IsPlaying() const { return m_bIsPlaying; }
		//---------------------------------------------------------------------------------------------------
		inline void Sequence::SetEndTime(const double& _fEndTime) { m_fEndTime = _fEndTime; }
		inline const double& Sequence::GetEndTime() const { return m_fEndTime; }
		inline void Sequence::SetLoop(const bool& _bLoop)
		{
			m_bIsLooping = _bLoop;
		}
		inline const bool& Sequence::GetLoop() const
		{
			return m_bIsLooping;
		}
		//---------------------------------------------------------------------------------------------------
		inline void Sequence::AddTrack(std::shared_ptr<Animation::ITrack> _Track)
		{
			m_fEndTime = std::max(_Track->GetEndTime(), m_fEndTime);
			m_Tracks.push_back(_Track);
		}
		//---------------------------------------------------------------------------------------------------
		inline size_t Sequence::GetNumberOfTracks() const
		{
			return m_Tracks.size();
		}
	}
}

#endif