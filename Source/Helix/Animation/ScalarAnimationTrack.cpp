//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "ScalarAnimationTrack.h"
#include "hlx\src\StandardDefines.h"

using namespace Helix::Animation;

float ScalarAnimationTrack::GetData() const
{
	ScalarSegment Segment = FindSegment(m_fAnimationTime);
	float fInterpolant = (m_fAnimationTime - Segment.fStartTime) / (Segment.fEndTime - Segment.fStartTime);
	fInterpolant = TimeDistanceSigmoidal(fInterpolant, Segment.fEaseIn, Segment.fEaseOut);

	return InterpolateLinear(Segment.fStartData, Segment.fEndData, fInterpolant);
}

float ScalarAnimationTrack::GetUnsmoothedData() const
{
	ScalarSegment Segment = FindSegment(m_fAnimationTime);
	float fInterpolant = (m_fAnimationTime - Segment.fStartTime) / (Segment.fEndTime - Segment.fStartTime);
	//fInterpolant = TimeDistanceSigmoidal(fInterpolant, Segment.fEaseIn, Segment.fEaseOut);
	
	return InterpolateLinear(Segment.fStartData, Segment.fEndData, fInterpolant);
}

void ScalarAnimationTrack::BuildTrack()
{
	float fStartTime = 0.0f;
	float fStartData = m_Origin;
	ScalarSegment Segment;

	HFOREACH_CONST(KeyFrameItr, KeyFrameEnd, m_KeyFrameList)
		
		Segment.fStartTime = fStartTime;
		Segment.fEndTime = KeyFrameItr->fTime;

		Segment.fStartData = fStartData;
		Segment.fEndData = KeyFrameItr->fTime;

		Segment.fEaseIn = 0.3f;
		Segment.fEaseOut = 0.3f;
		
		m_SegmentList.emplace(Segment.fEndTime,Segment);

	HFOREACH_CONST_END
	
	m_KeyFrameList.clear();
}

const ScalarAnimationTrack::ScalarSegment& ScalarAnimationTrack::FindSegment(const float _fTime) const
{
	return m_SegmentList.lower_bound(_fTime)->second;
}

float ScalarAnimationTrack::InterpolateLinear(const float _fStart, const float _fEnd, const float _fInterpolant) const
{
	return _fStart + _fInterpolant * (_fEnd - _fStart);
}

//float Helix::Animation::ScalarAnimationTrack::InterpolateSigmoidal(const float _fStart, const float _fEnd, const float _fInterpolant, const float _fSmoothness) const
//{
//	// sinusodial segments:
//	//				,--  ]- upper sinusodial segment
//	//			   x  <---- k2
//	//		     /   ]----- line segment
//	//		    x   <------ k1 
//	//		---�   ]------- lower sinusodial segment
//	const float fPiOverTwo = XM_PI / 2.0f;
//	const float fTwoOverPi = 2.0f / XM_PI;
//
//	// Computer Animation: Algorithms and Techniques, S82
//
//	const float fK1 = _fSmoothness;
//	const float fK2 = 1.0f - _fSmoothness;
//	const float fLine = fK1 * fTwoOverPi + fK2 - fK1 + (1.0f - fK2) * fTwoOverPi;
//	float fSinusodial = 0.0f;
//
//	if (_fInterpolant < fK1)
//	{
//		// lower sinusodial segment
//		fSinusodial = fK1 * fTwoOverPi * (XMScalarSinEst((_fInterpolant / fK1) * fPiOverTwo - fPiOverTwo) + 1);
//	}
//	else if (_fInterpolant > fK2)
//	{
//
//		// upper sinusodial segment
//		fSinusodial = fK1 * fTwoOverPi + fK2 - fK1 + (1.0f - fK2) * fTwoOverPi * XMScalarSinEst(((_fInterpolant - fK2) / (1.0f - fK2)) * fPiOverTwo);
//	}
//	else
//	{
//		// line segment
//		fSinusodial = fK1 * fTwoOverPi + _fInterpolant - fK1;
//	}
//	float fResult = fSinusodial / fLine;
//	return fResult;
//}
