//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

/////////////////////////////////////////////////////////////////////////////////////////////////
// AnimationTrack
//-----------------------------------------------------------------------------------------------
// Summary:
// This class contains a single animation track. The template parameter T can be a FLOAT2, 
// FLOAT3, FLOAT4 as well as any scalar value. 
// Bezier interpolation currently only works on FLOAT3 Values.
//-----------------------------------------------------------------------------------------------
// Future Work:
//	- Parabolic Ease-In/Ease-Out (REF#1 P89)
//	- Quaternion Interpolation
//	- Each animation controller could run in its own thread.?
//-----------------------------------------------------------------------------------------------
// Example:
// VectorAnimationTrack CameraTrack;
// CameraTrack.AddKeyFrame<XMFLOAT3>(0.0f, XMFLOAT3(0.0f, 0.0f, 0.0f));
// CameraTrack.AddKeyFrame<XMFLOAT3>(5.0f, XMFLOAT3(0.0f, 0.0f, 5.0f));
// CameraTrack.AddKeyFrame<XMFLOAT3>(10.0f, XMFLOAT3(5.0f, 0.0f, 0.0f));
// CameraTrack.BuildTrack();
// ...
// CameraTrack.Start();
// ...
// CameraTrack.Update(_fDeltaTime);
// ...
// Camera.Position = CameraTrack.GetData();
//-----------------------------------------------------------------------------------------------
// References:
// 1) Computer Animation: Algorithms and Techniques, by Rick Parent, ISBN:978-0124158429
/////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef ANIMATION_H
#define ANIMATION_H

#include <vector>

#include "Math\XMMath.h"
#include "hlx\src\Logger.h"


namespace Helix
{
	namespace Animation
	{
		using namespace DirectX;
		
		template<class T>
		class AnimationTrack
		{
		protected:
			template<class T>
			struct KeyFrame
			{
				KeyFrame():fTime(0.0f), Data()
				{
				}
				KeyFrame(float _fTime, std::vector<T>& _Data) : fTime(_fTime), Data(_Data)
				{ 
				}
				float fTime;
				std::vector<T> Data;
			};

		public:

			void Update(float _fDeltaTime);
			void Start();
			void Pause();
			void Restart();
			void SetLooping(bool _bIsLoopig);
			void AddKeyFrame(float _fTime, std::vector<T>& _Data); 
			void AddKeyFrame(float _fTime, T _Data);
			bool IsEmpty();

		protected:
			float TimeDistanceSigmoidal(float _fTime, float _fEaseIn, float _fEaseOut) const;
			void OnFinished();

			float m_fAnimationTime = 0.0f;
			float m_fStartTime = FLT_MAX;
			float m_fEndTime = -1.0f;

			bool m_bPaused = true;
			bool m_bIsLooping = false;

			std::vector<KeyFrame<T>> m_KeyFrameList;
			T m_Origin;
		};



		template<class T>
		inline void AnimationTrack<T>::Update(float _fDeltaTime)
		{
			if (m_bPaused)
			{
				return;
			}
			if (m_fAnimationTime + _fDeltaTime > m_fEndTime)
			{
				m_bPaused = true;
				OnFinished();
				return;
			}

			m_fAnimationTime += _fDeltaTime;
		}

		template<class T>
		inline void AnimationTrack<T>::Start()
		{
			m_bPaused = false;
		}

		template<class T>
		inline void AnimationTrack<T>::Pause()
		{
			m_bPaused = true;
		}

		template<class T>
		inline void AnimationTrack<T>::Restart()
		{
			m_bPaused = false;
			m_fAnimationTime = 0.0f;
		}

		template<class T>
		inline void AnimationTrack<T>::SetLooping(bool _bIsLoopig)
		{
			m_bIsLooping = _bIsLoopig;
		}

		template<class T>
		inline void AnimationTrack<T>::AddKeyFrame(float _fTime, std::vector<T>& _Data)
		{
			std::vector<T> Data = _Data;
			if (m_fEndTime < 0.0f)
			{
				m_fStartTime = 0.0f;
				m_Origin = Data.front();
				Data.erase(Data.begin());
			}
			m_fEndTime = _fTime;
			if (!Data.empty())
			{
				m_KeyFrameList.push_back(KeyFrame<T>(_fTime, Data));
			}
		}

		template<class T>
		inline void AnimationTrack<T>::AddKeyFrame(float _fTime, T _Data)
		{
			if (m_fEndTime < 0.0f)
			{
				m_fStartTime = 0.0f;
				m_Origin = _Data;
				m_fEndTime = _fTime;
				return;
			}
			m_fEndTime = _fTime;
			std::vector<T> Data; 
			Data.push_back(_Data);
			m_KeyFrameList.push_back(KeyFrame<T>(_fTime, Data));
		}

		template<class T>
		inline bool AnimationTrack<T>::IsEmpty()
		{
			return m_KeyFrameList.empty();
		}

		template<class T>
		inline float AnimationTrack<T>::TimeDistanceSigmoidal(float _fTime, float _fEaseIn, float _fEaseOut) const
		{
			// sinusodial segments:
			//				,--  ]- upper sinusodial segment
			//			   x  <---- k2
			//		     /   ]----- line segment
			//		    x   <------ k1 
			//		---�   ]------- lower sinusodial segment
			const float fPiOverTwo = XM_PI / 2.0f;
			const float fTwoOverPi = 2.0f / XM_PI;

			// Computer Animation: Algorithms and Techniques, S82

			const float fK1 = _fEaseOut;
			const float fK2 = 1.0f - _fEaseIn;
			const float fLine = fK1 * fTwoOverPi + fK2 - fK1 + (1.0f - fK2) * fTwoOverPi;
			float fSinusodial = 0.0f;

			if (_fTime < fK1)
			{
				// lower sinusodial segment
				fSinusodial = fK1 * fTwoOverPi * (XMScalarSinEst((_fTime / fK1) * fPiOverTwo - fPiOverTwo) + 1);
			}
			else if (_fTime > fK2)
			{

				// upper sinusodial segment
				fSinusodial = fK1 * fTwoOverPi + fK2 - fK1 + (1.0f - fK2) * fTwoOverPi * XMScalarSinEst(((_fTime - fK2) / (1.0f - fK2)) * fPiOverTwo);
			}
			else
			{
				// line segment
				fSinusodial = fK1 * fTwoOverPi + _fTime - fK1;
			}
			float fResult = fSinusodial / fLine;
			return fResult;
		}

		template<class T>
		inline void AnimationTrack<T>::OnFinished()
		{
			if (m_bIsLooping)
			{
				Restart();
			}
		}
	}
}

#endif
