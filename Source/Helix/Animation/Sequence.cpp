//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "Sequence.h"
#include "hlx\src\Logger.h"
#include <algorithm>

using namespace Helix::Animation;

//---------------------------------------------------------------------------------------------------
Sequence::Sequence()
{
}
//---------------------------------------------------------------------------------------------------
Sequence::~Sequence()
{
}
//---------------------------------------------------------------------------------------------------
void Sequence::Play()
{
	m_bIsPlaying = true;
}
//---------------------------------------------------------------------------------------------------
void Sequence::Pause()
{
	m_bIsPlaying = false;
}
//---------------------------------------------------------------------------------------------------
void Sequence::Reset()
{
	m_bIsPlaying = false;
	Jump(0.0);
}
//---------------------------------------------------------------------------------------------------
void Sequence::Jump(double _fTime)
{
	if (m_bIsLooping)
	{
		// jump to position in 'future' or 'before' loops
		m_fPlaytime = std::fmod(_fTime, m_fEndTime);
	}
	else
	{
		// clamp time to sequence
		m_fPlaytime = std::min(std::max(_fTime, 0.0), m_fEndTime);
	}
	
	// finally jump all the tracks to the accepted time
	for (auto it : m_Tracks)
	{
		it->SetTime(m_fPlaytime);
	}
}
//---------------------------------------------------------------------------------------------------
void Sequence::Resume(double _fDeltaTime)
{
	if (m_bIsPlaying)
	{
		// propose new time
		m_fPlaytime += _fDeltaTime;

		// check for end of sequence
		if (m_fPlaytime >= m_fEndTime)
		{
			if (m_bIsLooping)
			{
				// looping sequence -> reset time after end
				m_fPlaytime = std::fmod(m_fPlaytime, m_fEndTime);
			}
			else
			{
				// non looping sequence -> stop after end
				m_fPlaytime = std::min(m_fPlaytime, m_fEndTime);
				m_bIsPlaying = false;
			}
		}
	
		// finally resume all the tracks to the accepted time
		for (std::shared_ptr<ITrack>& it : m_Tracks)
		{
			it->SetTime(m_fPlaytime);
		}
	}
}