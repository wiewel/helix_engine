//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

/////////////////////////////////////////////////////////////////////////////////////////////////
// Curve Math
//-----------------------------------------------------------------------------------------------
// Summary:
// This file collects all the curves used for animation.
/////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef CURVEMATH_H
#define CURVEMATH_H

#include "hlx\src\StandardDefines.h"
#include "hlx\src\Logger.h"
#include "Math\XMMath.h"
#include "LookupTable.h"
#include <set>

namespace Helix
{
	namespace Animation
	{
		using namespace DirectX;

		// use this curve structure for storage
		struct Curve
		{
			Curve()
			{
				vP0 = vP1 = vP2 = vP3 = XMFLOAT3_ZERO;
			}
			Curve(const Curve& _C)
			{
				vP0 = _C.vP0;
				vP1 = _C.vP1;
				vP2 = _C.vP2;
				vP3 = _C.vP3;
			}

			XMFLOAT3 vP0;
			XMFLOAT3 vP1;
			XMFLOAT3 vP2;
			XMFLOAT3 vP3;
		};

		// this curve structure is only for computation
		struct XMVectorCurve
		{
			XMVectorCurve()
			{
			}
			XMVectorCurve(const XMFLOAT3 _vP0, const XMFLOAT3 _vP1, const XMFLOAT3 _vP2, const XMFLOAT3 _vP3)
				: vP0(XMLoadFloat3(&_vP0)), vP1(XMLoadFloat3(&_vP0)), vP2(XMLoadFloat3(&_vP0)), vP3(XMLoadFloat3(&_vP0))
			{
			}
			XMVectorCurve(const Curve _C)
				: vP0(XMLoadFloat3(&_C.vP0)), vP1(XMLoadFloat3(&_C.vP1)), vP2(XMLoadFloat3(&_C.vP2)), vP3(XMLoadFloat3(&_C.vP3))
			{

			}
			XMVECTOR vP0;
			XMVECTOR vP1;
			XMVECTOR vP2;
			XMVECTOR vP3;
		};

		//==============================================================================================================
		// Bezier Path
		//--------------------------------------------------------------------------------------------------------------
		// Summary:
		// Combination of single bezier curves to a more complex curve. 
		//==============================================================================================================
		class BezierPath
		{
			HDEBUGNAME("BezierPath");

		public:
			BezierPath();
			BezierPath(XMFLOAT3 _vOrigin);
			// Extend the bezier path by a point. 
			// The point will also influence the 2nd controll point of the curve before, so that
			// the bezier path remains stetic. Also until a further point is added, the newly
			// created segment will not reflect in the total length.
			void AddPoint(const XMFLOAT3& _Point);

			XMFLOAT3 GetPosition(const float _fDistance) const;

			// Get the total length of the bezier path. Note that until Finalize() is called,
			// the last segment in the path will not be part of the total length.
			float GetTotalLength() const;

			// Finalize should be called after the last point is added to the bezier path.
			// This point will then also reflect in the total length.
			void Finalize();
		
		protected:
			// the core of the bezier calculation
			XMFLOAT3 CalculateBezier(const Curve& _Curve, float _fParameter) const;

			// start subdivision algorithm
			void CalculateArcLength(const Curve& _Curve);

			// compute controll points so that the connection between the current and the
			// previous curve is stetic.
			void ComputeControlPoints(Curve& _CurrentCurve, Curve& _PreviousCurve);
			
			// Subdivision algorithm to approximate arc length
			void Subdivide(const XMVectorCurve& _FullCurve, float& _fTotalLength, const float _fTolerance = 0.01f, float _fChunkSize = 1.0f, float _fT = 0.0f);
			
			// Split bezier curve in two
			void Split(const XMVectorCurve& _Full, XMVectorCurve& _Left, XMVectorCurve& _Right) const;

			// Helper for appending the arc length approximation values of the subdivision 
			// algorithm to the lookup table
			void AppendToLookup(const float _fArcLength, const float _fBezierParameter);

			//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
			std::vector<Curve> m_PathSegments; // vector containing the individual curve segments
			IndexedNonUniformLookup<float, uint32_t, float> m_ArcLengthToBezierParameter; // the lookup table for the arc length approximation
			float m_fSmoothness = 1.0f; // smoothness of the compute controll points function
			float m_fTotalLength = 0.0f;	// the total length of the bezier path
			uint32_t m_uNumberSegments = 0;
			XMFLOAT3 m_vOrigin;
		};

		inline float BezierPath::GetTotalLength() const { return m_fTotalLength; }
		

		//==============================================================================================================
		// Speed Control
		// -------------------------------------------------------------------------------------------------------------
		// Summary:
		// The Time Distance function maps the animation time to a arc length on the value interpolator
		// -------------------------------------------------------------------------------------------------------------
		// TODO: 
		// After the demo this should be rewritten, with a parabolic Time Distance Funcion (not sinusoidal) 
		//==============================================================================================================
		class TimeDistance
		{
			HDEBUGNAME("TimeDistance");
			struct TimeSegment
			{
				float fStartTime;
				float fEndTime;
				float fStartDistance;
				float fEndDistance;
				float fEaseIn;
				float fEaseOut;
			};

		public:
			// returns the distance in arc length for a given time
			float GetDistance(float _fTime) const;

			// append a time segment
			void AddKeyTime(float _fTime, float _fDistance, float _fEaseIn = 0.3f, float _fEaseOut = 0.3f, float _fStartVelocity = 0.3f, float _fEndVelocity = 0.0f);
		
		private:

			// search the time segment list for the segment to use
			const TimeSegment& FindSegment(float _fTime) const;

			// the sinusodial easing function. later it should be replaced by a more adaptive
			// parabolic finction. also it is quite costly, maybe a lookup table should be used.
			float Sinusoidal(float _fTime, float _fEaseIn, float _fEaseOut) const;

			//- - - - - - - - - - - - - - - - - - - - - - 
			//float m_StartTime = 0.0f;
			std::vector<TimeSegment> m_TimeSegmentList;
		};

		///////////////////////////////////////////////////////////////////////////////////////////
		// The following only is stuff that is currently unused.
		// It is kept, because of the GaussianQuadrature and the general subdivision algorithm,
		// which can be used to compute the arc length of ANY curve.
		///////////////////////////////////////////////////////////////////////////////////////////

		////==============================================================================================================
		//// Arc Length
		////==============================================================================================================
		//class ArcLength
		//{
		//private:
		//	struct Interval
		//	{
		//		float fLength;
		//		float fLower;
		//		float fUpper;
		//	};

		//public:
		//	ArcLength();

		//	// Generate lookup table for arc length
		//	void Initialize(const Curve& _Curve, const float _fLower = 0.0f, const float _fUpper = 1.0f);

		//	// Get a value from the lookup table
		//	float At(const float _fT) const;

		//	// The total length of the arc
		//	float GetTotalLength() const;

		//private:
		//	//void SplitBezier(XMVectorCurve& _FullBezier, XMVectorCurve& _LeftBezier, XMVectorCurve& _RightBezier);

		//	// Integration
		//	float GaussianQuadrature(const Interval& _FullInterval);
		//	// 4th order polynome
		//	float EvaluatePolynome(const float _fT);
		//	// recursive subdivision algorithm
		//	//float Subdivide(const Interval& _FullInterval, float _fTotalLength, const float _fTolerance = 0.00001f);
		//	void Subdivide(const XMVectorCurve& _FullCurve, float& _fTotalLength, const float _fTolerance = 0.00001f, float _fChunkSize = 1.0f, float _fT = 0.0f);

		//	float m_fCoeff[5];
		//	ClumpedLookup<float, float> m_ParameterToArcLength;
		//	float m_fTotalLength;
		//};

		//inline float ArcLength::GetTotalLength() const { return m_fTotalLength; }

		////==============================================================================================================
		//// Bezier
		////==============================================================================================================
		//class BezierCurve
		//{
		//	HDEBUGNAME("BezierCurve");

		//public:
		//	BezierCurve();
		//	BezierCurve(const XMFLOAT3& _P0, const XMFLOAT3& _P1, const XMFLOAT3& _P2, const XMFLOAT3& _P3);

		//	void Calculate(_Out_ XMFLOAT3& _Point, _In_ float _fArcT);

		//	Curve& GetCurve() { return m_CurvePoints; }
		//	static void Split(const XMVectorCurve& _Full, XMVectorCurve& _Left, XMVectorCurve& _Right);

		//protected:
		//	Curve m_CurvePoints;
		//};

		//inline void BezierCurve::Split(const XMVectorCurve& _Full, XMVectorCurve& _Left, XMVectorCurve& _Right)
		//{
		//	_Left.vP0 = _Full.vP0;
		//	_Right.vP3 = _Full.vP3;
		//	_Left.vP1 = XMVectorLerp(_Full.vP0, _Full.vP1, 0.5f);
		//	XMVECTOR vP5 = XMVectorLerp(_Full.vP1, _Full.vP2, 0.5f);
		//	_Right.vP2 = XMVectorLerp(_Full.vP2, _Full.vP3, 0.5f);
		//	_Left.vP2 = XMVectorLerp(_Left.vP1, vP5, 0.5f);
		//	_Right.vP1 = XMVectorLerp(vP5, _Right.vP2, 0.5f);
		//	_Right.vP0 = _Left.vP3 = XMVectorLerp(_Left.vP2, _Right.vP1, 0.5f);
		//}
	}



}

#endif