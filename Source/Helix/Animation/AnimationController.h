//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

/////////////////////////////////////////////////////////////////////////////////////////////////
// AnimationController
//-----------------------------------------------------------------------------------------------
// Summary:
//
// Controls animation tracks for one single actor.
//-----------------------------------------------------------------------------------------------
// Future Work:
//
//	- container of all animation tracks for the actor
//	- controll functions for the tracks (update, pause, loop, etc)
//	- accessor for the animation tracks
/////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef ANIMATIONCONTROLLER_H
#define ANIMATIONCONTROLLER_H

#include "hlx\src\Logger.h"
#include "QuaternionAnimationTrack.h"
#include "VectorAnimationTrack.h"
#include "ScalarAnimationTrack.h"

namespace Helix
{
	namespace Animation
	{
		//using namespace DirectX;

		struct TVectorTrackFader
		{
			VectorAnimationTrack m_Track;
			ScalarAnimationTrack m_Blending;
		};

		struct TQuaternionTrackFader
		{
			QuaternionAnimationTrack m_Track;
			ScalarAnimationTrack m_Blending;
		};

		class AnimationController
		{
			HDEBUGNAME("AnimationController");
		public:
			
			AnimationController();
			~AnimationController();
			
			TVectorTrackFader* AddVectorTrack();
			TQuaternionTrackFader* AddQuaternionTrack();

			XMFLOAT3 GetVectorMaster();
			XMFLOAT3 GetVectorChannel(uint32_t _uChannelNum);
			
			XMFLOAT4 GetQuaternionMaster();
			XMFLOAT4 GetQuaternionChannel(uint32_t _uChannelNum);

			void Update(float _fDeltaMs);

		protected:
			std::vector<TVectorTrackFader> m_VectorTracks;
			std::vector<TQuaternionTrackFader> m_QuaternionTracks;

		};
	}
}

#endif