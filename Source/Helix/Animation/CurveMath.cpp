//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "CurveMath.h"

using namespace Helix::Animation;

//==============================================================================================================
// Bezier Path
//==============================================================================================================


BezierPath::BezierPath()
{
	m_vOrigin = XMFLOAT3_ZERO;
}

BezierPath::BezierPath(XMFLOAT3 _vOrigin)
{
	m_vOrigin = _vOrigin;
}

void BezierPath::AddPoint(const XMFLOAT3 & _Point)
{
	if (m_PathSegments.size() == 0)
	{
		// the first segment is between origin and the added point
		Curve First;
		First.vP0 = m_vOrigin;
		First.vP1 = Math::XMFloat3Get(XMVectorLerp(XMLoadFloat3(&m_vOrigin), XMLoadFloat3(&_Point), 0.4f));
		First.vP2 = Math::XMFloat3Get(XMVectorLerp(XMLoadFloat3(&m_vOrigin), XMLoadFloat3(&_Point), 0.6f));
		First.vP3 = _Point;
		
		m_PathSegments.push_back(First);
		
		return;
	}

	// create a new curve segment
	Curve Previous = m_PathSegments.back();
	Curve Current;
	Current.vP0 = Previous.vP3;
	Current.vP1 = Current.vP0;
	Current.vP2 = _Point;
	Current.vP3 = _Point;

	ComputeControlPoints(Current, Previous);

	Current.vP2 = Math::XMFloat3Get(XMVectorLerp(XMLoadFloat3(&Current.vP1), XMLoadFloat3(&_Point), 0.6f));
	// Previous now isn't the last segment anymore,
	// compute the arc distance function for it.
	m_PathSegments.back() = Previous;

	AppendToLookup(m_fTotalLength, 0.0f);
	CalculateArcLength(Previous);

	m_PathSegments.push_back(Current);
}

XMFLOAT3 BezierPath::GetPosition(const float _fDistance) const
{
	HASSERTD(_fDistance >= 0.0f && _fDistance <= m_fTotalLength, "Illegal bezier parameter: %f", _fDistance);

	std::pair<uint32_t, float> TableEntry = m_ArcLengthToBezierParameter.At(_fDistance);

	return CalculateBezier(m_PathSegments.at(TableEntry.first), TableEntry.second);
}

void BezierPath::Finalize()
{
	CalculateArcLength(m_PathSegments.back());
}

XMFLOAT3 BezierPath::CalculateBezier(const Curve & _Curve, float _fParameter) const
{
	// http://devmag.org.za/2011/04/05/bzier-curves-a-tutorial/
	float fT = _fParameter;
	float fU = 1.0f - fT;
	float fTT = fT * fT;
	float fUU = fU * fU;

	// cubic bezier curve:
	//	    vP_2 X----- ,--X vP_3	|		P_2------P_3
	//				 ,'				|		  \
	//				/				|		   \
	//			  ,'				|		    \
	//	vP_0 X--�------X vP_1		|	 P_0----P_1

	// P = (1-t)^3 * P_0 + 
	//	 + 3 * (1-t)^2 * t * P_1 +
	//	 + 3 * (1-t) * t^2 * P_2 +
	//	 + t^3 * P_3
	XMVECTOR vP = XMVectorScale(XMLoadFloat3(&_Curve.vP0), fUU * fU);
	vP += XMVectorScale(XMLoadFloat3(&_Curve.vP1), 3 * fUU * fT);
	vP += XMVectorScale(XMLoadFloat3(&_Curve.vP2), 3 * fU * fTT);
	vP += XMVectorScale(XMLoadFloat3(&_Curve.vP3), fTT * fT);

	return Math::XMFloat3Get(vP);
}

void BezierPath::CalculateArcLength(const Curve & _Curve)
{
	XMVectorCurve Curve(_Curve);
	
	Subdivide(Curve, m_fTotalLength);
}

void BezierPath::ComputeControlPoints(Curve & _CurrentCurve, Curve & _PreviousCurve)
{
	XMVECTOR vPreviousPoint = XMLoadFloat3(&_PreviousCurve.vP0);
	XMVECTOR vCurrentPoint = XMLoadFloat3(&_CurrentCurve.vP0);
	XMVECTOR vNextPoint = XMLoadFloat3(&_CurrentCurve.vP3);

	XMVECTOR vTangent = XMVector3Normalize(XMVectorSubtract(vNextPoint, vPreviousPoint));

	float fPreviousDistance = XMVectorGetX(XMVector3LengthEst(XMVectorSubtract(vCurrentPoint, vPreviousPoint)));
	float fNextDistance = XMVectorGetX(XMVector3LengthEst(XMVectorSubtract(vNextPoint, vCurrentPoint)));

	XMStoreFloat3(&_CurrentCurve.vP1, XMVectorAdd(XMVectorScale(vTangent, fNextDistance * m_fSmoothness * 0.5f), vCurrentPoint));
	XMStoreFloat3(&_PreviousCurve.vP2, XMVectorAdd(XMVectorScale(vTangent, -fPreviousDistance * m_fSmoothness * 0.5f), vCurrentPoint));
	
	_CurrentCurve.vP2 = _CurrentCurve.vP3;
}

void BezierPath::Subdivide(const XMVectorCurve & _FullCurve, float & _fTotalLength, const float _fTolerance, float _fChunkSize, float _fT)
{
	XMVectorCurve LeftCurve;
	XMVectorCurve RightCurve;

	float fLength = Math::XMVector3DistanceF(_FullCurve.vP0, _FullCurve.vP1);
	fLength += Math::XMVector3DistanceF(_FullCurve.vP1, _FullCurve.vP2);
	fLength += Math::XMVector3DistanceF(_FullCurve.vP2, _FullCurve.vP3);

	float fChord = Math::XMVector3DistanceF(_FullCurve.vP0, _FullCurve.vP3);
	float fDiff = (fLength - fChord);
	if (fDiff > _fTolerance)
	{
		Split(_FullCurve, LeftCurve, RightCurve);
		_fChunkSize *= 0.5;
		Subdivide(LeftCurve, _fTotalLength, _fTolerance, _fChunkSize, _fT);
		Subdivide(RightCurve, _fTotalLength, _fTolerance, _fChunkSize, _fT + _fChunkSize);
		return;
	}

	_fTotalLength += fChord +0.5f * fDiff;
	
	AppendToLookup(_fTotalLength, _fT + _fChunkSize);
	//m_ParameterToArcLength.Insert(_fT + _fChunkSize, _fTotalLength);
}

void BezierPath::Split(const XMVectorCurve & _Full, XMVectorCurve & _Left, XMVectorCurve & _Right) const
{
	_Left.vP0 = _Full.vP0;
	_Right.vP3 = _Full.vP3;
	_Left.vP1 = XMVectorLerp(_Full.vP0, _Full.vP1, 0.5f);
	XMVECTOR vP5 = XMVectorLerp(_Full.vP1, _Full.vP2, 0.5f);
	_Right.vP2 = XMVectorLerp(_Full.vP2, _Full.vP3, 0.5f);
	_Left.vP2 = XMVectorLerp(_Left.vP1, vP5, 0.5f);
	_Right.vP1 = XMVectorLerp(vP5, _Right.vP2, 0.5f);
	_Right.vP0 = _Left.vP3 = XMVectorLerp(_Left.vP2, _Right.vP1, 0.5f);
}

void BezierPath::AppendToLookup(const float _fArcLength, const float _fBezierParameter)
{
	m_ArcLengthToBezierParameter.Insert(_fArcLength, static_cast<const uint32_t>( m_PathSegments.size() - 1), _fBezierParameter);
}

//==============================================================================================================
// Speed Control
//==============================================================================================================
float TimeDistance::GetDistance(float _fTime) const
{
	if (_fTime >= 2.9f)
	{
		float ftest = 0.0f;
	}
	TimeSegment Segment = FindSegment(_fTime);
	float fTime = (_fTime - Segment.fStartTime) / (Segment.fEndTime - Segment.fStartTime);
	float fDistanceCovered = Segment.fEndDistance - Segment.fStartDistance;
	return Segment.fStartDistance + fDistanceCovered * Sinusoidal(fTime, Segment.fEaseIn, Segment.fEaseOut);
}

void TimeDistance::AddKeyTime(float _fTime, float _fDistance, float _fEaseIn, float _fEaseOut, float _fStartVelocity, float _fEndVelocity)
{
	TimeSegment Segment;
	if (m_TimeSegmentList.empty())
	{
		Segment.fStartTime = 0.0f;
		Segment.fStartDistance = 0.0f;
	}
	else
	{
		Segment.fStartTime = m_TimeSegmentList.back().fEndTime;
		Segment.fStartDistance = m_TimeSegmentList.back().fEndDistance;
	}

	//if (Segment.fStartTime >= _fTime)
	//{
	//	return;
	//}

	Segment.fEndTime = _fTime;
	Segment.fEndDistance = _fDistance;
	Segment.fEaseIn = _fEaseIn;
	Segment.fEaseOut = _fEaseOut;

	m_TimeSegmentList.push_back(Segment);
}

const TimeDistance::TimeSegment& TimeDistance::FindSegment(float _fTime) const
{
	HFOREACH_CONST(it, end, m_TimeSegmentList)
		if (it->fStartTime <= _fTime && it->fEndTime >= _fTime)
		{
			return *it;
		}
	HFOREACH_CONST_END
	return m_TimeSegmentList.front();
}

float TimeDistance::Sinusoidal(float _fTime, float _fEaseIn, float _fEaseOut) const
{
	// sinusodial segments:
	//				,--  ]- upper sinusodial segment
	//			   x  <---- k2
	//		     /   ]----- line segment
	//		    x   <------ k1 
	//		---�   ]------- lower sinusodial segment
	const float fPiOverTwo = XM_PI / 2.0f;
	const float fTwoOverPi = 2.0f / XM_PI;

	// Computer Animation: Algorithms and Techniques, S82

	const float fK1 = _fEaseOut;
	const float fK2 = 1.0f - _fEaseIn;
	const float fLine = fK1 * fTwoOverPi + fK2 - fK1 + (1.0f - fK2) * fTwoOverPi;
	float fSinusodial = 0.0f;
	
	if (_fTime <= 0.0f)
	{
		return 0.0f;
	}

	if (_fTime < fK1)
	{
		// lower sinusodial segment
		fSinusodial = fK1 * fTwoOverPi * (XMScalarSinEst((_fTime / fK1) * fPiOverTwo - fPiOverTwo) + 1);
	}
	else if (_fTime > fK2)
	{

		// upper sinusodial segment
		fSinusodial = fK1 * fTwoOverPi + fK2 - fK1 + (1.0f - fK2) * fTwoOverPi * XMScalarSinEst(((_fTime - fK2) / (1.0f - fK2)) * fPiOverTwo);
	}
	else
	{
		// line segment
		fSinusodial = fK1 * fTwoOverPi + _fTime - fK1;
	}
	float fResult = fSinusodial / fLine;
	return fResult;
}

///////////////////////////////////////////////////////////////////////////////////////////
// The following only is stuff that is currently unused.
// It is kept, because of the GaussianQuadrature and the general subdivision algorithm,
// which can be used to compute the arc length of any curve.
///////////////////////////////////////////////////////////////////////////////////////////

////==============================================================================================================
//// Arc Length
////==============================================================================================================
////--------------------------------------------------------------------------------------------------------------
//ArcLength::ArcLength()
//{
//	m_fTotalLength = 0.0f;
//}
//
////--------------------------------------------------------------------------------------------------------------
//void ArcLength::Initialize(const Curve& _Curve, const float _fLower, const float _fUpper)
//{
//	// http://steve.hollasch.net/cgindex/curves/cbezarclen.html
//	XMVECTOR vP0 = XMLoadFloat3(&_Curve.vP0);
//	XMVECTOR vP1 = XMLoadFloat3(&_Curve.vP1);
//	XMVECTOR vP2 = XMLoadFloat3(&_Curve.vP2);
//	XMVECTOR vP3 = XMLoadFloat3(&_Curve.vP3);
//
//	//// k1 = -p0 + 3*(p1 - p2) + p3;
//	//XMVECTOR vK0 = XMVectorAdd(XMVectorScale(XMVectorSubtract(vP1, vP2), 3.0f), XMVectorSubtract(vP3, vP0));
//	//// k2 = 3 * (p0 + p2) - 6 * p1;
//	//XMVECTOR vK1 = XMVectorSubtract(XMVectorScale(XMVectorAdd(vP0, vP2), 3.0f), XMVectorScale(vP1, 6.0f));
//	//// k3 = 3*(p1 - p0);
//	//XMVECTOR vK2 = XMVectorScale(XMVectorSubtract(vP1, vP0), 3.0f);
//	//// k4 = p0;
//	//XMVECTOR vK3 = vP0;
//
//	//// q1 = 9.0*(sqr(k1.x) + sqr(k1.y));
//	//m_fCoeff[4] = XMVectorGetX(XMVector3Dot(vK0, vK0)) * 9.0f;
//	//// q2 = 12.0*(k1.x*k2.x + k1.y*k2.y);
//	//m_fCoeff[3] = XMVectorGetX(XMVector3Dot(vK0, vK1)) * 12.0f;
//	//// q3 = 3.0*(k1.x*k3.x + k1.y*k3.y) + 4.0*(sqr(k2.x) + sqr(k2.y));
//	//m_fCoeff[2] = XMVectorGetX(XMVector3Dot(vK0, vK2)) * 6.0f + XMVectorGetX(XMVector3Dot(vK0, vK1)) * 4.0f;
//	//// q4 = 4.0*(k2.x*k3.x + k2.y*k3.y);
//	//m_fCoeff[1] = XMVectorGetX(XMVector3Dot(vK1, vK2)) * 4.0f;
//	//// q5 = sqr(k3.x) + sqr(k3.y);
//	//m_fCoeff[0] = XMVectorGetX(XMVector3Dot(vK2, vK2));
//
//
//	//Interval Interval;
//	//Interval.fLower = _fLower;
//	//Interval.fUpper = _fUpper;
//
//	//float fTemp = //GaussianQuadrature(Interval);
//
//	//Interval.fLength = fTemp;
//	XMVectorCurve Curve;
//	Curve.vP0 = vP0;
//	Curve.vP1 = vP1;
//	Curve.vP2 = vP2;
//	Curve.vP3 = vP3;
//
//	m_ParameterToArcLength.Insert(0.0f, 0.0f);
//	Subdivide(Curve, m_fTotalLength);
//}
//
/////<returns>The arc parameter to use.</returns>
//float ArcLength::At(const float _fT) const
//{
//	return m_ParameterToArcLength.At(_fT) / m_fTotalLength;
//}
//
//
////--------------------------------------------------------------------------------------------------------------
//float ArcLength::GaussianQuadrature(const Interval& _FullInterval)
//{
//	// Computer Animation Algorithms and techniques, P 82
//	float fAbscissae[5] = { 0.4333953941292471907992659f, 0.6133714327005903973087020f, 0.6794095682990244062343274f, 0.8650633666889845107320967f, 0.9739065285171717200779640f };
//	float fWeights[5] = { 0.2955242247147528701738930f, 0.2692667193099963550912269f, 0.2190863625159820439955349f, 0.1494513491505805931457763f, 0.0666713443086881375935688f };
//
//	float fMid = (_FullInterval.fLower + _FullInterval.fUpper) / 2.0f;
//	float fDiff = (_FullInterval.fUpper - _FullInterval.fLower) / 2.0f;
//
//	float fLength = 0.0f;
//	float fDX = 0.0;
//
//	for (int i = 0; i < 5; ++i)
//	{
//		fDX = fDiff * fAbscissae[i];
//		fLength += fWeights[i] * (sqrtf(fabsf(EvaluatePolynome(fMid + fDX))) + sqrtf(fabsf(EvaluatePolynome(fMid - fDX))));
//	}
//
//	fLength *= fDiff;
//	return fLength;
//}
////--------------------------------------------------------------------------------------------------------------
//// evaluate cubic polynome
//float ArcLength::EvaluatePolynome(const float _fT)
//{
//	// result = q5 + t*(q4 + t*(q3 + t*(q2 + t*q1)))
//	float result = m_fCoeff[4] + _fT * (m_fCoeff[3] + _fT * (m_fCoeff[2] + _fT * (m_fCoeff[1] + _fT * m_fCoeff[0])));
//	return result;
//}
////--------------------------------------------------------------------------------------------------------------
////float ArcLength::Subdivide(const Interval& _FullInterval, float _fTotalLength, const float _fTolerance)
////{
////	// Computer Animation Algorithms and Techniques
////	Interval LeftInterval;
////	Interval RightInterval;
////	float fMid;
////	float fTemp;
////	float fLeftLength;
////	float fRightLength;
////
////	fMid = (_FullInterval.fLower + _FullInterval.fUpper) / 2.0f;
////	LeftInterval.fLower = _FullInterval.fLower;
////	LeftInterval.fUpper = fMid;
////	RightInterval.fLower = fMid;
////	RightInterval.fUpper = _FullInterval.fUpper;
////	
////	fLeftLength = GaussianQuadrature(LeftInterval);
////	fRightLength = GaussianQuadrature(RightInterval);
////
////	fTemp = fabsf(_FullInterval.fLength - (fLeftLength + fRightLength));
////
////	if (fTemp > _fTolerance)
////	{
////		// Not Precise enough, subdivide
////		LeftInterval.fLength = fLeftLength;
////		RightInterval.fLength = fRightLength;
////
////		_fTotalLength = Subdivide(LeftInterval, _fTotalLength, _fTolerance);
////		_fTotalLength = Subdivide(RightInterval, _fTotalLength, _fTolerance);
////		
////		return _fTotalLength;
////	}
////	else
////	{
////		// Fill values into lookup table
////		_fTotalLength += fLeftLength;
////		m_ParameterToArcLength.Insert(fMid, _fTotalLength);
////		_fTotalLength += fRightLength;
////		m_ParameterToArcLength.Insert(_FullInterval.fUpper, _fTotalLength);
////		return _fTotalLength;
////	}
////}
//
//void ArcLength::Subdivide(const XMVectorCurve& _FullCurve, float& _fTotalLength, const float _fTolerance, float _fChunkSize, float _fT)
//{
//	XMVectorCurve LeftCurve;
//	XMVectorCurve RightCurve;
//
//
//	float fLength = Math::XMVector3DistanceF(_FullCurve.vP0, _FullCurve.vP1);
//	fLength += Math::XMVector3DistanceF(_FullCurve.vP1, _FullCurve.vP2);
//	fLength += Math::XMVector3DistanceF(_FullCurve.vP2, _FullCurve.vP3);
//
//	float fChord = Math::XMVector3DistanceF(_FullCurve.vP0, _FullCurve.vP3);
//
//	if ((fLength - fChord) > _fTolerance)
//	{
//		BezierCurve::Split(_FullCurve, LeftCurve, RightCurve);
//		_fChunkSize *= 0.5;
//		Subdivide(LeftCurve, _fTotalLength, _fTolerance, _fChunkSize, _fT);
//		Subdivide(RightCurve, _fTotalLength, _fTolerance, _fChunkSize, _fT + _fChunkSize);
//		return;
//	}
//	_fTotalLength += fLength;
//	m_ParameterToArcLength.Insert(_fT + _fChunkSize, _fTotalLength);
//
//	return;
//}
//
//
////==============================================================================================================
//// Bezier
////==============================================================================================================
////--------------------------------------------------------------------------------------------------------------
//BezierCurve::BezierCurve()
//{
//
//}
////--------------------------------------------------------------------------------------------------------------
//BezierCurve::BezierCurve(const XMFLOAT3& _P0, const XMFLOAT3& _P1, const XMFLOAT3& _P2, const XMFLOAT3& _P3)
//{
//	//	      P2 X----- ,--X P3
//	//				 ,'		
//	//				/			
//	//			  ,'				
//	//	  P0 X--�------X P1		
//	m_CurvePoints.vP0 = _P0;
//	m_CurvePoints.vP1 = _P1;
//	m_CurvePoints.vP2 = _P2;
//	m_CurvePoints.vP3 = _P3;
//}
////--------------------------------------------------------------------------------------------------------------
//void BezierCurve::Calculate(XMFLOAT3 & _Point, float _fArcT)
//{
//	// floats loose their f in this method for readabilities sake
//	XMVECTOR vP_0 = XMLoadFloat3(&m_CurvePoints.vP0);
//	XMVECTOR vP_1 = XMLoadFloat3(&m_CurvePoints.vP1);
//	XMVECTOR vP_2 = XMLoadFloat3(&m_CurvePoints.vP2);
//	XMVECTOR vP_3 = XMLoadFloat3(&m_CurvePoints.vP3);
//
//	// http://devmag.org.za/2011/04/05/bzier-curves-a-tutorial/
//	float fT = _fArcT;
//	float fU = 1.0f - fT;
//	float fTT = fT * fT;
//	float fUU = fU * fU;
//
//	// cubic bezier curve:
//	//	    vP_2 X----- ,--X vP_3	|		P_2------P_3
//	//				 ,'				|		  \
//		//				/				|		   \
//	//			  ,'				|		    \
//	//	vP_0 X--�------X vP_1		|	 P_0----P_1
//
//// P = (1-t)^3 * P_0 + 
////	 + 3 * (1-t)^2 * t * P_1 +
////	 + 3 * (1-t) * t^2 * P_2 +
////	 + t^3 * P_3
//	XMVECTOR vP = XMVectorScale(vP_0, fUU * fU);
//	vP += XMVectorScale(vP_1, 3 * fUU * fT);
//	vP += XMVectorScale(vP_2, 3 * fU * fTT);
//	vP += XMVectorScale(vP_3, fTT * fT);
//
//	XMStoreFloat3(&_Point, vP);
//}