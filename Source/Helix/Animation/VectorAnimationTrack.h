//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

/////////////////////////////////////////////////////////////////////////////////////////////////
// VectorAnimationTrack
//-----------------------------------------------------------------------------------------------
// Summary:
//
// A Vector animation track can be used to animate a XMFLOAT3 value, such as e.G. a postition.
//-----------------------------------------------------------------------------------------------
// Future Work:
//
//	- Path Following
//	- Continuous Speed between keyframes
//	- Time Distance function class
//	- Lookup table for interpolation method
//	! Frenet Frame Orientation
//-----------------------------------------------------------------------------------------------
// TODO:
//	- Test following example
//-----------------------------------------------------------------------------------------------
// Example:
//
// VectorAnimationTrack CameraTrack;
// CameraTrack.AddKeyFrame(0.0f, XMFLOAT3(0.0f, 0.0f, 0.0f))
// CameraTrack.AddKeyFrame(2.0f, XMFLOAT3(0.0f, 0.0f, 0.0f)); 
// CameraTrack.AddKeyFrame(10.0f, {XMFLOAT3(5.0f, 0.0f, 0.0f), XMFLOAT3(5.0f, 0.0f, 0.0f)});
// CameraTrack.BuildTrack();
// ...
// CameraTrack.Start();
// ...
// CameraTrack.Update(_fDeltaTime);
// ...
// Camera.Position = CameraTrack.GetData();
//-----------------------------------------------------------------------------------------------
// References:
//
// 1) Computer Animation: Algorithms and Techniques, by Rick Parent, ISBN:978-0124158429
/////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef VECTORANIMATIONTRACK_H
#define VECTORANIMATIONTRACK_H
#include "AnimationTrack.h"
#include "CurveMath.h"

namespace Helix
{
	namespace Animation
	{
		class VectorAnimationTrack : public AnimationTrack<XMFLOAT3>
		{
			HDEBUGNAME("Vector3AnimationTrack");
			struct VectorSegment
			{
				float fStartTime;
				float fEndTime;
			};

		public:
			XMFLOAT3 GetData() const;
			XMFLOAT3 GetPathDirection();
			XMFLOAT3 GetPathLookAt();

			void BuildTrack();

		protected:
			const VectorSegment& FindSegment(const float _fTime) const;
			XMFLOAT3 InterpolateLinear(const XMFLOAT3& _vStart, const XMFLOAT3& _vEnd, const float _fInterpolant);

			std::vector<VectorSegment> m_SegmentList;
			
			BezierPath m_Path;
			TimeDistance m_TimeToDistance;
			
			float m_Smoothness;
		};
	}
}

#endif