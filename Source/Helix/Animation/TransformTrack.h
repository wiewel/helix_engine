//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef TRANSFORMTRACK_H
#define TRANSFORMTRACK_H

#include "ITrack.h"
#include "ValueInterpolationTrack.h"
#include "Math\MathTypes.h"

#include <map>

namespace Helix
{
	namespace Animation
	{
		class TransformTrack : public ITrack
		{
		public:
			TransformTrack();
			~TransformTrack();

			// allow easy to use transform keyframing
			Math::transform& GetTransform();
			void SetTransform(const Math::transform& _Transform);
			__declspec(property(get = GetTransform, put = SetTransform))  Math::transform Transform;

			// create a keyframe from the member 'Transform', at the given time
			virtual void Keyframe(double _fTime) final;

			// update time (this is done by the sequence)
			virtual void SetTime(double _fTime) final;

		private:
			Math::transform m_Transform;
			
			ValueInterpolationTrack<float> m_PositionX;
			ValueInterpolationTrack<float> m_PositionY;
			ValueInterpolationTrack<float> m_PositionZ;
		};

		//---------------------------------------------------------------------------------------------------
		inline Math::transform& TransformTrack::GetTransform() 
		{ 
			m_Transform.p = Math::float3(m_PositionX.Value, m_PositionY.Value, m_PositionZ.Value);
			
			return m_Transform;
		}
		//---------------------------------------------------------------------------------------------------
		inline void TransformTrack::SetTransform(const Math::transform& _Transform) 
		{ 
			m_Transform = _Transform;
		}
	}
}
#endif // !TRANSFORMTRACK_H
