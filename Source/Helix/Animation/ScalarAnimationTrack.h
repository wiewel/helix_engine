//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

/////////////////////////////////////////////////////////////////////////////////////////////////
// ScalarAnimationTrack
//-----------------------------------------------------------------------------------------------
// Summary:
// A Scalar animation track can be used to animate a single float value, such as a brightness,
// opacity etc.
//-----------------------------------------------------------------------------------------------
// Future Work:
//	- Smooth interpolation methods
//-----------------------------------------------------------------------------------------------
// Example:
// ScalarAnimationTrack BrightnessTrack;
// BrightnessTrack.AddKeyFrame<float>(0.0f, 0.0f);
// BrightnessTrack.AddKeyFrame<float>(5.0f, 1.5f);
// BrightnessTrack.BuildTrack();
// ...
// BrightnessTrack.Start();
// ...
// BrightnessTrack.Update(_fDeltaTime);
// ...
// DirectionalLight.Brightness = BrightnessTrack.GetData();
//-----------------------------------------------------------------------------------------------
// References:
// 1) Computer Animation: Algorithms and Techniques, by Rick Parent, ISBN:978-0124158429
/////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef SCALARANIMATIONTRACK_H
#define SCALARANIMATIONTRACK_H
#include "AnimationTrack.h"
#include "LookupTable.h"

namespace Helix
{
	namespace Animation
	{
		class ScalarAnimationTrack : public AnimationTrack<float>
		{
			HDEBUGNAME("ScalarAnimationTrack");
			struct ScalarSegment
			{
				float fStartTime;
				float fEndTime;

				float fStartData;
				float fEndData;

				float fEaseIn;
				float fEaseOut;
			};

		public:
			float GetData() const;
			float GetUnsmoothedData() const;

			void BuildTrack();

		protected:
			const ScalarSegment& FindSegment(const float _fTime) const;
			float InterpolateLinear(const float _fStart, const float _fEnd, const float _fInterpolant) const;
			//float InterpolateSigmoidal(const float _fStart, const float _fEnd, const float _fInterpolant, const float _fSmoothness = 0.3) const;
			std::map<float, ScalarSegment> m_SegmentList;
		};
	}
}

#endif