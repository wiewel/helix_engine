//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef INTERPOLATIONTRACK_H
#define INTERPOLATIONTRACK_H

#include "hlx\src\Logger.h"
#include "ITrack.h"
#include <map>

namespace Helix
{
	namespace Animation
	{
		template<typename T>
		class ValueInterpolationTrack : public ITrack
		{
		//---------------------------------------------------------------------------------------------------
		public:
			// to allow cubic interpolation, also store tangent information in each keyframe
			struct TangentKeyframe
			{
				T value;
				T leftTangentValue;
				T rightTangentValue;
			};
		//---------------------------------------------------------------------------------------------------
		public:
			ValueInterpolationTrack();
			~ValueInterpolationTrack();

			// allow easy to use transform keyframing
			T& GetValue();
			void SetValue(const T& _Transform);
			__declspec(property(get = GetValue, put = SetValue))  T Value;

			// create a keyframe from the member 'Value', at the given time
			virtual void Keyframe(double _fTime) final;
			
			// update time (this is done by the sequence)
			virtual void SetTime(double _fTime) final;

		private:
			T EvaluateBezier(double _fParameter, const TangentKeyframe& _Left, const TangentKeyframe& _Right) const;

		//---------------------------------------------------------------------------------------------------
		private:
			std::map<double, TangentKeyframe> m_Keyframes;
			T m_Value;
			const double m_fEaseInOut = 0.1f;
		};
		//---------------------------------------------------------------------------------------------------
		template<typename T>
		inline T& ValueInterpolationTrack<T>::GetValue()
		{ 
			auto Right = m_Keyframes.upper_bound(m_fTime);
			auto Left = Right;
			// find the keyframes left and right to the current time
			if (Right == m_Keyframes.begin())
			{
				m_Value = Right->second.value;
				return m_Value;
			}
			else
			{
				--Left;
			}
			if (Right == m_Keyframes.end())
			{
				m_Value = Left->second.value;
				return m_Value;
			}
			double fParameter = (m_fTime - Left->first) / (Right->first - Left->first);
			
			// evaluate the interpolation function
			m_Value = EvaluateBezier(fParameter, Left->second, Right->second);
			return m_Value;
		}
		//---------------------------------------------------------------------------------------------------
		template<typename T>
		inline void ValueInterpolationTrack<T>::SetValue(const T& _Value)
		{ 
			m_Value = _Value;
		}
		//---------------------------------------------------------------------------------------------------
		template<typename T>
		inline ValueInterpolationTrack<T>::ValueInterpolationTrack()
		{
		}
		//---------------------------------------------------------------------------------------------------
		template<typename T>
		inline ValueInterpolationTrack<T>::~ValueInterpolationTrack()
		{
		}
		//---------------------------------------------------------------------------------------------------
		template<typename T>
		inline void ValueInterpolationTrack<T>::Keyframe(double _fTime)
		{
			TangentKeyframe Keyframe;
			Keyframe.value = m_Value;
			Keyframe.leftTangentValue = m_Value;
			Keyframe.rightTangentValue = m_Value;

			m_Keyframes.emplace(_fTime, Keyframe);
			m_fTime = _fTime;
		}
		//---------------------------------------------------------------------------------------------------
		template<typename T>
		inline void ValueInterpolationTrack<T>::SetTime(double _fTime)
		{
			m_fTime = _fTime;
		}
		//---------------------------------------------------------------------------------------------------
		template<typename T>
		inline T ValueInterpolationTrack<T>::EvaluateBezier(double _fParameter, const TangentKeyframe& _Left, const TangentKeyframe& _Right) const
		{
			double fT = _fParameter;
			double fU = 1.0 - _fParameter;
			double fTT = fT * fT;
			double fUU = fU * fU;

			// cubic bezier curve:
			//	    vP_2 X----- ,--X vP_3	|		P_2------P_3
			//				 ,'				|		  \
			//				/				|		   \
			//			  ,'				|		    \
			//	vP_0 X--�------X vP_1		|	 P_0----P_1

			// B(t) = (1-t)^3 * P_0 + 
			//	 + 3 * (1-t)^2 * t * P_1 +
			//	 + 3 * (1-t) * t^2 * P_2 +
			//	 + t^3 * P_3
			return static_cast<T>(fUU * fU * _Left.value
						+ 3.0 * fUU * fT * _Left.rightTangentValue
						+ 3.0 * fU * fTT * _Right.leftTangentValue
						+ fTT * fT * _Right.value);
		}
	}
}

#endif