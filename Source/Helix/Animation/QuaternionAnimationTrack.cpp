//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "QuaternionAnimationTrack.h"
#include "hlx\src\StandardDefines.h"

using namespace Helix::Animation;

XMFLOAT4 QuaternionAnimationTrack::GetData() const
{
	QuaternionSegment Segment = FindSegment(m_fAnimationTime);
	float fInterpolant = (m_fAnimationTime - Segment.fStartTime) / (Segment.fEndTime - Segment.fStartTime);
	fInterpolant = TimeDistanceSigmoidal(fInterpolant, Segment.fEaseIn, Segment.fEaseOut);

	return InterpolateLinear(Segment.fStartData, Segment.fEndData, fInterpolant);
}

void QuaternionAnimationTrack::BuildTrack()
{
	HASSERTD(m_KeyFrameList.size() >= 2, "There must be at least two keyframes in a track.");

	float fStartTime = 0.0f;
	HFOREACH_CONST(KeyFrameItr, KeyFrameEnd, m_KeyFrameList)

		QuaternionSegment Segment;
		Segment.fStartTime = fStartTime;
		Segment.fEndTime = KeyFrameItr->fTime;
		Segment.fEaseIn = 0.0f;
		Segment.fEaseOut = 0.0f;

	m_SegmentList.emplace(Segment.fEndTime, Segment);

	HFOREACH_CONST_END

	m_KeyFrameList.clear();
}

const QuaternionAnimationTrack::QuaternionSegment & QuaternionAnimationTrack::FindSegment(const float _fTime) const
{
	return m_SegmentList.lower_bound(_fTime)->second;
}

const XMFLOAT4 QuaternionAnimationTrack::InterpolateLinear(const XMFLOAT4 & _vStart, const XMFLOAT4 & _vEnd, const float _fInterpolant) const
{
	XMVECTOR vStart = XMLoadFloat4(&_vStart);
	XMVECTOR vEnd = XMLoadFloat4(&_vEnd);

	return Math::XMFloat4Get( XMQuaternionSlerp(vStart, vEnd, _fInterpolant));
}
