//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef TRACK_H
#define TRACK_H

#include <vector>
#include "hlx\src\Logger.h"
#include <algorithm>

namespace Helix
{
	namespace Animation
	{
		class ITrack
		{
		//---------------------------------------------------------------------------------------------------
		public:
			// set time at which the property value of the track will be read. usually done by sequence
			virtual void SetTime(double _fTime) = 0;

			// a label discribing the value on the track
			std::string GetLabel();
			void SetLabel(std::string _label);

			const double& GetEndTime() const;
			void MakeKeyframe(double _fTime);

		protected:
			// create a keyframe from the member transform, at the given time. must be called by derived class
			virtual void Keyframe(double _fTime) {};
		//---------------------------------------------------------------------------------------------------
		protected:
			// some unique name
			std::string m_sLabel;
			// playtime of the track
			double m_fTime = 0.0;
			double m_fEndTime = 0.0;
		};
		inline void ITrack::MakeKeyframe(double _fTime)
		{ 
			m_fEndTime = std::max(m_fEndTime, _fTime); 
			Keyframe(_fTime);
		}
		//---------------------------------------------------------------------------------------------------
		inline std::string ITrack::GetLabel() { return m_sLabel; }
		//---------------------------------------------------------------------------------------------------
		inline void ITrack::SetLabel(std::string _label) { m_sLabel = _label; }
		inline const double & ITrack::GetEndTime() const { return m_fEndTime; }
	}
}

#endif // !TRACK_H