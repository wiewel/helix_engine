//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "AnimationController.h"

using namespace Helix::Animation;
using namespace Helix::Math;
//---------------------------------------------------------------------------------------------------
AnimationController::AnimationController()
{
}
//---------------------------------------------------------------------------------------------------
AnimationController::~AnimationController()
{

}
//---------------------------------------------------------------------------------------------------
// if blending track is not set, the master channel will be computed without blending
TVectorTrackFader* AnimationController::AddVectorTrack()
{
	TVectorTrackFader Fader;

	m_VectorTracks.push_back(std::move(Fader));

	return &m_VectorTracks.back();
}
//---------------------------------------------------------------------------------------------------
// if blending track is not set, the master channel will be computed without blending
TQuaternionTrackFader* AnimationController::AddQuaternionTrack()
{
	TQuaternionTrackFader Fader;

	m_QuaternionTracks.push_back(std::move(Fader));

	return &m_QuaternionTracks.back();
}
//---------------------------------------------------------------------------------------------------
XMFLOAT3 AnimationController::GetVectorMaster()
{
	XMVECTOR vResult = XMVectorZero();
	HFOREACH_CONST(it, end, m_VectorTracks)
		if (it->m_Blending.IsEmpty())
		{
			vResult += XMLoadFloat3(&it->m_Track.GetData());
		}
		else
		{
			vResult += XMVectorScale(XMLoadFloat3(&it->m_Track.GetData()), it->m_Blending.GetData());
		}
	HFOREACH_CONST_END

	return XMFloat3Get(vResult);
}
//---------------------------------------------------------------------------------------------------
XMFLOAT3 AnimationController::GetVectorChannel(uint32_t _uChannelNum)
{
	HASSERTD(_uChannelNum < m_VectorTracks.size(), "invalid channel number");

	return m_VectorTracks.at(_uChannelNum).m_Track.GetData();
}
//---------------------------------------------------------------------------------------------------
XMFLOAT4 AnimationController::GetQuaternionMaster()
{
	XMVECTOR vResult = XMVectorZero();
	HFOREACH_CONST(it, end, m_QuaternionTracks)
		if (it->m_Blending.IsEmpty())
		{
			vResult += XMLoadFloat4(&it->m_Track.GetData());
		}
		else
		{
			vResult += XMVectorScale(XMLoadFloat4(&it->m_Track.GetData()), it->m_Blending.GetData());
		}
	HFOREACH_CONST_END

	return XMFloat4Get(vResult);
}
//---------------------------------------------------------------------------------------------------
XMFLOAT4 AnimationController::GetQuaternionChannel(uint32_t _uChannelNum)
{
	HASSERTD(_uChannelNum < m_VectorTracks.size(), "invalid channel number");

	return m_QuaternionTracks.at(_uChannelNum).m_Track.GetData();
}

void AnimationController::Update(float _fDeltaMs)
{
	HFOREACH_CONST(it, end, m_VectorTracks)
		it->m_Blending.Update(_fDeltaMs);
		it->m_Track.Update(_fDeltaMs);
	HFOREACH_CONST_END

	HFOREACH_CONST(it, end, m_QuaternionTracks)
		it->m_Blending.Update(_fDeltaMs);
		it->m_Track.Update(_fDeltaMs);
	HFOREACH_CONST_END
}
