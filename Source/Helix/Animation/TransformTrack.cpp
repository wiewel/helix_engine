//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "TransformTrack.h"

using namespace Helix::Animation;
//---------------------------------------------------------------------------------------------------
TransformTrack::TransformTrack()
{
}
//---------------------------------------------------------------------------------------------------
TransformTrack::~TransformTrack()
{
}
//---------------------------------------------------------------------------------------------------
void TransformTrack::Keyframe(double _fTime)
{
	m_PositionX.Value = m_Transform.p.x;
	m_PositionY.Value = m_Transform.p.y;
	m_PositionZ.Value = m_Transform.p.z;

	m_PositionX.Keyframe(_fTime);
	m_PositionY.Keyframe(_fTime);
	m_PositionZ.Keyframe(_fTime);
}
//---------------------------------------------------------------------------------------------------

void TransformTrack::SetTime(double _fTime)
{
	m_PositionX.SetTime(_fTime);
	m_PositionY.SetTime(_fTime);
	m_PositionZ.SetTime(_fTime);
}
