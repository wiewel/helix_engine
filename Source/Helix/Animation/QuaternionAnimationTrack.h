//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef QUATERNIONANIMATIONTRACK_H
#define QUATERNIONANIMATIONTRACK_H
#include "AnimationTrack.h"
#include <map>

namespace Helix
{
	namespace Animation
	{
		class QuaternionAnimationTrack : public AnimationTrack<XMFLOAT4>
		{
			struct QuaternionSegment
			{
				float fStartTime;
				float fEndTime;

				XMFLOAT4 fStartData;
				XMFLOAT4 fEndData;

				float fEaseIn;
				float fEaseOut;
			};
		public:
			XMFLOAT4 GetData() const;
			void BuildTrack();
		private:
			const QuaternionSegment& FindSegment(const float _fTime) const;

			const XMFLOAT4 InterpolateLinear(const XMFLOAT4& _vStart, const XMFLOAT4& _vEnd, const float _fInterpolant) const;

			std::map<float, QuaternionSegment> m_SegmentList;
		};
	}
}

#endif