//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef LOOKUPTABLE_H
#define LOOKUPTABLE_H

#include <vector>
#include <map>
#include "hlx\src\Logger.h"

namespace Helix
{
	namespace Animation
	{	
		///////////////////////////////////////////////////////////////////
		// Uniform Lookup
		//-----------------------------------------------------------------
		// Summary:
		// Use this sort of lookup table for uniformly distributed samples.
		//-----------------------------------------------------------------
		// Types:
		//	- Key: 
		//		Key operator*(Key)
		//		Key operator-(Key)
		//		Key operator/(Key)
		//		Value operator*(Value)
		//		bool operator<(Key)
		//
		//	- Value:
		//		Value operator-(Value)
		//		Value operator+(Value)
		///////////////////////////////////////////////////////////////////
		template<typename Key, typename Value>
		class UniformLookup
		{
			typedef std::vector<Value> Table;

		public:
			UniformLookup(Key _Start, Key _EntrySize)
			{
				m_Start = _Start;
				m_End = _Start;
				m_EntrySize = _EntrySize;
			}

			void push_back(Value _Value)
			{
				m_LookupTable.push_back(_Value);
				m_End += m_EntrySize;
			}

			inline Value at(Key _Key)
			{
				size_t index = (_Key - m_Start) / m_EntrySize;
				return m_LookupTable.at(index);
			}

			Value operator[] (Key _Key)
			{
				return at(_Key);
			}

			///<returns>An interpolated value between the nearest entries to _Key</returns>
			inline Value smooth_at(Key _Key)
			{
				Table::iterator UpperEntry = m_LookupTable.lower_bound(_Key);
				if (UpperEntry == m_LookupTable.cbegin())
				{
					return Upper->second;
				}
				if (UpperEntry == m_LookupTable.end())
				{
					return --UpperEntry;
				}

				Table::iterator LowerEntry = UpperEntry;
				--LowerEntry;

				const Key T = (_Key - LowerEntry->first) / (UpperEntry->first - LowerEntry->first);
				return LowerEntry->second + T * (UpperEntry->second - LowerEntry->second);
			}

		private:
			
			Table m_LookupTable;
			Key m_End;
			Key m_Start;
			Key m_EntrySize;
		};

		///////////////////////////////////////////////////////////////////
		// Non Uniform Lookup
		//-----------------------------------------------------------------
		// Summary:
		// Use this sort of lookup table for non uniformly distributed 
		// samples.
		//-----------------------------------------------------------------
		// Types:
		//	- Key: 
		//		Key operator*(Key)
		//		Key operator-(Key)
		//		Key operator/(Key)
		//		Value operator*(Value)
		//		bool operator<(Key)
		//
		//	- Value:
		//		Value operator-(Value)
		//		Value operator+(Value)
		///////////////////////////////////////////////////////////////////
		template<typename Key, typename Value>
		class NonUniformLookup
		{
			typedef std::map<Key, Value> Table;
		
		public:
			void emplace(const Key _Key, const Value _Value)
			{
				m_LookupTable.emplace(Key, Value);
				
			}

			inline Value at(Key _Key) const
			{
				return m_LookupTable.lower_bound(_Key)->second;
			}

			inline Value operator[] (Key _Key) const
			{
				return m_LookupTable.lower_bound(_Key)->second;
			}

			///<returns>An interpolated value between the nearest entries to _Key</returns>
			inline Value smooth_at(Key _Key) const
			{
				Table::iterator UpperEntry = m_LookupTable.lower_bound(_Key);
				if (UpperEntry == m_LookupTable.cbegin())
				{
					return Upper->second;
				}
				if (UpperEntry == m_LookupTable.end())
				{
					return --UpperEntry;
				}

				Table::iterator LowerEntry = UpperEntry;
				--LowerEntry;
				
				const Key T = (_Key - LowerEntry->first) / (UpperEntry->first - LowerEntry->first);
				return LowerEntry->second + T * (UpperEntry->second - LowerEntry->second);
			}

		private:
			Table m_LookupTable;
		};

		///////////////////////////////////////////////////////////////////
		// Indexed Non Uniform Lookup
		//-----------------------------------------------------------------
		// Summary:
		// Use this insead of a Non Uniform Lookup in case you also need
		// an index for each entry.
		//-----------------------------------------------------------------
		// Types:
		//	- Key: 
		//		Key operator*(Key)
		//		Key operator-(Key)
		//		Key operator/(Key)
		//		Value operator*(Value)
		//		bool operator<(Key)
		//
		//	- Value:
		//		Value operator-(Value)
		//		Value operator+(Value)
		///////////////////////////////////////////////////////////////////
		template<typename Key, typename Index, typename Value>
		class IndexedNonUniformLookup
		{
		public:
			HDEBUGNAME("IndexedNonUniformLookup");

			void Insert(const Key _Key, const Index _Index, const Value _Value)
			{
				std::pair<Index, Value> Entry(_Index, _Value);
				m_Table.emplace(_Key, Entry);
			}

			///<returns>A interpolated value between the two nearest entries in the table</returns>
			std::pair<Index, Value> At(const Key _Key) const
			{
				HASSERTD(!m_Table.empty(), "Table is empty");

				auto Upper = m_Table.lower_bound(_Key);
				auto Lower = Upper;
				if (Upper->first <= 0.0f)
				{
					return Upper->second;
				}
				--Lower;

				if (Upper->second.first != Lower->second.first)
				{
					const Key T = (_Key - Lower->first) / (Upper->first - Lower->first);
					std::pair<Index, Value> result(Upper->second.first, T * Upper->second.second);
					return result;
				}

				const Key T = (_Key - Lower->first) / (Upper->first - Lower->first);
				std::pair<Index, Value> result(Lower->second.first, Lower->second.second + T * (Upper->second.second - Lower->second.second));
				return result;
			}

		private:

			std::map<Key, std::pair<Index, Value>> m_Table;
		};
	}
}

#endif