//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "VectorAnimationTrack.h"
#include "hlx\src\StandardDefines.h"

using namespace Helix::Animation;
//---------------------------------------------------------------------------------------------------
XMFLOAT3 VectorAnimationTrack::GetData() const
{
	float fDistance = m_TimeToDistance.GetDistance(m_fAnimationTime);
	return m_Path.GetPosition(fDistance);
}
//---------------------------------------------------------------------------------------------------
XMFLOAT3 Helix::Animation::VectorAnimationTrack::GetPathDirection()
{
	HASSERTD(m_Path.GetTotalLength() > 0.0f, "Tried to get PATH orientation on point track. This is not possible.");

	float fHalfTime = m_fStartTime + 0.5f * (m_fEndTime - m_fStartTime);
	if (m_fAnimationTime <= fHalfTime)
	{
		float fDistance = m_TimeToDistance.GetDistance(m_fAnimationTime);
		XMVECTOR vOrientation = XMVectorSubtract(XMLoadFloat3(&m_Path.GetPosition(fDistance + 0.01f)), XMLoadFloat3(&m_Path.GetPosition(fDistance)));
		return Math::XMFloat3Get(vOrientation);
	}
	
	if (m_fAnimationTime > fHalfTime)
	{
		float fDistance = m_TimeToDistance.GetDistance(m_fAnimationTime);
		XMVECTOR vOrientation = XMVectorSubtract(XMLoadFloat3(&m_Path.GetPosition(fDistance)), XMLoadFloat3(&m_Path.GetPosition(fDistance - 0.01f)));
		return Math::XMFloat3Get(vOrientation);
	}

	return XMFLOAT3_ZERO;
}
//---------------------------------------------------------------------------------------------------
XMFLOAT3 Helix::Animation::VectorAnimationTrack::GetPathLookAt()
{
	HASSERTD(m_Path.GetTotalLength() > 0.0f, "Tried to get PATH look at on point track. This is not possible.");

	float fHalfTime = m_fStartTime + 0.5f * (m_fEndTime - m_fStartTime);
	if (m_fAnimationTime <= fHalfTime)
	{
		float fDistance = m_TimeToDistance.GetDistance(m_fAnimationTime);
		XMVECTOR vOrientation = XMLoadFloat3(&m_Path.GetPosition(fDistance + 0.01f));
		return Math::XMFloat3Get(vOrientation);
	}

	if (m_fAnimationTime > fHalfTime)
	{
		float fDistance = m_TimeToDistance.GetDistance(m_fAnimationTime);
		XMVECTOR vOrientation = XMVectorNegate(XMLoadFloat3(&m_Path.GetPosition(fDistance - 0.01f)));
		return Math::XMFloat3Get(vOrientation);
	}

	return XMFLOAT3_ZERO;
}
//---------------------------------------------------------------------------------------------------
void VectorAnimationTrack::BuildTrack()
{
	float fTotalLength = 0.0f;
	{
		float fStartTime = 0.0f;
		VectorSegment Segment;

		m_Path = BezierPath(m_Origin);
		
		std::vector<KeyFrame<XMFLOAT3>>::iterator PrevKeyFrame = m_KeyFrameList.begin();
		HFOREACH_CONST(it, end, PrevKeyFrame->Data)
			m_Path.AddPoint(*it);
		HFOREACH_CONST_END
		
		Segment.fStartTime = fStartTime;
		Segment.fEndTime = PrevKeyFrame->fTime;
		fStartTime = Segment.fEndTime;

		m_SegmentList.push_back(Segment);
		std::vector<KeyFrame<XMFLOAT3>>::iterator CurrentKeyFrame = PrevKeyFrame + 1;
		
		for (; CurrentKeyFrame != m_KeyFrameList.cend(); ++CurrentKeyFrame)
		{
			HFOREACH_CONST(it, end, CurrentKeyFrame->Data)
				m_Path.AddPoint(*it);
			HFOREACH_CONST_END


			m_TimeToDistance.AddKeyTime(PrevKeyFrame->fTime, m_Path.GetTotalLength());
			
			Segment.fStartTime = fStartTime;
			Segment.fEndTime = CurrentKeyFrame->fTime;
			fStartTime = Segment.fEndTime;

			m_SegmentList.push_back(Segment);
			PrevKeyFrame = CurrentKeyFrame;
		}

		m_Path.Finalize();
		m_TimeToDistance.AddKeyTime(fStartTime, m_Path.GetTotalLength());
	}

	m_KeyFrameList.clear();
}

//---------------------------------------------------------------------------------------------------
const VectorAnimationTrack::VectorSegment& VectorAnimationTrack::FindSegment(const float _fTime) const
{
	HFOREACH_CONST(it, end, m_SegmentList)
		if (it->fStartTime < _fTime && it->fEndTime > _fTime)
		{
			return *it;
		}
	HFOREACH_CONST_END
	return m_SegmentList.front();
}
//---------------------------------------------------------------------------------------------------
XMFLOAT3 VectorAnimationTrack::InterpolateLinear(const XMFLOAT3& _vStart, const XMFLOAT3& _vEnd, const float _fInterpolant)
{
	XMVECTOR vStart = XMLoadFloat3(&_vStart);
	XMVECTOR vEnd = XMLoadFloat3(&_vEnd);
	return Math::XMFloat3Get(XMVectorLerp(vStart, vEnd, _fInterpolant));
}
//---------------------------------------------------------------------------------------------------