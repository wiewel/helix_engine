//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher, David Labode
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "MTLFile.h"
#include "Util\Logger.h"
#include "stdrzr\source\StringHelpers.h"
#include "stdrzr\source\Bytes.h"
#include "Resources\FileSystem.h"
#include "Math\MathTypes.h"
#include <sstream>

using namespace Helix::H3D;
using namespace Helix::Resources;

MTLFile::MTLFile(const stdrzr::string& _sFilePath) : m_sFilePath(_sFilePath)
{
	m_sFileName = Resources::FileSystem::GetFileName(_sFilePath);
}
//---------------------------------------------------------------------------------------------------------------------
bool MTLFile::Load(const std::string& _MTLContent) 
{
	// no data available
	if (_MTLContent.empty())
	{
		HERROR("No mtl data available!");
		return false;
	}

	std::stringstream MTLStream(_MTLContent);
	m_Materials.resize(0);

	int32_t uMaterialIndex = -1;
	uint32_t uLineNumber = 0;

	float fToken = 0.0f;
	uint32_t uToken = 0u;

	while (MTLStream.good()) 
	{
		std::string sToken;
		std::getline(MTLStream, sToken);

		// remove all comments
		size_t uComment = sToken.find("#", 0);
		if (uComment != std::string::npos)
		{
			sToken.erase(uComment, std::string::npos);
		}

		std::stringstream Line(sToken);
		Line >> sToken;

		if (sToken.empty())
		{
			continue;
		}

		if (sToken == "newmtl")// start of new material
		{
			Material NewMat = {};
			Line >> NewMat.sName;
			m_Materials.push_back(NewMat);
			++uMaterialIndex;
		}
		else if (sToken == "Kd")// color diffuse
		{
			Line >> m_Materials[uMaterialIndex].vColorAlbedo.x;
			Line >> m_Materials[uMaterialIndex].vColorAlbedo.y;
			Line >> m_Materials[uMaterialIndex].vColorAlbedo.z;
		}
		else if (sToken == "Ke")// color emissive
		{
			Line >> m_Materials[uMaterialIndex].vColorEmissive.x;
			Line >> m_Materials[uMaterialIndex].vColorEmissive.y;
			Line >> m_Materials[uMaterialIndex].vColorEmissive.z;
		}
		else if (sToken == "Tf")// color transmission filter
		{
			float fToken;
			Line >> fToken; // m_Materials[uMaterialIndex].vTransmissionFilter.x;
			Line >> fToken; // m_Materials[uMaterialIndex].vTransmissionFilter.y;
			Line >> fToken; // m_Materials[uMaterialIndex].vTransmissionFilter.z;
		}
		else if (sToken == "d")// material opaqueness
		{
			HWARNING("MTL file uses unsupported material opaqueness in material #%d", uMaterialIndex);
			Line >> fToken; //m_Materials[uMaterialIndex].fTransparency = 1.f - fToken;
		}
		else if (sToken == "Tr")// transparency
		{
			HWARNING("MTL file uses unsupported transparency in material #%d", uMaterialIndex);
			Line >> fToken; // m_Materials[uMaterialIndex].fTransparency;
		}
		else if (sToken == "Ni")// optical density
		{
			HWARNING("MTL file uses unsupported optical density in material #%d", uMaterialIndex);
			Line >> fToken; // m_Materials[uMaterialIndex].fOpticalDensity;
		}
		else if (sToken == "illum")// illumination model
		{
			HWARNING("MTL file uses unsupported illumination model in material #%d", uMaterialIndex);
			Line >> uToken; // m_Materials[uMaterialIndex].uIlluminationModel;
		}
		else if (sToken == "sharpness")// sharpness
		{
			HWARNING("MTL file uses unsupported sharpness in material #%d", uMaterialIndex);
			Line >> uToken; // m_Materials[uMaterialIndex].uSharpness;
		}
		else if (sToken == "refl")// reflection map
		{
			HWARNING("MTL file uses unsupported reflection map in material #%d", uMaterialIndex);
			continue;
		}
		else if (sToken == "map_Kd")// diffuse map
		{
			ReadMapData(Line, m_Materials[uMaterialIndex].MapAlbedo);
		}
		else if (sToken == "map_Ke")// emissive map
		{
			ReadMapData(Line, m_Materials[uMaterialIndex].MapEmissive);
		}
		else if (sToken == "disp")// displacement map
		{
			ReadMapData(Line, m_Materials[uMaterialIndex].MapHeight);
		}
		else if (sToken == "bump")// bump map
		{
			ReadMapData(Line, m_Materials[uMaterialIndex].MapNormal);
		}
	}

	return true;
}
//---------------------------------------------------------------------------------------------------------------------
void MTLFile::ReadMapData(std::stringstream& _Stream, _Out_ TextureMap& _CurMap)
{
	std::string sToken;
	while (_Stream.good())
	{
		_Stream >> sToken;
		if (sToken == "-s")// scale
		{
			_Stream >> _CurMap.vScale.x;
			_Stream >> _CurMap.vScale.y;
			_Stream >> _CurMap.vScale.z;
		}
		else if (sToken == "-o")// offset
		{
			_Stream >> _CurMap.vOffset.x;
			_Stream >> _CurMap.vOffset.y;
			_Stream >> _CurMap.vOffset.z;
		}
		//else if (sToken == "-t")// turbulence
		//{
		//	//_Stream >> _CurMap.vTurbulence.x;
		//	//_Stream >> _CurMap.vTurbulence.y;
		//	//_Stream >> _CurMap.vTurbulence.z;
		//}
		//else if (sToken == "-mm")// modify texture map
		//{
		//	//_Stream >> _CurMap.vModifier.x;
		//	//_Stream >> _CurMap.vModifier.y;
		//}
		//else if (sToken == "-bm")// bump multiplier
		//{
		//	//_Stream >> _CurMap.fBumpMultiplier;
		//}
		//else if (sToken == "-boost")// boost
		//{
		//	//_Stream >> _CurMap.fBoost;
		//}
		//else if (sToken == "-imfchan")// image channel
		//{
		//	_Stream >> sToken;
		//	if (sToken == "r")
		//	{
		//	//	_CurMap.kImageChannel = ImageChannel_r;
		//	}
		//}
		//else if (sToken == "-blendu")// blend u
		//{
		//	_Stream >> sToken;
		//	//_CurMap.bBlendU = sToken == "on";
		//}
		//else if (sToken == "-blendv")// blend v
		//{
		//	_Stream >> sToken;
		//	//_CurMap.bBlendV = sToken == "on";
		//}
		//else if (sToken == "clamp")// clamp 
		//{
		//	_Stream >> sToken;
		//	//_CurMap.bClamp = sToken == "on";
		//}
		else // last string in current line is always the path
		{			
			_CurMap.sMapPath = sToken;
			sToken = _Stream.str();
			size_t uOffset = _Stream.tellg();
			if (uOffset < sToken.size())
			{
				_CurMap.sMapPath += sToken.substr(uOffset);
			}
			stdrzr::remove(_CurMap.sMapPath, '\0');
			return;
		}
	}

	return; // empty line
}