//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher, David Labode
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "H3DSceneNode.h"
#include "DirectXMesh\DirectXMesh\DirectXMesh.h"
#include "ConvexMeshCooking.h"

#include "Math\MathFunctions.h"
#include "Resources\FileSystem.h"
#include "Util\CRC32.h"

using namespace Helix::H3D;
using namespace Helix;
//-----------------------------------------------------------------------------------------------

inline Math::XMFLOAT3 Fbx4ToHelix3(const FbxVector4& _Vec)
{
	return Math::XMFLOAT3(static_cast<float>(_Vec[0]), static_cast<float>(_Vec[1]), static_cast<float>(_Vec[2]));
}

inline Math::XMFLOAT3 Fbx3ToHelix3(const FbxDouble3& _Vec)
{
	return Math::XMFLOAT3(static_cast<float>(_Vec[0]), static_cast<float>(_Vec[1]), static_cast<float>(_Vec[2]));
}

inline Math::XMFLOAT2 Fbx2ToHelix2(const FbxDouble2& _Vec)
{
	return Math::XMFLOAT2(static_cast<float>(_Vec[0]), static_cast<float>(_Vec[1]));
}
//-----------------------------------------------------------------------------------------------

H3DSceneNode::H3DSceneNode(FbxNode* _pNode, H3DSceneNode* _pParent, bool _bRecursive) :
	m_pNode(_pNode),
	m_pParent(_pParent),
	m_sName(hlx::replace(std::string(_pNode->GetNameOnly()), ":", "_")),
	m_bRootNode(_pNode == _pNode->GetScene()->GetRootNode())
{
	FbxNodeAttribute* pAttribute = m_pNode->GetNodeAttribute();

	// default Shape Unit sphere
	m_QueryShape.kType = Physics::GeometryType_Sphere;
	m_QueryShape.Sphere.fRadius = 0.5f;

	m_SimulationShape.kType = Physics::GeometryType_Sphere;
	m_SimulationShape.Sphere.fRadius = 0.5f;

	if (pAttribute != nullptr)
	{
		m_kFbxType = pAttribute->GetAttributeType();

		switch (m_kFbxType)
		{
		case fbxsdk::FbxNodeAttribute::eMesh:
		case fbxsdk::FbxNodeAttribute::eNurbs:
		case fbxsdk::FbxNodeAttribute::ePatch:
		case fbxsdk::FbxNodeAttribute::eNurbsSurface:
			m_kNodeType = NodeType_Mesh;
			break;
		//case fbxsdk::FbxNodeAttribute::eCamera:
		//	m_kNodeType = NodeType_Camera;
		//	break;
		case fbxsdk::FbxNodeAttribute::eLight:
			m_kNodeType = NodeType_Light;
			break;
		default:
			m_kNodeType = NodeType_Unknown;
			break;
		}
	}

	if (m_bRootNode == false && (pAttribute == nullptr || m_kFbxType == FbxNodeAttribute::eUnknown))
	{
		HERROR("Unknown or invalid NodeAttribute!");
	}

	// get local transform
	{
		Math::float3 vRot = Fbx3ToHelix3(m_pNode->LclRotation);
		Math::float3 vPos = Fbx3ToHelix3(m_pNode->LclTranslation);
		Math::float3 vScale = Fbx3ToHelix3(m_pNode->LclScaling);

		// Pitch = y
		// Yaw = z
		// Roll = x
		// <Pitch, Yaw, Roll, 0>
		vPos.x = -vPos.x;
		vPos.z = -vPos.z;

		Math::XMVECTOR XMRot = Math::XMQuaternionRotationRollPitchYaw(Math::Deg2Rad(-vRot.x), Math::Deg2Rad(-vRot.y), Math::Deg2Rad(vRot.z));
		
		Math::XMStoreQuaternion(&(m_Transform.q), XMRot);
		m_Transform.p = vPos;

		m_vScale = vScale;
		HLOG("%s Pos [%f %f %f] Rot [%f %f %f] Scale [%f %f %f]", CSTR(m_sName),
			vPos.x, vPos.y, vPos.z,
			vRot.x, vRot.y, vRot.z,
			vScale.y, vScale.y, vScale.z);
	}

	if (_bRecursive)
	{
		int iChildCount = m_pNode->GetChildCount();

		m_ChildNodes.resize(iChildCount);
		for (int i = 0; i < iChildCount; ++i)
		{
			m_ChildNodes[i] = new H3DSceneNode(m_pNode->GetChild(i), this, true);
		}
	}	
}
//-----------------------------------------------------------------------------------------------

H3DSceneNode::~H3DSceneNode()
{
	HSAFE_VECTOR_DELETE(m_ChildNodes);
}
//-----------------------------------------------------------------------------------------------

bool H3DSceneNode::GetLightData()
{
	if (m_kFbxType != FbxNodeAttribute::eLight)
	{
		HERROR("Unknown or invalid NodeAttribute!");
		return false;
	}

	FbxLight* pLight = m_pNode->GetLight();

	if (pLight == nullptr)
	{
		return false;
	}

	m_Light.fDecayStart = static_cast<float>(pLight->DecayStart) / 100.f; // use this as light sphere radius

	// default to 1.f
	if (m_Light.fDecayStart == 0.f)
	{
		m_Light.fDecayStart = 1.f;
	}

	m_Light.fIntensity = static_cast<float>(pLight->Intensity) / 100.f; // Maya scale fuck

	// default to 1.f
	if (m_Light.fIntensity == 0.f)
	{
		m_Light.fIntensity = 1.f;
	}

	m_Light.fRange = 150.f;
	m_Light.vColor = Fbx3ToHelix3(pLight->Color);
	m_Light.vDirection = { 0.f, 0.f, 1.f }; // normal in Z-direction
	m_Light.vPosition = HFLOAT3_ZERO;

	std::string sType = "UNKNOWN";

	FbxLight::EType kType = pLight->LightType;
	switch (kType)
	{
	case FbxLight::EType::eDirectional:
		m_Light.kType = Scene::LightType_Directional;
		m_Light.fRange = 10000.f;
		sType = "DirectionalLight";
		break;
	case FbxLight::EType::ePoint:
		m_Light.kType = Scene::LightType_Point;
		sType = "PointLight";
		break;
	case FbxLight::EType::eSpot:
		m_Light.kType = Scene::LightType_Spot;
		m_Light.fSpotAngle = static_cast<float>(pLight->OuterAngle);
		sType = "SpotLight";
		break;
	default:
		HERROR("Unsupported light type");
		return false;
	}

	HLOGD("%s: %s Color %f %f %f", CSTR(m_sName), CSTR(sType), m_Light.vColor.x, m_Light.vColor.y, m_Light.vColor.z);
	HLOGD("%s: Intensity %f DecayStart %f Range %f", CSTR(m_sName), m_Light.fIntensity, m_Light.fDecayStart, m_Light.fRange);

	return true;
}
//-----------------------------------------------------------------------------------------------
inline float GetFactor(const FbxProperty& _Property, const std::string& _sPropName, float _fDefaultValue = 1.0f)
{
	const FbxProperty Property = _Property.Find(_sPropName.c_str());

	if (Property.IsValid())
	{
		return static_cast<float>(Property.Get<FbxDouble>());
	}

	return _fDefaultValue;
}
//-----------------------------------------------------------------------------------------------
inline float GetFactor(const FbxSurfaceMaterial* _pMaterial, const std::string& _sPropName, float _fDefaultValue = 1.0f)
{
	const FbxProperty Property = _pMaterial->FindProperty(_sPropName.c_str());

	if (Property.IsValid())
	{
		return static_cast<float>(Property.Get<FbxDouble>());
	}

	return _fDefaultValue;
}
//-----------------------------------------------------------------------------------------------
inline Math::float2 GetVector2(const FbxProperty& _Property, const std::string& _sPropName, const Math::float2& _vDefaultValue = HFLOAT2_ZERO, const std::string& _sPropFactorName = "")
{
	Math::float2 vVec2 = _vDefaultValue;

	const FbxProperty Property = _Property.Find(_sPropName.c_str());

	if (Property.IsValid())
	{
		vVec2 = Math::float2(Fbx2ToHelix2(Property.Get<FbxDouble2>()));
		if (_sPropFactorName.empty() == false)
		{
			vVec2 *= GetFactor(_Property, _sPropFactorName);
		}
	}

	return vVec2;
}
//-----------------------------------------------------------------------------------------------
inline Math::float2 GetVector2(const FbxSurfaceMaterial* _pMaterial, const std::string& _sPropName, const Math::float2& _vDefaultValue = HFLOAT2_ZERO, const std::string& _sPropFactorName = "")
{
	Math::float2 vVec2 = _vDefaultValue;

	const FbxProperty Property = _pMaterial->FindProperty(_sPropName.c_str());

	if (Property.IsValid())
	{
		vVec2 = Fbx2ToHelix2(Property.Get<FbxDouble2>());
		if (_sPropFactorName.empty() == false)
		{
			vVec2 *= GetFactor(_pMaterial, _sPropFactorName);
		}
	}

	return vVec2;
}
//-----------------------------------------------------------------------------------------------
inline Math::float3 GetColor(const FbxProperty& _Property, const std::string& _sPropName, const Math::float3& _vDefaultValue = HFLOAT3_ZERO, const std::string& _sPropFactorName = "")
{	
	Math::float3 vColor = _vDefaultValue;

	const FbxProperty Property = _Property.Find(_sPropName.c_str());

	if (Property.IsValid())
	{
		vColor = Fbx3ToHelix3(Property.Get<FbxDouble3>());
		if(_sPropFactorName.empty() == false)
		{
			vColor *= GetFactor(_Property, _sPropFactorName);
		}
	}

	return vColor;
}
//-----------------------------------------------------------------------------------------------
inline Math::float3 GetColor(const FbxSurfaceMaterial* _pMaterial, const std::string& _sPropName, const Math::float3& _vDefaultValue = HFLOAT3_ZERO, const std::string& _sPropFactorName = "")
{
	Math::float3 vColor = _vDefaultValue;

	const FbxProperty Property = _pMaterial->FindProperty(_sPropName.c_str());

	if (Property.IsValid())
	{
		vColor = Fbx3ToHelix3(Property.Get<FbxDouble3>());
		if (_sPropFactorName.empty() == false)
		{
			vColor *= GetFactor(_pMaterial, _sPropFactorName);
		}
	}

	return vColor;
}
//-----------------------------------------------------------------------------------------------
inline FbxFileTexture* GetTexture(const FbxProperty& _Property)
{
	if (_Property.IsValid() == false)
	{
		return nullptr;
	}

	int iTextureCount = _Property.GetSrcObjectCount<FbxFileTexture>();
	int iLayeredTextureCount = _Property.GetSrcObjectCount<FbxLayeredTexture>();

	FbxFileTexture* pTexture = nullptr;
	if (iTextureCount > 0)
	{
		pTexture = _Property.GetSrcObject<FbxFileTexture>(0);
	}
	else if (iLayeredTextureCount > 0)
	{
		FbxLayeredTexture* pLayeredTexture = _Property.GetSrcObject<FbxLayeredTexture>(0);

		if (pLayeredTexture->GetSrcObjectCount<FbxFileTexture>() > 0)
		{
			pTexture = pLayeredTexture->GetSrcObject<FbxFileTexture>(0);
		}
	}

	return pTexture;
}

//-----------------------------------------------------------------------------------------------

inline std::string GetTextureMap(const FbxProperty& _Property, Resources::HTextureMapProp& _TexMap)
{
	FbxFileTexture* pTexture = GetTexture(_Property);

	if (pTexture == nullptr)
	{
		return{};
	}

	_TexMap.vScale.xyz = Fbx3ToHelix3(pTexture->Scaling);
	std::string sFile = hlx::to_sstring(Resources::FileSystem::GetShortName(STR(pTexture->GetFileName())));
	_TexMap.bEnabled = sFile.empty() == false;

	return sFile;
};

//-----------------------------------------------------------------------------------------------

inline std::string GetTextureMap(const FbxSurfaceMaterial* _pMaterial, const char* _sPropName, Resources::HTextureMapProp& _TexMap)
{
	const FbxProperty Property = _pMaterial->FindProperty(_sPropName);

	if (Property.IsValid() == false)
	{
		return nullptr;
	}

	return GetTextureMap(Property, _TexMap);
}

//-----------------------------------------------------------------------------------------------
bool H3DSceneNode::LoadCustomMaterials(
	const FbxSurfaceMaterial* _pMaterial,
	Resources::HMat& _OutMat,
	Resources::HMatProps& _OutProps)
{
	FbxProperty Property = _pMaterial->FindProperty("Maya");
	if (Property.IsValid() == false)
	{
		return false;
	}

	FbxProperty TypeProp = Property.Find("TypeId");
	if (TypeProp.IsValid() == false)
	{
		return false;
	}

	const int32_t iTypeID = static_cast<int32_t>(TypeProp.Get<int>());


	switch (iTypeID)
	{
	case CustomMaterial_StingrayPBS: // StringrayPBS => P: "Maya|TypeId", "int", "Integer", "", 1166017
		HLOGD("%s: Maya StingrayPBS material found", hlx::to_string(m_sName).c_str());

		_OutMat.sMapAlbedo = GetTextureMap(Property.Find("TEX_color_map"), _OutProps.AlbedoMap);
		_OutProps.AlbedoMap.bEnabled &= Property.Find("use_color_map").Get<float>() != 0.0f;
		_OutProps.vColorAlbedo = GetColor(Property, "base_color");

		_OutMat.sMapNormal = GetTextureMap(Property.Find("TEX_normal_map"), _OutProps.NormalMap);
		_OutProps.NormalMap.bEnabled &= Property.Find("use_normal_map").Get<float>() != 0.0f;

		_OutMat.sMapMetallic = GetTextureMap(Property.Find("TEX_metallic_map"), _OutProps.MetallicMap);
		_OutProps.MetallicMap.bEnabled &= Property.Find("use_metallic_map").Get<float>() != 0.0f;
		_OutProps.fMetallic = GetFactor(Property, "metallic", 0.0f); // Property.Find("metallic").Get<float>();

		_OutMat.sMapRoughness = GetTextureMap(Property.Find("TEX_roughness_map"), _OutProps.RoughnessMap);
		_OutProps.RoughnessMap.bEnabled &= Property.Find("use_roughness_map").Get<float>() != 0.0f;
		_OutProps.fRoughness = GetFactor(Property, "roughness", 0.33f); // Property.Find("roughness").Get<float>();

		_OutMat.sMapEmissive = GetTextureMap(Property.Find("TEX_emissive_map"), _OutProps.EmissiveMap);
		_OutProps.EmissiveMap.bEnabled &= Property.Find("use_emissive_map").Get<float>() != 0.0f;
		_OutProps.EmissiveMap.vScale *= GetFactor(Property, "emissive_intensity", 1.0f); // Property.Find("emissive_intensity").Get<float>();
		_OutProps.vColorEmissive = GetColor(Property, "emissive", HFLOAT3_ZERO, "emissive_intensity");

		/// treat AO entries as heightmap entry
		_OutMat.sMapHeight = GetTextureMap(Property.Find("TEX_ao_map"), _OutProps.HeightMap);
		_OutProps.HeightMap.bEnabled &= Property.Find("use_ao_map").Get<float>() != 0.0f;

		_OutProps.vTextureTiling = GetVector2(Property, "uv_scale", HFLOAT2_ONE);

		return true;
		break;
	default:
		HWARNING("%s: Unsupported custom material %d found!", CSTR(m_sName), iTypeID);
		return false;
		break;
	}
}
//-----------------------------------------------------------------------------------------------
bool H3DSceneNode::GetMaterialData()
{
	int iMaterialCount = m_pNode->GetMaterialCount();
	if (iMaterialCount == 0)
	{
		HERROR("%s: Node has no material data!", CSTR(m_sName));
		return false;
	}
	// support only one material atm
	
	for (int i = 0; i < iMaterialCount; ++i)
	{
		FbxSurfaceMaterial* pMaterial = m_pNode->GetMaterial(i);

		if (pMaterial == nullptr)
		{
			return false;
		}

		Resources::HMat Material;
		Resources::HMatProps Props;

		Material.sName = std::string(pMaterial->GetName());
		hlx::replace(Material.sName, ":", "_");

		Props.sHMatName = Material.sName;
		Props.sSubMeshName = m_Mesh.sSubName;

		/// Check for custom shader materials supported by maya
		bool bFoundCustomMaterial = LoadCustomMaterials(pMaterial, Material, Props);

		if (bFoundCustomMaterial == false)
		{
			/// Default Material
			Props.vColorAlbedo = GetColor(pMaterial, FbxSurfaceMaterial::sDiffuse, HFLOAT3_ONE, FbxSurfaceMaterial::sDiffuseFactor);
			Props.vColorEmissive = GetColor(pMaterial, FbxSurfaceMaterial::sEmissive, HFLOAT3_ZERO, FbxSurfaceMaterial::sEmissiveFactor);

			Material.sMapAlbedo = GetTextureMap(pMaterial, FbxSurfaceMaterial::sDiffuse, Props.AlbedoMap);
			Props.AlbedoMap.vScale *= GetFactor(pMaterial, FbxSurfaceMaterial::sDiffuseFactor, 1.0f);

			Material.sMapEmissive = GetTextureMap(pMaterial, FbxSurfaceMaterial::sEmissive, Props.EmissiveMap);
			Props.EmissiveMap.vScale *= GetFactor(pMaterial, FbxSurfaceMaterial::sEmissiveFactor, 1.0f);

			Material.sMapNormal = GetTextureMap(pMaterial, FbxSurfaceMaterial::sNormalMap, Props.NormalMap);
		}

		m_Materials.push_back(Material);
		m_MaterialProperties.push_back(Props);
	}

	return true;
}
//-----------------------------------------------------------------------------------------------

bool H3DSceneNode::Triangulate(bool _bRecursive, bool _bReverseWinding, bool _bToLeftHanded, bool _bFlipV, bool _bReversePivot, bool _bNullPivot)
{
	if (_bRecursive)
	{
		std::vector<H3DSceneNode*> Nodes = { this };
		GetChildNodesOfType(Nodes, NodeType_Mesh, true);

		for (H3DSceneNode* pNode : Nodes)
		{
			pNode->Triangulate(false, _bReverseWinding, _bToLeftHanded, _bFlipV, _bReversePivot, _bNullPivot);
		}
	}

	if (m_bRootNode)
	{
		return true;
	}

	FbxMesh* pMesh = m_pNode->GetMesh();

	if (pMesh == nullptr)
	{
		//HERROR("%s: has no Mesh!", CSTR(m_sName));
		return false;
	}

	if (_bNullPivot)
	{
		FbxAMatrix PivotTransform;
		PivotTransform.SetIdentity();
		pMesh->SetPivot(PivotTransform);
		pMesh->ApplyPivot();
	}

	if (_bReversePivot)
	{
		//Math::float3 vPos(GetGlobalTransform().p);
		Math::float3 vPos(m_Transform.p);
		if (vPos.isZero() == false)
		{
			FbxAMatrix PivotTransform(FbxVector4(-vPos.x, vPos.y, -vPos.z, 0.0), FbxVector4(0.0, 0.0, 0.0, 0.0), FbxVector4(1.0, 1.0, 1.0, 1.0));
			pMesh->SetPivot(PivotTransform);
			pMesh->ApplyPivot();
		}
	}

	FbxNodeAttribute* pAttribute = m_pNode->GetNodeAttribute();
	
	HLOGD("%s: Triangulating node", CSTR(m_sName));
	// convert controllpoints to vertex data
	{
		FbxGeometryConverter GeoConverter(m_pNode->GetFbxManager());

		if (pAttribute == nullptr || m_kFbxType == FbxNodeAttribute::eUnknown)
		{
			HERROR("%s: Unknown or invalid NodeAttribute!", CSTR(m_sName));
			return false;
		}

		pAttribute = GeoConverter.Triangulate(pAttribute, true);
		if (pAttribute == nullptr)
		{
			HERROR("%s: Failed to triangulate!", CSTR(m_sName));
			return false;
		}
	}

	pMesh = m_pNode->GetMesh();
	if (pMesh == nullptr)
	{
		return false;
	}

	m_Mesh.sSubName = std::string(pMesh->GetNameOnly());
	hlx::replace(m_Mesh.sSubName, ":", "_");

	if (m_Mesh.sSubName.empty())
	{
		m_Mesh.sSubName = m_sName;
	}

	// extract vertex data
	int iPolyCount = pMesh->GetPolygonCount();

	m_Mesh.uPolyCount = iPolyCount;

	m_Mesh.Vertices.reserve(iPolyCount * 3u);
	m_Mesh.Normals.reserve(iPolyCount * 3u);

	bool bImportNormals = true;

	Math::float3 vAABBMax = { -FLT_MAX, -FLT_MAX ,-FLT_MAX };
	Math::float3 vAABBMin = { FLT_MAX, FLT_MAX ,FLT_MAX };

	HLOGD("%s: Extracting %d Polygons", CSTR(m_Mesh.sSubName), iPolyCount);
	for (int p = 0; p < iPolyCount; ++p)
	{
		int uPolySize = pMesh->GetPolygonSize(p);

		for (int pi = 0; pi < uPolySize; ++pi)
		{
			int iPosInPoly = (_bReverseWinding ? (uPolySize - 1) - pi : pi);
			
			int iContolPointIndex = pMesh->GetPolygonVertex(p, iPosInPoly);

			Math::XMFLOAT3 vPos = Fbx4ToHelix3(pMesh->GetControlPointAt(iContolPointIndex));

			if (_bToLeftHanded)
			{
				vPos.z = -vPos.z;
			}

			vAABBMax = Math::Max(vAABBMax, vPos);
			vAABBMin = Math::Min(vAABBMin, vPos);

			m_Mesh.Vertices.push_back(vPos);

			FbxVector4 vNormal;
			bImportNormals = bImportNormals && pMesh->GetPolygonVertexNormal(p, iPosInPoly, vNormal);

			if(bImportNormals)
			{
				Math::XMFLOAT3 XMNormal = Fbx4ToHelix3(vNormal);

				if (_bToLeftHanded)
				{
					XMNormal.z = -XMNormal.z;
				}

				m_Mesh.Normals.push_back(XMNormal);
			}
		}
	}

	// Generate AABB
	m_QueryShape.kType = Physics::GeometryType_Box;
	m_QueryShape.Box.vHalfExtents = Math::MinMaxToHalfExtents(vAABBMin, vAABBMax);

	// account for planar geometry
	if(m_QueryShape.Box.vHalfExtents.x == 0.f || m_QueryShape.Box.vHalfExtents.x == -0.f)
	{
		m_QueryShape.Box.vHalfExtents.x = 0.1f;
	}
	if (m_QueryShape.Box.vHalfExtents.y == 0.f || m_QueryShape.Box.vHalfExtents.y == -0.f)
	{
		m_QueryShape.Box.vHalfExtents.y = 0.1f;
	}
	if (m_QueryShape.Box.vHalfExtents.z == 0.f || m_QueryShape.Box.vHalfExtents.z == -0.f)
	{
		m_QueryShape.Box.vHalfExtents.z = 0.1f;
	}

	// default simulation shape if no convex mesh will be created later
	m_SimulationShape = m_QueryShape;

	HLOGD("%s: Extracted %d Vertices and Normals", CSTR(m_Mesh.sSubName), static_cast<int>(m_Mesh.Vertices.size()));
	
	// extract tex coords
	FbxStringList UVSetNames;
	pMesh->GetUVSetNames(UVSetNames);
	int iUVSetCount = UVSetNames.GetCount();
	bool bUnmapped = false;

	HLOGD("%s: Extracting %d UV sets", CSTR(m_Mesh.sSubName), iUVSetCount);

	if (iUVSetCount > 1)
	{
		HWARNING("Only one set of UVs is supported");
		iUVSetCount = 1;
	}

	if (iUVSetCount == 1)
	{
		FbxString sName = UVSetNames.GetStringAt(0);

		// allocate minimum size
		m_Mesh.TexCoords.reserve(iPolyCount * 3);

		for (int p = 0; p < iPolyCount; ++p)
		{
			int uPolySize = pMesh->GetPolygonSize(p);

			for (int pi = 0; pi < uPolySize; ++pi)
			{
				int iPosInPoly = (_bReverseWinding ? (uPolySize - 1) - pi : pi);
				
				FbxVector2 vUV;
				if (pMesh->GetPolygonVertexUV(p, iPosInPoly, sName, vUV, bUnmapped) == false)
				{
					HERROR("Failed to extract VertexUV");
					return false;
				}

				Math::XMFLOAT2 OutUV = Fbx2ToHelix2(vUV);
				if (_bFlipV)
				{
					OutUV.y = 1.f - OutUV.y;
				}

				m_Mesh.TexCoords.push_back(OutUV);
			}
		}

		HLOGD("%s: Extracted %d UVs [%s]",
			CSTR(m_Mesh.sSubName),
			static_cast<int>(m_Mesh.TexCoords.size()),
			CSTR(sName.Buffer()));
	}
	
	return true;
}

//-----------------------------------------------------------------------------------------------

bool H3DSceneNode::Assemble(const Display::TVertexDeclaration& _kTargetDeclaration, Resources::TH3DFlags _kComponents, bool _bRecursive, bool _bOptimize)
{
	if (_bRecursive)
	{
		std::vector<H3DSceneNode*> Nodes;
		GetChildNodesWithMeshData(Nodes, true);

		for (H3DSceneNode* pNode : Nodes)
		{
			pNode->Assemble(_kTargetDeclaration, _kComponents, false, _bOptimize);
		}
	}

	if (m_bRootNode)
	{
		return true;
	}

	m_IndexData.clear();
	m_VertexData.clear();
	m_PhysxData.clear();

	const bool bWriteVertexPos = _kTargetDeclaration & Display::VertexLayout_Pos_XYZ;
	const bool bWriteUV = _kTargetDeclaration & Display::VertexLayout_UV;
	const bool bWriteNormals = _kTargetDeclaration & Display::VertexLayout_Normal;
	const bool bGenerateTangents = _kTargetDeclaration & Display::VertexLayout_Tangent;
	const bool bGenerateBitangents = _kTargetDeclaration & Display::VertexLayout_Bitangent;

	const bool bGeneratePhysxMesh = _kComponents & Resources::H3DFlag_PhysxMesh;

	const uint32_t uInputVertices = static_cast<uint32_t>(m_Mesh.Vertices.size());
	const uint32_t uInputNormals = static_cast<uint32_t>(m_Mesh.Normals.size());
	const uint32_t uInputUVs = static_cast<uint32_t>(m_Mesh.TexCoords.size());

	if (uInputVertices < 3)
	{
		HWARNING("%s: Has no vertices", CSTR(m_Mesh.sSubName));
		return true;
	}

	const uint32_t uOutputVertexStrideSize = Display::GetVertexDeclarationSize(_kTargetDeclaration);
	m_kTargetDeclaration = _kTargetDeclaration;
	
	const bool bHasNormals = uInputNormals > 3;
	const bool bHasUVs = uInputUVs > 3;

	if (bGenerateTangents || bGenerateBitangents)
	{
		if (m_Mesh.TexCoords.size() != m_Mesh.Vertices.size() ||
			m_Mesh.TexCoords.size() != m_Mesh.Normals.size())
		{
			HERROR("%s: Invalid number of UVs/Vertices/Normals", CSTR(m_Mesh.sSubName));
			return false;
		}
	}

	if (uInputVertices != m_Mesh.uPolyCount * 3u)
	{
		HERROR("%s: Mesh is not triangulated correctly!", CSTR(m_Mesh.sSubName));
		return false;
	}

	std::vector<uint32_t> Indices(uInputVertices);
	std::vector<Math::XMFLOAT3> Tangents;
	std::vector<Math::XMFLOAT3> Bitangents;

	HRESULT kResult = S_OK;

	//-----------------------------------------------------------------------------------------------
	// Compute Tangents
	//-----------------------------------------------------------------------------------------------

	if ((bGenerateTangents || bGenerateBitangents) && bHasNormals && bHasUVs)
	{
		if (bGenerateTangents)
		{
			Tangents.resize(uInputVertices);
		}

		if (bGenerateBitangents)
		{
			Bitangents.resize(uInputVertices);
		}

		// dummy index buffer, vertices are ordered by extraction
		for (uint32_t i = 0; i < uInputVertices; ++i)
		{
			Indices[i] = i;
		}

		HLOGD("%s: Computing TangentFrame", CSTR(m_Mesh.sSubName));

		HRIF(kResult = DirectX::ComputeTangentFrame(
			Indices.data(), // PolyCount * 3
			m_Mesh.uPolyCount,
			m_Mesh.Vertices.data(),
			m_Mesh.Normals.data(),
			m_Mesh.TexCoords.data(),
			uInputVertices,
			bGenerateTangents ? &Tangents[0] : static_cast<Math::XMFLOAT3*>(nullptr),
			bGenerateBitangents ? &Bitangents[0] : static_cast<Math::XMFLOAT3*>(nullptr)))
		{
			return false;
		}
	}

	hlx::bytestream VertexOut(m_VertexData);
	
	hlx::bytes StrideBuffer(uOutputVertexStrideSize);
	hlx::bytestream Stride(StrideBuffer);

	// hash -> stream index
	std::unordered_map<uint64_t, uint32_t> GeneratedVertexIndices;

	std::vector<Math::XMFLOAT3> GeneratedVertices;
	if (_bOptimize)
	{
		GeneratedVertices.reserve(uInputVertices);
	}

	//-----------------------------------------------------------------------------------------------
	// Convert & Reoder Vertices
	//-----------------------------------------------------------------------------------------------

	uint32_t uStreamIndex = 0;
	
	HLOG("%s: Assembling and reodering Vertices", CSTR(m_Mesh.sSubName));
	for (uint32_t v = 0; v < uInputVertices; ++v)
	{
		Stride.clear();

		if (bWriteVertexPos)
		{
			Stride << m_Mesh.Vertices[v];
		}
	
		if (bWriteNormals)
		{
			Stride << m_Mesh.Normals[v];
		}

		if (bGenerateTangents)
		{
			Stride << Tangents[v];
		}

		if (bGenerateBitangents)
		{
			Stride << Bitangents[v];
		}

		if (bWriteUV)
		{
			Stride << m_Mesh.TexCoords[v];
		}

		// check if this constellation (vertexlayout) already exists
		uint64_t uHash = DefaultHashFunction(StrideBuffer.data(), StrideBuffer.size());
		std::unordered_map<uint64_t, uint32_t>::iterator it = GeneratedVertexIndices.find(uHash);

		if (it != GeneratedVertexIndices.end())
		{
			Indices[v] = it->second; // reuse index
			continue;
		}

		Indices[v] = uStreamIndex; // save index
		GeneratedVertexIndices[uHash] = uStreamIndex;		

		// write stride to stream
		VertexOut << StrideBuffer;

		// copy newly ordered vertices for optimization
		if (_bOptimize)
		{
			GeneratedVertices.push_back(m_Mesh.Vertices[v]);
		}

		++uStreamIndex;
	}

	m_uGeneratedVertexCount = uStreamIndex;
	const uint32_t uGeneratedVertices = uStreamIndex;

	HLOGD("%s: Generated %d Vertices", CSTR(m_Mesh.sSubName), uGeneratedVertices);

	//-----------------------------------------------------------------------------------------------
	// Optimize Mesh
	//-----------------------------------------------------------------------------------------------

	if (_bOptimize)
	{
		HLOGD("%s: Optimizing Geometry", CSTR(m_Mesh.sSubName));

		float fACMR, fATVR;
		DirectX::ComputeVertexCacheMissRate(Indices.data(), m_Mesh.uPolyCount, uGeneratedVertices, DirectX::OPTFACES_V_DEFAULT, fACMR, fATVR);

		std::vector<uint32_t> AdjacencyRep(Indices.size());

		HRESULT kResult = S_OK;

		HRIF(kResult = DirectX::GenerateAdjacencyAndPointReps(Indices.data(), m_Mesh.uPolyCount, GeneratedVertices.data(), uGeneratedVertices, 0.f, nullptr, &AdjacencyRep[0]))
		{
			return false;
		}

		HLOGD("%s: Optimizing faces", CSTR(m_Mesh.sSubName));
		std::vector<uint32_t> ReMapping(Indices.size());
		HRIF(kResult = DirectX::OptimizeFaces(Indices.data(), m_Mesh.uPolyCount, AdjacencyRep.data(), &ReMapping[0]))
		{
			return false;
		}

		HLOGD("%s: Optimizing indices", CSTR(m_Mesh.sSubName));
		HRIF(kResult = DirectX::ReorderIB(&Indices[0], m_Mesh.uPolyCount, ReMapping.data()))
		{
			return false;
		}

		HLOGD("%s: Optimizing vertices", CSTR(m_Mesh.sSubName));
		HRIF(kResult = DirectX::OptimizeVertices(Indices.data(), m_Mesh.uPolyCount, uGeneratedVertices, &ReMapping[0]))
		{
			return false;
		}

		HLOGD("%s: Finalizing indices", CSTR(m_Mesh.sSubName));
		HRIF(kResult = DirectX::FinalizeIB(&Indices[0], m_Mesh.uPolyCount, ReMapping.data(), uGeneratedVertices))
		{
			return false;
		}

		HLOGD("%s: Finalizing vertices", CSTR(m_Mesh.sSubName));
		HRIF(kResult = DirectX::FinalizeVB(&m_VertexData[0], uOutputVertexStrideSize, uGeneratedVertices, ReMapping.data()))
		{
			return false;
		}

		float fACMR2, fATVR2;
		DirectX::ComputeVertexCacheMissRate(Indices.data(), m_Mesh.uPolyCount, uGeneratedVertices, DirectX::OPTFACES_V_DEFAULT, fACMR2, fATVR2);

		HLOGD("%f->%f average cache miss ratio (ACMR)", fACMR, fACMR2);
		HLOGD("%f->%f average triangle vertex resuse (ATVR)", fATVR, fATVR2);
	}

	//-----------------------------------------------------------------------------------------------
	// Convert Indices
	//-----------------------------------------------------------------------------------------------

	hlx::bytestream IndexOut(m_IndexData);

	m_kIndexBufferFormat = Indices.size() > 0xffff ? Display::PixelFormat_R32_UInt : Display::PixelFormat_R16_UInt;

	for (const uint32_t& uIndex : Indices)
	{
		if (m_kIndexBufferFormat == Display::PixelFormat_R32_UInt)
		{
			IndexOut << uIndex;
		}
		else
		{
			IndexOut << static_cast<uint16_t>(uIndex);
		}
	}

	//-----------------------------------------------------------------------------------------------
	// Compute PhysxMesh
	//-----------------------------------------------------------------------------------------------

	if (bGeneratePhysxMesh)
	{
		ConvexMeshCooking& MeshCook = *ConvexMeshCooking::Instance();

		HLOGD("%s: Generating ConvexMesh from %d vertices", CSTR(m_Mesh.sSubName), uInputVertices);
		uint32_t uReachedVertexLimit = 0;

		m_PhysxData = MeshCook.GenerateConvexMesh(m_Mesh.Vertices, 256u, uReachedVertexLimit);

		if (m_PhysxData.empty() == false)
		{
			m_SimulationShape.kType = Physics::GeometryType_ConvexMesh;
			m_SimulationShape.ConvexMesh.uCRC32 = CRC32(m_PhysxData);
			HLOGD("%s: Generated ConvexMesh with %d vertices [%X]", CSTR(m_Mesh.sSubName), uReachedVertexLimit, m_SimulationShape.ConvexMesh.uCRC32);
		}
		else
		{
			HERROR("%s: Failed to generate ConvexMesh", CSTR(m_Mesh.sSubName));
			return false;
		}
	}

	return true;
}

//-----------------------------------------------------------------------------------------------

bool H3DSceneNode::Save(Resources::H3DFile& _H3DOut, Resources::TH3DFlags _kComponents, const Resources::H3DVersion _kVersion, bool _bRecursive, bool _bClearTemporaryData)
{
	// stride size only depends on target layout
	_H3DOut.m_Header.kFlags = _kComponents;
	_H3DOut.m_Header.kVersion = _kVersion;
	if (_H3DOut.m_Header.sName.empty())
	{
		_H3DOut.m_Header.sName = m_sName;
	}

	std::vector<H3DSceneNode*> ChildNodes;

	if (m_VertexData.empty() == false && m_IndexData.empty() == false)
	{
		ChildNodes.push_back(this);
	}

	if (_bRecursive)
	{
		GetChildNodesWithAssembledMeshData(ChildNodes, _bRecursive);
	}

	for (H3DSceneNode* pNode : ChildNodes)
	{
		Resources::H3DHeader::SubMesh Mesh = {};
		// get name of h3d
		pNode->m_Mesh.sMasterName = _H3DOut.m_Header.sName;
		Mesh.sSubName = pNode->m_Mesh.sSubName;

		Mesh.kPrimitiveTopology = Display::PrimitiveTopology_TriangleList;

		Mesh.uIndexCount = pNode->m_Mesh.uPolyCount * 3u;
		Mesh.kIndexBufferFormat = pNode->m_kIndexBufferFormat;
		Mesh.uIndexElementSize = pNode->m_kIndexBufferFormat == Display::PixelFormat_R32_UInt ? 4u : 2u;

		Mesh.kVertexDeclaration = pNode->m_kTargetDeclaration;
		Mesh.uVertexCount = pNode->m_uGeneratedVertexCount;
		Mesh.uVertexStrideSize = Display::GetVertexDeclarationSize(pNode->m_kTargetDeclaration);
		
		// offset relative to section start (_H3DOut.m_Header.uVertexDataOffset etc)
		Mesh.uVertexByteOffset = _H3DOut.m_Header.uVertexDataSize;
		Mesh.uIndexByteOffset = _H3DOut.m_Header.uIndexDataSize;
		Mesh.uConvexPhysxMeshOffset = _H3DOut.m_Header.uPhysXDataSize;

		Mesh.uConvexPhysxMeshSize = static_cast<uint32_t>(pNode->m_PhysxData.size());

		_H3DOut.m_Header.uVertexDataSize += static_cast<uint32_t>(pNode->m_VertexData.size());
		_H3DOut.m_Header.uIndexDataSize += static_cast<uint32_t>(pNode->m_IndexData.size());
		_H3DOut.m_Header.uPhysXDataSize += static_cast<uint32_t>(pNode->m_PhysxData.size());

		_H3DOut.m_VertexData += pNode->m_VertexData;
		_H3DOut.m_IndexData += pNode->m_IndexData;
		_H3DOut.m_PhysxData += pNode->m_PhysxData;

		if (_bClearTemporaryData)
		{
			pNode->Clear();
		}

		_H3DOut.m_MeshCluster.push_back(Mesh);
	}

	// fill remaining header fields
	_H3DOut.m_Header.uVertexDataOffset = _H3DOut.m_Header.GetSizeOnDisk(_kVersion);
	_H3DOut.m_Header.uIndexDataOffset = _H3DOut.m_Header.uVertexDataOffset + _H3DOut.m_Header.uVertexDataSize;
	_H3DOut.m_Header.uPhysXDataOffset = _H3DOut.m_Header.uIndexDataOffset + _H3DOut.m_Header.uIndexDataSize;

	_H3DOut.m_Header.uClusterCount = static_cast<uint32_t>(_H3DOut.m_MeshCluster.size());

	return true;
}

//-----------------------------------------------------------------------------------------------
void H3DSceneNode::Clear()
{
	m_Mesh.Clear();
	m_IndexData.clear();
	m_VertexData.clear();
	m_PhysxData.clear();
	m_MaterialProperties.clear();
	m_Materials.clear();
	//m_Transform = Math::transform(physx::PxIdentity);
	//m_uGeneratedVertexCount = 0u;
}
//-----------------------------------------------------------------------------------------------

void H3DSceneNode::GetChildNodesOfType(std::vector<H3DSceneNode*>& _ChildNodesOut, const NodeType& _kType, bool _bRecursive) const
{
	for (H3DSceneNode* pChild : m_ChildNodes)
	{
		if (pChild->GetType() == _kType)
		{
			_ChildNodesOut.push_back(pChild);
		}

		if (_bRecursive)
		{
			pChild->GetChildNodes(_ChildNodesOut, _bRecursive);
		}
	}
}

//-----------------------------------------------------------------------------------------------

void H3DSceneNode::GetChildNodes(std::vector<H3DSceneNode*>& _ChildNodesOut, bool _bRecursive) const
{
	for (H3DSceneNode* pChild : m_ChildNodes)
	{
		_ChildNodesOut.push_back(pChild);

		if (_bRecursive)
		{
			pChild->GetChildNodes(_ChildNodesOut, _bRecursive);
		}
	}
}

//-----------------------------------------------------------------------------------------------
void H3DSceneNode::GetChildNodesWithMeshData(std::vector<H3DSceneNode*>& _ChildNodesOut, bool _bRecursive) const
{
	for (H3DSceneNode* pChild : m_ChildNodes)
	{
		if (pChild->m_Mesh.Vertices.empty() == false && pChild->m_Mesh.TexCoords.empty() == false)
		{
			_ChildNodesOut.push_back(pChild);
		}

		if (_bRecursive)
		{
			pChild->GetChildNodesWithMeshData(_ChildNodesOut, _bRecursive);
		}
	}
}
//-----------------------------------------------------------------------------------------------
void H3DSceneNode::GetChildNodesWithAssembledMeshData(std::vector<H3DSceneNode*>& _ChildNodesOut, bool _bRecursive) const
{
	for (H3DSceneNode* pChild : m_ChildNodes)
	{
		if (pChild->m_VertexData.empty() == false && pChild->m_IndexData.empty() == false)
		{
			_ChildNodesOut.push_back(pChild);
		}

		if (_bRecursive)
		{
			pChild->GetChildNodesWithAssembledMeshData(_ChildNodesOut, _bRecursive);
		}
	}
}
//-----------------------------------------------------------------------------------------------
Math::transform H3DSceneNode::GetGlobalTransform() const
{
	std::vector<Math::transform> Transforms = {m_Transform};

	const H3DSceneNode* pParent = m_pParent;

	// collect transfrom from this object to the root
	while (pParent != nullptr)
	{
		if (!(pParent->m_Transform == HTRANSFORM_IDENTITY))
		{
			Transforms.push_back(pParent->m_Transform);
		}

		pParent = pParent->m_pParent;
	}

	Math::transform GlobalTransform = HTRANSFORM_IDENTITY;

	// transform from root to this object
	for (std::vector<Math::transform>::reverse_iterator it = Transforms.rbegin(), end = Transforms.rend(); it != end; ++it)
	{
		GlobalTransform *= *it;
	}

	return GlobalTransform.getNormalized();
}
//-----------------------------------------------------------------------------------------------
Math::float3 H3DSceneNode::GetGlobalScale() const
{
	Math::float3 vScale = m_vScale;
	const H3DSceneNode* pParent = m_pParent;

	while (pParent != nullptr)
	{
		if (pParent->m_vScale.x == 0.f ||
			pParent->m_vScale.y == 0.f ||
			pParent->m_vScale.z == 0.f)
		{
			HERROR("Invalid Zero Scale for object %s", CSTR(pParent->GetName()));
			break;
		}

		vScale.x *= pParent->m_vScale.x;
		vScale.y *= pParent->m_vScale.y;
		vScale.z *= pParent->m_vScale.z;

		pParent = pParent->m_pParent;
	}

	return vScale;
}
//-----------------------------------------------------------------------------------------------
