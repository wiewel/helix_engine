//   Copyright 2020 Fabian Wahlster
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef TEXTUREIMPORTER_H
#define TEXTUREIMPORTER_H

#include "Display\ViewDefines.h"

namespace Helix
{
	namespace H3D
	{
		class TextureImporter
		{
		public:
			static bool Convert(
				const std::wstring& _sInPath,
				const std::wstring& _sOutPath,
				Display::PixelFormat _kTargetFormat,
				uint32_t _uMipLevels = 1u);
		};
	}
} // Helix

#endif // !TEXTUREIMPORTER_H
