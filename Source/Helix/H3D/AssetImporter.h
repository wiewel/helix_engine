//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher, David Labode
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef ASSETIMPORTER_H
#define ASSETIMPORTER_H

#include "Async\AsyncObject.h"
#include "H3D\H3DSceneNode.h"
#include "hlx\src\String.h"
#include "Util\NucleoProgressFunction.h"

namespace Helix
{
	namespace H3D
	{
		class AssetImporter : public Async::IAsyncObject
		{
		public:
			AssetImporter(const hlx::string& _sWorkingDir = {});
			~AssetImporter();

			void SetImportTextures(bool _bImportTextures);
			void SetImportMaterials(bool _bImportMaterials);
			void SetToLefthanded(bool _bToLefthanded);
			void SetReverseWinding(bool _bReverseWinding);
			void SetFlipV(bool _bFlipV);
			void SetCreateConvexHull(bool _bCreateConvexHull);
			void SetReversePivot(bool _bReversePivot);
			void SetNullPivot(bool _bNullPivot);

			void SetMipLevel(uint32_t _uMipLevel);
			void SetScale(float _fScale);
			void SetVertexLayout(Display::TVertexDeclaration _kVertexLayout);
			void SetCoordinateSystem(H3D::CoordinateSystem _kCoordSystem);
			void SetFilesToImport(const std::vector<hlx::string>& _Files);

			void SetHMatOutputDir(const hlx::string& _sHMatOutDir);
			void SetH3DOutDir(const hlx::string& _sH3DOutDir);
			void SetTexOutDir(const hlx::string& _sTexOutDir);

			void SetProgressFunction(const TProgressFunctor& _Func);

		private:
			void Execute() final;

			void ImportFBX(const hlx::string& _PathToFBX);
			void ImportOBJ(const hlx::string& _PathToOBJ);

			void ImportTexture(const hlx::string& _PathToImage, Display::PixelFormat _kTargetFormat);
			void ImportTextureByName(const hlx::string& _PathToImage);

		private:
			std::vector<hlx::string> m_FilesToImport;
			TProgressFunctor m_UpdateFunction;
			
			int32_t m_iProgressValue = 0;
			int32_t m_iProgressMin = 0;
			int32_t m_iProgressMax = 1;

			hlx::string m_sH3DOutDir;
			hlx::string m_sHMATOutDir;
			hlx::string m_sTextOutDir;

			Resources::H3DVersion m_kTargetVersion = Resources::H3DVersion_v1;
			Resources::TH3DFlags m_kComponentFlag = Resources::H3DFlag_None;

			bool m_bImportTextures = false;
			bool m_bImportMaterials = false;
			bool m_bToLefthanded = false;
			bool m_bReverseWinding = false;
			bool m_bFlipV = false;
			bool m_bCreateConvexHull = false;
			bool m_bReversePivot = false;
			bool m_bNullPivot = false;

			uint32_t m_uMipLevel = 1u;
			float m_fScale = 1.f;
			Display::TVertexDeclaration m_kVertexLayout = Display::VertexLayout_PosXYZNormTanTex;
			H3D::CoordinateSystem m_kCoordSytem = H3D::CoordinateSystem_DirectX;

		};
		inline void AssetImporter::SetImportTextures(bool _bImportTextures)
		{
			m_bImportTextures = _bImportTextures;
		}

		inline void AssetImporter::SetImportMaterials(bool _bImportMaterials)
		{
			m_bImportMaterials = _bImportMaterials;
		}

		inline void AssetImporter::SetToLefthanded(bool _bToLefthanded)
		{
			m_bToLefthanded = _bToLefthanded;
		}

		inline void AssetImporter::SetReverseWinding(bool _bReverseWinding)
		{
			m_bReverseWinding = _bReverseWinding;
		}

		inline void AssetImporter::SetFlipV(bool _bFlipV)
		{
			m_bFlipV = _bFlipV;
		}

		inline void AssetImporter::SetCreateConvexHull(bool _bCreateConvexHull)
		{
			m_bCreateConvexHull = _bCreateConvexHull;
		}

		inline void AssetImporter::SetReversePivot(bool _bReversePivot)
		{
			m_bReversePivot = _bReversePivot;
		}

		inline void AssetImporter::SetNullPivot(bool _bNullPivot)
		{
			m_bNullPivot = _bNullPivot;
		}

		inline void AssetImporter::SetMipLevel(uint32_t _uMipLevel)
		{
			m_uMipLevel = _uMipLevel;
		}

		inline void AssetImporter::SetScale(float _fScale)
		{
			m_fScale = _fScale;
		}

		inline void AssetImporter::SetVertexLayout(Display::TVertexDeclaration _kVertexLayout)
		{
			m_kVertexLayout = _kVertexLayout;
		}

		inline void AssetImporter::SetCoordinateSystem(H3D::CoordinateSystem _kCoordSystem)
		{
			m_kCoordSytem = _kCoordSystem;
		}

		inline void AssetImporter::SetFilesToImport(const std::vector<hlx::string>& _Files)
		{
			m_FilesToImport = _Files;
		}

		inline void AssetImporter::SetHMatOutputDir(const hlx::string& _sHMatOutDir)
		{
			m_sHMATOutDir = _sHMatOutDir;
		}

		inline void AssetImporter::SetH3DOutDir(const hlx::string& _sH3DOutDir)
		{
			m_sH3DOutDir = _sH3DOutDir;
		}

		inline void AssetImporter::SetTexOutDir(const hlx::string& _sTexOutDir)
		{
			m_sTextOutDir = _sTexOutDir;
		}

		inline void AssetImporter::SetProgressFunction(const TProgressFunctor& _Func)
		{
			m_UpdateFunction = _Func;
		}

	} // Nucleo
} // Helix

#endif // !ASSETIMPORTER_H
