//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef OBJFILE_H
#define OBJFILE_H

#include <vector>
#include "Math\MathTypes.h"
#include "hlx\src\StandardDefines.h"
#include "Resources\H3DFile.h"
#include "Util\BaseHash.h"

namespace Helix
{
	namespace H3D
	{
		class H3DFile;

		struct Triangle
		{
			//triangle indices
			struct PointIndex
			{
				uint32_t uVertexIndex = HUNDEFINED;
				uint32_t uNormalIndex = HUNDEFINED;
				uint32_t uUVIndex = HUNDEFINED;

				inline uint64_t GetHash() const
				{
					return DefaultHashFunction(reinterpret_cast<const uint8_t*>(&uVertexIndex), sizeof(uint32_t) * 3);
				}
			};

			PointIndex Indices[3];
		};

		// group of faces
		struct FaceGroup
		{
			std::string sName; // group name

			uint32_t uStartIndex; // first Triangle
			uint32_t uCount;

			std::string sMaterial;

			uint32_t uSmoothing;
		};

		using TFaceGroups = std::vector<FaceGroup>;

		struct OBJModel
		{
			std::string sName;

			TFaceGroups FaceGroups;

			std::vector<Math::float3> Vertices; // v
			std::vector<Math::float3> Normals; //vn
			std::vector<Math::float2> TexCoords; // vt => u,v

			std::vector<Triangle> Faces;

			void PrintStats()
			{
				HLOG("%s: Vertices %d Normals %d UVs %d Groups %d",
					CSTR(sName),
					Vertices.size(),
					Normals.size(),
					TexCoords.size(),
					FaceGroups.size());
			}
		};

		class OBJFile
		{
		public:
			HDEBUGNAME("OBJFile");

			OBJFile(const std::string& _sFilePath);
			~OBJFile();

			bool Load(hlx::bytestream& _Stream, bool _bConvertToLeftHanded = true, bool _bFlipV = true);

			// converts a OBJFile to a H3D with a desired target vertex declaration (generates normals & tangents)
			bool Save(_Out_ Resources::H3DFile& _H3DOut, const Display::TVertexDeclaration& _kTargetDeclaration, bool _bReverseWinding = true, Resources::TH3DFlags _kComponents = Resources::H3DFlag_None, const Resources::H3DVersion _kVersion = Resources::H3DVersion_v1);

			const std::vector<OBJModel>& GetModels() const;
			const std::vector<std::string>& GetMaterialLibs() const;
			const std::string& GetFileName() const;

		private:
			bool ValidateIndices(const OBJModel& _Model, const Triangle& _Face) const;

		private:
			std::string m_sFilePath;
			std::string m_sFileName;

			std::vector<std::string> m_MaterialLibs; // mtllib
			std::vector<OBJModel> m_OBJModels;			
		};

		inline const std::vector<OBJModel>& OBJFile::GetModels() const { return m_OBJModels; }

		inline const std::vector<std::string>& OBJFile::GetMaterialLibs() const	{ return m_MaterialLibs; }

		inline const std::string & OBJFile::GetFileName() const	{ return m_sFileName; }

	} // H3DTool

} // Helix

#endif