//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher, David Labode
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "FbxSDKFactory.h"
#include "Scene\PhysicsScene.h"

using namespace Helix::H3D;
//-----------------------------------------------------------------------------------------------

FbxSDKFactory::FbxSDKFactory()
{
	m_pSDKManager = FbxManager::Create();

	if (m_pSDKManager != nullptr)
	{
		m_pImporter = FbxImporter::Create(m_pSDKManager, "H3DImporter");
		m_pIOSettings = FbxIOSettings::Create(m_pSDKManager, IOSROOT);

		if (m_pIOSettings != nullptr)
		{
			m_pSDKManager->SetIOSettings(m_pIOSettings);
		}

		m_pScene = FbxScene::Create(m_pSDKManager, "H3DScene");
	}
}
//-----------------------------------------------------------------------------------------------

FbxSDKFactory::~FbxSDKFactory()
{
	HSAFE_FUNC_RELEASE(m_pImporter, Destroy);
	HSAFE_FUNC_RELEASE(m_pScene, Destroy);
	HSAFE_FUNC_RELEASE(m_pIOSettings, Destroy);
	HSAFE_FUNC_RELEASE(m_pSDKManager, Destroy);
}
//-----------------------------------------------------------------------------------------------

FbxNode* FbxSDKFactory::Initialize(const hlx::string& _sFilePath, const double& _fScaleFactor, const CoordinateSystem& _TargetCoordSystem)
{
	if (m_pSDKManager == nullptr)
	{
		HFATAL("Failed to create FbxSDK::Manager!");
		return nullptr;
	}
	
	if (m_pImporter == nullptr)
	{
		HFATAL("Failed to create FbxSDK::Importer!");
		return nullptr;
	}

	if (m_pScene == nullptr)
	{
		HFATAL("Failed to create FbxSDK::Scene!");
		return nullptr;
	}

	int iFileFormat = -1;
	if (m_pSDKManager->GetIOPluginRegistry()->DetectReaderFileFormat(hlx::to_sstring(_sFilePath).c_str(), iFileFormat) == false)
	{
		// fall back to binary fbx
		iFileFormat = m_pSDKManager->GetIOPluginRegistry()->FindReaderIDByDescription("FBX binary (*.fbx)");
	}

	HLOG("Opening %s", _sFilePath.c_str());
	if (m_pImporter->Initialize(hlx::to_sstring(_sFilePath).c_str(), iFileFormat, m_pIOSettings) == false)
	{
		
		HERROR("Failed to initialize FbxSDK::Importer for %s!", _sFilePath.c_str());
		return nullptr;
	}

	if (m_pImporter->Import(m_pScene) == false)
	{
		HERROR("Failed to import scene");
		return nullptr;
	}

	//MayaYUp(UpVector = +Y, FrontVector = +Z, CoordSystem = +X
	FbxAxisSystem TargetAxisSystem = FbxAxisSystem::MayaYUp;

	switch (_TargetCoordSystem)
	{
	case CoordinateSystem_DirectX:
		HLOG("Target AxisSystem DirectX");
		TargetAxisSystem = FbxAxisSystem::DirectX;
		break;
	case CoordinateSystem_OpenGL:
		HLOG("Target AxisSystem OpenGL");
		TargetAxisSystem = FbxAxisSystem::OpenGL;
		break;
	case CoordinateSystem_MayaZUp:
		HLOG("Target AxisSystem MayaZUp");
		TargetAxisSystem = FbxAxisSystem::MayaZUp;
		break;
	case CoordinateSystem_MayaYUp:
		HLOG("Target AxisSystem MayaYUp");
		TargetAxisSystem = FbxAxisSystem::MayaYUp;
		break;
	case CoordinateSystem_MotionBuilder:
		HLOG("Target AxisSystem Motionbuilder");
		TargetAxisSystem = FbxAxisSystem::Motionbuilder;
		break;
	case CoordinateSystem_Lightwave:
		HLOG("Target AxisSystem Lightwave");
		TargetAxisSystem = FbxAxisSystem::Lightwave;
		break;
	case CoordinateSystem_Max:
		HLOG("Target AxisSystem Max");
		TargetAxisSystem = FbxAxisSystem::Max;
		break;
	default:
		break;
	}

	//FbxAxisSystem::ConvertScene(), which acts on the node transforms(pre - rotation) and animations.
	//Note that calls to ConvertScene() do not change the vertex values of meshes, and only affect node transforms and animations.If the scene is already in the required axis system or required unit system, a call to ConvertScene() will have no effect on the scene.For example :

	// convert to target axis system
	if (_TargetCoordSystem != CoordinateSystem_Unknown && 
		m_pScene->GetGlobalSettings().GetAxisSystem() != TargetAxisSystem)
	{
		TargetAxisSystem.ConvertScene(m_pScene);
	}
	
	//FbxSystemUnit::ConvertScene(), which acts on the node transforms(scale) and animations.
	FbxSystemUnit SceneSystemUnit = m_pScene->GetGlobalSettings().GetSystemUnit();
	if (SceneSystemUnit.GetScaleFactor() != _fScaleFactor)
	{
		//pScaleFactor The equivalent number of centimeters in the new system unit. For example, an inch unit uses a scale factor of 2.54.
		//pMultiplier  A multiplier factor of pScaleFactor.

		FbxSystemUnit TargetUnitSystem(_fScaleFactor);
		HLOG("Converting %s UnitSystem to scale %f", _sFilePath.c_str(), _fScaleFactor);

		TargetUnitSystem.ConvertScene(m_pScene);
	}

	return m_pScene->GetRootNode();
}
