//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "OBJFile.h"
#include "hlx\src\Logger.h"
#include "hlx\src\StringHelpers.h"
#include "hlx\src\Bytes.h"
#include "Math\MathFunctions.h"
#include <sstream>
#include "ConvexMeshCooking.h"
#include <unordered_map>

using namespace Helix;
using namespace Helix::H3D;

//---------------------------------------------------------------------------------------------------------------------
OBJFile::OBJFile(const std::string& _sFilePath) :
	m_sFilePath(_sFilePath)
{
	m_sFileName = hlx::get_right_of(m_sFilePath, "\\");
}
//---------------------------------------------------------------------------------------------------------------------
OBJFile::~OBJFile()
{
}
//---------------------------------------------------------------------------------------------------------------------
bool OBJFile::Load(hlx::bytestream& _Stream, bool _bConvertToLeftHanded, bool _bFlipV)
{
	// no data available
	if (_Stream.good() == false)
	{
		HERROR("No obj data available!");
		return false;
	}

	OBJModel CurObject;
	FaceGroup CurFaceGroup = {};

	uint32_t uLineNumber = 0;
	uint32_t uUnkownTokens = 0;
	uint32_t uSmoothing = 0;
	std::string sMaterial;

	while (_Stream.good())
	{	
		++uLineNumber;

		std::string sToken = hlx::read_until(_Stream, '\n');

		//remove all comments
		size_t uComment = sToken.find("#", 0);
		if (uComment != std::string::npos)
		{
			sToken.erase(uComment, std::string::npos);
		}

		std::stringstream Line(sToken);
		Line >> sToken;

		if (sToken.empty())
		{
			continue;
		}

		//skip comments
		if (sToken[0] == '#')
		{
			// check for:
			//# object Name
			// and:
			//#object Name

			std::string sNextToken;
			Line >> sNextToken;

			if (sToken == "#object" || sNextToken == "object")
			{
				if (CurObject.Vertices.empty() == false)
				{
					if (CurObject.sName.empty())
					{
						CurObject.sName = m_sFileName;
					}

					CurObject.PrintStats();

					if (CurFaceGroup.uCount > 0)
					{
						CurFaceGroup.uSmoothing = uSmoothing;
						CurFaceGroup.sMaterial = sMaterial;
						CurObject.FaceGroups.push_back(CurFaceGroup);
						
						CurFaceGroup = {};
						CurFaceGroup.uStartIndex = static_cast<uint32_t>(CurObject.Faces.size());
						CurFaceGroup.uCount = 0;
					}

					m_OBJModels.push_back(CurObject);
					CurObject = OBJModel(); // reset
				}

				if (sNextToken == "object")
				{
					Line >> CurObject.sName; // read object name
				}
				else
				{
					CurObject.sName = sNextToken;
				}				
			}

			// skip the rest of the comment
			continue;
		}

		if (sToken == "mtllib")
		{
			Line >> sToken;
			m_MaterialLibs.push_back(sToken);
		}
		else if (sToken == "o")
		{
			if (CurObject.Vertices.empty() == false)
			{
				if (CurObject.sName.empty())
				{
					CurObject.sName = m_sFileName;
				}

				CurObject.PrintStats();

				if (CurFaceGroup.uCount > 0)
				{
					CurFaceGroup.sMaterial = sMaterial;
					CurFaceGroup.uSmoothing = uSmoothing;
					CurObject.FaceGroups.push_back(CurFaceGroup);

					CurFaceGroup = {};
					CurFaceGroup.uStartIndex = static_cast<uint32_t>(CurObject.Faces.size());
					CurFaceGroup.uCount = 0;
				}

				m_OBJModels.push_back(CurObject);
				CurObject = OBJModel(); // reset
			}

			// read object name
			Line >> CurObject.sName; 
		}
		else if (sToken == "usemtl")
		{
			Line >> sMaterial;
		}
		else if (sToken == "v") // vertices
		{
			Math::float3 vVertex;
			Line >> vVertex.x;
			Line >> vVertex.y;
			Line >> vVertex.z;

			if (_bConvertToLeftHanded)
			{
				vVertex.z = -vVertex.z;
			}

			CurObject.Vertices.push_back(vVertex);
		}
		else if (sToken == "vn") // normals
		{
			Math::float3 vNormal;
			Line >> vNormal.x;
			Line >> vNormal.y;
			Line >> vNormal.z;

			if (_bConvertToLeftHanded)
			{
				vNormal.z = -vNormal.z;
			}

			CurObject.Normals.push_back(vNormal);
		}
		else if (sToken == "vt") // texcoords
		{
			Math::float2 vTexCoord;
			Line >> vTexCoord.x; // u
			Line >> vTexCoord.y; // v
			Line >> sToken;

			if (Line.eof() == false)
			{
				//Line >> vTexCoord.z; // 3D Coords
				HERROR("3D tex coords not supported. Object %s Line %d", uLineNumber, CSTR(CurObject.sName));
				return false;
			}
			//else
			//{
			//	vTexCoord.z = 0.f; // 2D Coords
			//}

			if (_bFlipV)
			{
				vTexCoord.y = 1.f - vTexCoord.y;
			}

			CurObject.TexCoords.push_back(vTexCoord);
		}
		else if (sToken == "p")  // vertex point
		{
			HERROR("Vertex points not supported. Object %s Line %d", uLineNumber, CSTR(CurObject.sName));
			return false;
		}
		else if (sToken == "l")  // vertex line
		{
			HERROR("Vertex lines not supported. Object %s Line %d", uLineNumber, CSTR(CurObject.sName));
			return false;
		}
		else if (sToken == "f") // faces
		{
			//f 1/1/1 2/2/1 3/3/1 ...
			//f (v1,vt1,vn1) (v2,vt2,vn2) (v3,vt3,vn3) ...
			//parse faces

			uint32_t uVertexElementCount = 0;

			Triangle Face = {};

			while (Line.good() && uVertexElementCount < 3) // only support triangles
			{
				std::string sTempToken;

				Line >> sTempToken;

				//TODO: temp_token check is just a workaround, fix this
				if (sTempToken == sToken || sTempToken.empty())
				{
					break;
				}

				sToken = sTempToken;

				std::vector<std::string> IndiceTokens = hlx::split(sToken, '/');

				if (IndiceTokens.size() > 0)
				{
					// f 1 2 3 (vertex index only)
					Face.Indices[uVertexElementCount].uVertexIndex = hlx::FromString<uint32_t>(IndiceTokens.at(0)) - 1;

					if (IndiceTokens.size() == 2)
					{
						//f 1/1 2/1 3/1  (vertex / texcoords)
						Face.Indices[uVertexElementCount].uUVIndex = hlx::FromString<uint32_t>(IndiceTokens.at(1)) - 1;
					}
					else if (IndiceTokens.size() > 2)
					{
						if (IndiceTokens.at(1).empty() == false)
						{
							//f 1/1/1 2/2/1 3/3/1 (vertex / texcoord / normal)
							Face.Indices[uVertexElementCount].uUVIndex = hlx::FromString<uint32_t>(IndiceTokens.at(1)) - 1;
						}

						// if above case is false:
						// f 1//1 2//1 3//1 (vertex / normal)
						Face.Indices[uVertexElementCount].uNormalIndex = hlx::FromString<uint32_t>(IndiceTokens.at(2)) - 1;
					}
				}

				++uVertexElementCount;

				if (uVertexElementCount == 3)
				{
					Line >> sTempToken;
					if (Line.eof() == false)
					{
						HERROR("Invalid face data in line %u (Only triangle meshes supported)", uLineNumber);
						return false;
					}
				}
			}

			CurObject.Faces.push_back(Face);

			++CurFaceGroup.uCount;
		}
		else if (sToken == "g") // groups
		{
			//add new group or update existing
			Line >> sToken;

			if (CurFaceGroup.uCount > 0 && sToken != CurFaceGroup.sName)
			{
				CurFaceGroup.sMaterial = sMaterial;
				CurFaceGroup.uSmoothing = uSmoothing;
				CurObject.FaceGroups.push_back(CurFaceGroup);
			}

			CurFaceGroup = {};
			CurFaceGroup.uStartIndex = static_cast<uint32_t>(CurObject.Faces.size());
			CurFaceGroup.uCount = 0;
			CurFaceGroup.sName = sToken;
		}
		else if (sToken == "s") // groups
		{
			//next smoothing group
			Line >> uSmoothing;
		}
		else if(++uUnkownTokens < 10)
		{
			HLOGD("Unkown token '%s' Object %s Line %d", CSTR(sToken), CSTR(CurObject.sName), uLineNumber);
		}
	}

	// add lastest face group
	if (CurFaceGroup.uCount > 0)
	{
		CurFaceGroup.sMaterial = sMaterial;
		CurFaceGroup.uSmoothing = uSmoothing;
		CurObject.FaceGroups.push_back(CurFaceGroup);
	}

	// add last object
	if (CurObject.Vertices.empty() == false)
	{
		if (CurObject.sName.empty())
		{
			CurObject.sName = m_sFileName;
		}

		CurObject.PrintStats();

		m_OBJModels.push_back(CurObject);
	} 

	return true;
}
//---------------------------------------------------------------------------------------------------------------------
bool OBJFile::Save(Resources::H3DFile& _H3DOut, const Display::TVertexDeclaration& _kTargetDeclaration, bool _bReverseWinding, Resources::TH3DFlags _kComponents, const Resources::H3DVersion _kVersion)
{
	const std::vector<OBJModel>& InputModels = m_OBJModels;

	uint32_t uTotalOutputVertexBufferSize = 0;
	uint32_t uTotalOutputIndexBufferSize = 0;

	const bool bWriteVertexPos = _kTargetDeclaration & Display::VertexLayout_Pos_XYZ;
	const bool bWriteUV = _kTargetDeclaration & Display::VertexLayout_UV;
	const bool bWriteNormals = _kTargetDeclaration & Display::VertexLayout_Normal;
	const bool bGenerateTangents = _kTargetDeclaration & Display::VertexLayout_Tangent;
	const bool bGenerateBitangents = _kTargetDeclaration & Display::VertexLayout_Bitangent;

	// stride size only depends on target layout
	const uint32_t uOutputVertexStrideSize = Display::GetVertexDeclarationSize(_kTargetDeclaration);

	_H3DOut.m_MeshCluster.resize(0);

	_H3DOut.m_Header.kFlags = _kComponents;
	_H3DOut.m_Header.kVersion = _kVersion;

	if (_H3DOut.m_Header.sName.empty())
	{
		_H3DOut.m_Header.sName = m_sFileName;
	}

	//---------------------------------------------------------------------------------------------------
	// Write & convert vertex data:
	//---------------------------------------------------------------------------------------------------

	// generated vertex index data for each model, hash -> index
	std::vector<std::unordered_map<uint64_t, uint32_t>> GeneratedVertexIndices(InputModels.size());

	Math::float3 vGeneratedNormal;
	Math::float3 vGeneratedTangent;
	Math::float3 vGeneratedBitangent;

	uint32_t uModel = 0;
	uint32_t uMaxIndexCount = 0;

	_H3DOut.m_VertexData.resize(0);
	_H3DOut.m_PhysxData.resize(0);
	hlx::bytestream VertexOut(_H3DOut.m_VertexData);
	hlx::bytestream PhysxOut(_H3DOut.m_PhysxData);

	for (const OBJModel& Model : InputModels)
	{
		uint32_t uStreamIndex = 0;
		std::unordered_map<uint64_t, uint32_t>& VertexIndices = GeneratedVertexIndices[uModel++];

		Resources::H3DHeader::SubMesh Mesh = {};
		Mesh.sSubName = Model.sName;

		Mesh.uIndexCount = static_cast<uint32_t>(Model.Faces.size() * 3);
		uMaxIndexCount = std::max(uMaxIndexCount, Mesh.uIndexCount);

		Mesh.kVertexDeclaration = _kTargetDeclaration;
		Mesh.kPrimitiveTopology = Display::PrimitiveTopology_TriangleList;
		Mesh.uVertexStrideSize = uOutputVertexStrideSize;
		Mesh.uVertexByteOffset = uTotalOutputVertexBufferSize;

		const size_t uNormals = Model.Normals.size();
		const size_t uVertices = Model.Vertices.size();
		const size_t uUVs = Model.TexCoords.size();

		for (const Triangle& Face : Model.Faces)
		{
			if ((bWriteNormals && uNormals == 0) || bGenerateTangents || bGenerateBitangents)
			{
				if (ValidateIndices(Model, Face) == false)
				{
					return false;
				}

				Math::ComputeTangentBasis(
					Model.Vertices[Face.Indices[0].uVertexIndex], Model.Vertices[Face.Indices[1].uVertexIndex], Model.Vertices[Face.Indices[2].uVertexIndex],
					Model.TexCoords[Face.Indices[0].uUVIndex], Model.TexCoords[Face.Indices[1].uUVIndex], Model.TexCoords[Face.Indices[2].uUVIndex],
					vGeneratedNormal, vGeneratedTangent, vGeneratedBitangent);
			}

			// for each index
			for (uint32_t i = 0; i < 3; ++i)
			{
				const Triangle::PointIndex& Indices = Face.Indices[i];
				const uint64_t uHash = Indices.GetHash();

				// skip vertices that have the same indices
				if (VertexIndices.count(uHash) > 0)
				{
					continue;
				}

				// write vertex position in byte stream
				VertexIndices[uHash] = uStreamIndex++;

				// write vertex position
				if (bWriteVertexPos)
				{
					if (Indices.uVertexIndex < uVertices)
					{
						const Math::float3& vVertex = Model.Vertices[Indices.uVertexIndex];
						VertexOut << vVertex;
					}
					else
					{
						HERROR("Invalid vertex index %u", Indices.uVertexIndex);
						return false;
					}
				}

				// write normal
				if (bWriteNormals)
				{
					if (Indices.uNormalIndex < uNormals)
					{
						VertexOut << Model.Normals[Indices.uNormalIndex];
					}
					else if(uNormals == 0)
					{
						VertexOut << vGeneratedNormal;
					}
					else
					{
						HERROR("Invalid normal index %u", Indices.uNormalIndex);
						return false;
					}
				}				

				// write tangent
				if (bGenerateTangents)
				{
					VertexOut << vGeneratedTangent;
				}

				// write tangent
				if (bGenerateBitangents)
				{
					VertexOut << vGeneratedBitangent;
				}

				// write uv
				if (bWriteUV)
				{
					if (Indices.uUVIndex < uUVs)
					{
						VertexOut << Model.TexCoords[Indices.uUVIndex];
					}
					else
					{
						HERROR("Invalid UV index %u", Indices.uUVIndex);
						return false;
					}
				}
			}
		}

		// number of generated ouput vertices
		Mesh.uVertexCount = static_cast<uint32_t>(uStreamIndex);
		uTotalOutputVertexBufferSize += Mesh.uVertexCount * uOutputVertexStrideSize;

		_H3DOut.m_MeshCluster.push_back(Mesh);

		HLOG("%s: generated %d vertices %d indices", CSTR(Model.sName), Mesh.uVertexCount, Mesh.uIndexCount);
	}

	//---------------------------------------------------------------------------------------------------
	// Write Index data:
	//---------------------------------------------------------------------------------------------------

	// select the maximal index buffer format to ensure continues mesh rendering
	// more than 2^16 vertices -> R32
	const Display::PixelFormat kIndexBufferFormat = uMaxIndexCount > 0xffff ? Display::PixelFormat_R32_UInt : Display::PixelFormat_R16_UInt;
	const uint32_t uIndexElementSize = (kIndexBufferFormat == Display::PixelFormat_R32_UInt ? sizeof(uint32_t) : sizeof(uint16_t));

	_H3DOut.m_IndexData.resize(0);
	hlx::bytestream IndexOut(_H3DOut.m_IndexData);

	for (uint32_t m = 0; m < uModel; ++m)
	{
		Resources::H3DHeader::SubMesh& Mesh = _H3DOut.m_MeshCluster[m];

		Mesh.kIndexBufferFormat = kIndexBufferFormat;
		Mesh.uIndexElementSize = uIndexElementSize;
		Mesh.uIndexByteOffset = uTotalOutputIndexBufferSize;
		uTotalOutputIndexBufferSize += Mesh.uIndexCount * uIndexElementSize;

		std::unordered_map<uint64_t, uint32_t>& VertexIndices = GeneratedVertexIndices[m];

		for (const Triangle& Face : InputModels[m].Faces)
		{
			for (int32_t i = 0; i < 3; ++i)
			{
				const Triangle::PointIndex& Indices = Face.Indices[_bReverseWinding ? 2 - i : i];
				const uint64_t uHash = Indices.GetHash();

				const uint32_t& uIndex = VertexIndices[uHash];

				if (kIndexBufferFormat == Display::PixelFormat_R32_UInt)
				{
					IndexOut << uIndex;
				}
				else
				{
					IndexOut << static_cast<uint16_t>(uIndex);
				}
			}
		}
	}

	if (_H3DOut.m_Header.kFlags & Resources::H3DFlag_PhysxMesh)
	{
		_H3DOut.m_Header.uPhysXDataOffset = _H3DOut.m_Header.uIndexDataOffset + uTotalOutputIndexBufferSize;
		_H3DOut.m_Header.uPhysXDataSize = 0u;
		ConvexMeshCooking& MeshCook = *ConvexMeshCooking::Instance();
		
		uint32_t uPhysXOffset = _H3DOut.m_Header.uPhysXDataOffset;

		for (uint32_t m = 0; m < uModel; ++m)
		{
			Resources::H3DHeader::SubMesh& Mesh = _H3DOut.m_MeshCluster[m];
			const OBJModel& Model = InputModels[m];

			HLOG("Generating ConvexMesh %s", CSTR(Mesh.sSubName));
			uint32_t uReachedVertexLimit = 0;
			hlx::bytes CookedMesh = MeshCook.GenerateConvexMesh(Model.Vertices, static_cast<uint32_t>(Model.Vertices.size() / 5), uReachedVertexLimit);

			if (CookedMesh.empty() == false)
			{
				Mesh.uConvexPhysxMeshOffset = uPhysXOffset;
				Mesh.uConvexPhysxMeshSize = static_cast<uint32_t>(CookedMesh.size());

				_H3DOut.m_Header.uPhysXDataSize += Mesh.uConvexPhysxMeshSize;
				uPhysXOffset += static_cast<uint32_t>(CookedMesh.size());

				PhysxOut << CookedMesh;

				HLOG("Generated ConvexMesh with %d vertices", uReachedVertexLimit);
			}
			else
			{
				HERROR("Failed to generate ConvexMesh %s", CSTR(Mesh.sSubName));
			}
		}
	}

	// fill remaining header fields
	_H3DOut.m_Header.uVertexDataOffset = _H3DOut.m_Header.GetSizeOnDisk(_kVersion);
	_H3DOut.m_Header.uVertexDataSize = uTotalOutputVertexBufferSize;
	_H3DOut.m_Header.uIndexDataOffset = _H3DOut.m_Header.uVertexDataOffset + uTotalOutputVertexBufferSize;
	_H3DOut.m_Header.uIndexDataSize = uTotalOutputIndexBufferSize;
	_H3DOut.m_Header.uClusterCount = uModel;

	return true;
}
//---------------------------------------------------------------------------------------------------------------------

bool OBJFile::ValidateIndices(const OBJModel& _Model, const Triangle& _Face) const
{
	for (uint32_t i = 0; i < 3; ++i)
	{
		const Triangle::PointIndex& Indices = _Face.Indices[i];

		if (Indices.uVertexIndex >= _Model.Vertices.size())
		{
			HERROR("Invalid vertex index %u", Indices.uVertexIndex);
			return false;
		}

		if (Indices.uNormalIndex >= _Model.Normals.size())
		{
			HERROR("Invalid normal index %u", Indices.uNormalIndex);
			return false;
		}

		if (Indices.uUVIndex >= _Model.TexCoords.size())
		{
			HERROR("Invalid UV index %u", Indices.uUVIndex);
			return false;
		}
	}

	return true;
}
//---------------------------------------------------------------------------------------------------------------------
