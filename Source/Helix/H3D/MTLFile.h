//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher, David Labode
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef MTLFILE_H
#define MTLFILE_H

#include <vector>
#include "Math\MathTypes.h"
#include "Util\HelixDefines.h"
#include "Resources\HMATFile.h"

namespace Helix
{
	namespace H3D
	{
		class MTLFile
		{
		public:
			HDEBUGNAME("MTLFile");

			MTLFile(const stdrzr::string& _sFilePath);

			bool Load(const std::string& _MTLContent);

			const std::vector<Resources::Material>& GetMaterials() const;

		private:
			void ReadMapData(std::stringstream& _Stream, _Out_ Resources::TextureMap& _CurMap);

		private:
			stdrzr::string m_sFilePath;
			stdrzr::string m_sFileName;

			std::vector<Resources::Material> m_Materials;
		};

		inline const std::vector<Resources::Material>& MTLFile::GetMaterials() const {	return m_Materials;	}

	} // H3DTool

} // Helix

#endif