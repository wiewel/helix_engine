//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher, David Labode
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "AssetImporter.h"
#include "hlx\src\StringHelpers.h"
#include "Resources\FileSystem.h"
#include "TextureImporter.h"
#include <unordered_set>

using namespace Helix::H3D;
using namespace Helix::Resources;
using namespace Helix::Display;

AssetImporter::AssetImporter(const hlx::string& _sWorkingDir)
{
	if (_sWorkingDir.empty() == false)
	{
		if (FileSystem::Instance()->IsInitialized() == false)
		{
			FileSystem::Instance()->Initialize(_sWorkingDir, {}, false);
		}
	}
}
//---------------------------------------------------------------------------------------------------

AssetImporter::~AssetImporter()
{
}

//---------------------------------------------------------------------------------------------------

void AssetImporter::Execute()
{
	if (FileSystem::Instance()->IsInitialized())
	{
		if (m_sHMATOutDir.empty())
		{
			m_sHMATOutDir = FileSystem::Instance()->GetKnownDirectoryPath(HelixDirectories_Materials);
		}
		if (m_sH3DOutDir.empty())
		{
			m_sH3DOutDir = FileSystem::Instance()->GetKnownDirectoryPath(HelixDirectories_Models);
		}
		if (m_sTextOutDir.empty())
		{
			m_sTextOutDir = FileSystem::Instance()->GetKnownDirectoryPath(HelixDirectories_Textures);
		}
	}

	if (m_bCreateConvexHull)
	{
		m_kComponentFlag |= Resources::H3DFlag_PhysxMesh;
	}
	else
	{
		m_kComponentFlag.ClearFlag(Resources::H3DFlag_PhysxMesh);
	}

	// todo: report other stuff

	for (const hlx::string& sFile : m_FilesToImport)
	{
		hlx::string sExt = hlx::to_lower(hlx::get_right_of(sFile, S(".")));

		bool bOBJ = sExt == S("obj");
		bool bFBX = sExt == S("fbx");

		if (bFBX)
		{
			ImportFBX(sFile);
		}
		else if (bOBJ)
		{
			ImportOBJ(sFile);
		}
		else
		{
			ImportTextureByName(sFile);
		}
	}

	m_FilesToImport.resize(0);

	m_UpdateFunction(S("Import finished"), 1, 0, 1);
}
//---------------------------------------------------------------------------------------------------

void AssetImporter::ImportFBX(const hlx::string& _PathToFBX)
{
	FbxSDKFactory FbxSDK;

	hlx::string sFBXName = FileSystem::GetFileName(_PathToFBX);
	m_UpdateFunction(S("Opening ") + sFBXName, m_iProgressValue++, 0, m_iProgressMax + 5);
	FbxNode* pNode = FbxSDK.Initialize(_PathToFBX, m_fScale, m_kCoordSytem);

	if (pNode == nullptr)
	{
		return;
	}

	H3DSceneNode SceneNode(pNode);

	m_UpdateFunction(S("Triangulating ") + sFBXName, m_iProgressValue++, 0, m_iProgressMax+5);
	if (SceneNode.Triangulate(true, m_bReverseWinding, m_bToLefthanded, m_bFlipV, m_bReversePivot, m_bNullPivot) == false)
	{
		return;
	}

	m_UpdateFunction(S("Assembling ") + sFBXName, m_iProgressValue++, 0, m_iProgressMax + 5);
	if (SceneNode.Assemble(m_kVertexLayout, m_kComponentFlag, true) == false)
	{
		return;
	}

	Resources::H3DFile H3D;
	H3D.SetName(hlx::to_sstring(Resources::FileSystem::GetFileNameWithoutExtension(_PathToFBX)));

	// merge fbx scene into h3d
	m_UpdateFunction(S("Saving ") + sFBXName, m_iProgressValue++, 0, m_iProgressMax + 5);
	SceneNode.Save(H3D, m_kComponentFlag, m_kTargetVersion, true);

	hlx::bytes OutputBuffer;
	hlx::bytestream OutputStream(OutputBuffer);

	// save h3d to stream
	m_UpdateFunction(S("Writing ") + sFBXName, m_iProgressValue++, 0, m_iProgressMax += 5);
	if (H3D.Save(OutputStream) == false)
	{
		HERROR("Failed to create H3D file");
		return;
	}

	// Save to file
	{
		hlx::fbytestream OutputH3DFile(m_sH3DOutDir + hlx::to_string(H3D.GetName()) + S(".h3d"), std::ios::out | std::ios::binary);
		if (OutputH3DFile.is_open() == false)
		{
			HERROR("Failed to open output file %s", OutputH3DFile.GetFileName().c_str());
			return;
		}

		hlx::string sName = Resources::FileSystem::GetFileName(OutputH3DFile.GetFileName());

		HLOG("Writing %s [%.2fkb]", sName.c_str(), (float)OutputBuffer.size() / 1024.f);

		OutputH3DFile.put<hlx::bytes>(OutputBuffer);
		OutputH3DFile.close();

		if (FileSystem::Instance()->IsInitialized())
		{
			FileSystem::Instance()->AddFileToIndex(HelixDirectories_Models, OutputH3DFile.GetFileName());
		}
	}

	if (m_bImportMaterials == false && m_bImportTextures == false)
	{
		return;
	}

	//-----------------------------------------------------------------------------------------------
	// Export Materials
	//-----------------------------------------------------------------------------------------------

	hlx::string sFBMFolder(_PathToFBX);
	hlx::replace_extension(sFBMFolder, S("fbm"));
	std::vector<hlx::string> TexFiles = FileSystem::GetFilesFromDirectory(sFBMFolder, S(".jpg|.png|.tga|.dds"));

	std::vector<H3DSceneNode*> ChildNodes;
	SceneNode.GetChildNodes(ChildNodes, true);

	std::unordered_set<uint64_t> ExtractedMaterials;
	std::unordered_set<uint64_t> ExtractedProperties;

	if (m_bImportMaterials)
	{
		for (H3DSceneNode* pNode : ChildNodes)
		{
			if (pNode->HasMaterials() == false || pNode->GetMaterialData() == false)
			{
				continue;
			}

			for (const Resources::HMat& Material : pNode->GetMaterials())
			{
				// hmat has already been extracted
				if (ExtractedMaterials.insert(Material.Key()).second == false)
				{
					continue;
				}

				hlx::string sMatPath = m_sHMATOutDir + STR(Material.sName + ".hmat");

				m_UpdateFunction(STR("Writing " + Material.sName + ".hmat"), m_iProgressValue++, 0, m_iProgressMax++);
				Resources::HMATFile HMat(Material);

				HLOG("Writing %s", CSTR(Material.sName + ".hmat"));
				OutputStream.clear();
				HMat.Write(OutputStream);

				// Save to file
				{
					hlx::fbytestream OutputHMatFile(sMatPath, std::ios::out | std::ios::binary);
					if (OutputHMatFile.is_open() == false)
					{
						HWARNING("Failed to open output file %s", CSTR(OutputHMatFile.GetFileName()));
						continue;
					}

					OutputHMatFile.put<hlx::bytes>(OutputBuffer);
					OutputHMatFile.close();
					OutputStream.clear();

					if (FileSystem::Instance()->IsInitialized())
					{
						FileSystem::Instance()->AddFileToIndex(HelixDirectories_Materials, sMatPath);
					}
				}

				if (m_bImportTextures)
				{
					auto ImportFn = [&](const std::string& _sPath, PixelFormat _kFormat)
					{
						auto it = std::find_if(TexFiles.begin(), TexFiles.end(), [&](const hlx::string& _sFullPath)->bool {return hlx::contains(_sFullPath, STR(_sPath)); });
						if (it != TexFiles.end())
						{
							ImportTexture(*it, _kFormat);
							TexFiles.erase(it);
						}
					};

					if (Material.sMapAlbedo.empty() == false)
					{
						ImportFn(Material.sMapAlbedo, PixelFormat_BC3_UNorm_SRgb);
					}
					if (Material.sMapNormal.empty() == false)
					{
						ImportFn(Material.sMapNormal, PixelFormat_BC1_UNorm);
					}
					if (Material.sMapHeight.empty() == false)
					{
						ImportFn(Material.sMapHeight, PixelFormat_BC4_UNorm);
					}
					if (Material.sMapRoughness.empty() == false)
					{
						ImportFn(Material.sMapRoughness, PixelFormat_BC4_UNorm);
					}
					if (Material.sMapMetallic.empty() == false)
					{
						ImportFn(Material.sMapMetallic, PixelFormat_BC4_UNorm);
					}
				}
			}

			for (const Resources::HMatProps& Prop : pNode->GetMaterialProperties())
			{				
				// hmatprop has already been extracted
				uint64_t uExportHash = Prop.Hash();
				if (ExtractedProperties.insert(uExportHash).second == false)
				{
					continue;
				}

				hlx::string sPropName = STR(Prop.sHMatName + "_" + std::to_string(uExportHash) + ".hmatprop");
				hlx::string sMatPath = m_sHMATOutDir + sPropName;

				HLOG("Writing %s", CSTR(sPropName));
				m_UpdateFunction(STR("Writing ") + sPropName, m_iProgressValue++, 0, m_iProgressMax++);
				Resources::HMATPROPFile HMatProp(Prop);

				OutputStream.clear();
				HMatProp.Write(OutputStream);

				// Save to file
				{
					hlx::fbytestream OutputHMatPropFile(sMatPath, std::ios::out | std::ios::binary);
					if (OutputHMatPropFile.is_open() == false)
					{
						HWARNING("Failed to open output file %s", CSTR(OutputHMatPropFile.GetFileName()));
						continue;
					}

					OutputHMatPropFile.put<hlx::bytes>(OutputBuffer);
					OutputHMatPropFile.close();
					OutputStream.clear();

					if (FileSystem::Instance()->IsInitialized())
					{
						FileSystem::Instance()->AddFileToIndex(HelixDirectories_MaterialProperties, sMatPath);
					}
				}
			}
		}
	}

	// convert remaining textures by name
	if (m_bImportTextures)
	{
		for (const hlx::string& sTex : TexFiles)
		{
			ImportTextureByName(sTex);
		}
	}

	if (FileSystem::PathExists(sFBMFolder))
	{
		for (const hlx::string& sTex : FileSystem::GetFilesFromDirectory(sFBMFolder))
		{
			DeleteFile(sTex.c_str());
		}
		RemoveDirectory(sFBMFolder.c_str());
	}
}

//---------------------------------------------------------------------------------------------------

void AssetImporter::ImportOBJ(const hlx::string& _PathToOBJ)
{
	HERROR("Obj format not supported!");
}
//---------------------------------------------------------------------------------------------------

void AssetImporter::ImportTexture(const hlx::string& _PathToImage, PixelFormat _kTargetFormat)
{
	hlx::string sFileName = FileSystem::GetFileNameWithoutExtension(_PathToImage) + S(".dds");
	m_UpdateFunction(S("Writing ") + sFileName, m_iProgressValue++, 0, m_iProgressMax++);

	hlx::string sOutPath = m_sTextOutDir + sFileName;

	if (TextureImporter::Convert(hlx::to_wstring(_PathToImage), hlx::to_wstring(sOutPath), _kTargetFormat, m_uMipLevel))
	{
		if (FileSystem::Instance()->IsInitialized())
		{
			FileSystem::Instance()->AddFileToIndex(HelixDirectories_Textures, sOutPath);
		}
	}
	else
	{
		HERROR("Failed to convert %s", _PathToImage.c_str());
	}
}
//---------------------------------------------------------------------------------------------------

void AssetImporter::ImportTextureByName(const hlx::string& _PathToImage)
{
	PixelFormat kTargetFormat = PixelFormat_BC3_UNorm_SRgb;

	hlx::string sFile = hlx::get_left_of(_PathToImage, S("."), true, true);

	if (hlx::ends_with(sFile, S("albedo"), false))
	{
		kTargetFormat = PixelFormat_BC3_UNorm_SRgb;
	}
	else if (hlx::ends_with(sFile, S("normal"), false))
	{
		kTargetFormat = PixelFormat_BC1_UNorm;
	}
	else if (hlx::ends_with(sFile, S("height"), false) ||
		hlx::ends_with(sFile, S("metallic"), false) ||
		hlx::ends_with(sFile, S("roughness"), false))
	{
		kTargetFormat = PixelFormat_BC4_UNorm;
	}
	else if (hlx::ends_with(sFile, S("hdr"), false))
	{
		kTargetFormat = PixelFormat_BC6H_UF16;
	}

	ImportTexture(_PathToImage, kTargetFormat);
}
//---------------------------------------------------------------------------------------------------
