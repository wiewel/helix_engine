//   Copyright 2020 Fabian Wahlster
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "TextureImporter.h"
#include "DirectXTex\DirectXTex\DirectXTex.h"
#include "hlx\src\StringHelpers.h"

using namespace Helix::H3D;
using namespace Helix::Display;
using namespace DirectX;

inline static bool ispow2(size_t x)
{
	return ((x != 0) && !(x & (x - 1)));
}

//---------------------------------------------------------------------------------------------------

bool TextureImporter::Convert(
	const std::wstring& _sInPath,
	const std::wstring& _sOutPath,
	PixelFormat _kTargetFormat,
	uint32_t _uMipLevels)
{
	DXGI_FORMAT kTargetFormat = static_cast<DXGI_FORMAT>(_kTargetFormat);

	HRESULT hr = E_FAIL;
	TexMetadata InInfo;
	std::shared_ptr<ScratchImage> pInImage(HNEW ScratchImage);

	HLOG("Converting %s [%d]", CSTR(_sInPath), kTargetFormat);

	// load image
	if (hlx::ends_with(_sInPath, L".dds", false))
	{
		HRIF(hr = LoadFromDDSFile(_sInPath.c_str(), 0, &InInfo, *pInImage))
		{
			return false;
		}
	}
	else if (hlx::ends_with(_sInPath, L".tga", false))
	{
		HRIF(hr = LoadFromTGAFile(_sInPath.c_str(), &InInfo, *pInImage))
		{
			return false;
		}
	}
	else
	{
		// needed for WIC
		hr = CoInitializeEx(nullptr, COINIT_MULTITHREADED);

		if (hr != S_OK && hr != S_FALSE && hr != RPC_E_CHANGED_MODE)
		{
			HERROR("Failed to initialize COM");
			return false;
		}

		DWORD wicFlags = WIC_FLAGS_ALL_FRAMES;

		HRIF(hr = LoadFromWICFile(_sInPath.c_str(), wicFlags, &InInfo, *pInImage))
		{
			return false;
		}
	}

	std::shared_ptr<ScratchImage> pOutImage = nullptr;


	if (InInfo.format == kTargetFormat && _uMipLevels == 1u)
	{
		pOutImage = pInImage;
	}
	else
	{
		pOutImage = std::shared_ptr<ScratchImage>(HNEW ScratchImage);
		std::shared_ptr<ScratchImage> pTemp(HNEW ScratchImage);

		// in format is compressed
		if (IsCompressed(InInfo.format))
		{
			HRIF(hr = Decompress(pInImage->GetImage(0, 0, 0), pInImage->GetImageCount(), InInfo, DXGI_FORMAT_UNKNOWN, *pTemp))
			{
				return false;
			}

			pInImage.swap(pTemp);
			InInfo = pInImage->GetMetadata();
		}

		// target is not compressed, try to convert
		if(IsCompressed(kTargetFormat) == false)
		{		
			DWORD dwFilter = TEX_FILTER_DEFAULT;
			HRIF(hr = DirectX::Convert(pInImage->GetImages(), pInImage->GetImageCount(), InInfo, kTargetFormat, dwFilter, 0.5f, *pTemp))
			{
				return false;
			}

			pInImage.swap(pTemp);
			InInfo = pInImage->GetMetadata();
		}

		if (_uMipLevels != 1u && InInfo.mipLevels != _uMipLevels)
		{
			if (ispow2(InInfo.width) == false || ispow2(InInfo.height) == false)
			{
				HERROR("Input [%dx%d] resulition is not a power of 2 which is required for mip generation!", InInfo.width, InInfo.height);
				return false;
			}

			std::unique_ptr<ScratchImage> pSliceImage(HNEW ScratchImage);

			TexMetadata SliceInfo = InInfo;
			SliceInfo.mipLevels = 1u;

			// extract highest res slice
			HRIF(hr = pSliceImage->Initialize(SliceInfo))
			{
				return false;
			}

			// 3d mips not supported for now
			if (InInfo.dimension != TEX_DIMENSION_TEXTURE3D)
			{
				for (size_t i = 0; i < InInfo.arraySize; ++i)
				{
					hr = CopyRectangle(
						*pInImage->GetImage(0, i, 0),
						Rect(0, 0, InInfo.width, InInfo.height),
						*pSliceImage->GetImage(0, i, 0),
						TEX_FILTER_DEFAULT, 0, 0);

					HRIF(hr)
					{
						HERROR("FAILED [copy to single level]");
						return false;
					}
				}
			}

			HRIF(GenerateMipMaps(
				pSliceImage->GetImages(),
				pSliceImage->GetImageCount(),
				pSliceImage->GetMetadata(),
				TEX_FILTER_DEFAULT,
				_uMipLevels,
				*pTemp))
			{
				HERROR("Failed to generate Mips!");
				return false;
			}

			pInImage.swap(pTemp);
			InInfo = pInImage->GetMetadata();
		}
		
		// target format is compressed
		if(IsCompressed(kTargetFormat))
		{
			if (InInfo.height % 4u != 0u || InInfo.width % 4u != 0u)
			{
				HERROR("Input [%dx%d] resulition is not a multiple of 4 which is required for block compression!", InInfo.width, InInfo.height);
				return false;
			}

			// if the input format type is IsSRGB(), then SRGB_IN is on by default
			// if the output format type is IsSRGB(), then SRGB_OUT is on by default

			DWORD dwCompress = TEX_COMPRESS_DEFAULT;
			{
				dwCompress |= TEX_COMPRESS_PARALLEL;
				hr = Compress(pInImage->GetImages(), pInImage->GetImageCount(), InInfo, kTargetFormat, dwCompress, 0.5f, *pTemp);
			}

			HRIF(hr)
			{
				HERROR("Failed to compress image");
				return false;
			}

			pInImage.swap(pTemp);
			InInfo = pInImage->GetMetadata();
		}

		pOutImage.swap(pInImage);
		InInfo = pOutImage->GetMetadata();
	}

	if (pOutImage != nullptr)
	{
		HRIF(hr = SaveToDDSFile(pOutImage->GetImage(0, 0, 0), pOutImage->GetImageCount(), pOutImage->GetMetadata(), DDS_FLAGS_NONE, _sOutPath.c_str()))
		{
			return false;
		}
	}

	return true;
}
//---------------------------------------------------------------------------------------------------
