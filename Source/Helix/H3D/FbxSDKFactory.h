//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher, David Labode
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef FBXSDKFACTORY_H
#define FBXSDKFACTORY_H

#include "fbxsdk.h"
#include "hlx\src\Logger.h"

namespace Helix
{
	namespace H3D
	{
		enum CoordinateSystem
		{
			CoordinateSystem_DirectX = 0,
			CoordinateSystem_OpenGL,
			CoordinateSystem_Max,
			CoordinateSystem_MayaZUp,
			CoordinateSystem_MayaYUp,
			CoordinateSystem_MotionBuilder,
			CoordinateSystem_Lightwave,
			CoordinateSystem_Unknown
		};

		class FbxSDKFactory
		{
		public:
			HDEBUGNAME("FbxSDKFactory");
			FbxSDKFactory();
			~FbxSDKFactory();

			// fbx unit to centimeter -> helix unit 1 = 1m => fbx to helix = 0.01
			FbxNode* Initialize(const hlx::string& _sFilePath, const double& _fScaleFactor = 0.01f, const CoordinateSystem& _TargetCoordSystem = CoordinateSystem_DirectX);

		private:
			FbxManager* m_pSDKManager = nullptr;
			FbxImporter* m_pImporter = nullptr;
			FbxScene* m_pScene = nullptr;
			FbxIOSettings* m_pIOSettings = nullptr;
		};
	} // H3DTool
} // Helix

#endif // !FBXSDKFACTORY_H
