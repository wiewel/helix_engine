//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher, David Labode
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef CONVEXMESHCOOKING_H
#define CONVEXMESHCOOKING_H

#include "foundation\PxFoundation.h"
#include "cooking\PxCooking.h"
#include "PxPhysics.h"
#include "hlx\src\Bytes.h"
#include "Math\MathTypes.h"
#include "PxPhysicsAPI.h"
#include "hlx\src\Singleton.h"
#include "extensions/PxDefaultStreams.h"
#include "Physics\PhysXByteStream.h"
#include "hlx\src\Logger.h"
#include <vector>

namespace Helix
{
	namespace H3D
	{

		class ConvexMeshCooking : public hlx::Singleton<ConvexMeshCooking>
		{
		public:
			ConvexMeshCooking();
			~ConvexMeshCooking();

			template <class Vector3>
			hlx::bytes GenerateConvexMesh(const std::vector<Vector3>& _Vertices, const uint32_t _uVertexLimit, _Out_ uint32_t& _uVertexLimitOut);

		private:
			physx::PxFoundation* m_pFoundation = nullptr;
			physx::PxPhysics* m_pPhysics = nullptr;
			physx::PxCooking* m_pCookingSDK = nullptr;
		};

		//---------------------------------------------------------------------------------------------------
		template <class Vector3>
		inline hlx::bytes ConvexMeshCooking::GenerateConvexMesh(const std::vector<Vector3>& _Vertices, const uint32_t _uVertexLimit, _Out_ uint32_t& _uVertexLimitOut)
		{
			using namespace physx;

			static_assert(sizeof(Vector3) == sizeof(Math::float3), "Invalid Vertex Format");

			PxConvexMeshDesc convexDesc;
			convexDesc.points.count = static_cast<uint32_t>(_Vertices.size());
			convexDesc.points.stride = sizeof(Vector3);
			convexDesc.points.data = _Vertices.data();
			convexDesc.flags = PxConvexFlag::eCOMPUTE_CONVEX | PxConvexFlag::eINFLATE_CONVEX;
			convexDesc.vertexLimit = std::max(std::min(_uVertexLimit, 256u), 4u);

			uint32_t uVertexLimit = convexDesc.vertexLimit;

			hlx::bytes CookedMesh;

			if (m_pCookingSDK == nullptr || m_pPhysics == nullptr)
			{
				HERROR("PhysxCookingSDK has not been initialized!");
				return CookedMesh;
			}

			{
				PxDefaultMemoryOutputStream buf;
				if (m_pCookingSDK->cookConvexMesh(convexDesc, buf))
				{
					CookedMesh = hlx::bytes(buf.getData(), buf.getSize());
				}
				else
				{
					convexDesc.vertexLimit = std::min(256u, convexDesc.points.count); // relax limit
					PxDefaultMemoryOutputStream buf2;
					if (m_pCookingSDK->cookConvexMesh(convexDesc, buf2))
					{
						CookedMesh = hlx::bytes(buf2.getData(), buf2.getSize());
					}
					else
					{
						return CookedMesh;
					}			
				}
			}

			Physics::PhysXInputStream input(CookedMesh);
			PxConvexMesh* pMesh = m_pPhysics->createConvexMesh(input);

			// try to shrink the mesh
			if (pMesh != nullptr)
			{
				bool bIterate = true;
				uint32_t uCurLimit = convexDesc.vertexLimit;
				while (bIterate)
				{
					_uVertexLimitOut = pMesh->getNbVertices();

					if (pMesh->getNbVertices() > uVertexLimit)
					{
						if (uCurLimit - (pMesh->getNbVertices() - uCurLimit) > 4)
						{
							convexDesc.vertexLimit = uCurLimit - (pMesh->getNbVertices() - uCurLimit);
						}
						else
						{
							// abort if convex mesh would be less than a tetrahedron
							pMesh->release();
							pMesh = nullptr;
							return CookedMesh;
						}

						uCurLimit = convexDesc.vertexLimit;

						pMesh->release();
						pMesh = nullptr;

						PxDefaultMemoryOutputStream buf2;
						if (m_pCookingSDK->cookConvexMesh(convexDesc, buf2))
						{
							PxDefaultMemoryInputData input2(buf2.getData(), buf2.getSize());
							pMesh = m_pPhysics->createConvexMesh(input2);

							if (pMesh == nullptr)
							{
								return CookedMesh;
							}
						}
						else
						{
							return CookedMesh;
						}

						CookedMesh = hlx::bytes(buf2.getData(), buf2.getSize());
					}
					else
					{
						return CookedMesh;
					}
				}
			}

			return CookedMesh;
		}
		//---------------------------------------------------------------------------------------------------

	} // H3DTool
} // Helix

#endif // !CONVEXMESHCOOKING_H
