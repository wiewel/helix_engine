//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher, David Labode
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "ConvexMeshCooking.h"
#include "Physics\PhysicsFactory.h"

using namespace Helix;
using namespace Helix::H3D;
using namespace physx;


ConvexMeshCooking::ConvexMeshCooking()
{
	Physics::PhysicsFactory::Instance()->Initialize();

	Physics::PhysicsFactory::Instance()->GetLogger().SetMaxErrorLevel(hlx::kMessageType_Warning);
	m_pFoundation = Physics::PhysicsFactory::Instance()->GetFoundation();

	HASSERT(m_pFoundation != nullptr, "Failed to initialize PhysxFoundation!");

	m_pCookingSDK = Physics::PhysicsFactory::Instance()->GetCooking();

	HASSERT(m_pCookingSDK != nullptr, "Failed to initialize CookingSDK!");

	m_pPhysics = Physics::PhysicsFactory::Instance()->GetPhysics();

	HASSERT(m_pPhysics != nullptr, "Failed to initialize PhysxPhysics!");
}
//---------------------------------------------------------------------------------------------------

ConvexMeshCooking::~ConvexMeshCooking()
{
}
//---------------------------------------------------------------------------------------------------


