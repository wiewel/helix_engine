//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher, David Labode
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef FBXSCENENODE_H
#define FBXSCENENODE_H

#include "FbxSDKFactory.h"
#include <vector>
#include "Math\MathTypes.h"
#include "Resources\H3DFile.h"
#include "Scene\LightDefines.h"
#include "Physics\GeometryDescriptions.h"
#include "Datastructures\GameObjectDescription.h"
#include "Resources\HMATFile.h"
#include "Resources\HMATPROPFile.h"

namespace Helix
{
	namespace H3D
	{
		struct UVSet
		{
			std::string sName;
			std::vector<Math::XMFLOAT2> TexCoords; //u,v
		};

		// use XMMATH types here to make it easier to use DirectXMesh functions

		struct H3DMeshNode
		{
			std::string sMasterName; // h3dfile name
			std::string sSubName; // mesh cluster name
			uint32_t uPolyCount;
			std::vector<Math::XMFLOAT3> Vertices; // v
			std::vector<Math::XMFLOAT3> Normals; //vn
			std::vector<Math::XMFLOAT2> TexCoords; //u,v

			inline void Clear()
			{
				//sName.clear();
				//uPolyCount = 0u;
				Vertices.clear();
				Normals.clear();
				TexCoords.clear();
			}
		};

		enum NodeType
		{
			NodeType_Mesh = 0,
			NodeType_Light = 1,
			//NodeType_Locator = 2,
			//NodeType_Material = 2,
			//NodeType_Camera = 3,
			NodeType_Unknown
		};

		// ShaderFX Maya
		enum CustomMaterial
		{
			CustomMaterial_Unkown = 0,
			CustomMaterial_StingrayPBS = 1166017,
		};

		class H3DSceneNode
		{
		public:
			HDEBUGNAME("H3DSceneNode");
			H3DSceneNode(FbxNode* _pNode, H3DSceneNode* _pParent = nullptr, bool _bRecursive = true);
			~H3DSceneNode();

			bool GetLightData();

			bool GetMaterialData();

			bool Triangulate(
				bool _bRecursive = true,
				bool _bReverseWinding = false,
				bool _bToLeftHanded = true,
				bool _bFlipV = false,
				bool _bReversePivot = false,
				bool _bNullPivot = false);

			bool Assemble(
				const Display::TVertexDeclaration& _kTargetDeclaration,
				Resources::TH3DFlags _kComponents = Resources::H3DFlag_None,
				bool _bRecursive = true,
				bool _bOptimize = false);

			bool Save(
				Resources::H3DFile& _OutH3D,
				Resources::TH3DFlags _kComponents,
				const Resources::H3DVersion _kVersion,
				bool _bRecursive = true,
				bool _bClearTemporaryData = true);

			// frees all temporary data
			void Clear();

			void GetChildNodesOfType(std::vector<H3DSceneNode*>& _ChildNodesOut, const NodeType& _kType, bool _bRecursive) const;

			void GetChildNodes(std::vector<H3DSceneNode*>& _ChildNodesOut, bool _bRecursive) const;
			void GetChildNodesWithMeshData(std::vector<H3DSceneNode*>& _ChildNodesOut, bool _bRecursive) const;
			void GetChildNodesWithAssembledMeshData(std::vector<H3DSceneNode*>& _ChildNodesOut, bool _bRecursive) const;
			const std::string& GetName() const;
			std::string GetFullName() const;

			const NodeType& GetType() const;

			const Scene::LightDesc& GetLight() const;
			const std::vector<Resources::HMat>& GetMaterials() const;
			const std::vector<Resources::HMatProps>& GetMaterialProperties() const;

			const Physics::GeometryDesc& GetShape() const;

			Math::transform GetGlobalTransform() const;
			Math::float3 GetGlobalScale() const;

			bool HasMaterials() const;

		private:
			bool LoadCustomMaterials(const FbxSurfaceMaterial* _pMaterial, Resources::HMat& _OutMat, Resources::HMatProps& _OutProps);

		private:
			const std::string m_sName;
			const bool m_bRootNode = false;
			FbxNode* m_pNode = nullptr;
			H3DSceneNode* m_pParent = nullptr;

			FbxNodeAttribute::EType m_kFbxType = FbxNodeAttribute::eUnknown;
			NodeType m_kNodeType = NodeType_Unknown;

			std::vector<H3DSceneNode*> m_ChildNodes;

			// Extracted Data
			Math::transform m_Transform = HTRANSFORM_IDENTITY;
			Math::float3 m_vScale = HFLOAT3_ONE;

			Scene::LightDesc m_Light;
			Physics::GeometryDesc m_QueryShape;
			Physics::GeometryDesc m_SimulationShape;

			std::vector<Resources::HMat> m_Materials;
			std::vector<Resources::HMatProps> m_MaterialProperties;

			H3DMeshNode m_Mesh;

			// Generated Data
			hlx::bytes m_IndexData;
			Display::PixelFormat m_kIndexBufferFormat = Display::PixelFormat_R16_UInt;

			hlx::bytes m_VertexData;
			uint32_t m_uGeneratedVertexCount = 0u;
			Display::TVertexDeclaration m_kTargetDeclaration = Display::VertexLayout_PosXYZNormTanTex;

			hlx::bytes m_PhysxData;
		};

		inline const std::string& H3DSceneNode::GetName() const { return m_sName; }
		inline std::string H3DSceneNode::GetFullName() const { return std::string(m_pNode->GetName()); }
		inline const NodeType& H3DSceneNode::GetType() const { return m_kNodeType; }

		inline const Scene::LightDesc& H3DSceneNode::GetLight() const{return m_Light;}
		inline const std::vector<Resources::HMat>& H3DSceneNode::GetMaterials() const{	return m_Materials;	}
		inline const std::vector<Resources::HMatProps>& H3DSceneNode::GetMaterialProperties() const	{return m_MaterialProperties;}

		inline const Physics::GeometryDesc& H3DSceneNode::GetShape() const{	return m_QueryShape;}

		inline bool H3DSceneNode::HasMaterials() const
		{
			return m_pNode->GetMaterialCount() > 0;
		}

	} // H3DTool
} // Helix

#endif // !FBXSCENENODE_H
