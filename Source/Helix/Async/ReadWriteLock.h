//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef READWRITELOCK_H
#define READWRITELOCK_H

#include <synchapi.h>

namespace Helix
{
	namespace Async
	{
		// The SimpleReadWriteLock only supports one thread for writing (single producer multiple consumer pattern)
		struct LockObject
		{
			friend struct ReadLock;
			friend struct WriteLock;

			constexpr LockObject() {}

			LockObject(const LockObject&) = delete;
			LockObject& operator=(const LockObject&) = delete;

		private:
			SRWLOCK m_SyncObj = SRWLOCK_INIT;
		};

		struct ReadLock
		{
			inline ReadLock(LockObject& _Lock) : m_Lock(_Lock)
			{
				AcquireSRWLockShared(&m_Lock.m_SyncObj);
			}

			inline ~ReadLock()
			{
				ReleaseSRWLockShared(&m_Lock.m_SyncObj);
			}
		private:
			LockObject& m_Lock;
		};

		struct WriteLock
		{
			inline WriteLock(LockObject& _Lock) : m_Lock(_Lock)
			{
				AcquireSRWLockExclusive(&m_Lock.m_SyncObj);
			}

			inline ~WriteLock()
			{
				ReleaseSRWLockExclusive(&m_Lock.m_SyncObj);
			}
		private:
			LockObject& m_Lock;
		};
	} // Async
} // Helix

#endif // !READWRITELOCK_H
