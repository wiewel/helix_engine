//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef DELEGATE_H
#define DELEGATE_H

#include "hlx\src\Logger.h"

namespace Helix
{
	namespace Async
	{
		//global function
		template <typename Result, typename ...Args>
		struct Delegate
		{
			constexpr Delegate(std::nullptr_t) noexcept : fn(nullptr) {}
			Delegate() : fn(nullptr) {}
			Delegate(Result(*_fn)(Args...)) : fn(_fn) {}
			Delegate(const Delegate& _Func) : fn(_Func.fn){}
			
			inline explicit operator bool() const {	return fn != nullptr; }
			inline Delegate& operator=(const Delegate& _Func) { fn = _Func.fn; return *this; }
			inline Delegate& operator=(Result(*_fn)(Args...)) { fn = _fn; return *this; }
			inline Delegate& operator=(std::nullptr_t) { fn = nullptr; return *this; }

			inline bool operator==(Result(*_fn)(Args...)) const { return fn == _fn; }
			inline bool operator==(const Delegate& _Func) const { return fn == _Func.fn; }
			inline bool operator!=(const Delegate& _Func) const { return fn != _Func.fn; }

			inline bool operator==(std::nullptr_t) const { return fn == nullptr; }
			inline bool operator!=(std::nullptr_t) const { return fn != nullptr; }

			virtual Result invoke(Args&&... args)
			{
				HASSERTD(this->operator bool(), "Invalid Function");
				return fn(std::forward<Args>(args)...);
			}

			virtual const Result invoke(Args&&... args) const
			{
				HASSERTD(this->operator bool(), "Invalid Function");
				return fn(std::forward<Args>(args)...);
			}

			inline Result operator()(Args&&... args)
			{
				return invoke(std::forward<Args>(args)...);
			}

			inline const Result operator()(Args&&... args) const
			{
				return invoke(std::forward<Args>(args)...);
			}

			inline Result exec_if_valid(Args&&... args)
			{
				if (fn != nullptr)
					return invoke(std::forward<Args>(args)...);

				return Result();
			}

		private:
			Result(*fn) (Args...) = nullptr;
		};

		//Object member function
		template<typename Obj, typename Result, typename ...Args>
		struct DelegateObj : public Delegate<Result, Args...>
		{
			using TBaseType = DelegateObj<Result, Args...>;

			constexpr DelegateObj(std::nullptr_t) noexcept : m_pObj(nullptr), fn(nullptr) {}
			DelegateObj() : m_pObj(nullptr), fn(nullptr) {}
			DelegateObj(Obj* _pObj, Result(Obj::*_fn)(Args...)) : m_pObj(_pObj), fn(_fn) {}
			DelegateObj(const DelegateObj& _Func) : m_pObj(_Func.m_pObj), fn(_Func.fn) {}

			inline explicit operator bool() const { return fn != nullptr && m_pObj != nullptr; }
			inline DelegateObj& operator=(const DelegateObj& _Func) { fn = _Func.fn; m_pObj = _Func.m_pObj; return *this; }
			inline DelegateObj& operator=(std::nullptr_t) { fn = nullptr; m_pObj = nullptr; return *this; }

			inline bool operator==(const DelegateObj& _Func) const { return fn == _Func.fn && m_pObj == _Func.m_pObj; }
			inline bool operator!=(const DelegateObj& _Func) const { return fn != _Func.fn || m_pObj == _Func.m_pObj; }

			inline bool operator==(std::nullptr_t) const { return m_pObj == nullptr || fn == nullptr; }
			inline bool operator!=(std::nullptr_t) const { return m_pObj != nullptr && fn != nullptr; }

			Result invoke(Args&&... args) final
			{
				HASSERTD(this->operator bool(), "Invalid Function");
				return (m_pObj->*fn)(std::forward<Args>(args)...);
			}

			const Result invoke(Args&&... args) const final
			{
				HASSERTD(this->operator bool(), "Invalid Function");
				return (m_pObj->*fn)(std::forward<Args>(args)...);
			}
			
		private:
			Result(Obj::*fn)(Args...) = nullptr;
			Obj* m_pObj = nullptr;
		};

		template<typename Result, typename ...Args>
		auto make_delegate(Result(*fn)(Args...)) -> Delegate<Result, Args...>
		{
			return Delegate<Result, Args...>(fn);
		}

		template<typename Obj, typename Result, typename ...Args>
		auto make_delegate(Result(Obj::*fn)(Args...), Obj* m_pObj) -> DelegateObj<Obj, Result, Args...>
		{
			return DelegateObj<Obj, Result, Args...>(m_pObj, fn);
		}

		template<typename Obj, typename Result, typename ...Args>
		auto make_const_delegate(Result(Obj::*fn)(Args...), const Obj* m_pObj) -> DelegateObj<const Obj, Result, Args...>
		{
			return DelegateObj<const Obj, Result, Args...>(m_pObj, fn);
		}

		//usage:
		//std::function<> fn = make_delegate(&Foo::bar, object) 
		//fn()

	} // Async
}// Helix

#endif