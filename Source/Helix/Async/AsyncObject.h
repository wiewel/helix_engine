//   Copyright 2020 Fabian Wahlster
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef ASYNCOBJECT_H
#define ASYNCOBJECT_H

#include <memory>

#include "MutableThread.h"
#include "hlx\src\Logger.h"
#include "hlx\src\StandardDefines.h"
#include <future>

namespace Helix
{
	namespace Async
	{
		class IAsyncObject
		{
		public:
			HDEBUGNAME("IAsyncObject");

			// all subsystems must be initialized in the constructor
			IAsyncObject(IAsyncObject* _pParent = nullptr, IAsyncObserver * _pObserver = nullptr, bool _bLoopedExecution = false, bool _bPauseAfterExecution = false);

			~IAsyncObject();

			//Start synchron without thread
			void Start(bool _bStartSuspended = true, bool _bStartSubsystemsFirst = true);

			//Starts bound function in the std::thread
			bool StartInNewThread(bool _bStartSuspended = true, bool _bStartSubsystemsFirst = true);

			//Starts bound function in MutableThread
			bool MoveToThread(MutableThread* thread);

			//Signals the loop function to stop execution, this will also stop all child objects
			void Stop();

			//Signals the loop function to continue execution
			void Continue();

			//Signals the loop function to pause execution
			void Pause(bool _bBlocking = false);

			//This will join the std::thread (not the MutableThread!)
			void StopThread();

			//Set new observer
			void SetObserver(IAsyncObserver * _Observer);

			//Get bound function
			const std::function<void()>& GetFunction() const;

			std::future<void> GetFuture(std::launch _kPolicy = std::launch::async) const;

			//Returns true if the execution is currently paused
			bool IsPaused() const;

			// Returns true if currently running
			bool IsRunning() const;
		private:
			//Repeatedly calls the Execute() function
			void ExecuteLooped();

		protected:
			//main work loop; has to be implemented by derived class
			virtual void Execute() = 0;

			// This function is called at the end of the Stop() function, the child class should implement this function
			// and stop all AsyncObjects that are members of the parent class.
			// This function can be empty if the parent object does not have any child AsyncObjects.
			//virtual void StopSubsystems() {};

			// This function is called before executing the bound function, it should set all child AsyncObjects observers and start them (in a thread or synchronous)
			// if they are not started in the Execute function itself.
			// This function can be empty if the parent object does not have any child AsyncObjects.
			//virtual void InitializeSubsystems() {};

			//Calls NotifyBarrier of Observer object
			void SignalObserverBarrier();

			//These functions can be called from within the derived Execute function:

			//Signals the loop function to stop execution
			void StopInternal();

			//Signals the loop function to continue execution
			void ContinueInternal();

			//Signals the loop function to pause execution
			void PauseInternal();

			const IAsyncObject* GetParent() const;

			const std::vector<IAsyncObject*>& GetChildren() const;

		private:
			//don't provide access to derived class
			std::unique_ptr<std::thread> m_StdThread;
			std::function<void()> m_Function; // TODO: make delegate

			std::vector<IAsyncObject*> m_ChildObjects;

			IAsyncObject* m_pParent = nullptr;
			IAsyncObserver* m_pObserver = nullptr;

			bool m_bPauseAfterExecution;

			std::mutex m_ObserverLockMutex;
			std::mutex m_ThreadLockMutex;
			std::mutex m_WaitMutex;

			bool m_bRun;
			bool m_bPause;
			
			std::condition_variable m_CV;
		};

		inline const IAsyncObject* IAsyncObject::GetParent() const { return m_pParent; }

		inline const std::vector<IAsyncObject*>& IAsyncObject::GetChildren() const { return m_ChildObjects; }

		inline const std::function<void()>& IAsyncObject::GetFunction() const { return m_Function; }

		inline std::future<void> IAsyncObject::GetFuture(std::launch _kPolicy) const
		{
			HASSERTD(m_StdThread != nullptr, "The function of this AsyncObject is already bound to a thread!");
			return std::async(_kPolicy, m_Function);
		}

		inline void IAsyncObject::SetObserver(IAsyncObserver* _pObserver) { std::lock_guard<std::mutex> lock(m_ObserverLockMutex); m_pObserver = _pObserver; }

		//TODO: protect with mutex
		inline bool IAsyncObject::IsPaused() const { return m_bPause; }

		//Returns true if the 
		inline bool IAsyncObject::IsRunning() const { return m_bRun; }

	}; // Namespace aync
}; // Namespace Helix

#endif