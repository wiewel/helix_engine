//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef SPINLOCK_H
#define SPINLOCK_H

#include <atomic>

namespace Helix
{
	namespace Async
	{
		struct SpinLock
		{
			friend struct ScopedSpinLock;

			constexpr SpinLock() {}
		public:
			inline void Lock()
			{
				while (m_InternalObject.test_and_set(std::memory_order_acquire))
				{
					int i = 0;
					++i;
				}
			}

			inline void Unlock()
			{
				m_InternalObject.clear(std::memory_order_release);
			}
		private:
			std::atomic_flag m_InternalObject = ATOMIC_FLAG_INIT;
		};

		struct ScopedSpinLock
		{
			inline ScopedSpinLock(SpinLock& _LockObject, bool _bLock = true) :
				m_LockObject(_LockObject), m_bLock(_bLock)
			{
				if (m_bLock)
				{				
					m_LockObject.Lock();
				}
			}

			~ScopedSpinLock()
			{
				if (m_bLock)
				{
					m_LockObject.Unlock();
				}
			}
		private:
			const bool m_bLock;
			SpinLock& m_LockObject;
		};

	} // Async
} // Helix


#endif // !SPINLOCK_H
