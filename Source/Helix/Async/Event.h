//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef EVENT_H
#define EVENT_H

#include "hlx\src\StandardDefines.h"
#include "Async\Delegate.h"
#include "Async\SpinLock.h"
#include <vector>
#include <future>
#include <chrono>

namespace Helix
{
	namespace Async
	{
		template<typename Ret, class... Args>
		class Event
		{
		public:
			typedef typename Async::Delegate<Ret, Args...> Callback;

			void AddListener(const Callback& _Fn);
			void RemoveListener(const Callback& _Fn);
			void Invoke(Args... _Args) const;
			void InvokeAfter(const std::chrono::duration<int64_t> _Duration, Args... _Args);
			const uint32_t GetListenerCount() const;

		private:
			void WaitAndInvoke(const std::chrono::duration<int64_t> _Duration, Args... _Args) const;

			std::future<void> m_TimerFuture;
			std::vector<Callback> m_Listeners;
			mutable SpinLock m_ListenersLock;
			mutable bool m_bWaiting = false;
		};
		//---------------------------------------------------------------------------------------------------------------------
		template<typename Ret, typename... Args>
		inline void Event<Ret, Args...>::AddListener(const Callback& _Fn)
		{
			ScopedSpinLock Lock(m_ListenersLock);
			m_Listeners.push_back(Callback(_Fn));
		}
		//---------------------------------------------------------------------------------------------------------------------
		template<typename Ret, typename... Args>
		inline void Event<Ret, Args...>::Invoke(Args... _Args) const
		{
			ScopedSpinLock Lock(m_ListenersLock);
			for (const Callback& Dg : m_Listeners)
			{
				if (Dg != nullptr)
				{
					Dg(_Args...);
				}
			}
		}
		//---------------------------------------------------------------------------------------------------------------------
		template<typename Ret, typename... Args>
		inline void Event<Ret, Args...>::RemoveListener(const Callback& _Fn)
		{
			ScopedSpinLock Lock(m_ListenersLock);
			auto foundIt = std::find(m_Listeners.begin(), m_Listeners.end(), _Fn);
			if (foundIt != m_Listeners.end())
			{
				m_Listeners.erase(foundIt);
			}
		}
		//---------------------------------------------------------------------------------------------------------------------
		template<typename Ret, class ...Args>
		inline void Event<Ret, Args...>::InvokeAfter(const std::chrono::duration<int64_t> _Duration, Args... _Args)
		{
			//ScopedSpinLock Lock(m_ListenersLock);
			if (m_bWaiting)
			{
				return;
			}
			m_bWaiting = true;
		
			m_TimerFuture = std::move(std::async(std::launch::async, &Event<Ret, Args...>::WaitAndInvoke, this, _Duration, _Args...));
		}
		//---------------------------------------------------------------------------------------------------------------------
		template<typename Ret, typename... Args>
		inline const uint32_t Event<Ret, Args...>::GetListenerCount() const
		{
			ScopedSpinLock Lock(m_ListenersLock);
			return static_cast<uint32_t>(m_Listeners.size());
		}
		//---------------------------------------------------------------------------------------------------------------------
		template<typename Ret, class ...Args>
		inline void Event<Ret, Args...>::WaitAndInvoke(const std::chrono::duration<int64_t> _Duration, Args... _Args) const
		{
			std::this_thread::sleep_for(_Duration);
			Invoke(_Args...);
			m_bWaiting = false;
		}
	}
}
#endif // !EVENT_H
