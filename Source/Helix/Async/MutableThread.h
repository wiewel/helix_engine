//   Copyright 2020 Fabian Wahlster
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef MUTABLETHREAD_H
#define MUTABLETHREAD_H

#include <thread>
#include <functional>
#include <mutex>
#include <condition_variable>
#include <system_error>

#include "AsyncObserver.h"
#include "hlx\src\Logger.h"

//isable unreferenced variable warning
#pragma warning( disable : 4101 )

namespace Helix
{
	namespace Async
	{
		class MutableThread
		{
		private:
			std::thread m_Thread;
			std::function<void()> m_Function;
			bool m_bNotified;
			bool m_bRun;
			std::mutex m_LockMutex;
			std::mutex m_WaitMutex;
			std::condition_variable m_CV;
			IAsyncTemplateObserver<MutableThread>* m_Observer = nullptr;

		private:
			void Execute()
			{
				while (m_bRun)
				{
					while (m_bNotified == false)
					{
						std::unique_lock<std::mutex> wait(m_WaitMutex);
						m_CV.wait(wait);
					}

					//reset notification state
					m_bNotified = false;

					bool bFinished = false;

					// Execute the function
					{
						std::lock_guard<std::mutex> lock(m_LockMutex);

						if (m_bRun && m_Function)
						{
							m_Function();
							m_Function = std::function<void()>();

							bFinished = true;
						}
					}
					
					//signal the observer / threadpool
					if (bFinished && m_Observer != nullptr)
						m_Observer->Signal(this);
				}
			}

		public:
			MutableThread(const MutableThread& thread) = delete;

			MutableThread(IAsyncTemplateObserver<MutableThread>* _Observer)
			{
				m_Observer = _Observer;
				m_bNotified = false;
				m_bRun = true;
				m_Thread = std::thread(&MutableThread::Execute, this);
			}

			MutableThread()
			{
				m_Observer = nullptr;
				m_bNotified = false;
				m_bRun = true;
				m_Thread = std::thread(&MutableThread::Execute, this);
			}

			~MutableThread()
			{
				m_bRun = false;

				m_bNotified = true;
				m_CV.notify_one();

				try
				{
					if (m_Thread.joinable())
						m_Thread.join();
				}
				catch (std::system_error& ex)
				{
					HWARNINGD("%s", ex.what());
				}
			}

			inline bool Start(const std::function<void()>& f)
			{
				std::lock_guard<std::mutex> lock(m_LockMutex);

				if (m_Function != nullptr)
					return false;

				m_Function = f;

				m_bNotified = true;
				m_CV.notify_one();

				return true;
			}

			inline bool isFree()
			{
				std::lock_guard<std::mutex> lock(m_LockMutex);
				return !m_Function;
			}

			inline std::thread::id getId()
			{
				//std::lock_guard<std::mutex> lock(m_LockMutex);
				return m_Thread.get_id();
			}

			inline bool Join()
			{
				std::lock_guard<std::mutex> lock(m_LockMutex);

				m_bRun = false;

				m_bNotified = true;
				m_CV.notify_one();

				try
				{
					if (m_Thread.joinable())
						m_Thread.join();
				}
				catch (std::system_error& ex)
				{
					HWARNINGD("%s", ex.what());
				}
			}
		};
	}; // Namespace Async
}; // Namepspace Helix

#endif