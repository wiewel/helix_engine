//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef ASYNCOBSERVER_H
#define ASYNCOBSERVER_H

#include <condition_variable>
#include <atomic>
#include "hlx\src\Logger.h"

namespace Helix
{
	namespace Async
	{
		class IAsyncObserver
		{
		private:
			std::condition_variable m_CV;
			std::mutex m_WaitMutex;
			bool m_bNotified;
			bool m_bUseBarrier;

			uint32_t m_uNumOfNotifications;
			uint32_t m_uNotificationCount;

		public:

			IAsyncObserver()
			{
				m_bUseBarrier = false;
				m_bNotified = false;
				m_uNumOfNotifications = 0u;
				m_uNotificationCount = 0u;
			}

			~IAsyncObserver()
			{
				{
					std::lock_guard<std::mutex> Lock(m_WaitMutex);
					m_bNotified = true;
				}

				m_CV.notify_one();

				if (m_bUseBarrier && m_uNotificationCount < m_uNumOfNotifications)
				{
					HWARNINGD("Barrier still active! Observer has not been signaled before destruction.");
				}
			}

			inline void SetBarrier(uint32_t _uNumOfNotifications = 1u)
			{
				std::lock_guard<std::mutex> Lock(m_WaitMutex);

				m_uNumOfNotifications = _uNumOfNotifications;
				m_uNotificationCount = 0u;
				m_bNotified = false;
				m_bUseBarrier = true;
			}

			inline void NotifyBarrier(bool _bAll = true)
			{
				if (m_bUseBarrier)
				{
					{
						std::lock_guard<std::mutex> Lock(m_WaitMutex);
						m_bNotified = ++m_uNotificationCount == m_uNumOfNotifications;
					}

					if (_bAll)
					{
						m_CV.notify_all();
					}
					else
					{
						m_CV.notify_one();
					}
				}
			}

			// _uDurationInMilliSec = 0 => wait infinitely long
			inline void WaitForNotifications(uint32_t _uDurationInMilliSec = 0u)
			{
				if (m_bUseBarrier)
				{
					std::unique_lock<std::mutex> Lock(m_WaitMutex);

					if (_uDurationInMilliSec == 0u)
					{
						while (m_bNotified == false)
						{
							m_CV.wait(Lock);
						}
					}
					else
					{
						auto end = std::chrono::system_clock::now() + std::chrono::milliseconds(_uDurationInMilliSec);
						while (m_bNotified == false)
						{
							m_CV.wait_until(Lock, end);
						}					
					}

					m_bNotified = false;
					m_bUseBarrier = false;

					m_uNotificationCount = 0u;
				}
			}
		}; // IAsyncObserver

		template <typename T>
		class IAsyncTemplateObserver : public IAsyncObserver
		{
		public:
			virtual void Signal(T* _Obj) = 0;
		};
	}; // Namespace Async

}; // Namespace Helix

#endif