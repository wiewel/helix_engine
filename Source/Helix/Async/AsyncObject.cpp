//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "AsyncObject.h"

using namespace Helix::Async;
//---------------------------------------------------------------------------------------------------
IAsyncObject::IAsyncObject(IAsyncObject* _pParent, IAsyncObserver * _pObserver, bool _bLoopedExecution, bool _bPauseAfterExecution) :
	m_bPauseAfterExecution(_bPauseAfterExecution),
	m_pParent(_pParent),
	m_pObserver(_pObserver)
{
	if (m_pParent != nullptr)
	{
		m_pParent->m_ChildObjects.push_back(this);
	}

	m_StdThread = nullptr;

	m_bRun = false;
	//Start in a suspended state
	m_bPause = true;

	if (_bLoopedExecution)
	{
		m_Function = std::bind(&IAsyncObject::ExecuteLooped, this);
	}
	else
	{
		m_Function = std::bind(&IAsyncObject::Execute, this);
	}
}
//---------------------------------------------------------------------------------------------------
IAsyncObject::~IAsyncObject()
{
	Stop();
}
//---------------------------------------------------------------------------------------------------
void IAsyncObject::StopThread()
{
	std::lock_guard<std::mutex> lock(m_ThreadLockMutex);
	if (m_StdThread != nullptr)
	{
		try
		{
			if (m_StdThread->joinable())
			{
				m_StdThread->join();
			}			

			m_StdThread.reset(nullptr);
		}
		catch (std::system_error& ex)
		{
			HWARNINGD("Failed to join thread!");
		}
	}
}
//---------------------------------------------------------------------------------------------------
bool IAsyncObject::MoveToThread(MutableThread* thread)
{
	return thread->Start(m_Function);
}
//---------------------------------------------------------------------------------------------------
void IAsyncObject::Start(bool _bStartSuspended, bool _bStartSubsystemsFirst)
{
	m_bRun = true;
	m_bPause = _bStartSuspended;

	if (_bStartSubsystemsFirst)	
	{
		for (IAsyncObject* pChild : m_ChildObjects)
		{
			pChild->Start();
		}
	}

	m_Function();

	if (_bStartSubsystemsFirst == false)
	{
		for (IAsyncObject* pChild : m_ChildObjects)
		{
			pChild->Start();
		}
	}
}
//---------------------------------------------------------------------------------------------------
bool IAsyncObject::StartInNewThread(bool _bStartSuspended, bool _bStartSubsystemsFirst)
{
	{
		std::lock_guard<std::mutex> lock(m_ThreadLockMutex);
		if (m_StdThread != nullptr)
			return false;

		m_bRun = true;
		m_bPause = _bStartSuspended;
	}
	
	if (_bStartSubsystemsFirst)
	{
		for (IAsyncObject* pChild : m_ChildObjects)
		{
			pChild->StartInNewThread();
		}
	}

	{
		std::lock_guard<std::mutex> lock(m_ThreadLockMutex);
		m_StdThread = std::make_unique<std::thread>(m_Function);
	}

	if (_bStartSubsystemsFirst == false)
	{
		for (IAsyncObject* pChild : m_ChildObjects)
		{
			pChild->StartInNewThread();
		}
	}
	
	return true;
}
//---------------------------------------------------------------------------------------------------
//main work loop;
void IAsyncObject::ExecuteLooped()
{
	while (m_bRun)
	{
		//suspend simulation
		{
			std::unique_lock<std::mutex> wait(m_WaitMutex);
			while (m_bPause)
			{
				m_CV.wait(wait);
			}
		}

		if (m_bRun == false)
		{
			//skip the last execution if Stop has been called during last wait
			SignalObserverBarrier();
			return;
		}

		//next execution
		Execute();

		if (m_bPauseAfterExecution)
		{
			std::lock_guard<std::mutex> wait(m_WaitMutex);
			m_bPause = true;
		}

		SignalObserverBarrier();
	}
}
//---------------------------------------------------------------------------------------------------
//call this function afte each simulation step
void IAsyncObject::SignalObserverBarrier()
{
	std::lock_guard<std::mutex> lock(m_ObserverLockMutex);
	if (m_pObserver != nullptr)
	{
		m_pObserver->NotifyBarrier();
	}
	//else
	//{
	//	//this can be desired behaviour - the warning is just here to inform the developer
	//	HWARNINGD("No observer has been assigned");
	//}
}
//---------------------------------------------------------------------------------------------------
void IAsyncObject::Stop()
{
	if (m_bRun)
	{		
		{
			std::lock_guard<std::mutex> lock(m_WaitMutex);
			m_bRun = false;
			m_bPause = false;
		}

		m_CV.notify_one();

		for (IAsyncObject* pChild : m_ChildObjects)
		{
			pChild->Stop();
		}
		//StopSubsystems();

		StopThread();
	}
}
//---------------------------------------------------------------------------------------------------
void IAsyncObject::Pause(bool _bBlocking)
{
	if (m_bPause == false && m_bRun)
	{
		IAsyncObserver Observer;
		{
			std::lock_guard<std::mutex> lock(m_WaitMutex);
			m_bPause = true;

			if (_bBlocking)
			{
				Observer.SetBarrier(1u + static_cast<uint32_t>(m_ChildObjects.size()));
				SetObserver(&Observer);
			}
		}

		//m_CV.notify_one(); // why?

		for (IAsyncObject* pChild : m_ChildObjects)
		{
			pChild->Pause();
		}

		if (_bBlocking)
		{
			Observer.WaitForNotifications();
			SetObserver(nullptr);
		}
	}	
}
//---------------------------------------------------------------------------------------------------
void IAsyncObject::Continue()
{
	if (m_bPause)
	{
		{
			std::lock_guard<std::mutex> lock(m_WaitMutex);
			m_bPause = false;
		}

		m_CV.notify_one();

		for (IAsyncObject* pChild : m_ChildObjects)
		{
			pChild->Continue();
		}
	}
}
//---------------------------------------------------------------------------------------------------
void IAsyncObject::StopInternal()
{
	if (m_bRun)
	{
		m_bRun = false;
		m_bPause = false;

		m_CV.notify_one();

		for (IAsyncObject* pChild : m_ChildObjects)
		{
			pChild->StopInternal();
		}
	}
}
//---------------------------------------------------------------------------------------------------
void IAsyncObject::PauseInternal()
{
	if (m_bPause == false)
	{
		m_bPause = true;

		m_CV.notify_one();

		for (IAsyncObject* pChild : m_ChildObjects)
		{
			pChild->PauseInternal();
		}
	}	
}
//---------------------------------------------------------------------------------------------------
void IAsyncObject::ContinueInternal()
{
	if (m_bPause)
	{
		m_bPause = false;

		m_CV.notify_one();

		for (IAsyncObject* pChild : m_ChildObjects)
		{
			pChild->ContinueInternal();
		}
	}	
}
//---------------------------------------------------------------------------------------------------