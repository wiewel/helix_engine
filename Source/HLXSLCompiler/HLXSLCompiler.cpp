//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "HLXSLCompiler.h"
#include "Resources\HLXSLFile.h"
#include "Async\Delegate.h"
#include "Resources\FileSystem.h"
#include "stdrzr\source\StringHelpers.h"

using namespace Helix;
using namespace Helix::Compiler;

//---------------------------------------------------------------------------------------------------
HLXSLCompiler::HLXSLCompiler()
{
	m_pPermuationCompiler = new PermutationCompiler();

	m_pSettingsFile = new stdrzr::TextToken("Config/HLXSLCompilerSettings.ini", "HLXSLCompilerSettings", false, std::ios::in);

	// in case the file could not be loaded, set the root key
	if (m_pSettingsFile->getKey().empty())
	{
		HLOG("No settings file found! Please create a HLXSLCompilerSettings.ini file in your working directory!");
		m_pSettingsFile->setKey("HLXSLCompilerSettings");		
	}
	else
	{
		std::string sPermutationFile = m_pSettingsFile->getValue("PermutationFile");

		if (sPermutationFile.empty() == false)
		{
			m_pPermutationConfig = new stdrzr::TextToken(sPermutationFile, "Permutations", false, std::ios::in);
			if (m_pPermutationConfig->getKey().empty() == false)
			{
				std::vector<std::pair<std::string, std::string>>&& CurrentPairs = m_pPermutationConfig->getAllPairs<std::string>();

				std::vector<std::pair<std::string, std::string>>::iterator it = CurrentPairs.begin();
				std::vector<std::pair<std::string, std::string>>::iterator end = CurrentPairs.end();

				while (it != end) 
				{
					// TODO: check if bits are skipped and display message
					m_DefineMap.insert({ stdrzr::FromString<uint32_t>(it->second), it->first });
					++it;
				}
			}
		}
		else
		{
			HLOG("No permutation file found! Please create a Permutation.ini file!");
		}

		AddIncludeDirectories();
	}
}
//---------------------------------------------------------------------------------------------------
HLXSLCompiler::~HLXSLCompiler()
{
	HSAFE_DELETE(m_pPermuationCompiler);
	HSAFE_DELETE(m_pSettingsFile);
	HSAFE_DELETE(m_pPermutationConfig);
}
//---------------------------------------------------------------------------------------------------
void HLXSLCompiler::OpenFile(const std::string& _sFilePath)
{

}
//---------------------------------------------------------------------------------------------------
void HLXSLCompiler::AddShaderFunctions(const std::vector<std::pair<Helix::CompilerArgument, std::string>>& _Arguments, const std::string& _sShaderModel)
{
	for (auto const &it : _Arguments)
	{
		if (_sShaderModel == "5.0") 
		{
			switch (it.first)
			{
			case CompilerArgument_VS:
				m_ShaderDocument.AddShaderFunction(Compiler::ShaderFunction({ it.second, ShaderModel_VS_5_0 }));
				break;
			case CompilerArgument_HS:
				m_ShaderDocument.AddShaderFunction(Compiler::ShaderFunction({ it.second, ShaderModel_HS_5_0 }));
				break;
			case CompilerArgument_DS:
				m_ShaderDocument.AddShaderFunction(Compiler::ShaderFunction({ it.second, ShaderModel_DS_5_0 }));
				break;
			case CompilerArgument_GS:
				m_ShaderDocument.AddShaderFunction(Compiler::ShaderFunction({ it.second, ShaderModel_GS_5_0 }));
				break;
			case CompilerArgument_PS:
				m_ShaderDocument.AddShaderFunction(Compiler::ShaderFunction({ it.second, ShaderModel_PS_5_0 }));
				break;
			default:
				break;
			}
		}
		else 
		{
			HWARNING("Given shader target version is not implemented!");
		}
	}
}
//---------------------------------------------------------------------------------------------------
void HLXSLCompiler::AddIncludeDirectories()
{
	std::vector<std::string> IncludeDirs = m_pSettingsFile->getAllValues("IncludePath");

	HLOGD("Adding include directories...");

	std::vector<std::string>::iterator it = IncludeDirs.begin();
	std::vector<std::string>::const_iterator end = IncludeDirs.cend();

	if (it == end)
	{
		HLOGD("No include directory found! No header files added!");
	}

	while (it != end)
	{
		if (it->empty() == false)
		{
			HLOGD("\t%s", it->c_str());

			m_pPermuationCompiler->AddIncludeDirectory(*it);
		}

		++it;
	}
}
//---------------------------------------------------------------------------------------------------
void HLXSLCompiler::Parse()
{
	//HLOG("Parsing %s...", m_ShaderDocument.m_sFileName.c_str());

	// Parses file header and include directories
	m_ShaderDocument.Parse();

	m_ShaderDocument.PrintPermutations("\t");
}
//---------------------------------------------------------------------------------------------------
void HLXSLCompiler::Compile()
{
	m_pPermuationCompiler->Compile(m_ShaderDocument, m_DefineMap, "\t");
}
//---------------------------------------------------------------------------------------------------
void HLXSLCompiler::SaveOpenDocument()
{

}
//---------------------------------------------------------------------------------------------------
