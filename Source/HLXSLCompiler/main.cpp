//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "hlx\src\StandardDefines.h"
#include "hlx\src\Logger.h"
#include "hlx\src\StringHelpers.h"
#include "Resources\FileSystem.h"
#include "Compiler\ShaderDocument.h"
#include "Compiler\PermutationCompiler.h"
#include "CompilerSettings.h"
#include "Resources\HLXSLFile.h"
#include "hlx\src\Timer.h"

using namespace Helix;

std::mutex gMutex;
std::string gShaderName;

void UpdateProgressCallback(uint32_t _uCurrent, uint32_t _uTotal)
{
	std::lock_guard<std::mutex> Lock(gMutex);
	char pBuffer[128] = { 0 };
	_snprintf_s(pBuffer, 256, "HLXSLCompiler v1 - %s %u/%u", gShaderName.c_str(), _uCurrent, _uTotal);
	SetConsoleTitleA(pBuffer);
}
inline void PrintError(const Compiler::CompilationErrorInfo& Error)
{
	switch (Error.kType)
	{
	case Compiler::CompilationErrorType_Warning:
		HWARNING("%s %s\n %s", CSTR(Error.sFunction), CSTR(Error.sPermutationName), CSTR(Error.sMessage));
		break;
	case Compiler::CompilationErrorType_Error:
		HERROR("%s %s\n %s", CSTR(Error.sFunction), CSTR(Error.sPermutationName), CSTR(Error.sMessage));
		break;
	default:
		break;
	}
};

/// Usage: hlxslcompiler.exe "path/to/shaders/testshader.hlsl"
int main (int argc, char *argv[])
{
	HInitDebug();
	
	SetConsoleTitle("HLXSLCompiler v1");

	hlx::Logger::Instance()->SetSuppressLoggerStartedStopped(true);
	hlx::Logger::Instance()->WriteToStream(&std::wcout);

	HRESULT Result = E_FAIL;

	// Input verification
	if (argc < 2)
	{
		HLOG("Usage:");
		HLOG("\tHLXSLCompiler.exe \"path\\to\\shaders\\testshader.hlsl\" -sm x.x -vs VSHADERNAME -hs HSHADERNAME -ds DSHADERNAME -gs GSHADERNAME -ps PSHADERNAME");
		HLOG("Parameters:");
		HLOG("\t\"-sm\": define shader target version (e.g. 5.0) which applies for all following functions");
		HLOG("\t\"-vs\": define name of vertex shader");
		HLOG("\t\"-hs\": define name of hull shader (optional)");
		HLOG("\t\"-ds\": define name of domain shader (optional)");
		HLOG("\t\"-gs\": define name of geometry shader (optional)");
		HLOG("\t\"-ps\": define name of pixel shader");
		HLOG("\t\"-cs\": define name of compute shader");

		HLOG("\t\"-serial\": compile in serial order");
		HLOG("\t\"-dbg\": compile with debug info");
		HLOG("\t\"-o\": custom output path (overrides path from HLXSLCompilerSettings.ini)");
		HLOG("\t\"-lzma #\": LZMA compression");
		HLOG("\t\"-lzma-lvl #\": LZMA compression level (0 = non, 4 = default, 9 = max)");
		HLOG("\t\"-d3d#\": D3D compression");

		HLOG("\t\"-printerrors\": print all errors and warnings");

		system("pause");

		return Result;
	}

	CompilerSettings Settings;
	
	Resources::FileSystem::Instance()->AddMiscFilesFromDirectory(Resources::FileSystem::GetDirectoryFromPath(std::string(argv[1])));

	// add dirs before parsing the shader!
	for (const std::string& sIncludeDir : Settings.GetIncludeDirectories())
	{
		Resources::FileSystem::Instance()->AddMiscFilesFromDirectory(STR(sIncludeDir), S(".h*"));
	}

	std::string sOutputFile = Settings.GetOutputDirectory();
	std::string sShaderModelVersion = "5_0";
	Display::ShaderModel kTargetVersion = Display::ShaderModel_VS_5_0;
	Resources::CodeCompression kCompressionMethod = Resources::CodeCompression_None;
	Compiler::ShaderDocument ShaderFile;

	bool bDbg = false;
	bool bParallel = true;
	bool bPrintAllErrors = false;

	int32_t iLZMACompressionLevel = 2;

	for (int i = 1; i < argc; ++i)
	{
		std::string sParam(argv[i]);

		if (i == 1 && hlx::ends_with(sParam, ".hlsl"))
		{
			ShaderFile = Compiler::ShaderDocument(sParam);
			if (ShaderFile.IsOpen() == false)
			{
				break;
			}

			gShaderName = ShaderFile.GetFileName();

			continue;
		}

		Display::ShaderFunction Function = {};
		Function.kTargetVersion = kTargetVersion;
		std::string sVal;
		if (i + 1 < argc)
		{
			sVal = std::string(argv[i + 1]);
		}

		Display::ShaderType kShader = Display::ShaderType_None;

		if (hlx::starts_with(sParam, "-vs"))
		{
			kShader = Display::ShaderType_VertexShader;
			Function.sFunctionName = sVal;
		}
		else if (hlx::starts_with(sParam, "-hs"))
		{
			kShader = Display::ShaderType_HullShader;
			Function.sFunctionName = sVal;
		}
		else if (hlx::starts_with(sParam, "-ds"))
		{
			kShader = Display::ShaderType_DomainShader;
			Function.sFunctionName = sVal;
		}
		else if (hlx::starts_with(sParam, "-gs"))
		{
			kShader = Display::ShaderType_GeometryShader;
			Function.sFunctionName = sVal;
		}
		else if (hlx::starts_with(sParam, "-ps"))
		{
			kShader = Display::ShaderType_PixelShader;
			Function.sFunctionName = sVal;
		}
		else if (hlx::starts_with(sParam, "-cs"))
		{
			kShader = Display::ShaderType_ComputeShader;
			Function.sFunctionName = sVal;
		}
		else if (hlx::starts_with(sParam, "-dbg"))
		{
			bDbg = true;
		}
		else if (hlx::starts_with(sParam, "-o"))
		{
			sOutputFile = sVal;
		}
		else if (hlx::starts_with(sParam, "-serial"))
		{
			bParallel = false;
		}
		else if (hlx::starts_with(sParam, "-lzma-lvl"))
		{
			iLZMACompressionLevel = std::stoul(sVal);
		}
		else if (hlx::starts_with(sParam, "-lzma"))
		{
			HLOG("CompressionMethod: LZMA");
			kCompressionMethod = Resources::CodeCompression_LZMA;
		}
		else if (hlx::starts_with(sParam, "-printerrors"))
		{
			bPrintAllErrors = true;
		}
		else if (hlx::starts_with(sParam, "-sm"))
		{
			// todo: implement other shader models
			sShaderModelVersion = sVal;
			hlx::replace(sShaderModelVersion, ".", "_");
		}

		if (sShaderModelVersion.empty() == false && kShader != Display::ShaderType_None)
		{
			for (uint32_t i = 0; i < Display::ShaderModel_NumOf; ++i)
			{
				const Display::ShaderModelDesc& Desc = Display::ShaderModelVersions[i];
				if (Desc.kType != kShader) continue;

				if (hlx::ends_with(std::string(Desc.pVersionString), sShaderModelVersion))
				{
					kTargetVersion = Desc.kVersion;
					break;
				}
			}
		}

		if(kShader != Display::ShaderType_None)
		{
			Function.kTargetVersion = kTargetVersion;

			ShaderFile.AddShaderFunction(Function);

			HLOG("Function: %s -> %s", CSTR(Function.sFunctionName), CSTR(Display::ShaderModelVersions[Function.kTargetVersion].pVersionString));
		}
	}

	if (ShaderFile.IsOpen() == false)
	{
		HERROR("Failed to open shader!");

		system("pause");

		return  Result;
	}

	// add default shader functions
	if (ShaderFile.GetFunctions().empty())
	{
		Display::ShaderFunction Function("VShader", Display::ShaderModel_VS_5_0);
		ShaderFile.AddShaderFunction(Function);

		Function = Display::ShaderFunction("PShader", Display::ShaderModel_PS_5_0);
		ShaderFile.AddShaderFunction(Function);

		Function = Display::ShaderFunction("DShader", Display::ShaderModel_DS_5_0);
		ShaderFile.AddShaderFunction(Function);

		Function = Display::ShaderFunction("GShader", Display::ShaderModel_GS_5_0);
		ShaderFile.AddShaderFunction(Function);

		Function = Display::ShaderFunction("HShader", Display::ShaderModel_HS_5_0);
		ShaderFile.AddShaderFunction(Function);

		Function = Display::ShaderFunction("CShader", Display::ShaderModel_CS_5_0);
		ShaderFile.AddShaderFunction(Function);
	}

	// todo: add permutations to be ignored
	//Function.AddPermutationIgnoreMask(/*(1 << 1) |*/ (1 << 4));

	Compiler::TCompilerFlags kFlags = Compiler::CompilerFlag_Enable_Strictness;

	HLOG("LZMA Compression Level: %d", iLZMACompressionLevel);

	if (bDbg)
	{
		kFlags |= Compiler::CompilerFlag_Debug;
		kFlags |= Compiler::CompilerFlag_SkipOptimization;
		HLOG("DebugInfo: Enabled");
	}
	else
	{
		kFlags |= Compiler::CompilerFlag_Optimization_Level3;
		HLOG("DebugInfo: Disabled");
	}

	if (bParallel)
	{
		HLOG("ParallelCompilation: Enabled");
	}
	else
	{
		HLOG("ParallelCompilation: Disabled");
	}

	ShaderFile.SetCompilationFlags(kFlags);
	
	Compiler::PermutationCompiler Compiler(UpdateProgressCallback, bParallel);

	// pre-process shader infos
	ShaderFile.Parse();

	if (sOutputFile.empty())
	{
		sOutputFile = ShaderFile.GetFilePath();
	}
	else
	{
		if (hlx::ends_with(sOutputFile, "\\") == false && hlx::ends_with(sOutputFile, "/") == false)
		{
			sOutputFile += "\\";
		}
		sOutputFile += ShaderFile.GetFileName();
	}

	// save a copy of the parsed file for debugging
	if (bDbg)
	{
		hlx::replace_extension(sOutputFile, "dbg");

		const std::string& sParsedCode = ShaderFile.GetCode();

		HLOG("Writing %s [%.2fkb]", CSTR(sOutputFile), (float)sParsedCode.size() / 1024.f);

		hlx::fbytestream ParsedFile(sOutputFile, std::ios::out | std::ios::binary);
		if (ParsedFile.is_open() == false)
		{
			HERROR("Failed to open parsed file %s", CSTR(sOutputFile));
			return E_FAIL;
		}

		ParsedFile.write(sParsedCode.c_str(), std::min(strlen(sParsedCode.c_str()), sParsedCode.size()));
		ParsedFile.close();

		ShaderFile.SetParsedFilePath(STR(sOutputFile));
	}

	Resources::ShaderCompilationResult& CompileOutput = Compiler.GetCompilationResult();

	CompileOutput.SetCompressionMethod(kCompressionMethod);
	CompileOutput.SetLZMACompressionLevel(iLZMACompressionLevel);

	hlx::Timer timer;

	if (Compiler.Compile(ShaderFile))
	{
		double fTime = timer.ElapsedTimeD();

		HLOG("Compilation took %f seconds", fTime);		

		hlx::replace_extension(sOutputFile, "hlxsl");

		hlx::fbytestream OutputFile(sOutputFile, std::ios::out | std::ios::binary);
		if (OutputFile.is_open() == false)
		{
			HERROR("Failed to open output file %s", CSTR(sOutputFile));
			return E_FAIL;
		}

		HLOG("Assembling %s", CSTR(CompileOutput.GetShaderName()));

		hlx::bytes OutputBuffer;
		hlx::bytestream OutputStream(OutputBuffer);

		Resources::HLXSLFile HLXSL;

		HLXSL.Write(OutputStream, CompileOutput);

		HLOG("Writing %s [%.2fkb]", CSTR(sOutputFile), (float)OutputBuffer.size() / 1024.f);

		OutputFile.put<hlx::bytes>(OutputBuffer);
		OutputFile.close();

		HLOG("Compilation finished =)");

		Result = S_OK;
	}
	else
	{
		HERROR("Some permutations failed to compile =(");
	}

	const Compiler::TErrorReport& ErrorReport = Compiler.GetErrorReport();

	if (ErrorReport.empty() == false)
	{
		if (bPrintAllErrors)
		{
			for (const Compiler::CompilationErrorInfo& Error : ErrorReport)
			{
				PrintError(Error);
			}
		}
		else // Just print last error
		{
			PrintError(ErrorReport.back());
		}

		HLOG("There are %u warnings and errors", ErrorReport.size());
	}

	return Result;
}