//   Copyright 2020 Fabian Wahlster, Steffen Wiewel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef HLXSLCOMPILER_H
#define HLXSLCOMPILER_H

#include "stdrzr\source\TextToken.h"
#include "Util\Logger.h"
#include "Compiler\PermutationCompiler.h"
#include <map>

namespace Helix
{
	enum CompilerArgument
	{
		//CompilerArgument_Path,
		CompilerArgument_VS,
		CompilerArgument_HS,
		CompilerArgument_DS,
		CompilerArgument_GS,
		CompilerArgument_PS//,
		//CompilerArgument_SM
	};

	class HLXSLCompiler
	{
	public:
		HDEBUGNAME("HLXSLCompiler");

		HLXSLCompiler();
		~HLXSLCompiler();

	public:
		void OpenFile(const std::string& _sFilePath);
		void AddShaderFunctions(const std::vector<std::pair<Helix::CompilerArgument, std::string>>& _Arguments, const std::string& _sShaderModel);
		void Parse();
		void Compile();
		void SaveOpenDocument();

	private:		
		void AddIncludeDirectories();

	private:
		Compiler::PermutationCompiler* m_pPermuationCompiler = nullptr;
		
		stdrzr::TextToken* m_pSettingsFile = nullptr;
		stdrzr::TextToken* m_pPermutationConfig = nullptr;

		//std::unordered_map<std::string, uint32_t> m_DefineMap;
		std::map<uint64_t, std::string> m_DefineMap;

		Compiler::ShaderDocument m_ShaderDocument;
	};
} // Helix

#endif // HLXSLCOMPILER_H
