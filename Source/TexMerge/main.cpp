//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "Math\XMMath.h"

#include "directxtex.h"
#include "DirectXPackedVector.h"

#include "hlx\src\Logger.h"
#include "Util\Flag.h"
#include <vector>
#include <array>
#include "hlx\src\StringHelpers.h"

//https://msdn.microsoft.com/de-de/library/Windows/Hardware/ff728749%28v=vs.85%29.aspx
//https://msdn.microsoft.com/de-de/library/windows/desktop/ee418728%28v=vs.85%29.aspx
// dxgi format listing:
//https://msdn.microsoft.com/de-de/library/windows/desktop/mt426648%28v=vs.85%29.aspx
//---------------------------------------------------------------------------------------------------

using namespace DirectX;
using namespace Helix;

enum ColorChannel
{
	ColorChannel_R = 0,
	ColorChannel_G,
	ColorChannel_B,
	ColorChannel_A,
	ColorChannel_NumOf,
	Colorchannel_None
};

//---------------------------------------------------------------------------------------------------
struct SupportedFormat
{
	SupportedFormat() = default;
	SupportedFormat(const hlx::string& _sName, DXGI_FORMAT _kFormat, uint32_t _uByteStride, uint32_t _uBytesPerChannel, const std::array<uint32_t, ColorChannel_NumOf>& _ChannelOffsets) :
		sName(_sName), kFormat(_kFormat), uByteStride(_uByteStride), uBytesPerChannel(_uBytesPerChannel), pChannelOffsets(_ChannelOffsets) {}

	hlx::string sName;
	DXGI_FORMAT kFormat;
	uint32_t uByteStride;
	uint32_t uBytesPerChannel;
	std::array<uint32_t, ColorChannel_NumOf> pChannelOffsets = { HUNDEFINED32 };
};
#define DEFFMT(fmt,size,chsize, ro,go,bo,ao) { L#fmt, DXGI_FORMAT_ ## fmt, size, chsize, {ro,go,bo,ao}}
//---------------------------------------------------------------------------------------------------

struct InputImage
{
	std::shared_ptr<ScratchImage> pImage = nullptr;
	hlx::string sFilePath;
	TexMetadata Info = {};
	SupportedFormat Format;
	ColorChannel pMapping[ColorChannel_NumOf] = { Colorchannel_None,Colorchannel_None,Colorchannel_None,Colorchannel_None };
};
//---------------------------------------------------------------------------------------------------

inline bool Merge(const uint8_t* _pInputStride, uint8_t* _pOutputStride, const ColorChannel(&_ChannelMapping)[ColorChannel_NumOf], const SupportedFormat& _Format)
{
	for (uint32_t c = 0; c < ColorChannel_NumOf; ++c)
	{
		uint32_t uOutOffset = _ChannelMapping[c];
		if (uOutOffset != Colorchannel_None)
		{
			uOutOffset = _Format.pChannelOffsets[uOutOffset];
			uint32_t uInOffset = _Format.pChannelOffsets[c];

			if (uOutOffset == HUNDEFINED32 || uInOffset == HUNDEFINED32)
			{
				HERROR("Invalid format/channel mapping");
				return false;
			}
			memcpy_s(&_pOutputStride[uOutOffset], _Format.uBytesPerChannel, &_pInputStride[uInOffset], _Format.uBytesPerChannel);
		}
	}

	return true;
};
//---------------------------------------------------------------------------------------------------

SupportedFormat g_pFormats[] =
{
	// List does not include _TYPELESS or depth/stencil formats
	DEFFMT(R32G32B32A32_FLOAT,16,4,	0,4,8,12),
	DEFFMT(R32G32B32A32_UINT,16,4,	0,4,8,12),
	DEFFMT(R32G32B32A32_SINT,16,4,	0,4,8,12),
	DEFFMT(R32G32B32_FLOAT,12,4,	0,4,8,HUNDEFINED32),
	DEFFMT(R32G32B32_UINT,12,4,	0,4,8,HUNDEFINED32),
	DEFFMT(R32G32B32_SINT,12,4,	0,4,8,HUNDEFINED32),
	DEFFMT(R16G16B16A16_FLOAT,8,2,	0,2,4,6),
	DEFFMT(R16G16B16A16_UNORM,8,2,	0,2,4,6),
	DEFFMT(R16G16B16A16_UINT,8,2,	0,2,4,6),
	DEFFMT(R16G16B16A16_SNORM,8,2,	0,2,4,6),
	DEFFMT(R16G16B16A16_SINT,8,2,	0,2,4,6),
	DEFFMT(R32G32_FLOAT,8,4,	0,4,HUNDEFINED32,HUNDEFINED32),
	DEFFMT(R32G32_UINT,8,4,	0,4,HUNDEFINED32,HUNDEFINED32),
	DEFFMT(R32G32_SINT,8,4,	0,4,HUNDEFINED32,HUNDEFINED32),
	//DEFFMT(R10G10B10A2_UNORM,4,HUNDEFINED32,HUNDEFINED32,HUNDEFINED32,HUNDEFINED32), // XMDECN4
	//DEFFMT(R10G10B10A2_UINT,4,HUNDEFINED32,HUNDEFINED32,HUNDEFINED32,HUNDEFINED32), // XMXDEC4
	//DEFFMT(R11G11B10_FLOAT,4),
	DEFFMT(R8G8B8A8_UNORM,4,1,	0,1,2,3),
	DEFFMT(R8G8B8A8_UNORM_SRGB,4,1,	0,1,2,3),
	DEFFMT(R8G8B8A8_UINT,4,1,	0,1,2,3),
	DEFFMT(R8G8B8A8_SNORM,4,1,	0,1,2,3),
	DEFFMT(R8G8B8A8_SINT,4,1,	0,1,2,3),
	DEFFMT(R16G16_FLOAT,4,2,	0, 2, HUNDEFINED32, HUNDEFINED32),
	DEFFMT(R16G16_UNORM,4,2,	0, 2, HUNDEFINED32, HUNDEFINED32),
	DEFFMT(R16G16_UINT,4,2,		0, 2, HUNDEFINED32, HUNDEFINED32),
	DEFFMT(R16G16_SNORM,4,2,	0, 2, HUNDEFINED32, HUNDEFINED32),
	DEFFMT(R16G16_SINT,4,2,		0, 2, HUNDEFINED32, HUNDEFINED32),
	DEFFMT(R32_FLOAT,4,4,	0, HUNDEFINED32, HUNDEFINED32, HUNDEFINED32),
	DEFFMT(R32_UINT,4,4,	0, HUNDEFINED32, HUNDEFINED32, HUNDEFINED32),
	DEFFMT(R32_SINT,4,4, 	0, HUNDEFINED32, HUNDEFINED32, HUNDEFINED32),
	DEFFMT(R8G8_UNORM,2,1,	0,1, HUNDEFINED32, HUNDEFINED32),
	DEFFMT(R8G8_UINT,2,1,	0,1, HUNDEFINED32, HUNDEFINED32),
	DEFFMT(R8G8_SNORM,2,1,	0,1, HUNDEFINED32, HUNDEFINED32),
	DEFFMT(R8G8_SINT,2,1,	0,1, HUNDEFINED32, HUNDEFINED32),
	DEFFMT(R16_FLOAT,2,2,	0, HUNDEFINED32, HUNDEFINED32, HUNDEFINED32),
	DEFFMT(R16_UNORM,2,2,	0, HUNDEFINED32, HUNDEFINED32, HUNDEFINED32),
	DEFFMT(R16_UINT,2,2,	0, HUNDEFINED32, HUNDEFINED32, HUNDEFINED32),
	DEFFMT(R16_SNORM,2,2,	0, HUNDEFINED32, HUNDEFINED32, HUNDEFINED32),
	DEFFMT(R16_SINT,2,2,	0, HUNDEFINED32, HUNDEFINED32, HUNDEFINED32),
	DEFFMT(R8_UNORM,1,1,	0, HUNDEFINED32, HUNDEFINED32, HUNDEFINED32),
	DEFFMT(R8_UINT,1,1,		0, HUNDEFINED32, HUNDEFINED32, HUNDEFINED32),
	DEFFMT(R8_SNORM,1,1,	0, HUNDEFINED32, HUNDEFINED32, HUNDEFINED32),
	DEFFMT(R8_SINT,1,1,		0, HUNDEFINED32, HUNDEFINED32, HUNDEFINED32),
	DEFFMT(A8_UNORM,1,1,	0, HUNDEFINED32, HUNDEFINED32, HUNDEFINED32),

	//DEFFMT(R9G9B9E5_SHAREDEXP),
	//DEFFMT(R8G8_B8G8_UNORM),
	//DEFFMT(G8R8_G8B8_UNORM),

	//DEFFMT(BC1_UNORM,8),
	//DEFFMT(BC1_UNORM_SRGB,8),
	//DEFFMT(BC2_UNORM,16),
	//DEFFMT(BC2_UNORM_SRGB,16),
	//DEFFMT(BC3_UNORM,16),
	//DEFFMT(BC3_UNORM_SRGB,16),
	//DEFFMT(BC4_UNORM,8),
	//DEFFMT(BC4_SNORM,8),
	//DEFFMT(BC5_UNORM,16),
	//DEFFMT(BC5_SNORM,16),

	//DEFFMT(B5G6R5_UNORM,2), // XMU565
	//DEFFMT(B5G5R5A1_UNORM,2), // XMU555

	//// DXGI 1.1 formats
	//DEFFMT(B8G8R8A8_UNORM,4),
	//DEFFMT(B8G8R8X8_UNORM,4),
	////DEFFMT(R10G10B10_XR_BIAS_A2_UNORM),
	//DEFFMT(B8G8R8A8_UNORM_SRGB,4),
	//DEFFMT(B8G8R8X8_UNORM_SRGB,4),

	//DEFFMT(BC6H_UF16,16),
	//DEFFMT(BC6H_SF16,16),
	//DEFFMT(BC7_UNORM,16),
	//DEFFMT(BC7_UNORM_SRGB,16),

	{ S("Unknown"), DXGI_FORMAT_UNKNOWN,0,0, { HUNDEFINED32 } }
};

//---------------------------------------------------------------------------------------------------
inline hlx::string GeFormatName(DXGI_FORMAT _kFormat)
{
	const SupportedFormat* pFormat = g_pFormats;
	for (; pFormat->kFormat != DXGI_FORMAT_UNKNOWN && pFormat->kFormat != _kFormat; ++pFormat) {}
	return pFormat->sName;
}
//---------------------------------------------------------------------------------------------------
inline SupportedFormat GetSupportedFormat(const hlx::string& _sFormatName)
{
	const SupportedFormat* pFormat = g_pFormats;
	for (; pFormat->kFormat != DXGI_FORMAT_UNKNOWN && pFormat->sName != _sFormatName; ++pFormat) {}
	return *pFormat;
}
//---------------------------------------------------------------------------------------------------
inline SupportedFormat GetSupportedFormat(DXGI_FORMAT _kFormat)
{
	const SupportedFormat* pFormat = g_pFormats;
	for (; pFormat->kFormat != DXGI_FORMAT_UNKNOWN && pFormat->kFormat != _kFormat; ++pFormat) {}
	return *pFormat;
}
//---------------------------------------------------------------------------------------------------


InputImage LoadAndUncompressDDS(const std::wstring& _sPath, DWORD _uDDSFlags = DDS_FLAGS_NONE)
{
	HRESULT hr = S_OK;
	InputImage LoadedTex = {};
	LoadedTex.sFilePath = _sPath;

	std::shared_ptr<ScratchImage> InputImage(new ScratchImage);

	HRERROR(hr = LoadFromDDSFile(_sPath.c_str(), _uDDSFlags, &LoadedTex.Info, *InputImage));
	if (FAILED(hr))
	{
		return LoadedTex;
	}

	LoadedTex.pImage = InputImage;

	if (IsCompressed(LoadedTex.Info.format))
	{
		const Image* pImage = InputImage->GetImage(0, 0, 0);

		if (pImage == nullptr)
		{
			HERROR("Failed to get Image");
			return LoadedTex;
		}

		size_t uImageCount = InputImage->GetImageCount(); // or just use the first one

		std::shared_ptr<ScratchImage> UncompressedImage(new ScratchImage);

		HRERROR(hr = Decompress(pImage, uImageCount, LoadedTex.Info, DXGI_FORMAT_UNKNOWN, *UncompressedImage));
		if (FAILED(hr))
		{
			return LoadedTex;
		}

		LoadedTex.Info = UncompressedImage->GetMetadata();
		LoadedTex.pImage = UncompressedImage;
	}

	LoadedTex.Format = GetSupportedFormat(LoadedTex.Info.format);

	if (LoadedTex.Format.kFormat == DXGI_FORMAT_UNKNOWN)
	{
		HERROR("Unsupported input format %X", LoadedTex.Format.kFormat);
		LoadedTex.pImage = nullptr;
	}
	else
	{
		HLOG("Loaded %s %ux%u [%s]", _sPath.c_str(), LoadedTex.Info.width, LoadedTex.Info.height, LoadedTex.Format.sName.c_str());
	}

	return LoadedTex;
}
//---------------------------------------------------------------------------------------------------

inline ColorChannel GetChannelFromString(const hlx::string& sTarget)
{
	hlx::string&& sTargetChannel = hlx::get_right_of(sTarget, S("-"));
	hlx::remove_whitespaces(sTargetChannel);

	if (sTargetChannel == S("r"))
	{
		return ColorChannel_R;
	}
	else if (sTargetChannel == S("g"))
	{
		return ColorChannel_G;
	}
	else if (sTargetChannel == S("b"))
	{
		return ColorChannel_B;
	}
	else if (sTargetChannel == S("a"))
	{
		return ColorChannel_A;
	}

	return Colorchannel_None;
}
//---------------------------------------------------------------------------------------------------

int wmain(_In_ int argc, _In_z_count_(argc) wchar_t* argv[])
{
	HInitDebug();
	SetConsoleTitleA("TexMerge v1");

	hlx::Logger::Instance()->SetSuppressLoggerStartedStopped(true);
	hlx::Logger::Instance()->WriteToStream(&std::wcout);

	std::vector<InputImage> InputTextures;
	hlx::string sOutputPath;

	SupportedFormat OutputFormat = { S("Unknown"), DXGI_FORMAT_UNKNOWN,0, 0,{ HUNDEFINED32 } };
	SupportedFormat InputFormat = { S("Unknown"), DXGI_FORMAT_UNKNOWN,0, 0,{ HUNDEFINED32 } };

	size_t uWidth = 0;
	size_t uHeight = 0;

	//---------------------------------------------------------------------------------------------------
	// Parse input
	//---------------------------------------------------------------------------------------------------

	for (int i = 1; i < argc; ++i)
	{
		hlx::string sParam(argv[i]);
		hlx::to_lower(sParam);

		hlx::string sVal;
		if (i + 1 < argc)
		{
			sVal = hlx::string(argv[i + 1]);
		}

		if (sParam == S("-in"))
		{
			InputImage Input = LoadAndUncompressDDS(sVal);
			if (Input.pImage == nullptr)
			{
				return E_FAIL;
			}

			if (uWidth == 0 && uHeight == 0)
			{
				uWidth = Input.Info.width;
				uHeight = Input.Info.height;
				InputFormat = Input.Format;
			}
			else if (uWidth != Input.Info.width || uHeight != Input.Info.height)
			{
				HERROR("Inconsistent image resulution %ux%u vs %ux%u", uWidth, uHeight, Input.Info.width, Input.Info.height);
				return E_FAIL;
			}
			else if (InputFormat.kFormat != Input.Format.kFormat)
			{
				HERROR("Inconsistent pixel format %s vs %s", InputFormat.sName.c_str(), Input.Format.sName.c_str());
				return E_FAIL;
			}

			InputTextures.push_back(Input);
		}
		else if (sParam == S("-out"))
		{
			sOutputPath = sVal;
		}
		else if (sParam == S("-f"))
		{
			OutputFormat = GetSupportedFormat(sVal);
			if (OutputFormat.kFormat == DXGI_FORMAT_UNKNOWN || IsCompressed(OutputFormat.kFormat))
			{
				HERROR("Unsupported output format %X", OutputFormat.kFormat);
				return E_FAIL;
			}
		}

		if (InputTextures.empty())
		{
			continue;
		}

		InputImage& Input = InputTextures.back();

		if (hlx::starts_with(sParam, S("-r")))
		{
			Input.pMapping[ColorChannel_R] = GetChannelFromString(sParam);
		}
		else if (hlx::starts_with(sParam, S("-g")))
		{
			Input.pMapping[ColorChannel_G] = GetChannelFromString(sParam);
		}
		else if (hlx::starts_with(sParam, S("-b")))
		{
			Input.pMapping[ColorChannel_B] = GetChannelFromString(sParam);
		}
		else if (hlx::starts_with(sParam, S("-a")))
		{
			Input.pMapping[ColorChannel_A] = GetChannelFromString(sParam);
		}
	}

	if (InputTextures.empty())
	{
		HERROR("No input image could be loaded");
		return E_FAIL;
	}

	if (OutputFormat.kFormat == DXGI_FORMAT_UNKNOWN)
	{
		HWARNING("Unspecified output format, assuming %s", InputFormat.sName.c_str());
		OutputFormat = InputFormat;
	}

	//---------------------------------------------------------------------------------------------------
	// merge input textrues into intermediate texture with same format
	//---------------------------------------------------------------------------------------------------

	HRESULT hr = S_OK;
	std::shared_ptr<ScratchImage> pIntermediateTexture(new ScratchImage);

	HRERROR(hr = pIntermediateTexture->Initialize2D(InputFormat.kFormat, uWidth, uHeight, 1, 1));
	if (FAILED(hr))
	{
		HERROR("Failed to create intermediate image");
		return E_FAIL;
	}

	const Image* pOutImage = pIntermediateTexture->GetImage(0, 0, 0);

	if (pOutImage == nullptr)
	{
		HERROR("Failed to get intermediate image");
		return E_FAIL;
	}

	for (const InputImage& Input : InputTextures)
	{
		HLOG("Merging %s", Input.sFilePath.c_str());

		const Image* pInImage = Input.pImage->GetImage(0, 0, 0);

		if (pInImage == nullptr)
		{
			HERROR("Failed to get input image");
			return E_FAIL;
		}

		if (pInImage->rowPitch != pOutImage->rowPitch)
		{
			HERROR("rowPitch mismatch");
			return E_FAIL;
		}

		uint8_t* pOutputPixel = pOutImage->pixels;
		const uint8_t* pInPixel = pInImage->pixels;
		size_t uPixelCount = pOutImage->height*pOutImage->width;

		for (size_t i = 0; i < uPixelCount; ++i)
		{
			if (Merge(pInPixel, pOutputPixel, Input.pMapping, InputFormat) == false)
			{
				return E_FAIL;
			}

			pInPixel += InputFormat.uByteStride;
			pOutputPixel += InputFormat.uByteStride;
		}
	}

	//---------------------------------------------------------------------------------------------------
	// convert intermediate texture to target format
	//---------------------------------------------------------------------------------------------------
	std::shared_ptr<ScratchImage> pOutputTexture(new ScratchImage);

	if (InputFormat.kFormat != OutputFormat.kFormat)
	{
		DWORD dwFilter = TEX_FILTER_DEFAULT;		

		HLOG("Converting %s to %s", InputFormat.sName.c_str(), OutputFormat.sName.c_str());

		HRERROR(hr = Convert(pOutImage, 1, pIntermediateTexture->GetMetadata(), OutputFormat.kFormat, dwFilter, 0.5f, *pOutputTexture));
		if (FAILED(hr))
		{
			return E_FAIL;
		}
	}
	else
	{
		pOutputTexture = pIntermediateTexture;
	}

	HLOG("Saving %s", sOutputPath.c_str());
	HRERROR(hr = SaveToDDSFile(pOutputTexture->GetImages(), pOutputTexture->GetImageCount(), pOutputTexture->GetMetadata(), DDS_FLAGS_NONE, sOutputPath.c_str()));
	if (FAILED(hr))
	{
		return E_FAIL;
	}

	//DWORD ddsFlags = DDS_FLAGS_NONE;
	//if (dwOptions & (DWORD64(1) << OPT_DDS_DWORD_ALIGN))
	//	ddsFlags |= DDS_FLAGS_LEGACY_DWORD;
	//if (dwOptions & (DWORD64(1) << OPT_EXPAND_LUMINANCE))
	//	ddsFlags |= DDS_FLAGS_EXPAND_LUMINANCE;

	return S_OK;
}
//---------------------------------------------------------------------------------------------------
