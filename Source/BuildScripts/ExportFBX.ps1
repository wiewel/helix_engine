param([string]$FBXScene = "\test.FBX", [string]$HelixDir = "C:\Projects\Helix\", $OptionalTextureOutputDir = "", [switch]$ConvexMesh = $false)

Echo "ExportFBX"
Echo "==========="

Echo ("FBXScene: " + $FBXScene)
Echo ("ConvexMesh: " + $ConvexMesh)

if($HelixDir.Length -eq 0)
{
    Echo "Invalid HelixDir (use SolutionDir\..\)"
    return
}
Echo ("HelixDir: " + $HelixDir)


[string]$H3DTool = $HelixDir + "ToolBuild\H3DTool\H3DToolx64Release.exe"

[string]$AssetDir = $HelixDir + "Assets\"
[string]$GameDir = $HelixDir + "GameBuild\"
[string]$CompressTexScript = $HelixDir +"Source\BuildScripts\CompressTextures.ps1"

if(Test-Path $CompressTexScript)
{
    Echo $CompressTexScript
}
else
{
    Echo "CompressTexture not found!"
    pause
    return
}

if(Test-Path $H3DTool)
{
    Echo $H3DTool
}
else
{
    Echo "H3DTool not found!"
    pause
    return
}

if(Test-Path $AssetDir)
{
    Echo $AssetDir
}
else
{
    Echo "AssetDir not found!"
    pause
    return
}

if(Test-Path $GameDir)
{
    Echo $GameDir
}
else
{
    Echo "GameDir not found!"
    pause
    return
}

[string]$TextureOutputDir

if(([string]::IsNullOrWhiteSpace($OptionalTextureOutputDir) -eq $false) -and (Test-Path $OptionalTextureOutputDir))
{
	$TextureOutputDir = $OptionalTextureOutputDir
}else
{
    if (!(Test-Path -path ($HelixDir + "Temp\Textures\")))
    {
        New-Item ($HelixDir + "Temp\Textures\") -Type Directory
    }

	$TextureOutputDir = $HelixDir + "Temp\Textures\"
}

Echo ("TextureOutputDir: " + $TextureOutputDir)

if (!(Test-Path -path ($GameDir + "Data\Models\")))
{
    New-Item ($GameDir +  "Data\Models\") -Type Directory
}
if (!(Test-Path -path ($GameDir + "Data\Materials\")))
{
    New-Item ($GameDir + "Data\Materials\") -Type Directory
}
if (!(Test-Path -path ($GameDir + "Data\Textures\")))
{
    New-Item ($GameDir + "Data\Textures\") -Type Directory
}

$ProcessInfo = New-Object System.Diagnostics.ProcessStartInfo 
$ProcessInfo.FileName = $H3DTool
$ProcessInfo.UseShellExecute = $false

$Process = New-Object System.Diagnostics.Process

if(([string]::IsNullOrWhiteSpace($FBXScene) -eq $false) -and (Test-Path $FBXScene))
{
	Echo ("Converting " + $FBXScene)
}else
{
	Echo ("No valid input FBX specified: " + $FBXScene)
	return
}

$args = $FBXScene + " -h3dout " + $GameDir + "Data\Models\ " + "-hmatout " + $GameDir + "Data\Materials\ " + "-texout " + $TextureOutputDir + " -n -t -uv -flipv -directx -lhs -r "

if($ConvexMesh)
{
	$args += "-physx"
}   

#$ProcessInfo.RedirectStandardOutput = $true 

$ProcessInfo.RedirectStandardError = $false
$ProcessInfo.RedirectStandardOutput = $false 
$ProcessInfo.UseShellExecute = $false 

$ProcessInfo.Arguments = $args
$Process.StartInfo = $ProcessInfo 
$Process.Start() #| Out-Null 
$Process.WaitForExit()
#Echo $Process.StandardOutput.ReadToEnd()
[int]$exitcode = $Process.ExitCode
if($exitcode -ne 0)
{
    Echo $args
    Echo "Meshes could not be converted!"
    pause
    return
}

[string]$OutDir  = $TextureOutputDir + [System.IO.Path]::GetFileNameWithoutExtension($FBXScene) + "\"

if(Test-Path $OutDir)
{
    Invoke-Expression "& `"$CompressTexScript`" `"$HelixDir`" `"$OutDir`""
}

if($TextureOutputDir -eq ($HelixDir + "Temp\Textures\"))
{
	if(Test-Path $OutDir)
	{
		Echo ("Cleaning " + $OutDir)
		Remove-Item ($OutDir + "*.*")
	}
}

Echo "Export complete"
