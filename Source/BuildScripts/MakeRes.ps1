param([string]$HelixDir = "C:\Projects\Helix\", [switch]$ConvexMesh = $false)

Echo "MakeRes for HELiX"
Echo "================="

Echo ("CleanData: " + $CleanData)
Echo ("ConvexMesh: " + $ConvexMesh)

if($HelixDir.Length -eq 0)
{
    Echo "Invalid HelixDir (use SolutionDir\..\)"
    return
}
Echo ("HelixDir: " + $HelixDir)

[string]$AssetDir = $HelixDir + "Assets\"
[string]$GameDir = $HelixDir + "GameBuild\"

if(Test-Path $AssetDir)
{
    Echo $AssetDir
}
else
{
    Echo "AssetDir not found!"
    Echo $AssetDir
    pause
    return
}

if(Test-Path $GameDir)
{
    Echo $GameDir
}
else
{
    Echo "GameDir not found!"
    pause
    return
}

if (!(Test-Path -path ($GameDir + "Data\Levels\")))
{
    New-Item ($GameDir + "Data\Levels\") -Type Directory
}

if (!(Test-Path -path ($GameDir + "Data\Sounds\")))
{
    New-Item ($GameDir + "Data\Sounds\") -Type Directory
}

if (!(Test-Path -path ($GameDir + "Configs\")))
{
    New-Item ($GameDir + "Configs\") -Type Directory
}

if (!(Test-Path -path ($GameDir + "Data\Materials\")))
{
    New-Item ($GameDir + "Data\Materials\") -Type Directory
}

[string]$H3DTool = $HelixDir + "ToolBuild\H3DTool\H3DToolx64Release.exe"
[string]$CompressTexScript = $HelixDir +"Source\BuildScripts\CompressTextures.ps1"
[string]$CleanGameDataScript = $HelixDir +"Source\BuildScripts\CleanGameData.ps1"
[string]$CompileShadersScript = $HelixDir +"Source\BuildScripts\CompileShaders.ps1"
[string]$ExportFBXScript = $HelixDir +"Source\BuildScripts\ExportFBX.ps1"
[string]$UpdateSVNScript = $HelixDir +"Source\BuildScripts\UpdateSVN.ps1"

if(Test-Path $ExportFBXScript)
{
    Echo $ExportFBXScript
}
else
{
    Echo "ExportFBXScript not found!"
    pause
    return
}

if(Test-Path $CompressTexScript)
{
    Echo $CompressTexScript
}
else
{
    Echo "CompressTexture not found!"
    pause
    return
}

if(Test-Path $CompileShadersScript)
{
    Echo $CompileShadersScript
}
else
{
    Echo "CompileShadersScript not found!"
    pause
    return
}

if(Test-Path $CleanGameDataScript)
{
    Echo $CleanGameDataScript
}
else
{
    Echo "CleanGameDataScript not found!"
    pause
    return
}

if(Test-Path $H3DTool)
{
    Echo $H3DTool
}
else
{
    Echo "H3DTool not found!"
    pause
    return
}

Invoke-Expression "& `"$UpdateSVNScript`" `"$HelixDir`" `"$SVNDir`""

Invoke-Expression "& `"$CompileShadersScript`" `"$HelixDir`""

foreach ($file in get-ChildItem -recurse -path ($AssetDir + "Levels\") -include *.hell)
{
    [string]$OutFile = $GameDir + "Data\Levels\" + $file.Name

    Echo ("Copying " + $file.FullName)
    Copy-Item $file.FullName $OutFile
}

foreach ($file in get-ChildItem -recurse -path ($AssetDir + "Sounds\") -include *.wav)
{
    [string]$OutFile = $GameDir + "Data\Sounds\" + $file.Name

    Echo ("Copying " + $file.FullName)
    Copy-Item $file.FullName $OutFile
}

foreach ($file in get-ChildItem -recurse -path ($AssetDir + "Configs\") -include @("*.ini","*.cfg","*.stylesheet"))
{
    [string]$OutFile = $GameDir + "Configs\" + $file.Name

    Echo ("Copying " + $file.FullName)
    Copy-Item $file.FullName $OutFile
}

foreach ($file in get-ChildItem -recurse -path  ($AssetDir + "Models\") -include *.fbx)
{
	Invoke-Expression "& `"$ExportFBXScript`" `"$file`" `"$HelixDir`""
}

foreach ($file in get-ChildItem -recurse -path ($AssetDir + "Materials\") -include @("*.hmat","*.hmatprop"))
{
    [string]$OutFile = $GameDir + "Data\Materials\" + $file.Name

    Echo ("Copying " + $file.FullName)
    Copy-Item $file.FullName $OutFile
}

Invoke-Expression "& `"$CompressTexScript`" `"$HelixDir`""
