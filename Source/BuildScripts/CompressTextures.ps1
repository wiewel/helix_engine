param([string]$HelixDir = "C:\Projects\Helix\",[string]$OptionalInputDir = "",[switch]$SkipExisting = $false, [switch]$HighQuality = $false, [switch]$Clean = $false)

Echo "CompressTextures"
Echo "================"

Echo ("SkipExisting: " + $SkipExisting)
Echo ("Clean: " + $Clean)
Echo ("HightQuality: " + $HighQuality)

if($HelixDir.Length -eq 0)
{
    Echo "Invalid HelixDir (use SolutionDir\..\)"
    return
}
Echo ("HelixDir: " + $HelixDir)

[string]$TexConvert = $HelixDir + "ToolBuild\TexConvert\TexConvertx64Release.exe"
[string]$TextureDir = $HelixDir + "Assets\Textures\"
[string]$GameDir = $HelixDir + "GameBuild\"

if($OptionalInputDir)
{
	if(Test-Path $OptionalInputDir)
	{
		$TextureDir = $OptionalInputDir;
	}
	elseif($OptionalInputDir.Length -ne 0)
	{
		Echo ("Invalid OptionalInputDir " + $OptionalInputDir)
		return
	}
}

if(Test-Path $TexConvert)
{
    Echo $TexConvert
}
else
{
    Echo "TexConvert not found!"
    pause
    return
}

if(Test-Path $TextureDir)
{
    Echo $TextureDir
}
else
{
    Echo "TextureDir not found!"
    pause
    return
}

if(Test-Path $GameDir)
{
    Echo $GameDir
}
else
{
    Echo "GameDir not found!"
    pause
    return
}

if (!(Test-Path -path ($GameDir + "Data\Textures\")))
{
    New-Item ($GameDir + "Data\Textures\") -Type Directory
}

if($Clean -eq $true)
{
    Echo "Removing old textures"

    Remove-Item ($GameDir + "Data\Textures\*.dds")
}

$ProcessInfo = New-Object System.Diagnostics.ProcessStartInfo  
$Process = New-Object System.Diagnostics.Process

$ProcessInfo.RedirectStandardError = $true 
$ProcessInfo.RedirectStandardOutput = $true 
$ProcessInfo.UseShellExecute = $false

$ProcessInfo.FileName = $TexConvert

[string]$NormalFormat = "BC1_UNORM"

[string]$HeightFormat = "BC4_UNORM"
[string]$MetallicFormat = "BC4_UNORM"
[string]$RoughnessFormat = "BC4_UNORM"

[string]$ColorFormat

if($HighQuality)
{
	$NormalFormat = "BC7_UNORM"
    $ColorFormat = "BC7"
}
 else
{
    $ColorFormat = "BC3"
}

$ColorFormat += "_UNORM_SRGB"
#R8G8B8A8_UNORM_SRGB R8G8B8A8_UNORM

Echo "###################################################################################"
Echo "# Convert Textures"
Echo "###################################################################################"

[int]$TextureCount = 0

foreach ($file in get-ChildItem -recurse -path ($TextureDir) -include *.dds,*.jpg,*.png,*tga,*.DDS,*.JPG,*.PNG,*.TGA)
{
    $OutFile = $GameDir + "Data\Textures\" + $file.Name.Replace($file.Extension,".dds")

    if($file.Extension -eq ".dds" -or $file.Extension -eq ".DDS")
    {
        Echo ("Copying " + $file.FullName)
        Copy-Item $file.FullName $OutFile
        continue
    }
    
    if($SkipExisting)
    {
        if(Test-Path $OutFile)
        {
            Echo ("Skipping " + $file.FullName)
            continue
        }
    }

    $args = "-nologo -bcmax -m 0 -o " + $GameDir + "Data\Textures\" + " -f "

    if($file.Name.Contains("normal"))
    {        
        $args += $NormalFormat
    }
    elseif($file.Name.Contains("height") -or $file.Name.Contains("displacement"))
    {
        $args += $HeightFormat
    }
	elseif($file.Name.Contains("metallic"))
    {
        $args += $MetallicFormat
    }
	elseif($file.Name.Contains("roughness"))
    {
        $args += $RoughnessFormat
    }
    else    
    {
        $args += $ColorFormat
    }

    $args += " " + $file.FullName

    #Echo $args

    $ProcessInfo.Arguments = $args
    $Process.StartInfo = $ProcessInfo 
    $Process.Start() | Out-Null 
    $Process.WaitForExit() 
    Echo $Process.StandardOutput.ReadToEnd()
    [int]$exitcode = $Process.ExitCode

    if($exitcode -ne 0)
    {
        Echo $args
        Echo "Some textures could not be converted!"
        pause
        return
    }

    $TextureCount++
}

Echo ("Converted " + $TextureCount + " Textures")