param([string]$HelixDir = "C:\Projects\Helix\", [switch]$Debug = $false)

Echo "HLXSLCompiler"
Echo "============="

Echo ("Debug: " + $Debug)

if($HelixDir.Length -eq 0)
{
    Echo "Invalid HelixDir (use SolutionDir\..\)"
    return
}
Echo ("HelixDir: " + $HelixDir)



[string]$HLXSLCompiler = $HelixDir + "ToolBuild\HLXSLCompiler\HLXSLCompilerx64Release.exe"
[string]$EngineSoureDir = $HelixDir + "Source\Helix\"
[string]$GameDir = $HelixDir + "GameBuild\"

if(Test-Path $HLXSLCompiler)
{
    Echo $HLXSLCompiler
}
else
{
    Echo "HLXSLCompiler not found!"
    pause
    return
}

$ProcessInfo = New-Object System.Diagnostics.ProcessStartInfo  
$Process = New-Object System.Diagnostics.Process

Echo "###################################################################################"
Echo "# Compiling Shaders "
Echo "###################################################################################"

if (!(Test-Path -path ($GameDir + "Bin\Shaders\")))
{
    New-Item ($GameDir + "Bin\Shaders\") -Type Directory
}

$ProcessInfo.FileName = $HLXSLCompiler 
$ProcessInfo.RedirectStandardError = $false
$ProcessInfo.RedirectStandardOutput = $false 
$ProcessInfo.UseShellExecute = $false 

foreach ($file in get-ChildItem -recurse -path ($EngineSoureDir + "Display\Shaders\") -include *.hlsl)
{
    
    $args = $file.FullName + " -lzma -o " +$GameDir + "Bin\Shaders\"
	
	if($Debug -eq $true)
	{
		$args += " -dbg"
	}

    $ProcessInfo.Arguments = $args
    $Process.StartInfo = $ProcessInfo 
    $Process.Start() | Out-Null 
    $Process.WaitForExit() 
    #Echo $Process.StandardOutput.ReadToEnd()
	Echo "###################################################################################"
	Echo ("# Compiling " + $file.FullName)
	Echo "###################################################################################"
    [int]$exitcode = $Process.ExitCode
    if($exitcode -ne 0)
    {
        Echo $args
        Echo "Some shaders could not be compiled!"
        pause
        return
    }
}