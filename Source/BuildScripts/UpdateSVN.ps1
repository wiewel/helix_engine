param([string]$HelixDir = "C:\Projects\Helix\", [string]$SVNDir = "")

Echo "###################################################################################"
Echo "# SVN Update"
Echo "###################################################################################"

if($HelixDir.Length -eq 0)
{
    Echo "Invalid HelixDir (use SolutionDir\..\)"
    return
}
Echo ("HelixDir: " + $HelixDir)

[string]$AssetDir = $HelixDir + "Assets\"
if($SVNDir)
{
	$AssetDir += ($SVNDir + "\")
}

if(Test-Path $AssetDir)
{
    Echo $AssetDir
}
else
{
    Echo "AssetDir not found!"
    pause
    return
}

$ProcessInfo = New-Object System.Diagnostics.ProcessStartInfo  
$Process = New-Object System.Diagnostics.Process

$ProcessInfo.FileName = "tortoiseproc"

#tortoiseproc /command:update /closeonend:3 /path:$AssetDir
$ProcessInfo.Arguments = "/command:update /closeonend:2 /path:" + $AssetDir

$ProcessInfo.RedirectStandardError = $true 
$ProcessInfo.RedirectStandardOutput = $true 
$ProcessInfo.UseShellExecute = $false

$Process.StartInfo = $ProcessInfo 
$Process.Start() | Out-Null 
$Process.WaitForExit() 
Echo $Process.StandardOutput.ReadToEnd()
[int]$exitcode = $Process.ExitCode
if($exitcode -ne 0)
{
    Echo "Update failed!"
}
else
{
    Echo "Update finished!"
}
