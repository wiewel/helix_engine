param([string]$HelixDir = "C:\Projects\Helix\", [switch]$CleanShaders = $false)

Echo "CleanGameData"
Echo "============="

Echo ("CleanShaders: " + $CleanShaders)

if($HelixDir.Length -eq 0)
{
    Echo "Invalid HelixDir (use SolutionDir\..\)"
    return
}
Echo ("HelixDir: " + $HelixDir)

[string]$GameDir = $HelixDir + "GameBuild\"

Echo "Removing old objects"

Remove-Item ($GameDir + "Data\Models\*.h3d")
Remove-Item ($GameDir + "Data\Materials\*.hmat")
Remove-Item ($GameDir + "Data\Textures\*.dds")
Remove-Item ($GameDir + "Data\Levels\*.hell")

if($CleanShaders)
{
	Remove-Item ($GameDir + "Bin\Shaders\*.hlxsl")
	Remove-Item ($GameDir + "Bin\Shaders\*.dbg")
}
