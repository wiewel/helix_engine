SET GameDir=%~dp0..\..\GameBuild\
SET ThirdDir=%~dp0..\..\ThirdParty\
SET ToolDir=%~dp0..\..\ToolBuild\

robocopy.exe %ThirdDir%Build %1\GameBuild\ PhysX*Release*.dll
robocopy.exe %ThirdDir%Source\PhysXSDK\Bin\vc14win64 %1\GameBuild\ nvToolsExt64_1.dll

robocopy.exe %ToolDir%H3DTool %1\GameBuild\ H3DToolx64Release.exe
robocopy.exe %ToolDir%TexConvert %1\GameBuild\ TexConvertx64Release.exe
robocopy.exe %ToolDir%TexMerge %1\GameBuild\ TexMergex64Release.exe

robocopy.exe %GameDir%Bin\Shaders\ %1\GameBuild\Bin\Shaders\ *.hlxsl

robocopy.exe %GameDir%Nucleo\Bin\ %1\GameBuild\Nucleo\Bin\ Nucleox64ReleaseDynamicNucleo.exe
robocopy.exe %GameDir%TheRedLine\Bin\ %1\GameBuild\TheRedLine\Bin\ TheRedLinex64ReleaseDynamic.exe

pause