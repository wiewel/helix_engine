﻿//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "SyncDockWidget.hpp"
#include "SyncGroupWidget.hpp"
#include "hlx\src\StandardDefines.h"

#include <qformlayout.h>
#include <qgridlayout.h>
#include <qdesktopwidget.h>

using namespace Helix::Nucleo;
using namespace Helix::Datastructures;

//---------------------------------------------------------------------------------------------------
SyncDockWidget::SyncDockWidget(const SyncDock* _pSyncDock, QWidget* _pParent) :
	QDockWidget(QString::fromStdString(_pSyncDock->sName), _pParent), m_pSyncDock(_pSyncDock)
{
	QDesktopWidget widget;
	QRect mainScreenSize = widget.availableGeometry(widget.primaryScreen());
	m_MaxSize.setWidth(mainScreenSize.width());
	m_MaxSize.setHeight(mainScreenSize.height());

	Initialize(_pSyncDock);
	Update();
}
//---------------------------------------------------------------------------------------------------
SyncDockWidget::~SyncDockWidget()
{
	HSAFE_VECTOR_DELETE(m_Groups);
	HSAFE_DELETE(m_pLayout);
	HSAFE_DELETE(m_pDockGroup);
	HSAFE_DELETE(m_pScrollArea);
}
//---------------------------------------------------------------------------------------------------
void SyncDockWidget::Initialize(const SyncDock* _pSyncDock, bool _bUpdate)
{
	setUpdatesEnabled(false);
	setEnabled(false);

	if (m_pDockGroup == nullptr)
	{
		m_pDockGroup = HNEW QWidget(this);
	}

	if (_bUpdate == false)
	{
		setFloating(_pSyncDock->bFloating);
	}

	if (m_pScrollArea == nullptr)
	{
		m_pScrollArea = HNEW QScrollArea(this);
		m_pScrollArea->setWidgetResizable(true);
	}

	bool bNewLayout = false;

	if (m_pLayout == nullptr || (m_pLayout != nullptr && m_kLayout != _pSyncDock->kLayout))
	{
		HSAFE_DELETE(m_pLayout);

		switch (_pSyncDock->kLayout)
		{
		case WidgetLayout_Horizontal:
			m_pLayout = HNEW QHBoxLayout();
			break;
		case WidgetLayout_Vertical:
			m_pLayout = HNEW QVBoxLayout();
			break;
		case WidgetLayout_Form:
			m_pLayout = HNEW QFormLayout();
			break;
		case WidgetLayout_Grid:
			m_pLayout = HNEW QGridLayout();
			break;
		default:
			m_pLayout = HNEW QHBoxLayout();
			break;
		}

		bNewLayout = true;

		m_kLayout = _pSyncDock->kLayout;
	}

	for (SyncGroupWidget* pGroup : m_Groups)
	{
		m_pLayout->removeWidget(pGroup);
	}

	const std::vector<SyncGroup*>& NewGroups = _pSyncDock->GetGroups();

	uint32_t uCol = std::sqrt(static_cast<float>(NewGroups.size()));

	auto AddToLayout = [&](size_t i, SyncGroupWidget* pGroup)
	{
		if (_pSyncDock->kLayout != WidgetLayout_Grid)
		{
			m_pLayout->addWidget(pGroup);
		}
		else
		{
			static_cast<QGridLayout*>(m_pLayout)->addWidget(pGroup, i % uCol, i / uCol);
		}
	};

	if (NewGroups.size() == m_Groups.size())
	{
		for (size_t i = 0ull; i < m_Groups.size(); ++i)
		{
			m_Groups[i]->Initialize(NewGroups[i]);
			AddToLayout(i, m_Groups[i]);
		}
	}
	else if (NewGroups.size() > m_Groups.size())
	{
		for (size_t i = 0ull; i < m_Groups.size(); ++i)
		{
			m_Groups[i]->Initialize(NewGroups[i]);
			AddToLayout(i, m_Groups[i]);
		}
		for (size_t i = m_Groups.size(); i < NewGroups.size(); ++i)
		{
			SyncGroupWidget* pGroupWidget = HNEW SyncGroupWidget(NewGroups[i], this);
			AddToLayout(i, pGroupWidget);
			m_Groups.push_back(pGroupWidget);
		}
	}
	else // NewGroups.size() < m_Groups.size()
	{
		for (size_t i = 0ull; i < NewGroups.size(); ++i)
		{
			m_Groups[i]->Initialize(NewGroups[i]);
			AddToLayout(i, m_Groups[i]);
		}
		while (m_Groups.size() > NewGroups.size())
		{
			HSAFE_DELETE(m_Groups.back());
			m_Groups.pop_back();
		}
	}

	m_pLayout->setAlignment(Qt::AlignTop);

	if (bNewLayout)
	{
		m_pDockGroup->setLayout(m_pLayout);
	}

	m_pScrollArea->setWidget(m_pDockGroup);

	if (bNewLayout)
	{
		switch (_pSyncDock->kLayout)
		{
		case WidgetLayout_Horizontal:
			static_cast<QHBoxLayout*>(m_pLayout)->addStretch();
			break;
		case WidgetLayout_Vertical:
			static_cast<QVBoxLayout*>(m_pLayout)->addStretch();
			break;
		default:
			break;
		}
	}

	setAllowedAreas(static_cast<Qt::DockWidgetAreas>(_pSyncDock->kAllowedAreas.GetFlag()));

	setWidget(m_pScrollArea);

	m_pSyncDock = _pSyncDock;
	
	AdjustScrollAndDockSize();

	setEnabled(true);
	setUpdatesEnabled(true);
}
//---------------------------------------------------------------------------------------------------
void SyncDockWidget::Update()
{
	if (isVisible() == false)
		return;

	if (m_pSyncDock->GetUpdated())
	{
		Initialize(m_pSyncDock, true);

		for (const SyncGroup* pGroup : m_pSyncDock->GetGroups())
		{
			pGroup->ResetUpdated();
		}

		m_pSyncDock->ResetUpdated();
	}

	QSize uParentSize = parentWidget()->size();
	uParentSize = uParentSize.boundedTo(m_MaxSize);

	for (SyncGroupWidget* pGroup : m_Groups)
	{
		QSize uSize = pGroup->sizeHint();
		if (uSize.isValid())
		{
			uSize = uSize.boundedTo(uParentSize);
			pGroup->setMinimumSize(uSize);
			//pGroup->resize(uSize);
		}
		pGroup->Update();
	}

	AdjustScrollAndDockSize();
}
//---------------------------------------------------------------------------------------------------

void SyncDockWidget::AdjustScrollAndDockSize()
{
	QSize uDockSize = m_pDockGroup->sizeHint();
	QSize uParentSize = parentWidget()->size();
	uParentSize = uParentSize.boundedTo(m_MaxSize);

	QSize uScrollSize = uDockSize.boundedTo(uParentSize * 0.9);
	uDockSize = uDockSize.boundedTo(uParentSize * 0.85);

	//m_pDockGroup->setMinimumSize(uDockSize);
	m_pScrollArea->setMinimumSize(uDockSize);
	
	if (isFloating())
	{
		//m_pDockGroup->setMaximumSize(uParentSize * 1.5);
		m_pScrollArea->setMaximumSize(uParentSize * 1.5);
	}
	else
	{
		//m_pDockGroup->setMaximumSize(uScrollSize);
		m_pScrollArea->setMaximumSize(uScrollSize);
	}
}
//---------------------------------------------------------------------------------------------------
const SyncDock* SyncDockWidget::GetEngineDock() const
{
	return m_pSyncDock;
}
//---------------------------------------------------------------------------------------------------
