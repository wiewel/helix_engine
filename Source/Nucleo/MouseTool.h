//   Copyright 2020 Steffen Wiewel, Fabian Wahlster, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef MOUSETOOL_H
#define MOUSETOOL_H

#include <array>

#include "hlx\src\StandardDefines.h"

#include "Math\MathTypes.h"
#include "Display/DX11/DebugRenderDX11.h"

#include <qstyle.h>
#include <qapplication.h>
#include <qpushbutton.h>
#include "QStringHelper.h"

namespace Helix
{
	namespace Scene
	{
		class CameraComponent;
	}
	namespace Datastructures
	{
		class GameObject;
	}

	namespace Nucleo
	{
		class EditorControlScript;
		class NucleoMouseEvent;

		//---------------------------------------------------------------------------------------------------
		class MouseTool : public QPushButton
		{
			Q_OBJECT

			friend class MouseToolGroup;

		public:
			enum MouseToolType
			{
				kMouseToolType_Select = 0,
				kMouseToolType_Move,
				kMouseToolType_Resize,
				kMouseToolType_Rotate,
				kMouseToolType_NumOf
			};
			const std::array<hlx::string, kMouseToolType_NumOf> MouseToolTypeNames =
			{
				S("Select"),
				S("Move"),
				S("Resize"),
				S("Rotate")
			};

			enum MovementAxis : uint32_t
			{
				kMovementAxis_X = 0,
				kMovementAxis_Y,
				kMovementAxis_Z,
				kMovementAxis_NumOf,
				kMovementAxis_Inactive
			};

		public:
			MouseTool(MouseToolType _kType, const QIcon& _Icon, QWidget* _pParent = Q_NULLPTR);
			~MouseTool();

		public:
			void HandleMouseEvent(const NucleoMouseEvent* _pEvent, const EditorControlScript* _pControlScript = nullptr);

			Datastructures::GameObject* GetActiveSelection() const;
			void ResetActiveSelection();

		protected:
			void ShowMarker(bool _bShow);

			bool IsNewActivation() const;
			bool IsActiveSelectionValid() const;

			void SetActiveSelection(Datastructures::GameObject* _pActiveSelection, const EditorControlScript* _pControlScript);
			
			const Math::float2& GetLastMousePosition() const;
			const MovementAxis GetMovementAxis() const;

			void SetMarkerTransform(Math::transform& _NewTransform);

		private:
			void Activate(_In_ Datastructures::GameObject*& _pActiveSelection);
			void Disable(_Outptr_ Datastructures::GameObject*& _pActiveSelection);
			void Update(_In_ const Scene::CameraComponent* _pCamera);

		private:
			virtual void OnActivate() {}
			virtual void OnDisable() {}
			virtual void OnUpdate(const Scene::CameraComponent* _pCamera) {}
			virtual std::array<Math::transform, kMovementAxis_NumOf> OnSetMarkerTransform(Math::transform& _NewTransform);

			virtual void SpawnMovementMarkers() = 0;
			virtual void OnMouseEvent(const NucleoMouseEvent* _pEvent, const EditorControlScript* _pControlScript) = 0;

		public:
			const MouseToolType m_kType;
			const QSize m_Size = QSize(24, 24);

		protected:
			Math::transform m_MarkerWorldTransform;

			const Scene::CameraComponent* m_pCamera = nullptr;
			std::array<Datastructures::GameObject*, kMovementAxis_NumOf> m_MovementMarker;

			float m_fGizmoScale = 7.0f;

		private:
			Datastructures::GameObject* m_pActiveSelection;
			Display::DebugRenderDX11::LineIndices m_SelectionVisualization;

			MovementAxis m_kActiveAxis = kMovementAxis_Inactive;
			Math::float2 m_vLastMousePos = Math::float2(0, 0);

			bool m_bNewActivation = true;
		};
		//---------------------------------------------------------------------------------------------------
		inline Datastructures::GameObject* MouseTool::GetActiveSelection() const { return m_pActiveSelection; }
		inline void MouseTool::ResetActiveSelection() { m_pActiveSelection = nullptr; }
		inline bool MouseTool::IsNewActivation() const { return m_bNewActivation; }
		inline const Helix::Math::float2& MouseTool::GetLastMousePosition() const { return m_vLastMousePos; }
		inline const MouseTool::MovementAxis MouseTool::GetMovementAxis() const { return m_kActiveAxis; }
	}
} // Helix

#endif // MOUSETOOL_H