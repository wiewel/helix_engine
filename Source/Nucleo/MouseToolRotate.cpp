//   Copyright 2020 Steffen Wiewel, Fabian Wahlster, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "MouseToolRotate.h"

#include "EditorControlScript.h"
#include "NucleoMouseEvent.h"
#include "Display\DX11\CommonRenderPasses.h"

using namespace Helix::Nucleo;
using namespace Helix::Datastructures;
using namespace Helix::Math;
using namespace Helix::Scene;

//---------------------------------------------------------------------------------------------------
MouseToolRotate::MouseToolRotate(QWidget* _pParent):
	MouseTool(kMouseToolType_Rotate,
	          qApp->style()->standardIcon(QStyle::SP_BrowserReload),
	          _pParent)
{
	SpawnMovementMarkers();

	Display::MaterialProperties MatProp;
	MatProp.kRenderPasses = Display::g_kGizmoPass();
	MatProp.fRoughness = 1.0f;
	MatProp.fMetallic = 0.0f;

	m_pZDirectionArrow = GameObjectPool::Instance()->Create().get();
	if (m_pZDirectionArrow != nullptr)
	{
		const float fLength = 1.0f;
		std::string sName = "ZDirectionArrow";
		float3 vColor = { 0.1f, 0.1f, 0.9f };
		float3 vOffset = { fLength, 0.0f, 0.0f };
		quaternion vOrientation = quaternion();
		float3 vExtents = { fLength, 0.1f, 0.1f };

		m_pZDirectionArrow->SetMesh(Display::MeshClusterDX11("Arrow"));
		m_pZDirectionArrow->CreatePhysicsActorDynamic(transform(vOffset, vOrientation), 1.0f, true);
		MatProp.vAlbedo = vColor;

		m_pZDirectionArrow->SetName(sName);

		Physics::GeometryDesc QueryShape;
		QueryShape.kType = Physics::GeometryType_Box;
		QueryShape.Box.vHalfExtents = vExtents;
		QueryShape.kUsage = Physics::ShapeUsageType_Query;
		QueryShape.Filter = Physics::CollisionFilter(Physics::CollisionFilterFlag_None);
		m_pZDirectionArrow->AddShape(QueryShape);

		m_pZDirectionArrow->AddToScene();

		Display::MaterialDesc MatDesc(MatProp, sName + "Mat", false, false);
		Display::MaterialDX11 Mat(MatDesc);

		m_pZDirectionArrow->SetMaterial(Mat);
	}
	ShowDirectionArrow(false);
}
//---------------------------------------------------------------------------------------------------
MouseToolRotate::~MouseToolRotate()
{
	if (m_pZDirectionArrow != nullptr)
	{
		m_pZDirectionArrow->Destroy();
		m_pZDirectionArrow = nullptr;
	}
}
//---------------------------------------------------------------------------------------------------
void MouseToolRotate::SpawnMovementMarkers()
{
	for (uint32_t i = 0u; i < kMovementAxis_NumOf; ++i)
	{
		Display::MaterialProperties MatProp;
		MatProp.kRenderPasses = Display::g_kGizmoPass();
		MatProp.fRoughness = 1.0f;
		MatProp.fMetallic = 0.0f;

		GameObject* pGO = GameObjectPool::Instance()->Create().get();
		if (pGO != nullptr)
		{
			const float fLength = 1.0f;
			std::string sName;
			float3 vColor;
			float3 vOffset;
			quaternion vOrientation;
			float3 vExtents = { fLength, 0.1f, 0.1f };
			switch (i)
			{
			case kMovementAxis_X:
				sName = "MovementMarkerX";
				//vExtents = { fLength, 0.25f, 0.25f };
				vOffset = { fLength, 0.0f, 0.0f };
				vColor = { 1.0f, 0.0f, 0.0f };
				vOrientation = quaternion();
				break;
			case kMovementAxis_Y:
				sName = "MovementMarkerY";
				//vExtents = { 0.25f, fLength, 0.25f };
				vOffset = { 0.0f, fLength, 0.0f };
				vColor = { 0.0f, 1.0f, 0.0f };
				vOrientation = QuaternionFromEulerDegreesXYZ(0.0f, 0.0f, 90.0f);
				break;
			case kMovementAxis_Z:
				sName = "MovementMarkerZ";
				//vExtents = { 0.25f, 0.25f, fLength };
				vOffset = { 0.0f, 0.0f, fLength };
				vColor = { 0.0f, 0.0f, 1.0f };
				vOrientation = QuaternionFromEulerDegreesXYZ(0.0f, -90.0f, 0.0f);
				break;
			default:
				break;
			}

			pGO->SetMesh(Display::MeshClusterDX11("Arrow"));
			pGO->CreatePhysicsActorDynamic(transform(vOffset, vOrientation), 1.0f, true);
			MatProp.vAlbedo = vColor;

			pGO->SetName(sName);

			Physics::GeometryDesc QueryShape;
			QueryShape.kType = Physics::GeometryType_Box;
			QueryShape.Box.vHalfExtents = vExtents;
			QueryShape.kUsage = Physics::ShapeUsageType_Query;
			QueryShape.Filter = Physics::CollisionFilter(Physics::CollisionFilterFlag_MouseTool);
			pGO->AddShape(QueryShape);

			pGO->AddToScene();

			Display::MaterialDesc MatDesc(MatProp, sName + "Mat", false, false);
			Display::MaterialDX11 Mat(MatDesc);

			pGO->SetMaterial(Mat);
		}

		m_MovementMarker[i] = pGO;
	}
	ShowMarker(false);
}
//---------------------------------------------------------------------------------------------------
void MouseToolRotate::OnMouseEvent(const NucleoMouseEvent* _pEvent, const EditorControlScript* _pControlScript)
{
	float2 vMousePos = _pEvent->GetMousePosition();

	/// only execute this code when left button is already (and still) down and not when the event is fired!
	if (_pEvent->GetMouseButtonDown() != Qt::LeftButton &&
		_pEvent->GetNativeEvent().buttons() & Qt::LeftButton &&
		GetMovementAxis() != kMovementAxis_Inactive)
	{
		GameObject* pActiveSelection = GetActiveSelection();

		float3 vAxis;
		switch (GetMovementAxis())
		{
		case kMovementAxis_X:
			vAxis = { 1.0f, 0.0f, 0.0f };
			break;
		case kMovementAxis_Y:
			vAxis = { 0.0f, 1.0f, 0.0f };
			break;
		case kMovementAxis_Z:
			vAxis = { 0.0f, 0.0f, 1.0f };
			break;
		default:
			HASSERT(false, "Rotation Tool Failure");
			break;
		}
		float2 vMouseMovement = (GetLastMousePosition() - vMousePos);

		float fAngle = Deg2Rad(vMouseMovement.magnitude() * 200.0f);
		fAngle = copysignf(fAngle, float2(1.0f, 0.0f).dot(vMouseMovement));

		quaternion vQuat(fAngle, vAxis);
		pActiveSelection->Rotate(vQuat, false);
	}
}
//---------------------------------------------------------------------------------------------------
std::array<transform, MouseTool::kMovementAxis_NumOf> MouseToolRotate::OnSetMarkerTransform(transform& _NewTransform)
{
	std::array<transform, kMovementAxis_NumOf> ScaledTrans;

	for (uint32_t i = 0u; i < kMovementAxis_NumOf; ++i)
	{
		if (m_MovementMarker[i] != nullptr)
		{
			ScaledTrans[i] = m_MovementMarker[i]->Transform;
			ScaledTrans[i].p = m_MarkerWorldTransform.p;
		}
	}

	if (m_pCamera != nullptr)
	{
		const float fNearFarDist = m_pCamera->GetFarDistance() - m_pCamera->GetNearDistance();
		FrustumCorners Corners = m_pCamera->GetFrustumCorners();
		float fObjDepth = m_pCamera->GetProjectedPointOnNearPlane(_NewTransform.p).distance(_NewTransform.p);
		fObjDepth /= fNearFarDist;

		/// Calculate side vectors of frustum with object distance as length -> shortened frustum
		float3 vUpperLeft = Corners[kFrustumCorner_NearUpperLeft] + (Corners[kFrustumCorner_FarUpperLeft] - Corners[kFrustumCorner_NearUpperLeft]) * fObjDepth;
		float3 vUpperRight = Corners[kFrustumCorner_NearUpperRight] + (Corners[kFrustumCorner_FarUpperRight] - Corners[kFrustumCorner_NearUpperRight]) * fObjDepth;
		float3 vLowerLeft = Corners[kFrustumCorner_NearLowerLeft] + (Corners[kFrustumCorner_FarLowerLeft] - Corners[kFrustumCorner_NearLowerLeft]) * fObjDepth;

		float3 vRight = vUpperRight - vUpperLeft;
		float3 vDown = vLowerLeft - vUpperLeft;

		float3 vToObj = _NewTransform.p - vUpperLeft;

		float3 vAxis = vToObj.projectOnto(vRight);
		float fDistanceX = copysignf(vAxis.magnitude() / vRight.magnitude(), vAxis.dot(vRight));

		vAxis = vToObj.projectOnto(vDown);
		float fDistanceY = copysignf(vAxis.magnitude() / vDown.magnitude(), vAxis.dot(vDown));

		/// Build new position on given depth
		float fNewDepth = m_fGizmoScale / fNearFarDist;
		vUpperLeft = Corners[kFrustumCorner_NearUpperLeft] + (Corners[kFrustumCorner_FarUpperLeft] - Corners[kFrustumCorner_NearUpperLeft]) * fNewDepth;
		vUpperRight = Corners[kFrustumCorner_NearUpperRight] + (Corners[kFrustumCorner_FarUpperRight] - Corners[kFrustumCorner_NearUpperRight]) * fNewDepth;
		vLowerLeft = Corners[kFrustumCorner_NearLowerLeft] + (Corners[kFrustumCorner_FarLowerLeft] - Corners[kFrustumCorner_NearLowerLeft]) * fNewDepth;

		vRight = vUpperRight - vUpperLeft;
		vDown = vLowerLeft - vUpperLeft;

		float3 vNewPos = vUpperLeft + vRight * fDistanceX;
		vNewPos += vDown * fDistanceY;

		for (uint32_t i = 0u; i < kMovementAxis_NumOf; ++i)
		{
			if (m_MovementMarker[i] != nullptr)
			{
				ScaledTrans[i].p = vNewPos;
			}
		}

		GameObject* pActiveSelection = GetActiveSelection();
		if(pActiveSelection != nullptr)
		{
			m_pZDirectionArrow->Orientation = pActiveSelection->Orientation * QuaternionFromEulerDegreesXYZ(0.0f, -90.0f, 0.0f);
			m_pZDirectionArrow->Position = vNewPos;
		}
	}
	return ScaledTrans;
}
//---------------------------------------------------------------------------------------------------
void MouseToolRotate::ShowDirectionArrow(bool _bShow)
{
	if (m_pZDirectionArrow != nullptr)
	{
		if (_bShow)
		{
			m_pZDirectionArrow->ClearFlags(GameObjectFlag_Invisible);
			m_pZDirectionArrow->ActivatePhysicsQuery(true);
		}
		else
		{
			m_pZDirectionArrow->SetFlags(GameObjectFlag_Invisible);
			m_pZDirectionArrow->ActivatePhysicsQuery(false);
		}
	}
}
//---------------------------------------------------------------------------------------------------
void MouseToolRotate::OnActivate()
{
	ShowDirectionArrow(true);
}
//---------------------------------------------------------------------------------------------------
void MouseToolRotate::OnDisable()
{
	ShowDirectionArrow(false);
}
//---------------------------------------------------------------------------------------------------