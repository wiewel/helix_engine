﻿//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef VIEWPORT_HPP
#define VIEWPORT_HPP

#include "Engine\D3DWidget.hpp"

namespace Helix
{
	namespace Nucleo
	{
		class Viewport : public Engine::D3DWidget
		{
			Q_OBJECT

		public:
			Viewport(QWidget * parent = Q_NULLPTR);
			~Viewport();

		private:
			void mousePressEvent(QMouseEvent* _pEvent) final;
			void mouseMoveEvent(QMouseEvent* _pEvent) final;

			void MouseEvent(QMouseEvent* _pEvent);
		};
	} // Nucleo
} // Helix

#endif // VIEWPORT_HPP