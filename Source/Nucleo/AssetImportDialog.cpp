﻿//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "AssetImportDialog.hpp"
#include "hlx\src\StandardDefines.h"
#include "Resources\FileSystem.h"
#include "QStringHelper.h"

using namespace Helix::H3D;
using namespace Helix::Display;
using namespace Helix::Nucleo;

//---------------------------------------------------------------------------------------------------

AssetImportDialog::AssetImportDialog() : QObject()
{
	hlx::string sBasePath = Resources::FileSystem::Instance()->GetKnownDirectoryPath(Resources::HelixDirectories_BasePath);

	QString sDir = fromhlxString(sBasePath + S("..\\Assets"));

	m_pFileDialog = HNEW QFileDialog(nullptr, "Asset Import", sDir, "Mesh Files (*.fbx *.obj *.png *.tga *.jpg *.dds)");
	m_pFileDialog->setFileMode(QFileDialog::ExistingFiles);
	m_pFileDialog->setOption(QFileDialog::DontUseNativeDialog, true);
	connect(m_pFileDialog, SIGNAL(finished(int)), this, SLOT(Hide(int)));

	QLayout* pLayout = m_pFileDialog->layout();

	m_pToLeftHanded = HNEW QCheckBox("To Lefthanded");
	m_pToLeftHanded->setChecked(true);
	pLayout->addWidget(m_pToLeftHanded);
	
	m_pFlipV = HNEW QCheckBox("Flip V");
	m_pFlipV->setChecked(true);
	pLayout->addWidget(m_pFlipV);

	m_pReverseWinding = HNEW QCheckBox("Reverse Winding");
	m_pReverseWinding->setChecked(true);
	pLayout->addWidget(m_pReverseWinding);

	m_pConvexHull = HNEW QCheckBox("Create Convex Hull");
	m_pConvexHull->setChecked(true);
	pLayout->addWidget(m_pConvexHull);

	m_pImportTextures = HNEW QCheckBox("Import Textures");
	m_pImportTextures->setChecked(true);
	pLayout->addWidget(m_pImportTextures);

	m_pImportMaterials = HNEW QCheckBox("Import Materials");
	m_pImportMaterials->setChecked(true);
	pLayout->addWidget(m_pImportMaterials);

	m_pMipLabel = HNEW QLabel("Mip Level");
	pLayout->addWidget(m_pMipLabel);

	m_pMipLevel = HNEW QSpinBox();
	m_pMipLevel->setValue(0);
	m_pMipLevel->setRange(0, 10);
	m_pMipLevel->setSingleStep(1);
	pLayout->addWidget(m_pMipLevel);

	m_pScaleLabel = HNEW QLabel("Mesh Scale");
	pLayout->addWidget(m_pScaleLabel);

	m_pScale = HNEW QDoubleSpinBox();
	m_pScale->setValue(1.0);
	m_pScale->setRange(0.0, 10000.0);
	pLayout->addWidget(m_pScale);

	m_pCoordSystem = HNEW QComboBox();
	for (size_t i = 0u; i < _countof(CoordSystems); ++i)
	{
		m_pCoordSystem->addItem(QString(CoordSystems[i].sName));
	}
	m_pCoordSystem->setCurrentIndex(0);
	pLayout->addWidget(m_pCoordSystem);

	m_pVertexLayout = HNEW QComboBox();
	for (size_t i = 0u; i < _countof(Layouts); ++i)
	{
		m_pVertexLayout->addItem(QString(Layouts[i].sName));
	}
	m_pVertexLayout->setCurrentIndex(2);
	pLayout->addWidget(m_pVertexLayout);

	m_pReversePivot = HNEW QCheckBox("Revese Pivot");
	m_pReversePivot->setChecked(false);
	pLayout->addWidget(m_pReversePivot);

	m_pNullPivot = HNEW QCheckBox("Null Pivot");
	m_pNullPivot->setChecked(false);
	pLayout->addWidget(m_pNullPivot);
}
//---------------------------------------------------------------------------------------------------

AssetImportDialog::~AssetImportDialog()
{
	HSAFE_DELETE(m_pImportMaterials);
	HSAFE_DELETE(m_pImportTextures);
	HSAFE_DELETE(m_pToLeftHanded);
	HSAFE_DELETE(m_pFlipV);
	HSAFE_DELETE(m_pReverseWinding);
	HSAFE_DELETE(m_pReversePivot);
	HSAFE_DELETE(m_pNullPivot);
	HSAFE_DELETE(m_pConvexHull);
	HSAFE_DELETE(m_pScaleLabel);
	HSAFE_DELETE(m_pScale);
	HSAFE_DELETE(m_pVertexLayout);
	HSAFE_DELETE(m_pCoordSystem);
	HSAFE_DELETE(m_pMipLevel);
	HSAFE_DELETE(m_pMipLabel);

	HSAFE_DELETE(m_pFileDialog);
}
//---------------------------------------------------------------------------------------------------

bool AssetImportDialog::Show()
{
	m_pFileDialog->setVisible(true);
	return m_pFileDialog->exec() != 0;
}
//---------------------------------------------------------------------------------------------------

void AssetImportDialog::Hide(int)
{
	m_pFileDialog->setVisible(false);
	m_pFileDialog->close();
}
//---------------------------------------------------------------------------------------------------

std::vector<hlx::string> AssetImportDialog::GetSelectedFiles() const
{
	std::vector<hlx::string> Files;

	for (QString qStr : m_pFileDialog->selectedFiles())
	{
		Files.push_back(hlx::to_string(qStr.toStdWString()));
	}

	return Files;
}
//---------------------------------------------------------------------------------------------------

VertexLayout AssetImportDialog::GetVertexLayout() const
{
	return Layouts[m_pVertexLayout->currentIndex()].kLayout;
}
//---------------------------------------------------------------------------------------------------

CoordinateSystem AssetImportDialog::GetCoordinateSystem() const
{
	return CoordSystems[m_pCoordSystem->currentIndex()].kCoordSystem;
}
//---------------------------------------------------------------------------------------------------

bool AssetImportDialog::GetToLefthanded() const
{
	return m_pToLeftHanded->isChecked();
}
//---------------------------------------------------------------------------------------------------

bool AssetImportDialog::GetFilpV() const
{
	return m_pFlipV->isChecked();
}
//---------------------------------------------------------------------------------------------------

bool AssetImportDialog::GetReverseWinding() const
{
	return m_pReverseWinding->isChecked();
}
//---------------------------------------------------------------------------------------------------

bool AssetImportDialog::GetConvexHull() const
{
	return m_pConvexHull->isChecked();
}
//---------------------------------------------------------------------------------------------------
bool AssetImportDialog::GetImportTextures() const
{
	return m_pImportTextures->isChecked();
}
//---------------------------------------------------------------------------------------------------

bool AssetImportDialog::GetImportMaterials() const
{
	return m_pImportMaterials->isChecked();
}
//---------------------------------------------------------------------------------------------------

bool AssetImportDialog::GetReversePivot() const
{
	return m_pReversePivot->isChecked();
}
//---------------------------------------------------------------------------------------------------
bool AssetImportDialog::GetNullPivot() const
{
	return m_pNullPivot->isChecked();
}
//---------------------------------------------------------------------------------------------------
int AssetImportDialog::GetMipLevel() const
{
	return m_pMipLevel->value();
}
//---------------------------------------------------------------------------------------------------

float AssetImportDialog::GetScale() const
{
	return static_cast<float>(m_pScale->value());
}
//---------------------------------------------------------------------------------------------------
