//   Copyright 2020 Steffen Wiewel, Fabian Wahlster, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef MOUSETOOLMOVE_H
#define MOUSETOOLMOVE_H

#include "MouseTool.h"

#include "Display\DX11\CommonRenderPasses.h"
#include "Scene\CameraComponent.h"
#include "Math\MathTypes.h"

namespace Helix
{
	namespace Nucleo
	{
		//---------------------------------------------------------------------------------------------------
		class MouseToolMove : public MouseTool
		{
			Q_OBJECT

		public:
			MouseToolMove(QWidget* _pParent = Q_NULLPTR);
			~MouseToolMove();

		private:
			void SpawnMovementMarkers() final;
			void OnMouseEvent(const NucleoMouseEvent* _pEvent, const EditorControlScript* _pControlScript) final;
			std::array<Math::transform, kMovementAxis_NumOf> OnSetMarkerTransform(Math::transform& _NewTransform) final;
		};

		//---------------------------------------------------------------------------------------------------
	}
} // Helix

#endif // MOUSETOOLMOVE_H
