﻿//   Copyright 2020 Steffen Wiewel, Fabian Wahlster, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef NUCLEOMOUSEEVENT_H
#define NUCLEOMOUSEEVENT_H

#include <QWidget>
#include <QEvent>
#include <QMouseEvent>
#include "Math\MathTypes.h"

namespace Helix
{
	namespace Nucleo
	{
		const QEvent::Type kNucleoMouseEvent = static_cast<QEvent::Type>(QEvent::User + 1);

		class NucleoMouseEvent : public QEvent
		{
		public:
			NucleoMouseEvent(const QMouseEvent& _MouseEvent, const QWidget& _Caller);
			~NucleoMouseEvent();

			const Math::float2& GetMousePosition() const;
			Qt::MouseButtons GetMouseButtonDown() const;
			const QMouseEvent& GetNativeEvent() const;

		private:
			Math::float2 m_vMousePosition;
			const QMouseEvent& m_MouseEvent;
		};

		inline Qt::MouseButtons NucleoMouseEvent::GetMouseButtonDown() const { return m_MouseEvent.button(); }
		inline const Math::float2& NucleoMouseEvent::GetMousePosition() const { return m_vMousePosition; }
		inline const QMouseEvent& NucleoMouseEvent::GetNativeEvent() const { return m_MouseEvent; }
	} // Nucleo
} // Helix

#endif // !NUCLEOMOUSEEVENT_H

