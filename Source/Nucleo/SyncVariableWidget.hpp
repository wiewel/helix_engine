﻿//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef SYNCVARIABLEWIDGET_H
#define SYNCVARIABLEWIDGET_H

#include <QWidget>
#include <qlabel.h>
#include <qlineedit.h>
#include <qcheckbox.h>
#include <qcombobox.h>
#include <qlayout.h>
#include <qpushbutton.h>
#include "DataStructures\SyncVariable.h"

namespace Helix
{
	//namespace Datastructures
	//{
	//	struct SyncVariableBase;
	//}

	namespace Nucleo
	{
		// forward decls
		class SyncListVariableWidget;
		class SyncLineChartWidget;

		class SyncVariableWidget : public QWidget
		{
			Q_OBJECT;
			static constexpr uint32_t kLineEdits = 4;
		public:
			SyncVariableWidget(Datastructures::SyncVariableBase* _pSyncVariable, QWidget* _pParent = Q_NULLPTR);
			~SyncVariableWidget();

			void Initialize(Datastructures::SyncVariableBase* _pSyncVariable);
			void Update();
		private:

		private slots:
			void EditingFinished();
			void PushButtonClicked();
			void CheckBoxStateChanged(int);
			void ComboBoxIndexChanged(int);
			
		private:
			Datastructures::SyncVariableBase* m_pVariable;
			QLabel* m_pLabel = nullptr;
			QLineEdit* m_pLineEdits[kLineEdits];
			QCheckBox* m_pCheckBox = nullptr;
			QComboBox* m_pComboBox = nullptr;
			QPushButton* m_pPushButton = nullptr;
			QLayout* m_pLayout = nullptr;

			SyncListVariableWidget* m_pSyncList = nullptr;
			SyncLineChartWidget* m_pSyncLineChart = nullptr;

			//line edits changed
			bool m_bEdited = false;

			Datastructures::WidgetLayout m_kLayout = Datastructures::WidgetLayout_Horizontal;
		};
	} // Nucleo
} // Helix



#endif // !SYNCVARIABLEWIDGET_H


