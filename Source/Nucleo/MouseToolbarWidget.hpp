﻿//   Copyright 2020 Steffen Wiewel, Fabian Wahlster, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef MOUSETOOLBARWIDGET_HPP
#define MOUSETOOLBARWIDGET_HPP

#include <array>
#include <qdockwidget.h>
#include <qlistwidget.h>
#include <qbuttongroup.h>

#include "MouseTool.h"

namespace Helix
{
	namespace Nucleo
	{
		class NucleoMouseEvent;
		class EditorControlScript;

		//---------------------------------------------------------------------------------------------------
		class MouseToolGroup : public QButtonGroup
		{
			Q_OBJECT

		public:
			MouseToolGroup(QObject* parent = Q_NULLPTR) : QButtonGroup(parent)
			{
				connect(this, SIGNAL(buttonToggled(QAbstractButton*, bool)), this, SLOT(Toggled(QAbstractButton*, bool)));
			}
			~MouseToolGroup()
			{
			}

		public:
			Datastructures::GameObject* GetActiveSelection() const
			{
				MouseTool* pActiveTool = static_cast<MouseTool*>(checkedButton());
				if(pActiveTool != nullptr)
				{
					return pActiveTool->GetActiveSelection();
				}
				return nullptr;
			}
			void Update(const Scene::CameraComponent* _pCamera)
			{
				MouseTool* pTool = static_cast<MouseTool*>(checkedButton());
				if (pTool != nullptr)
				{
					pTool->Update(_pCamera);
				}
			}


		private slots:
			void Toggled(QAbstractButton* _pButton, bool _bEnabled)
			{
				if(_bEnabled)
				{
					static_cast<MouseTool*>(_pButton)->Activate(m_pActiveSelection);
				}
				else
				{
					static_cast<MouseTool*>(_pButton)->Disable(m_pActiveSelection);
				}
			}

		private:
			Datastructures::GameObject* m_pActiveSelection = nullptr;
		};
		//---------------------------------------------------------------------------------------------------
		class MouseToolbarWidget : public QListWidget
		{
			Q_OBJECT

		public:
			MouseToolbarWidget(QWidget* _pParent = Q_NULLPTR);
			~MouseToolbarWidget();

		public:
			void HandleMouseEvent(const NucleoMouseEvent* _pEvent, const EditorControlScript* _pControlScript = nullptr);

			void Update(const Scene::CameraComponent* _pCamera);

		private:
			void Initialize();

		private:
			MouseToolGroup* m_pToolGroup = nullptr;
			std::vector<MouseTool*> m_MouseTools;
		};

		//---------------------------------------------------------------------------------------------------
		inline void MouseToolbarWidget::Update(const Scene::CameraComponent* _pCamera)
		{
			if (m_pToolGroup != nullptr)
			{
				m_pToolGroup->Update(_pCamera);
			}
		}
		//---------------------------------------------------------------------------------------------------
	}
} // Helix

#endif // MOUSETOOLBARWIDGET_HPP