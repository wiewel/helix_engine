//   Copyright 2020 Steffen Wiewel, Fabian Wahlster, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "MouseToolSelect.h"

#include "EditorControlScript.h"
#include "NucleoMouseEvent.h"

using namespace Helix::Nucleo;
using namespace Helix::Datastructures;
using namespace Helix::Math;

//---------------------------------------------------------------------------------------------------
MouseToolSelect::MouseToolSelect(QWidget* _pParent) :
	MouseTool(kMouseToolType_Select,
	          qApp->style()->standardIcon(QStyle::SP_ArrowForward),
	          _pParent)
{
}
//---------------------------------------------------------------------------------------------------
MouseToolSelect::~MouseToolSelect()
{
}
//---------------------------------------------------------------------------------------------------
void MouseToolSelect::OnMouseEvent(const NucleoMouseEvent* _pEvent, const EditorControlScript* _pControlScript)
{
	switch (_pEvent->GetMouseButtonDown())
	{
	case Qt::LeftButton:
		if (_pControlScript != nullptr)
		{
			SetActiveSelection(_pControlScript->ScenePicking(_pEvent->GetMousePosition()), _pControlScript);
			if (IsActiveSelectionValid())
			{
				//{
				//	Math::float3 vRayWorldNorm = _pControlScript->GetCamera()->ScreenPosToWorldRay(_pEvent->GetMousePosition());
				//	Math::float3 vStart = _pControlScript->GetCamera()->GetPosition();
				//	Math::float3 vEnd = vStart + vRayWorldNorm * 100.0f;
				//	Display::DebugRenderDX11::Instance()->DrawLine(vStart, vEnd);
				//}

				GetActiveSelection()->BindToNucleo();
			}
		}
		break;
	case Qt::RightButton:
		break;
	case Qt::MiddleButton:
		break;
	default:
		break;
	}
}
//---------------------------------------------------------------------------------------------------
void MouseToolSelect::SpawnMovementMarkers()
{
	ShowMarker(false);
}
//---------------------------------------------------------------------------------------------------
