﻿//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef PROGRESSWIDGET_H
#define PROGRESSWIDGET_H

#include <QWidget>
#include "Util\NucleoProgressFunction.h"
#include <qprogressbar.h>
#include "Async\SpinLock.h"
#include <qlayout.h>

namespace Helix
{
	namespace Nucleo
	{
		class ProgressWidget : public QWidget
		{
			Q_OBJECT

		public:
			ProgressWidget(QWidget* _pParent = Q_NULLPTR);
			~ProgressWidget();

			const TProgressFunctor& GetProgressFunctor() const;
			void UpdateProgess(const hlx::string& _sText, int32_t _iValue, int32_t _iMin, int32_t _iMax);

		signals:

			void SetProgressValue(int _iValue);
			void SetProgressRange(int _iMin, int _iMax);
			void SetProgressLabel(const QString& _sText);

		private slots:
			void SetProgressLabelSlot(const QString& _sText);

		private:
			QHBoxLayout* m_pLayout = nullptr;
			TProgressFunctor m_UpdateFunc;
			QProgressBar* m_pProgressBar = nullptr;
			Async::SpinLock m_ProgressLock;
		};

		inline const TProgressFunctor& ProgressWidget::GetProgressFunctor() const
		{
			return m_UpdateFunc;
		}

	} // Nucleo
} // Helix

#endif // !PROGRESSWIDGET_H

