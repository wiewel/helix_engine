﻿//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef SYNCDOCKWIDGET_HPP
#define SYNCDOCKWIDGET_HPP

#include <qdockwidget.h>
#include <qlayout.h>
#include <qscrollarea.h>
#include "DataStructures\SyncVariable.h"

namespace Helix
{
	namespace Nucleo
	{		
		class SyncGroupWidget;

		class SyncDockWidget : public QDockWidget
		{
			Q_OBJECT

		public:
			SyncDockWidget(const Datastructures::SyncDock* _pSyncDock, QWidget* _pParent = Q_NULLPTR);
			~SyncDockWidget();
		
			const Datastructures::SyncDock* GetEngineDock() const;

			void Initialize(const Datastructures::SyncDock* _pSyncDock, bool _bUpdate = false);

			// update groups
			void Update();

		private:
			void AdjustScrollAndDockSize();

		private:
			const Datastructures::SyncDock* m_pSyncDock;
			std::vector<SyncGroupWidget*> m_Groups;
			QLayout* m_pLayout = nullptr;
			QWidget *m_pDockGroup = nullptr;
			QScrollArea *m_pScrollArea = nullptr;
			QSize m_MaxSize = QSize(1920, 1080);

			Datastructures::WidgetLayout m_kLayout = Datastructures::WidgetLayout_Vertical;
		};
	}
} // Helix


#endif // SYNCDOCKWIDGET_HPP