﻿//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "Viewport.hpp"
#include "NucleoMouseEvent.h"
#include <QMouseEvent>
#include <qapplication.h>

using namespace Helix;
using namespace Helix::Nucleo;

//---------------------------------------------------------------------------------------------------

Viewport::Viewport(QWidget* _pParent) : D3DWidget(false, _pParent)
{
	Initialize(S("Nucleo"));
}
//---------------------------------------------------------------------------------------------------

Viewport::~Viewport()
{
}
//---------------------------------------------------------------------------------------------------


///---------------------------------------------------------------------------------------------------
/// Events
///---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
void Viewport::mousePressEvent(QMouseEvent* _pEvent)
{
	MouseEvent(_pEvent);
	setFocus();
}
//---------------------------------------------------------------------------------------------------
void Viewport::mouseMoveEvent(QMouseEvent* _pEvent)
{
	MouseEvent(_pEvent);
}
//---------------------------------------------------------------------------------------------------
void Viewport::MouseEvent(QMouseEvent* _pEvent)
{
	if (_pEvent != nullptr)
	{
		QApplication::postEvent(parent(), new NucleoMouseEvent(*_pEvent, *this));
	}
}
//---------------------------------------------------------------------------------------------------
