//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "Nucleo.h"
#include <QtWidgets/QApplication>

int main(int argc, char *argv[])
{
	HInitDebug();
	//HBreakOnAlloc(255953);
	//HBreakOnAlloc(283211);
	//HBreakOnAlloc(292350);

	QApplication a(argc, argv);
	a.setOrganizationName("HELiX Corporation");
	a.setOrganizationDomain("helix.graphics");
	a.setApplicationName("nucleo");

	QString sLevelName = "level";

	if (argc > 1)
	{
		sLevelName = QString(argv[1]);
	}

	Helix::Nucleo::Nucleo Nucleo(sLevelName);
	Nucleo.show();
	return a.exec();
}
