﻿//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "SyncGroupWidget.hpp"
#include "hlx\src\StandardDefines.h"
#include "SyncVariableWidget.hpp"
#include <qformlayout.h>
#include <qgridlayout.h>

using namespace Helix::Nucleo;
using namespace Helix::Datastructures;
//---------------------------------------------------------------------------------------------------

SyncGroupWidget::SyncGroupWidget(SyncGroup* _pSyncGroup, QDockWidget* _pParent) :
	QGroupBox(QString::fromStdString(_pSyncGroup->sName),_pParent), m_pSyncGroup(_pSyncGroup)
{
	setCheckable(true);
	connect(this, SIGNAL(toggled(bool)), this, SLOT(Hide(bool)));
	Initialize(m_pSyncGroup);
}
//---------------------------------------------------------------------------------------------------

SyncGroupWidget::~SyncGroupWidget()
{
	HSAFE_VECTOR_DELETE(m_Variables);
	HSAFE_DELETE(m_pLayout);
}
//---------------------------------------------------------------------------------------------------

inline void SyncGroupWidget::Initialize(SyncGroup* _pSyncGroup)
{
	// dont allow changes to variables while constructing
	std::lock_guard<std::recursive_mutex> lock(_pSyncGroup->GetLockObject());

	setUpdatesEnabled(false);

	bool bNewLayout = false;

	if (m_pLayout == nullptr || (m_pLayout != nullptr && m_kLayout != _pSyncGroup->kLayout))
	{
		HSAFE_DELETE(m_pLayout);

		switch (_pSyncGroup->kLayout)
		{
		case WidgetLayout_Horizontal:
			m_pLayout = HNEW QHBoxLayout(this);
			break;
		case WidgetLayout_Vertical:
			m_pLayout = HNEW QVBoxLayout(this);
			break;
		case WidgetLayout_Form:
			m_pLayout = HNEW QFormLayout(this);
			break;
		case WidgetLayout_Grid:
			m_pLayout = HNEW QGridLayout(this);
			break;
		default:
			m_pLayout = HNEW QHBoxLayout(this);
			break;
		}

		m_pLayout->setSizeConstraint(QLayout::SizeConstraint::SetMinAndMaxSize);
		bNewLayout = true;

		m_kLayout = _pSyncGroup->kLayout;
	}	

	for (SyncVariableWidget* pVar : m_Variables)
	{
		m_pLayout->removeWidget(pVar);
	}

	const std::vector<SyncVariableBase*>& NewVars = _pSyncGroup->GetVariables();
	uint32_t uCol = std::sqrt(static_cast<float>(NewVars.size()));

	auto AddToLayout = [&](size_t i, SyncVariableWidget* pGroup)
	{
		if (_pSyncGroup->kLayout != WidgetLayout_Grid)
		{
			m_pLayout->addWidget(pGroup);
		}
		else
		{
			static_cast<QGridLayout*>(m_pLayout)->addWidget(pGroup, i % uCol, i / uCol);
		}
	};

	if (NewVars.size() == m_Variables.size())
	{
		for (size_t i = 0; i < m_Variables.size(); ++i)
		{
			m_Variables[i]->Initialize(NewVars[i]);
			AddToLayout(i, m_Variables[i]);
		}
	}
	else if (NewVars.size() > m_Variables.size())
	{
		for (size_t i = 0ull; i < m_Variables.size(); ++i)
		{
			m_Variables[i]->Initialize(NewVars[i]);
			AddToLayout(i, m_Variables[i]);
		}
		for (size_t i = m_Variables.size(); i < NewVars.size(); ++i)
		{
			SyncVariableWidget* pVarWidget = HNEW SyncVariableWidget(NewVars[i], this);
			AddToLayout(i, pVarWidget);
			m_Variables.push_back(pVarWidget);
		}
	}
	else //NewVars.size() < m_Variables.size()
	{
		for (size_t i = 0ull; i < NewVars.size(); ++i)
		{
			m_Variables[i]->Initialize(NewVars[i]);
			AddToLayout(i, m_Variables[i]);
		}
		while (m_Variables.size() > NewVars.size())
		{
			HSAFE_DELETE(m_Variables.back());
			m_Variables.pop_back();
		}
	}

	m_pLayout->setAlignment(Qt::AlignTop);
	setLayout(m_pLayout);

	m_pSyncGroup = _pSyncGroup;
	setUpdatesEnabled(true);
}
//---------------------------------------------------------------------------------------------------

void SyncGroupWidget::Update()
{
	if (m_bVisible)
	{
		for (SyncVariableWidget* pVar : m_Variables)
		{
			pVar->Update();
		}
	}
}
//---------------------------------------------------------------------------------------------------

void SyncGroupWidget::Hide(bool _bChecked)
{
	m_bVisible = _bChecked;

	for (SyncVariableWidget* pVar : m_Variables)
	{
		pVar->setVisible(_bChecked);
	}
}
//---------------------------------------------------------------------------------------------------
