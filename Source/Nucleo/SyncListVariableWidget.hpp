﻿//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef SYNCLISTVARIABLEWIDGET_HPP
#define SYNCLISTVARIABLEWIDGET_HPP
#include "DataStructures\SyncVariable.h"
#include <qabstractitemmodel.h>
#include <qwidget.h>
#include <qlistview.h>

namespace Helix
{
	namespace Nucleo
	{
		class SyncListVariableModel : public QAbstractListModel
		{
			Q_OBJECT

		public:
			SyncListVariableModel(Datastructures::SyncListBase* _pList, QWidget* _pParent = Q_NULLPTR);
			~SyncListVariableModel();

			int rowCount(const QModelIndex &parent = QModelIndex()) const final;

			QVariant data(const QModelIndex &index, int role) const final;

			void SetList(Datastructures::SyncListBase* _pList);

			void SelectItem(const QModelIndex& current);

			void Update();

		private:
			Datastructures::SyncListBase* m_pList = nullptr;
		};

		class SyncListVariableWidget : public QListView
		{
		public:
			SyncListVariableWidget(Datastructures::SyncListBase* _pList, QWidget* _pParent = Q_NULLPTR) :
				QListView(_pParent), m_Model(_pList, this)
			{
				setResizeMode(QListView::ResizeMode::Adjust);
				setModel(&m_Model);
			}

			inline void SetList(Datastructures::SyncListBase* _pList) { m_Model.SetList(_pList); }

			inline void currentChanged(const QModelIndex& current, const QModelIndex& previous) final
			{
				m_Model.SelectItem(current);
			}

			inline void Update()
			{ 
				m_Model.Update();
			}

		private:
			SyncListVariableModel m_Model;
		};
	} // Nucleo
}// Helix



#endif // SYNCLISTVARIABLEWIDGET_HPP