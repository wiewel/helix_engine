﻿//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "SyncVariableWidget.hpp"
#include "hlx\src\StandardDefines.h"
#include "Math\MathFunctions.h"
#include <qgridlayout.h>
#include <qformlayout.h>
#include <qapplication.h>

#include "SyncListVariableWidget.hpp"
#include "SyncLineChartWidget.hpp"

using namespace Helix::Nucleo;
using namespace Helix::Datastructures;
//---------------------------------------------------------------------------------------------------

SyncVariableWidget::SyncVariableWidget(SyncVariableBase* _pSyncVariable, QWidget* _pParent) :
	QWidget(_pParent), m_pVariable(_pSyncVariable)
{
	for (uint32_t i = 0; i < kLineEdits; ++i)
	{
		m_pLineEdits[i] = nullptr;
	}

	Initialize(m_pVariable);
}
//---------------------------------------------------------------------------------------------------

SyncVariableWidget::~SyncVariableWidget()
{
	HSAFE_DELETE_ARRAY(m_pLineEdits);
	HSAFE_DELETE(m_pLabel);
	HSAFE_DELETE(m_pComboBox);
	HSAFE_DELETE(m_pCheckBox);
	HSAFE_DELETE(m_pPushButton);
	HSAFE_DELETE(m_pSyncList);
	HSAFE_DELETE(m_pSyncLineChart)
	HSAFE_DELETE(m_pLayout);
}
//---------------------------------------------------------------------------------------------------
void SyncVariableWidget::Initialize(SyncVariableBase* _pSyncVariable)
{
	setUpdatesEnabled(false);

	m_pVariable = _pSyncVariable;

	uint32_t uVars = 0u;
	switch (_pSyncVariable->kType)
	{
	case VarType_Int32:
	case VarType_UInt32:
	case VarType_Int64:
	case VarType_UInt64:
	case VarType_Float:
	case VarType_Double:
	case VarType_String:
		uVars = 1u;
		break;
	case VarType_Float2:
	case VarType_Int2:
	case VarType_UInt2:
		uVars = 2u;
		break;
	case VarType_Float3:
	case VarType_Quaternion: // converted to euler angles
	case VarType_Int3:
	case VarType_UInt3:
		uVars = 3u;
		break;
	case VarType_Float4:
	case VarType_Int4:
	case VarType_UInt4:
		uVars = 4u;
		break;
	default:
		break;
	}

	bool bNewLayout = false;

	if (m_pLayout == nullptr || (m_pLayout != nullptr && m_kLayout != _pSyncVariable->kLayout))
	{
		HSAFE_DELETE(m_pLayout);

		switch (_pSyncVariable->kLayout)
		{
		case WidgetLayout_Horizontal:
			m_pLayout = HNEW QHBoxLayout(this);
			break;
		case WidgetLayout_Vertical:
			m_pLayout = HNEW QVBoxLayout(this);
			break;
		case WidgetLayout_Form:
			m_pLayout = HNEW QFormLayout(this);
			break;
		case WidgetLayout_Grid:
			m_pLayout = HNEW QGridLayout(this);
			break;
		default:
			m_pLayout = HNEW QHBoxLayout(this);
			break;
		}

		m_kLayout = _pSyncVariable->kLayout;
		bNewLayout = true;
	}
	else if (m_pLayout != nullptr)
	{
		// remove previous edits
		for (uint32_t i = 0u; i < kLineEdits; ++i)
		{
			m_pLayout->removeWidget(m_pLineEdits[i]);
		}

		if (m_pCheckBox != nullptr)
		{
			m_pLayout->removeWidget(m_pCheckBox);
		}

		if (m_pComboBox != nullptr)
		{
			m_pLayout->removeWidget(m_pComboBox);
		}

		if (m_pPushButton != nullptr)
		{
			m_pLayout->removeWidget(m_pPushButton);
		}

		if (m_pSyncList != nullptr)
		{
			m_pLayout->removeWidget(m_pSyncList);
		}

		if (m_pSyncLineChart != nullptr)
		{
			m_pLayout->removeWidget(m_pSyncLineChart);
		}
	}

	if (_pSyncVariable->kType != VarType_Button &&
		_pSyncVariable->kType != VarType_List &&
		_pSyncVariable->kType != VarType_Chart)
	{
		if (m_pLabel == nullptr)
		{
			m_pLabel = HNEW QLabel(QString::fromStdString(_pSyncVariable->sName), this);
			m_pLayout->addWidget(m_pLabel);
		}
		else
		{
			m_pLabel->setText(QString::fromStdString(_pSyncVariable->sName));
		}
	}

	for (uint32_t i = 0u; i < uVars; ++i)
	{
		if (m_pLineEdits[i] == nullptr)
		{
			m_pLineEdits[i] = HNEW QLineEdit(this);
			if (_pSyncVariable->bReadOnly == false)
			{
				connect(m_pLineEdits[i], SIGNAL(editingFinished()), this, SLOT(EditingFinished()));
			}
		}

		m_pLineEdits[i]->setEnabled(_pSyncVariable->bReadOnly == false);

		m_pLayout->addWidget(m_pLineEdits[i]);
	}

	if (_pSyncVariable->kType == VarType_Bool)
	{
		if (m_pCheckBox == nullptr)
		{
			m_pCheckBox = HNEW QCheckBox(this);

			if (_pSyncVariable->bReadOnly == false)
			{
				connect(m_pCheckBox, SIGNAL(stateChanged(int)), this, SLOT(CheckBoxStateChanged(int)));
			}
		}
		m_pCheckBox->setTristate(false);
		m_pLayout->addWidget(m_pCheckBox);

		m_pCheckBox->setEnabled(_pSyncVariable->bReadOnly == false);
	}
	else if (_pSyncVariable->kType == VarType_Enum)
	{
		if (m_pComboBox == nullptr)
		{
			m_pComboBox = HNEW QComboBox(this);

			if (_pSyncVariable->bReadOnly == false)
			{
				connect(m_pComboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(ComboBoxIndexChanged(int)));
			}
		}
		else
		{
			m_pComboBox->clear();
		}

		const SyncEnum Enum = *_pSyncVariable->GetAs<SyncEnum>();
		for (const SyncEnumValue& Val : Enum.Values)
		{
			m_pComboBox->addItem(QString(Val.sName));
		}

		m_pComboBox->setCurrentIndex(Enum.uSelectedIndex);
		m_pLayout->addWidget(m_pComboBox);
		m_pComboBox->setEnabled(_pSyncVariable->bReadOnly == false);
	}
	else if (_pSyncVariable->kType == VarType_Button)
	{
		if (m_pPushButton == nullptr)
		{
			m_pPushButton = HNEW QPushButton(QString::fromStdString(_pSyncVariable->sName), this);
			connect(m_pPushButton, SIGNAL(clicked()), this, SLOT(PushButtonClicked()));
		}
		else
		{
			m_pPushButton->setText(QString::fromStdString(_pSyncVariable->sName));
		}

		m_pLayout->addWidget(m_pPushButton);
	}
	else if (_pSyncVariable->kType == VarType_List)
	{
		SyncListBase* pList = dynamic_cast<SyncListBase*>(_pSyncVariable);
		if (pList != nullptr)
		{
			if (m_pSyncList == nullptr)
			{
				m_pSyncList = HNEW SyncListVariableWidget(pList, this);
			}
			else
			{
				m_pSyncList->SetList(pList);
			}
		}

		m_pLayout->addWidget(m_pSyncList);
	}
	else if (_pSyncVariable->kType == VarType_Chart)
	{
		if (m_pSyncLineChart == nullptr)
		{
			m_pSyncLineChart = HNEW SyncLineChartWidget(_pSyncVariable->sName, this);
		}
		else
		{
			m_pSyncLineChart->Clear();
		}

		m_pLayout->addWidget(m_pSyncLineChart);
	}

	setLayout(m_pLayout);
	setUpdatesEnabled(true);
}
//---------------------------------------------------------------------------------------------------
void SyncVariableWidget::Update()
{
	m_bEdited = false;
	// try to compensate float conversion
	auto Epsilon = [](float _fRadAngle) -> float
	{
		if (fabsf(_fRadAngle) < 0.0001f)
		{
			return 0.f;
		}
		else
		{
			return _fRadAngle;
		}
	};

	QString sLine[kLineEdits];
	switch (m_pVariable->kType)
	{
	case VarType_Int32:
		sLine[0].setNum(*m_pVariable->GetAs<int32_t>());
		break;
	case VarType_UInt32:
		sLine[0].setNum(*m_pVariable->GetAs<uint32_t>());
		break;
	case VarType_Int64:
		sLine[0].setNum(*m_pVariable->GetAs<int64_t>());
		break;
	case VarType_UInt64:
		sLine[0].setNum(*m_pVariable->GetAs<uint64_t>());
		break;
	case VarType_Float:
		sLine[0].setNum(*m_pVariable->GetAs<float>());
		break;
	case VarType_Double:
		sLine[0].setNum(*m_pVariable->GetAs<double>());
		break;
	case VarType_String:
		sLine[0] = QString::fromStdString(*m_pVariable->GetAs<std::string>());
		break;
	case VarType_Float2:
	{
		const Math::float2* pVec2 = m_pVariable->GetAs<Math::float2>();
		sLine[0].setNum(pVec2->x);
		sLine[1].setNum(pVec2->y);
	}
		break;
	case VarType_Float3:
	{
		const Math::float3* pVec3 = m_pVariable->GetAs<Math::float3>();
		sLine[0].setNum(pVec3->x);
		sLine[1].setNum(pVec3->y);
		sLine[2].setNum(pVec3->z);
	}
		break;
	case VarType_Float4:
	{
		const Math::float4* pVec4 = m_pVariable->GetAs<Math::float4>();
		sLine[0].setNum(pVec4->x);
		sLine[1].setNum(pVec4->y);
		sLine[2].setNum(pVec4->z);
		sLine[3].setNum(pVec4->w);
	}
		break;
	case VarType_Quaternion:
	{
		Math::float3 vRot = m_pVariable->GetAs<Math::quaternion>()->toEulerRadians();
		sLine[0].setNum(Math::Rad2Deg(Epsilon(vRot.x)));
		sLine[1].setNum(Math::Rad2Deg(Epsilon(vRot.y)));
		sLine[2].setNum(Math::Rad2Deg(Epsilon(vRot.z)));
	}
	break;
	case VarType_Int2:
	{
		const Math::int2* pVec2 = m_pVariable->GetAs<Math::int2>();
		sLine[0].setNum(pVec2->x);
		sLine[1].setNum(pVec2->y);
	}
	break;
	case VarType_Int3:
	{
		const Math::int3* pVec3 = m_pVariable->GetAs<Math::int3>();
		sLine[0].setNum(pVec3->x);
		sLine[1].setNum(pVec3->y);
		sLine[2].setNum(pVec3->z);
	}
	break;
	case VarType_Int4:
	{
		const Math::int4* pVec4 = m_pVariable->GetAs<Math::int4>();
		sLine[0].setNum(pVec4->x);
		sLine[1].setNum(pVec4->y);
		sLine[2].setNum(pVec4->z);
		sLine[3].setNum(pVec4->w);
	}
		break;
	case VarType_UInt2:
	{
		const Math::uint2* pVec2 = m_pVariable->GetAs<Math::uint2>();
		sLine[0].setNum(pVec2->x);
		sLine[1].setNum(pVec2->y);
	}
		break;
	case VarType_UInt3:
	{
		const Math::uint3* pVec3 = m_pVariable->GetAs<Math::uint3>();
		sLine[0].setNum(pVec3->x);
		sLine[1].setNum(pVec3->y);
		sLine[2].setNum(pVec3->z);
	}
	break;
	case VarType_UInt4:
	{
		const Math::uint4* pVec4 = m_pVariable->GetAs<Math::uint4>();
		sLine[0].setNum(pVec4->x);
		sLine[1].setNum(pVec4->y);
		sLine[2].setNum(pVec4->z);
		sLine[3].setNum(pVec4->w);
	}
		break;
	case VarType_Bool:
		m_pCheckBox->setCheckState((*m_pVariable->GetAs<bool>()) ? Qt::CheckState::Checked : Qt::CheckState::Unchecked);
		break;
	case VarType_Enum:
		m_pComboBox->setCurrentIndex(m_pVariable->GetAs<SyncEnum>()->uSelectedIndex);
		break;
	case VarType_List:
		m_pSyncList->Update();
		break;
	case VarType_Chart:
		m_pSyncLineChart->AddValue(*m_pVariable->GetAs<float>());
		break;
	default:
		// no need to update button, it does not have any data to display
		break;
	}

	for (uint32_t i = 0u; i < kLineEdits; ++i)
	{
		QLineEdit* pLineEdit = m_pLineEdits[i];
		if (pLineEdit != nullptr && pLineEdit->isModified() == false && pLineEdit->hasFocus() == false)
		{
			pLineEdit->setText(sLine[i]);
		}
	}
}
//---------------------------------------------------------------------------------------------------

void SyncVariableWidget::PushButtonClicked()
{
	if (m_pVariable->kType == VarType_Button)
	{
		m_pVariable->Set(nullptr);
	}
}
//---------------------------------------------------------------------------------------------------

void SyncVariableWidget::CheckBoxStateChanged(int)
{
	if (m_pVariable->bReadOnly)
		return;

	if (m_pVariable->kType == VarType_Bool)
	{
		bool Val = m_pCheckBox->checkState() != Qt::CheckState::Unchecked;
		m_pVariable->Set(&Val);
	}
}
//---------------------------------------------------------------------------------------------------
void SyncVariableWidget::ComboBoxIndexChanged(int index)
{
	if (m_pVariable->bReadOnly || index == -1)
		return;

	if (m_pVariable->kType == VarType_Enum)
	{
		SyncEnum Enum = *m_pVariable->GetAs<SyncEnum>();
		Enum.uSelectedIndex = index;
		m_pVariable->Set(&Enum);
	}
}
//---------------------------------------------------------------------------------------------------
void SyncVariableWidget::EditingFinished()
{
	if (m_pVariable->bReadOnly || m_bEdited)
		return;

	// avoid double triggered edits
	m_bEdited = true;

	QLineEdit* pSender = qobject_cast<QLineEdit*>(sender());

	if (m_pVariable->kType == VarType_Int32)
	{
		int32_t Val = m_pLineEdits[0]->text().toInt();
		m_pVariable->Set(&Val);
	}
	else if (m_pVariable->kType == VarType_UInt32)
	{
		uint32_t Val = m_pLineEdits[0]->text().toUInt();
		m_pVariable->Set(&Val);
	}
	else if (m_pVariable->kType == VarType_Int64)
	{
		int64_t Val = m_pLineEdits[0]->text().toLongLong();
		m_pVariable->Set(&Val);
	}
	else if (m_pVariable->kType == VarType_UInt64)
	{
		uint64_t Val = m_pLineEdits[0]->text().toULongLong();
		m_pVariable->Set(&Val);
	}
	else if (m_pVariable->kType == VarType_Float)
	{
		float Val = m_pLineEdits[0]->text().toFloat();
		m_pVariable->Set(&Val);
	}
	else if (m_pVariable->kType == VarType_Double)
	{
		double Val = m_pLineEdits[0]->text().toDouble();
		m_pVariable->Set(&Val);
	}
	else if (m_pVariable->kType == VarType_String)
	{
		std::string Val = m_pLineEdits[0]->text().toStdString();
		m_pVariable->Set(&Val);
	}
	else if (m_pVariable->kType == VarType_Float2)
	{
		Math::float2 Val(m_pLineEdits[0]->text().toFloat(), m_pLineEdits[1]->text().toFloat());
		m_pVariable->Set(&Val);
	}
	else if (m_pVariable->kType == VarType_Float3)
	{
		Math::float3 Val(
			m_pLineEdits[0]->text().toFloat(),
			m_pLineEdits[1]->text().toFloat(),
			m_pLineEdits[2]->text().toFloat());
		m_pVariable->Set(&Val);
	}
	else if (m_pVariable->kType == VarType_Float4)
	{
		Math::float4 Val(
			m_pLineEdits[0]->text().toFloat(),
			m_pLineEdits[1]->text().toFloat(),
			m_pLineEdits[2]->text().toFloat(),
			m_pLineEdits[3]->text().toFloat());
		m_pVariable->Set(&Val);
	}
	else if (m_pVariable->kType == VarType_Quaternion)
	{
		Math::quaternion Val = Math::QuaternionFromEulerDegreesXYZ(
			m_pLineEdits[0]->text().toFloat(),
			m_pLineEdits[1]->text().toFloat(),
			m_pLineEdits[2]->text().toFloat());
		m_pVariable->Set(&Val);
	}

	pSender->setModified(false);
}
//---------------------------------------------------------------------------------------------------
