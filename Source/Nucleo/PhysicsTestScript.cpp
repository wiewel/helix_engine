//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "PhysicsTestScript.h"
#include "DataStructures\GameObject.h"
#include "DataStructures\GameObjectPool.h"
#include "Scene\PhysicsScene.h"

using namespace Helix::Nucleo;
using namespace Helix::Physics;
using namespace Helix::Datastructures;

STATICSCRIPT_CPP(PhysicsTestScript)

//---------------------------------------------------------------------------------------------------
PhysicsTestScript::PhysicsTestScript()
{

}
//---------------------------------------------------------------------------------------------------
PhysicsTestScript::~PhysicsTestScript()
{

}
//---------------------------------------------------------------------------------------------------
void PhysicsTestScript::OnTrigger(Helix::Physics::TriggerInfo _Info)
{
}
//---------------------------------------------------------------------------------------------------
void PhysicsTestScript::OnContact(Helix::Physics::ContactInfo _Info)
{
}
//---------------------------------------------------------------------------------------------------
void PhysicsTestScript::OnSceneBegin()
{

}
//---------------------------------------------------------------------------------------------------
void PhysicsTestScript::OnSceneEnd()
{
}
//---------------------------------------------------------------------------------------------------
void PhysicsTestScript::OnUpdate(double _fDeltaTime, double _fTotalTime)
{
	//m_pCameraController->Walk(-0.1f * _fDeltaTime);

	//Math::float3 vPosition = m_pCameraJoint->GetPosition();
	//vPosition.z += 0.1f * _fDeltaTime;
	//m_pCameraJoint->SetPosition(vPosition);
	//if (m_Registered == false)
	//{
	//	std::vector<GameObject*> GOs;
	//	GameObjectPool::Instance()->GetObjectsByName("Trigger", GOs);
	//	if (GOs.empty() == false)
	//	{
	//		GameObject* pTrigger = GOs[0];
	//		ShapeComponent* pTriggerShape = pTrigger->GetComponentsByType<ShapeComponent>()[1];
	//		pTriggerShape->RegisterCallback(Physics::TriggerEvent::Callback(&OnTrigger));
	//		GOs.clear();
	//		m_Registered = true;
	//	}

	//	GOs.clear();
	//	GameObjectPool::Instance()->GetObjectsByName("Object", GOs);
	//	if (GOs.empty() == false)
	//	{
	//		GameObject* pObject = GOs[0];
	//		ShapeComponent* pSimulationShape = pObject->GetComponentsByType<ShapeComponent>()[1];
	//		pSimulationShape->RegisterCallback(Physics::ContactEvent::Callback(&OnContact));
	//		GOs.clear();
	//		m_Registered = true;
	//	}
	//}
}