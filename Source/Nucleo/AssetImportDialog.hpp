﻿//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef ASSETIMPORTDIALOG_H
#define ASSETIMPORTDIALOG_H

#include <QObject>
#include <qfiledialog.h>
#include <qcheckbox.h>
#include <qcombobox.h>
#include <qspinbox.h>
#include <qgridlayout.h>
#include <qgroupbox.h>
#include <qlabel.h>
#include "Display\ViewDefines.h"
#include "H3D\FbxSDKFactory.h"

namespace Helix
{
	namespace Nucleo
	{
		class AssetImportDialog : private QObject
		{
			Q_OBJECT

		public:
			AssetImportDialog();
			~AssetImportDialog();

			bool Show();

			std::vector<hlx::string> GetSelectedFiles() const;
			Display::VertexLayout GetVertexLayout() const;
			H3D::CoordinateSystem GetCoordinateSystem() const;

			bool GetToLefthanded() const;
			bool GetFilpV() const;
			bool GetReverseWinding() const;
			bool GetConvexHull() const;
			bool GetImportTextures() const;
			bool GetImportMaterials() const;
			bool GetReversePivot() const;
			bool GetNullPivot() const;

			int GetMipLevel() const;
			float GetScale() const;

		private slots:
			void Hide(int res);

		private:
			QFileDialog* m_pFileDialog = nullptr;

			QCheckBox* m_pImportMaterials = nullptr;
			QCheckBox* m_pImportTextures = nullptr;
			QCheckBox* m_pToLeftHanded = nullptr;
			QCheckBox* m_pFlipV = nullptr;
			QCheckBox* m_pReverseWinding = nullptr;
			QCheckBox* m_pConvexHull = nullptr;
			QCheckBox* m_pReversePivot = nullptr;
			QCheckBox* m_pNullPivot = nullptr;

			QLabel* m_pScaleLabel = nullptr;
			QDoubleSpinBox* m_pScale = nullptr;

			QLabel* m_pMipLabel = nullptr;
			QSpinBox* m_pMipLevel = nullptr;
			

			QComboBox* m_pVertexLayout = nullptr;
			QComboBox* m_pCoordSystem = nullptr;
		};

		struct CommonVertexLayout
		{
			const char* sName;
			Display::VertexLayout kLayout;
		};

		static const CommonVertexLayout Layouts[] =
		{
			{ "PosTex",	Display::VertexLayout_PosXYZTex },
			{ "PosNormTex",	Display::VertexLayout_PosXYZNormTex },
			{ "PosNormTanTex",	Display::VertexLayout_PosXYZNormTanTex },
			{ "PosNormTanBitanTex",	Display::VertexLayout_PosXYZNormTanBitanTex }
		};

		struct CoordSysDesc
		{
			const char* sName;
			H3D::CoordinateSystem kCoordSystem;
		};

		static const CoordSysDesc CoordSystems[] =
		{
			{ "DirectX", H3D::CoordinateSystem_DirectX },
			{ "OpenGL", H3D::CoordinateSystem_OpenGL },
			{ "Max", H3D::CoordinateSystem_Max },
			{ "MayaZUp", H3D::CoordinateSystem_MayaZUp },
			{ "MayaYUp", H3D::CoordinateSystem_MayaYUp },
			{ "MotionBuilder", H3D::CoordinateSystem_MotionBuilder },
			{ "LightWave", H3D::CoordinateSystem_Lightwave }
		};

	} // Nucleo
} // Helix


#endif // !ASSETIMPORTDIALOG_H

