//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef EDITORCONTROLSCRIPT_H
#define EDITORCONTROLSCRIPT_H

#include "Scene\ScriptObject.h"
#include "Scene\Level.h"
#include "Input\InputReciever.h"
#include "Scene\CameraComponent.h"
#include "Display\DX11\RenderObjectDX11.h"
#include "Math\MathTypes.h"
#include "Sound\Listener.h"
#include "Display\DX11\DebugRenderDX11.h"

namespace Helix
{
	namespace Nucleo
	{
		class EditorControlScript : public Scene::IScriptObject
		{
		public:
			EditorControlScript(const hlx::string& _sLevelName);
			~EditorControlScript();

			void NewLevel();
			void ChangeLevel(const hlx::string& _sLevelName);
			void ExportLevel(const hlx::string& _sLevelFilePath);

			void SetProgressFunction(const TProgressFunctor& _Func);

			Datastructures::GameObject* ScenePicking(
				const Math::float2& _vViewportCoords,
				Physics::TCollisionFilterMask _CollisionFilters = { Physics::CollisionFilterFlag_Default }) const;

			// todo: remove
			const Scene::CameraComponent* GetCamera() const;

			void CreateDefaultLight();

			void RemoveGameObject(Datastructures::GameObject* _pObject);

		private:
			void OnSceneBegin() final;
			void OnSceneEnd() final;
			void OnUpdate(double _fDeltaTime, double _fTotalTime) final;
			void OnDestroyObjects() final;
			void OnPreRendering() final;

			Datastructures::GameObject* ScenePicking(
				const Math::float2& _vViewportCoords,
				Scene::CameraComponent* _pCamera,
				Physics::TCollisionFilterMask _CollisionFilters = { Physics::CollisionFilterFlag_Default }) const;

			void RenderDebugShapes();
			void RemoveDebugShapes();

		private:
			hlx::string m_sLevelName; // initial (short) level name
			Scene::Level m_Level;
			Input::InputReciever m_InputReciever;

			// no duplicates to avoid destruction of locked gameobject
			std::set<Datastructures::GameObject*> m_ObjectsToDestroy;

			Scene::CameraComponent* m_pMainCamera = nullptr;

			std::vector<Display::DebugRenderDX11::LineIndices> m_DebugShapes;

			Datastructures::GameObject* m_pDirLightParent = nullptr;
			Datastructures::GameObject* m_pCameraFrustum = nullptr;

			std::array<Math::float3, 8> m_DebugPoints;

			Helix::Datastructures::IBaseObject* m_pCamera = nullptr;
			Helix::Physics::ControllerComponent* m_pCameraController = nullptr;
		};

		inline const Scene::CameraComponent* EditorControlScript::GetCamera() const { return m_pMainCamera; }
		inline void EditorControlScript::SetProgressFunction(const TProgressFunctor& _Func)	{m_Level.SetProgressFunction(_Func);}
	}
}

#endif // EDITORCONTROLSCRIPT_H