﻿//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef SYNCLINECHARTWIDGET_HPP
#define SYNCLINECHARTWIDGET_HPP

#include <QtCharts/qchartview.h>
#include <QtCharts/qlineseries.h>
#include <QtCharts/qvalueaxis.h>

namespace Helix
{
	namespace Nucleo
	{
		class SyncLineChartInternal : public QtCharts::QChart
		{
			Q_OBJECT
		public:
			SyncLineChartInternal(QGraphicsItem *parent = Q_NULLPTR, Qt::WindowFlags wFlags = 0);
			~SyncLineChartInternal();

			void AddValue(float _fValue);
			void Clear();

		private:
			QtCharts::QLineSeries* m_pLineSeries = nullptr;
			QtCharts::QValueAxis* m_pValueAxis = nullptr;

			float m_fMin = std::numeric_limits<float>::max();
			float m_fMax = std::numeric_limits<float>::lowest();

			int m_iCurIndex = 0;
		};

		class SyncLineChartWidget : public QtCharts::QChartView
		{
			Q_OBJECT

		public:
			SyncLineChartWidget(const std::string& _sTitle, QWidget* parent = Q_NULLPTR);
			~SyncLineChartWidget();

			inline void AddValue(float _fValue) { m_pChart->AddValue(_fValue); }
			inline void Clear() { m_pChart->Clear(); }

		private:
			SyncLineChartInternal* m_pChart = nullptr;
		};
	} // Nucleo
}// Helix



#endif // SYNCLINECHARTWIDGET_HPP