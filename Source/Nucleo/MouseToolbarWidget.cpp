﻿//   Copyright 2020 Steffen Wiewel, Fabian Wahlster, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "MouseToolbarWidget.hpp"

#include <qformlayout.h>
#include <qgridlayout.h>

#include "MouseToolSelect.h"
#include "MouseToolMove.h"
#include "MouseToolRotate.h"

#include "NucleoMouseEvent.h"
#include "EditorControlScript.h"

using namespace Helix::Nucleo;

//---------------------------------------------------------------------------------------------------
MouseToolbarWidget::MouseToolbarWidget(QWidget* _pParent) :
	QListWidget(_pParent)
{
	m_MouseTools.push_back(HNEW MouseToolSelect(this));
	m_MouseTools.push_back(HNEW MouseToolMove(this));
	m_MouseTools.push_back(HNEW MouseToolRotate(this));

	Initialize();
}
//---------------------------------------------------------------------------------------------------
MouseToolbarWidget::~MouseToolbarWidget()
{
	m_MouseTools.clear();
	HSAFE_DELETE(m_pToolGroup);
}
//---------------------------------------------------------------------------------------------------
void MouseToolbarWidget::HandleMouseEvent(const NucleoMouseEvent* _pEvent, const EditorControlScript* _pControlScript)
{
	MouseTool* pTool = static_cast<MouseTool*>(m_pToolGroup->checkedButton());
	if(pTool != nullptr)
	{
		pTool->HandleMouseEvent(_pEvent, _pControlScript);
	}
}
//---------------------------------------------------------------------------------------------------
void MouseToolbarWidget::Initialize()
{
	HSAFE_DELETE(m_pToolGroup);

	/// set exclusive selection mode
	if (m_pToolGroup == nullptr)
	{
		m_pToolGroup = HNEW MouseToolGroup(this);
		m_pToolGroup->setExclusive(true);
	}

	for(MouseTool* pTool : m_MouseTools)
	{
		QListWidgetItem* pItem = new QListWidgetItem(this);
		QSize SizeHint = pTool->minimumSizeHint();
		pItem->setSizeHint(SizeHint);
		setItemWidget(pItem, pTool);

		m_pToolGroup->addButton(pTool);
	}

	if(m_MouseTools.empty() == false)
	{
		m_MouseTools.front()->setChecked(true);
	}

	setFlow(QListWidget::LeftToRight);
	setFixedSize(sizeHintForColumn(0) * count() + 2 * frameWidth(), sizeHintForRow(0) + 2 * frameWidth());
}
//---------------------------------------------------------------------------------------------------
