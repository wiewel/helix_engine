﻿//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "SyncLineChartWidget.hpp"
#include "hlx\src\StandardDefines.h"

using namespace Helix::Nucleo;
using namespace QtCharts;
//---------------------------------------------------------------------------------------------------

SyncLineChartInternal::SyncLineChartInternal(QGraphicsItem *parent, Qt::WindowFlags wFlags) : 
	QChart(QChart::ChartTypeCartesian, parent, wFlags),
	m_pLineSeries(HNEW QLineSeries(this)),
	m_pValueAxis(HNEW QValueAxis(this))
{
	QPen green(Qt::green);
	green.setWidth(3);
	m_pLineSeries->setPen(green);

	addSeries(m_pLineSeries);
	createDefaultAxes();
	setAxisY(m_pValueAxis, m_pLineSeries);
	
	this->axisX()->setMax(100.f);

	m_pValueAxis->setTickCount(1);

	m_pValueAxis->setMin(0.f);
	m_pValueAxis->setMax(1.f);

	AddValue(0.f);
}
//---------------------------------------------------------------------------------------------------

SyncLineChartInternal::~SyncLineChartInternal()
{
	m_pLineSeries->clear();
	HSAFE_DELETE(m_pLineSeries);
	HSAFE_DELETE(m_pValueAxis);
}
//---------------------------------------------------------------------------------------------------

void SyncLineChartInternal::AddValue(float _fValue)
{
	m_fMax = std::max(_fValue, m_fMax);
	m_fMin = std::min(_fValue, m_fMin);

	m_pValueAxis->setMin(m_fMin);

	if (m_fMax != m_fMin)
	{
		m_pValueAxis->setMax(m_fMax);
	}

	int iCount = m_pLineSeries->count();
	m_iCurIndex = (m_iCurIndex + 1) % 100;

	if (iCount < 100)
	{
		m_pLineSeries->append(static_cast<float>(m_iCurIndex), _fValue);
	}
	else
	{
		m_pLineSeries->clear();

		////for (int i = 0; i < iCount-1; ++i)
		////{
		////	m_pLineSeries->replace(i, m_pLineSeries->at(i + 1));
		////}

		//m_pLineSeries->replace(m_iCurIndex, static_cast<float>(m_iCurIndex), _fValue);
	}
}
//---------------------------------------------------------------------------------------------------

void SyncLineChartInternal::Clear()
{
	m_fMin = std::numeric_limits<float>::max();
	m_fMax = std::numeric_limits<float>::lowest();

	m_iCurIndex = 0;

	m_pLineSeries->clear();
	//m_pValueAxis->setMin(0.f);
	//m_pValueAxis->setMax(1.f);
}
//---------------------------------------------------------------------------------------------------

SyncLineChartWidget::SyncLineChartWidget(const std::string& _sTitle, QWidget* parent) :
	m_pChart(HNEW SyncLineChartInternal),
	QChartView(parent)
{
	//m_pChart->setAnimationOptions(QChart::AllAnimations);
	m_pChart->setTitle(QString::fromStdString(_sTitle));
	m_pChart->legend()->hide();
	setChart(m_pChart);
	setRenderHint(QPainter::Antialiasing);
}
//---------------------------------------------------------------------------------------------------

SyncLineChartWidget::~SyncLineChartWidget()
{
	HSAFE_DELETE(m_pChart);
}
//---------------------------------------------------------------------------------------------------
