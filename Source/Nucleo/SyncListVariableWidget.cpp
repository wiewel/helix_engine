﻿//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "SyncListVariableWidget.hpp"

using namespace Helix::Nucleo;
using namespace Helix::Datastructures;

SyncListVariableModel::SyncListVariableModel(SyncListBase* _pList, QWidget* _pParent)
	: QAbstractListModel(_pParent), m_pList(_pList)
{
}
//---------------------------------------------------------------------------------------------------

SyncListVariableModel::~SyncListVariableModel()
{	
}
//---------------------------------------------------------------------------------------------------

int SyncListVariableModel::rowCount(const QModelIndex& parent) const
{
	return m_pList->GetSize();
}
//---------------------------------------------------------------------------------------------------

QVariant SyncListVariableModel::data(const QModelIndex& index, int role) const
{
	int iSize = m_pList->GetSize();

	if (index.isValid() == false || index.row() >= iSize || role != Qt::DisplayRole)
		return QVariant();

	return QString::fromStdString(m_pList->GetText(index.row()));
}
//---------------------------------------------------------------------------------------------------

void SyncListVariableModel::SetList(SyncListBase* _pList)
{
	m_pList = _pList;
}
//---------------------------------------------------------------------------------------------------

void SyncListVariableModel::SelectItem(const QModelIndex& index)
{
	int iSize = m_pList->GetSize();

	if (index.isValid() && index.row() < iSize)
	{
		m_pList->SelectItem(index.row());
	}
}
//---------------------------------------------------------------------------------------------------

void SyncListVariableModel::Update()
{
	QModelIndex topLeft = createIndex(0, 0);
	emit dataChanged(topLeft, topLeft);
}
//---------------------------------------------------------------------------------------------------
