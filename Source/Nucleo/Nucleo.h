//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef NUCLEO_H
#define NUCLEO_H

#include <QtWidgets/QMainWindow>
#include "ui_Nucleo.h"
#include "Viewport.hpp"
#include "EditorControlScript.h"
#include "Async\SpinLock.h"
#include "SyncDockWidget.hpp"
#include "AssetImportDialog.hpp"
#include "H3D\AssetImporter.h"
#include "MouseToolbarWidget.hpp"
#include "ProgressWidget.hpp"

#include <qtoolbar.h>
#include <qsettings.h>

namespace Helix
{
	namespace Datastructures
	{
		struct SyncDock;
	}

	namespace Nucleo
	{
		using TWidgetMap = std::unordered_map<std::string, SyncDockWidget*>;
		using TDockMap = std::unordered_map<std::string, const Datastructures::SyncDock*>;

		class Nucleo : public QMainWindow
		{
			Q_OBJECT

		public:
			Nucleo(const QString& _sActiveLevel);
			~Nucleo();

		protected:
			void customEvent(QEvent* _pEvent) final;

		private:
			void Bind(const Datastructures::SyncDock* _pSyncDock);
			void Unbind(const Datastructures::SyncDock* _pSyncDock);
			
			void InitializeMenuBar();

			template<class Component, class ...Args>
			void AddComponent(Args&&... _args);

			void LoadSettings();

		private slots:
			void UpdateWidgets();
			void UpdateStatusBar();
			void UpdateGizmos();

			void StartEngine();
			void ConnectToPVD();
			void ToggleProfiler();

			// file menu
			void NewLevel();
			void OpenLevel();
			void SaveLevel();
			void ImportAsset();

			// component menu
			void CreateLightComponent();
			void CreateShapeComponent();
			void CreateSoundComponent();
			void CreateRotationComponent();

			// TODO: remove (it is RedLine specific)
			void CreateStatueComponent();

			// view menu
			void ToggleDocks();
			void FloatDocks();

		private:
			int32_t m_iWidgetRefreshRate = 250u;
			int32_t m_iGizmoRefreshRate = 15u;

			Viewport* m_pViewport = nullptr;
			MouseToolbarWidget* m_pMouseToolbarWidget = nullptr;
			ProgressWidget* m_pProgressWidget = nullptr;

			QTimer* m_pUpdateTimer = nullptr;
			QTimer* m_pGizmoUpdateTimer = nullptr;

			AssetImportDialog* m_pImportAssetDialog = nullptr;
			H3D::AssetImporter m_AssetImporter;

			QMenu* m_pFileMenu = nullptr;
			QAction* m_pStartEngineAction = nullptr;
			QAction* m_pConnectToPVDAction = nullptr;
			QAction* m_pEnableProfilerAction = nullptr;

			QAction* m_pNewLevelAction = nullptr;
			QAction* m_pOpenLevelAction = nullptr;
			QAction* m_pSaveLevelAction = nullptr;
			QAction* m_pImportAssetAction = nullptr;

			QMenu* m_pComponentMenu = nullptr;

			QAction* m_pCreateLightComponentAction = nullptr;
			QAction* m_pCreateShapeComponentAction = nullptr;
			QAction* m_pCreateSoundComponentAction = nullptr;
			QAction* m_pCreateRotationComponentAction = nullptr;

			// TODO: remove (it is RedLine specific)
			QAction* m_pCreateStatueComponentAction = nullptr; 

			QMenu* m_pViewMenu = nullptr;
			QAction* m_pToggleDocksAction = nullptr;
			QAction* m_pFloatDocksAction = nullptr;

			bool m_bDocksVisible = true;
			bool m_bFloatDocks = false;

			Ui::NucleoClass ui;
			Async::SpinLock m_DockLock;
			Async::SpinLock m_UpdateLevelLock;

			TWidgetMap m_Widgets;
			TDockMap m_Docks;
			EditorControlScript m_ControlScript;

			std::array<SyncDockWidget*, 4> m_TopDockWidgets = {nullptr, nullptr, nullptr, nullptr};
			std::array<SyncDockWidget*, 4> m_LastDockWidgets = { nullptr, nullptr, nullptr, nullptr };
		};

		//---------------------------------------------------------------------------------------------------
		template<class Component, class ...Args>
		inline void Nucleo::AddComponent(Args&& ..._args)
		{
			Datastructures::GameObject* pGO = Scene::NucleoSelectionInterface::Instance()->GetActiveSelection();

			if (pGO == nullptr)
				return;

			Component* pComponent = pGO->AddComponent<Component>(std::forward<Args>(_args)...);
			if (pComponent != nullptr)
			{
				Datastructures::SyncComponent* pSyncComponent = pComponent->GetSyncComponent();
				if (pSyncComponent == nullptr)
					return;

				pGO->AddSyncComponent(pSyncComponent);
			}
		}
	}
}
#endif // NUCLEO_H
