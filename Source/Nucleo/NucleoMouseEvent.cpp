﻿//   Copyright 2020 Steffen Wiewel, Fabian Wahlster, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "NucleoMouseEvent.h"

#include "hlx\src\Logger.h"
#include "Math\MathFunctions.h"

using namespace Helix::Nucleo;

//---------------------------------------------------------------------------------
NucleoMouseEvent::NucleoMouseEvent(const QMouseEvent& _MouseEvent, const QWidget& _Caller) :
	QEvent(kNucleoMouseEvent),
	m_MouseEvent(_MouseEvent)
{
	QPoint Pos = _MouseEvent.pos();

	// 0,0 => upper left
	// 1,1 => lower right
	m_vMousePosition = Math::float2(
		static_cast<float>(Pos.x()) / static_cast<float>(_Caller.width()),
		static_cast<float>(Pos.y()) / static_cast<float>(_Caller.height()));

	Math::Saturate(m_vMousePosition.x);
	Math::Saturate(m_vMousePosition.y);
}
//---------------------------------------------------------------------------------
NucleoMouseEvent::~NucleoMouseEvent()
{
}
//---------------------------------------------------------------------------------
