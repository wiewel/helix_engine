//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef QSTRINGHELPER_H
#define QSTRINGHELPER_H

#include <qstring.h>
#include "hlx\src\String.h"

inline QString fromhlxString(const hlx::string& _sStr)
{
#ifdef UNICODE
	return QString::fromStdWString(_sStr);
#else
	return QString::fromStdString(_sStr);
#endif
}

#endif // !QSTRINGHELPER_H
