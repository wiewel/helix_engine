//   Copyright 2020 Steffen Wiewel, Fabian Wahlster, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "MouseTool.h"

#include "EditorControlScript.h"
#include "NucleoMouseEvent.h"
#include "DataStructures/GameObject.h"
#include "DataStructures/ShapeComponent.h"
#include "Scene\CameraComponent.h"
#include "Scene\NucleoSelectionInterface.h"

using namespace Helix::Nucleo;
using namespace Helix::Datastructures;
using namespace Helix::Math;
using namespace Helix::Scene;

//---------------------------------------------------------------------------------------------------

MouseTool::MouseTool(MouseToolType _kType, const QIcon& _Icon, QWidget* _pParent) :
	QPushButton(_pParent),
	m_kType(_kType)
{
	//QPixmap pix(":/images/images/logo.png");
	//QIcon icon(pix);
	setIcon(_Icon);
	setIconSize(m_Size);
	setToolTip(fromhlxString(MouseToolTypeNames[m_kType]));
	setCheckable(true);

	for (uint32_t i = 0u; i < kMovementAxis_NumOf; ++i)
	{
		m_MovementMarker[i] = nullptr;
	}
}
//---------------------------------------------------------------------------------------------------
MouseTool::~MouseTool()
{
	for (GameObject*& pObj : m_MovementMarker)
	{
		if (pObj != nullptr)
		{
			pObj->Destroy();
			pObj = nullptr;
		}
	}
}
//---------------------------------------------------------------------------------------------------
void MouseTool::HandleMouseEvent(const NucleoMouseEvent* _pEvent, const EditorControlScript* _pControlScript)
{
	if (_pControlScript != nullptr)
	{
		m_pCamera = _pControlScript->GetCamera();

		if (_pEvent->GetMouseButtonDown() == Qt::LeftButton)
		{
			MovementAxis kSelectedAxis = kMovementAxis_Inactive;
			GameObject* pSelectedMarker = _pControlScript->ScenePicking(_pEvent->GetMousePosition(), { Physics::CollisionFilterFlag_MouseTool });

			if (pSelectedMarker != nullptr)
			{
				for (uint32_t i = 0u; i < kMovementAxis_NumOf; ++i)
				{
					if (pSelectedMarker == m_MovementMarker[i])
					{
						kSelectedAxis = static_cast<MovementAxis>(i);
						break;
					}
				}
			}
			else
			{
				GameObject* pHit = _pControlScript->ScenePicking(_pEvent->GetMousePosition(), { Physics::CollisionFilterFlag_Default });
				if (pHit != nullptr &&  pHit->IsDynamic)
				{
					SetActiveSelection(pHit, _pControlScript);
					SetMarkerTransform(pHit->GetTransform());
					ShowMarker(true);
				}
			}
			m_kActiveAxis = kSelectedAxis;
		}
	}

	if (IsActiveSelectionValid())
	{
		float2 vMousePos = _pEvent->GetMousePosition();
		if (IsNewActivation())
		{
			m_vLastMousePos = vMousePos;
		}

		OnMouseEvent(_pEvent, _pControlScript);
		m_vLastMousePos = vMousePos;
	}
	else
	{
		ShowMarker(false);
	}

	m_bNewActivation = false;
}
//---------------------------------------------------------------------------------------------------
bool MouseTool::IsActiveSelectionValid() const
{
	return m_pActiveSelection != nullptr && m_pActiveSelection->IsAccessible() && m_pActiveSelection->IsDynamic;
}
//---------------------------------------------------------------------------------------------------
void MouseTool::SetActiveSelection(GameObject* _pActiveSelection, const EditorControlScript* _pControlScript)
{
	if (_pControlScript != nullptr)
	{
		m_pCamera = _pControlScript->GetCamera();
	}

	m_pActiveSelection = _pActiveSelection;

	NucleoSelectionInterface::Instance()->SetActiveSelection(m_pActiveSelection);

	if (m_pActiveSelection != nullptr)
	{
		m_pActiveSelection->BindToNucleo();
	}
}
//---------------------------------------------------------------------------------------------------
void MouseTool::ShowMarker(bool _bShow)
{
	for (GameObject* pMarker : m_MovementMarker)
	{
		if (pMarker != nullptr)
		{
			if (_bShow)
			{
				pMarker->ClearFlags(GameObjectFlag_Invisible);
				pMarker->ActivatePhysicsQuery(true);
			}
			else
			{
				pMarker->SetFlags(GameObjectFlag_Invisible);
				pMarker->ActivatePhysicsQuery(false);
			}
		}
	}
}
//---------------------------------------------------------------------------------------------------
void MouseTool::SetMarkerTransform(transform& _NewTransform)
{
	m_MarkerWorldTransform = _NewTransform;

	std::array<transform, kMovementAxis_NumOf> MarkerTransforms = OnSetMarkerTransform(_NewTransform);

	for (uint32_t i = 0u; i < kMovementAxis_NumOf; ++i)
	{
		if (m_MovementMarker[i] != nullptr)
		{
			m_MovementMarker[i]->Transform = MarkerTransforms[i];
		}
	}
}
//---------------------------------------------------------------------------------------------------
void MouseTool::Activate(GameObject*& _pActiveSelection)
{
	m_pActiveSelection = _pActiveSelection;
	m_bNewActivation = true;
	OnActivate();

	/// Spawn movement game object
	if (IsActiveSelectionValid())
	{
		SetMarkerTransform(m_pActiveSelection->Transform);
		ShowMarker(true);
	}

	m_kActiveAxis = kMovementAxis_Inactive;
}
//---------------------------------------------------------------------------------------------------
void MouseTool::Disable(GameObject*& _pActiveSelection)
{
	_pActiveSelection = m_pActiveSelection;

	/// hide movement GO
	ShowMarker(false);

	OnDisable();

	m_bNewActivation = false;
}
//---------------------------------------------------------------------------------------------------
void MouseTool::Update(const CameraComponent* _pCamera)
{
	if (_pCamera == nullptr)
	{
		return;
	}
	m_pCamera = _pCamera;

	SetMarkerTransform(m_MarkerWorldTransform);

	OnUpdate(_pCamera);
}
//---------------------------------------------------------------------------------------------------
std::array<transform, MouseTool::kMovementAxis_NumOf> MouseTool::OnSetMarkerTransform(transform& _NewTransform)
{
	std::array<transform, kMovementAxis_NumOf> Transforms;

	for (uint32_t i = 0u; i < kMovementAxis_NumOf; ++i)
	{
		if (m_MovementMarker[i] != nullptr)
		{
			Transforms[i] = m_MovementMarker[i]->Transform;
		}
	}
	return Transforms;
}//---------------------------------------------------------------------------------------------------
