﻿//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef SYNCGROUPWIDGET_H
#define SYNCGROUPWIDGET_H

#include <QGroupBox>
#include <QDockWidget>
#include <qlayout.h>
#include "DataStructures\SyncVariable.h"

namespace Helix
{
	// forward delcs
	//namespace Datastructures
	//{
	//	struct SyncGroup;
	//}

	namespace Nucleo
	{
		class SyncVariableWidget;

		class SyncGroupWidget : public QGroupBox
		{
			Q_OBJECT

		public:
			SyncGroupWidget(Datastructures::SyncGroup* _pSyncGroup, QDockWidget* _pParent = Q_NULLPTR);
			~SyncGroupWidget();

			void Initialize(Datastructures::SyncGroup* _pSyncGroup);

			// Updates variables
			void Update();

		private slots:
			void Hide(bool _bChecked);

		private:
			Datastructures::SyncGroup* m_pSyncGroup;
			std::vector<SyncVariableWidget*> m_Variables;
			QLayout* m_pLayout = nullptr;
			bool m_bVisible = true;
			Datastructures::WidgetLayout m_kLayout = Datastructures::WidgetLayout_Vertical;
		};
	} // Nucleo
} // Helix
#endif // !SYNCGROUPWIDGET_H


