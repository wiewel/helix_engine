//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "EditorControlScript.h"
#include "Scene\LightContext.h"
#include "Scene\CameraManager.h"
#include "Display\Geometry.h"
#include "Display\DX11\MaterialPoolDX11.h"
#include "Resources\GameObjectSerializer.h"
#include "Scene\RenderingScene.h"
#include "Scene\ScriptScene.h"
#include "Sound\Speaker.h"
#include "DataStructures\ShapeComponent.h"
#include "Display\DX11\DebugRenderDX11.h"
#include "Scene\CameraComponent.h"
#include "Physics\ControllerComponent.h"
#include "Scene\NucleoSelectionInterface.h"
#include "Scene\ComponentScene.h"
#include "Scene\SceneGuard.h"

using namespace Helix;
using namespace Nucleo;
using namespace Helix::Scene;
//---------------------------------------------------------------------------------------------------

EditorControlScript::EditorControlScript(const hlx::string& _sLevelName) :
	Scene::IScriptObject(), m_sLevelName(_sLevelName)
{
}
//---------------------------------------------------------------------------------------------------

EditorControlScript::~EditorControlScript()
{
	OnSceneEnd();
}
//---------------------------------------------------------------------------------------------------

void EditorControlScript::OnSceneBegin()
{
	if (m_pMainCamera == nullptr)
	{
		m_pMainCamera = Scene::CameraManager::Instance()->GetCameraByName("MainCamera");
	}

	HASSERT(m_pMainCamera != nullptr, "No camera set!");

	m_Level.Load(m_sLevelName, true, true);

	m_InputReciever.Initialize();
	m_InputReciever.AddContext("Nucleo", S("Editor_InputMapping"));

	if (m_Level.HasLight() == false)
	{
		CreateDefaultLight();
	}

	m_pCamera = m_pMainCamera->GetParent();
	if (m_pCamera->GetComponent<Physics::ControllerComponent>() == nullptr)
	{
		m_pCameraController = m_pCamera->AddComponent<Physics::ControllerComponent>();
		Physics::CapsuleControllerDesc Desc;
		Desc.vPosition = m_pCamera->GetPosition();
		m_pCameraController->CreateBody(Desc);
		m_pCameraController->SetClipping(false);
		m_pCameraController->SetGravityEnabled(false);	
	}

	if (m_pCamera->GetComponent<Sound::Listener>() == nullptr)
	{
		m_pCamera->AddComponent<Sound::Listener>();
	}
}
//---------------------------------------------------------------------------------------------------

void EditorControlScript::OnSceneEnd()
{
	// remove old shape
	RemoveDebugShapes();

	m_InputReciever.Uninitialize();

	m_pDirLightParent = nullptr;

	HSAFE_FUNC_RELEASE(m_pCameraFrustum, Destroy);

	m_Level.Unload();
}
//---------------------------------------------------------------------------------------------------

void EditorControlScript::OnUpdate(double _fDeltaTime, double _fTotalTime)
{
	HASSERT(m_pMainCamera != nullptr, "No camera set!");

	if (m_pCameraFrustum != nullptr)
	{
		m_pCameraFrustum->Transform = m_pMainCamera->GetCullingFrustum().GetTransform();
	}

	//Display::RenderProcedureManagerDX11& Renderer = Scene::RenderingScene::Instance()->GetRenderer();

	float fMoveSpeed = 2.0f;
	
	if (m_InputReciever.Action("Keyboard_Btn_SPACE"))
	{
		Math::transform Transform(m_pMainCamera->GetTransform());
		Transform.q = HQUATERNION_IDENTITY;
		Datastructures::GameObject* pGO = m_Level.SpawnBox(Transform, false, true);		
	}

	// remove object
	if (m_InputReciever.Action("Keyboard_Btn_R"))
	{
		RemoveGameObject(NucleoSelectionInterface::Instance()->GetActiveSelection());
	}
	if (m_InputReciever.State("Move_Fast"))
	{
		fMoveSpeed *= 2.0f;
	}

	// Keyboard movement
	if (m_InputReciever.State("Move_Forward"))
	{
		m_pCameraController->Walk(fMoveSpeed * _fDeltaTime);
	}
	if (m_InputReciever.State("Move_Right"))
	{
		m_pCameraController->Strafe(fMoveSpeed * _fDeltaTime);
	}
	if (m_InputReciever.State("Move_Back"))
	{
		m_pCameraController->Walk(-fMoveSpeed * _fDeltaTime);
	}
	if (m_InputReciever.State("Move_Left"))
	{
		m_pCameraController->Strafe(-fMoveSpeed * _fDeltaTime);
	}

	int32_t iLookX = 0;
	int32_t iLookY = 0;
	float fLookSpeed = 15.0f;
	if (m_InputReciever.Mouse(iLookX, iLookY))
	{
		/// Camera Orientation
		if (m_InputReciever.State("RMB"))
		{
			float fLookX = iLookX;
			float fLookY = iLookY;
			m_pCameraController->Turn(fLookX  * fLookSpeed * _fDeltaTime);
			m_pCameraController->Nod(fLookY * fLookSpeed * _fDeltaTime);
		}
	}

	m_InputReciever.Sync();
}
//---------------------------------------------------------------------------------------------------
void EditorControlScript::OnDestroyObjects()
{
	if (m_ObjectsToDestroy.empty() == false)
	{
		for (Datastructures::GameObject* pGO : m_ObjectsToDestroy)
		{
			HLOG("Object %s destoryed", CSTR(pGO->GetName()));
			pGO->Destroy();
		}

		m_ObjectsToDestroy.clear();
	}
	//RemoveDebugShapes();
}
//---------------------------------------------------------------------------------------------------

void EditorControlScript::OnPreRendering()
{
	RenderDebugShapes();
}
//---------------------------------------------------------------------------------------------------
/// public function
/// _vViewportCoords: upper left = (0,0); lower right = (1,1)
Datastructures::GameObject* EditorControlScript::ScenePicking(const Math::float2& _vViewportCoords, Physics::TCollisionFilterMask _CollisionFilters) const
{
	return ScenePicking(_vViewportCoords, m_pMainCamera, _CollisionFilters);
}
//---------------------------------------------------------------------------------------------------
/// private function
/// _vViewportCoords: upper left = (0,0); lower right = (1,1)
Datastructures::GameObject* EditorControlScript::ScenePicking(const Math::float2& _vViewportCoords, CameraComponent* _pCamera, Physics::TCollisionFilterMask _CollisionFilters) const
{
	Math::float3 vRayWorldNorm  = _pCamera->ScreenPosToWorldRay(_vViewportCoords);

	//HLOG("World Ray: (%f, %f, %f)", vRayWorldNorm.x, vRayWorldNorm.y, vRayWorldNorm.z);

	Datastructures::GameObject* pHitObject = nullptr;
	pHitObject = _pCamera->Raycast(_pCamera->GetPosition(), vRayWorldNorm, _pCamera->GetFarDistance(), nullptr, _CollisionFilters);

	return pHitObject;
}
//---------------------------------------------------------------------------------------------------

void EditorControlScript::RemoveDebugShapes()
{
	Display::DebugRenderDX11* pDbgRender = Display::DebugRenderDX11::Instance();

	// remove old shape
	for (Display::DebugRenderDX11::LineIndices& DbgShape : m_DebugShapes)
	{
		pDbgRender->RemoveDebugObject(DbgShape);
	}

	m_DebugShapes.clear();
}
//---------------------------------------------------------------------------------------------------

void EditorControlScript::RenderDebugShapes()
{
	Datastructures::GameObject* pActiveSelection = NucleoSelectionInterface::Instance()->GetActiveSelection();

	if (pActiveSelection == nullptr)
	{
		return;
	}

	RemoveDebugShapes();

	Display::DebugRenderDX11* pDbgRender = Display::DebugRenderDX11::Instance();

	for (Datastructures::ShapeComponent* pShape : pActiveSelection->GetComponentsByType<Datastructures::ShapeComponent>())
	{
		if (pShape->Type == Physics::GeometryType_Box)
		{		
			const Physics::GeometryDesc& Desc = pShape->GetDescription();

			//if (Desc.kUsage != Physics::ShapeUsageType_Query)
			//	continue;

			Math::transform t = pActiveSelection->Transform * Desc.LocalTransform;

			Math::float3 h = Desc.Box.vHalfExtents * Desc.vScale;

			if (Desc.bScaleWithParent)
			{
				h = h * pActiveSelection->GetRenderingScale();
			}

			std::array<Math::float3, 8> Points;
			Points[kFrustumCorner_NearUpperLeft] = { -h.x, h.y, -h.z };
			Points[kFrustumCorner_NearUpperRight] = { h.x, h.y, -h.z };
			Points[kFrustumCorner_NearLowerRight] = { h.x, -h.y, -h.z };
			Points[kFrustumCorner_NearLowerLeft] = { -h.x, -h.y, -h.z };

			Points[kFrustumCorner_FarUpperLeft] = { -h.x, h.y, h.z };
			Points[kFrustumCorner_FarUpperRight] = { h.x, h.y, h.z };
			Points[kFrustumCorner_FarLowerRight] = { h.x, -h.y, h.z };
			Points[kFrustumCorner_FarLowerLeft] = { -h.x, -h.y, h.z };

			for (uint32_t i = 0; i < 8; ++i)
			{
				Points[i] = t.transform(Points[i]);
			}

			//if (m_DebugPoints != Points)
			//{
			//	//m_DebugPoints = Points;

				switch (Desc.kUsage)
				{
				case Physics::ShapeUsageType_Simulation:
					m_DebugShapes.push_back(pDbgRender->DrawBox(Points, { 1.f, 0.f,0.f }));
					break;
				case Physics::ShapeUsageType_Query:
					m_DebugShapes.push_back(pDbgRender->DrawBox(Points, { 0.f, 1.f,0.f }));
					break;
				case Physics::ShapeUsageType_Trigger:
					m_DebugShapes.push_back(pDbgRender->DrawBox(Points, { 0.f, 0.f, 1.f }));
					break;
				default:
					break;
				}
			//}			
		}
	}
}
//---------------------------------------------------------------------------------------------------

void EditorControlScript::NewLevel()
{
	SceneGuard lock;

	// todo: unselect last active go
	OnSceneEnd();
	CreateDefaultLight();

	m_InputReciever.Initialize();
	m_InputReciever.AddContext("Nucleo", S("Editor_InputMapping"));
}
//---------------------------------------------------------------------------------------------------

void EditorControlScript::ChangeLevel(const hlx::string& _sLevelName)
{
	SceneGuard lock;

	m_sLevelName = _sLevelName;

	OnSceneEnd(); // clean up
	OnSceneBegin(); // load
}
//---------------------------------------------------------------------------------------------------

void EditorControlScript::ExportLevel(const hlx::string& _sLevelFilePath)
{
	SceneGuard lock;

	if (_sLevelFilePath.empty() == false)
	{
		m_Level.Save(_sLevelFilePath);
	}
	else if(m_Level.GetFilePath().empty() == false)
	{
		m_Level.Save(m_Level.GetFilePath());
	}
}
//---------------------------------------------------------------------------------------------------
void EditorControlScript::CreateDefaultLight()
{
	if (m_pDirLightParent == nullptr)
	{
		Math::quaternion vRot = Math::QuaternionFromEulerDegreesXYZ(0.f, 0.f, 70.f);
		Math::transform Transform(HFLOAT3_ZERO, vRot);

		m_pDirLightParent = m_Level.SpawnBox(Transform, true, true);
		if (m_pDirLightParent != nullptr)
		{
			m_pDirLightParent->SetName("MainLight");
			//m_pDirLightParent->SetMesh(nullptr);

			LightComponent* pLight = m_pDirLightParent->AddComponent<LightComponent>(LightType_Directional);

			if (pLight != nullptr)
			{
				pLight->Color = { 0.5f, 0.5f, 0.5f };
				pLight->Intensity = 0.5f;
				m_pDirLightParent->AddSyncComponent(pLight->GetSyncComponent());
			}
		}
	}
}
//---------------------------------------------------------------------------------------------------
void EditorControlScript::RemoveGameObject(Datastructures::GameObject* _pObject)
{
	if (_pObject != nullptr)
	{
		m_Level.RemoveGameObject(_pObject);
		if (m_ObjectsToDestroy.count(_pObject) == 0)
		{
			m_ObjectsToDestroy.insert(_pObject);
		}
	}
}
//---------------------------------------------------------------------------------------------------
