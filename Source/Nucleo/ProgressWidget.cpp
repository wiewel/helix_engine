﻿//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "ProgressWidget.hpp"
#include "QStringHelper.h"

using namespace Helix::Nucleo;
//---------------------------------------------------------------------------------------------------

ProgressWidget::ProgressWidget(QWidget* parent) :
	QWidget(parent)
{
	m_pLayout = HNEW QHBoxLayout(this);

	m_pProgressBar = HNEW QProgressBar(this);
	m_pProgressBar->setAlignment(Qt::AlignCenter);
	m_pProgressBar->setTextVisible(true);

	m_pLayout->addWidget(m_pProgressBar);
	setLayout(m_pLayout);

	connect(this, SIGNAL(SetProgressValue(int)), m_pProgressBar, SLOT(setValue(int)));
	connect(this, SIGNAL(SetProgressRange(int, int)), m_pProgressBar, SLOT(setRange(int, int)));
	connect(this, SIGNAL(SetProgressLabel(const QString&)), this, SLOT(SetProgressLabelSlot(const QString&)));

	m_UpdateFunc = Async::make_delegate(&ProgressWidget::UpdateProgess, this);
}
//---------------------------------------------------------------------------------------------------

ProgressWidget::~ProgressWidget()
{
	HSAFE_DELETE(m_pProgressBar);
	HSAFE_DELETE(m_pLayout);
}
//---------------------------------------------------------------------------------------------------

void ProgressWidget::UpdateProgess(const hlx::string& _sText, int32_t _iValue, int32_t _iMin, int32_t _iMax)
{
	Async::ScopedSpinLock Lock(m_ProgressLock);

	emit SetProgressRange(_iMin, _iMax);
	emit SetProgressValue(_iValue);
	emit SetProgressLabelSlot(fromhlxString(_sText));
}
//---------------------------------------------------------------------------------------------------
void ProgressWidget::SetProgressLabelSlot(const QString& _sText)
{
	m_pProgressBar->setFormat(_sText);
}
//---------------------------------------------------------------------------------------------------
