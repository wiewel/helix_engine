//   Copyright 2020 Steffen Wiewel, Fabian Wahlster, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef MOUSETOOLROTATE_H
#define MOUSETOOLROTATE_H

#include "MouseTool.h"

#include "Display\DX11\DebugRenderDX11.h"
#include "Scene\CameraComponent.h"

namespace Helix
{
	namespace Nucleo
	{
		//---------------------------------------------------------------------------------------------------
		class MouseToolRotate : public MouseTool
		{
			Q_OBJECT

		public:
			MouseToolRotate(QWidget* _pParent = Q_NULLPTR);
			~MouseToolRotate();

		private:
			void ShowDirectionArrow(bool _bShow);

			void SpawnMovementMarkers() final;
			void OnMouseEvent(const NucleoMouseEvent* _pEvent, const EditorControlScript* _pControlScript) final;
			std::array<Math::transform, kMovementAxis_NumOf> OnSetMarkerTransform(Math::transform& _NewTransform) final;

			void OnActivate() final;
			void OnDisable() final;

		private:
			Datastructures::GameObject* m_pZDirectionArrow = nullptr;
		};
		//---------------------------------------------------------------------------------------------------
	}
} // Helix

#endif // MOUSETOOLROTATE_H
