//   Copyright 2020 Steffen Wiewel, Fabian Wahlster, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "MouseToolMove.h"

#include "EditorControlScript.h"
#include "NucleoMouseEvent.h"

using namespace Helix::Nucleo;
using namespace Helix::Datastructures;
using namespace Helix::Math;
using namespace Helix::Scene;

//---------------------------------------------------------------------------------------------------
MouseToolMove::MouseToolMove(QWidget* _pParent) :
	MouseTool(kMouseToolType_Move,
		qApp->style()->standardIcon(QStyle::SP_DesktopIcon),
		_pParent)
{
	SpawnMovementMarkers();
}
//---------------------------------------------------------------------------------------------------
MouseToolMove::~MouseToolMove()
{
}
//---------------------------------------------------------------------------------------------------
void MouseToolMove::SpawnMovementMarkers()
{
	for (uint32_t i = 0u; i < kMovementAxis_NumOf; ++i)
	{
		Display::MaterialProperties MatProp;
		MatProp.kRenderPasses = Display::g_kGizmoPass();
		MatProp.fRoughness = 1.0f;
		MatProp.fMetallic = 0.0f;

		GameObject* pGO = GameObjectPool::Instance()->Create().get();
		if (pGO != nullptr)
		{
			const float fLength = 1.0f;
			std::string sName;
			float3 vColor;
			float3 vOffset;
			quaternion vOrientation;
			float3 vExtents = { fLength, 0.1f, 0.1f };
			switch (i)
			{
			case kMovementAxis_X:
				sName = "MovementMarkerX";
				//vExtents = { fLength, 0.25f, 0.25f };
				vOffset = { fLength, 0.0f, 0.0f };
				vColor = { 1.0f, 0.0f, 0.0f };
				vOrientation = quaternion();
				break;
			case kMovementAxis_Y:
				sName = "MovementMarkerY";
				//vExtents = { 0.25f, fLength, 0.25f };
				vOffset = { 0.0f, fLength, 0.0f };
				vColor = { 0.0f, 1.0f, 0.0f };
				vOrientation = QuaternionFromEulerDegreesXYZ(0.0f, 0.0f, 90.0f);
				break;
			case kMovementAxis_Z:
				sName = "MovementMarkerZ";
				//vExtents = { 0.25f, 0.25f, fLength };
				vOffset = { 0.0f, 0.0f, fLength };
				vColor = { 0.0f, 0.0f, 1.0f };
				vOrientation = QuaternionFromEulerDegreesXYZ(0.0f, -90.0f, 0.0f);
				break;
			default:
				break;
			}

			pGO->SetMesh(Display::MeshClusterDX11("Arrow"));
			pGO->CreatePhysicsActorDynamic(transform(vOffset, vOrientation), 1.0f, true);
			MatProp.vAlbedo = vColor;

			pGO->SetName(sName);

			Physics::GeometryDesc QueryShape;
			QueryShape.kType = Physics::GeometryType_Box;
			QueryShape.Box.vHalfExtents = vExtents;
			QueryShape.kUsage = Physics::ShapeUsageType_Query;
			QueryShape.Filter = Physics::CollisionFilter(Physics::CollisionFilterFlag_MouseTool);
			pGO->AddShape(QueryShape);

			pGO->AddToScene();

			Display::MaterialDesc MatDesc(MatProp, sName + "Mat", false, false);
			Display::MaterialDX11 Mat(MatDesc);

			pGO->SetMaterial(Mat);
		}

		m_MovementMarker[i] = pGO;
	}
	ShowMarker(false);
}
//---------------------------------------------------------------------------------------------------
void MouseToolMove::OnMouseEvent(const NucleoMouseEvent* _pEvent, const EditorControlScript* _pControlScript)
{
	float2 vMousePos = _pEvent->GetMousePosition();

	//{
	//	float3 vRayWorldNorm = _pControlScript->GetCamera()->ScreenPosToWorldRay(_pEvent->GetMousePosition());
	//	float3 vStart = _pControlScript->GetCamera()->GetPosition();
	//	float3 vEnd = vStart + vRayWorldNorm * 100.0f;
	//	Display::DebugRenderDX11::Instance()->DrawLine(vStart, vEnd, {0,0,1});
	//}

	/// only execute this code when left button is already (and still) down and not when the event is fired!
	if (_pEvent->GetMouseButtonDown() != Qt::LeftButton &&
		_pEvent->GetNativeEvent().buttons() & Qt::LeftButton &&
		GetMovementAxis() != kMovementAxis_Inactive)
	{
		GameObject* pActiveSelection = GetActiveSelection();

		// vMousePos: [0,0] => upper left, [1,1] lower right
		FrustumCorners Corners = m_pCamera->GetFrustumCorners();

		float fObjDepth = m_pCamera->GetProjectedPointOnNearPlane(pActiveSelection->Position).distance(pActiveSelection->Position);
		fObjDepth /= (m_pCamera->GetFarDistance() - m_pCamera->GetNearDistance());

		/// Calculate side vectors of frustum with object distance as length -> shortened frustum
		float3 vUpperLeft = Corners[kFrustumCorner_NearUpperLeft] + (Corners[kFrustumCorner_FarUpperLeft] - Corners[kFrustumCorner_NearUpperLeft]) * fObjDepth;
		float3 vUpperRight = Corners[kFrustumCorner_NearUpperRight] + (Corners[kFrustumCorner_FarUpperRight] - Corners[kFrustumCorner_NearUpperRight]) * fObjDepth;
		float3 vLowerLeft = Corners[kFrustumCorner_NearLowerLeft] + (Corners[kFrustumCorner_FarLowerLeft] - Corners[kFrustumCorner_NearLowerLeft]) * fObjDepth;

		/// Project mouse positions with object depth
		float3 vMouseOnDepth = vUpperLeft + (vUpperRight - vUpperLeft) * vMousePos.x;
		vMouseOnDepth += (vLowerLeft - vUpperLeft) * vMousePos.y;
		float3 vLastMouseOnDepth = vUpperLeft + (vUpperRight - vUpperLeft) * GetLastMousePosition().x;
		vLastMouseOnDepth += (vLowerLeft - vUpperLeft) * GetLastMousePosition().y;

		/// Finally calculate mouse movement in worldspace
		float3 vMouseMovement = (vMouseOnDepth - vLastMouseOnDepth);

		float3 vNewPos = pActiveSelection->Position;
		float3 vAxis;
		switch (GetMovementAxis())
		{
		case kMovementAxis_X:
			vAxis = {1.0f, 0.0f, 0.0f};
			break;
		case kMovementAxis_Y:
			vAxis = {0.0f, 1.0f, 0.0f};
			break;
		case kMovementAxis_Z:
			vAxis = {0.0f, 0.0f, 1.0f};
			break;
		default:
			HASSERT(false, "Movement Tool Failure");
			break;
		}
		/// TODO: finish the more sophisticated model below
		///// axis is projected onto our on screen mouse movement
		//float3 vProjectedAxis = vAxis.projectOnto(vMouseMovement);
		///// the magnitude of the projected axis gives us our multiplicator to scale the correct axis with
		//float fAxisScale = copysignf(vMouseMovement.magnitude() / vProjectedAxis.magnitude(), vMouseMovement.dot(vProjectedAxis));
		///// then we add the scaled axis to the old position to get our new one
		//vNewPos += fAxisScale * vAxis;

		vNewPos += vMouseMovement.projectOnto(vAxis);

		pActiveSelection->Position = vNewPos;
		SetMarkerTransform(pActiveSelection->Transform);
	}
}
//---------------------------------------------------------------------------------------------------
std::array<transform, MouseTool::kMovementAxis_NumOf> MouseToolMove::OnSetMarkerTransform(transform& _NewTransform)
{
	std::array<transform, kMovementAxis_NumOf> ScaledTrans;

	for (uint32_t i = 0u; i < kMovementAxis_NumOf; ++i)
	{
		if (m_MovementMarker[i] != nullptr)
		{
			ScaledTrans[i] = m_MovementMarker[i]->Transform;
			ScaledTrans[i].p = m_MarkerWorldTransform.p;
		}
	}

	if (m_pCamera != nullptr)
	{
		const float fNearFarDist = m_pCamera->GetFarDistance() - m_pCamera->GetNearDistance();
		FrustumCorners Corners = m_pCamera->GetFrustumCorners();
		float fObjDepth = m_pCamera->GetProjectedPointOnNearPlane(_NewTransform.p).distance(_NewTransform.p);
		fObjDepth /= fNearFarDist;

		/// Calculate side vectors of frustum with object distance as length -> shortened frustum
		float3 vUpperLeft = Corners[kFrustumCorner_NearUpperLeft] + (Corners[kFrustumCorner_FarUpperLeft] - Corners[kFrustumCorner_NearUpperLeft]) * fObjDepth;
		float3 vUpperRight = Corners[kFrustumCorner_NearUpperRight] + (Corners[kFrustumCorner_FarUpperRight] - Corners[kFrustumCorner_NearUpperRight]) * fObjDepth;
		float3 vLowerLeft = Corners[kFrustumCorner_NearLowerLeft] + (Corners[kFrustumCorner_FarLowerLeft] - Corners[kFrustumCorner_NearLowerLeft]) * fObjDepth;

		float3 vRight = vUpperRight - vUpperLeft;
		float3 vDown = vLowerLeft - vUpperLeft;

		float3 vToObj = _NewTransform.p - vUpperLeft;

		float3 vAxis = vToObj.projectOnto(vRight);
		float fDistanceX = copysignf(vAxis.magnitude() / vRight.magnitude(), vAxis.dot(vRight));

		vAxis = vToObj.projectOnto(vDown);
		float fDistanceY = copysignf(vAxis.magnitude() / vDown.magnitude(), vAxis.dot(vDown));

		/// Build new position on given depth
		float fNewDepth = m_fGizmoScale / fNearFarDist;
		vUpperLeft = Corners[kFrustumCorner_NearUpperLeft] + (Corners[kFrustumCorner_FarUpperLeft] - Corners[kFrustumCorner_NearUpperLeft]) * fNewDepth;
		vUpperRight = Corners[kFrustumCorner_NearUpperRight] + (Corners[kFrustumCorner_FarUpperRight] - Corners[kFrustumCorner_NearUpperRight]) * fNewDepth;
		vLowerLeft = Corners[kFrustumCorner_NearLowerLeft] + (Corners[kFrustumCorner_FarLowerLeft] - Corners[kFrustumCorner_NearLowerLeft]) * fNewDepth;

		vRight = vUpperRight - vUpperLeft;
		vDown = vLowerLeft - vUpperLeft;

		float3 vNewPos = vUpperLeft + vRight * fDistanceX;
		vNewPos += vDown * fDistanceY;

		for (uint32_t i = 0u; i < kMovementAxis_NumOf; ++i)
		{
			if (m_MovementMarker[i] != nullptr)
			{
				ScaledTrans[i].p = static_cast<physx::PxVec3>(vNewPos);
			}
		}
	}
	return ScaledTrans;
}
//---------------------------------------------------------------------------------------------------
