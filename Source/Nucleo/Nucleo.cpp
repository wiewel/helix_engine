//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "Nucleo.h"
#include "Resources\FileSystem.h"
#include "Datastructures\SyncDockManager.h"
#include "Scene\ScriptScene.h"
#include "Scene\RenderingScene.h"
#include <qfiledialog.h>
#include "NucleoMouseEvent.h"
#include "QStringHelper.h"
#include "DataStructures\ShapeComponent.h"
#include "Scene\RotationComponent.h"
#include "Sound\Speaker.h"
#include "Util\AdvancedProfiler.h"
#include "DefaultStyleSheet.h"

// TODO: remove (it is RedLine specific)
#include "../TheRedLine/StatueComponent.h"

using namespace Helix::Nucleo;
using namespace Helix::Datastructures;
using namespace Helix::Async;

//---------------------------------------------------------------------------------------------------

Nucleo::Nucleo(const QString& _sActiveLevel)
	: QMainWindow(nullptr), m_ControlScript(hlx::to_string(_sActiveLevel.toStdWString()))
{
	ui.setupUi(this);

	InitializeMenuBar();

	// ensure widget manager registers before engine is initialized
	SyncDockManager::Instance()->SetBindDelegates(
		make_delegate(&Nucleo::Bind, this),
		make_delegate(&Nucleo::Unbind, this));

	m_pViewport = HNEW Viewport(this);
	Scene::PhysicsScene::Instance()->SetSimulatePhysics(false);

	{
		hlx::fbytestream Style;
		if (Resources::FileSystem::Instance()->OpenFileStream(Resources::HelixDirectories_EngineConfigs, S("nucleo.stylesheet"), std::ios::in, Style))
		{
			std::string sStyleSheet = Style.get<std::string>();
			qApp->setStyleSheet(sStyleSheet.c_str());
		}
		else
		{
			HLOG("Loading default style sheet 'dark'");
			qApp->setStyleSheet(pDefaultStyleSheet);
		}
	}

	// initialize widgets
	UpdateWidgets();

	m_pUpdateTimer = HNEW QTimer(this);
	connect(m_pUpdateTimer, SIGNAL(timeout()), this, SLOT(UpdateWidgets()));
	connect(m_pUpdateTimer, SIGNAL(timeout()), this, SLOT(UpdateStatusBar()));

	// for debugging:
	//m_iWidgetRefreshRate = 10000;

	m_pUpdateTimer->start(m_iWidgetRefreshRate);

	/// Gizmo Update
	m_pGizmoUpdateTimer = HNEW QTimer(this);
	connect(m_pGizmoUpdateTimer, SIGNAL(timeout()), this, SLOT(UpdateGizmos()));
	m_pGizmoUpdateTimer->start(m_iGizmoRefreshRate);

	setCentralWidget(m_pViewport);

	m_pProgressWidget = HNEW ProgressWidget(this);
	if (statusBar() != nullptr)
	{
		statusBar()->addPermanentWidget(m_pProgressWidget);
	}

	m_ControlScript.SetProgressFunction(m_pProgressWidget->GetProgressFunctor());
	m_AssetImporter.SetProgressFunction(m_pProgressWidget->GetProgressFunctor());

	/// initialize control widgets
	m_pMouseToolbarWidget = HNEW MouseToolbarWidget(this);
	ui.mainToolBar->addWidget(m_pMouseToolbarWidget);
	m_pMouseToolbarWidget->show();

	m_pImportAssetDialog = HNEW AssetImportDialog();

	//LoadSettings();

	move(0, 0);
}

//---------------------------------------------------------------------------------------------------
Nucleo::~Nucleo()
{
	//{
	//	QSettings settings("settings.ini", QSettings::Format::IniFormat); //("HELiX Corporation", "Nucleo");

	//	settings.setValue("geometry", saveGeometry());
	//	settings.setValue("windowState", saveState());
	//}

	SyncDockManager::Instance()->SetBindDelegates(nullptr, nullptr);
	m_Docks.clear();

	HSAFE_DELETE(m_pImportAssetDialog);
	HSAFE_DELETE(m_pUpdateTimer);
	HSAFE_DELETE(m_pGizmoUpdateTimer);
	HSAFE_DELETE(m_pImportAssetAction);
	HSAFE_DELETE(m_pConnectToPVDAction);
	HSAFE_DELETE(m_pStartEngineAction);
	HSAFE_DELETE(m_pOpenLevelAction);

	HSAFE_DELETE(m_pCreateLightComponentAction);
	HSAFE_DELETE(m_pCreateSoundComponentAction);
	HSAFE_DELETE(m_pCreateShapeComponentAction);
	HSAFE_DELETE(m_pCreateRotationComponentAction);

	HSAFE_DELETE(m_pToggleDocksAction);

	HSAFE_MAP_DELETE(m_Widgets);
	m_Widgets.clear();

	HSAFE_DELETE(m_pMouseToolbarWidget);

	HSAFE_DELETE(m_pViewport);
}
//---------------------------------------------------------------------------------------------------

void Nucleo::LoadSettings()
{
	QSettings settings("settings.ini", QSettings::Format::IniFormat); //("HELiX Corporation", "Nucleo");

	QByteArray state = settings.value("windowState", QByteArray()).toByteArray();
	QByteArray geometry = settings.value("geometry", QByteArray()).toByteArray();

	restoreState(state);
	restoreGeometry(geometry);
}
//---------------------------------------------------------------------------------------------------
void Nucleo::customEvent(QEvent* _pEvent)
{
	if (_pEvent->type() == kNucleoMouseEvent && m_pMouseToolbarWidget != nullptr)
	{
		NucleoMouseEvent* pNucleoMouseEvent = static_cast<NucleoMouseEvent*>(_pEvent);
		m_pMouseToolbarWidget->HandleMouseEvent(pNucleoMouseEvent, &m_ControlScript);
	}
}
//---------------------------------------------------------------------------------------------------
void Nucleo::Bind(const SyncDock* _pSyncDock)
{
	Async::ScopedSpinLock Lock(m_DockLock);

	m_Docks[_pSyncDock->sName] = _pSyncDock;
}
//---------------------------------------------------------------------------------------------------
void Nucleo::Unbind(const SyncDock* _pSyncDock)
{
	Async::ScopedSpinLock Lock(m_DockLock);

	TDockMap::iterator dit = m_Docks.find(_pSyncDock->sName);

	if (dit != m_Docks.end())
	{
		if (dit->second == _pSyncDock)
		{
			TWidgetMap::iterator wit = m_Widgets.find(_pSyncDock->sName);
			SyncDockWidget* pWidget = wit->second;
			m_Docks.erase(dit);
		}
	}
}
//---------------------------------------------------------------------------------------------------
void Nucleo::InitializeMenuBar()
{
	//---------------------------------------------------------------------------------------------------
	// file menu
	//---------------------------------------------------------------------------------------------------

	if (m_pFileMenu == nullptr)
	{
		m_pFileMenu = menuBar()->addMenu(tr("File"));
	}

	if (m_pNewLevelAction == nullptr)
	{
		m_pNewLevelAction = HNEW QAction("&New Level", this);
		connect(m_pNewLevelAction, &QAction::triggered, this, &Nucleo::NewLevel);
		m_pFileMenu->addAction(m_pNewLevelAction);
	}

	if (m_pOpenLevelAction == nullptr)
	{
		m_pOpenLevelAction = HNEW QAction("&Open Level", this);
		connect(m_pOpenLevelAction, &QAction::triggered, this, &Nucleo::OpenLevel);
		m_pFileMenu->addAction(m_pOpenLevelAction);
	}

	if (m_pSaveLevelAction == nullptr)
	{
		m_pSaveLevelAction = HNEW QAction("&Save Level", this);
		connect(m_pSaveLevelAction, &QAction::triggered, this, &Nucleo::SaveLevel);
		m_pFileMenu->addAction(m_pSaveLevelAction);
	}

	if (m_pImportAssetAction == nullptr)
	{
		m_pImportAssetAction = HNEW QAction("Import Asset", this);
		connect(m_pImportAssetAction, &QAction::triggered, this, &Nucleo::ImportAsset);
		m_pFileMenu->addAction(m_pImportAssetAction);
	}

	//---------------------------------------------------------------------------------------------------
	// component menu
	//---------------------------------------------------------------------------------------------------
	
	if (m_pComponentMenu == nullptr)
	{
		m_pComponentMenu = menuBar()->addMenu(tr("&Create Component"));
	}

	if (m_pCreateLightComponentAction == nullptr)
	{
		m_pCreateLightComponentAction = HNEW QAction("Light", this);
		connect(m_pCreateLightComponentAction, &QAction::triggered, this, &Nucleo::CreateLightComponent);
		m_pComponentMenu->addAction(m_pCreateLightComponentAction);
	}

	if (m_pCreateShapeComponentAction == nullptr)
	{
		m_pCreateShapeComponentAction = HNEW QAction("Shape", this);
		connect(m_pCreateShapeComponentAction, &QAction::triggered, this, &Nucleo::CreateShapeComponent);
		m_pComponentMenu->addAction(m_pCreateShapeComponentAction);
	}

	if (m_pCreateSoundComponentAction == nullptr)
	{
		m_pCreateSoundComponentAction = HNEW QAction("Sound", this);
		connect(m_pCreateSoundComponentAction, &QAction::triggered, this, &Nucleo::CreateSoundComponent);
		m_pComponentMenu->addAction(m_pCreateSoundComponentAction);
	}

	if (m_pCreateRotationComponentAction == nullptr)
	{
		m_pCreateRotationComponentAction = HNEW QAction("Rotation", this);
		connect(m_pCreateRotationComponentAction, &QAction::triggered, this, &Nucleo::CreateRotationComponent);
		m_pComponentMenu->addAction(m_pCreateRotationComponentAction);
	}

	// TODO: remove (it is RedLine specific)
	if (m_pCreateStatueComponentAction == nullptr)
	{
		m_pCreateStatueComponentAction = HNEW QAction("Statue", this);
		connect(m_pCreateStatueComponentAction, &QAction::triggered, this, &Nucleo::CreateStatueComponent);
		m_pComponentMenu->addAction(m_pCreateStatueComponentAction);
	}

	//---------------------------------------------------------------------------------------------------
	// View menu
	//---------------------------------------------------------------------------------------------------

	if (m_pViewMenu == nullptr)
	{
		m_pViewMenu = menuBar()->addMenu(tr("View"));
	}

	if (m_pToggleDocksAction == nullptr)
	{
		m_pToggleDocksAction = HNEW QAction("Toggle Docks", this);
		connect(m_pToggleDocksAction, &QAction::triggered, this, &Nucleo::ToggleDocks);
		m_pViewMenu->addAction(m_pToggleDocksAction);
	}

	if (m_pFloatDocksAction == nullptr)
	{
		m_pFloatDocksAction = HNEW QAction("Float/Pin Docks", this);
		connect(m_pFloatDocksAction, &QAction::triggered, this, &Nucleo::FloatDocks);
		m_pViewMenu->addAction(m_pFloatDocksAction);
	}

	//---------------------------------------------------------------------------------------------------
	// other
	//---------------------------------------------------------------------------------------------------

	if (m_pStartEngineAction == nullptr)
	{
		m_pStartEngineAction = HNEW QAction("Start", this);
		connect(m_pStartEngineAction, &QAction::triggered, this, &Nucleo::StartEngine);
		menuBar()->addAction(m_pStartEngineAction);
	}

	if (m_pConnectToPVDAction == nullptr)
	{
		m_pConnectToPVDAction = HNEW QAction("Connect To PVD", this);
		connect(m_pConnectToPVDAction, &QAction::triggered, this, &Nucleo::ConnectToPVD);
		menuBar()->addAction(m_pConnectToPVDAction);
	}

	if (m_pEnableProfilerAction == nullptr)
	{
		m_pEnableProfilerAction = HNEW QAction("Enable Profiler", this);
		connect(m_pEnableProfilerAction, &QAction::triggered, this, &Nucleo::ToggleProfiler);
		menuBar()->addAction(m_pEnableProfilerAction);
	}
}
//---------------------------------------------------------------------------------------------------

void Nucleo::UpdateStatusBar()
{
	Scene::RenderingScene* pRenderingScene = Scene::RenderingScene::Instance();

	uint32_t uObjects = pRenderingScene->GetGatheredObjectCount();
	float fRendering = pRenderingScene->GetExecutionTime();
	float fRenderingTimed = pRenderingScene->GetDeltaTime();
	float fScript = Scene::ScriptScene::Instance()->GetExecutionTime();
	float fPhysics = Scene::PhysicsScene::Instance()->GetExecutionTime();

	QString sStatus = QString::asprintf("Rendering %.2f (%.2f) FPS %.2f (%.2f) ms Scripts %.2f ms Physics %.2f ms Objects %u",
					1.f / fRenderingTimed, 1.f/ fRendering, fRenderingTimed * 1000.f, fRendering * 1000.f, fScript* 1000.f, fPhysics * 1000.f, uObjects);

	if (statusBar() != nullptr)
	{
		statusBar()->showMessage(sStatus);
	}
}
//---------------------------------------------------------------------------------------------------

void Nucleo::UpdateGizmos()
{
	if(m_pMouseToolbarWidget != nullptr)
	{
		m_pMouseToolbarWidget->Update(m_ControlScript.GetCamera());
	}
}
//---------------------------------------------------------------------------------------------------

void Nucleo::StartEngine()
{
	Async::ScopedSpinLock Lock(m_DockLock);

	Scene::PhysicsScene* pPhysics = Scene::PhysicsScene::Instance();
	// physics scene still active (not shutdown)
	if (pPhysics->IsRunning())
	{
		if (pPhysics->GetSimulatePhysics())
		{
			HLOG("Physics paused");
			pPhysics->SetSimulatePhysics(false);
			m_pStartEngineAction->setText("Start");
		}
		else
		{
			HLOG("Physics unpaused");
			pPhysics->SetSimulatePhysics(true);
			m_pStartEngineAction->setText("Pause");
		}
	}

}
//---------------------------------------------------------------------------------------------------

void Nucleo::ConnectToPVD()
{
	Scene::PhysicsScene::Instance()->ConnectToPVD();
}
//---------------------------------------------------------------------------------------------------

void Nucleo::ToggleProfiler()
{
	AdvancedProfiler* pProfiler = AdvancedProfiler::Instance();

	if (pProfiler->GetEnabled())
	{
		pProfiler->SetEnabled(false);
		m_pEnableProfilerAction->setText("Enable Profiler");
	}
	else
	{
		pProfiler->SetEnabled(true);
		m_pEnableProfilerAction->setText("Disable Profiler");
	}
}
//---------------------------------------------------------------------------------------------------

void Nucleo::NewLevel()
{
	m_pUpdateTimer->stop();
	m_pGizmoUpdateTimer->stop();

	Async::ScopedSpinLock Lock(m_UpdateLevelLock);

	m_ControlScript.NewLevel();

	m_pUpdateTimer->start(m_iWidgetRefreshRate);
	m_pGizmoUpdateTimer->start(m_iGizmoRefreshRate);
}
//---------------------------------------------------------------------------------------------------

void Nucleo::OpenLevel()
{
	// load level
	QString sLevelFolder = fromhlxString(Resources::FileSystem::Instance()->GetKnownDirectoryPath(Resources::HelixDirectories_Levels));

	QString sLevelName = QFileDialog::getOpenFileName(this,
		tr("Open Level"), sLevelFolder, tr("HELiX Level Files (*.hell)"), nullptr, QFileDialog::DontUseNativeDialog);

	if (sLevelName.isEmpty() == false)
	{
		m_pUpdateTimer->stop();
		m_pGizmoUpdateTimer->stop();

		Async::ScopedSpinLock Lock(m_UpdateLevelLock);

		//QFileInfo Info(sLevelName);
		m_ControlScript.ChangeLevel(hlx::to_string(sLevelName.toStdWString())); // hlx::to_string(Info.baseName().toStdWString())

		m_pUpdateTimer->start(m_iWidgetRefreshRate);
		m_pGizmoUpdateTimer->start(m_iGizmoRefreshRate);
	}
}
//---------------------------------------------------------------------------------------------------

void Nucleo::SaveLevel()
{
	// save level
	QString sLevelFolder = fromhlxString(Resources::FileSystem::Instance()->GetKnownDirectoryPath(Resources::HelixDirectories_Levels));

	QString sLevelName = QFileDialog::getSaveFileName(this,
		tr("Open Level"), sLevelFolder, tr("HELiX Level Files (*.hell)"), nullptr, QFileDialog::DontUseNativeDialog);

	if (sLevelName.isEmpty() == false)
	{
		m_pUpdateTimer->stop();
		m_pGizmoUpdateTimer->stop();

		Async::ScopedSpinLock Lock(m_UpdateLevelLock);

		hlx::string sPath = STR(sLevelName.toStdWString());
		if (hlx::ends_with(sPath, S(".hell")) == false)
		{
			sPath += S(".hell");
		}

		m_ControlScript.ExportLevel(sPath);

		m_pUpdateTimer->start(m_iWidgetRefreshRate);
		m_pGizmoUpdateTimer->start(m_iGizmoRefreshRate);
	}
}
//---------------------------------------------------------------------------------------------------

void Nucleo::ImportAsset()
{
	m_AssetImporter.Stop();

	if (m_pImportAssetDialog->Show())
	{
		std::vector<hlx::string> Meshes = m_pImportAssetDialog->GetSelectedFiles();

		if (Meshes.empty())
			return;
		
		m_AssetImporter.SetFilesToImport(Meshes);
		m_AssetImporter.SetCoordinateSystem(m_pImportAssetDialog->GetCoordinateSystem());
		m_AssetImporter.SetCreateConvexHull(m_pImportAssetDialog->GetConvexHull());
		m_AssetImporter.SetFlipV(m_pImportAssetDialog->GetFilpV());
		m_AssetImporter.SetImportMaterials(m_pImportAssetDialog->GetImportMaterials());
		m_AssetImporter.SetImportTextures(m_pImportAssetDialog->GetImportTextures());
		m_AssetImporter.SetReverseWinding(m_pImportAssetDialog->GetReverseWinding());
		m_AssetImporter.SetScale(m_pImportAssetDialog->GetScale());
		m_AssetImporter.SetToLefthanded(m_pImportAssetDialog->GetToLefthanded());
		m_AssetImporter.SetVertexLayout(m_pImportAssetDialog->GetVertexLayout());
		m_AssetImporter.SetMipLevel(m_pImportAssetDialog->GetMipLevel());
		m_AssetImporter.SetReversePivot(m_pImportAssetDialog->GetReversePivot());
		m_AssetImporter.SetNullPivot(m_pImportAssetDialog->GetNullPivot());
		m_AssetImporter.StartInNewThread(false, false);
	}
}
//---------------------------------------------------------------------------------------------------

void Nucleo::CreateLightComponent()
{
	AddComponent<Scene::LightComponent>(Scene::LightType_Point);
}
//---------------------------------------------------------------------------------------------------
void Nucleo::CreateShapeComponent()
{
	Physics::GeometryDesc Desc;
	AddComponent<Datastructures::ShapeComponent>(Desc);
}
//---------------------------------------------------------------------------------------------------
void Nucleo::CreateSoundComponent()
{
	AddComponent<Sound::Speaker>(Sound::Mixer::ChannelGroup::SFX);
}
//---------------------------------------------------------------------------------------------------
void Nucleo::CreateRotationComponent()
{
	AddComponent<Helix::Scene::RotationComponent>(Math::float3(0.f, 1.f, 0.f), 1.f, true, true);
}
//---------------------------------------------------------------------------------------------------
// TODO: remove (it is RedLine specific)
void Nucleo::CreateStatueComponent()
{
	RedLine::StatueSettings Settings;
	AddComponent<RedLine::StatueComponent>(Settings);
}
//---------------------------------------------------------------------------------------------------

void Nucleo::ToggleDocks()
{
	Async::ScopedSpinLock Lock(m_DockLock);

	m_bDocksVisible = !m_bDocksVisible;

	for (TWidgetMap::value_type& KV : m_Widgets)
	{
		KV.second->setVisible(m_bDocksVisible);
	}
}
//---------------------------------------------------------------------------------------------------

void Nucleo::FloatDocks()
{
	Async::ScopedSpinLock Lock(m_DockLock);

	m_bFloatDocks = !m_bFloatDocks;

	for (TWidgetMap::value_type& KV : m_Widgets)
	{
		KV.second->setFloating(m_bFloatDocks);
	}
}
//---------------------------------------------------------------------------------------------------

void Nucleo::UpdateWidgets()
{
	Async::ScopedSpinLock DockLock(m_DockLock);
	Async::ScopedSpinLock LevelLock(m_UpdateLevelLock);

	for (const TDockMap::value_type& SDock : m_Docks)
	{
		const Datastructures::SyncDock* pDock = SDock.second;

		// dont allow changes the dock (add / remove groups)
		std::lock_guard<std::recursive_mutex> lock(pDock->GetLockObject());

		TWidgetMap::iterator it = m_Widgets.find(pDock->sName);

		SyncDockWidget* pWidget = nullptr;
		if (it == m_Widgets.end())
		{
			pWidget = HNEW SyncDockWidget(pDock, this);
			Qt::DockWidgetArea area = static_cast<Qt::DockWidgetArea>(pDock->kAnchor);
			addDockWidget(area, pWidget/*, Qt::Orientation::Vertical*/);

			//restoreDockWidget(pWidget);

			if (pDock->bFloating == false)
			{
				SyncDockWidget*& pLastWidget = m_LastDockWidgets[Math::Log2(pDock->kAnchor)];

				if (pLastWidget != nullptr)
				{
					tabifyDockWidget(pLastWidget, pWidget);
				}

				pLastWidget = pWidget;
			}

			m_Widgets[pDock->sName] = pWidget;
			pWidget->show();
			pWidget->setEnabled(pDock->IsRegisterTemplate() == false);

			if (pDock->bOnTopTab)
			{
				m_TopDockWidgets[Math::Log2(pDock->kAnchor)] = pWidget;
			}

			SyncDockWidget* pTopWidget = m_TopDockWidgets[Math::Log2(pDock->kAnchor)];

			if (pTopWidget != nullptr)
			{
				QList<QDockWidget*> TabbedWidgets = tabifiedDockWidgets(pTopWidget);
				for (QDockWidget* pTabbedWidget : TabbedWidgets)
				{
					tabifyDockWidget(pTabbedWidget, pTopWidget);
				}
			}
		}
		else
		{
			pWidget = it->second;
			if (pWidget->GetEngineDock() != pDock)
			{
				pWidget->Initialize(pDock, true);
			}			
		}

		if (pDock->IsRegisterTemplate() == false)
		{
			pWidget->setEnabled(true);
			// update variables
			pWidget->Update();
		}
	}
}
//---------------------------------------------------------------------------------------------------
