HLX - The HELiX C++ HeaderOnly Lib

This collection consists of:

- TextToken a key-value parser for .acf files used by Valve
- Extended std basic_bytes and streams in Bytes.h, ByteStream.h & FileStream.h
- UNICODE dependant strings and helper functions in String.h & StringHelpers.h
- Templated Vector Type in Vector.h