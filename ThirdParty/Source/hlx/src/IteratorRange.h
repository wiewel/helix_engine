//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef ITERATORRANGE_H
#define ITERATORRANGE_H

#include <iterator>

// http://en.cppreference.com/w/cpp/concept/Container

namespace hlx
{
	template <typename Iterator>
	struct IteratorRange
	{
		IteratorRange(Iterator _start, Iterator _end) :
			m_start(_start), m_end(_end) {}

		IteratorRange(const IteratorRange& _other) : m_start(_other.m_start), m_end(_other.m_end) {}

		inline auto begin() const { return m_start; }
		inline auto end() const { return m_end; }

		inline auto cbegin() const { return m_start; }
		inline auto cend() const { return m_end; }

		inline auto size() const { return std::distance(m_end, m_start); }
		inline bool empty() const { return m_start == m_end; }

	private:
		Iterator m_start;
		Iterator m_end;
	};

	template <typename Iterator>
	inline IteratorRange<Iterator> make_range(Iterator _start, Iterator _end)
	{
		return IteratorRange<Iterator>(_start, _end);
	}

	// usage: for(auto& i : make_range(start, end){i++;}
} // hlx

#endif // !ITERATORRANGE_H
