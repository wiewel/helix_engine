//   Copyright 2020 Fabian Wahlster, Steffen Wiewel, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

// All defines of this header are in the global scope outside the hlx namespace

#ifndef HLX_STANDARDDEFINES_H
#define HLX_STANDARDDEFINES_H

//---------------------------------------------------------------------------------------------------
// Mem Debugging

#ifdef _DEBUG

#if !defined(NO_CRT_DEBUG) && defined(_WIN32)

//#ifndef _CRTDBG_MAP_ALLOC
//#define _CRTDBG_MAP_ALLOC
//#endif

#include <stdlib.h>
#include <crtdbg.h>

inline void HInitDebug(void)
{
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF /*| _CRTDBG_CHECK_ALWAYS_DF*/);
	_CrtSetReportMode(_CRT_ERROR, _CRTDBG_MODE_DEBUG);
}

inline void HBreakOnAlloc(long iAllocId)
{
	_CrtSetBreakAlloc(iAllocId);
}

#else //NO_CRT_DEBUG

#define HInitDebug()
#define HBreakOnAlloc(x)

#endif //!NO_CRT_DEBUG


#ifndef HNEW
#define HNEW new ( _NORMAL_BLOCK , __FILE__ , __LINE__ ) 
#endif

#if !defined(NO_DEBUG_NEW) && defined (HNEW)
#define new HNEW 
#endif

#define HDEBUGNAME(_name)

//inline compile only if in debug mode
#define HDBG(x) x
#endif  // _DEBUG

#if defined(_NDEBUG) && defined(_WIN32)

#ifndef NDEBUG
#define NDEBUG 1//suppress assert()
#endif

#ifndef HNEW
#define HNEW new
#endif

#include <assert.h> //assert

#define HInitDebug()
#define HDEBUGNAME(_name)
#define HDBG(x)

#endif // _NDEBUG

//---------------------------------------------------------------------------------------------------
// DLL 

#ifdef HDYNAMIC_LINKAGE

#ifdef HDLL_EXPORT
#define HABI __declspec(dllexport)
#else
#define HABI __declspec(dllimport)
#endif

#else

#define HABI

#endif

//---------------------------------------------------------------------------------------------------
// Windows
#ifdef _WIN32

// if windows.h has been included before this header, undef existing macro
#ifdef min
#undef min
#endif

#ifdef max
#undef max
#endif

#ifndef WIN32_LEAN_AND_MEAN
#define	WIN32_LEAN_AND_MEAN
#endif

#ifndef NOMINMAX
#define NOMINMAX
#endif

#include <Windows.h>
#include <ppl.h>

#endif //!_WIN32

//---------------------------------------------------------------------------------------------------
// Standard includes

#include <algorithm>

//---------------------------------------------------------------------------------------------------
// misc types & functions

struct DefaultInitializerType {};
static constexpr DefaultInitializerType DefaultInit;

#ifndef HNULLSTR
#define HNULLSTR(ptr) (((ptr) != nullptr) ? (ptr) : "" )
#endif

#ifndef HNULLSTRW
#define HNULLSTRW(ptr) (((ptr) != nullptr) ? (ptr) : L"" )
#endif

#ifndef HUNDEFINED
#define HUNDEFINED -1
#define HUNDEFINED32 0xfffffffful
#define HUNDEFINED64 0xffffffffffffffffull
#endif

#ifdef HNUCLEO
#define HNCL(x) x
#else
#define HNCL(x)
#endif

// alternative to _countof
template<typename T, size_t size>
inline constexpr size_t GetArrayLength(const T(&)[size]){return size;}

//---------------------------------------------------------------------------------------------------
// for each

// HFOREACH usage :
//std::vector<SomeClass> vec;
//HFOREACH(it,end,vec)
//{
//	it->Member++
//}

// why use this instead of this ?
//for (auto &val : container)
//{
//}

//our macro enforces the convention to get the end iterator once before iterating, not in every iteration.
//it also forces the end iterator to be const, this also means that the vector size should not be changed during iteration.

#ifndef HFOREACH
#define HFOREACH(_it,_end,_container) { auto _it = _container.begin();auto _end = _container.end(); for(;_it != _end; ++_it){
#endif

#ifndef HFOREACH_END
#define HFOREACH_END }}
#endif

#ifndef HFOREACH_TYPE
#define HFOREACH_TYPE(_it,_end,_container,_type) { _type::iterator _it = _container.begin(); _type::iterator _end = _container.end(); for(;_it != _end; ++_it){
#endif

#ifndef HFOREACH_TYPE_END
#define HFOREACH_TYPE_END }}
#endif

#ifndef HFOREACH_CONST
#define HFOREACH_CONST(_it,_end,_container) { auto _it = _container.begin();const auto _end = _container.cend(); for(;_it != _end; ++_it){
#endif

#ifndef HFOREACH_CONST_END
#define HFOREACH_CONST_END }}
#endif

#ifndef HFOREACH_CONST_TYPE
#define HFOREACH_CONST_TYPE(_it,_end,_container,_type) { _type::iterator _it = _container.begin();const _type::iterator _end = _container.cend(); for(;_it != _end; ++_it){
#endif

#ifndef HFOREACH_CONST_TYPE_END
#define HFOREACH_CONST_TYPE_END }}
#endif

// linux?
//#ifndef HFOREACH_PARALLEL
//#define HFOREACH_PARALLEL(_val, _begin, _end, _container_type)\
//	Concurrency::parallel_for(_begin,_end, [&](const _container_type::value_type& _val) {
//#endif
//
//#ifndef HFOREACH_PARALLEL_END
//#define HFOREACH_PARALLEL_END });
//#endif

//---------------------------------------------------------------------------------------------------
// safe delete & release

#ifndef HSAFE_DELETE
#define HSAFE_DELETE(x) if((x) != nullptr) {delete (x); (x) = nullptr;}
#endif

#ifndef HSAFE_FUNC_RELEASE
#define HSAFE_FUNC_RELEASE(x, _func) if((x) != nullptr) {(x)->_func(); (x) = nullptr;}
#endif

#ifndef HSAFE_DELETE_ARRAY
#define HSAFE_DELETE_ARRAY(x) for(uint32_t i = 0; i < GetArrayLength(x); ++i) { if((x)[i] != nullptr) {delete (x)[i];} (x)[i] = nullptr;}
#endif

#ifndef HSAFE_RELEASE
#define HSAFE_RELEASE(x) if((x) != nullptr){ (x)->Release(); (x) = nullptr;}
#endif

#ifndef HSAFE_MAP_RELEASE
#define HSAFE_MAP_RELEASE(x) \
HFOREACH(it, end, x) \
HSAFE_RELEASE(it->second); \
HFOREACH_END
#endif

#ifndef HSAFE_MAP_DELETE
#define HSAFE_MAP_DELETE(x) \
HFOREACH(it, end, x) \
HSAFE_DELETE(it->second); \
HFOREACH_END
#endif

#ifndef HSAFE_VECTOR_RELEASE
#define HSAFE_VECTOR_RELEASE(x) \
HFOREACH(it, end, x) \
HSAFE_RELEASE(*it); \
HFOREACH_END
#endif

#ifndef HSAFE_VECTOR_DELETE
#define HSAFE_VECTOR_DELETE(x) \
HFOREACH(it, end, x) \
HSAFE_DELETE(*it); \
HFOREACH_END
#endif

#ifndef HSAFE_VECTOR_FUNC_DELETE
#define HSAFE_VECTOR_FUNC_DELETE(x, func) \
HFOREACH(it, end, x) \
HSAFE_FUNC_RELEASE(*it, func); \
HFOREACH_END
#endif

#endif //HLX_STANDARDDEFINES_H