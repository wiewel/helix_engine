//Copyright(c) 2017
//Authors: Fabian Wahlster
//Website: www.singul4rity.com
//Contact: razor@singul4rity.com

//////////////////////////////////////////////////////////////////////////////
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files(the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute copies of the Software, and
//to permit persons to whom the Software is furnished to do so, subject to the
//following conditions:
//
//- Naming the author(s) of this software in any of the following locations:
//	About page, README file, credits.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.
//////////////////////////////////////////////////////////////////////////////

#ifndef HLX_VECTOR_H
#define HLX_VECTOR_H

#include <cstdint>
#include <type_traits>
#include <array>

namespace hlx
{
	template <typename T, int Dim = 2>
	struct VecType
	{
		VecType() {};

		template <typename... Tail>
		VecType(typename std::enable_if<sizeof...(Tail)+1 == Dim, T>::type head, Tail... tail)
			: Data{ head, T(tail)... } {}

		template <int size>
		VecType(const T(&_Other)[size])
		{
			std::copy(std::cbegin(_Other), std::cend(_Other), Data);
		}

		template <int size>
		VecType(const std::array<T, size>& _Other)
		{
			std::copy(std::cbegin(_Other), std::cend(_Other), Data);
		}

		template <int size>
		VecType& operator=(const T(&_Other)[size])
		{
			std::copy(std::cbegin(_Other), std::cend(_Other), Data);
			return *this;
		}

		template <int size>
		VecType& operator=(const std::array<T, size>& _Other)
		{
			std::copy(std::cbegin(_Other), std::cend(_Other), Data);
			return *this;
		}

		T Data[Dim] = {};
		inline T& operator[](uint32_t n)
		{
			return Data[n];
		}
		inline const T& operator[](uint32_t n) const
		{
			return Data[n];
		}
		static constexpr int Dimension = Dim;
	};

	using VecUInt2 = VecType<uint32_t, 2>;
	using VecUInt3 = VecType<uint32_t, 3>;
	using VecUInt4 = VecType<uint32_t, 4>;

	using VecInt2 = VecType<int32_t, 2>;
	using VecInt3 = VecType<int32_t, 3>;
	using VecInt4 = VecType<int32_t, 4>;

	using VecFloat2 = VecType<float, 2>;
	using VecFloat3 = VecType<float, 3>;
	using VecFloat4 = VecType<float, 4>;
} // hlx

#endif // HLX_VECTOR_H