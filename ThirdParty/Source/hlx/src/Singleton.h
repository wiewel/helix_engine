//   Copyright 2020  Steffen Wiewel, Fabian Wahlster, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef HLX_SINGLETON_H
#define HLX_SINGLETON_H

//#include <mutex>
#include <atomic>
#include <memory>

namespace hlx
{
    //Usage:
    //class A : public hlx::Singleton<A> {
    //	A() {
    //		std::cout << "A constructed!\n";
    //	}
    //
    //	~A() {
    //		std::cout << "A destructed!\n";
    //	}
    //	int k;
    //};


    template<typename T>
    class Singleton
    {
        static std::shared_ptr<T> m_pSingletonInstance;

    public:
		virtual ~Singleton() { m_pSingletonInstance = nullptr; };

        static T* Instance()
        {
			std::shared_ptr<T> pInstance(std::atomic_load_explicit(&m_pSingletonInstance, std::memory_order_acquire));

            if (pInstance == nullptr)
            {
                std::shared_ptr<T> pNewInstance(std::make_shared<T>());

                std::atomic_compare_exchange_strong_explicit(&m_pSingletonInstance, &pInstance, pNewInstance, std::memory_order_release, std::memory_order_acquire);

                return std::atomic_load_explicit(&m_pSingletonInstance, std::memory_order_acquire).get();
            }

            return pInstance.get();
        }
    };

    template<typename T>
    std::shared_ptr<T> Singleton<T>::m_pSingletonInstance = nullptr;
} //hlx

#endif // HLX_SINGLETON_H
