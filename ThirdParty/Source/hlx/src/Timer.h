//   Copyright 2020  Steffen Wiewel, Fabian Wahlster, Moritz Becher
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef HLX_TIMER_H
#define HLX_TIMER_H

#include <stdint.h>
#include "String.h"

#include <time.h>
#include <iomanip>
#include <chrono>

namespace hlx
{
	class Timer
	{
	public:
		using TClock = std::chrono::system_clock;
		using TTimePoint = std::chrono::time_point<TClock>;

		Timer();
		
		void Reset();

		float ElapsedTimeF() const;
		double ElapsedTimeD() const;		

		template <typename char_t>
		static std::basic_string<char_t> GetLocalTimeString();

		template <class Fp = float>
		inline Fp Elapsed() const;

		static int64_t GetCurrentCount();
		static double GetGlobalTime();
		static float GetGlobalTimeF();

	private:
		TTimePoint m_startTime; //time the timer was started
	};

	//---------------------------------------------------------------------------------------------------
	// constructor
	inline Timer::Timer() : m_startTime(TClock::now())
	{
	}

	//---------------------------------------------------------------------------------------------------

	//Call this function before using any other than the static functions above
	inline void Timer::Reset()
	{
		//Restart the timer
		m_startTime = TClock::now();
	}

	//---------------------------------------------------------------------------------------------------

	template <class Fp>
	inline Fp Timer::Elapsed() const { return std::chrono::duration<Fp>(TClock::now() - m_startTime).count(); }

	inline float Timer::ElapsedTimeF() const { return Elapsed<float>(); }
	inline double Timer::ElapsedTimeD() const {return Elapsed<double>(); }
	
	inline int64_t Timer::GetCurrentCount()
	{
		return clock();
	}

	inline double Timer::GetGlobalTime()
	{
		return static_cast<double>(GetCurrentCount()) / static_cast<double>(CLOCKS_PER_SEC);
	}

	inline float Timer::GetGlobalTimeF()
	{
		return static_cast<float>(GetCurrentCount()) / static_cast<float>(CLOCKS_PER_SEC);
	}
	//---------------------------------------------------------------------------------------------------

	template <typename char_t>
	inline std::basic_string<char_t> Timer::GetLocalTimeString()
	{
		time_t rawtime;
		time(&rawtime);
		struct tm timeinfo;

		if (localtime_s(&timeinfo, &rawtime) == 0)
		{
			std::basic_stringstream<char_t, std::char_traits<char_t>, std::allocator<char_t>> sstream;
			sstream << std::put_time(&timeinfo, ST(char_t, "%H::%M::%S"));
			return sstream.str();
		}
		else
		{
			return{};
		}		
	}
} // hlx

#endif // HLX_TIMER_H